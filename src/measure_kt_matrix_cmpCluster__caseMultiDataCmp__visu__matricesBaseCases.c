char stringOf_experimentDescription[2000] = {'\0'}; sprintf(stringOf_experimentDescription, "%s.%s", stringOf_experimentDescription_base, "idealCorrelation");
uint local_col_index = 0; 
uint groupId_mul = 1;
for(uint mult_factor = 1; mult_factor <= numberOf_matrices; mult_factor++) {
  s_kt_correlationMetric_t corrMetric_prior = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3); //! ie, the 'ideal appraoch.
  //!
  //! Apply logics:
  //! Note: the [”elow] "metric" prefix is used to 'give our approach' a uneique rpefix (when comarped to the "e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef"), ie, to 'aovid the file-naems from overallping, ie, to avoid in-cossitnecies in oru result-genration.
  const char *stringOf_prefix = "metric:O3--MINE--Mic"; //! ie, the 'cateory' of type "e_kt_correlationMetric_initCategory_timeComplexitySet__small_t" which we expec thte result-set to be 'gnerated' in
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases__stub__dataSets.c"
  //! Update the column-idnex-count:
  local_col_index++;
 }

//! -----------------------------------------------------------------------
//! 
//! Evlaute wrt. the different colleation-cateogry-enums
groupId_mul = 2;
local_col_index = 0; 
for(uint corr_index = 0; corr_index < (uint)e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; corr_index++) {
  const e_kt_correlationMetric_initCategory_timeComplexitySet__small_t corrEnum = (e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)corr_index;
  assert(corrEnum != e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef);
  s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(corrEnum);
  if(isTo_use_directScore__e_kt_correlationFunction(corrMetric_prior.metric_id)) {continue;} //! ie, as we assume the data-input is Not an adjecnye-matrix.
  //!
  //! Apply logics:
  const char *stringOf_prefix = getStringOf__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(corrEnum);
  const uint mult_factor = 1;
#include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases__stub__dataSets.c"
  //! Update the column-idnex-count:
  local_col_index++;
 }


/* //! ----------------------------------------------------------------------- */
/* //!  */
/* //! Evlaute wrt. different sytnetit-corr-metircs: */

/* assert(false); // FIXME: complete */

/* groupId_mul = 3; */
/* local_col_index = 0;  */
/* for(uint corr_index = 0; corr_index < (uint)e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef; corr_index++) { */
/*   const e_kt_correlationMetric_initCategory_timeComplexitySet__small_t corrEnum = (e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)corr_index; */
/*   assert(corrEnum != e_kt_correlationMetric_initCategory_timeComplexitySet__small_undef); */
/*   s_kt_correlationMetric_t corrMetric_prior = getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(corrEnum); */
/*   if(isTo_use_directScore__e_kt_correlationFunction(corrMetric_prior.metric_id)) {continue;} //! ie, as we assume the data-input is Not an adjecnye-matrix. */
/*   //! */
/*   //! Apply logics: */
/*   const char *stringOf_prefix = getStringOf__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(corrEnum); */
/*   const uint mult_factor = 1; */
/* #include "measure_kt_matrix_cmpCluster__caseMultiDataCmp__visu__matricesBaseCases__stub__dataSets.c" */
/*   //! Update the column-idnex-count: */
/*   local_col_index++; */
/*  } */
