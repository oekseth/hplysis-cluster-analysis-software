#ifndef kt_clusterResult_condensed_h
#define kt_clusterResult_condensed_h
/**
   @file 
   @brief a simplfied itnerface to store clsuter-results (oekseth, 06. apr. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks the object is (moang others) used in our "hpLysisVisu.js" to visaulzie clsuteirng-resutls.
**/
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "kt_list_1d_string.h"
#include "kt_list_1d.h"

/**
   @struct s_kt_clusterResult_condensed
   @brief a simplfied itnerface to store clsuter-results (oekseth, 06. apr. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks the object is (moang others) used in our "hpLysisVisu.js" to visaulzie clsuteirng-resutls.
**/
typedef struct s_kt_clusterResult_condensed {
  s_kt_list_1d_string_t arr_names_clustering; //! for HCA, kMeans, SOM extend the 'name-arrya' with speclized cluster-di-names, e
  s_kt_list_1d_pairFloat_t arrOf_clusterRelationships; //! ie, (head, tail, score) for the differnet relationshisp in a clustering-result. 
  s_kt_list_1d_uint_t arr_vertexTo_centroid; //! SOM: the som(x, y); kMeans and disjoint: clusterId(x), hca: the kMeans-bestPerforming-clusterId 
  s_kt_list_1d_float_t arr_hcaResult_distanceFromRoot; //! for HCA hold the min-distance from 'center' to leaf-nodes; otherwise describe [non-imaginary, imgairnary] vertex. 
  s_kt_list_1d_float_t arr_clusterSets_child_countOfScoreSum; //! the count of vertices associated 'downstream' to each vertex, ie, '1' for each 'leaf-vertex': 
  s_kt_list_1d_float_t arr_clusterSets_parent_countOfScoreSum; //! the count of vertices associated 'upstream' to each vertex, ie, '1' for each 'leaf-vertex': 
} s_kt_clusterResult_condensed_t;
/**
   @brief intiates an object to empty, and object used to provide an overview of clsuteirng-results (oekseth, 06. arp. 2017).
   @remarks the object is (moang others) used in our "hpLysisVisu.js" to visaulzie clsuteirng-resutls.
 **/
void initSetToEmpty__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *self);
//! Update the scalar_Result argument witht he data-set in quesiton.
static void s_kt_clusterResult_condensed_t__getObject__arr_names_clustering(s_kt_clusterResult_condensed_t *self, s_kt_list_1d_string_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = self->arr_names_clustering ;
}
//! Update the scalar_Result argument witht he data-set in quesiton.
static void s_kt_clusterResult_condensed_t__getObject__arrOf_clusterRelationships(s_kt_clusterResult_condensed_t *self, s_kt_list_1d_pairFloat_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = self->arrOf_clusterRelationships;
}
//! Update the scalar_Result argument witht he data-set in quesiton.
static void s_kt_clusterResult_condensed_t__getObject__arr_vertexTo_centroid(s_kt_clusterResult_condensed_t *self, s_kt_list_1d_uint_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = self->arr_vertexTo_centroid;
}
//! Update the scalar_Result argument witht he data-set in quesiton.
static void s_kt_clusterResult_condensed_t__getObject__arr_hcaResult_distanceFromRoot(s_kt_clusterResult_condensed_t *self, s_kt_list_1d_float_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = self->arr_hcaResult_distanceFromRoot;
}
//! Update the scalar_Result argument witht he data-set in quesiton.
static void s_kt_clusterResult_condensed_t__getObject__arr_clusterSets_child_countOfScoreSum(s_kt_clusterResult_condensed_t *self, s_kt_list_1d_float_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = self->arr_clusterSets_child_countOfScoreSum;
}
//! Update the scalar_Result argument witht he data-set in quesiton.
static void s_kt_clusterResult_condensed_t__getObject__arr_clusterSets_parent_countOfScoreSum(s_kt_clusterResult_condensed_t *self, s_kt_list_1d_float_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = self->arr_clusterSets_parent_countOfScoreSum;
}
/* //! Update the scalar_Result argument witht he data-set in quesiton. */
/* static void s_kt_clusterResult_condensed_t__getObject__(s_kt_clusterResult_condensed_t *self, s_kt_list_1d_float_t *scalar_result) { */
/*   assert(self); assert(scalar_result); */
/*   *scalar_result = self-> ; */
/* } */


/**
   @brief intiates an object to empty, and object used to provide an overview of clsuteirng-results (oekseth, 06. arp. 2017).
   @return an itnalized object of the s_kt_clusterResult_condensed_t type
   @remarks the object is (moang others) used in our "hpLysisVisu.js" to visaulzie clsuteirng-resutls.
 **/
s_kt_clusterResult_condensed_t initAndReturn__s_kt_clusterResult_condensed_t();
//! Construct a new-allocates s_kt_clusterResult_condensed_t cluster-object (oekseth, 06. apr. 2017).
void init__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *self, const uint nrows, const uint max_cntClusters);
//! @return a new-allocates s_kt_clusterResult_condensed_t cluster-object (oekseth, 06. apr. 2017).
s_kt_clusterResult_condensed_t init__s_kt_clusterResult_condensed_t(const uint nrows, const uint max_cntClusters);
//! De-allcoates memory in an tianlseid s_kt_clusterResult_condensed_t object
void free__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *self);
//! Relate the head to the tail, and udpat ehte inverse relationship-mapping (oekseth, 06. apr. 2017).
void relate__useCount_updateInverse__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *self, const uint head, const uint tail, const t_float score);

/**
   @brief export the object to a TSV-sparse-fileformat (oekseth, 06. apr. 2017).
   @param <self>  the object which hodl the alredy-comptued cluster-results
   @param <fileName_result> the new fiel to constuct: if Not set, then we export to STDOUT
   @param <seperator> optioanl: the sepertor between tntiees, eg, "&" for LAtex, "," for tsv and "," for CSV
   @param <seperator_newLine> optioanl: the sepertor between each row, eg, "\\" for Latex, and "\n" for tsv and  CSV
   @return true upon success.
   @remarks in the export-format we cpature use-cases ipmictly described (among others) in: "hpLysisVisu_api__initClusterResult(..)" ("hpLysisVisu_server.js"), "export_result_clusterResults(..)" ("hpLysis.pm"), "hpLysisVisu_api__initClusterResult(..)" tut-example ("tut_hpLysis.js")
 **/
bool export__toFormat__tsv__s_kt_clusterResult_condensed_t(const s_kt_clusterResult_condensed_t *self, const char *fileName_result, const char *sepertor, const char *seperator_newLine);
/**
   @brief export the object to a java-script-fileformat (oekseth, 06. apr. 2017).
   @param <self>  the object which hodl the alredy-comptued cluster-results
   @param <fileName_result> the new fiel to constuct: if Not set, then we export to STDOUT
   @return true upon success.
   @remarks in the export-format we cpature use-cases ipmictly described (among others) in: "hpLysisVisu_api__initClusterResult(..)" ("hpLysisVisu_server.js"), "export_result_clusterResults(..)" ("hpLysis.pm"), "hpLysisVisu_api__initClusterResult(..)" tut-example ("tut_hpLysis.js")
 **/
bool export__toFormat__javaScript__s_kt_clusterResult_condensed_t(const s_kt_clusterResult_condensed_t *self, const char *fileName_result);



#endif //! EOF
