#include "kt_api.h" //! which is sued to access the input-matrix.
#include "kt_sparse_sim.h"
//! ------------------
//#include "graphAlgorithms_distribution.h"
//#include "graphAlgorithms_distance.h"
//! ------------------
#include "kt_clusterAlg_fixed.h"
#include "graphAlgorithms_timeMeasurements_syntetic.h"
#include "graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary.h"
//#include "s_dense_closestPair.h"
#include "assert_intri.h"
#include "measure_intri.h"
#include "measure_som.h"
#include "measure_syntetic_tiling_fixedRowAppliedOn2dMatrix.h"
#include "measure_codeStyle_functionRef_api.h"
#include "kt_matrix_clusterDistance.h"
#include "measure_distribution_studentT.h"
#include "measure_kt_clusterAlg_fixed.h"
#include "measure_kt_clusterAlg_hca.h"
#include "measure_externalCxx__nipals.h"
//#include "measure_kt_centrality.h"
#include "measure_kt_set_sparse.h"
#include "measure_kt_forest_findDisjoint.h"
#include "measure_kt_para_3dSchedule.h"
#include "measure_kt_matrix_filter.h"
#include "measure_mask.h"
#include "measure_sort.h"
#include "measure_correlation.h"
#include "measure_terminal.h"
#include "ex_kMeans.h"
#include "measure_kt_matrix_cmpCluster.h"
#include "hpLysis_api.h"
#include "measure_base.h"
#include "kt_math_matrix.h" //! used wrt. our "kt_func_metricInner_fast__fitsIntoSSE__euclid__float(..)"
#include "measureStub__case__basicCases__language.c" //! ie, include directly: describes function "measureStub__case__basicCases__language(..)" (oekseth, 06. feb. 2017).
#include "kt_terminal.h"
#include "measure_base.h"
#include "kt_distance.h" //! which (among others) is used for comptatuion of corrleation-atmrix in oru [”elow] exampels.
#include "correlation_base.h"
#include "correlation_macros__distanceMeasures.h"
#include "hpLysis_api.h"
#include "kt_assessPredictions.h"
#include "math_generateDistribution.h" //! which is used iot. 'gave' radnom permtuations.
#include "hp_evalHypothesis_algOnData.h"
#include "db_ds_directMapping.h"
#include "measure_db.h"
#include "hp_distance_wrapper.h"
#include "tut_wrapper_histogram.h"
#include "tut_wrapper_emd.h"
#include "hp_api_fileInputTight.h"
#define __M__calledInsideFunction 1//! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "tut_wrapper_image_segment.h"
#include "alg_dbScan_brute.h"
#include "tut_disjointCluster_2_algComparisonSingleAlg.c"
#include "tut_disjointCluster_3_algComparisonSingleMatrix.c"
#include "tut_disjointCluster_4_differentAlgDifferentSyntheticDataSets.c"
#include "tut_ccm_17_ccmAlgDataSyn.c"
#include "tut_sintef_1_helloWorld.c"
//! ------------------------
/* //! Libraries for file-copying: */
/* #include <sys/types.h> */
/* #include <sys/stat.h> */
/* #include <unistd.h> */
/* //! ------------------------ */

#include "tut_kd_1_matrix_sse.c"
#include "tut_simEval_1_syntheticData_syntheticFunctions.c" 
#undef __M__calledInsideFunction


#include "kt_entropy.h"

#include "hp_clusterShapes.h" //! ie, used in our "testHardCodedDistributions--dataUsedAsInput" and our "testHardCodedDistributions--CCM"
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
#include "matrixdata.h" //! which is sued for the Vilje-matrix-data-loading in our "tut_inputFile_6.c" (eosekth, 60. feb. 2017).
#include "aux_sysCalls.h"
#include "hp_ccm.h"
#include "aux_exportMeta.h" //! which is used to 'enable' the 'pre-construciton' of web-based visualziaiton-wrappers, eg, using the "Hiogcharts.js" java-scirpt-lbirary (oeskejth, 06. mar. 2017)
#define __M__calledInsideFunction
//! ------------------
#undef __MiV__mainIsDecelared
#include "tut_sse_matrixMul_cmpApproaches.c"
#undef  __M__calledInsideFunction

//! Specify a gnerlaized funciton to idnetifyu the result-directiry.
#define __MF__getResultPrefixFromArg(defArg, arg_pos) ({const char *resultPrefix = defArg; if(array_cnt > arg_pos) {const char *arg = array[arg_pos]; if(arg && strlen(arg)) {resultPrefix = arg;}} resultPrefix;})


static void apply__tut_inputFile_9__algCmp__syntAndRealLife() {
  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
  const bool isTo__testHCA_komb__inKmeans = false;
#define __M__calledInsideFunction
  //#define __Mi__useSmall 1
#define __Mi__useSmall 1
#define __Mi__dataCaseLarge 2
  const char *stringOf_resultDir = "";
#include "tut_inputFile_9__algCmp__syntAndRealLife.c"
#undef __M__calledInsideFunction
#undef __Mi__useSmall
#undef __Mi__dataCaseLarge
}

#include "hp_entropy.h"
#include "kt_sim_string.h"

static void apply__tut_random_difference_1() {
#define __M__calledInsideFunction
#include "tut_random_difference_1.c"
#undef __M__calledInsideFunction
}
static void apply__tut_sim(const char *resultPrefix) {
#define __M__calledInsideFunction
  const uint nrows = 3; const uint ncols = 100;  const uint nrows_2 = 10; //! where latter is used in some of our examples.
  {
#include "tut_sim_9_manyMany_Kendall_coVariance.c"
  }
  {
#include "tut_sim_3_vec_MINE.c"
  }
  { //! Use a diffenre tsize-prop to 'foce' an SSE-evalaution of our appraoch:
    const uint nrows = 1; const uint ncols = 170;  const uint nrows_2 = 300; //! where latter is used in some of our examples.
#include "tut_sim_5_oneToMany_hideValues_Gower.c"
  }
  {
#include "tut_sim_1_vec_Euclid.c"
  }
  {
#include "tut_sim_2_vec_Shannon.c"
  }
  {
#include "tut_sim_4_vec_chebychev.c"
  }
  {
#include "tut_sim_6_oneToMany_squaredChord.c"
  }
  {
#include "tut_sim_7_oneToMany_Lortentizan.c"
  }
  {
#include "tut_sim_8_oneToMany_WaweHedges.c"
  }
  {
#include "tut_sim_10_manyMany_sampleCoVariance.c"
  }
  {
#include "tut_sim_11_manyMany_max_cosine.c"
  }
  {
#include "tut_sim_12_manyMany_min_Neyman.c"
  }
  {
#include "tut_sim_13_manyMany_min_Dice.c"
  }
  {
#include "tut_sim_14_manyMany_Matusita__setInputInLoop.c"
  }
#undef __M__calledInsideFunction
}

#define __M__calledInsideFunction
#include "tut_ccm_11_compareMatrixBased.c" //! which defined the function
#undef __M__calledInsideFunction

static void apply__tut_ccm(const char *resultPrefix) {
#define __M__calledInsideFunction
  {
#include "tut_ccm_14_matrixVSgold_SSE.c"
    assert(false); // FIXEM: remvoe.
  }
  {
  const uint nclusters = 10; const uint nrows = 100; const uint ncols = 20;
  s_kt_correlationMetric_t obj_metric = setTo_empty__andReturn__e_kt_correlationMetric_initCategory_t(e_kt_correlationMetric_initCategory_inAccurateAndFast_timeComplexity_O3); //! ie, the simlairty-metirc to be used.
#include "tut_ccm_12_nestedApplication__centroid_simMetric_ccm.c"
  //assert(false); // FIXEM: remvoe.
  }

  {
#include "tut_ccm_1_matrixVSgold_VRC.c"
  }
  {
    #include "tut_ccm_2_matrixVSgold_Silhouette.c"
  }
  {
    #include "tut_ccm_3_matrixVSgold_DavisBouldin.c"
  }
  {
    #include "tut_ccm_4_matrixVSgold_Dunn.c"
  }
  {
#include "tut_ccm_5_goldVSgold_RandsIndex_alt1.c"
  }
  {
#include "tut_ccm_6_goldVSgold_RandsIndex_alt2.c"
  }
  {
#include "tut_ccm_7_goldVSgold_ARI.c"
  }
  {
#include "tut_ccm_8_goldVSgold_chiSquared.c"
  }
  {
#include "tut_ccm_9_goldVSgold_FowlesMallows.c"
  }
  {
#include "tut_ccm_10_goldVSgold_fMeasure.c"
  }
#define call_apply_tut_ccm_11_compareMatrixBased() ({char filePrefix_spec[2000] = {'\0'}; sprintf(filePrefix_spec, "%s%s_%u", resultPrefix, filePrefix, ncluster_id); apply_tut_ccm_11_compareMatrixBased(filePrefix_spec, config_cnt_casesToEvaluate, config_nrows_base, config_clusterBase, isTo_useRverseOrder_inInit, isTo_setClusterSzei_toFixed_sizes,score_weak);})  
  for(uint ncluster_id = 1; ncluster_id < 4; ncluster_id++) {
    uint config_clusterBase = 5;  
    if(ncluster_id == 1) {config_clusterBase = 1;} 
    else if(ncluster_id == 2) {config_clusterBase = 2;}
    else if(ncluster_id == 3) {config_clusterBase = 50;}
    else {assert(false);} //! ie, add for this case
    {
      const bool isTo_useRverseOrder_inInit = false;
      {
	const uint config_cnt_casesToEvaluate = 10;
	const uint config_nrows_base = 100; //const uint config_clusterBase = 5;  
	const char *filePrefix = "ccm_small";
	const t_float score_weak = 100;
	const bool isTo_setClusterSzei_toFixed_sizes = true;
      }
      {
	const uint config_cnt_casesToEvaluate = 100;
	const uint config_nrows_base = 100; // const uint config_clusterBase = 5;  
	const char *filePrefix = "ccm_1a_syntLinearEval";
	const t_float score_weak = 100;
	const bool isTo_setClusterSzei_toFixed_sizes = true;
	call_apply_tut_ccm_11_compareMatrixBased(); // #include "tut_ccm_11_compareMatrixBased.c"
      }
      {
	const uint config_cnt_casesToEvaluate = 100;
	const uint config_nrows_base = 100; // const uint config_clusterBase = 5;  
	const char *filePrefix = "ccm_1b_syntLinearEval__biggerScoreDiff";
	const t_float score_weak = 1000;
	const bool isTo_setClusterSzei_toFixed_sizes = true;
	call_apply_tut_ccm_11_compareMatrixBased(); // #include "tut_ccm_11_compareMatrixBased.c"
      }
    }
    // ---------- 
    {
      const bool isTo_useRverseOrder_inInit = true;
      {
	const uint config_cnt_casesToEvaluate = 100;
	const uint config_nrows_base = 100; // const uint config_clusterBase = 5;  
	const char *filePrefix = "ccm_2a_syntLinearEval_revInit";
	const t_float score_weak = 100;
	const bool isTo_setClusterSzei_toFixed_sizes = true;
	call_apply_tut_ccm_11_compareMatrixBased(); // #include "tut_ccm_11_compareMatrixBased.c"
	  }
      {
	const uint config_cnt_casesToEvaluate = 100;
	const uint config_nrows_base = 100; // const uint config_clusterBase = 5;  
	const char *filePrefix = "ccm_2b_syntLinearEval_revInit__biggerScoreDiff";
	const t_float score_weak = 1000;
	const bool isTo_setClusterSzei_toFixed_sizes = true;
	call_apply_tut_ccm_11_compareMatrixBased(); // #include "tut_ccm_11_compareMatrixBased.c"
      }
    }
    // ---------- 
    {
      const bool isTo_useRverseOrder_inInit = true;
      const bool isTo_setClusterSzei_toFixed_sizes = false;
      {
	const uint config_cnt_casesToEvaluate = 100;
	const uint config_nrows_base = 100; //const uint config_clusterBase = 5;  
	const char *filePrefix = "ccm_3a_syntLinearEval_revInit_dynSize";
	const t_float score_weak = 100;
	call_apply_tut_ccm_11_compareMatrixBased(); // #include "tut_ccm_11_compareMatrixBased.c"
      }
      {
	const uint config_cnt_casesToEvaluate = 100;
	const uint config_nrows_base = 100; //const uint config_clusterBase = 5;  
	const char *filePrefix = "ccm_3b_syntLinearEval_revInit_dynSize__biggerScoreDiff";
	const t_float score_weak = 1000;
	call_apply_tut_ccm_11_compareMatrixBased(); // #include "tut_ccm_11_compareMatrixBased.c"
      }
    }
  }
//   {
// #include ""
//   }
#undef __M__calledInsideFunction
}

#define __MF__concatStrings(str, prefix, suffix) ({sprintf(str, "%s%s", prefix, suffix);})
#define __MF__concatStrings__build(prefix) char stringOf_resultDir_base[2000] = {'\0'}; sprintf(stringOf_resultDir_base, "%s%s", prefix, stringOf_prefix);

static void apply__tut_hypothesis_whyResultDiffers_1_dataSyn(const char *stringOf_prefix) {
#define __Mi__createDirectory
#define __M__calledInsideFunction
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.
  __MF__concatStrings__build("results/differentHyp/dataSyn_"); //! ie, constrct a "stringOf_resultDir_base" string
  //const char *stringOf_resultDir_base = "results/differentHyp/dataSyn_";
  //!
  //! Incldue wrapper-logics:
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
  //! 
  //! Reset macros:
#undef __M__calledInsideFunction
#undef __Mi__createDirectory
#undef __Mi__fileToHold__dataLoads
}
static void apply__tut_hypothesis_whyResultDiffers_2_realMine(const char *stringOf_prefix) {
#define __Mi__createDirectory
#define __M__calledInsideFunction
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
  //const char *stringOf_resultDir_base = "results/differentHyp/dataRealMine_";
  __MF__concatStrings__build("results/differentHyp/dataRealMine_"); //! ie, constrct a "stringOf_resultDir_base" string
  //__MF__concatStrings__build("results/differentHyp/_"); //! ie, constrct a "stringOf_resultDir_base" string
  //!
  //! Incldue wrapper-logics:
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
  //! 
  //! Reset macros:
#undef __M__calledInsideFunction
#undef __Mi__createDirectory
#undef __Mi__fileToHold__dataLoads
}
static void apply__tut_hypothesis_whyResultDiffers_4_realMine_large(const char *stringOf_prefix) {
#define __Mi__createDirectory
#define __M__calledInsideFunction
  //! Note: [”elow] coide-chunk is 'taken from' our "tut_inputFile_4.c":
  const uint config__kMeans__defValue__k__min = 2;
  const uint config__kMeans__defValue__k__max = 4;
  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__realMine_large.c" //! ie, the file which hold the cofniguraitosn to be used.
  //const char *stringOf_resultDir_base = "results/differentHyp/dataRealMine_large_";
  __MF__concatStrings__build("results/differentHyp/dataRealMine_large_"); //! ie, constrct a "stringOf_resultDir_base" string
  //!
  //! Incldue wrapper-logics:
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
  //! 
  //! Reset macros:
#undef __M__calledInsideFunction
#undef __Mi__createDirectory
#undef __Mi__fileToHold__dataLoads
}

static void apply__tut_hypothesis_whyResultDiffers_3_synt_mathFunctions__noise(const char *stringOf_prefix) {
#define __Mi__createDirectory
#define __M__calledInsideFunction
  //! Note: [”elow] coide-chunk is 'taken from' our "tut_inputFile_3.c":
  const uint config__kMeans__defValue__k__min = 2;
  const uint config__kMeans__defValue__k__max = 4;
  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.
  //  const char *stringOf_resultDir_base = "results/differentHyp/data_mathFuncNoise_";
  __MF__concatStrings__build("results/differentHyp/data_mathFuncNoise_"); //! ie, constrct a "stringOf_resultDir_base" string
  //!
  //! Incldue wrapper-logics:
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
  //! 
  //! Reset macros:
#undef __M__calledInsideFunction
#undef __Mi__createDirectory
#undef __Mi__fileToHold__dataLoads
}
static void apply__tut_hypothesis_whyResultDiffers_5_realMine(const char *stringOf_prefix) {
#define __Mi__createDirectory
#define __M__calledInsideFunction
  //! Note: [”elow] coide-chunk is 'taken from' our "tut_inputFile_5.c":
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
  {
    //const char *stringOf_resultDir_base = "results/differentHyp/dataRealKT_";
    __MF__concatStrings__build("results/differentHyp/dataRealKT_transp_"); //! ie, constrct a "stringOf_resultDir_base" string
    //!
    //! Incldue wrapper-logics:
#define __MiC__isTo_useTrans true //! ie, to transpose the matrix. 
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
#undef __MiC__isTo_useTrans
  }
  {
    //const char *stringOf_resultDir_base = "results/differentHyp/dataRealKT_";
    __MF__concatStrings__build("results/differentHyp/dataRealKT_"); //! ie, constrct a "stringOf_resultDir_base" string
    //!
    //! Incldue wrapper-logics:
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
  }
  //! 
  //! Reset macros:
#undef __M__calledInsideFunction
#undef __Mi__createDirectory
#undef __Mi__fileToHold__dataLoads
}

static void apply__tut_hypothesis_whyResultDiffers_6_Vilje(const char *stringOf_prefix) {
#define __Mi__createDirectory
#define __M__calledInsideFunction
  //! Note: [”elow] coide-chunk is 'taken from' our "tut_inputFile_6_vilje.c":
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__Vilje.c" //! ie, the file which hold the cofniguraitosn to be used.
  //const char *stringOf_resultDir_base = "results/differentHyp/Vilje_";
  __MF__concatStrings__build("results/differentHyp/Vilje_"); //! ie, constrct a "stringOf_resultDir_base" string
  //!
  //! Incldue wrapper-logics:
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
  //! 
  //! Reset macros:
#undef __M__calledInsideFunction
#undef __Mi__createDirectory
#undef __Mi__fileToHold__dataLoads
}
/**
   @brief test effects of matrix-dimensions: build a set of evlauations where we grudally increase the data-size (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks based on "tut_inputFile_7.c": a permtuation of "tut_inputFile_3.c".
   @remarks evaluate the exec-time-effect wrt. different dimesnions of the input-amtrix .... eg, [300, 300], [3000, 300], [12000,300], [300, 3000], [300, 12000], [12000, 12000] ...
**/
static void apply__tut_hypothesis_whyResultDiffers_7_differentDimensions_synt(const char *stringOf_prefix) {
#define __Mi__createDirectory
#define __M__calledInsideFunction
  //! Note: [”elow] coide-chunk is 'taken from' our "tut_inputFile_7.c":
  //! ---------------------------------------------
  const uint config__matrixDims__minSize = 256;
  const uint config__matrixDims__maxSizeApprox = 12000;
  const uint config__matrixDims__stepMult = 4;
  const uint config__kMeans__defValue__k__min = 2;
  const uint config__kMeans__defValue__k__max = 4;
  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
  //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
  // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
  const char *config__nameOfDefaultVariable = "uniform";
  const char *config__nameOfDefaultVariable__2 = "binomial_p005";
  //! ---------------------------------------------
  const uint __cnt_cases__eachDim = (config__matrixDims__maxSizeApprox - config__matrixDims__minSize)/config__matrixDims__stepMult;
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__differentDimensions.c" //! ie, the file which hold the cofniguraitosn to be used.
  //const char *stringOf_resultDir_base = "results/differentHyp/synt_differentDimensions_";
  __MF__concatStrings__build("results/differentHyp/synt_differentDimensions_"); //! ie, constrct a "stringOf_resultDir_base" string
  //!
  //! Incldue wrapper-logics:
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
  //! 
  //! Reset macros:
#undef __M__calledInsideFunction
#undef __Mi__createDirectory
#undef __Mi__fileToHold__dataLoads
}
/**
   @brief elvauat ethe "data/FCPS/01FCPSdata/" data-set (oekseth, 06. feb. 2017).   
   @remarks based on "tut_inputFile_8.c" and "tut_inputFile_4.c"
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks a permtuation of "tut_inputFile_4.c".
   @remarks thsi exampel is indteded to increase a users famliarty with how data-sets may be applied iot apply large-scale clsuteirnga-anlsyssi.
**/
static void apply__tut_hypothesis_whyResultDiffers_8_FCPS(const char *stringOf_prefix) {
#define __Mi__createDirectory
#define __M__calledInsideFunction
  //! Note: [”elow] coide-chunk is 'taken from' our "tut_inputFile_8.c":
  const uint config__kMeans__defValue__k__min = 2;
  const uint config__kMeans__defValue__k__max = 4;
  //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
  // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
  const char *config__nameOfDefaultVariable = "binomial_p005";
  assert(config__kMeans__defValue__k__max >= config__kMeans__defValue__k__min);
  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
#define __Mi__fileToHold__dataLoads "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
  //const char *stringOf_resultDir_base = "results/differentHyp/FCPS_";
  __MF__concatStrings__build("results/differentHyp/FCPS_"); //! ie, constrct a "stringOf_resultDir_base" string
  //!
  //! Incldue wrapper-logics:
#include "config_tut_hypothesis_whyResultDiffers_1__config.c"
  //! 
  //! Reset macros:
#undef __M__calledInsideFunction
#undef __Mi__createDirectory
#undef __Mi__fileToHold__dataLoads
}


static void apply__matrix() {
 #define __M__calledInsideFunction
  //#include ""
#include "tut_matrix_0_initAccessAndFree.c"
#include "tut_matrix_1_toAndFromFile_values.c"
#include "tut_matrix_2_toAndFromFile_values__explicitWriting.c"
#include "tut_matrix_3_toAndFromFile_valuesAndStrings.c"
 #undef __M__calledInsideFunction
}
static void apply__kt_1d_list() {
#define __M__calledInsideFunction
#include "tut_list_2_pairFloat_readFromFile_sim.c"
#include "tut_list_1_pairFloat_sim.c"
#include "tut_list_0_pairFloat.c"
  //
// #include ""
#undef __M__calledInsideFunction
}



static void apply__kt_sparseSim(const char *stringOf_resultPrefix) {
 #define __M__calledInsideFunction
  //const char *stringOf_resultPrefix = "sparseSim_apache_";
#include "tut_sparseSim_7_realLife_apacheLog_compareMultipleFiles.c" //! where latter use the "stringOf_resultPrefix" to 'constuct' the rsutl-file.
  //! 
#include "tut_sparseSim_1_vec_Euclid.c"
#include "tut_sparseSim_2_oneToMany_Shannon.c"
#include "tut_sparseSim_3_manyToMany_densePostSelfComparison_Canberra.c"
#include "tut_sparseSim_4_fromFile_intersection__postOps_hca.c"
#include "tut_sparseSim_5_para_oneToMany_Gower.c"
#include "tut_sparseSim_6_para_manyMany_denseResult_Topsoe.c"
  /*
#include ""
#include ""
*/
// #include ""
 #undef __M__calledInsideFunction
}
#define __M__calledInsideFunction
#include "tut_clust_dynamicK_6_find_nCluster_multipleData_Euclid.c" //! where altter fucniton is called in [below]
#include "tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering.c" //! where altter fucniton is called in [below]
#undef __M__calledInsideFunction

static void __apply__tut_dynamicK__syntetic__noise(const char *resultPrefix) {
#include "__apply__tut_dynamicK__syntetic_config.c"
    { 
#include "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Dynamically evlauate the acucrayc of differnet simliarty-metrics and cluster-algortihsm wrt. each of the data-sets, ie, producing (for each clsutering-algorithm) a data-matrix [simMetric][data_id] (oekseth, 06. arp. 2017)
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
      //! And at this exec-point we assume the 'ccm-eval-operiaotn' to have been completed
    }
}

// !! Manuyally set the cluster-count = 'nrows/2': Your configuraiton does not follow the minimum-requirements: we the 'nclusters'=4294967295 attribute to be set to less than 10 vertices, a case which does Not hold; if evlauating your latter API does not help, then we suggest contacting the senior devleoper at [oekseth@gmail.com]. Observaiton at [cluster__hpLysis_api]:/home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/hpLysis_api.c:809


// kt_api__func__kmeans__findCentralElements.c
static void __apply__tut_dynamicK__syntetic__differentDims(const char *resultPrefix) {
#include "__apply__tut_dynamicK__syntetic_config.c"
    {
#include "tut__aux__dataFiles__differentDimensions_wrapper.c"
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }
}

static void __apply__tut_dynamicK__syntetic(const char *resultPrefix) {
#include "__apply__tut_dynamicK__syntetic_config.c"
    //! Specify the data-set to use: 
    { 
#include "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }

}
static void __apply__tut_dynamicK__realLife(const char *resultPrefix) {
#include "__apply__tut_dynamicK__syntetic_config.c"
   { 
#include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics (oekseth, 06. arp. 2017)
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }
    {
  //  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
#include "tut__aux__dataFiles__realMine_large.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }
    if(false) //! where 'this' is omitted as "syntetic_noise" seems to be aasicated with a relative large tiem-cost
    {
#include "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }
    { //const char *nameOf_experiment = "vincentarelbundock";  //! ie, where latter data-sets are created by "vincentarelbundock". 
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }
    //!
    //!
    if(false) //! where 'this' is omitted as Vilje seems to be aasicated with a relative large tiem-cost
    { const char *nameOf_experiment = "vilje";  //! 
#include "tut__aux__dataFiles__Vilje.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }

    { 
#include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }
}
static void apply__tut_dynamicK(const char *resultPrefix) {
#define __M__calledInsideFunction
  
  { //! Build result-matrices where we evlauate for different HCA:
#include "__apply__tut_dynamicK__syntetic_config.c"
    //! Comptue for: real-life:
    __apply__tut_dynamicK__realLife(resultPrefix);
    //! Comptue for: sytnetic:
    __apply__tut_dynamicK__syntetic(resultPrefix);
    __apply__tut_dynamicK__syntetic__noise(resultPrefix);
    if(false) { //! where [”elow] seems to be assicated with a high exeuction-time-cost
      __apply__tut_dynamicK__syntetic__differentDims(resultPrefix);
    }

    /*
    { const char *nameOf_experiment = "";  //! 
#include "" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering__setOf_clustAlgs__CCMs(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset);
    }
*/
    //! -----------------------------------------------------------------------------------------
    //!
    //! Completed: 
    //assert(false); // FIXME: remove
  }
  {    
  //! Specify the data-set to use: 
#include "tut__stub__config__inputData.c" //! which is used as 'cefualt cofnigruations' for [”elow]
#include "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.
    //! Cofngiure parameters:
    const e_hpLysis_clusterAlg_t clustAlg = e_hpLysis_clusterAlg_HCA_single;
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
    const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    //! Apply logics: 
    tut_clust_dynamicK_6_find_nCluster_multipleData_Euclid(mapOf_realLife, mapOf_realLife_size, metric_id, clustAlg, config__ccmMatrixBased__toUse);
  }
#include "tut_clust_dynamicK_1_hca_Silhoette.c"
#include "tut_clust_dynamicK_2_kMeans_Silhoette.c"
#include "tut_clust_dynamicK_3_miniBatch_Dunn.c"
#include "tut_clust_dynamicK_4_kruskal_VRC.c"
#include "tut_clust_dynamicK_5_hca_forResult_mulitpleCCMs.c"
  /*

#include ""
#include ""
  */
#undef __M__calledInsideFunction

}
//! Evluate logics in our "hp_api_fileInputTight.h"
static void hp_api_fileInputTight() {
#define __M__calledInsideFunction
  // #include ""
#include "tut_hp_api_fileInputTight_2_ccm_2_matrixHyp_sparseInput.c"
  assert(false); // FIXME: remove.
 #include "tut_hp_api_fileInputTight_10_exportClusterResult.c"
 #include "tut_hp_api_fileInputTight_9_clustAlg_3_sparse_kMeans.c"
#include "tut_hp_api_fileInputTight_8_clustAlg_2_dynamicK_hca.c"
 #include "tut_hp_api_fileInputTight_7_clustAlg_1_hca.c"
 #include "tut_hp_api_fileInputTight_6_dist_3_sparse_Pearson.c"
#include "tut_hp_api_fileInputTight_5_dist_2_CityBlock.c"
#include "tut_hp_api_fileInputTight_4_dist_1_Euclid.c"
  /*
  assert(false); // FIXME: remove.
  */
#include "tut_hp_api_fileInputTight_3_ccm_3_hypHyp.c"
#include "tut_helloWorld.c" //! which 'wraps' oguics foudn in our "hp_api_fileInputTight.h".
  assert(false); // FIXME: remove.
#include "tut_hp_api_fileInputTight_1_ccm_1_matrixHyp.c"
// #include ""
  //assert(false); // FIXME: remove.

#undef __M__calledInsideFunction
}

#define __M__calledInsideFunction 1
#include "tut_ccm_13_categorizeData.c"
#undef __M__calledInsideFunction

//! **********************************'                                                                                                                                                                                           
//! **********************************'                                                                                                                                                                                           
//! **********************************'                                                                                                                                                                                           //! ----------------------------------------------------------------
//!
//! Idea: rpovide a geneirc strategy to spefiy fucnitons, thereby simplifying future updates and fucniton-genrlaiziaotns (oekseth, 06. apr. 2017).
//#ifndef __MiFu__tutSubCasemanyToMany
#define __MiFu__tutSubCase__internal_inner(fun,suffix) fun ## _ ## suffix
#define __MiFu__tutSubCase__internal(fun,suffix) __MiFu__tutSubCase__internal_inner(fun, suffix)
//! -------------------------------------------------------------------------------------------------------------------
//! Start:"tut_ccm_13_categorizeData.c": -------------------------------------------------------------------------------------------------------------------
#define MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment) ({ \
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%u_%s", resultPrefix, *data_id_category_global, nameOf_experiment); \
  tut_ccm_13_categorizeData(mapOf_realLife, mapOf_realLife_size, resultPrefix__dataSubset, *data_id_category_global); /*! where latter is deifne din our "tut_ccm_13_categorizeData.c".*/ \
  *data_id_category_global++; /*! ie, increment. */})
#define __MiFu__tut_ccm_13_categorizeData(suffix) __MiFu__tutSubCase__internal(tut_ccm_13_categorizeData, suffix)
//#endif //! ie, as we otehrwise assuemt aht these 'fucntion-macro-wrapeprs' are defined (oekseth, 06. otk. 2016).                                                                                                               
#define __MiF__wrapperTut__fileSets__internalFunc() ({MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment);}
#define __MiFa__wrapperTut__prefixName __MiFu__tut_ccm_13_categorizeData
#define __MiFa__wrapperTut__funcArgs   const char *resultPrefix, uint *data_id_category_global
#include "wrapperTut__fileSets.c"
//! completed:"tut_ccm_13_categorizeData.c"-------------------------------------------------------------------------------------------------------------------
//! -------------------------------------------------------------------------------------------------------------------


//! Main-function to [ªbove]
static void __local__tut_ccm_13_categorizeData(const char *resultPrefix) {
  uint data_id_category = 0;
  //§ NotE: [”elow] wrapper is used/itnroduced to both simplfiy reading, and simplfiy future code-updates.
#define __MiF__call(group) ({__MiFu__tut_ccm_13_categorizeData(group)(resultPrefix, &data_id_category);})
  //! MAke the fucniton-calls:
  //! Note: compelcity (of [”elow] is due tot he limtieaitons of stack-size, ie, where a 'simple' 'incldue ""' woudl result in run-time-error. 
  __MiF__call(syntetic);
  if(false) { //! where 'thsi' option is used to avodi large tiem-lags when we investigate the 'bes-tfit-cases'w r.t he diffneret sytntic data-sets.-
    __MiF__call(syntetic_noise);
    __MiF__call(syntetic_differentDims);
    __MiF__call(realLife_1);
    __MiF__call(realLife_1_large);
    __MiF__call(vincentarelbundock);
    __MiF__call(FCPS);
  }
  // __MiF__call();
  // __MiF__call();
  // __MiFu__tut_ccm_13_categorizeData(syntetic)(resultPrefix, &data_id_category);
  // __MiFu__tut_ccm_13_categorizeData(syntetic)(resultPrefix, &data_id_category);
}

#define __MiF__startMeasurement__newCase() ({fprintf(stderr, "\n\n#!  %s\n", str_measurement);})
#include "cluster.c" //! ie, to ensure that we do nto voersimpfly wr.t peromfrnac-eocmparison-aprpaoches

#include "tut_perf_metric_slowVS_fast_rankBased.c"



static void apply__entropy() {
#define __M__calledInsideFunction
#include "tut_entropy_1_shannon.c"
  assert(false); // FIXME: remvoe ... then add support for parallel wrappers ... 
#include "tut_entropy_2_matrix_Renyi.c"
  assert(false); // FIXME: impelmtn for [below]


  /*
#include "tut_entropy_3_file_Dirichlet_Jeffreys.c"
#include "tut_entropy_4_matrix_frequency_Gini.c"
#include "tut_entropy_5_file_Dirichlet_Jeffreys.c"
  */
#undef __M__calledInsideFunction
}

static void apply__() {
// #define __M__calledInsideFunction
// #include ""
// #undef __M__calledInsideFunction
}

#define __M__calledInsideFunction 1
#include "tut_measure_dbScan_auxFunctions.c" //! where latter deinfes the "tut_measure_dbScan_auxFunctions(..)" funciton (oekseth, 06. apr. 2017).

#include "tut_sem_cmpAlg_reachComputation.c"

#include "tut__altLAng_entropy.c"
#define __M__calledInsideFunction 1
#include "tut_entropy_x_time.c"
#include "tut_entropy_x_log.c"
#include "tut_entropy_3_rankAcrossFiles.c"
#include "tut_entropy_4_imageCmp.c"

#include "tut_ccm_15_useCase_singleMatrix_differentPatterns.c"

#include "tut_clust_mcl.c"
#include "kd_tree.h" //! ie, the simplifed API for hpLysis
#include "tut_kd_2_performanceEvaluate.c"
#include "tut_list_3_heap_uint.c"
#include "tut_list_4_heap_keyValue.c"
#include "tut_kd_0_cluster.c"
#include "tut_kd_1_cluster_multiple_simMetrics.c"
#include "tut_norm_1_differentNormStrategies.c"
#include "tut_norm_2_cluster_kd_dbScan.c"
#include "tut_norm_3_cluster_kd_dbScan_80DataSets.c"
#include "kt_sparse_sim.c" //! where the latter is used as an altneritve to the "kt_sim_string", ie, to afcilaitet/supprot comptaution of "Pairwise simalrity-meitrcs (PSMs)"
#include "tut_string_sim_1_singlePair.c"
#include "tut_string_sim_2_stringArray.c"
#include "tut_hash_1_duplicateKeys.c"
#include "tut_hash_2_wordsFromStrings.c"
#include "tut_sim_15_manMany_Euclid__selectBestInsideMetricComp.c"
#include "tut_parse_1_matrix_dense.c"
#include "tut_parse_2_sparseList_headTailScore.c"
#include "tut_parse_3_fromSparseStrings_dbScan.c"
#include "tut_parse_4_sparseList_headTail.c"
#include "tut_parse_5_sparseList_score1Score2.c"
#include "tut_parse_6_sparseList_headScore.c"
#include "tut_parse_7_fromSparseString_split.c"
#include "tut_parse_8_fromSparseString_split_multipleTailsEachRow.c"
#include "tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix.c"
#include "tut_sim_16_manyMany_singleInput_allMetrics_ccmEval.c"
#include "tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs.c"
#include "tut_kd_3_data_simMetric.c"
//! 
//! Wrapper moduels for different ut-examples 
#include "kt_measureStub_time.h" //! eg, wrt. a call to "tut_time_3_ccm_synt.c"

//#include ""

//! The main-function to be called for startint hte class-based correctness-tests.
static int __stub__main(const int array_cnt, char **array) {
  assert(array);
  assert(array_cnt >= 1);
  //correlation-metrics metrics_naive_Spearman 

  // double p, result;   int n;   p = 15.2;  result = frexp (p , &n);     printf ("%f * 2^%d = %f\n", result, n, p);
  //printf("... at %s:%d\n", __FILE__, __LINE__);
  // printf("string=\"%s\", at %s:%d\n",array[1],  __FILE__, __LINE__);
  /*
  if( (0 == strncmp("", array[1], strlen(""))) ) {    
    const char *default_path = "results/";
    const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, 2);
    //! 
    //! The call:
    //!     
    return true;
  } else 
*/
  //printf("at %s:%d\n", __FILE__, __LINE__);
  // if( (0 == strncmp("", array[1], strlen(""))) ) {  
  //   assert(false);
  //   //  } else if( (0 == strncmp("", array[1], strlen(""))) ) {  
  // } else 
    /*if( (0 == strncmp("", array[1], strlen(""))) ) {  
    } else*/ 

    if(kt_measureStub_time(array_cnt, array)) {return true;} //! ie, as lgocis hav ethen been applied in the "kt_measureStub_time.c" module.

    //    printf("cnt-sims=%u, at %s:%d\n", e_kt_correlationFunction_undef, __FILE__, __LINE__);assert(false);
    if( (0 == strncmp("tut_kd_1_matrix_sse", array[1], strlen("tut_kd_1_matrix_sse")) ) ) {
      return tut_kd_1_matrix_sse(array_cnt, array); 
    /*
    if( (0 == strncmp("", array[1], strlen(""))) ) {  
    } else  if( (0 == strncmp("", array[1], strlen(""))) ) {  
    } else  
    */
    } else if( (0 == strncmp("tut_wrapper_image_segment", array[1], strlen("tut_wrapper_image_segment"))) ) {  
      tut_wrapper_image_segment(array_cnt, array);
    } else   if( (0 == strncmp("tut_wrapper_histogram", array[1], strlen("tut_wrapper_histogram"))) ) {  
      tut_wrapper_hp_histogram(array_cnt, array);
    } else   if( (0 == strncmp("tut_wrapper_emd", array[1], strlen("tut_wrapper_emd"))) ) {  
      tut_wrapper_emd(array_cnt, array);
    } else  
    if( (0 == strncmp("tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs", array[1], strlen("tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs"))) ) {  
      tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
    } else if( (0 == strncmp("tut_sim_16_manyMany_singleInput_allMetrics_ccmEval", array[1], strlen("tut_sim_16_manyMany_singleInput_allMetrics_ccmEval"))) ) {  
      tut_sim_16_manyMany_singleInput_allMetrics_ccmEval();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
    } else if( (0 == strncmp("tut_parse_6_sparseList_headScore", array[1], strlen("tut_parse_6_sparseList_headScore"))) ) {  
      tut_parse_6_sparseList_headScore();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
    } else  if( (0 == strncmp("tut_parse_5_sparseList_score1Score2", array[1], strlen("tut_parse_5_sparseList_score1Score2"))) ) {  
      tut_parse_5_sparseList_score1Score2();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
    } else  if( (0 == strncmp("tut_parse_4_sparseList_headTail", array[1], strlen("tut_parse_4_sparseList_headTail"))) ) {  
      tut_parse_4_sparseList_headTail();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;      
    } else  if( (0 == strncmp("tut_parse_3_fromSparseStrings_dbScan", array[1], strlen("tut_parse_3_fromSparseStrings_dbScan"))) ) {  
      tut_parse_3_fromSparseStrings_dbScan();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
    } else  if( (0 == strncmp("tut_parse_8_fromSparseString_split_multipleTailsEachRow", array[1], strlen("tut_parse_8_fromSparseString_split_multipleTailsEachRow"))) ) {  
      tut_parse_8_fromSparseString_split_multipleTailsEachRow();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
    } else  if( (0 == strncmp("tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix", array[1], strlen("tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix"))) ) {  
      tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
      /*
    } else  if( (0 == strncmp("", array[1], strlen(""))) ) {  
      */
    } else  if( (0 == strncmp("tut_parse_7_fromSparseString_split", array[1], strlen("tut_parse_7_fromSparseString_split"))) ) {  
      tut_parse_7_fromSparseString_split();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
    } else  if( (0 == strncmp("tut_parse_2_sparseList_headTailScore", array[1], strlen("tut_parse_2_sparseList_headTailScore"))) ) {  
      tut_parse_2_sparseList_headTailScore();
      printf("at %s:%d\n", __FILE__, __LINE__);
      return true;
    } else  if( (0 == strncmp("tut_parse_1_matrix_dense", array[1], strlen("tut_parse_1_matrix_dense"))) ) {  
      printf("at %s:%d\n", __FILE__, __LINE__);
      tut_parse_1_matrix_dense();
      return true;
    } else if( (0 == strncmp("tut_sim_15_manMany_Euclid__selectBestInsideMetricComp", array[1], strlen("tut_sim_15_manMany_Euclid__selectBestInsideMetricComp"))) ) {  
      tut_sim_15_manMany_Euclid__selectBestInsideMetricComp(); 
      return true;
    } else if( (0 == strncmp("tut_hash_2_wordsFromStrings", array[1], strlen("tut_hash_2_wordsFromStrings"))) ) {  
      tut_hash_2_wordsFromStrings();
    } else if( (0 == strncmp("tut_hash_1_duplicateKeys", array[1], strlen("tut_hash_1_duplicateKeys"))) ) {  
      tut_hash_1_duplicateKeys();
    } else if( (0 == strncmp("tut_string_sim_2_stringArray", array[1], strlen("tut_string_sim_2_stringArray"))) ) {  
      tut_string_sim_2_stringArray();
    } else if( (0 == strncmp("tut_string_sim_1_singlePair", array[1], strlen("tut_string_sim_1_singlePair"))) ) {  
      tut_string_sim_1_singlePair();
    } else if( (0 == strncmp("tut_norm_3_cluster_kd_dbScan_80DataSets", array[1], strlen("tut_norm_3_cluster_kd_dbScan_80DataSets"))) ) {  
      tut_norm_3_cluster_kd_dbScan_80DataSets(array_cnt, array);
      return true;
    } else if( (0 == strncmp("tut_kd_3_data_simMetric", array[1], strlen("tut_kd_3_data_simMetric"))) ) {  
      tut_kd_3_data_simMetric(array_cnt, array);
      return true;
    } else if( (0 == strncmp("tut_norm_2_cluster_kd_dbScan", array[1], strlen("tut_norm_2_cluster_kd_dbScan"))) ) {  
      tut_norm_2_cluster_kd_dbScan(array_cnt, array);
      return true;
    } else if( (0 == strncmp("tut_norm_1_differentNormStrategies", array[1], strlen("tut_norm_1_differentNormStrategies"))) ) {  
      tut_norm_1_differentNormStrategies(array_cnt, array);
      return true;
    } else if( (0 == strncmp("tut_kd_1_cluster_multiple_simMetrics", array[1], strlen("tut_kd_1_cluster_multiple_simMetrics"))) ) {  
    tut_kd_1_cluster_multiple_simMetrics(array_cnt, array);
      return true;
  } else if( (0 == strncmp("tut_kd_0_cluster", array[1], strlen("tut_kd_0_cluster"))) ) {  
    printf("at %s:%d\n", __FILE__, __LINE__);
    return tut_kd_0_cluster(array_cnt, array); //! a tut-file speified in our "tut_kd_0_cluster.c" (oekseth, 06. jul. 2017).
  } else if( (0 == strncmp("tut_list_4_heap_keyValue", array[1], strlen("tut_list_4_heap_keyValue"))) ) {  
    printf("# Computes(tut): at %s:%d\n", __FILE__, __LINE__);
    tut_list_4_heap_keyValue();
    return 1;
  } else if( (0 == strncmp("tut_list_3_heap_uint", array[1], strlen("tut_list_3_heap_uint"))) ) {  
    printf("# Computes(tut): at %s:%d\n", __FILE__, __LINE__);
    tut_list_3_heap_uint();
  } else if( (0 == strncmp("tut_kd_2_performanceEvaluate", array[1], strlen("tut_kd_2_performanceEvaluate"))) ) {  
    tut_kd_2_performanceEvaluate();
    return 1;
  } else if( (0 == strncmp("tut_clust_mcl.c", array[1], strlen("tut_clust_mcl.c"))) ) {  
    tut_clust_mcl();
  } else if( (0 == strncmp("ccm-singleDataSet", array[1], strlen("ccm-singleDataSet"))) ) {  
    tut_ccm_15_useCase_singleMatrix_differentPatterns(array[2]);
  } else if( (0 == strncmp("tut_entropy_3_rankAcrossFiles", array[1], strlen("tut_entropy_3_rankAcrossFiles"))) ) {  
      tut_entropy_3_rankAcrossFiles();
  } else if( (0 == strncmp("entropy-time", array[1], strlen("entropy-time"))) ) {  
    tut_entropy_x_time(); //! defined in our "tut_entropy_x_time.c"
  } else if( (0 == strncmp("entropy", array[1], strlen("entropy"))) ) {  
    apply__entropy();
  } else if( (0 == strncmp("dataParsing_createAndLoad_dataFile", array[1], strlen("dataParsing_createAndLoad_dataFile"))) ) {  //! which is used to text/elvaua teht tiem to creoate a file (oekseht, 06. jul 2017)..
    const char *input_file = "test_tut.csv";
    const char config_file_sep = ',';
    const char config_file_sep_newLine = '\n';
    //! --------
    const bool config_rows__allHaveSameLength = true;
    const bool config_rows__ignoreFirstRow = false;
    const bool config_rows__ingoreFirstColumn = false; //! which si to be set ot true if the first column in each row describes a stirng-idntifer. 
    //! --------
    const uint nrows = 9; 
    for(uint i = 10; i <= 100; i+= 10) { //! where this funciton'emulates' our "testData__buildFreqList.pl"
      const uint ncols = 1000 * i; 
      { //! Generate the file:
	FILE *file = fopen(input_file, "w");
	assert(file);
	for(uint row_1 = 0; row_1 < nrows; row_1++) {
	  for(uint col_1 = 0; col_1 < ncols; col_1++) {
	    //! 
	    const t_float score = (t_float)col_1;
	    fprintf(file, "%f%c", score, config_file_sep);
	  }
	  fputc(config_file_sep_newLine, file);
	}
	fclose(file);
      }
      //!
      //!
      t_float sum = 0;
#define __MiF__parseRow() ({for(uint i = 0; i < cnt_tabs; i++) {sum += obj_current_row.list[i];}})
      //!
      //!
      { //! Load the file: 
	start_time_measurement();
	FILE *file = fopen(input_file, "rb");
	assert(file);
	uint word_buffer_currentPos = 0;
#define  word_buffer_size  128
	char word_buffer[word_buffer_size] = {'\0'}; //! ie, as a number having mroe then 128 digits is aboute the T_FLOAT_MAX limit (oesketh, 06. jul. 2017).
	char c = '\0';
	s_kt_list_1d_float_t obj_current_row = setToEmpty__s_kt_list_1d_float_t();
	uint cnt_tabs = 0;
	uint cnt_rows = 0;
	//!
	//!
	while ((c = getc(file)) != EOF) {
	  if(c == config_file_sep) {
	    const t_float score = (t_float)atof(word_buffer);
	    if(config_rows__allHaveSameLength && (cnt_rows > 0)) {
	      obj_current_row.list[cnt_tabs] = score;
	    } else { //! then we need to provide lgocis to handle 'veroflows' wr.t the row.
	      set_scalar__s_kt_list_1d_float_t(&obj_current_row, cnt_tabs, score); 
	    }
	    cnt_tabs++;
	    // if(cnt_rows > self->ncols) {
	    //   fprintf(stderr, "!!\t Your input-weight-file has more elements than the reserved number of columns, ie, where we expected %u <= %u, an error whcih could arise if your are using the 'tranposed option' in-cosnistnely: we expec thte weights to be deifned for the columsn (and not rows) For questions please contact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_rows, self->ncols, __FUNCTION__, __FILE__, __LINE__);
	    // }
	    //! Reset:
	    for(uint i = 0; i < word_buffer_size; i++) {word_buffer[i] = '\0';}
	    word_buffer_currentPos = 0;
	  } else if(c == config_file_sep_newLine) {
	    //! Apply logics:
	    __MiF__parseRow();
	    cnt_tabs = 0;
	    //! Note: for cases where the first column is a string-id, we use an column-offset of "-1":
	    // if(false) {
	    //   //assert(pos_in_column_0_prev <= 1000);
	    //   char buffer_local[1000]; strncpy(buffer_local, word_buffer, word_buffer_currentPos);
	    //   printf("[%u][%u] = \"%s\"=%f, at %s:%d\n", cnt_rows, cnt_tabs-1, buffer_local, val,  __FILE__, __LINE__);
	    // }
	    //	    assert(val != INFINITY);       assert(val != NAN);
	    //	    self->weight[cnt_rows] = val;      
	    cnt_rows++;
	  } else { //! then inser tthe value in the current word-buffer-chunk:
	    if( (c != '-') ) {
	      word_buffer[word_buffer_currentPos] = c; 
	      word_buffer_currentPos++;
	    }
	  }
	}
	{
	  const char *str_local = "Parsed-large-data-file";
	  const t_float prev_time_inSeconds__ignore = end_time_measurement(/*msg=*/str_local, FLT_MAX);
	  fprintf(stdout, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	  fprintf(stderr, "(%u, %f) ", i, prev_time_inSeconds__ignore); //! ie, an out-rpint-fucntion simliart to our "tut__altLAng_entropy_dataLoad.R"
	}
	fclose(file);
	free__s_kt_list_1d_float_t(&obj_current_row);
      }
#undef __MiF__parseRow
    }
      fprintf(stderr, " #! generated at %s:%d\n",  __FILE__, __LINE__); //! ie, an out-rpint-fucntion simliart to our "tut__altLAng_entropy_dataLoad.R"
  } else if( (0 == strncmp("tut__altLAng_entropy", array[1], strlen("tut__altLAng_entropy"))) ) {   //! then we evlauate time-cost of lgoarithm-comtpatuiosn (oekseth, 06. jul. 2017).
    tut__altLAng_entropy__start_measurement();
  } else if( (0 == strncmp("simple-cmp-logComputeCost_narrow", array[1], strlen("simple-cmp-logComputeCost_narrow"))) ) {  
    //ncols=1000 000

    uint nrows = 1000 * 10; //1000 * 100;
    for(uint i = 0; i < 5; i++) {
      start_time_measurement();
      t_float sum = 0;
      for(uint i = 0; i < nrows; i++) {
	sum += (t_float)(i);
	//      sum += (t_float)(i*i);
      }      
      const t_float time_result = end_time_measurement(/*msg=*/"simple-measurement", T_FLOAT_MAX);    
      fprintf(stdout, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);	
      fprintf(stderr, "list[%u]\t%f\n", nrows, time_result);
      nrows *= 10;
    }
  } else if( (0 == strncmp("simple-cmp-logComputeCost", array[1], strlen("simple-cmp-logComputeCost"))) ) {  
    tut_entropy_x_log(array_cnt, array); //! a function defined in our "tut_entropy_x_log.c"
    return 1;
  } else if( (0 == strncmp("tut_perf_metric_slowVS_fast_rankBased", array[1], strlen("tut_perf_metric_slowVS_fast_rankBased"))) ) {  
    //! 
    //! The call, defined in our "tut_perf_metric_slowVS_fast_rankBased.c":
    tut_perf_metric_slowVS_fast_rankBased__spearman();
    //tut_perf_metric_slowVS_fast_rankBased__kendall();
    return 1;
  } else if( (0 == strncmp("tut_perf_matrixVsSparse", array[1], strlen("tut_perf_matrixVsSparse"))) ) {  
    //! Evaluate the time-cost-benefit of sparse computiaonts of data-sets (oekseth, 06. jun. 2017)
    //! Note: from [below] we we observ htat even for small saprse data-sets the time-cost to comptue a simliarty-metic signclaty out-performs any time-benefitt by usign a comrepssed data-matrix. 
    const uint nrows = 1000 * 10; const uint ncols = 10; //! ie, where $n^2 < 10^9$.
    float prev_time_inSeconds = FLT_MAX;
    { //! A simpel loop to comptue simlarity.:
      s_kt_matrix_t mat_input = initAndReturn__s_kt_matrix(nrows, ncols);
      //! Set values:
      for(uint row_1 = 0; row_1 < nrows; row_1++) {
	for(uint col_1 = 0; col_1 < ncols; col_1++) {
	  mat_input.matrix[row_1][col_1] = 1/rand();
	}
      }
      t_float sum = 0;
      start_time_measurement();
      for(uint row_1 = 0; row_1 < nrows; row_1++) {
	t_float sum_local = 0;
	for(uint row_2 = 0; row_2 < nrows; row_2++) {
	  for(uint col_1 = 0; col_1 < ncols; col_1++) {      
	    sum_local += 
	      (mat_input.matrix[row_1][col_1]  
	       * mat_input.matrix[row_2][col_1]
	       );
	  }
	}
	sum = sum/sum_local; //! which is used to 'ensure' that the data is acually aaccssed in our [below] db-scan-permtuation-evaluation.rotuine.
      }
      {
	const char *str_local = "simple-3d-metric-dense";
	const t_float prev_time_inSeconds__ignore = end_time_measurement(/*msg=*/str_local, prev_time_inSeconds);
	fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      }
      //! De-allcoate:
      free__s_kt_matrix(&mat_input);
    }    
    { //! Evalaute the time-cost of a DB-scan-appraoch, ie, to idnity/capture the stiuiaotn/case where matix-based iteriaotn has a higher time-cost thant he comtpatuion of simarlity-emtric. 
      const uint ncols = nrows; //! ie, as we evalaute the 
      s_kt_matrix_t mat_input = initAndReturn__s_kt_matrix(nrows, ncols);
      //! Set values:
      for(uint row_1 = 0; row_1 < nrows; row_1++) {
	for(uint col_1 = 0; col_1 < ncols; col_1++) {
	  mat_input.matrix[row_1][col_1] = 1/rand();
	}
      }      
      t_float sum = 0;
      start_time_measurement();
      for(uint row_1_ = 0; row_1_ < nrows; row_1_++) {
	//uint row_1 = rand() % (nrows-1);
	uint row_1 = row_1_;
	t_float sum_local = 0;
	//for(uint row_2 = 0; row_2 < nrows; row_2++) {
	for(uint col_1 = 0; col_1 < ncols; col_1++) {      
	  sum_local += 
	    (mat_input.matrix[row_1][col_1] 	     );
	}
	sum = sum/sum_local; //! which is used to 'ensure' that the data is acually aaccssed in our db-scan-permtuation-evaluation.rotuine.
      }
      {
	const char *str_local = "2d-matrix-access";
	//	const t_float prev_time_inSeconds__ignore = 
	end_time_measurement(/*msg=*/str_local, prev_time_inSeconds);
	fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
      }
      //! De-allcoate:
      free__s_kt_matrix(&mat_input);
    }
  } else if( (0 == strncmp("tut_sem_cmpAlg_reachComputation", array[1], strlen("tut_sem_cmpAlg_reachComputation"))) ) {    
    const char *default_path = "results/tut_sem_cmpAlg_reachComputation/";
    const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
    //! 
    //! The call:
    const uint case_id = 2;
    tut_sem_cmpAlg_reachComputation(resultPrefix, case_id);
    //! 
    return true;
  } else   if( (0 == strncmp("mainFunc__tut_simEval_1_syntheticData_extremeCases_caseX", array[1], strlen("mainFunc__tut_simEval_1_syntheticData_extremeCases_caseX"))) ) {    
    /**
       @idea keep the clsuter-aprtiosn fixed whiel gruantlayy change/mofiy the matrix ... each column represents a += 2 cluster-count-seperaiton from the ideal-cluster-count ... in each row we grautally increase the sepraiton between min-max-score-diff ... in tronstrast to ourl \cV{mainFunc__tut_simEval_1_syntheticData_extremeCases} we modify the matrix in the 'outer step' (ie, instead of moidying the matrix in the innermost-iteraiotn-loop-step) ... 
       % FIXME[scope]: consider using [”elow] as the main-topic of our ccm-meta-artilce ... 
       ... the results of this evlauation is expected to provdie/give novel isnight wrt. .... the sensitty of CCMs to idnetify in-accurate clsuter-membeships .... eg, wrt. the use of CCMs in combaiotn with HCa to predict accurate clstuers .... ie, for which the latter resutls provides/gives (indcatiosn of) an error-prediocnt-radius .... 
       
     **/
    e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval_t typeOf_ccmTest = e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedClusterPartition_eachRow_increaseWeithSplit;
    {
      const uint case_id = 1;
      printf("----------------\n\n\n## compute for case=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
      const char *default_path = "results/cpy_mainFunc__tut_simEval_1_clusterCount_c1/";
      //"results/mainFunc__tut_simEval_1_syntheticData_extremeCases/"; //! ie, the default path which we use
      const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
      //! Apply logics:
      mainFunc__tut_simEval_1_syntheticData_syntheticFunctions__subCase__CCM__synteticInput__effects(resultPrefix, case_id, typeOf_ccmTest);
    }
    {
      const uint case_id = 2;
      printf("----------------\n\n\n## compute for case=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
      const char *default_path = "results/cpy_mainFunc__tut_simEval_1_clusterCount_c2/";
      //"results/mainFunc__tut_simEval_1_syntheticData_extremeCases/"; //! ie, the default path which we use
      const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
      //! Apply logics:
      mainFunc__tut_simEval_1_syntheticData_syntheticFunctions__subCase__CCM__synteticInput__effects(resultPrefix, case_id, typeOf_ccmTest);
    }
    {
      const uint case_id = 3;
      printf("----------------\n\n\n## compute for case=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
      const char *default_path = "results/cpy_mainFunc__tut_simEval_1_clusterCount_c3/";
      //"results/mainFunc__tut_simEval_1_syntheticData_extremeCases/"; //! ie, the default path which we use
      const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
      //! Apply logics:
      mainFunc__tut_simEval_1_syntheticData_syntheticFunctions__subCase__CCM__synteticInput__effects(resultPrefix, case_id, typeOf_ccmTest);
    }
    //! 
    return true;
  } else if( (0 == strncmp("mainFunc__tut_simEval_1_syntheticData_extremeCases", array[1], strlen("mainFunc__tut_simEval_1_syntheticData_extremeCases"))) ) {    
    e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval_t typeOf_ccmTest = e_eval__CCM__hardCodedCases__constructMatrix_t__typeOf_ccmEval__fixedMatrix__splitClusterSetDataSet_eachRow_increaseRowSize;
    {
      const uint case_id = 1;
      printf("----------------\n\n\n## compute for case=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
      const char *default_path = "results/cpy_mainFunc__tut_simEval_1_clusterSizeIncrease_c2/";
      //"results/mainFunc__tut_simEval_1_syntheticData_extremeCases/"; //! ie, the default path which we use
      const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
      //! Apply logics:
      mainFunc__tut_simEval_1_syntheticData_syntheticFunctions__subCase__CCM__synteticInput__effects(resultPrefix, case_id, typeOf_ccmTest);
    }
    {
      const uint case_id = 2;
      printf("----------------\n\n\n## compute for case=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
      const char *default_path = "results/cpy_mainFunc__tut_simEval_1_clusterSizeIncrease_c2/";
      //"results/mainFunc__tut_simEval_1_syntheticData_extremeCases/"; //! ie, the default path which we use
      const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
      //! Apply logics:
      mainFunc__tut_simEval_1_syntheticData_syntheticFunctions__subCase__CCM__synteticInput__effects(resultPrefix, case_id, typeOf_ccmTest);
    }
    {
      const uint case_id = 3;
      printf("----------------\n\n\n## compute for case=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
      const char *default_path = "results/cpy_mainFunc__tut_simEval_1_clusterSizeIncrease_c3/";
      //"results/mainFunc__tut_simEval_1_syntheticData_extremeCases/"; //! ie, the default path which we use
      const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
      //! Apply logics:
      mainFunc__tut_simEval_1_syntheticData_syntheticFunctions__subCase__CCM__synteticInput__effects(resultPrefix, case_id, typeOf_ccmTest);
    }
    return true;
  } else if( (0 == strncmp("tut_simEval_1_syntheticData_syntheticFunctions", array[1], 
			   strlen("tut_simEval_1_syntheticData_syntheticFunctions"))) ) {    
    const char *default_path = "results/tut_simEval_1_syntheticData_syntheticFunctions/"; //! ie, the default path which we use
    const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
    //! Apply logics:
    tut_simEval_1_syntheticData_syntheticFunctions(resultPrefix);
    return true;
  } else if( (0 == strncmp("dbScan", array[1], strlen("dbScan"))) ) {    
    //! Apply logics:
    return tut_measure_dbScan_auxFunctions(array_cnt, array);
  } else if( (0 == strncmp("tut_ccm_13_categorizeData", array[1], strlen("tut_ccm_13_categorizeData"))) ) {    
    //const char *resultPrefix = __MF__getResultPrefixFromArg("tut_sparseSim", /*arg=*/2);
    // TODO[verif]: validte that [below] systme-call may Not be misused, eg, by 'verorwiritng' the "default_path" memory-psotion.
    const char *default_path = "results/tut_ccm_13_categorizeData/"; //! ie, the default path which we use
    {
      allocOnStack__char__sprintf(2000, str_cmd, "rm -f %s", default_path); //! ie, creat eht direcotry (if directy does No0t relayd exists).
      MF__system(str_cmd);
    }    
    const char *resultPrefix = __MF__getResultPrefixFromArg(default_path, /*arg=*/2);
    __local__tut_ccm_13_categorizeData(resultPrefix); //! where latter is deifne din our "tut_ccm_13_categorizeData.c".
    //apply__kt_sparseSim(resultPrefix);
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("kt_sparseSim", array[1], strlen("kt_sparseSim"))) ) {    
    const char *resultPrefix = __MF__getResultPrefixFromArg("tut_sparseSim", /*arg=*/2);
    apply__kt_sparseSim(resultPrefix);
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("tut_dynamicK", array[1], strlen("tut_dynamicK"))) ) {    
    const char *resultPrefix = __MF__getResultPrefixFromArg("tut_dynamicK", /*arg=*/2);
    fprintf(stderr, "resultPrefix=\"%s\", at %s:%d\n", resultPrefix, __FILE__, __LINE__);
    // apply__kt_sparseSim(resultPrefix);
    apply__tut_dynamicK(resultPrefix);
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("hp_api_fileInputTight", array[1], strlen("hp_api_fileInputTight"))) ) {    

    { //! then fist test a simple case swrt. 'memory-alllcaotion-and-free' in our "kt_clusterAlg_hca_resultObject.c"
      s_kt_clusterAlg_hca_resultObject_t dummy = setToEmptyAndreturn__s_kt_clusterAlg_hca_resultObject_t();
      free__s_kt_clusterAlg_hca_resultObject_t(&dummy);
      setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&dummy, 0);
      free__s_kt_clusterAlg_hca_resultObject_t(&dummy);
      setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&dummy, 10);
      free__s_kt_clusterAlg_hca_resultObject_t(&dummy);
      // assert(false);
    }

    hp_api_fileInputTight();
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  // } else if( (0 == strncmp("", array[1], strlen(""))) ) {    
  //   assert(false); // TODO:: add seomthing
  //   //! 
  //   //! ---------------------------------------------------------------------
  //   //cnt_Tests_evalauted++;
  //   return true;
  } else if( (0 == strncmp("kt_list", array[1], strlen("kt_list"))) ) {    
    apply__kt_1d_list();
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("kt_matrix_cmpCluster",array[1]) ) ) { 
    // printf("at %s:%d\n", __FILE__, __LINE__);
    measure_kt_matrix_cmpCluster(array_cnt, array);
    return true;
  } else if( (0 == strncmp("kt_matrix", array[1], strlen("kt_matrix"))) ) {    
    apply__matrix();
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("db", array[1], strlen("db"))) ) {    
    const char *resultPrefix = __MF__getResultPrefixFromArg("tut_ccm", /*arg=*/2);
    //! Apply lgoics
    measure_db__main(array_cnt, array);
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("tut_inputFile_9__algCmp__syntAndRealLife", array[1], strlen("tut_inputFile_9__algCmp__syntAndRealLife"))) ) {    
    apply__tut_inputFile_9__algCmp__syntAndRealLife();
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("tut_random_difference_1", array[1], strlen("tut_random_difference_1"))) ) {    
    apply__tut_random_difference_1();
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("tut_hypothesis_whyResultDiffers_1_vincent", array[1], strlen("tut_hypothesis_whyResultDiffers_1_vincent"))) ) {    
    const char *resultPrefix = __MF__getResultPrefixFromArg("tut_whyResultsDiffers", /*arg=*/2);
    //!
    //! Then we apply extensive tests covering approx 200++ different data-sets:
    apply__tut_hypothesis_whyResultDiffers_5_realMine(resultPrefix); //! where 'this call' results in an evlauation of aprpxo. 80+ real-life data-set-files.
    fprintf(stderr, "!!\t remove this line, at %s:%d\n", __FILE__, __LINE__); return true; //! ie, to abort during otpmzied compiatlion.    
  } else if( (0 == strncmp("tut_hypothesis_whyResultDiffers_1_all", array[1], strlen("tut_hypothesis_whyResultDiffers_1_all"))) ) {    
    const char *resultPrefix = __MF__getResultPrefixFromArg("tut_whyResultsDiffers", /*arg=*/2);
    //!
    //! Then we apply extensive tests covering approx 200++ different data-sets:
    apply__tut_hypothesis_whyResultDiffers_2_realMine(resultPrefix);
    fprintf(stderr, "!!\t remove this line, at %s:%d\n", __FILE__, __LINE__); return true; //! ie, to abort during otpmzied compiatlion.
    
    apply__tut_hypothesis_whyResultDiffers_5_realMine(resultPrefix); //! where 'this call' results in an evlauation of aprpxo. 80+ real-life data-set-files.
    apply__tut_hypothesis_whyResultDiffers_8_FCPS(resultPrefix);
    assert(false); // FIXME: remove.
    assert(false); // FIXME: remove.

    assert(false); // FIXME: update our init-procedure ... in/from our "kt_graphAlgorithms_main.cxx" ... use sim-metrics found/listed in our "tut_simEval_1_syntheticData_syntheticFunctions.c"
    assert(false); // FIXME["hp_evalHypothesis_algOnData.c"::code]: ... clustering new iteration .... comptue []
    assert(false); // FIXME["hp_evalHypothesis_algOnData.c"::code]: ... write out the sim-metics ... usign oru new-written data-strucutre for data-epxort and evalautions
    assert(false); // FIXME[result-eval]: ... manually invetigate/inspect the reuslts .... scaffold/itnrodpsect upon different web-visualizaitons. 
    assert(false); // FIXME[result-eval]: ... update our "tut_simEval_1_syntheticData_syntheticFunctions.c" wrt. latter .... 
    assert(false); // FIXME: remvoe when [ªove] emmory-isuses are reoslved.!
    apply__tut_hypothesis_whyResultDiffers_1_dataSyn(resultPrefix);
    apply__tut_hypothesis_whyResultDiffers_3_synt_mathFunctions__noise(resultPrefix);
    apply__tut_hypothesis_whyResultDiffers_4_realMine_large(resultPrefix);
    apply__tut_hypothesis_whyResultDiffers_6_Vilje(resultPrefix);
    apply__tut_hypothesis_whyResultDiffers_7_differentDimensions_synt(resultPrefix);
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("tut_sim", array[1], strlen("tut_sim"))) ) {    
    const char *resultPrefix = __MF__getResultPrefixFromArg("tut_sim", /*arg=*/2);
    apply__tut_sim(resultPrefix);
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else if( (0 == strncmp("tut_ccm", array[1], strlen("tut_ccm"))) ) {    
    const char *resultPrefix = __MF__getResultPrefixFromArg("tut_ccm", /*arg=*/2);
    //! Apply lgoics
    apply__tut_ccm(resultPrefix);
    //! 
    //! ---------------------------------------------------------------------
    //cnt_Tests_evalauted++;
    return true;
  } else  if( (array_cnt >= 2) && (0 == strncmp("small_basicCases__language",array[1], strlen("small_basicCases__language")) ) ) { //! then we elvauate a small subset, a subset designed to 'reflect' our "tut__altLAng_simMetric.java" java-measurement-appraoch. 
    tut_sse_matrixMul_cmpApproaches(); //! ie, the call
  } else  if( (array_cnt >= 2) && (0 == strncmp("basicCases__language",array[1], strlen("basicCases__language")) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"    
    if(false) { //! then we use an udpated prodcued where we 'traverse' two lists, and then costnrcut one 'unfied' result-amtrix:
      const char *str_filePath = "results/basicCases__language__synt_simMetricEval";
      const uint listOf_rows_size = 2; const uint listOf_cols_size = 10;
      const uint b = 128;
      uint listOf_rows[listOf_rows_size]
	= {b*50*1, b*50*10}
	;
      for(uint i = 1; i <= listOf_rows_size; i++) {listOf_rows[i-1] = b*50*i;}
      uint listOf_cols[listOf_cols_size];
      for(uint i = 1; i <= listOf_cols_size; i++) {listOf_cols[i-1] = b*50*i;}
      //!
      //! Apply the test:
      const bool is_ok = measureStub__case__basicCases__language__setOf(str_filePath, listOf_rows, listOf_rows_size, listOf_cols, listOf_cols_size, /*useColVal_alsoForRow=*/false);
      assert(is_ok);
    } else {      
      measureStub__case__basicCases__language(/*nrows=*/2560, /*ncols=*/256);
      //measureStub__case__basicCases__language(/*nrows=*/10*128, /*ncols=*/2*128);
      if(true) {
	//! Note: in [below] we comapre wrth the ruslts from \cite{cmp}="http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9"
	measureStub__case__basicCases__language(/*nrows=*/50*128, /*ncols=*/4*128); //! ie, [54675, 559]=MULTMYEL in [above] \cite{cmp}
	measureStub__case__basicCases__language(/*nrows=*/100*128, /*ncols=*/4*128); //! ie, [54675, 559]=MULTMYEL in [above] \cite{cmp}
	measureStub__case__basicCases__language(/*nrows=*/140*128, /*ncols=*/4*128); //! ie, TCGA in [above] \cite{cmp}
	measureStub__case__basicCases__language(/*nrows=*/200*128, /*ncols=*/4*128); //! ie, TCGA in [above] \cite{cmp}
	measureStub__case__basicCases__language(/*nrows=*/428*128, /*ncols=*/4*128); //! ie, [54675, 559]=MULTMYEL in [above] \cite{cmp}
       }
      //const uint nrows = 30*128; 	const uint ncols = 2*128;
      measureStub__case__basicCases__language(/*nrows=*/10*128, /*ncols=*/2*128);
      measureStub__case__basicCases__language(/*nrows=*/10*128, /*ncols=*/6*128);
      //! --
      measureStub__case__basicCases__language(/*nrows=*/20*128, /*ncols=*/2*128); 
      measureStub__case__basicCases__language(/*nrows=*/20*128, /*ncols=*/6*128);
      //! --
      measureStub__case__basicCases__language(/*nrows=*/30*128, /*ncols=*/2*128);
      measureStub__case__basicCases__language(/*nrows=*/30*128, /*ncols=*/6*128);
      //! --
      measureStub__case__basicCases__language(/*nrows=*/40*128, /*ncols=*/2*128);
      measureStub__case__basicCases__language(/*nrows=*/40*128, /*ncols=*/6*128);
      //! --
    }
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("kt_set_sparse",array[1]) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"
    measure_kt_set_sparse__main(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("correlation-metrics",array[1]) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"
    measure_correlation__main(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("kt_forest_findDisjoint",array[1]) ) ) { 
    measure_kt_forest_findDisjoint__main(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("kt_para_3dSchedule",array[1]) ) ) { 
    measure_kt_para_3dSchedule__main(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("kt_matrix_filter",array[1]) ) ) { 
    measure_kt_matrix_filter__main(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("mask-basics",array[1]) ) ) { 
    measure_mask(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("measure_terminal",array[1]) ) ) { 
    measure_terminal_main(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("ex_kMeans",array[1]) ) ) { 
    ex_kMeans_main(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("sort",array[1]) ) ) { 
    measure_sort__main(array_cnt, array);
    return true;
  } else if( (array_cnt >= 2) && (0 == strcmp("-t_studentT",array[1]) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"
    //printf("-----, at %s:%d\n", __FILE__, __LINE__);
    test_measure_distribution_studentT(); //array_cnt, array); //! then we measure for SOM. 
    return true;
  }
  // if( (array_cnt >= 2) && (0 == strcmp("-t_centrality",array[1]) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"
  //   //printf("-----, at %s:%d\n", __FILE__, __LINE__);
  //   test_measure_kt_centrality(); //array_cnt, array); //! then we measure for SOM. 
  //   return true;
  // }

  if( (array_cnt >= 2) && (0 == strcmp("measure_kt_clusterAlg_fixed",array[1]) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"
    //printf("-----, at %s:%d\n", __FILE__, __LINE__);
    main__kt_clusterAlg_fixed(array_cnt, array); //array_cnt, array); //! then we measure for k-means. 
    return true;
  }
  if( (array_cnt >= 2) && (0 == strcmp("measure_hca",array[1]) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"
    //printf("-----, at %s:%d\n", __FILE__, __LINE__);
    main__kt_clusterAlg_hca(array_cnt, array); //array_cnt, array); //! then we measure for SOM. 
    return true;
  }

  if( (array_cnt >= 2) && (0 == strcmp("syntetic_codeStyle",array[1]) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"
    //printf("-----, at %s:%d\n", __FILE__, __LINE__);
    main__measure_codeStyle_functionRef(array_cnt, array); //! then we measure for SOM. 
    return true;
  }

  if( (array_cnt >= 2) && (0 == strcmp("SSE_baseConcepts",array[1]) ) ) {
    //! Then we test a set of sysntentic example
    measure_intri_main(array_cnt, array); //! ie, our "measure_intri.cxx"
    return true;
  }

  if( (array_cnt == 2) && (0 == strcmp("-s",array[1]) ) ) {
    //! Then we test a set of sysntentic example
    // measure_intri_main(); //! ie, our "measure_intri.cxx"
    // assert(false); // FIXME: remvoe.
    graphAlgorithms_timeMeasurementsExternalTools_syntetic::main();
    return true;
  }



  if( (array_cnt >= 2) && (0 == strcmp("measure_som",array[1]) ) ) {
    measure_som_main(array_cnt, array); //! then we measure for SOM. 
    return true;
  }
  if( (array_cnt >= 2) && (0 == strcmp("nipals",array[1]) ) ) {
    main__externalCXX_nipals(array_cnt, array);
    return true;
  }
  if( (array_cnt >= 2) && (0 == strcmp("-c_memTilingFixedRow",array[1]) ) ) {
    measure_syntetic_tiling_fixedRowAppliedOn2dMatrix_main(array_cnt, array); //! then we measure for SOM. 
    return true;
  }

  if( (array_cnt == 2) && (0 == strcmp("-c",array[1]) ) ) {
    //! Then we test a set of sysntentic examples
    graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary::main();
    return true;
  }


  if( (array_cnt == 2) && (0 == strcmp("-a",array[1]) ) ) {
    //! Then we generate/call/'perform' the 'asserts':  
    const bool print_info = true;

    assert_intri_main();
    //assert(false); // FIXME: remvoe


    //assert_s_dense_closestPair(); //! found in our "s_dense_closestPair.cxx" (oekseth, 06. jun. 2016).
    //assert(false); // FIXME: remvoe

    // graphAlgorithms_kCluster::assert_class(print_info);
    //graphAlgorithms_distance::assert_class(print_info);
    // assert_class__kt__clusterAlg_fixed(print_info);
    //main__kt_clusterAlg_fixed();
    return true;
  }
  

  fprintf(stderr, "!!\t Seems like none of the input-parameters marched for case-id=\"%s\", ie, please validate your calls and/or give the developer an heads-up at [oekseth@gmail.com]. Observat at [%s]:%s:%d\n", array[1], __FUNCTION__, __FILE__, __LINE__); //! (oekseth, 06. okt. 2016).

  /**


   **/

  // FIXME: remember meeting with rune-stære at: ITV-104 at 1400 on Firday

  /** Upcoming challenges, ie, motivation:
      6-11. juni: implemented different permutaitons of "transitive closures" funcitons described in "kt_graphAlgorithms/transitive_closures/", and managed to get intal time-benchmarks for the "Fact++" library. Result: intal time-estimates of our imrpoved algorithm for coputing transitvie clsoures (when compared to polular software-libraries).
      13-18. juni: completed intial time-evlauation of "FAct++" and .... if time-difference 'is signficant enough' then written two new aritlces wrt. our new semantic library
      20-25. juni: implemented new sparse and dense distance-metrics (eg, to be used from our "dbParse/stat_pubmed/stat_pubmed.pl" Pubmed-evaluation-script)
      27-02. july: first iteration of a new "biopax-merge-procedure", "data-anoramlization-database" and "web-accessible database of information": evalaute the non-matches with 'other name-merging databases' and then apply/use our updated "cluster.c" to asses exactness of the new-inferred 'similar identiifers' (eg, wrt. similairy in ontology-annotations when compared to 'before the name-merging').
      04-10. july: updated our sematnci-PLOS-artilce with examples of new-identifed inferences (from a combined applicaiton of semantics and clsuter-analsysis).
      12-18. july: completed above, and started work on ... 
   **/
  /** Upcoming routine-work, ie, time-cosnuming though of relative relaxing complexity
      6-11. juni: correctness-tests of cluster.c, and intal perofmrance-esitmates (of the latter);
      13-18. juni: compled the "mine.c" library, using JCs new-written macros.
      20-25. juni: completed perofmrance-tests of our "cluster.c", and written one HPC aritlce and one life-science article wrt. in-depth description of optimizaiton-strageis in our software.
      27-02. july: re-implemented an optimized version of our semantic library
      04-10. july: written an extension to our "cluster.c" where ... support both sparse and dense distance/proximity measures ... and the 'upload' of data-files ... and then updated our "dbParse/stat_pubmed/stat_pubmed.pl" wrt. the latter ... where use-cases are ...
      12-18. july: written a new "MESH" parser, constructed a new MESH-ontology for the latter ... and used the latter in our "dbParse/stat_pubmed/stat_pubmed.pl" wrt. a comparison of "MESH" and "citations".
   **/

  /** Time-line: 
      -----------------------------------------------------------------------------------------------
      ok: 2-5. mai: clsteruing-alg and centrlaity-basics working. Result: time costs/benchmarks/evlauations of clustering-algorithms and distance method-inferences ... 'whiteboard-scaffold' of a new article.
      9-14. mai: completed first version/iteration of both the "mine.c" software and the "mine" artilce.
      16-19. mai: completed first version/iteration of both the "cluster.c" software and the "cluster" artilce.
      23-25. mai: complete first version/iteraiton of the 'artilce-type to be scaffolded by Jan-Christian'.
      30. mai: completed first version of th 'biopax-expansion-artilce'.
      7.juni: completed first verison of our two recahbility-and-software-articles ...??...  and identifed three different 'template-articles' wrt. our new "recahbility-algorithm" <--  discuss with RuneSætre the different 'forms we are to publish in, eg, article(1) "a new web-based software for merging datasets" (focusing on our large number of software-tools to merge datasaets through our SPORA); article(2) "a new web-based tool for entailment and visualization of data" ; article(3) "a new algorithm for effective computation of rechability" <-- in contrast to other software our tool supports entailmetns of both "non-semantic" relationships/data (eg, inference between BioPax entites and protein-prtoein interactions) and the inclusioin of provenance <-- verifuy this assumpiton of 'related work', eg, in contrast to "SnoRocket" and "FacTC++" <-- in this context also compare with the new "CyNetworkBMA" software-tool, ie, ensure that our web-itnerface outperforms the latter.
     ??: pubmed-dataset parsed and entailed. Result: 'intial' categorization and clusters of keywords, eg, wrt. "relateness between different univeristy-departments".
     ??. mai: updated the Almås artilce with new results ... and 'completed' a 'new' library for network-analsysis ... 'whiteboard-scaffold' of two new articles, ie, one 'meothd-result-aritle' and one 'new-software-aritlce'  <-- in order to publish our 'network-library-tool' as a 'stand-alone' library, we update our web-interface with ....??... ... eg, for the use-cases of ....??..  <-- consider in this context to read the 'evalaution of network-analsysis-tools' ["https://www.researchgate.net/profile/Nadeem_Akhtar11/publication/269305257_Social_Network_Analysis_Tools/links/55e7e87b08ae65b638996352.pdf"]
     ??. mai:  Result: clusters of pubmed-artilces ... indications of feature-space ... identify contraints/parameter which may be used to icnrease speciify of litterature-search ... and evalaute signficance/descriptivness of "disjoint symmetric networks of researhc-articles".   <-- consider (among others) to write artilces fo the "Journal of Biomedical Semantics" journal
     ??. juni: Result: a first version of our 'iterative algorithm for discovery of artilces reflecting a user-defined search-pattern'.
     14. juni: Result: a 'speciilised' database to perofmr simple/basis/'narrowd' user-queries: used to improve our own understanding of current research <-- conside rto write an artilce which foucs on perofmrance-evalaution of different data-access-schemes in life-science research .... <--  fixme[jc]: may you dietnife similar articles as "https://arxiv.org/pdf/1205.2889.pdf" (which seems like a rather poor aritlce w/title="A Comparative Study on the Performance of the Top DBMS Systems") 
     21. juni: Result: identified convergence-criterias wrt. what may be disocvered of new applicaitons through litterature-database ... and web-interface updated wrt. new interactive applications.
     28. juni. Result: .... identifed and optimized two new entwork-analsysis-software (ie, two enw aritcles) .... and a new article  which describe implication of basic optimization-technieuqes in life-science software, both wrt. optimization-strageis (eg, to recuce the time of computing Euclidian distance in a transposed matrix) and different datasets 'which may be evaluated' (ie, through the decreased time-complexity of teh operaitons) ... and in this context consider to write a 'parallel' artilce whcih 'reviews strategies for apåplicaiton of network-analsysis-software in life-science VS how the software are implemented' <--- in this context identify artilces describing/comparing/suggesting algorithms where 'only difference is found wrt. the constats/assumptiosn/heuristics assicated to the algorithms', eg, where difference between algorithms/softwares are minor/insignficant.
      -----------------------------------------------------------------------------------------------
      Expects the following artilces to be the result, ie, in the beginning of June:
      ------------------------------------------------
      1. improved software: "C cluster library". <-- use "http://bioinformatics.oxfordjournals.org/content/20/9/1453.full.pdf+html" as template.
      2. improved software: "mine" correlation-library.  <-- use "http://bioinformatics.oxfordjournals.org/content/29/3/407.full" as a template ...    ... we describe an otpmized softeare of the ...??..    ... in this work we have exmaplfied how algorithms may be improved without cost in accuracy through: (1) memory-allocation: fwer number of memory-allocations through an re-use of allcoated memory isntead of allocating memory muliple times; (2) cahce-sensitive memory-policies: improved memory-utilzaiton through data-access similar to the size of memory-caches;    (3) parallisation: how established seiral algorithms may be partioned into serial 'chunks'
      ---
      5. new software: "a new web-application for comparison/evaluation/analysis of networks";  <-- compare with ... "https://nar.oxfordjournals.org/content/42/W1/W167.full" ... and "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1013-x" <--- cosndier to combine the visualizeiton-artilces 'which we have already read' with an 'new set of aritlce-readings' using "http://scholar.google.no/scholar?as_ylo=2012&q=javascript+library+centrality+network-analysis&hl=no&as_sdt=0,5" <-- first 'figure out' the uniquness of our approach: there are considerably better software-tools for visualization of centrlaities and distributions in networks, though the wekaness of all of the approaches is that they....??... 
      ---
      6. new software: a new software for rechability-comptuation; <-- compare with ONTO-PERL, Protege, "FacT++"
      7. new method: "Linear-time algorithm for semantic entailments" a new algorithm for rechability-computation; <-- compare with exisintg/other algorithms, eg, used in [above].
      ---
      8. new database: "a new biological database of pathway interactions and annotations" <--- among others read about tools and method such as "narada/" ["http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.143.6470&rep=rep1&type=pdf"]  and "http://compbio.case.edu/koyuturk/publications/networkbiology_wires09.pdf"
      9. new software: "a new web-based tool for graphical SPAR-QL user-searches" <-- need to first complete our current updates in our entailment-procedure .... related work: 'generating code from holictic query evaluation' ["http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.371.2055&rep=rep1&type=pdf"] .. and "BioMart -- biological queries made easy" ["http://bmcgenomics.biomedcentral.com/articles/10.1186/1471-2164-10-22"] <--- compare with a 'network-analsyer' tool ("http://www.networkanalyst.ca/faces/Secure/network/NetworkBuilder.xhtml") <-- compare (among others) with teh web-based search-tool "SmarktDrugSearch" ("http://sing.ei.uvigo.es/sds/") ... and compare with web-based search-tools such as Swoogle, Semantic Web Search Engine (SWSE) , WikiDB , Sindice , Watson , Falcons , and CORESE  ... eg, as listed at "http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3989774/"
      10. new method:
      ---
      ??. new software: "a new web-based software which enables network-analysis of datasets stored in different data-formats"; <-- compare our local javaScript "lib_centrality.js" library and our C++ "signaure" algorithm towards other ranking-methods ... and describe the 'impact' wrt. datbases without use of 'randing' iont heir searches, eg, as seen in the "SSB" approach. <-- why is this of interet to users? <-- consider instead to publish a new Perl package for unificaiton of data-formats, a Perl-package which in contrast to ...??...  <-- consider to extend this (or write a seperate article) which provide a web-interface for the "c cluster" lbirary ... where an example of 'gluing muliple dat-soruces' is to upload data-files and for each data-file a user may 'generate'/specify a set of filters, eg, wrt. SBML where users may analyse ...??.. 
      --
      11. new method:   "A new semantic search-engine: real-time data-search" <-- describes how we have optimized our query-scheme to reduce the time-cost of SPAR-QL searches. <-- cosnider to bombine  "'Vertical optimization of data-heavy searches'" from our "slides_concretePresentation_articles.pm" with the description of our "query scheme" in our "slides_tmp.html"
      12. new method: (similar to "10. new method", with difference that we targett our work towards a different journal)
      ---- July ---- 
      13. new database: "the PubMed database updated with new keywords to capture underlyign similarities in litterature";
      ??. new software: a software which provide a search-interface to the software-tools listed at code-repostiories (eg, "http://bioconductor.org/packages/release/BiocViews.html#___Software" and ..??...) <-- use this approach to also idnetify new/other software which we may imrpove the quality of ... eg, using the seleciton-criteiras of: (a) "popular software"; (b) "written by un-expereinced programmers"; (c) ...??...  <-- consieer to manually investigate the soruce-vode of each software  (first writing a script to (a) idntiy keywrods and software-dpenedencies assicated to eahc software, (b) download the software, (c) descirbe the 'conceptual' complexity (eg, 'number of files' and 'length of each file')) <-- through aneva latuion wrt. the software-dpeendnceis (eg, "numpy") try to identify which software-tools which 're-impolemetbns' code-tasks which may be better suited/supported by otpmized tools/approaches ...  <-- compare with software-search-tools such as "http://omictools.com/" ["http://database.oxfordjournals.org/content/2014/bau069.long"], a work which used manual curation/classification using a "three-level classification to sort analytical tools according to their technology, application and their analytical steps". To our surpise the "http://omictools.com/" web-based software-tool does not make use of ltiterature-cruation nor incorporates litterature-search in their web-site. <-- consider writing a script to 'get' the web-pages and classifciaotns for each tool at "http://omictools.com/" (eg, wrt. cases such as '<a href="/bioinformatics-resource-inventory-tool" class="tool-section-grid-item">' at "http://omictools.com/text-mining-category" ... and at "http://omictools.com/acromine-tool") ... and others the "http://omictools.com/" does not provuide insight into 'which software-tools which prelude other software-tools', ie, givne the 'short life-time' of many sfotware-approaches, a 'short life-time' which is due to ...??...
      14. new software: "a new software for litterature-search: how algorithms for network-analsysis in combination may reveal new insights into litterature"; 
      ??. new method: approaches/algorithms/methods/strategies to idenfy new/ptuative knowledge/artilce-lreaitonships in litterature .... eg, "Idenitfy all sofware-artilces which are central in research and are not yet optimzed wrt. performance, for which we evluate/sort for 'children' (wrt. use-case-applications) and 'arpents' (wrt. annotations/interpretations of the software)" <-- for the latter use-case try to calssify/descirbe software based on the 'users' of a given software-tool ... eg, to ansder (in a differen reseahrc-aritlce) the quesiton of "to what extendt may software be categorized by the broadness of its usage VS the softwares inital description?"
      ---
      20. new software: write/build/construct a 'test-bed' to simualrte tehe xeuction-time cost of different implementaiton-appraoches. As 'code-basis' we provide web-access to  "graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary.cxx" and "graphAlgorithms_timeMeasurementsExternalTool_mine.cxx". An example-use-case is (for users/researchers) to ....??... 
      ---

      <-- when [above] artilces are completed, then update wikipedia-pages with references/ciations to our work.
   **/

  // FIXME[time-benchmark]: test different memory-allocation-rotuines in the "c cluster-re-implemetnaiton", step(1): perofrm 'simple' time-comparison ... (a) describe the memory-access-patterns in each of the funcitons in our "graphAlgorithms_distance.cxx", ie, to hypothese/evaluate wrt. the effects of optimized memory-access-patterns; (b) update and use our "bm_core/list_for_bm.h" ... ; (c) to get an 'indication' of the signficnace in memory-costs ....


  // FIXME[new-algorithm]: ... re-implemnet the 'path-idneitfciaotn' in our JavaScript disjktra-library ("/home/klatremus/poset_src/web/js/alg/lib_algo_sp_dijikstras.js").  <-- complete the code in ou2 Tmp.h" and ... describe some use-cases ... and then mvoe the latter to a new class anemd ....??.... 
// FIXME[new-algorithm]: ... re-implemnet our "all-agaisnt-all-shortest-paths" 'into this' ... 
  //FIXME[new-algorithm]: ... write a new "graphAlgorithms_entropy" to be 'filled' with algorithms used in the "mine.c" code/software
  // FIXME[new-algorithm]: ... complete "graphAlgorithms_buildPaths_fromNetwork.cxx" and "graphAlgorithms_buildPaths_fromNetwork.h"
  // FIXME: update our "list_files_for_libs" with: graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary.cxx graphAlgorithms_timeMeasurementsExternalTool_mine.cxx  graphAlgorithms_timeMeasurements_syntetic.cxx graphAlgorithms_entropy.cxx graphAlgorithms_optimizedDistance.cxx graphAlgorithms_scc.cxx graphAlgorithms_scc_list.cxx graphAlgorithms_centrality_subset.cxx sp_compressed_base.cxx graphAlgorithms_buildPaths_fromNetwork.cxx ... and then move the "sp_*" fiels from "nip" to "kt_graphAlgorithms/" ... and validate that 'this folder' with the "semantic" dependnecies.
  // FIXME[new-algorithm]: ... update our "graphAlgorithms_centrality" with [ªbove] new algorims wrt. SSSP and "SCCs" 
  // FIXME[new-algorithm]: finalize update: (a) move functioanlity from "sp_compressed" to sp_compressed_base"; (b) "move funciotnality from "graphAlgorithms_centrality" to "graphAlgorithms_centrality_subset"; (c) update "graphAlgorithms_centrality" with fucntioaitnliy fro 'partial construction'; (d) add 'basic' funciionality for users to user our 'three categorizations of centlirty' to test/compute new metrics
  

  // FIXME[new-software]: based on feedback from JanChristian complete the update/optimilization of ... "C clustering library"
  // FIXME[new-software]: based on feedback from JanChristian complete the update/optimilization of ... "mine" software-library
  // FIXME[new-software]: based on feedback from JanChristian complete the update/optimilization of ... 

  // FIXME: litterature: ... read/skim the following aritcle ... context="semantics" ... <AbstractText Label="CONCLUSIONS" NlmCategory="CONCLUSIONS">CyNetworkBMA is an easy-to-use tool that makes network inference accessible to non-programmers through seamless integration with Cytoscape. CyNetworkBMA is available on the Cytoscape App Store at http://apps.cytoscape.org/apps/cynetworkbma.</AbstractText>
  // FIXME: litterature: ... read/skim the following aritcle ... context="semantics" ... <AbstractText Label="RESULTS" NlmCategory="RESULTS">We developed owlcpp, a library for storing and searching RDF triples, parsing RDF/XML documents, converting triples into OWL axioms, and reasoning. The library is written in ISO-compliant C++ to facilitate efficiency, portability, and accessibility from other programming languages. Internally, owlcpp uses the Raptor RDF Syntax library for parsing RDF/XML and the FaCT++ library for reasoning. The current version of owlcpp is supported under Linux, OSX, and Windows platforms and provides an API for Python.</AbstractText> <--- in this context get "FaCT++" running on our system ... and update our export-Perl-modules to convert tab-formatted data to OWL 
  // FIXME: litterature: ... read/skim the following aritcle ... context="" ... 
  // FIXME: litterature: ... 


  // FIXME[use-case]: .... how "knowledge of keywords descritpvie for researhc-articles may guide rsearchers in their selection of journals (for publishing)" (eg, inferred through k-means clustering of assicated aritlce-features) <--- try to outline this use-case wrt. 'application'/applicability. 
  // FIXME[use-case]: .... 



// FIXME[new-network]: write a new Perl-script to ... 
// FIXME[new-network]: update our C++ to ... 
// FIXME[new-network]: write a new Perl-script to ... 

// FIXME: update our 'evlauation of distrubitons' wrt. the following task wrt. "identificaiton of Least Publishable Articles" for software-development: (a) 'intersects' the keywords of "software" (or words which are not matched by our kewyrdo-searches in abstracts); (b) where researhc-articles are not assicated to dpeartmenst of copmuter-science; (c) are not assicated to 'specilised' comptuer-approahces (eg, "GPU", "MPI" and "CUDA") .... and try to idneitfy the importance 'of such groups'.


  // FIXME[analyis] .... write a use-case where we use "sp_johnson", "graphAlgorithms_scc", "graphAlgorithms_distribution" and "graphAlgorithms_centrality" to comptue the betweenness-centrliaty. <-- seems simpler to instead 'move' the exitign impelmetnation to 'this'.

// FIXME: manage to write tests for the different code-parts.
// FIXME: manage to write time-benchmarks for the different code-parts ... among others exploring effects/importance/applicaiton of different memory-access-schemes
// FIXME: write code and test correctness of 'basic' distribution-comptuation-strategies
// FIXME[new-algorithm]: ... consider to implemnet 'our own version of' SNAPs ... "K-core decompositon" ... which repreatedly remvoe nodes with low centrality (eg, degree-centrlaity) ... 'forming a n hioercrchy of central nodes'.
// FIXME[new-algorithm]: ... consider to implemnet 'our own version of' SNAPs ... 
// FIXME[new-algorithm]: ... identify 'representative' "k-means" clsuters, and then test of these clusters 'fit' to unkown data ... ie, first describe some example cases/categories, eg, ...??... .... in order to select the features we consider/evaluate several approaches: (a) 'coverage' and 'centrality' in data-sets; (b) ....??...
// FIXME[new-algorithm]: ... 

// FIXME[time-benchmark]: test different memory-allocation-rotuines in the "c cluster-re-implemetnaiton", step(2): ... 
// FIXME[time-benchmark]: compare the exueciton-time of centrlaity-comptuation in "SNAP" VS 'our new-implemtantion' in our "graphAlgorithms_centrality" ... and thereafter consider to (a) write an article which discuss/evlauate the perference of our appraoch and (b) give teh "SNAP" develoeprs an heads-up, eg, to give them accccess to our enw datasets, as the authros seems 'unaware' of biologcial datasets (eg, as examplfid at "http://snap.stanford.edu/data/") ... eg, to write an aritcle similar to "Mapping the Gnutella Network: Properties of Large-Scale Peer-to-Peer Systems and Implications for System Design" ["http://globustoolkit.org/alliance/publications/papers/gnutella.computing.pdf"] <-- what are the 'findings' which we need?
// FIXME[time-benchmark]: compare the time-differnece of our "lib_centrality.js" JavaScript-centrlaity-lbirary and our "graphAlgorithms_centrality" C++ centrlaity-algorithm
// FIXME: manage to get the "bio2rdf" parser to wrok for the pubmed-datset, step(2): .. 
// FIXME: manage to get the "bio2rdf" parser to wrok for the pubmed-datset, step(3): .. 
// FIXME: feed 'extracts' from tghe pubmed-datset into our [ªbove] algorithms/fucntions ... and evalaute both time-performance and 'information inferred from applicaiton of [ªbove] algorithsms/proceudres'.

  // FIXME[new-article]: ... evaluate the optimizaiton-degree of clusteirng-algorithms in "http://pointclouds.org/", eg, "http://pointclouds.org/documentation/tutorials/conditional_euclidean_clustering.php"
  // FIXME[new-article]: ... 


// ok: FIXME: write code and test correctness of 'basic' distribution-algorithms ...  eg, to count use "mean" and/or "median" to ...??... and tehn 'palce' teh valeus into K pre-defined buckets ... write a new class named "graphAlgorithms_distribution"
// ok: FIXME: write code and test correctness of 'basic' centrlaity-comptuation-strategies ... use "/home/klatremus/poset_src/web/js/alg/lib_centrality.js" as template when writing a new class named "graphAlgorithms_centrality"
  // ok: FIXME: ok(a) update description and 'consistency-comments' in our "graphAlgorithms_kCluster.cxx"; ok(b) replace all "uint **mask" with "char **mask"; (c) write a time-comparison of the latter 'comparessed' data-representaiton ... and then udpate our docuemtatnion wrt. (1) reduced memory-consumption, and (2) reduced exeuction-time.
  // ok: FIXME: imrpvoe documetnaiton of our "graphAlgorithms_tree" class ... and write benchmark-tests.
  // ok: FIXME: imrpvoe documetnaiton of our "graphAlgorithms_som" class ... and write benchmark-tests.
//ok: FIXME[new-algorithm]: ... re-implemnet our "SCC-algorithm" 'into this'




// FIXME: re-write the test in "/home/klatremus/poset_src/externalLibs/c_cluster/example/example.c" ... an use the latter to explain the applicaiotn, implemtnation and wekaensses/'cotrnaitns' of reahc implemantion7fucniton/algorithm ... 

// FIXME: write new classes for: ... 

// FIXME: try to replace all "int" occurences with "uint"

// FIXME: in the algorithms which use 'random intiation' consider to try using centlaityv-valeus as 'intiation' wrt. clusters ... eg, to use ditrubiotn of 'relative central vertices' to suggest the number of clsutersin "k-means" and "k-median" 'clstuering-calls'.

// FIXME: givne the poularity of this cluster-library .. try to ivnestigate the effects of diffenret memory-optimizaitons and FLOP-optimization-technqiues ... adn then wrtie an artilce descirigng/higlight the resutls ... and use the differnet sub-examples in this libraries to descirbe/idneitfy 'bottlnecskk' wehn a cache-aware memory-poolicy 'decreases the xeuction-cost without increaisng the complexity of the implenmtantion nor reducing the accurayc of the reuslts' ... and in this context demonstrate/compare the 'unability' of compielrs to increase the efficnecy 'of poorly cosntructed memory-allcoation-rotuiens' when compared to 'their aiblity to optimize wrt. poorly structured FLOPS centered/dpendent for-loops', eg, wrt. "the placign of varibiables either isndie the for-lopp or otuside the for-loop' (where the latter is used by som programmers, eg, in the "c-library", to reduce the size of assembly-code-stack-size.


// FIXME: read the PDF-documetnaiton in the "c-clsuter-lbirary' ... and then update our docuemtnation ... and description of use-cases (in the file/class).

  // FIXME: add opption for ... asserts
  // FIXME: add opption for ... time-benchmark ... and consider to use "signature_network_timeLog.cxx signature_network_stat_scc_timeLog.cxx " as templates wrt. 'time-meausrements'.
  // FIXME: add opption for ... 
  // FIXME: add soemthiong.




  // traverse_entailCycle::assert_class(/*print_info=*/true); //! (oekseth, 06. feb. 2016).
  //signature_network::assert_class(/*print_info=*/true); //! (oekseth, 06. feb. 2016).
#ifdef assert_code
  //const bool print_info = true;
  // TODO: add something.
#endif

  return 1;
    }



//! The main-function to be called for startint hte class-based correctness-tests.
int main(const int array_cnt, char **array) {
  // {
  //   const uint size = 50; const char empty_0 = 0;
  //   char *rmask1 = allocate_1d_list_char(size, empty_0);
  //   const uint j2 = 0;
  //   VECTOR_FLOAT_TYPE vec_mask_1 = VECTOR_FLOAT_convertFrom_CHAR_buffer(&rmask1[j2]);
  //   VECTOR_FLOAT_TYPE vec_mask_1_alt = VECTOR_FLOAT_convertFrom_CHAR_buffer__xmtCharOverFlow(rmask1, j2);
  //   assert(false);
  // }
  //printf("tmp=(%f OR %f OR %f), at %s:%d\n", mathLib_float_pow(-1, /*(3/2)=*/1.5), mathLib_float_pow(0, /*(3/2)=*/1.5), mathLib_float_pow(2, /*(3/2)=*/1.5), __FILE__, __LINE__); assert(false);
  // printf("MAX*MAX=%f, at %s:%d\n", T_FLOAT_MAX * T_FLOAT_MAX, __FILE__, __LINE__); assert(false);
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************

  const int ret_val = __stub__main(array_cnt, array);

  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  //! @return:
  return ret_val;
}
