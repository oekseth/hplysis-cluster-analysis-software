#include "correlation_base.h"

// //! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
// t_float *allocate_1d_list_float(const uint size, const t_float default_value) {  
//   t_float *tmp = new t_float[size];
//   memset(tmp, default_value, sizeof(t_float)*size);
//   return tmp;
// }
// //! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
// void free_1d_list_float(t_float **list) {  
//   assert(list);
//   t_float *tmp = *list;
//   delete [] tmp;
// }


//! Note: this funciton is used to 'only' iterate throug half of the items in a [][] matrix.
void __get_updatedNumberOF_lastSizeChunk_entry(const uint nrows, const uint SM, uint *numberOf_chunks, uint *chunkSize_row_last) {
  assert_possibleOverhead(SM != UINT_MAX);
  //assert(nrows >= SM);
  //! Avoid overflwo in the last chunks of cahce-buffers:
  //*numberOf_chunks = (nrows >= SM) ? nrows / SM : 1;
  if(nrows >= SM) {
    *numberOf_chunks = nrows / SM;
    // FIXME: vliadte correctness of [”elow] new-added 'fix'.
    //if(*numberOf_chunks == 0) {*numberOf_chunks = 1;}
    //assert(*numberOf_chunks != 0); // FIXME: valdiate this new-added assertion.
    // 
    const uint size_of_chunksTotal = (SM**numberOf_chunks);
    /* if(size_of_chunksTotal <= nrows) { */
    *chunkSize_row_last = nrows - size_of_chunksTotal;
    /* } else { */
    
    /* } */
    assert( (*chunkSize_row_last + SM**numberOf_chunks) == nrows );
    if(*chunkSize_row_last == 0) {*chunkSize_row_last = SM;}
    assert(*chunkSize_row_last <= SM);
  } else { //! then we handle this case (oekseth, 06. nov. 2016):
    *numberOf_chunks = 1;
    *chunkSize_row_last = nrows;
  }
  // printf("nrows=%u, *numberOf_chunks=%u=%.4f, SM=%u, at %s:%d\n", nrows, *numberOf_chunks, (t_float)nrows/(t_float)SM, SM, __FILE__, __LINE__);
  // printf("chunkSize_row_last=%u, sum-prev=%u, at %s:%d\n", chunkSize_row_last, SM**numberOf_chunks, __FILE__, __LINE__);
}

//! Identify the last chunk-size for datasets where nrows and ncols are not divisible by SM.
void __get_updatedNumberOF_lastSizeChunk(const uint nrows, const uint ncols, const uint SM, uint *chunkSize_row_last, uint *chunkSize_col_last) {
  //! Avoid overflwo in the last chunks of cahce-buffers:
  { //! First for the rows:
    assert(SM > 0); assert(nrows > 0);
    const uint numberOf_chunks = nrows / SM;
    // 
    *chunkSize_row_last = nrows - (SM*numberOf_chunks);
    assert( (*chunkSize_row_last + SM*numberOf_chunks) == nrows );
    if(*chunkSize_row_last == 0) {*chunkSize_row_last = SM;}
    assert(*chunkSize_row_last <= SM);
    // printf("nrows=%u, numberOf_chunks=%u=%.4f, SM=%u, at %s:%d\n", nrows, numberOf_chunks, (t_float)nrows/(t_float)SM, SM, __FILE__, __LINE__);
    // printf("chunkSize_row_last=%u, sum-prev=%u, at %s:%d\n", chunkSize_row_last, SM*numberOf_chunks, __FILE__, __LINE__);
  }
  { //! Thereafter for the columns:
    const uint numberOf_chunks = ncols / SM;
    // const int diff = ncols - (((int)(ncols-1) - (int)numberOf_chunks)); // * (int)SM; //! ie, "(ncols * SM) - (numberOf_chunks * SM)"
    // assert(diff >= 0);
    // assert((uint)diff <= SM);
    *chunkSize_col_last = ncols - (SM*numberOf_chunks);
    if(*chunkSize_col_last == 0) {*chunkSize_col_last = SM;}
    // printf("ncols=%u, numberOf_chunks=%u=%.4f, SM=%u, at %s:%d\n", ncols, numberOf_chunks, (t_float)ncols/(t_float)SM, SM, __FILE__, __LINE__);
    assert(*chunkSize_col_last <= SM);
  }	
  if(false) { //! Validate [abov€]:
    const uint cnt_elementInMatrix = nrows * ncols * ncols;
    const uint numberOf_chunks_rows = nrows / SM;
    const uint numberOf_chunks_cols = ncols / SM;
    const uint sumOf_beforeLast = ( numberOf_chunks_rows*numberOf_chunks_cols*numberOf_chunks_cols*SM*SM*SM);
    printf("\t sumOf_beforeLast=%u, cnt_elementInMatrix=%u, chunkSize_row_last=%u, chunkSize_col_last=%u, at %s:%d\n", sumOf_beforeLast, cnt_elementInMatrix, *chunkSize_row_last, *chunkSize_col_last, __FILE__, __LINE__);
    assert(cnt_elementInMatrix >= sumOf_beforeLast);
    const uint remaining = cnt_elementInMatrix - sumOf_beforeLast;
	  
  }
}



//! @return the max-value of the vector
static t_float __get_maxValue_slow__float(const uint nrows, const uint ncols, t_float **data) {
  t_float max_value_return = T_FLOAT_MIN_ABS;
  if(!ncols || !nrows) {return max_value_return;}
  assert(nrows);   assert(ncols);  assert(data);
  // FIXME: perofrmance-test this funciton.
  // FIXME[jc]: suggestions for 'how makign this function of itnerest in researhc-aritlces?
  for(uint index1 = 0; index1 < nrows; index1++) {
    assert_possibleOverhead(data[index1]);
    uint index2 = 0;
    //! Then iterate through the 'inner' values:
    for(; index2 < ncols; index2++) {
      if(data[index1][index2] > max_value_return) {max_value_return = data[index1][index2];}
    }
  }
  //! @return the result:
  return max_value_return;
}
//! @return the min-value of the vector
static t_float __get_minValue_slow__float(const uint nrows, const uint ncols, t_float **data) {
  assert(nrows);   assert(ncols);  assert(data);
  t_float min_value_return = T_FLOAT_MIN_ABS;
  // FIXME: perofrmance-test this funciton.
  // FIXME[jc]: suggestions for 'how makign this function of itnerest in researhc-aritlces?
  for(uint index1 = 0; index1 < nrows; index1++) {
    uint index2 = 0;
    //! Then iterate through the 'inner' values:
    for(; index2 < ncols; index2++) {
      if(data[index1][index2] > min_value_return) {min_value_return = data[index1][index2];}
    }
  }
  //! @return the result:
  return min_value_return;
}


//! @return the max-value of the vector
static t_float __get_maxValue_SIMD__float(const uint nrows, const uint ncols, t_float **data) {
#define type t_float
#define type_operator >
#define type_defaultValue T_FLOAT_MIN_ABS
#define SSE_TYPE VECTOR_FLOAT_TYPE
#define SSE_store VECTOR_FLOAT_STORE
#define SSE_load VECTOR_FLOAT_LOAD
#define SSE_operator VECTOR_FLOAT_MAX
#define SSE_iterSize VECTOR_FLOAT_ITER_SIZE
  //! Apply the lgocis and return:
  assert(data);
#include "__get_maxValue_SIMD__float.c"
}



//! @return the min-value of the vector
static t_float __get_minValue_SIMD__float(const uint nrows, const uint ncols, t_float **data) {
#define type t_float
#define type_operator <
#define type_defaultValue T_FLOAT_MAX
#define SSE_TYPE VECTOR_FLOAT_TYPE
#define SSE_store VECTOR_FLOAT_STORE
#define SSE_load VECTOR_FLOAT_LOAD
#define SSE_operator VECTOR_FLOAT_MIN
#define SSE_iterSize VECTOR_FLOAT_ITER_SIZE
  //! Apply the lgocis and return:
  assert(data);
#include "__get_maxValue_SIMD__float.c"
}

//! @return the sum of values of the vector
t_float get_sum_SIMD_vetor1d__float(const uint size, const t_float *list) {
  assert(size > 0);   assert(list);
  const uint ncols_maxEffective_size = (size > 4) ? size - 4 : 0;
  VECTOR_FLOAT_TYPE  vec_sum;   t_float sum_return = 0; //T_FLOAT_MIN_ABS;
  // FIXME[jc]: suggestions for 'how makign this function of itnerest in researhc-aritlces?
  uint index2 = 0;
  if(ncols_maxEffective_size > 0) {
    for(; index2 < ncols_maxEffective_size; index2 += 4) {
      //! Then udpate the max-count:
      vec_sum = VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOAD(&list[index2]), vec_sum);
    }
  }
  //! Then iterate through the 'inner' values:
  for(; index2 < size; index2++) {
    sum_return += list[index2];
  }
  if(ncols_maxEffective_size > 0) { //! then iterate through the vector-values:
    // FIXME[jc]: may you idneitfy an intristinc function for [”elow]?
    t_float result[4]; VECTOR_FLOAT_STORE(result, vec_sum);    
    for(uint i = 0; i < 4; i++) {
      sum_return = result[i];
    }
  }
  //! @return the result:
  return sum_return;
}



//! ------------ char ------------------------

//! @return the max-value of the vector
static char __get_maxValue_slow__char(const uint nrows, const uint ncols, char **data) {
  assert(nrows);   assert(ncols);  assert(data);
  char max_value_return = CHAR_MIN;
  // FIXME: perofrmance-test this funciton.
  // FIXME[jc]: suggestions for 'how makign this function of itnerest in researhc-aritlces?
  for(uint index1 = 0; index1 < nrows; index1++) {
    uint index2 = 0;
    //! Then iterate through the 'inner' values:
    for(; index2 < ncols; index2++) {
      if(data[index1][index2] > max_value_return) {max_value_return = data[index1][index2];}
    }
  }
  //! @return the result:
  return max_value_return;
}
//! @return the min-value of the vector
static char __get_minValue_slow__char(const uint nrows, const uint ncols, char **data) {
  assert(nrows);   assert(ncols);  assert(data);
  char min_value_return = CHAR_MAX;
  // FIXME: perofrmance-test this funciton.
  // FIXME[jc]: suggestions for 'how makign this function of itnerest in researhc-aritlces?
  for(uint index1 = 0; index1 < nrows; index1++) {
    uint index2 = 0;
    //! Then iterate through the 'inner' values:
    for(; index2 < ncols; index2++) {
      if(data[index1][index2] > min_value_return) {min_value_return = data[index1][index2];}
    }
  }
  //! @return the result:
  return min_value_return;
}

//! @return the max-value of the vector
static char __get_maxValue_SIMD__char(const uint nrows, const uint ncols, char **data) {

#ifndef NDEBUG
// #warning "TODO: may you JC gtry to get [”elow] char-code working and then 'remove' the inslusion of the slow proceudre?"
#endif

  return __get_maxValue_slow__char(nrows, ncols, data);  // FIXME: remove this and include [”elow].

// #define type char
// #define type_operator >
// #define type_defaultValue CHAR_MIN
// #define SSE_TYPE VECTOR_CHAR_TYPE
// #define SSE_store VECTOR_CHAR_STORE
// #define SSE_load VECTOR_CHAR_LOAD
// #define SSE_operator VECTOR_CHAR_MAX
// #define SSE_iterSize VECTOR_CHAR_ITER_SIZE
//   //! Apply the lgocis and return:
// #include "__get_maxValue_SIMD__float.c"
}
//! @return the min-value of the vector
static char __get_minValue_SIMD__char(const uint nrows, const uint ncols, char **data) {

#ifndef NDEBUG
// #warning "TODO: may you JC try to get [”elow] char-code working and then 'remove' the inslusion of the slow proceudre .... and in this cotnext suggest an approx wrt. 32-bit uint?"
#endif

  return __get_minValue_slow__char(nrows, ncols, data);  // FIXME: remove this and include [”elow].

// #define type char
// #define type_operator <
// #define type_defaultValue CHAR_MAX
// #define SSE_TYPE VECTOR_CHAR_TYPE
// #define SSE_store VECTOR_CHAR_STORE
// #define SSE_load VECTOR_CHAR_LOAD
// #define SSE_operator VECTOR_CHAR_MIN
// #define SSE_iterSize VECTOR_CHAR_ITER_SIZE
//   //! Apply the lgocis and return:
// #include "__get_maxValue_SIMD__float.c"




}


//! ------------ uint ------------------------

//! @return the max-value of the vector
static uint __get_maxValue_slow__uint(const uint nrows, const uint ncols, uint **data) {
  assert(nrows);   assert(ncols);  assert(data);
  uint max_value_return = 0;
  // FIXME: perofrmance-test this funciton.
  // FIXME[jc]: suggestions for 'how makign this function of itnerest in researhc-aritlces?
  for(uint index1 = 0; index1 < nrows; index1++) {
    uint index2 = 0;
    //! Then iterate through the 'inner' values:
    for(; index2 < ncols; index2++) {
      if(data[index1][index2] > max_value_return) {max_value_return = data[index1][index2];}
    }
  }
  //! @return the result:
  return max_value_return;
}
//! @return the min-value of the vector
static uint __get_minValue_slow__uint(const uint nrows, const uint ncols, uint **data) {
  assert(nrows);   assert(ncols);  assert(data);
  uint min_value_return = UINT_MAX;  
  // FIXME: perofrmance-test this funciton.
  // FIXME[jc]: suggestions for 'how makign this function of itnerest in researhc-aritlces?
  for(uint index1 = 0; index1 < nrows; index1++) {
    uint index2 = 0;
    //! Then iterate through the 'inner' values:
    for(; index2 < ncols; index2++) {
      if(data[index1][index2] > min_value_return) {min_value_return = data[index1][index2];}
    }
  }
  //! @return the result:
  return min_value_return;
}


//! @return the max-value of the vector
static uint __get_maxValue_SIMD__uint(const uint nrows, const uint ncols, uint **data) {
  uint max_value_return = 0;

  // FIXME[jc]: ... seemslike the 'uint' max-value requires SIMD4 ... may you validate that assumtpion ... ie, that instrinsticts-operaitons will not reduce the exueciton-tiem for this oepraiton?
  max_value_return = __get_maxValue_slow__uint(nrows, ncols, data);

  //! @return the result:
  return max_value_return;
}
//! @return the min-value of the vector
static uint __get_minValue_SIMD__uint(const uint nrows, const uint ncols, uint **data) {
  assert(nrows);   assert(ncols);  assert(data);
  uint min_value_return = UINT_MAX;

  // FIXME[jc]: ... seemslike the 'uint' max-value requires SIMD4 ... may you validate that assumtpion ... ie, that instrinsticts-operaitons will not reduce the exueciton-tiem for this oepraiton?
  min_value_return = __get_minValue_slow__uint(nrows, ncols, data);

  //! @return the result:
  return min_value_return;
}



//! @reutrn the min value for a matrix.
t_float get_minValue__float(const uint nrows, const uint ncols, t_float **data, const bool isTo_use_fastFunction) {
  //! Note: if the "isTo_use_fastFunction" parameter is sued, tehn the comptuation goes apprxo. 3x faster.
  assert(data);
  if(isTo_use_fastFunction) { return __get_minValue_SIMD__float(nrows, ncols, data);}
  else { return __get_minValue_slow__float(nrows, ncols, data);}
}

//! @reutrn the max value for a matrix.
t_float get_maxValue__float(const uint nrows, const uint ncols, t_float **data, const bool isTo_use_fastFunction) {
  assert(data);
  //! Note: if the "isTo_use_fastFunction" parameter is sued, tehn the comptuation goes apprxo. 3x faster.
  if(isTo_use_fastFunction) { return __get_maxValue_SIMD__float(nrows, ncols, data);}
  else { return __get_maxValue_slow__float(nrows, ncols, data);}
}

//! @reutrn the min value for a matrix.
char get_minValue__char(const uint nrows, const uint ncols, char **data, const bool isTo_use_fastFunction) {
  //! Note: if the "isTo_use_fastFunction" parameter is sued, tehn the comptuation goes apprxo. 3x faster.
  if(isTo_use_fastFunction) { return __get_minValue_SIMD__char(nrows, ncols, data);}
  else { return __get_minValue_slow__char(nrows, ncols, data);}
}

//! @reutrn the max value for a matrix.
char get_maxValue__char(const uint nrows, const uint ncols, char **data, const bool isTo_use_fastFunction) {
  //! Note: if the "isTo_use_fastFunction" parameter is sued, tehn the comptuation goes apprxo. 3x faster.
  if(isTo_use_fastFunction) { return __get_maxValue_SIMD__char(nrows, ncols, data);}
  else { return __get_maxValue_slow__char(nrows, ncols, data);}
}
//! @reutrn the min value for a matrix.
uint get_minValue__uint(const uint nrows, const uint ncols, uint **data, const bool isTo_use_fastFunction) {
  //! Note: if the "isTo_use_fastFunction" parameter is sued, tehn the comptuation goes apprxo. 3x faster.
  if(isTo_use_fastFunction) { return __get_minValue_SIMD__uint(nrows, ncols, data);}
  else { return __get_minValue_slow__uint(nrows, ncols, data);}
}

//! @reutrn the max value for a matrix.
uint get_maxValue__uint(const uint nrows, const uint ncols, uint **data, const bool isTo_use_fastFunction) {
  //! Note: if the "isTo_use_fastFunction" parameter is sued, tehn the comptuation goes apprxo. 3x faster.
  if(isTo_use_fastFunction) { return __get_maxValue_SIMD__uint(nrows, ncols, data);}
  else { return __get_maxValue_slow__uint(nrows, ncols, data);}
}



//! @return true if we need to 'care' cabotu the mask-property when calculating matrices:
bool matrixMakesUSeOf_mask(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const bool transpose, const uint iterationIndex_2) {
  bool maskNotSet = false;
  assert(data1);
  if(mask1) {
    // TODO[JC]: investigate/identify the case where masks 'in practise' is not set .... for "mask" .... ask JC to 'write such a function. <-- first start wokring ont hsi wehnwe have verified our 'transposed algorithm' wrt. the 'char' data-type
    maskNotSet = (0 != __get_minValue_SIMD__char(nrows, ncols, mask1));
  } else { //! Then we investgiate if the mask-valeus are set.
    if(transpose == 0) {
      //    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(nrows, ncols, data1));
      //maskNotSet = (T_FLOAT_MAX != __get_maxValue_SIMD__float(nrows, ncols, data1));
    } else {
      assert(iterationIndex_2 == UINT_MAX); //! ie, oterhwise udpate [”elow] call.
      // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(ncols, nrows, data1)); //! ie, the inverse of [ªbove].
      //maskNotSet = (T_FLOAT_MAX != __get_maxValue_SIMD__float(ncols, nrows, data1)); //! ie, the inverse of [ªbove].
    }
  }
  if(mask2) {
    if(mask2 != mask1) {
      //! Note: we choose to 'ignore the "iterationIndex_2" parameter in order to be on 'the safe side' wrt. evalatuion of mask-proeprteis:

      // assert(false); // FIXME: how are we to use the "iterationIndex_2"?
      // assert(false); // FIXME[JC]: investigate/identify the case where masks 'in practise' is not set .... for "mask" .... ask JC to 'write such a function.
      if(transpose == 0) {
	maskNotSet = (0 != __get_minValue_SIMD__char(/*nrows=*/(iterationIndex_2 == UINT_MAX) ? nrows : iterationIndex_2, ncols, mask2));
      } else {
	maskNotSet = (0 != __get_minValue_SIMD__char(nrows, /*ncols=*/(iterationIndex_2 == UINT_MAX) ? ncols : iterationIndex_2, mask2));
      }
    }
  } else { //! Then we investgiate if the mask-valeus are set.
    if(data2 && (data1 != data2) ) {
      assert(data2);
      // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      if(transpose == 0) {
	maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(/*nrows=*/(iterationIndex_2 == UINT_MAX) ? nrows : iterationIndex_2, ncols, data2));
      } else {
	maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(nrows, /*ncols=*/(iterationIndex_2 == UINT_MAX) ? ncols : iterationIndex_2, data2));
	//assert(iterationIndex_2 == UINT_MAX); //! ie, oterhwise udpate [”elow] call.
	maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(ncols, nrows, data2)); //! ie, the inverse of [ªbove].
      }
    }
  }

  //! @return the result:
  return (maskNotSet == false);
}



//! @return true if we need to 'care' cabotu the mask-property when calculating matrices:
bool matrixMakesUSeOf_mask__noSSE(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const bool transpose, const uint iterationIndex_2) {
  bool maskNotSet = false;
  assert(data1);
  if(mask1) {
    // TODO[JC]: investigate/identify the case where masks 'in practise' is not set .... for "mask" .... ask JC to 'write such a function. <-- first start wokring ont hsi wehnwe have verified our 'transposed algorithm' wrt. the 'char' data-type
    maskNotSet = (0 != __get_minValue_slow__char(nrows, ncols, mask1));
  } else { //! Then we investgiate if the mask-valeus are set.
    if(transpose == 0) {
      maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(nrows, ncols, data1));
    } else {
      assert(iterationIndex_2 == UINT_MAX); //! ie, oterhwise udpate [”elow] call.
      maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(ncols, nrows, data1)); //! ie, the inverse of [ªbove].
    }
  }
  if(mask2) {
    if(mask2 != mask1) {
      //! Note: we choose to 'ignore the "iterationIndex_2" parameter in order to be on 'the safe side' wrt. evalatuion of mask-proeprteis:

      // assert(false); // FIXME: how are we to use the "iterationIndex_2"?
      // assert(false); // FIXME[JC]: investigate/identify the case where masks 'in practise' is not set .... for "mask" .... ask JC to 'write such a function.
      maskNotSet = (0 != __get_minValue_slow__char(nrows, /*ncols=*/(iterationIndex_2 == UINT_MAX) ? ncols : iterationIndex_2, mask2));
    }
  } else { //! Then we investgiate if the mask-valeus are set.
    if(data2 && (data1 != data2) ) {
      assert(data2);
      // TODO: vlaidte that [bleow] udpate reuslts in correct results (oekseth, 06. feb. 2017): <-- udpated correctness-assumption is that the 'column-number' if fixed while the row-number is dymaic, ie, wrt. the 'usage of this function'.
      if(transpose == 0) {
	maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(/*ncols=*/(iterationIndex_2 == UINT_MAX) ? nrows : iterationIndex_2, ncols, data2));
	//maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(nrows, /*ncols=*/(iterationIndex_2 == UINT_MAX) ? ncols : iterationIndex_2, data2));
      } else {
	assert(iterationIndex_2 == UINT_MAX); //! ie, oterhwise udpate [”elow] call.
	//maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(ncols, nrows, data2)); //! ie, the inverse of [ªbove].
	maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(/*ncols=*/(iterationIndex_2 == UINT_MAX) ? ncols : iterationIndex_2, nrows, data2));
      }
    }
  }

  //! @return the result:
  return (maskNotSet == false);
}

//! @return true if we need to 'care' cabotu the mask-property when calculating matrices:
bool matrixMakesUSeOf_mask__uint(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  uint** mask1, uint** mask2, const bool transpose, const uint iterationIndex_2) {
  bool maskNotSet = false;
  if(mask1) {
    // TODO[JC]: investigate/identify the case where masks 'in practise' is not set .... for "mask" .... ask JC to 'write such a function. <-- first start wokring ont hsi wehnwe have verified our 'transposed algorithm' wrt. the 'uint' data-type
    maskNotSet = (0 != __get_minValue_slow__uint(nrows, ncols, mask1));
  } else { //! Then we investgiate if the mask-valeus are set.
    if(transpose == 0) {
      assert(data1);
      maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(nrows, ncols, data1));
    } else {
      assert(iterationIndex_2 == UINT_MAX); //! ie, oterhwise udpate [”elow] call.
      assert(data1);
      maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(ncols, nrows, data1)); //! ie, the inverse of [ªbove].
    }
  }
  if(mask2) {
    if(mask2 != mask1) {
      //! Note: we choose to 'ignore the "iterationIndex_2" parameter in order to be on 'the safe side' wrt. evalatuion of mask-proeprteis:

      // assert(false); // FIXME: how are we to use the "iterationIndex_2"?
      // assert(false); // FIXME[JC]: investigate/identify the case where masks 'in practise' is not set .... for "mask" .... ask JC to 'write such a function.
      maskNotSet = (0 != __get_minValue_slow__uint(nrows, /*ncols=*/(iterationIndex_2 == UINT_MAX) ? ncols : iterationIndex_2, mask2));
    }
  } else if(data2) { //! Then we investgiate if the mask-valeus are set.
    if(data1 != data2) {
      if(transpose == 0) {
	assert(data2);
	maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(nrows, /*ncols=*/(iterationIndex_2 == UINT_MAX) ? ncols : iterationIndex_2, data2));
      } else {
	assert(iterationIndex_2 == UINT_MAX); //! ie, oterhwise udpate [”elow] call.
	assert(data2);
	maskNotSet = (T_FLOAT_MAX != __get_maxValue_slow__float(ncols, nrows, data2)); //! ie, the inverse of [ªbove].
      }
    }
  }

  //! @return the result:
  return (maskNotSet == false);
}


