{
  assert(objCaller_postProcessResult);
  // t_float *result = objCaller_postProcessResult->listOf_result_afterCorrelation; //! ie, the list to update.
  // assert(result);
  //assert((SM % 4) == 0); //! otehrwise udpate [below].
  // assert(false); // FIXME: sub-divide [below] into 'sperate squares'.	    
	    
  const uint chunkSize_index2_iterSize = (chunkSize_index2 >= 256) ? macro__get_maxIntriLength_float(chunkSize_index2) : 0; //! where we use an 'extra-voerhead-thresohld' givne teh comptuational overheaf wrt. SSE-functinos (oekseth, 06. sept. 2016).
  //const uint chunkSize_index2_iterSize = macro__get_maxIntriLength_float(chunkSize_index2);
 
  VECTOR_FLOAT_TYPE vec_sumOf = VECTOR_FLOAT_SET1(0);
  t_float scalar_sumVal = 0;

  for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1++) {
    uint local_index2 = 0;
    if(chunkSize_index2_iterSize > 0) {
      // VECTOR_FLOAT_TYPE vec_result_inner = VECTOR_FLOAT_SET1(0);
      for(; local_index2 < chunkSize_index2_iterSize; local_index2 += VECTOR_FLOAT_ITER_SIZE) {
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&listOf_inlineResults[local_index1][local_index2]);	
	vec_sumOf = VECTOR_FLOAT_ADD(vec_sumOf, vec_input);
      }
    }
    
    // FIXME: write a macro-funciton to avoid the need of [”elow] for-loop ... and then udpate our other/similar functions ... and test the perofrmance-effect of 'this imrpvoement'.
    for(; local_index2 < chunkSize_index2; local_index2++) {
      scalar_sumVal += listOf_inlineResults[local_index1][local_index2];
    }
  }
  //! Merge the sum-values identifed from our SSE-idnetificaiotn with the non-SSE-iterations:
  const t_float scalar_sumVal_alt = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sumOf);
  //! Update the 'glboal max-val-score':
  ((s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)objCaller_postProcessResult)->return_distance += scalar_sumVal +  scalar_sumVal_alt;
}
