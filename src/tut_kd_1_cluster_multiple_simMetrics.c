#include "hpLysis_api.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hp_ccm.h"
#include "kt_metric_aux.h"
#include "kt_list_1d_string.h"

static const uint arrOf_CCMs_size = 13;
static e_kt_matrix_cmpCluster_clusterDistance__cmpType_t arrOf_CCMs[arrOf_CCMs_size] = {
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC_relative__oekseth,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin__sumBetween,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns_relative__oekseth,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_cIndex,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_pointBiserial,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD,
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_rSquared,
};


#define __MiF__isTo_evaluate__local(sim_id) (!isTo_use_directScore__e_kt_correlationFunction((e_kt_correlationFunction_t)sim_id))

typedef struct s_tut_kd_1_cluster_multiple_simMetrics {
  const char *tag;
  e_hpLysis_clusterAlg clusterAlg;
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccmPost_matrix;
  e_kt_matrix_cmpCluster_metric_t ccmPost_gold;
  e_kd_tree_searchStrategy_t enum_kdTree;
  bool corrMetric_prior_use;  
} s_tut_kd_1_cluster_multiple_simMetrics_t;

//! @reutnra na object with defualt cofniguraitons.
s_tut_kd_1_cluster_multiple_simMetrics_t init__s_tut_kd_1_cluster_multiple_simMetrics_t() {
  s_tut_kd_1_cluster_multiple_simMetrics_t self;
  self.tag = NULL;
  self.clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;
  self.ccmPost_matrix = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
  self.ccmPost_gold = e_kt_matrix_cmpCluster_metric_randsIndex;
  self.enum_kdTree = e_kd_tree_searchStrategy_n_nearest;
  self.corrMetric_prior_use = true;
  ///!
  ///! @return
  return self;
}

//! Evaluate for a given data-set.
//! @return the results of the measurements wrt. the CCM-exec-time.
static void  __tut_kd_1_cluster_multiple_simMetrics__dataSet(s_tut_kd_1_cluster_multiple_simMetrics_t self, s_kt_matrix_t mat_input, s_kt_list_1d_uint_t *obj_gold, s_kt_matrix_t *matResult_ccm_matrix, s_kt_matrix_t *matResult_ccm_gold) {
  //  assert(mat_input); 
  assert(mat_input.nrows > 0);
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_max;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_average;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint_kdTree;
  const e_hpLysis_clusterAlg clusterAlg = self.clusterAlg; //e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN;

  // IFXME[tut+eval]: write a permtatuion of [”elow] where we isntead iterate through a set of "e_kt_matrix_cmpCluster_metric_t" .... then select the clsuter with best CCM-score .... where mtoviaton/novelty conserns how we ......??...

  if(obj_gold) {
    assert(obj_gold->list_size == mat_input.nrows); //! ie, for cosnistency.
  }

  { //! Intalize the ccm-scores:
    uint cnt_simMetrics = 0; for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) { if(__MiF__isTo_evaluate__local(sim_id)) { cnt_simMetrics++;}} assert(cnt_simMetrics > 0);
    //! Allocate:
    *matResult_ccm_matrix = initAndReturn__s_kt_matrix(arrOf_CCMs_size, cnt_simMetrics);
    *matResult_ccm_gold   = initAndReturn__s_kt_matrix(arrOf_CCMs_size, cnt_simMetrics);
    //! 
    //! Set the strings: rows:
    for(uint ccm_id = 0; ccm_id < arrOf_CCMs_size; ccm_id++) {
      e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = arrOf_CCMs[ccm_id];
      const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm);
      assert(str); assert(strlen(str));
      assert(ccm_id < matResult_ccm_gold->nrows);
      set_stringConst__s_kt_matrix(matResult_ccm_matrix, ccm_id, str, /*addFor_column=*/false);
      set_stringConst__s_kt_matrix(matResult_ccm_gold, ccm_id, str, /*addFor_column=*/false);
    }
    //! Set the strings: columns:
    uint index_local = 0;    
    for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
      if(__MiF__isTo_evaluate__local(sim_id)) {
	const char *str = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix((e_kt_correlationFunction_t)sim_id);
	assert(str); assert(strlen(str));
	assert(index_local < matResult_ccm_gold->ncols);
	set_stringConst__s_kt_matrix(matResult_ccm_matrix, index_local, str, /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(matResult_ccm_gold, index_local, str, /*addFor_column=*/true);
	//! Increment:
	index_local++;
      }
    }
  }

  for(uint ccm_id = 0; ccm_id < arrOf_CCMs_size; ccm_id++) {
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = arrOf_CCMs[ccm_id];
    uint index_local = 0;    
    for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
      if(__MiF__isTo_evaluate__local(sim_id)) {

	//if(true) {continue;} // FIXME: remove

	//!
	//! Handle 'non-set-valeus':
	//!
	//! Allocate object:
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
	obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///*enum_ccm=*/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id;
	obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
	obj_hp.config.kdConfig.enum_id = self.enum_kdTree; //! ie, the defualt enum-kd-type
	if(true) {
	  obj_hp.config.corrMetric_prior.metric_id = (e_kt_correlationFunction_t)sim_id;
	  obj_hp.config.corrMetric_prior_use = self.corrMetric_prior_use;
	}
	obj_hp.config.corrMetric_insideClustering = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_id, e_kt_categoryOf_correaltionPreStep_none); //(e_kt_correlationFunction_t)sim_id;
	
	//! 
	//! Apply logics:
	// printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
	const bool is_ok = cluster__hpLysis_api(&obj_hp, clusterAlg, &mat_input, /*nclusters=*/UINT_MAX, /*npass=*/100);
	assert(is_ok);
	{
	  //! Get the CCM-score:
	  const t_float ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, self.ccmPost_matrix, NULL, NULL);
	  if(ccm_score != T_FLOAT_MAX) {
	    assert(matResult_ccm_matrix);
	    assert(index_local < matResult_ccm_matrix->ncols);
	    assert(ccm_id < matResult_ccm_matrix->nrows);
	    //!
	    //! Set the score:
	    matResult_ccm_matrix->matrix[ccm_id][index_local] = ccm_score;
	  }
	}
	if(obj_gold && obj_gold->list_size) {
	  //! Get the CCM-score:	  
	  uint *vertex_clusterId = obj_hp.obj_result_kMean.vertex_clusterId;
	  uint cnt_vertex = obj_hp.obj_result_kMean.cnt_vertex;	  
	  if(cnt_vertex > 0) {
	    assert(vertex_clusterId);
	    cnt_vertex = macro_min(cnt_vertex, obj_gold->list_size);
	    assert(cnt_vertex > 0);
	    t_float ccm_score = 0; //scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, self.ccmPost_gold, NULL, NULL);
	    const bool is_ok = ccm__twoHhypoThesis__xmtMatrixBased__hp_ccm(self.ccmPost_gold, vertex_clusterId, obj_gold->list, cnt_vertex, &ccm_score);
	    if(is_ok) {
	      assert(matResult_ccm_gold);
	      assert(index_local < matResult_ccm_gold->ncols);
	      assert(ccm_id < matResult_ccm_gold->nrows);
	      //!
	      //! Set the score:
	      matResult_ccm_gold->matrix[ccm_id][index_local] = ccm_score;
	    }
	  }
	}
	/* //! */
	/* //! Get the result-aprameters 'of our itnerest': */
	/* scalarResult_ncluster = obj_hp.dynamicKMeans__best__nClusters; */
	/* scalarResult_ccmScore = obj_hp.dynamicKMeans__best__score; */
	//! 
	//! De-allcoate object, and return:
	free__s_hpLysis_api_t(&obj_hp);
	//!
	//! Increment:
	index_local++;
      }      
    }
  }
}

      //! 
      //! Describe the data-marging-strategies: 
      enum {
	ccm_min_score, ccm_min_simMetricId, 
	ccm_max_score, ccm_max_simMetricId,
	//ccm_euclid_min_score, 
	//ccm_euclid_max_score,
	//! -----------------
	ccm_undef
      };



/**
   @brief Computes result-predicitosn for a collection of data-sets
   @remarks 
   .. evalaute for both kd-tree and brute-appraoch to comapre implciaotn-effect of kd-tree-data-set
   .. 
   .. 
**/
static void tut_kd_1_cluster_multiple_simMetrics__fromMatrix(const s_kt_matrix_t *mat_input, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, const char *resultPrefix_local, const bool config_rowNameIdentifiesCluster) {
  const uint arrOf_eval_size = 9+4;
  const s_tut_kd_1_cluster_multiple_simMetrics_t arrOf_eval[arrOf_eval_size] = {
    {/*tag=*/"dbScanKdCCMsSilRand", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"dbScanBruteCCMsSilRand", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"dbScanDirectCCMsSilRand", /*alg=*/e_hpLysis_clusterAlg_disjoint, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/false},
    // --- 
    {/*tag=*/"dbScanKdCCMsSilRand2", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex_alt2, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"dbScanBruteCCMsSilRand2", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex_alt2, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"dbScanDirectCCMsSilRand2", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex_alt2, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/false},
    // --- 
    {/*tag=*/"dbScanKdCCMsDunnsRand", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"dbScanBruteCCMsDunnsRand", /*alg=*/e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"dbScanDirectCCMsDunnsRand", /*alg=*/e_hpLysis_clusterAlg_disjoint, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/false},
    // --- 
    {/*tag=*/"hcaCCMsDunnsRand", /*alg=*/e_hpLysis_clusterAlg_HCA_single, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_n_nearest, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"mclCCMsDunnsRand", /*alg=*/e_hpLysis_clusterAlg_disjoint_mclPostStrategy, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"randomBbest", /*alg=*/e_hpLysis_clusterAlg_random_best, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/true},
    {/*tag=*/"randomWorst", /*alg=*/e_hpLysis_clusterAlg_random_worst, /*ccmPost_matrix=*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, /*ccmPost_gold=*/e_kt_matrix_cmpCluster_metric_randsIndex, /*enum_kdTree=*/e_kd_tree_searchStrategy_brute_fast, /*use-corrMetric-prior-clustering=*/true},
    // --- 
  };  

  printf("at %s:%d\n", __FILE__, __LINE__);

  //! 
  //! Evalaute for different algorithm-cases wrt. KD-tree: 
  for(uint eval_id = 0; eval_id < arrOf_eval_size; eval_id++) {
    //    fprintf(stderr, "eval_id=%u, at %s:%d\n", eval_id, __FILE__, __LINE__); 
    //! Note: we expect .... 
    //! allocate the result-data-sets .... seperately for "[data-id][sim-metric]" and "[data-id][internal-matrix-ccm]"
    //!
    //! Seprately ivnestigate ofr diferent nroamlziaiton-appraoches: 
    for(uint isTo_normalize = 0; isTo_normalize <= e_kt_normType_undef; isTo_normalize++) {
      //!
      //! Itniate result-matrices:
      s_kt_matrix_t mat_result_ccmMatrix[ccm_undef];  s_kt_matrix_t mat_result_ccmGold[ccm_undef]; //! ie, to identify the best-fit wrt. simliarty-metircs
      s_kt_matrix_t mat_result_ccmMatrix_sim[ccm_undef];  s_kt_matrix_t mat_result_ccmGold_sim[ccm_undef]; //! ie, to identify the best-fit wrt. simliarty-metircs
      uint cnt_simMetrics = 0; for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) { if(__MiF__isTo_evaluate__local(sim_id)) { cnt_simMetrics++;}} assert(cnt_simMetrics > 0);
      //!
      //!
      for(uint case_id = 0; case_id < ccm_undef; case_id++) {
	mat_result_ccmMatrix[case_id] = initAndReturn__s_kt_matrix(mat_input_size, arrOf_CCMs_size);
	mat_result_ccmGold[case_id]   = initAndReturn__s_kt_matrix(mat_input_size, arrOf_CCMs_size);
	//! Set column-headers:
	for(uint alg_id = 0; alg_id < arrOf_CCMs_size; alg_id++) {
	  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm = arrOf_CCMs[alg_id];
	  const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_ccm);
	  /* const e_hpLysis_clusterAlg_t clusterAlg = self->setOfAlgsToEvaluate[alg_id]; */
	  /* const char *str = get_stringOf__short__e_hpLysis_clusterAlg_t(clusterAlg);	   */
	  set_stringConst__s_kt_matrix(&(mat_result_ccmMatrix[case_id]), alg_id, str, /*addFor_column=*/true);
	  set_stringConst__s_kt_matrix(&(mat_result_ccmGold[case_id]), alg_id, str, /*addFor_column=*/true);
	}
	mat_result_ccmMatrix_sim[case_id] = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
	mat_result_ccmGold_sim[case_id]   = initAndReturn__s_kt_matrix(mat_input_size, cnt_simMetrics);
	//! Set column-headers:
	//! Set the strings: columns:
	uint index_local = 0;    
	for(uint  sim_id = 0; sim_id < (uint)e_kt_correlationFunction_undef; sim_id++) {  
	  if(__MiF__isTo_evaluate__local(sim_id)) {
	    const char *str = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix((e_kt_correlationFunction_t)sim_id);
	    assert(str); assert(strlen(str));
	    assert(index_local < mat_result_ccmGold_sim[case_id].ncols);
	    set_stringConst__s_kt_matrix(&mat_result_ccmMatrix_sim[case_id], index_local, str, /*addFor_column=*/true);
	    set_stringConst__s_kt_matrix(&mat_result_ccmGold_sim[case_id], index_local, str, /*addFor_column=*/true);
	    //! Increment:
	    index_local++;
	  }
	}
      }
      //! Note: we expect .... 
      //!
      //! Iterate: 
      for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
	s_kt_list_1d_uint_t obj_gold = setToEmpty__s_kt_list_1d_uint_t();
	char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id);
	const char *stringOf_tagSample = stringOf_tagSample_local; 
	if(arrOf_stringNames) {
	  const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id);
	  if(str && strlen(str)) {stringOf_tagSample = str;} /*! ie, then use the user-providced data-description to 'set' the string.*/
	}

	assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
	//! 
	//! Set the row-name of the summary-containers:
#define __setRowName(obj) ({set_stringConst__s_kt_matrix(&(obj[case_id]), data_id, stringOf_tagSample, /*addFor_column=*/false);})
	for(uint case_id = 0; case_id < ccm_undef; case_id++) {
	  __setRowName(mat_result_ccmMatrix);
	  __setRowName(mat_result_ccmMatrix_sim);
	  __setRowName(mat_result_ccmGold);
	  __setRowName(mat_result_ccmGold_sim);
	}
#undef __setRowName
	//!
	//! Construct gold-data-mapping:
	if(config_rowNameIdentifiesCluster == false) { //! then Construct gold-data-mapping .... use a MINE+HCA-pre-step: 
	  //!
	  //! Handle 'non-set-valeus':
	  //!
	  //! Allocate object:
	  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = /*isAn__adjcencyMatrix=*/false; 
	  /* obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = enum_ccm; ///\*enum_ccm=*\/(e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id; */
	  /* obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true; */
	  //obj_hp.config.kdConfig = self.enum_kdTree; //! ie, the defualt enum-kd-type
	  if(true) {
	    obj_hp.config.corrMetric_prior.metric_id = e_kt_correlationFunction_groupOf_MINE_mic;
	    obj_hp.config.corrMetric_prior_use = true;
	  }
	  //! 
	  //! Apply logics:
	  printf("[%u]\t (gold-cluster)\t start-clustering for matrix=[%u, %u]: at %s:%d\n", data_id, mat_input[data_id].nrows, mat_input[data_id].ncols, __FILE__, __LINE__);
	  const bool is_ok = cluster__hpLysis_api(&obj_hp, /*clusterAlg*/e_hpLysis_clusterAlg_HCA_max, &(mat_input[data_id]), /*nclusters=*/UINT_MAX, /*npass=*/100);
	  assert(is_ok);
	  printf("[%u]\t (gold-cluster)\t ok: post-process for matrix=[%u, %u]: at %s:%d\n", data_id, mat_input[data_id].nrows, mat_input[data_id].ncols, __FILE__, __LINE__);
	  const uint *vertex_clusterId = obj_hp.obj_result_kMean.vertex_clusterId;
	  uint cnt_vertex = obj_hp.obj_result_kMean.cnt_vertex;	  
	  if(cnt_vertex > 0) {
	    obj_gold = init__s_kt_list_1d_uint_t(cnt_vertex);
	    memcpy(obj_gold.list, vertex_clusterId, sizeof(uint)*cnt_vertex);
	  }
	  //! 
	  //! De-allcoate object, and return:
	  free__s_hpLysis_api_t(&obj_hp);
	} else { //! then Construct gold-data-mapping .... supprot idneitiocnat of clsuter-dis based on gold-name-comparison ... 
	  //! 
	  //! Find the local index of each data-set:
	  const uint cnt_vertex = mat_input[data_id].nrows;
	  obj_gold = init__s_kt_list_1d_uint_t(cnt_vertex);
	  s_kt_list_1d_string_t map_clusterIds = setToEmpty_andReturn__s_kt_list_1d_string_t();
	  //! 
	  //! Find:
	  assert(mat_input[data_id].nameOf_rows);
	  for(uint i = 0; i < cnt_vertex; i++) {
	    const char *str_search = mat_input[data_id].nameOf_rows[i];
	    if(str_search && strlen(str_search)) {
	      const uint clsuter_id = get_indexOf_string_slowSearch__s_kt_list_1d_string_t(&map_clusterIds, str_search, /*insertIf_notFound=*/true);
	      assert(clsuter_id != UINT_MAX);
	      obj_gold.list[i] = clsuter_id; //! ie, set the clsuter-id
	    }
	  }
	  //! De-allocate:
	  free__s_kt_list_1d_string(&map_clusterIds);
	}


	//!
	//! Apply noramizaiotn, ie, if requested:
	s_kt_matrix_t mat_data = mat_input[data_id]; //setToEmptyAndReturn__s_kt_matrix_t();
	if(isTo_normalize != e_kt_normType_undef) {
	  mat_data = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&(mat_input[data_id]), (e_kt_normType_t)isTo_normalize);
	  //free__s_kt_matrix(&obj_matrixInput); //! ie, as we assuem [below] has 'perofmred' a deed copy-oepration.
	  //obj_matrixInput = mat_copy; //! ie, a sahholw copy.
	}
	//!
	//! Allocate for data-sets:  
	s_kt_matrix_t matResult_ccm_matrix = setToEmptyAndReturn__s_kt_matrix_t();
	s_kt_matrix_t matResult_ccm_gold   = setToEmptyAndReturn__s_kt_matrix_t();
	//!
	//! Comptue teh clsuters
	printf("[%u]\t start-clustering for matrix=[%u, %u]: at %s:%d\n", data_id, mat_data.nrows, mat_data.ncols, __FILE__, __LINE__);
	__tut_kd_1_cluster_multiple_simMetrics__dataSet(arrOf_eval[eval_id], mat_data, 
							(obj_gold.list_size > 0) ? &obj_gold : NULL, 
							&matResult_ccm_matrix, &matResult_ccm_gold);
	  printf("[%u]\t ok: post-process for matrix=[%u, %u]: at %s:%d\n", data_id, mat_data.nrows, mat_data.ncols, __FILE__, __LINE__);
	//!
	//! Update the gloca sumamrize-data-strucutres:      
	//! Note: descirbe different summarize-data-strucutres .... [eval-id][data-id] = [min,max][sim-metric-id, ccm-score(matrix), ccm-score(gold)], [eval-id][data-id] = [min,max][sim-metric-id, ccm-score(matrix), ccm-score(gold)]
	assert(matResult_ccm_matrix.nrows == matResult_ccm_gold.nrows);
	assert(matResult_ccm_matrix.ncols == matResult_ccm_gold.ncols);
	for(uint row_id = 0; row_id < matResult_ccm_gold.nrows; row_id++) { //! Then select the min-max-scores wrt. each matrix-internal-CCM:
	  for(uint col_id = 0; col_id < matResult_ccm_gold.ncols; col_id++) { 
#define __MiF__updateExtreme(__mat_result, alg_id, sim_pre, CCM_score) ({ if(CCM_score != T_FLOAT_MAX) { if(__mat_result[ccm_min_score].matrix[data_id][alg_id] != T_FLOAT_MAX) { if(__mat_result[ccm_min_score].matrix[data_id][alg_id]  > CCM_score) { __mat_result[ccm_min_score].matrix[data_id][alg_id] = CCM_score; __mat_result[ccm_min_simMetricId].matrix[data_id][alg_id] = (t_float)sim_pre; } if(__mat_result[ccm_max_score].matrix[data_id][alg_id]  < CCM_score) {  __mat_result[ccm_max_score].matrix[data_id][alg_id] = CCM_score; __mat_result[ccm_max_simMetricId].matrix[data_id][alg_id] = (t_float)sim_pre;} } else { __mat_result[ccm_min_score].matrix[data_id][alg_id] = __mat_result[ccm_max_score].matrix[data_id][alg_id] = CCM_score; __mat_result[ccm_min_simMetricId].matrix[data_id][alg_id] = __mat_result[ccm_max_simMetricId].matrix[data_id][alg_id] = (t_float)sim_pre;}    }	     })
	    
	    //! 
	    //! Update for: matrix-ccm:
	    __MiF__updateExtreme(mat_result_ccmMatrix, row_id, col_id, /*CCM_score=*/matResult_ccm_matrix.matrix[row_id][col_id]);
	    //! Update for: matrix-ccm-inverted:
	    __MiF__updateExtreme(mat_result_ccmMatrix_sim, col_id, row_id, /*CCM_score=*/matResult_ccm_matrix.matrix[row_id][col_id]);
	    //! Update for: gold-ccm:
	    __MiF__updateExtreme(mat_result_ccmGold, row_id, col_id, /*CCM_score=*/matResult_ccm_gold.matrix[row_id][col_id]);
	    //! Update for: gold-ccm-inverted:
	    __MiF__updateExtreme(mat_result_ccmGold_sim, col_id, row_id, /*CCM_score=*/matResult_ccm_gold.matrix[row_id][col_id]);
	  }
	}      
	//!
	{//! Export the results: 
	  assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
	  //! 
	  { //! Write out the data: matrix-scores:
	    char fileName_result[10000]; memset(fileName_result, '\0', 10000); sprintf(fileName_result, "%s_%s_ccmMatrix_%s_norm_%u.tsv", resultPrefix_local, stringOf_tagSample, arrOf_eval[eval_id].tag, isTo_normalize);
	    printf("export: \"%s\", at %s:%d\n", fileName_result, __FILE__, __LINE__);
	    export__singleCall__s_kt_matrix_t(&matResult_ccm_matrix, fileName_result, NULL);
	  }
	  { //! Write out the data: matrix-scores:
	    char fileName_result[10000]; memset(fileName_result, '\0', 10000); sprintf(fileName_result, "%s_%s_ccmGold_%s_norm_%u.tsv", resultPrefix_local, stringOf_tagSample, arrOf_eval[eval_id].tag, isTo_normalize);
	    export__singleCall__s_kt_matrix_t(&matResult_ccm_gold, fileName_result, NULL);
	  }
	}
	//!
	//! De-allocate:
	free__s_kt_matrix(&matResult_ccm_matrix);
	free__s_kt_matrix(&matResult_ccm_gold);
	free__s_kt_list_1d_uint_t(&obj_gold);
	if(isTo_normalize != e_kt_normType_undef) {
	  assert(mat_data.matrix != mat_input[data_id].matrix); //! ie, to avoid a 'clearing' of the input-data-set-id.
	  free__s_kt_matrix(&mat_data);
	}	
	//free__s_kt_matrix(&obj_matrixInput);
      }
#define __writeOut_matrix__summarySets(list_matrix, list_matrix_tag) ({ for(uint case_id = 0; case_id < ccm_undef; case_id++) { const char *str_case = "ccm_min"; if(case_id == ccm_min_simMetricId) {str_case = "ccm_min_simId";} else if(case_id == ccm_max_score) {str_case = "ccm_max";}     else if(case_id == ccm_max_simMetricId) {str_case = "ccm_max_simId";}    else if(case_id != ccm_min_score) {assert(false);} char fileName_result[10000]; memset(fileName_result, '\0', 10000); sprintf(fileName_result, "%s_%s_%s_%s_norm_%u.tsv", resultPrefix_local, "summary", list_matrix_tag, arrOf_eval[eval_id].tag, isTo_normalize); export__singleCall__s_kt_matrix_t(&(list_matrix[case_id]), fileName_result, NULL); free__s_kt_matrix(&(list_matrix[case_id])); }})
      __writeOut_matrix__summarySets(mat_result_ccmMatrix, "ccmMatrix_ccm");
      __writeOut_matrix__summarySets(mat_result_ccmMatrix_sim, "ccmMatrix_simId");
      __writeOut_matrix__summarySets(mat_result_ccmGold, "ccmGold_ccm");
      __writeOut_matrix__summarySets(mat_result_ccmGold_sim, "ccmGold_simId");
      //! -------------------------
#undef __writeOut_matrix__summarySets
      //!
      //!
    }
  }
}
//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).
static s_kt_matrix_t __static__readDataFromObject__kd(s_hp_clusterFileCollection data_obj, const uint data_id, const uint sizeOf__nrows, const uint sizeOf__ncols, const bool config__isTo__useDummyDatasetForValidation) {
  //const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
  const char *stringOf_tagSample = data_obj.file_name;
  //const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  data_obj.tag;}
  //printf("(data=%u)\t#\t[algPos=%u]\t alg_id=%u, rand_id=%u\t\t %s \t at %s:%d\n", data_id, cnt_alg_counts, alg_id, rand_id, stringOf_tagSample, __FILE__, __LINE__);
  assert(stringOf_tagSample);
  
  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(config__isTo__useDummyDatasetForValidation == false) {
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else { //! then we inveeigate using a dummy data-set:
    obj_matrixInput = initAndReturn__s_kt_matrix(sizeOf__nrows, sizeOf__ncols);
    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
      for(uint k = 0; k < obj_matrixInput.ncols; k++) {
	obj_matrixInput.matrix[i][k] = (t_float)(i*k);
      }
    }
  }
  return obj_matrixInput;
}



static void __eval__dataCollection(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *resultPrefix, const bool rowName_identifiesClusterId) {
  assert(mapOf_realLife); assert(mapOf_realLife_size > 0);
  //!
  //! Transform data-set to a differnet foramt: 
  s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0);  
  s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t();
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
    s_kt_matrix_t obj_matrixInput = __static__readDataFromObject__kd(mapOf_realLife[data_id], data_id, 10, 10, /*config__isTo__useDummyDatasetForValidation*/false); //, self->sizeOf__nrows, ->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
    assert(obj_matrixInput.nrows > 0);
    const char *tag = mapOf_realLife[data_id].tag;
    assert(tag); assert(strlen(tag));
    //! Add: string:
    set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag);
    //! Add: matrix:
    assert(mat_collection.list[data_id].nrows == 0); //! ie, to avoid the need for de-allocation.
    mat_collection.list[data_id] = obj_matrixInput; //! ie, copy the cotnent.
  }
  //!
  //! Apply logics: 
  { //! Comptue where gold is usedinfed through MINE+HCA:
    char resultPrefix_local[10000]; memset(resultPrefix_local, '\0', 10000); sprintf(resultPrefix_local, "%s_isToUseHCAMINE", resultPrefix);
    tut_kd_1_cluster_multiple_simMetrics__fromMatrix(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix_local, /*config_rowNameIdentifiesCluster=*/false); //! ie, use HCA+MINE.
  }
  if(rowName_identifiesClusterId) { //! Comptue where gold is usedinfed through overallpgin row-names:
    tut_kd_1_cluster_multiple_simMetrics__fromMatrix(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix, /*config_rowNameIdentifiesCluster=*/true);
  } 
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(&mat_collection);
  free__s_kt_list_1d_string(&arrOf_stringNames);
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief demosntrtes how muliple simlairty-metircs and CCMs may be used during the simliarty-metric-evaluation  (oekseth, 06. jul. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks we evaluate the implcaiton-effect of different CCMs and simlairty-metics for a given data-set. We comptue for both matrix-based CCMs and gold-based CCMs. We meausre the implciaton-evaluate three synetic and three real-life data-sets. For the real-life data-sets we for gold-CCM-comparison use MINE+HCA as a reference-frame. To evaluate correctness and quality-accurayc of the altter we for the synteitc data-sets combiend MINE+HCA with knowledge of sytentic seperation between the prediction-results. 
   @remarks related-tut-examples:
   -- "tut_ccm_15_useCase_singleMatrix_differentPatterns.c": logics to evaluate implicaiton of different patterns. 
   -- "hp_evalHypothesis_algOnData.c": an API for data-anlaysis.
**/
int main(const int array_cnt, char **array) 
#else
  int tut_kd_1_cluster_multiple_simMetrics(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  const char *resultPrefix = "tut_evalKd_";

  { //! Fris seperatley for a gvien input-file:
    const char *file_name = "tests/data/kt_mine/fish_growth.tsv"; //! ie, a samlmer rela-life dat-aset than the IRIS-data-set.
    //    const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set"). 
    const uint index_pos = 3;
    bool fileIs_speificed = false; //! which is used to simplify error-generiaotn.
    if( (array_cnt >= (index_pos+1)) && array && strlen(array[index_pos])) {
      //! then set the file-name:
      file_name = array[index_pos];
      fileIs_speificed = true;
    }
    //!
    //! Load an arbitrary data-set:
    // const uint nrows = 1000; const uint ncols = 20;
    //const uint nrows = 1000; const uint ncols = 20;
    //  const uint nrows = 1000*10; const uint ncols = 5;
    //const uint nrows = 1000*1000; const uint ncols = 20;
    s_kt_matrix_t mat_input = setToEmptyAndReturn__s_kt_matrix_t(); //initAndReturn__s_kt_matrix(nrows, ncols);
    //! Then laod the file:
    assert(file_name); assert(strlen(file_name));
    import__s_kt_matrix_t(&mat_input, file_name);
    if(mat_input.nrows == 0) {
      if(fileIs_speificed) {
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name where correct locaiton requries your exeuciton-locaiton to be in the \"src/\" folder of the hpLysis-repository. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      } else { //! then we asusmet eh file-name was spefieid by the user.
	fprintf(stderr, "!!\t Unable to load input-file=\"%s\", a file-name which was spefiec by your call. Observation at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      }
      return false;
    }
    //!
    //! Apply logics:
    tut_kd_1_cluster_multiple_simMetrics__fromMatrix(&mat_input, /*mapOf_realLife_size=*/1, NULL, /*resultPrefix=*/"result_tutEval_1", /*config_rowNameIdentifiesCluster=*/false);    
    //!
    //! De-allocate:
    free__s_kt_matrix(&mat_input);
  }
  if(false)
  { //! Evaluate large real-lfie GEO-data:sets
    //! Note: For this test-case to work you need to have downlaoded the GEO-data-sets, eg, the " ../../mine-data-analysis/src/sample_data/GSE19753_series_matrix.txt" which is pased through KnittingTools "../../mine-data-analysis/src/perl_buildSamples/parse_geo.pl" GEO-patser.
    const char *fileName = "../../mine-data-analysis/src/result_GSE19743.tsv";
    const int result = access(fileName, R_OK);
    if (result == 0) {  //! thewe we were able to open the file:
      //! PArse the data-file:
      s_kt_matrix_t mat_input = readFromAndReturn__file__advanced__s_kt_matrix_t(fileName, initAndReturn__s_kt_matrix_fileReadTuning_t());
      if(mat_input.nrows > 0) {
	//!
	//! Apply logics:
	tut_kd_1_cluster_multiple_simMetrics__fromMatrix(&mat_input, /*mapOf_realLife_size=*/1, NULL, /*resultPrefix=*/"GEO_GSE19743", /*config_rowNameIdentifiesCluster=*/false);    	  
	//!
	//! A transposed verison:
	s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_input);
	tut_kd_1_cluster_multiple_simMetrics__fromMatrix(&mat_transp, /*mapOf_realLife_size=*/1, NULL, /*resultPrefix=*/"GEO_GSE19743.transposed", /*config_rowNameIdentifiesCluster=*/false);    	  
	//!
	//! De-allocate: 
	free__s_kt_matrix(&mat_input);
	free__s_kt_matrix(&mat_transp);
      } else {
	fprintf(stderr, "!!\t Unable to read file \"%s\" at %s:%d\n", fileName, __FILE__, __LINE__);
      }
    }
  }
  { //! Load for different real-life data-sets:
#define __MiCo__useLocalVariablesIn__dataRealFileLoading 1
#ifdef __MiCo__useLocalVariablesIn__dataRealFileLoading
 const uint sizeOf__nrows = 100;
 const uint sizeOf__ncols = 100;
    const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
    const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
    const bool isTo__testHCA_komb__inKmeans = false;
    const bool isTo__evaluate__simMetric__MINE__Euclid = true;
    const char *stringOf_resultDir = "";
// ---
  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------

  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  // printf("at %s:%d\n", __FILE__, __LINE__);
  //!
  //! We are interested in a more performacne-demanind approach: 

  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  //! --------------------------------------------
  //!
  //! File-specific cofnigurations: 
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!

#endif

    {
#include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset, /*rowName_identifiesClusterId=*/false);
    }
    { //const char *nameOf_experiment = "vincentarelbundock";  //! ie, where latter data-sets are created by "vincentarelbundock". 
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset, /*rowName_identifiesClusterId=*/false);
    }
    { 
#include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset, /*rowName_identifiesClusterId=*/true);
    }
    {
#include "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.
      //! 
      //! Apply logics  (oekseth, 06. arp. 2017):
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
    }
#undef __MiCo__useLocalVariablesIn__dataRealFileLoading
  }

  // ------------------- 
  assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... update our proceudre .... add support for a 'direct' DB-SCAN-evaluation .... where the all-agianst-all-sialrmity-metirc-evlauation udrig a dynamic-CCM-evlauation-aprpaoch is 'seen' as un-neccesary for use-cases where users have domain-knwoeldge of their data-sets (hence correctenss of the latter) .... 



  //! *************************************************************
  //! 
  //! Case: synetic data-sets where the cluster-sepraiotns are explcitly 'encoded' in the input-data-sets.
  //! 
  { //! Compute for synteitc data-sets where cluster-partiions is expllicty known.
    //! Note: we make use of logics in our \hpFile{hp_clusterShapes.h} for cnsturciotn of synteitc data-sets, ie, to eanble/allwo a controlled/careful comaprsin/cosniderioant of CCMs prediocnt-accuracy-difference. 
    const uint nrows = 20; 
    { //! A linear increase in in-correct cluster-predictions:
      //! Note: for simplcitiy we choose to gradulaly decrease teh score-difference between in-clsuter versus between-cluster-members, ie, where latter provides a cocneutally intutive relationship wrt. estlibhsihing cluster-rpediocnt-accuracy. 
      { //! A squared shape: 

      }
      { //! Linear-increase  in cluster-result-sets: 

      }
    }
    { //! Cases where all clusters are correctly idneitfied/set (ie, test in-accuracies/differences wrt. differnet strategies) .... examplfies prediocnt-in-accuraices/base-assumptiosn wrt. CCM-score-senstitiy .... 
      //! Note: to investitat the implication of predictoin-difference (for the same clsuters) we have desinged an appraoch to adjust the cluster-topologeis while keepign the correncess of the clsuter-seperaiton fixed. .... 
      { //! A squared shape:

      }
      { //! Linear-increase in cluster-result-sets.

      }
    }
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... concpetual .... representive data-sets to explore .... case(1) ... a linear increase in in-correct cluster-predioncts ... 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... concpetual .... representive data-sets to explore .... case(2) ... cases where all clusters are correctly idneitfied/set (ie, test in-accuracies/differences wrt. differnet strategies) .... examplfies prediocnt-in-accuraices/base-assumptiosn wrt. CCM-score-senstitiy .... 
    // ---- 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... iteriaotn-funciton to consturct different sytneitc dat-distributions ... 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... set the row-name 'as' the gold-standard-cluster-id
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... evaluate for differet matrix-dimesnions ..... testing the implcaiotn/effect of data-size-pertubrations .... 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... evaluate CCMs directly .... compute [CCM-permtautnions for ccmType=[matrix, gold]][dataEnsamlbe('x' w/ 'syntetic-data' x 'gold-cluster'] = [``Kendall's Tau'' ] ... ie, (a) cosntruct systneitc data-sets; (b) apply CCM-measures, ie, cosntruct a vector of CCM-scores (seperately for each gold-ccm and matrix-ccm); (c) after a data-ensamble is comptued then (sperately for each CCM) compare with a vector holding the corretly rodered ranks (ie, for which we apply/use the pariwise simlairty-metric in quesiton); (d) when all data-ensambles and CCMs are evlauated then export the result. 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... code ... 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... new function .... syntethic data-sets with known cluster-distributions ... 
    // ------------------- 
    assert(false); // FIXME[code::"tut_kd_1_cluster_multiple_simMetrics.c"] ... 
  }



  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}

#undef __MiF__isTo_evaluate__local
