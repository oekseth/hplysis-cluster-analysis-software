#ifndef correlation_s_allAgainstAll_SIMD_inlinePostProcess_h
#define correlation_s_allAgainstAll_SIMD_inlinePostProcess_h


#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "correlation_enums.h"

#include "s_allAgainstAll_config.h"


//! Then we compute: "weight[i][j] = (constant*log(1 - distance/cutoff))^exponent"; 
// FIXME[jc]: what is the name/description of [”elow] funciton? <-- seems like the function does not 'reflect' the formula in the documetnation, ie, may you JC evalaute the correctness of the following euqation (wrt. 'what the fucntion tries to acheive')? <--- in [below] we compute: "weight[i][j] = (constant*log(1 - distance/cutoff))^exponent"; 
#define macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(distance, cutoff, exponent, index1, index2, arr_result, isTo_updateAlsoFor_symmetric_elements) ({ \
if(distance < cutoff) { \
const t_float log_value = 1 - distance/cutoff; \
if(log_value != 0) {						\
  t_float dweight = exp(exponent*log(log_value));			    \
  if(isTo_updateAlsoFor_symmetric_elements) {dweight *= 2;}	    \
  arr_result[index1] += dweight;				    \
  arr_result[index2] += dweight;				    \
 } \
\
 }						\
})



//! A wrapper-function to compute correlations for a matrix.
void ktCorr__postProcess_afterCorrelation_for_value(const uint index1, const uint index2, const t_float distance, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const bool isTo_updateAlsoFor_symmetric_elements) ;

#endif //! EOF
