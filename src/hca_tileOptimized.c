#include "hca_tileOptimized.h"


//! Validate correctenss of our "s_obj_closestPair" algorithm-implementaiton:
char __distanceIsCorrect(const uint n, t_float **distmatrix, uint is, uint js, const t_float distance) {
#ifndef NDEBUG
  const uint local_matrixSize = n; //! ie, to simplify our writing.
  uint is_tmp = 1; uint js_tmp = 0;
  // FIXME: .... consider to both update [”elow] and our procedure wr.t the 'zero' case ... eg, to ingore 'zero-values' from evlauation.
  const t_float distance_cmp = __local__find_minPair(n, n, distmatrix, &is_tmp, &js_tmp);
  if(false){printf("(cmp)\t compare rows=(fast, slow)=(%u, %u) and columns(%u, %u) for distnace=(%f, %f), and |matrix|=%u, at %s:%d\n", is, is_tmp, js, js_tmp, distance, distance_cmp, n, __FILE__, __LINE__);}
  if(distance_cmp != result[inode].distance) {
    //! then write out the current score-table, ie, to simplify debugging:
    if(true) {writeOut_MinScores_humanized(stdout, &s_obj_closestPair);}
  }
  assert_local(distance_cmp == distance);
  assert_local(is_tmp == is); assert_local(js_tmp == js);
#endif
  return true;
}

/**
   @brief Compute Pairwise Maximum- (complete-) Linking (PML) cluster
   @param <nrows> the number of rows in the distmatrix input-data-set
   @param <ncols> the number of columns in the distmatrix input-data-set
   @param <distmatrix> the inptu-data-set.
   @return a cluster-memership-index for each vertex: call our "hca_cutTree(..)" funciton to infer the cluster-ids.
**/
hca_node_t* hca_computeCluster_tileOptimized_pml(const uint nrows, const uint ncols, float** distmatrix) {
  if(nrows != ncols) {
    fprintf(stderr, "!!\t expected the input-matrix to describe the distnace between two vertices, ie, not a set of features: from nrows(%u) != ncols(%u) we assume that the latter is nto the case, ie, please cosntuct a distance/correlation-matrix (before calling thsi funciton), eg, using one of the functiosn which we provide. Fro questions pelase cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
    return NULL;
  }
  assert_local(nrows == ncols); // FIXME:r emvoe this and then update [below]
  const uint nelements = nrows;
  const uint nnodes = nelements - 1;
  //! --- 
  const uint cnt_bucketsEachRows = 10;
  s_dense_closestPair_t s_obj_closestPair; s_dense_closestPair_init(&s_obj_closestPair, nrows, ncols, cnt_bucketsEachRows, distmatrix, /*config_useIntrisintitcs=*/false, /*isTo_use_fast_minTileOptimization=*/true);
  s_dense_closestPair_t *s_obj_closestPair_ref = &s_obj_closestPair;

  uint* clusterid = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/0);
  hca_node_t* result = allocate_1d_list_hca_node(/*size=*/nelements);
  /* Setup a list specifying to which cluster a gene belongs */
  for(uint j = 0; j < nelements; j++) clusterid[j] = j;
  for(uint n = nelements; n > 1; n--) {
    uint is = 1;     uint js = 0;
    //printf("at %s:%d\n", __FILE__, __LINE__);   
    const uint inode = nelements-n;
    //result[inode].distance = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &is, &js, /*nrows_threshold=local_matrixSize=*/nrows, /*ncols_threshold=*/ncols); 

    assert(false); // FIXME: write a permuation of [below] .... to handle the case where "[bucketTile_id][rowTile_id]" is 'implictly updated'.
    result[inode].distance = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &is, &js, /*nrows_threshold=local_matrixSize=*/n, /*ncols_threshold=*/n); 

    assert_local(is < nrows); assert_local(js < ncols);
    assert(__distanceIsCorrect(n, distmatrix, is, js, distance));

    assert(false); // FIXME: in [below] update "is", "js" if 'there is an earlier udpate of these' ... and similary for "n-1".
    assert(false); // FIXME: support the [”elow] ... and update [ªbove].
    
    //! Find the min-distance for the rows:
    //! Note: in [below] we compute: distmatrix[js][j] = max(distmatrix[is][j],distmatrix[js][j]);
    s_rowChunk_chooseMax_for_rowAndRow(__get_obj_rowChunk(s_obj_closestPair_ref), distmatrix, /*fixed_row_id_1=*/js, /*fixed_row_id_2=*/is, /*columnStartPos=*/0, /*columnEndPos=*/js);
    //! Find the minmum-max-values (ie, "min{max-tiles}") and ensure cosnsitency wrt. the 'non-updated' columns: 
    //! Note: in [below] we compute: distmatrix[js][j] = max(distmatrix[is][j], distmatrix[js][j]);
    s_rowChunk_chooseMax_for_columnAndRow(__get_obj_rowChunk(s_obj_closestPair_ref), distmatrix, /*fixed_column_id_fixed=*/js, /*fixed_row_id=*/is, /*rowStartPos=*/0, /*rowEndPos=*/is);
    //! Find the minmum-max-values (ie, "min{max-tiles}") and ensure cosnsitency wrt. the 'non-updated' columns: 
    //! Note: in [below] we compute: distmatrix[j][js] = max(distmatrix[j][is],distmatrix[j][js]);
    s_rowChunk_chooseMax_for_columnAndColumn(__get_obj_rowChunk(s_obj_closestPair_ref), distmatrix, js, is, /*rowStartPos=*/0, /*rowEndPos=*/n-1);
    
    

    //!
    //! Copy
    //! First for the row:
    const uint endPos_rowUpdate_is = is;
    memcpy(&distmatrix[is][j], &distmatrix[n-1][j], sizeof(t_float)*endPos_rowUpdate_is);
    // for (j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];

    //! Thereafter for the column
    //! Note: [below] 'update' is used if: case(a) the [j][is] is a min-value OR case(b) [j][is] is a max-value. <-- ??
    //! Note: in [below] we compute: distmatrix[j][is] = distmatrix[n-1][j];
    assert(false); // FIXME: support the [”elow] ... 
    s_rowChunk_chooseMax_for_columnCopy(__get_obj_rowChunk(s_obj_closestPair_ref), distmatrix, /*fixed_column_id_fixed*/is, /*fixed_row_id=*/n-1, /*rowStartPos=*/is+1, /*rowEndPos=*/n-1);


    /* Update clusterids */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];
    clusterid[js] = n-nelements-1;
    clusterid[is] = clusterid[n-1];

  }
  //! De-allcoate the locally reserved memory:
  s_dense_closestPair_free(&s_obj_closestPair);
}
