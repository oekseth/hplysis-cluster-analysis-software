#ifndef kt_resultS_ccmTime_h
#define kt_resultS_ccmTime_h


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_resultS_ccmTime
   @brief pprovice logics to store resutls of CCM and time meausrements (oekseth, 06. okt.2 017).
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2017).
**/

#include "hp_ccm.h"
#include "hp_distance.h"
#include "kt_metric_aux.h"
#include "kt_list_1d_string.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>



// Primitive container type for PPM image data
// PPM maxvalue parameter omitted, hard-coded to 255 for simplicity
typedef struct {
    size_t nrows, ncols;   // Image dimensions
    uint8_t *data;  // Interleaved triplets of color values in [0,255] range
} imgExport_ppm_t;
//! Fucntion to export a given image
imgExport_ppm_t *init__imgExport_ppm_t (    float **matrix, const size_t nrows, const size_t ncols,     char **mapOf_names_rows, char **mapOf_names_cols );
//! De-allcoet ethe object.
void imgExport_ppm_finalize ( imgExport_ppm_t *ppm );


//! The call to wrtie otu the conctnets of a given file.
int imgExport_ppm_write ( const char *filename, imgExport_ppm_t *ppm ); 

//! Export a given matrix to a *ppm file
void kt_resultS_ccmTime__static__exportTo_ppm(s_kt_matrix_t mat_result, const char *file_pref);

//! A genelirec wrapper to store the results of the comptuation: 
typedef struct s_kt_resultS_ccmTime {
  const char *tag;
  s_kt_matrix_t mat_result_ccm;
  s_kt_matrix_t mat_result_time;
} s_kt_resultS_ccmTime_t;
//! @return an itnaliesd veirosn of the "s_kt_resultS_ccmTime_t" object.
s_kt_resultS_ccmTime_t init__s_kt_resultS_ccmTime_t();
//! Export the result-set to both feature-amtrixes and correlation-sets, an appraoch used to simplify correlation anlaysis of the result set:
void free_andExport__s_kt_resultS_ccmTime_t(s_kt_resultS_ccmTime_t *self, const char *str_prefix);

#endif //! EOF
