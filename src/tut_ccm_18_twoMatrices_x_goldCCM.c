    const e_kt_matrix_cmpCluster_metric_t enum_id = getEnumOf__e_kt_matrix_cmpCluster_metric_t(stringOf_enum);
    //	printf("at %s:%d\n", __FILE__, __LINE__);
    if(enum_id != e_kt_matrix_cmpCluster_metric_undef) {
      __isApplied = true; //! ie, as the 'string matched the enum'.
      if(mat_2->ncols == 0) {
	// printf("at %s:%d\n", __FILE__, __LINE__);
	//! Allcoate:
	*mat_result = initAndReturn__s_kt_matrix(/*cnt-ccms=*/mat_1->nrows, /*evaluation-cases-each=*/mat_1->nrows);
	bool result_hasIndeitifedAtLEastOne = false;
	//! Then we apply a 'self-comparisn':
	for(uint data_id = 0; data_id < mat_1->nrows; data_id++) {	
	  { //! Set headers: columns: 
	    allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis[%u]", data_id); //! ie, intate a new variable "str_local". 
	    set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id, str_local, /*addFor_column=*/false);
	  }
	  //! Iterate:
	  for(uint data_id_out = 0; data_id_out < mat_1->nrows; data_id_out++) {	
	    if(data_id == 0)  { //! Set headers: columns: 
	      allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis[%u]", data_id_out); //! ie, intate a new variable "str_local". 
	      set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id_out, str_local, /*addFor_column=*/true);
	    } 
	    s_kt_list_1d_uint_t list_1 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
	    s_kt_list_1d_uint_t list_2 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
	    uint cnt_added_0 = 0; uint cnt_added_1 = 0;
	    for(uint i = 0; i < mat_1->ncols; i++) { 	//! Set valeus:
	      {
		const t_float score = mat_1->matrix[data_id][i];
		if(score != T_FLOAT_MAX) {
		  list_1.list[i] = (uint)score;
		  cnt_added_0++;
		} else {list_1.list[i] = UINT_MAX;}
	      }
	    }
	    if(mat_2 && (mat_2->ncols > 0) ) {
	      for(uint i = 0; i < mat_2->ncols; i++) { 	//! Set valeus:
		{
		  assert(mat_2);
		  const t_float score = mat_2->matrix[data_id_out][i];
		  if(score != T_FLOAT_MAX) {
		    list_2.list[i] = score;
		    cnt_added_1++;
		  } else {list_2.list[i] = UINT_MAX;}
		}
	      }
	    } else {
	      for(uint i = 0; i < mat_1->ncols; i++) { 	//! Set valeus:
		{
		  const t_float score = mat_1->matrix[data_id_out][i];
		  if(score != T_FLOAT_MAX) {
		    list_2.list[i] = (uint)score;
		    cnt_added_1++;
		  } else {list_2.list[i] = UINT_MAX;}
		}
	      }
	    }
	    //! Comptue the CCM: 
	    t_float scalar_result = 0;
	    if(cnt_added_0 && cnt_added_1) {
	      // printf("compute, for cnt_added_1=%u, cnt_added_0=%u, at %s:%d\n", cnt_added_1, cnt_added_0, __FILE__, __LINE__);
	      const bool is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(enum_id, &list_1, &list_2, &scalar_result);
	      assert(is_ok);
	    } else {
	      scalar_result = T_FLOAT_MAX;
	    }
	    //!
	    //! Udpate result:
	    assert(data_id < mat_result->nrows);
	    assert(data_id_out < mat_result->ncols);
	    mat_result->matrix[data_id][data_id_out] = scalar_result;
	    if(isOf_interest(scalar_result)) {result_hasIndeitifedAtLEastOne = true;}
	    printf("\t\t [%u][%u]=%f, at %s:%d\n", data_id, data_id_out, scalar_result, __FILE__, __LINE__);
	    //!
	    //! De-allcoate:
	    free__s_kt_list_1d_uint_t(&list_1);
	    free__s_kt_list_1d_uint_t(&list_2);
	  }
	}
	if(result_hasIndeitifedAtLEastOne == false) {
	  // MF__inputError_generic(str_local);
	  fprintf(stderr, "(info)\t Did not identify any scores which were regarded as significant in your data-sets, ie, which explains why the result-matrix is empty. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	}
      } else { //! Then we comapre the 'hyptothesis' in matrix(1) with 'hyptothesis' in matrix(2):
	if(mat_1->ncols != mat_2->ncols) {
	  allocOnStack__char__sprintf(2000, str_local, "Expected the number of hyptothesis (ie, columns) in the first matrix to equal the number of hyptothesis (ie, rows) in the second matrix. However, what we observe is that dim(file=1)=[%u, %u], while dim(file=2)?[%u, %u], ie, indicating an error in your data-input. ", mat_1->nrows, mat_1->ncols, mat_2->nrows, mat_2->ncols); //! ie, intate a new variable "str_local". 
	  MF__inputError_generic(str_local);
	  assert(false);
	  return false;
	} 
	//! Allcoate:
	//const uint cnt_hyp = mat_1->nrows * mat_2->nrows
	*mat_result = initAndReturn__s_kt_matrix(/*cnt-ccms=*/mat_1->nrows, /*evaluation-cases-each=*/mat_2->nrows);
	//! Then we apply a 'self-comparisn':
	for(uint data_id = 0; data_id < mat_1->nrows; data_id++) {	
	  { //! Set headers: columns: 
	    allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis_data1[%u]", data_id); //! ie, intate a new variable "str_local". 
	    set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id, str_local, /*addFor_column=*/false);
	  }
	  //! Iterate:
	  for(uint data_id_out = 0; data_id_out < mat_1->nrows; data_id_out++) {	
	    if(data_id == 0)  { //! Set headers: columns: 
	      allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis_data2[%u]", data_id_out); //! ie, intate a new variable "str_local". 
	      set_stringConst__s_kt_matrix(mat_result, /*index=*/data_id_out, str_local, /*addFor_column=*/true);
	    } 
	    assert(mat_1->ncols == mat_2->ncols);
	    s_kt_list_1d_uint_t list_1 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
	    s_kt_list_1d_uint_t list_2 = init__s_kt_list_1d_uint_t(/*count=*/mat_1->ncols);
	    for(uint i = 0; i < mat_1->ncols; i++) { 	//! Set valeus:
	      {
		const t_float score = mat_1->matrix[data_id][i];
		if(score != T_FLOAT_MAX) {
		  list_1.list[i] = score;
		} else {list_1.list[i] = UINT_MAX;}
	      }
	      {
		const t_float score = mat_2->matrix[data_id_out][i];
		if(score != T_FLOAT_MAX) {
		  list_2.list[i] = score;
		} else {list_2.list[i] = UINT_MAX;}
	      }
	    }
	    //! Comptue the CCM: 
	    t_float scalar_result = 0;
	    const bool is_ok = apply__ccm__twoClusters__hp_api_fileInputTight(enum_id, &list_1, &list_2, &scalar_result);
	    assert(is_ok);
	    //!
	    //! Udpate result:
	    assert(data_id < mat_result->nrows);
	    assert(data_id_out < mat_result->ncols);
	    mat_result->matrix[data_id][data_id_out] = scalar_result;
	    printf("\t\t [%u][%u]=%f, at %s:%d\n", data_id, data_id_out, scalar_result, __FILE__, __LINE__);
	    //!
	    //! De-allcoate:
	    free__s_kt_list_1d_uint_t(&list_1);
	    free__s_kt_list_1d_uint_t(&list_2);
	  }
	}	
      }
    }
