 if( (charOf_group == 0) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_minkowski_euclid;}
 if( (charOf_group == 0) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_minkowski_cityblock;}
 if( (charOf_group == 0) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_minkowski_minkowski;}
 if( (charOf_group == 0) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne;}
 if( (charOf_group == 0) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow3;}
 if( (charOf_group == 0) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow4;}
 if( (charOf_group == 0) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__pow5;}
 if( (charOf_group == 0) && (charOf_groupMember == 7) ) { return e_kt_correlationFunction_groupOf_minkowski_chebychev;}
 if( (charOf_group == 0) && (charOf_groupMember == 8) ) { return e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max;}
 if( (charOf_group == 1) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen;}
 if( (charOf_group == 1) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_Gower;}
 if( (charOf_group == 1) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_Soergel;}
 if( (charOf_group == 1) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_Kulczynski;}
 if( (charOf_group == 1) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_Canberra;}
 if( (charOf_group == 1) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_Lorentzian;}
 if( (charOf_group == 1) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_mean;}
 if( (charOf_group == 1) && (charOf_groupMember == 7) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable;}
 if( (charOf_group == 1) && (charOf_groupMember == 8) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow_variable_zeroOne;}
 if( (charOf_group == 1) && (charOf_groupMember == 9) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2;}
 if( (charOf_group == 1) && (charOf_groupMember == 10) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow3;}
 if( (charOf_group == 1) && (charOf_groupMember == 11) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow4;}
 if( (charOf_group == 1) && (charOf_groupMember == 12) ) { return e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow5;}
 if( (charOf_group == 2) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_intersection_intersection;}
 if( (charOf_group == 2) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_intersection_intersection_altDef;}
 if( (charOf_group == 2) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_intersection_WaveHedges;}
 if( (charOf_group == 2) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_intersection_WaveHedges_alt;}
 if( (charOf_group == 2) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_intersection_Czekanowski;}
 if( (charOf_group == 2) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_intersection_Czekanowski_altDef;}
 if( (charOf_group == 2) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_intersection_Motyka;}
 if( (charOf_group == 2) && (charOf_groupMember == 7) ) { return e_kt_correlationFunction_groupOf_intersection_Motyka_altDef;}
 if( (charOf_group == 2) && (charOf_groupMember == 8) ) { return e_kt_correlationFunction_groupOf_intersection_Kulczynski;}
 if( (charOf_group == 2) && (charOf_groupMember == 9) ) { return e_kt_correlationFunction_groupOf_intersection_Ruzicka;}
 if( (charOf_group == 2) && (charOf_groupMember == 10) ) { return e_kt_correlationFunction_groupOf_intersection_Tanimoto;}
 if( (charOf_group == 2) && (charOf_groupMember == 11) ) { return e_kt_correlationFunction_groupOf_intersection_Tanimoto_altDef;}
 if( (charOf_group == 3) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_innerProduct_innerProduct;}
 if( (charOf_group == 3) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_innerProduct_harmonicMean;}
 if( (charOf_group == 3) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_innerProduct_cosine;}
 if( (charOf_group == 3) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_innerProduct_Jaccard_or_KumarHassebrook;}
 if( (charOf_group == 3) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_innerProduct_Dice;}
 if( (charOf_group == 3) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_innerProduct_Jaccard_altDef;}
 if( (charOf_group == 3) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_innerProduct_Dice_altDef;}
 if( (charOf_group == 3) && (charOf_groupMember == 7) ) { return e_kt_correlationFunction_groupOf_innerProduct_sampleCoVariance;}
 if( (charOf_group == 3) && (charOf_groupMember == 8) ) { return e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian;}
 if( (charOf_group == 3) && (charOf_groupMember == 9) ) { return e_kt_correlationFunction_groupOf_innerProduct_sampleCorrelation_or_brownian__innerRootInDenumerator;}
 if( (charOf_group == 4) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_fidelity_fidelity;}
 if( (charOf_group == 4) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_fidelity_Bhattacharyya;}
 if( (charOf_group == 4) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_fidelity_Hellinger;}
 if( (charOf_group == 4) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_fidelity_Matusita;}
 if( (charOf_group == 4) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_fidelity_Hellinger_altDef;}
 if( (charOf_group == 4) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_fidelity_Matusita_altDef;}
 if( (charOf_group == 4) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_fidelity_squaredChord;}
 if( (charOf_group == 4) && (charOf_groupMember == 7) ) { return e_kt_correlationFunction_groupOf_fidelity_squaredChord_altDef;}
 if( (charOf_group == 5) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_squared_Euclid;}
 if( (charOf_group == 5) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_squared_Pearson;}
 if( (charOf_group == 5) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_squared_Neyman;}
 if( (charOf_group == 5) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_squared_squaredChi;}
 if( (charOf_group == 5) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_squared_probabilisticChi;}
 if( (charOf_group == 5) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_squared_divergence;}
 if( (charOf_group == 5) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_squared_Clark;}
 if( (charOf_group == 5) && (charOf_groupMember == 7) ) { return e_kt_correlationFunction_groupOf_squared_addativeSymmetricSquaredChi;}
 if( (charOf_group == 5) && (charOf_groupMember == 8) ) { return e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic;}
 if( (charOf_group == 5) && (charOf_groupMember == 9) ) { return e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute;}
 if( (charOf_group == 5) && (charOf_groupMember == 10) ) { return e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered;}
 if( (charOf_group == 5) && (charOf_groupMember == 11) ) { return e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncenteredAbsolute;}
 if( (charOf_group == 6) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_shannon_KullbackLeibler;}
 if( (charOf_group == 6) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_shannon_Jeffreys;}
 if( (charOf_group == 6) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_shannon_kDivergence;}
 if( (charOf_group == 6) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_shannon_Topsoee;}
 if( (charOf_group == 6) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_shannon_JensenShannon;}
 if( (charOf_group == 6) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_shannon_JensenDifference;}
 if( (charOf_group == 7) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_combinations_Taneja;}
 if( (charOf_group == 7) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_combinations_KumarJohnson;}
 if( (charOf_group == 7) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock;}
 if( (charOf_group == 8) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedges;}
 if( (charOf_group == 8) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMax;}
 if( (charOf_group == 8) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisWaveHedgesMin;}
 if( (charOf_group == 8) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetric;}
 if( (charOf_group == 8) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_downwardMutability_VicisSymmetricMax;}
 if( (charOf_group == 8) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax;}
 if( (charOf_group == 8) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_downwardMutability_symmetricMin;}
 if( (charOf_group == 9) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;}
 if( (charOf_group == 9) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_rank_kendall_coVariance;}
 if( (charOf_group == 9) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock;}
 if( (charOf_group == 9) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_rank_kendall_Dice;}
 if( (charOf_group == 9) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_rank_kendall_Goodman;}
 if( (charOf_group == 9) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized;}
 if( (charOf_group == 10) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_MINE_mic;}
 if( (charOf_group == 10) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_MINE_mas;}
 if( (charOf_group == 10) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_MINE_mev;}
 if( (charOf_group == 10) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_MINE_mcn;}
 if( (charOf_group == 10) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_MINE_mcn_general;}
 if( (charOf_group == 10) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_MINE_gmic;}
 if( (charOf_group == 10) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_MINE_tic;}
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 0) ) { return e_kt_correlationFunction_groupOf_directScore_direct_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 1) ) { return e_kt_correlationFunction_groupOf_directScore_direct_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 2) ) { return e_kt_correlationFunction_groupOf_directScore_direct_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 3) ) { return e_kt_correlationFunction_groupOf_directScore_direct_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 4) ) { return e_kt_correlationFunction_groupOf_directScore_direct_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 5) ) { return e_kt_correlationFunction_groupOf_directScore_direct_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 6) ) { return e_kt_correlationFunction_groupOf_directScore_direct_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 7) ) { return e_kt_correlationFunction_groupOf_directScore_direct_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 8) ) { return e_kt_correlationFunction_groupOf_directScore_direct_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 9) ) { return e_kt_correlationFunction_groupOf_directScore_direct_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 10) ) { return e_kt_correlationFunction_groupOf_directScore_direct_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 11) ) { return e_kt_correlationFunction_groupOf_directScore_direct_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 12) ) { return e_kt_correlationFunction_groupOf_directScore_direct_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 13) ) { return e_kt_correlationFunction_groupOf_directScore_direct_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 14) ) { return e_kt_correlationFunction_groupOf_directScore_direct_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 15) ) { return e_kt_correlationFunction_groupOf_directScore_direct_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 16) ) { return e_kt_correlationFunction_groupOf_directScore_direct_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 17) ) { return e_kt_correlationFunction_groupOf_directScore_direct_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 18) ) { return e_kt_correlationFunction_groupOf_directScore_direct_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 19) ) { return e_kt_correlationFunction_groupOf_directScore_direct_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 20) ) { return e_kt_correlationFunction_groupOf_directScore_direct_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 21) ) { return e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 22) ) { return e_kt_correlationFunction_groupOf_directScore_direct_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 23) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 24) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 25) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 26) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 27) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 28) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 29) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 30) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 31) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 32) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 33) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 34) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 35) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 36) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 37) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 38) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 39) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 40) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 41) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 42) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 43) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 44) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 45) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__sum_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 46) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 47) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 48) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 49) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 50) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 51) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 52) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 53) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 54) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 55) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 56) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 57) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 58) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 59) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 60) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 61) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 62) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 63) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 64) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 65) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 66) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 67) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 68) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__mul_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 69) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 70) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 71) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 72) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 73) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 74) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 75) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 76) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 77) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 78) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 79) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 80) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 81) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 82) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 83) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 84) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 85) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 86) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 87) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 88) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 89) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 90) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 91) ) { return e_kt_correlationFunction_groupOf_directScore_mergeMatrices__avg_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 92) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 93) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 94) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 95) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 96) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 97) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 98) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 99) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 100) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 101) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 102) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 103) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 104) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 105) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 106) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 107) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 108) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 109) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 110) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 111) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 112) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 113) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 114) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__sum_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 115) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 116) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 117) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 118) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 119) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 120) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 121) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 122) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 123) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 124) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 125) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 126) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 127) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 128) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 129) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 130) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 131) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 132) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 133) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 134) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 135) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 136) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 137) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__mul_sqrt_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 138) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_avg;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 139) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 140) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 141) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 142) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_sum;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 143) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 144) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 145) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divMax_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 146) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_divAbsMinus_sq_abs_minus;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 147) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_mul;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 148) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_headIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 149) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_div_tailIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 150) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_maxIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 151) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_minIsNumerator;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 152) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_min;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 153) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_max;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 154) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_1;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 155) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_useScore_2;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 156) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 157) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 158) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_2log_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 159) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs;}
#endif
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
 if( (charOf_group == 11) && (charOf_groupMember == 160) ) { return e_kt_correlationFunction_groupOf_directScore_symmetricProperty__avg_sqrt_abs_minus;}
#endif
