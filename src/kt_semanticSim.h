#ifndef kt_semanticSim_h
#define kt_semanticSim_h

#include "kt_set_2dsparse.h"
#include "kt_matrix.h"


typedef enum e_trans_global_traverseType {
  //  e_trans_global_traverseType_recursive,
  e_trans_global_traverseType_directUpdate,
  e_trans_global_traverseType_afterChildrenIsProcessed,
  /* e_trans_global_traverseType_none, */
  /* e_trans_global_traverseType_none, */
  //e_trans_global_traverseType_,
  e_trans_global_traverseType_undef
} e_trans_global_traverseType_t;
typedef enum e_trans_global_resultType {
  //  e_trans_global_traverseType_recursive,
  e_trans_global_resultType_directList,
  //e_trans_global_resultType_directList_apiWrapper,
  e_trans_global_resultType_pointerList_apiWrapper, //! here latter repsetns/cpatures the defualt appraoch wrt. libraires we have inveistgated.
  e_trans_global_resultType_none,
  e_trans_global_resultType_undef
  /* e_trans_global_traverseType_none, */
  /* e_trans_global_traverseType_none, */
  //e_trans_global_traverseType_,
} e_trans_global_resultType_t;

/* typedef struct s_trans_node { */
/*   uint id; */
/*   uint pred; */
/* } s_trans_node_t; */
/**
   @struct
   @brief (oekseth, 06. jun. 2017)
 **/
typedef struct s_trans_global_list {
  char *map_vertexIsVisisted;
  s_kt_list_1d_uint_t *map_children; //! children: where we hold knowledge of chidlren-vertex-sets for each.
  s_kt_list_1d_uint_t *map_result; //! Transistive-closures: where we hold knowledge of chidlren-vertex-sets for each.
  s_kt_list_1d_uint_t *map_result_distance; //! Transistive-closures: where we hold knowledge of chidlren-vertex-sets for each.
  s_kt_list_1d_uint_pointer_t *map_result_pointer; //! which is an alterntive appraoch.
  s_kt_list_1d_uint_pointer_t *map_result_pointer_distance; //! which is an alterntive appraoch.
  uint cnt_vertices;
  //! --------------------- 
  e_trans_global_traverseType_t typeOf_resultMerge; //! which latter is used to evlauat ehte improtance of memory-lcoality for different data-topologies ... 
  bool isTo_update_resultSet; //! whivh w set to false too investigate time-effect of this.
  e_trans_global_resultType_t typeOf_resultAccess;
  /* uint id; */
  /* uint pred; */
} s_trans_global_list_t;

//! @reutrn an tinlized veriosn of the s_trans_global_list_t object (oekseth, 06. jun. 2017).
static s_trans_global_list_t init__s_trans_global_list_t(const uint cnt_vertices, e_trans_global_traverseType_t typeOf_resultMerge, const bool isTo_update_resultSet, e_trans_global_resultType_t typeOf_resultAccess) {
  s_trans_global_list_t self;
  //! 
  //! 
  self.cnt_vertices = cnt_vertices;
  self.typeOf_resultMerge = typeOf_resultMerge;
  self.typeOf_resultAccess = typeOf_resultAccess;
  self.isTo_update_resultSet = isTo_update_resultSet;
  // -----------
  self.map_vertexIsVisisted = allocate_1d_list_char(cnt_vertices, false);
  //! 
  //! Allcoate 2d-spase-lists: 
  alloc_generic_type_1d_xmtMemset(s_kt_list_1d_uint_t, (self.map_children), cnt_vertices);
  if(self.isTo_update_resultSet) {
    alloc_generic_type_1d_xmtMemset(s_kt_list_1d_uint_t, (self.map_result), cnt_vertices);
    alloc_generic_type_1d_xmtMemset(s_kt_list_1d_uint_t, (self.map_result_distance), cnt_vertices);
    // ---
    alloc_generic_type_1d_xmtMemset(s_kt_list_1d_uint_pointer_t, (self.map_result_pointer), cnt_vertices);
    alloc_generic_type_1d_xmtMemset(s_kt_list_1d_uint_pointer_t, (self.map_result_pointer_distance), cnt_vertices);
  }
  for(uint i = 0; i < self.cnt_vertices; i++) {
    self.map_children[i] = setToEmpty__s_kt_list_1d_uint_t();
    if(self.isTo_update_resultSet) {
      self.map_result[i] = setToEmpty__s_kt_list_1d_uint_t();
      self.map_result_distance[i] = setToEmpty__s_kt_list_1d_uint_t();
      // ---
      self.map_result_pointer[i] = setToEmpty__s_kt_list_1d_uint_pointer_t();
      self.map_result_pointer_distance[i] = setToEmpty__s_kt_list_1d_uint_pointer_t();
    }
  }
  //! 
  //! 
  //! 
  //! @return 
  return self;
}



/**
   @bief a dummy fucntion to evluat ehte tiemc-sot of our tieraitve-procuedre
   @retunr the number of veritces ivneistted.
 **/
static uint dummyTraversal__s_trans_global_list_t(s_trans_global_list_t *self, const s_kt_list_1d_uint_t *arrOf_startNodes) {
  int current_pos = 0;
  //s_kt_list_1d_uint_t id_parent = setToEmpty__s_kt_list_1d_uint_t();   
  s_kt_list_1d_uint_t id_child = init__s_kt_list_1d_uint_t(self->cnt_vertices * 10);
  //s_kt_list_1d_uint_t id_child = setToEmpty__s_kt_list_1d_uint_t();
  //! 
  //! Add start-nodes:
  uint cnt_verticesEvalaute = 0;
  for(uint i = 0; i < arrOf_startNodes->current_pos; i++) {
    const uint node_id = arrOf_startNodes->list[i];
    { //! Iniate the DFS-criteria:
      const uint cnt_children = self->map_children[node_id].current_pos;
      for(uint i = 0; i < cnt_children; i++) {
	const uint child_id = self->map_children[node_id].list[i];
	//	assert(child_id < self->cnt_vertices);
	if(self->map_vertexIsVisisted[child_id] == false) {
	  self->map_vertexIsVisisted[child_id] = true;
	  //! 
	  //! 'Recursive' call: 
	  id_child.list[current_pos] = child_id;
	  //inline__set_scalar__s_kt_list_1d_uint_t(&id_child, /*index=*/current_pos, child_id);
	  //set_scalar__s_kt_list_1d_uint_t(&id_parent, /*index=*/current_pos, node_id);
	  //! 
	  //! Increment:
	  current_pos++;
	  cnt_verticesEvalaute++;
	}
      }
      /* { //! Set 'stop-critiera': */
      /* 	//!  */
      /* 	//! 'Recursive' call:  */
      /* 	set_scalar__s_kt_list_1d_uint_t(&id_child, /\*index=*\/current_pos, node_id); */
      /* 	set_scalar__s_kt_list_1d_uint_t(&id_parent, /\*index=*\/current_pos, node_id); */
      /* 	//! Increment: */
      /* 	current_pos++; */
      /* } */
      /* push__s_kt_list_1d_uint_t(&id_parent, child_id); */
      /* push__s_kt_list_1d_uint_t(&id_child, child_id); */
      /* current_pos++;  */
    }
  }
  // assert(current_pos > 0);
  //! 
  //! Iterate:
  do {
    //! -----------------------------------------------------------
    //!
    //! Get the identify:
    const uint node_id = id_child.list[current_pos-1]; 
    //    bool isTo_updateResultSet = false;
    /* { */
    /*   const uint parent_id = id_parent.list[current_pos-1];  */
    /*   isTo_updateResultSet = (node_id == parent_id); //! ie, as weh hav ehten 'walked around the clock'. */
    /* } */
    current_pos--;
    //! -----------------------------------------------------------
    //!
    //! A copy-paste from our requsive funciton:
    //if(isTo_updateResultSet == false) 
    {
      const uint cnt_children = self->map_children[node_id].current_pos;
      for(uint i = 0; i < cnt_children; i++) {
	const uint child_id = self->map_children[node_id].list[i];
	if(self->map_vertexIsVisisted[child_id] == false) {	  
	  self->map_vertexIsVisisted[child_id] = true;
	  //! 
	  //! 'Recursive' call: 
	  id_child.list[current_pos] = child_id;
	  /* set_scalar__s_kt_list_1d_uint_t(&id_child, /\*index=*\/current_pos, child_id); */
	  /* set_scalar__s_kt_list_1d_uint_t(&id_parent, /\*index=*\/current_pos, node_id); */
	  //! Increment:
	  current_pos++;
	  cnt_verticesEvalaute++;	  
	}
      }
      /* { //! Set 'stop-critiera': */
      /* 	//!  */
      /* 	//! 'Recursive' call:  */
      /* 	set_scalar__s_kt_list_1d_uint_t(&id_child, /\*index=*\/current_pos, node_id); */
      /* 	set_scalar__s_kt_list_1d_uint_t(&id_parent, /\*index=*\/current_pos, node_id); */
      /* 	//! Increment: */
      /* 	current_pos++; */
      /* } */
    }/*  else { */
    /*   if(self->isTo_update_resultSet) { //! then we udpate thr transitve-closure: */
    /* 	const uint cnt_children = self->map_children[node_id].current_pos; */
    /* 	for(uint i = 0; i < cnt_children; i++) { */
    /* 	  const uint child_id = self->map_children[node_id].list[i]; */
    /* 	  __transClose__recursive__each__list__mergeSets(self, node_id, child_id); */
    /* 	} */
    /*   } */
    /* }y */
  } while(current_pos > 0);
  //! 
  //! De-allocate:
  free__s_kt_list_1d_uint_t(&id_child);
  //free__s_kt_list_1d_uint_t(&id_parent);
  return cnt_verticesEvalaute;
}


//! De-allcoate the "s_trans_global_list_t" object (oekseth, 06. jun. 2017).
static void free__s_trans_global_list_t(s_trans_global_list_t *self) {
  for(uint i = 0; i < self->cnt_vertices; i++) {
    free__s_kt_list_1d_uint_t(&(self->map_children[i]));
    if(self->isTo_update_resultSet) {
      free__s_kt_list_1d_uint_t(&(self->map_result[i]));
      free__s_kt_list_1d_uint_t(&(self->map_result_distance[i]));
      // --
      free__s_kt_list_1d_uint_pointer_t(&(self->map_result_pointer[i]));
      free__s_kt_list_1d_uint_pointer_t(&(self->map_result_pointer_distance[i]));
    }
  }
  free_generic_type_1d(self->map_children);
  if(self->isTo_update_resultSet) {
    free_generic_type_1d(self->map_result);
    free_generic_type_1d(self->map_result_distance);
    // --
    free_generic_type_1d(self->map_result_pointer);
    free_generic_type_1d(self->map_result_pointer_distance);
  }
}

static void __transClose__recursive__each__list__mergeSets(s_trans_global_list_t *self, const uint node_id, const uint child_id) {
  /* const uint cnt_coverage__child = self->map_result[child_id].list_size; */
  /* const uint cnt_coverage__node  = self->map_result[node_id].list_size; */
  if(self->typeOf_resultAccess == e_trans_global_resultType_directList) {
    assert(child_id != UINT_MAX);
    assert(child_id < self->cnt_vertices);
    const uint cnt_coverage__child = self->map_result[child_id].current_pos;
    const bool config__addNewIdentifed_afterInsertion = true; //! where latter is sued to test the 'effect' of our seahc-sapce-splitting: note: seems like this option does Not finlcuence the exeuciont-time, ei, whcih is as sepcted givne teh topology which we expect to handle/address.
    const uint cnt_coverage__node__  = self->map_result[node_id].current_pos;
    //uint min_distance = T_FLOAT_MAX;

    for(uint c = 0; c < cnt_coverage__child; c++) {
      uint cnt_coverage__node = cnt_coverage__node__; if(!config__addNewIdentifed_afterInsertion) {cnt_coverage__node = self->map_result[node_id].current_pos;}
      uint dist_cmp = self->map_result_distance[child_id].list[c]; 	if(dist_cmp == UINT_MAX) { dist_cmp = 0;} 	dist_cmp++;
      const uint v_id = self->map_result[child_id].list[c];
      bool is_found = false;
      for(uint n = 0; n < cnt_coverage__node; n++) {
	const uint n_id = self->map_result[node_id].list[n];
	if(n_id == v_id) {
	  is_found = true;
	  const uint dist_cmp_this = self->map_result_distance[node_id].list[n];
	  const uint min_distance = macro_min(dist_cmp_this, dist_cmp);
	  //! Then ensure that 'this' is udapted with the min-distance attriubte
	  self->map_result_distance[node_id].list[n] = min_distance;
	}
      }
      if(is_found == false) { //! then we add:
	// TODO[perf]: cosndier to first add vertices when 'we have elvauted for all'.
	push__s_kt_list_1d_uint_t(&(self->map_result[node_id]), v_id);
	//! Set the distance:
	uint dist_cmp = self->map_result_distance[child_id].list[c];
	push__s_kt_list_1d_uint_t(&(self->map_result_distance[node_id]), dist_cmp);
      }
    }
    //! Then add the child-id itself: 
    push__s_kt_list_1d_uint_t(&(self->map_result[node_id]), child_id);
    push__s_kt_list_1d_uint_t(&(self->map_result_distance[node_id]), /*dist=*/1);
  } else if(self->typeOf_resultAccess == e_trans_global_resultType_pointerList_apiWrapper) {
    s_kt_list_1d_uint_node_t *node_child = self->map_result_pointer[child_id].lastInserted;
    while(node_child != NULL) {
      s_kt_list_1d_uint_node_t *node_parent = self->map_result_pointer[node_id].lastInserted;      
      bool is_found = false;
      while(node_parent != NULL) {
	if(node_child->key == node_parent->key) {is_found = true;}
	//! Update:
	node_parent = node_parent->link_parent;
      }
      if(is_found == false) { //! then we add:
	// TODO[perf]: cosndier to first add vertices when 'we have elvauted for all'.
	push__s_kt_list_1d_uint_pointer_t(&(self->map_result_pointer[node_id]), node_child->key);
	//! Set the distance:
	s_kt_list_1d_uint_node_t *node_child_dist = self->map_result_pointer_distance[child_id].lastInserted;	
	uint dist_cmp = node_child_dist->key;
	if(dist_cmp == UINT_MAX) { dist_cmp = 0;}
	dist_cmp++;
	push__s_kt_list_1d_uint_pointer_t(&(self->map_result_pointer[node_id]), dist_cmp);
      }
      //! Update:
      node_child = node_child->link_parent;
    }
    //! Then add the child-id itself: 
    push__s_kt_list_1d_uint_pointer_t(&(self->map_result_pointer[node_id]), child_id);
    push__s_kt_list_1d_uint_pointer_t(&(self->map_result_pointer_distance[node_id]), /*dist=*/1);
  }
}


//! An iterative fucntion to comptue transtive-closures.
static void start_transitiveClosureComp__kt_semanticSim(s_trans_global_list_t *self, const s_kt_list_1d_uint_t *arrOf_startNodes) {
  int current_pos = 0;
  s_kt_list_1d_uint_t id_parent = setToEmpty__s_kt_list_1d_uint_t();   s_kt_list_1d_uint_t id_child = setToEmpty__s_kt_list_1d_uint_t();
  //! 
  //! Add start-nodes:
  for(uint i = 0; i < arrOf_startNodes->current_pos; i++) {
    const uint node_id = arrOf_startNodes->list[i];
    { //! Iniate the DFS-criteria:
      { //! Set 'stop-critiera':
	//! 
	//! 'Recursive' call: 
	set_scalar__s_kt_list_1d_uint_t(&id_child, /*index=*/current_pos, node_id);
	set_scalar__s_kt_list_1d_uint_t(&id_parent, /*index=*/current_pos, node_id);
	//! Increment:
	current_pos++;
      }
      const uint cnt_children = self->map_children[node_id].current_pos;
      for(uint i = 0; i < cnt_children; i++) {
	const uint child_id = self->map_children[node_id].list[i];
	assert(child_id < self->cnt_vertices);
	if(self->map_vertexIsVisisted[child_id] == false) {
	  self->map_vertexIsVisisted[child_id] = true;
	  //! 
	  //! 'Recursive' call: 
	  set_scalar__s_kt_list_1d_uint_t(&id_child, /*index=*/current_pos, child_id);
	  set_scalar__s_kt_list_1d_uint_t(&id_parent, /*index=*/current_pos, node_id);
	  //! Increment:
	  current_pos++;
	}
      }
    /* push__s_kt_list_1d_uint_t(&id_parent, child_id); */
    /* push__s_kt_list_1d_uint_t(&id_child, child_id); */
    /* current_pos++;  */
    }
  }
  assert(current_pos > 0);
  //! 
  //! Iterate:
  do {
    //! -----------------------------------------------------------
    //!
    //! Get the identify:
    const uint node_id = id_child.list[current_pos-1]; 
    bool isTo_updateResultSet = false;
    const uint cnt_children = self->map_children[node_id].current_pos;
    if(cnt_children > 0) {
      const uint parent_id = id_parent.list[current_pos-1]; 
      isTo_updateResultSet = (node_id == parent_id); //! ie, as weh hav ehten 'walked around the clock'.
    } else {isTo_updateResultSet = true;} //! ie, as the vertex then 'alreayd' have coverge wrt. its own result.
    current_pos--;
    //! -----------------------------------------------------------
    //!
    //! A copy-paste from our requsive funciton:
    if(isTo_updateResultSet == false) {
      { //! Set 'stop-critiera':
	//! 
	//! 'Recursive' call: 
	set_scalar__s_kt_list_1d_uint_t(&id_child, /*index=*/current_pos, node_id);
	set_scalar__s_kt_list_1d_uint_t(&id_parent, /*index=*/current_pos, node_id);
	//! Increment:
	current_pos++;
      }
      for(uint i = 0; i < cnt_children; i++) {
	const uint child_id = self->map_children[node_id].list[i];
	if(self->map_vertexIsVisisted[child_id] == false) {
	  
	    self->map_vertexIsVisisted[child_id] = true;
	    //! 
	    //! 'Recursive' call: 
	    set_scalar__s_kt_list_1d_uint_t(&id_child, /*index=*/current_pos, child_id);
	    set_scalar__s_kt_list_1d_uint_t(&id_parent, /*index=*/current_pos, node_id);
	    //! Increment:
	    current_pos++;
	  
	}
      }
    } else {
      if(self->isTo_update_resultSet) { //! then we udpate thr transitve-closure:
	const uint cnt_children = self->map_children[node_id].current_pos;
	for(uint i = 0; i < cnt_children; i++) {
	  const uint child_id = self->map_children[node_id].list[i];
	  __transClose__recursive__each__list__mergeSets(self, node_id, child_id);
	}
      }
    }
  } while(current_pos > 0);
  //! 
  //! De-allocate:
  free__s_kt_list_1d_uint_t(&id_child);
  free__s_kt_list_1d_uint_t(&id_parent);
}

#include "correlation_sort.h"

static s_trans_global_list_t init__s_trans_global_list_t(const char *file_name_input, const bool isTo_sortFor__scores) {
  //!
  //! Load the data:
  s_kt_list_1d_pair_t obj_pairs = initFromFile__s_kt_list_1d_pair_t(file_name_input);    
  uint cnt_vertices = 0;
  for(uint i = 0; i < obj_pairs.list_size; i++) {
    const uint head = obj_pairs.list[i].head;
    const uint tail = obj_pairs.list[i].tail;
    assert(head != UINT_MAX);
    assert(tail != UINT_MAX);
    cnt_vertices = macro_max(cnt_vertices, head);
    cnt_vertices = macro_max(cnt_vertices, tail);
  }
  cnt_vertices++;
  //!
  //! Intiate: 
  s_trans_global_list_t obj_tree = init__s_trans_global_list_t(cnt_vertices, e_trans_global_traverseType_directUpdate, 
							       /*isTo_update_resultSet=*/true, //(resultAccess != e_trans_global_resultType_none),
							       e_trans_global_resultType_directList);
  
  s_kt_list_1d_uint_t arrOf_startNodes = setToEmpty__s_kt_list_1d_uint_t();
  {
    s_kt_list_1d_uint_t arrOf_countNodes = setToEmpty__s_kt_list_1d_uint_t();
    //! Step(1.b): Indeitfy the start-nodes;
    for(uint i = 0; i < obj_pairs.list_size; i++) {
      const uint head = obj_pairs.list[i].head;
      const uint tail = obj_pairs.list[i].tail;
      assert(head != UINT_MAX);
      assert(tail != UINT_MAX);
      if(head != tail) {
	increment_scalar__s_kt_list_1d_uint_t(&arrOf_countNodes, tail, /*increment-value=*/1);
	//! Add the relationsuip
	push__s_kt_list_1d_uint_t(&(obj_tree.map_children[head]), tail);
      }
    }
    //! Then 'isnert a s start-nodes' the vertices wihtout any 'ingoing' arcs:
    for(uint i = 0; i < arrOf_countNodes.list_size; i++) {
      const uint cnt_ingoing = arrOf_countNodes.list[i];
      if(cnt_ingoing && (cnt_ingoing != UINT_MAX)) {
	push__s_kt_list_1d_uint_t(&arrOf_startNodes, /*id=*/i);
      }
    }
    //! De-allcoate local list:
    free__s_kt_list_1d_uint_t(&arrOf_countNodes);
  }
  
  /* s_kt_set_2dsparse_t obj_tree = convertFrom__s_kt_list_1d_pair__s_kt_set_2dsparse_t(&obj_pairs); */
  //! De-allcoate hte input:
  free__s_kt_list_1d_pair_t(&obj_pairs);
  //!
  //! Comptye the transitve clsoreus:
  start_transitiveClosureComp__kt_semanticSim(&obj_tree, &arrOf_startNodes);
  //!
  { //! Apply soritng, ie, to recue the comparison-time wrt. our appraoch.
    //! -----------------------
    const bool isTo_sortKeys = (isTo_sortFor__scores == false); //! ie, as we asusemt atht eh 'first mathcing key with the lwoest score' idneities t he convergence-point for the searhcing, eg, wrt. the comtpaution of "least-subsubers' 
    //! -----------------------
#define t_type uint
#define t_type_max UINT_MAX
#define __MiF__memScore_alloc(cnt) ({allocate_1d_list_uint(cnt, empty_0);})
#define __MiF__memScore_free(ptr) ({free_1d_list_uint(ptr);})
#define __MiF__get_rowSize(row_id) ({obj_tree.map_result[row_id].current_pos;})
#define __MiF__get_row(row_id) ({ uint *row = obj_tree.map_result[row_id].list; row;})
#define __MiF__get_apply_sort_forScores() ({quicksort_uint(row_size, /*input=*/row, /*arr_index=*/arr_index, /*arr_result=*/row_tmp_float);})
#define __MiF__get_rowScores(row_id) ({uint *row = obj_tree.map_result_distance[row_id].list; row;})
#define __MiF__get_rowScores_pair(row_id, col_id) ({obj_tree.map_result_distance[row_id].list[col_id]; })
#define __MiF__copyIntoGlobal_rowScores(row_id) ({	memcpy(obj_tree.map_result_distance[row_id].list, arr_index_localScores, sizeof(t_type)*row_size); })
    const uint nrows = obj_tree.cnt_vertices;
    uint matrixOf_scores[1] = {};  //obj_tree.map_result_distance[0].list; // self->matrixOf_scores;
    assert(matrixOf_scores);
  //!
  //! Apply logics:
#include "kt_set_2dsparse__stub__sortMatrix.c"
  }
  
  // sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(&obj_reach, /*isTo_sortKeys=*/false); //! where latter boolean variable is set based on the assumption of "Least Common Subsumer", ie, that 'first select for the same keys' and then 'choose the keys with the smallest overapping values'.

  //! 
  //! Free memory: 
  free__s_kt_list_1d_uint_t(&arrOf_startNodes);
  //free__s_trans_global_list_t(&obj_tree);
  return obj_tree;
}

/**
   @enum e_kt_semanticSim
   @brief provide supprot for sematnic simalrity-measures (oekseth, 06. jun. 2017).
   @remarks the sematnic-siarlmity-eamsures may be sued as a rpe-step wrt. our vertex-based isimalrity-meausres, eg, wrt. our "kt_sparse_sim.h".
 **/
typedef enum e_kt_semanticSim_cmpOutComes {
  e_kt_semanticSim_cmpOutComes_min,
  e_kt_semanticSim_cmpOutComes_max,
  e_kt_semanticSim_cmpOutComes_diff,
  e_kt_semanticSim_cmpOutComes_sum,
  e_kt_semanticSim_cmpOutComes_div,
  e_kt_semanticSim_cmpOutComes_div_minMax,
  // ---- 
  e_kt_semanticSim_cmpOutComes_default, //! ie, we then use the reccomented appraoch.
  // --- 
  e_kt_semanticSim_cmpOutComes_undef
} e_kt_semanticSim_cmpOutComes_t;

/**
   @enum e_kt_semanticSim_adjust
   @brief defined post-adjustmetn-stpes to the sematnic-siamrlity-measures (oesketh, 06. jun. 2017)
 **/
typedef enum e_kt_semanticSim_adjust {
  e_kt_semanticSim_adjust_none,
  /* e_kt_semanticSim_adjust_max,  */
  /* e_kt_semanticSim_adjust_min,  */
  e_kt_semanticSim_adjust_abs,
  e_kt_semanticSim_adjust_log_10, //! large differences are reduced, eg, wrt. Shannon-Jensen-difference
  e_kt_semanticSim_adjust_log_2,
  e_kt_semanticSim_adjust_undef,
} e_kt_semanticSim_adjust_t;

/**
   @enum e_kt_semanticSim_base
   @brief provide supprot for basic sematnic simalrity-measures (oekseth, 06. jun. 2017).
   @remarks the sematnic-siarlmity-eamsures may be sued as a rpe-step wrt. our vertex-based isimalrity-meausres, eg, wrt. our "kt_sparse_sim.h".
 **/
typedef enum e_kt_semanticSim_base {
  //! First a bunch/set of base-semantic-silamrity-metics/meausres: 
  //! Note: the [”elow] $\sum(d(...)$ is used in Ciorpuse-based simalirty-metircs, eg, as exmaplfied in ["https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3864022/"]
  // FIXME[condpetaul]: consider using the "Informaiton Content" (IC) definition foudn in the work of "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3864022/" ... eg, in combiaont with using/comptuign the proability ... 
  e_kt_semanticSim_base_IC_depth, //! ie, infromaion-content .. where we comptue d(coverageCount(head1), coverageCount(head2))
  e_kt_semanticSim_base_IC_score, //! ie, infromaion-content .. where we comptue d(sum-of-scores([head1]), sum-of-scores([head2]))
  // FIXME[article::conceptual]: update our algortihm-artilce wr.t [below] ... ie, wrt. the 'general itnerpations' wrt. the metrics such as "depth(lc(c1, c2))", eg, wr.t the work of "Wu and Palmer": Wu Z, Palmer M. Verbs semantics and lexical selection. Proceedings of the 32nd Meeting of Association of Computational Linguistics; 1994; pp. 33–138. [Ref list]
  e_kt_semanticSim_base_LCS_depth_min, //! ie, least-common-subset/subsumber: the min-depth=(min(d(head1, v), d(head2, v))) where "v" is the lcs-subsumber.
  e_kt_semanticSim_base_LCS_depth_max, //! ie, choose the 'max' instead of the 'min', ie, the 'largest lcs(..)'.
  e_kt_semanticSim_base_LCS_sum,
  //! Note: the [below] "LCS_depth" may be seen as apemrtuaiton of the "shortest-path" atibute/proeprty 
  e_kt_semanticSim_base_spath, //! ie, the "shortest-path", ie, "min(d(row1, row2), d(row2, row1))"
} e_kt_semanticSim_base_t;

/**
   @enum e_kt_semanticSim
   @brief provide supprot for sematnic simalrity-measures (oekseth, 06. jun. 2017).
   @remarks the sematnic-siarlmity-eamsures may be sued as a rpe-step wrt. our vertex-based isimalrity-meausres, eg, wrt. our "kt_sparse_sim.h".
 **/
typedef enum e_kt_semanticSim {
  // ----------------
  // ----------------
  // ----------------
  // ---------------- data-pre-step-routines .... 
  //! 
  //! 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ...
  //! 
  //! 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ...
  //! 
  //! 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... how to include "Evidence Codes" (ECs), eg, wrt. work of \cite{deng2013ppipre} .... eg, wrt. "Wang's Method" \cite{wang2007new}, where latter may efficnelty be comptued by udpating the transitive-closure-metoid-implementaiton with a $score(v_i) = max\{EC_e\ * score(v_k \in child(v_i)}$ comptuation ...  while the latter appraoch may seem evidnet, it is clealry not applied in estlibhesd software-aprpaoches such as \cite{deng2013ppipre} .... 
  // ----------------
  // ---------------- sim-metrics: 
  //! 
  //! 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ...
  //! 
  //! 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... GoSemSim \cite{} ... eg, wrt. "https://bioconductor.org/packages/devel/bioc/vignettes/GOSemSim/inst/doc/GOSemSim.html" ... geneirc measures ... simple to impment
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... \cite{mcinnes2013evaluating}="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3864022/" icte{mcinnes2013evaluating}="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3864022/" ... a set of baisc semantic-metrics ... 
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... "http://atlas.ahc.umn.edu/umls_similarity/similarity_measures.html" ... which provides/present taula immetnatiosn to 'baisc' sematnic-measures 
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... \cite{garla2012semantic}="https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-13-261" 
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... \cite{frohlich2007gosim}="https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-8-166" ... basic sematnic-measures ...  
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... \cite{ehsani2016topoicsim}="https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-016-1160-0" ... both wrt the 'defualt' eamsures .. the 'sum-between-genes' and the uahtors enw emausre (where we for the altter use the \url{http://bigr.medisin.ntnu.no/tools/TopoICSim.R} as a template) ... <-- first impemnt the sim-metrics described in .. \cite{ehsani2016topoicsim} ... \cite{deng2013ppipre}="https://bmcsystbiol.biomedcentral.com/articles/10.1186/1752-0509-7-S2-S8"
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... "https://academic.oup.com/nar/article/38/suppl_1/D244/3112319/FunSimMat-update-new-features-for-exploring" ... $|A \cap B|$, $(|A \cap B|)/min(|A|. |B|)i$, ... and "http://ai2-s2-pdfs.s3.amazonaws.com/923d/b50f7b0978a5ca18fa714db700605c2c5806.pdf" \cite{schlicker2007funsimmat} ... 
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... \cite{wu2013improving}="http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0066745" ... where the latter presnet new semantic-meetrics ... itneresitgnl dicussing diffnere tpermtautiosn of metrics ... which may be used as an arugmetnaion for our parameitrzed-metric-appraoch .... ie, to argue/dmeosntate that our aprpaoch may be used to efifntly explroe new/different hyptoehses wrt. new/possible emtrics for semantic-silmairty .... ie, an software-itnerface to .....??...
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... "https://academic.oup.com/bib/article/doi/10.1093/bib/bbw067/2562801/Gene-Ontology-semantic-similarity-tools-survey-on" ... eg, "IC(x) = -ln(p(x)) = -ln(relativeFrequency(x))" ... 
  // --- 
  // FIXME[code]: ... impelmetn [”elow]
  // FIXME[conpceutal]: ... add/desicrbe meausres found at "https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-14-284" ... eg, wrt. "averaging all the best matches (ABM) for calculating protein functional similarity scores" ... 
  // FIXME[code]: ... 
  // FIXME[code]: ... impelmetn [”elow]
  e_kt_semanticSim_WuP, //! Eq: $2*depth(lcs(c1, c2))/(depth(c1) + depth(c2))$; Cite: Wu Z, Palmer M. Verbs semantics and lexical selection. Proceedings of the 32nd Meeting of Association of Computational Linguistics; 1994; pp. 33–138. [Ref list]
  e_kt_semanticSim_LCh, //! Eq: $-log(spath(c1, c2)/2*D)$, where $D$ describes the global diamater of the evlauated/described ontology/netowrk; Cite: Leacock C, Chodorow M. Combining local context and WordNet similarity for word sense identification. WordNet: An electronic lexical database. 1998;49(2):265–283. [Ref list]
  e_kt_semanticSim_NAm, //! Eq: $log(2 + (minpath(c1, c2)-1))*(D-d)$, where $d$ is "the depth of the concepts' LCS (d)" ; Cite: Nguyen H, Al-Mubaid H. New ontology-based semantic similarity measure for the biomedical domain. Proceedings of the IEEE International Conference on Granular Computing; 2006; pp. 623–628. [Ref list]
  e_kt_semanticSim_Res, //! Eq: $IC(lcs(c1, c2) = -log(P(lcs(c1, c2)))$; Cite: 22. Resnik P. Using information content to evaluate semantic similarity in a taxonomy. Proceedings of the 14th International Joint Conference on Artificial Intelligence; 1995; pp. 448–453. [Ref list]
  e_kt_semanticSim_Lin, //! Eq: $(2*IC(lcs(c1, c2)))/(IC(c1)+IC(c2))$; Cite: 24. Lin D. An information-theoretic definition of similarity. Proceedings of the International Conference on Machine Learning; 1998; pp. 296–304. URL citeseer.ist.psu.edu/95071.html. [Ref list]
  e_kt_semanticSim_JcN, //! Eq: $1/(IC(c1) + IC(c2) - 2*IC(lcs(c1, c2)))$; Cite: Cite(1): 23. Jiang J, Conrath D. Semantic similarity based on corpus statistics and lexical taxonomy. Proceedings on International Conference on Research in Computational Linguistics; 1997; pp. 19–33. [Ref list ... 
  //! --- 
  e_kt_semanticSim_Chen, //! $\sum(A_i \cap B_i)/(A + B)$ ["https://academic.oup.com/bioinformatics/article/23/10/1274/197095/A-new-method-to-measure-the-semantic-similarity-of"]
  //e_kt_semanticSim_

  // FIXMe[code]:  udpate our init-fucntion for the "metric_id_base" wrt. [below] ... eg, for the "LcH(..)" metric ... 
  // FIXMe[conceptaul]: validte correnctess of our "kt_semanticSim__stub__cmpWrapper.c" ... eg, wrt. the 'interptation' of the 'depth(...)' property .... for our "e_kt_semanticSim_WuP"

  // FIXME: consider to 'globally tinatie' a mappign-talb eto hodl teh ifnroiaotin-cotnent ... based on a givne metric ... eg, suign/through [”elow] .... and/or for a rpbaitliy-metirc-set ... cosntrucitn g a enw enum for 'this'
  // e_kt_semanticSim_base_IC_San, //! Eq: $-log( ((|leaves|/|subsumbers(c))+1)/(max\-leaves+1) ) $; Cite: 25. Sánchez D, Batet M, Isern D. Ontology-based information content computation. Knowledge-Based Systems. 2011;24(2):297–303. [Ref list]
  // e_kt_semanticSim_, //! Eq: $$; Cite: 
  // e_kt_semanticSim_, //! Eq: $$; Cite: 
  // e_kt_semanticSim_,
  /* e_kt_semanticSim_, */
  /* e_kt_semanticSim_, */
  e_kt_semanticSim_undef
} e_kt_semanticSim_t;

/**
   @struct s_kt_semanticSim_metric
   @brief idneitifes the semantic-simlairty-emtirc to comptue (oekseth, 06. jun. 2017).
 **/
typedef struct s_kt_semanticSim_metric {
  e_kt_semanticSim_t metric_id;
  e_kt_semanticSim_base_t metric_id_base;
  e_kt_semanticSim_cmpOutComes_t result_cmp;
  e_kt_semanticSim_adjust_t result_cmpPost_adjust;
} s_kt_semanticSim_metric_t;


static s_kt_semanticSim_metric_t initAndReturn__s_kt_semanticSim_metric_t(e_kt_semanticSim_t metric_id) {
  s_kt_semanticSim_metric_t obj_metric;
  obj_metric.metric_id = metric_id;
  // FIXME[cocneptaul]: add if-valeuses to correctly set [”elow]
  obj_metric.metric_id_base = e_kt_semanticSim_base_LCS_sum; 
  obj_metric.result_cmp = e_kt_semanticSim_cmpOutComes_default;
  obj_metric.result_cmpPost_adjust = e_kt_semanticSim_adjust_none;
 
  //! @return
  return obj_metric;
}

#ifndef macro_pluss
#define macro_pluss(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_minus
#define macro_minus(term1, term2) ({term1 - term2;})
#endif
#ifndef macro_mul
#define macro_mul(term1, term2) ({term1 * term2;})
#endif

//! Idnetife a scalar from a pair of valeus in a "s_kt_semanticSim_metric_t" object (oekseth, 06. jun. 2017).
#define MiF__kt_semanticSim__postAdjust_simScore(v1, v2) ({ \
  t_float result = T_FLOAT_MAX;				    \
  if((v1 != 0) && (v2!= 0)) {						\
  if( (obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_min) || (obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_default) ) {result = macro_min(v1, v2);} \
  else if(obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_max) {result = macro_max(v1, v2);} \
  else if(obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_diff) {result = macro_minus(v1, v2);} \
  else if(obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_sum) {result = macro_pluss(v1, v2);} \
  else if(obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_div) {result = (t_float)v1/(t_float)v2;;} \
  else if(obj_metric.result_cmp == e_kt_semanticSim_cmpOutComes_div_minMax) {result = (t_float)macro_min(v1, v2)/(t_float)macro_max(v2, v2);;} \
  else {assert(false);}							\
  /*! Handle post-processing steps:*/					\
  if(obj_metric.result_cmpPost_adjust != e_kt_semanticSim_adjust_none) { \
    if(obj_metric.result_cmpPost_adjust == e_kt_semanticSim_adjust_abs) { result = mathLib_float_abs(result); \
    } else if(obj_metric.result_cmpPost_adjust == e_kt_semanticSim_adjust_log_10) {result = (result != 0) ? mathLib_float_log_abs(result) : T_FLOAT_MAX; \
    } else if(obj_metric.result_cmpPost_adjust == e_kt_semanticSim_adjust_log_2) {result = (result != 0) ? mathLib_float_log_abs(result) / mathLib_float_log(2): T_FLOAT_MAX; \
    } else {assert(false);} /*! ie, as we then need adding support for this enum. */ \
  } \
  } result;})

/**
   @brief comptue semantic-siamrltiy for the tow veritces (oekseth, 06. jun. 2017).
   @return the idneifed semantic-score
   @remarks we do Not provide assert(..) tests: whiel hlatter requries etra awareness of users, the stnegth (of this) is the ptoential/aiblity to aovid asserT(..) perf-lags to reduce perofmranc eof the code-exeuction.
 **/
static t_float semanticSimilairty__xmtAssertTests__s_kt_semanticSim_metric(const s_kt_set_2dsparse_t *self, const uint head_1, const uint head_2, const s_kt_semanticSim_metric_t obj_metric) {  
#define t_type t_float
#define t_type_max T_FLOAT_MAX
#define t_type_min_abs  T_FLOAT_MIN_ABS
  const uint *__restrict__ row_1 = &(self->matrixOf_id[head_1][e_kt_set_sparse_colAttribute_currentPos]);
  const uint *__restrict__ row_2 = &(self->matrixOf_id[head_2][e_kt_set_sparse_colAttribute_currentPos]);
  const t_type *__restrict__ rowOf_scores_1 = self->matrixOf_scores[head_1];
  const t_type *__restrict__ rowOf_scores_2 = self->matrixOf_scores[head_2];
  const uint row_size   = self->matrixOf_id[head_1][2];      const uint row_size_2 = self->matrixOf_id[head_2][2];
  //!
  //! Apply lgoics:
#define __MiB__supportTopologicalMetrics 0
#include "kt_semanticSim__stub__cmpWrapper.c"
  return retVal_result;
}
/**
   @brief comptue semantic-siamrltiy for the tow veritces (oekseth, 06. jun. 2017).
   @return the idneifed semantic-score
   @remarks we do Not provide assert(..) tests: whiel hlatter requries etra awareness of users, the stnegth (of this) is the ptoential/aiblity to aovid asserT(..) perf-lags to reduce perofmranc eof the code-exeuction.
 **/
static t_float semanticSimilairty__manyArgs__xmtAssertTests__s_kt_semanticSim_metric(const uint *__restrict__ row_1, const uint *__restrict__ row_2, const t_float *__restrict__ rowOf_scores_1, const t_float *__restrict__ rowOf_scores_2, const uint n_1, const uint n_2, const s_kt_semanticSim_metric_t obj_metric) {  
#define t_type t_float
#define t_type_max T_FLOAT_MAX
#define t_type_min_abs  T_FLOAT_MIN_ABS
  const uint row_size   = n_1;      const uint row_size_2 = n_2;
  //!
  //! Apply lgoics:
#define __MiB__supportTopologicalMetrics 0
#include "kt_semanticSim__stub__cmpWrapper.c"
  return retVal_result;
}
/**
   @brief comptue semantic-siamrltiy for the tow veritces (oekseth, 06. jun. 2017).
   @return the idneifed semantic-score
   @remarks we do Not provide assert(..) tests: whiel hlatter requries etra awareness of users, the stnegth (of this) is the ptoential/aiblity to aovid asserT(..) perf-lags to reduce perofmranc eof the code-exeuction.
 **/
static t_float semanticSimilairty__manyArgs__uint__xmtAssertTests__s_kt_semanticSim_metric(const uint *__restrict__ row_1, const uint *__restrict__ row_2, const uint *__restrict__ rowOf_scores_1, const uint *__restrict__ rowOf_scores_2, const uint n_1, const uint n_2, const s_kt_semanticSim_metric_t obj_metric) {  
/* #define t_type t_float */
/* #define t_type_max T_FLOAT_MAX */
/* #define t_type_min_abs  T_FLOAT_MIN_ABS */
#define t_type uint
#define t_type_max UINT_MAX
#define t_type_min_abs  0
  const uint row_size   = n_1;      const uint row_size_2 = n_2;
  //!
  //! Apply lgoics:
#define __MiB__supportTopologicalMetrics 0
#include "kt_semanticSim__stub__cmpWrapper.c"
  return retVal_result;
}


/**
   @struct s_kt_semanticSim
   @brief a warpper to faclite logics and post-procesisng steps wr.t semantic-siamlrity on sparse adjcuency-data (oekseth, 06. jun. 2017)
 **/
typedef struct s_kt_semanticSim {
  s_trans_global_list_t obj_traverse;
  t_float *mapVertex_sum;  //! eg, for "Wang's method" \cite{wang2007new}. 
  t_float networkProp_maxDiameter;
  s_kt_semanticSim_metric_t obj_metric;
} s_kt_semanticSim_t;
//! @return an tinalised veirosn of the "s_kt_semanticSim_t" object (oekseth, 06. jun. 2017)
static s_kt_semanticSim_t init__s_kt_semanticSim_t(const char *file_name_input, const s_kt_semanticSim_metric_t obj_metric) {
  s_kt_semanticSim_t self;
  //!
  //! Comptue the rechability-sets: 
  bool isTo_sortFor__scores = true; //! ie, as we then assueme that a sorting for scores produce/give the correct resutls.
  if(obj_metric.metric_id == e_kt_semanticSim_Chen) {
    isTo_sortFor__scores = false; //! ie, as we then asusemt aht the 'the mathcin enties' are to be used.
  }

  self.obj_traverse = init__s_trans_global_list_t(file_name_input, isTo_sortFor__scores);
  // TODO: consider making [”elow] optional .... eg, for use-cases of ....??...
  { //! Allocate space for each vertex: 
    self.networkProp_maxDiameter = 0;
    const uint nrows = self.obj_traverse.cnt_vertices;
    assert(nrows > 0);
    const t_float empty_0 = 0;
    self.mapVertex_sum = allocate_1d_list_float(nrows, empty_0);


    // -------------
    // FIXME[conceptaul::pre-condition]: ... 
    // FIXME[conceptaul::pre-condition]: ... 
    // FIXME[conceptaul::pre-condition]: ... 
    // FIXME[conceptaul::pre-condition::metric::"TCSS"]: ... cosndier to write a deitlad desicpriotn of the progrma-flow in "/home/klatremus/Dokumenter/master_project/hg-bioprosjekt/poset_structure_0.0.0.1/src/externalLibs/networkAnalyse_python_semMetric_TCSS/TCSS" ... ie, if latter is sufifcnet to get out and ariltc e.... 
    // FIXME[conceptaul::pre-condition::metric::"TCSS"]: ... re-implement our 'transivie-closure-algorithm' .... and then use latter to .....??.... 
    // FIXME[conceptaul::pre-condition::metric::"TCSS"]: ... try desicrinb/understanding the TCSS algorithm .... ewrt. graph-sub-divison ... and how simlair/rleated algorithms may be used (eg, Kruskal's MST) .... 
    // FIXME[conceptaul::pre-condition::metric::"TCSS"::code]: ... how \cite{deng2013ppipre} access/maps proteins to correpsodning gnees and sub-grpahs .... eg, if a a MySQL data-base is used/integrated .... 
    // FIXME[conceptaul::pre-condition]: ... ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-11-562"] ... understand + expalin the latter .... eg, wrt. the Resnik and Chen measure .... the work of \cite{deng2013ppipre} .... observes that the TCSS algrotihm consists of two steps: step(1) divide GO into mulipel sub-graphs (using IC as a score-threshold: if roots of differnet sub-graphs has the same IC, then merge the sub-graphs), 
    // FIXME[conceptaul::pre-condition]: ... ["https://bmcsystbiol.biomedcentral.com/articles/10.1186/1752-0509-7-S2-S8"] ... to validte our [above] undersintaindg/interprtaiton .... eg wrt. construciton fo mappgin-tables for GO-terms based on prtoein-anntoatiosn/GOA .... 
    // -------------
    // FIXME[conceptual]: for IC(..) metrics seperately udpate the scores for each vertex .... computing "self.mapVertex_sum[head_1] = -1* mathLib_float_log(self.mapVertex_sum[head_1] / totalSum)".  ... 
    // FIXME[conceptual]: ... 
    // FIXME[conceptual]: ... 
    // FIXME[conceptual]: ... Resnik .... add suppport for 'sorting using the reverse sort-order' .... ie, wher ethe max-elements are the first scores/elements' ... 
    // FIXME[conceptual]: ... Perl-gene-tonolgoy-parsing .... weight different relationships ... both wr.t predicots/rleation-tyeps and evidnece-codes (ECs) .... eg, using the evidence-code-rankning/classifciaont at ["table(1)" in "https://academic.oup.com/bioinformatics/article-lookup/doi/10.1093/bioinformatics/btp122"] .... 
    // FIXME[conceptual]: ... weight different relation-types ... update our Perl-script ... (eg, to include different relationships such as is-a a dn part-of) ... then add supprot for different seleiocnt-funciotns (eg, min(..) or max(..)) ... and an optmion to 'mulity' the scores (eg, as seen for teh Chen metric) ... and/or/latneritvly to use an ordnary 'sum(...)' apporaoch/interptation ....  
    // FIXME[conceptual]: ... consturct a frequency-table for the terms/entities .... consider to use topoglcial distances as weithgts .... ie, where latter proivide an opporutnity for .....??.... 
    // FIXME[conceptual]: 

    //! 
    //! Comptue teh sum of scores for each vertex: 
    for(uint head_1 = 0; head_1 < nrows; head_1++) {
      const uint row_size   = self.obj_traverse.map_result[head_1].list_size;
      // const uint *__restrict__ row_1 = self->obj_traverse.map_result[head_1].list;
      const uint *__restrict__ rowOf_scores_1 = self.obj_traverse.map_result_distance[head_1].list;
      if(row_size > 0) {
	t_float sum = 0;
	for(uint i = 0; i < row_size; i++) {
	  sum += rowOf_scores_1[i];
	  self.networkProp_maxDiameter = macro_max(self.networkProp_maxDiameter, rowOf_scores_1[i]);
	}
	//!
	//! Set score:
	self.mapVertex_sum[head_1] = sum;
      }
    }
  }
  return self;
}
//! De-allcoate the "s_trans_global_list_t" object (oekseth, 06. jun. 2017).
static void free__s_kt_semanticSim_t(s_kt_semanticSim_t *self) {
  free__s_trans_global_list_t(&(self->obj_traverse));
}

//! Comptue sematnic simalrity between all veritces in the set (oekseth, 06. jun. 2017).
//! @return a matrix holind the scores betwene all vertices.
//! @remarks an exampel-use-case cosernsn is seen in the copmaprsion-ork of "https://github.com/sharispe/sm-tools-evaluation" where the latter combien pairwise LCS scores (eg, between 100,000,000 gene-pairs)
static s_kt_matrix_t computeScores__forAll__s_kt_semanticSim_t(const s_kt_semanticSim_t *self) {
  const s_kt_semanticSim_metric_t obj_metric = self->obj_metric;
  const uint nrows = self->obj_traverse.cnt_vertices;
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, nrows);
  for(uint head_1 = 0; head_1 < nrows; head_1++) {
    const uint row_size   = self->obj_traverse.map_result[head_1].list_size;
    const uint *__restrict__ row_1 = self->obj_traverse.map_result[head_1].list;
    const uint *__restrict__ rowOf_scores_1 = self->obj_traverse.map_result_distance[head_1].list;
    if(row_size > 0) {
      for(uint head_2 = 0; head_2 < nrows; head_2++) {
	// --- 
#define t_type uint
#define t_type_max UINT_MAX
#define t_type_min_abs  0
	//! Fetch the data-containers: 
	const uint *__restrict__ row_2 = self->obj_traverse.map_result[head_2].list;
	const uint *__restrict__ rowOf_scores_2 = self->obj_traverse.map_result_distance[head_2].list;
	const uint row_size_2 = self->obj_traverse.map_result[head_2].list_size;
#if(1 == 2)
	const uint alt_row_size   = self->obj_traverse.map_result_distance[head_1].list_size;
	assert(alt_row_size == row_size);
	const uint alt_row_size_2 = self->obj_traverse.map_result_distance[head_2].list_size;
	assert(alt_row_size_2 == row_size_2);
	/* assert(row_size > 0); */
	/* assert(row_size_2 > 0); */
#endif
	if(row_size && row_size_2) { //! then both hav esocres:
	  //!
	  //! Apply logics:
#define __MiB__supportTopologicalMetrics 1
#include "kt_semanticSim__stub__cmpWrapper.c"
	  //!
	  // --- 
	  mat_result.matrix[head_1][head_2] = retVal_result; //! ie, hte reuslt comptued in [ªbove].
	}
      }
    }
  }
  return mat_result;
}

//! Comptue sematnic simalrity, a simlairty-score which is used to update the obj_pairs strucutre witht the scores in the data-set (oekseth, 06. jun. 2017).
static void computeScores__forList__s_kt_semanticSim_t(const s_kt_semanticSim_t *self, s_kt_list_1d_pairFloat_t *obj_pairs) {
  const s_kt_semanticSim_metric_t obj_metric = self->obj_metric;
  for(uint pair_id = 0; pair_id < obj_pairs->list_size; pair_id++) {
    const uint head_1 = obj_pairs->list[pair_id].head;
    const uint head_2 = obj_pairs->list[pair_id].tail;
    //!
    //! Apply lgoics:
    t_float ret_val_local = T_FLOAT_MAX;
#define __MiB__supportTopologicalMetrics 1
#include "kt_semanticSim__stub__simPair.h"
    obj_pairs->list[pair_id].score = ret_val_local;
  }
}


//! Comptue sematnic simalrity, a simlairty-score which is used to update the obj_pairs strucutre witht the scores in the data-set (oekseth, 06. jun. 2017).
//! @returnt he comptued score.
static t_float computeScores__forList__vertexPair__s_kt_semanticSim_t(const s_kt_semanticSim_t *self, const s_kt_list_1d_uint_t *obj_pairs_1, const s_kt_list_1d_uint_t *obj_pairs_2) {
  assert(self);
  assert(obj_pairs_1);
  assert(obj_pairs_2);
  assert(obj_pairs_1->list_size > 0);
  assert(obj_pairs_2->list_size > 0);
  const s_kt_semanticSim_metric_t obj_metric = self->obj_metric;
  //! Note: the [”elow] 'default' proceudre is taken from ["https://academic.oup.com/bioinformatics/article/23/10/1274/197095/A-new-method-to-measure-the-semantic-similarity-of"]. 
  //! FIXME[conceptaul]: try lating [”elow] to CCMs ... ie, as [”elow] 'takes' the max-scores seperately for "matrix[*][i]" and "matrix[i][*]" ... 
#define __MiC__defValue  T_FLOAT_MIN_ABS
#define __MiF__cmpInner macro_max
#define __MiF__cmpOuter macro_pluss 
  //! 
  //! Allocate the result-matrix:
  const t_float empty_0 = 0; 
  t_float **mat_result_transp = allocate_2d_list_float(obj_pairs_2->list_size, obj_pairs_1->list_size, empty_0);
  t_float result_score_1 = 0;
  //t_float result_score = T_FLOAT_MAX;
  for(uint pair_id = 0; pair_id < obj_pairs_1->list_size; pair_id++) {
    const uint head_1 = obj_pairs_1->list[pair_id]; //.head;
    t_float local_score = __MiC__defValue;
    //! Iterate;
    for(uint pair_id_out = 0; pair_id_out < obj_pairs_2->list_size; pair_id_out++) {
      const uint head_2 = obj_pairs_1->list[pair_id]; //.head;
      t_float ret_val_local = T_FLOAT_MIN_ABS;
      if(head_1 != head_2) {
	//!
	//! Apply lgoics:
#define __MiB__supportTopologicalMetrics 1
#include "kt_semanticSim__stub__simPair.h"
	local_score = __MiF__cmpInner(local_score, ret_val_local);
      }
      //! 
      //! Set the transposed score:
      mat_result_transp[pair_id_out][pair_id] = ret_val_local;
    }
    //const uint head_2 = obj_pairs->list[pair_id].tail;
    if(local_score != __MiC__defValue) {result_score_1 = __MiF__cmpOuter(result_score_1, local_score);}
  }
  //!
  //! Comptue for thr transposed: 
  t_float result_score_2 = 0;
  for(uint pair_id_out = 0; pair_id_out < obj_pairs_2->list_size; pair_id_out++) {
    t_float local_score = __MiC__defValue;
    //! Iterate;
    for(uint pair_id = 0; pair_id < obj_pairs_1->list_size; pair_id++) {
      const t_float ret_val_local = mat_result_transp[pair_id_out][pair_id];
      local_score = __MiF__cmpInner(local_score, ret_val_local);
    }
    if(local_score != __MiC__defValue) {result_score_2 = __MiF__cmpOuter(result_score_2, local_score);}
  }
  //!
  //! De-allocate:
  free_2d_list_float(&mat_result_transp, obj_pairs_2->list_size); mat_result_transp = NULL;
  //!
  //! Adjust: 
  t_float result_score = result_score_1 + result_score_2;
  if(result_score != 0) {result_score = result_score / (obj_pairs_1->list_size + obj_pairs_2->list_size);}
  //! ---------------------------------
  //!
  //! @return 
  return result_score;
}

//! 
#endif //! EOF
