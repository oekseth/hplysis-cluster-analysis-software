#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
//#include "db_ds_directMapping.h"
#include "db_inputData_matrix.h"
//#include "db_ds_bTree_rel.h"
#include "kt_matrix.h"
#include "measure_base.h"
/**
   @brief examplfies how logics in "tut_db_useCase__1_normalizeData.c" may be sued for a real-life data-set.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks
   -- for access to the data-file used in this example, please contact senior delveoper [oekseth@gmail.com].
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const uint cnt_heads = 100; const uint cnt_pred = 4; const uint nrows_each = 10; const t_float fraction_syns = 0.2; //! ie, 1/5 = 20 per-cent of the of the vetices 'are overlapping', a case which we observe is an udner-estiamnte for severla database-cases. 
#endif
  const bool config__init__onlyUseNorm = true; //! which if set to false impleis that we 'compare' the xeuciton-time for 'non-data-nroamlizaiton-pre-step' VS 'data-normalization-pre-step'

  const char *fName__rel = "data/skb_large.rel.tsv";
  const char *fName__syn = "data/skb_large.syn.tsv";
  /* //! */
  /* //! Use a gneerliaed funciton to simplifyin building of sample-matrices: */
  /* generateSample__storeInFiles__db_inputData_matrix(cnt_heads, cnt_pred, nrows_each, fraction_syns, fName__rel, fName__syn); */

  //! ----------------------------------------------------------------------------------
  //!
  //! The 'main call': load relationships and inclue into the search-object:  
  for(uint useNorm = 0; useNorm < 2; useNorm++) { //! ie, to investigat ethe time-overhead wrt. 'normalizaiton' VS 'no-noramlizaiton' appraoch: we expect that the nroamlziioant-step has aisngicnfat time-cost when compared to 'data-isneriton-rpcoess' itself (ie, givne the itme-compleixty of oru disjoitn-froeset-algorithm-implementiaotn).
    if(!useNorm && config__init__onlyUseNorm) {continue;} //! ie, as we then assume 'this case' is Not of interest.
    start_time_measurement(); //! ie, start the time-emasurement:
    e_db_ds_directMapping__initConfig_t objConfig = e_db_ds_directMapping__initConfig__synMappings;
    //! Load the data from file, and insert boht the synonysm and relationshisp into the strucutre:
    s_db_ds_directMapping_t obj = loadData__s_db_ds_directMapping__db_inputData_matrix(fName__rel, fName__syn, /*isTo_applyPreNormalizaiotnStep__onSynonyms=*/(bool)useNorm, objConfig);
    { //! Complete time-measurement:
      char str_local[2000] = {'\0'}; sprintf(str_local, "loadFromFile(%s):useNorm=%s", getString__e_db_ds_directMapping__initConfig(objConfig), (useNorm == 1) ? "true" : "false");
      float cmp_time_search = 0;
      const float endTime = end_time_measurement(str_local, cmp_time_search);
      //! Update oru result-set:
    }
    { //! Search: find the diameter (ie, max-path-dsitance) seperately for 'k' different 'start-vertices' ... 'as input' use a two files (for relations and synonyms)
      // FIXME: add seomthing.
    }
    //! De-allocate
    free__s_db_ds_directMapping_t(&obj);
  }
  

  // assert(false); // FIXME: update [ªbove] "tut_db_useCase__1_normalizeData.c" with different search-use-cases usign oru "db_searchNode_rel.h"

  //!
  //! De-allocate:


  //!
  //! @return
#ifndef __M__calledInsideFunction
#undef __Mi__getRandVal
  return true;
#endif
}
