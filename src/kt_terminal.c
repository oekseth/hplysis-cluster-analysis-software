#include "kt_terminal.h"
#include "kt_distance.h"
#include "kt_clusterAlg_hca.h"
#include "parse_main.h"
#include "kt_api.h"

//! @returnt the enum assicated ot the styring.
const e_kt_terminal_whenToMerge_t get_enumFromString__e_kt_terminal_whenToMerge_t(const char *stringOf_enum) {
  if( 0 == strncmp(stringOf_enum, "beforeFilter", macro_min(strlen(stringOf_enum), strlen("beforeFilter")))) { return e_kt_terminal_whenToMerge_beforeFilter;}
  else if( 0 == strncmp(stringOf_enum, "afterFilter", macro_min(strlen(stringOf_enum), strlen("afterFilter")))) { return e_kt_terminal_whenToMerge_afterFilter;}
  //  else if( 0 == strncmp(stringOf_enum, "beforeClustering", macro_min(strlen(stringOf_enum), strlen("beforeClustering")))) { return e_kt_terminal_whenToMerge_beforeClustering;}
  else if( 0 == strncmp(stringOf_enum, "inClustering", macro_min(strlen(stringOf_enum), strlen("inClustering")))) { return e_kt_terminal_whenToMerge_inClustering;}
  //! ---------------------------------------------------------------------------

  //!
  //! Warn if the user-configuraiton was not properly supported:
  fprintf(stderr, "!!\t None of the if-clauses matched for input-string=\"%s\", ie, please investigate your call: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_enum, __FUNCTION__, __FILE__, __LINE__);
  return e_kt_terminal_whenToMerge_inClustering;
}

//! Initaite the s_kt_terminal_t object to default values.
void init__s_kt_terminal_t(s_kt_terminal_t *self) {
  assert(self);
  self->object_is_usedInternally_resultdoesThereforeNotNeedBeExported = false;
  setTo_empty__s_kt_matrix_t(&(self->inputMatrix_1));
  setTo_empty__s_kt_matrix_t(&(self->inputMatrix_2));
  setTo_empty__s_kt_matrix_t(&(self->inputMatrix_result));
  self->nrows = 0;   self->ncols = 0;
  self->isTo_transposeMatrix = false;
  self->stringOf_input_file_1 = NULL;   self->stringOf_input_file_1_mask = NULL; self->stringOf_input_file_1_weightList = NULL;
  self->stringOf_input_file_2 = NULL;   self->stringOf_input_file_2_mask = NULL; self->stringOf_input_file_2_weightList = NULL;
  self->isTo_readData_fromStream = false; self->isTo_include_stringIdentifers_in_matrix = false;
  self->stringOf_sampleData_type = NULL;
  self->stringOf_sampleData_type_secondMatrix = NULL;
  self->stringOf_sampleData_type_realLife = NULL;
  self->fractionOf_toAppendWith_sampleData_type_rows = 0;   self->fractionOf_toAppendWith_sampleData_type_columns = 0;
  self->stringOf_resultFile = NULL; self->result_format = NULL;
  self->isTo_inResult_isTo_useJavaScript_syntax = false;
  self->isTo_inResult_isTo_useJSON_syntax = false;
  self->isTo_inResult_isTo_exportInputData = false;
  self->whenToMerge_differentMatrices = e_kt_terminal_whenToMerge_afterFilter;

  setTo_default__s_kt_correlationMetric_t(&(self->metric_beforeFilter));
  setTo_default__s_kt_correlationMetric_t(&(self->metric_afterFilter));
  setTo_default__s_kt_correlationMetric_t(&(self->metric_insideClustering));
  //! ---
  setTo_empty__s_kt_matrix_filter_t(&(self->filter_middleOf_correlationApplication));
  self->isTo_applyDataFilter = false; self->isTo_applyDataAdjust = false;
  //! ---
  self->isToApply_correlation_beforeFilter = false;
  self->isToApply_correlation_afterFilter = false;
  self->isToApply_correlation_beforeFilter_includeInResult = false;
  self->isToApply_correlation_afterFilter_includeInResult = false;

  init__s_kt_clusterResult_config_t(&(self->clusterConfig));

}

//! De-allcoate the s_kt_terminal_t object.
void free__s_kt_terminal_t(s_kt_terminal_t *self) {
  assert(self);
  
  { //! Handle overlapping regions wrt. memory:
    if(self->inputMatrix_1.matrix == self->inputMatrix_2.matrix) {
      self->inputMatrix_2.matrix = NULL;
    }
    if(self->inputMatrix_1.matrix == self->inputMatrix_result.matrix) {
      self->inputMatrix_result.matrix = NULL;
    }
    //! ---- 
    if(self->inputMatrix_1.weight == self->inputMatrix_2.weight) {
      self->inputMatrix_2.weight = NULL;
    }
    if(self->inputMatrix_1.weight == self->inputMatrix_result.weight) {
      self->inputMatrix_result.weight = NULL;
    }
    //! ---- 
    if(self->inputMatrix_1.nameOf_rows == self->inputMatrix_2.nameOf_rows) {
      self->inputMatrix_2.nameOf_rows = NULL;
    }
    if(self->inputMatrix_1.nameOf_rows == self->inputMatrix_result.nameOf_rows) {
      self->inputMatrix_result.nameOf_rows = NULL;
    }
    //! ---- 
    if(self->inputMatrix_1.nameOf_columns == self->inputMatrix_2.nameOf_columns) {
      self->inputMatrix_2.nameOf_columns = NULL;
    }
    if(self->inputMatrix_1.nameOf_columns == self->inputMatrix_result.nameOf_columns) {
      self->inputMatrix_result.nameOf_columns = NULL;
    }
  }
  free__s_kt_matrix(&(self->inputMatrix_1));
  free__s_kt_matrix(&(self->inputMatrix_2));
  free__s_kt_matrix(&(self->inputMatrix_result));
  //! ---
  free__s_kt_matrix_filter_t(&(self->filter_middleOf_correlationApplication));
}

//! Export the data-sets to a singular file, ie, for each of the matrices 'which has data' (oekseth, 06. des. 2016).
//! @return true if no internel in-cosnsitencies were dsicovered.
//! @remarks to reduce the compelxtiy wwrt. data-marging we write out seperate fiels for the differnet result-cases:
bool exportDataSets__toACombinedFile__kt_terminal(s_kt_terminal_t *self, const char *stringOf_dataLabel, const bool isTo_useMatrixFormat) {
  assert(self); assert(stringOf_dataLabel); assert(strlen(stringOf_dataLabel));   
  uint cnt_objs_toExport = 0;
  if(self->inputMatrix_result.nrows) {cnt_objs_toExport++;}
  if(self->inputMatrix_1.nrows) {cnt_objs_toExport++;}
  if(self->inputMatrix_2.nrows) {cnt_objs_toExport++;}
  

  // printf("-------------- cnt_objs_toExport=%u, at %s:%d\n", cnt_objs_toExport, __FILE__, __LINE__);

  if(cnt_objs_toExport > 0) {
    //! --------------------
    //!
    //! Initate:
    const char *stringOf_id = stringOf_dataLabel; //"inputData";
    const char *suffix = "tsv";
    if(self->isTo_inResult_isTo_useJavaScript_syntax) {
      suffix = "js";
    } else if( self->isTo_inResult_isTo_useJSON_syntax) {
      suffix = "json";
    } 
    const char *stringOf_resultFile = self->stringOf_resultFile;
    if(stringOf_resultFile && !strlen(stringOf_resultFile)) {
      stringOf_resultFile = "result_hpLysis";
      fprintf(stderr, "!!\t Note: the file-prefix for the result-fiels were Not set: we therefore use the prefix=\"%s\" when generating your result-files: for questions pelase cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
    }
    assert(strlen(stringOf_resultFile) < 1000); //! ie, givne [”elow] assumption
    char local_stringOf_resultFile[1300] = {'\0'}; 
    bool isTo_useStream = true;
    if(stringOf_resultFile && strlen(stringOf_resultFile)) {
      sprintf(local_stringOf_resultFile, "%s.%s.%s", stringOf_resultFile, stringOf_dataLabel, suffix);
      isTo_useStream = true;
    }

    //! Update the object:
    export_config_extensive_s_kt_matrix_t(&(self->inputMatrix_result), 
					  (isTo_useStream) ? NULL : local_stringOf_resultFile,
					  isTo_useMatrixFormat, self->isTo_inResult_isTo_useJavaScript_syntax, self->isTo_inResult_isTo_useJSON_syntax, /*matrix-variable-suffix=*/stringOf_id, (bool)(stringOf_id != NULL), /*cnt_dataObjects_toGenerate=*/cnt_objs_toExport); 

    
    // printf("\n\n # exports the input-matrix to file=\"%s\", at %s:%d\n", (isTo_useStream) ? "<use-stdout>" : local_stringOf_resultFile, __FILE__, __LINE__);
      
    //export_config__s_kt_matrix_t(&(self->inputMatrix_result), local_stringOf_resultFile);

    //! --------------------
    //!
    //! Export:
    if(self->inputMatrix_result.nrows > 0) {
      assert(self->inputMatrix_result.ncols > 0);
      export__dataObj_seperateFromFileObj__s_kt_matrix_t(&(self->inputMatrix_result), &(self->inputMatrix_result), /*stringOf_header=*/"data_result");
    }
    if(self->inputMatrix_1.nrows > 0) {
      assert(self->inputMatrix_1.ncols > 0);
      export__dataObj_seperateFromFileObj__s_kt_matrix_t(&(self->inputMatrix_result), &(self->inputMatrix_1), /*stringOf_header=*/"matrix_1");
    }
    if(self->inputMatrix_2.nrows > 0) {
      assert(self->inputMatrix_2.ncols > 0);
      export__dataObj_seperateFromFileObj__s_kt_matrix_t(&(self->inputMatrix_result), &(self->inputMatrix_2), /*stringOf_header=*/"matrix_2");
    }

      
    //! --------------------
    //!
    //! Close:
    closeFileHandler__s_kt_matrix_t(&(self->inputMatrix_result));
  } //! else we asusme all corr-matrices are empty.
  //! ---------------
  //!
  //! At this exeuciton-point we asusem the oepraion was a scuccess.
  return true;
}


static const char *get_subString_fromString_ifSet(char *string, const char *str_needle) {
  assert(string); assert(strlen(string)); assert(str_needle); assert(str_needle);

  // printf("compare: \"%s\" VS \"%s\", at %s:%d\n", string, str_needle, __FILE__, __LINE__);

  if(NULL != strstr(string, str_needle) ) {
  //  if(NULL != strcasestr(string, str_needle) ) {
    const char *tmp_string = string + strlen(str_needle);

    //printf("compare: \"%s\" VS \"%s\", at %s:%d\n", string, str_needle, __FILE__, __LINE__);

    //assert(strlen(tmp_string) > 0);
    
    //! @return the string:
    return tmp_string;
  } else {return NULL;}
}

//! @return an itneger from a parsed string:
static t_float get_float_fromString(char *string, const char *str_needle) {
  assert(string); assert(strlen(string)); assert(str_needle); assert(str_needle);
  if(NULL != strstr(string, str_needle) ) {
    const char *tmp_string = string + strlen(str_needle);
    const t_float val = (t_float)atof(tmp_string);

    //! @return the result:
    return (t_float)val;
  } else {return T_FLOAT_MAX;}
}


//! @return an itneger from a parsed string:
static uint get_number_fromString(char *string, const char *str_needle) {
  assert(string); assert(strlen(string)); assert(str_needle); assert(str_needle);
  if(NULL != strstr(string, str_needle) ) {
    //  if(NULL != strcasestr(string, str_needle) ) {
    const char *tmp_string = string + strlen(str_needle);
    if( (tmp_string[0] != ' ') && (tmp_string[0] != '\0') ) {
    
      //errno = 0;    /* To distinguish success/failure after call */
      //char *endptr;
      const int val = atoi(tmp_string);
      //const long val = strtol(tmp_string, &endptr, /*base=*/10);
    
      /* if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))  || (errno != 0 && val == 0)) { */
      /*   perror("strtol"); */
      /*   exit(EXIT_FAILURE); */
      /* } */
    
      /* if (endptr == tmp_string) { */
      /*   fprintf(stderr, "No digits were found\n"); */
      /*   exit(EXIT_FAILURE); */
      /* } */
      if(val == LONG_MIN) {
	assert(false);
      } else if(val == LONG_MAX) {
	assert(false);
      }
     
      //! Then return:
      assert(val >= 0);
      //! @return the result:
      return (uint)val;
    } else {return 1;} //! ie, as we then assume the option is a 'boolean true option'
  } else {return UINT_MAX;}
}

//#include "mine_wrapper.c"
//! Include the funciton which comptue the cell-data:
//! NotE: mvoed intoa seperate fucntion to simplify itnegration with otehr languages.
//#include "mine_matrix.c" //! which include the "compute_2d_scoreMatrix(...)" funciton.


//! From the configuraiton-object perform the cluster-analysis, eg, wrt. appluication of correlation-metics and k-means clustering.
int hpLysis_computeCorrelations__kt_terminal(s_kt_terminal_t *self) {
  assert(self);

  
  //! -------------------------------
  //!
  //! Initiate local varialbes for the "kt_terminal__stub__parse__input.c":
  const bool needToMerge_results = (self->stringOf_input_file_2 != NULL);
  bool needToMerge_results_areMerged = (needToMerge_results == false);
  uint cnt_dataObjects_toGenerate = 0; 
  bool isTo_useMatrixFormat = true;
  //uint cnt_dataObjects_written = 0;

  //! -----------------------------------------------------
  //!
  //! Apply logics to parse both the arguments and the data:
#include "kt_terminal__stub__parse__input.c"
  

  //! -----------------------------------------------------
  //!
  //! Combine matrix-fitlers and correlation-analsyssi for the data-set(s):
#include "kt_terminal__stub__parse__correlationAndFilter.c"

 
  //! -----------------------------------------------------------------------
  //!
  //! ##                   cluster analysis                                ## 
  const e_kt_terminal_clusterResult__format_t resultFormat = get_enumType__e_kt_terminal_clusterResult__format_t(self->isTo_inResult_isTo_useJavaScript_syntax, self->isTo_inResult_isTo_useJSON_syntax, isTo_useMatrixFormat);
  const bool is_ok = performClusterAnalysis__andGenerateResults__s_kt_clusterResult_config_t(&(self->clusterConfig), &(self->inputMatrix_result), self->metric_insideClustering, resultFormat, /*resultPrefix=*/self->stringOf_resultFile);
  assert(is_ok); //! ie, what we expect.

  return is_ok;
}

//! Relate the string-input-params to an internal cofniguraiton-object (oekseth, 06. des. 2016).
//! @return an intaited object of the configuraitons assicated to the tmerinal-inptu-params
//! @remarks the first input-arguemtn is assumed to be the tmerinal-software-name, ie, for which we ingore the first tmerinal-input-argument
s_kt_terminal_t parse_cmdTerminalInputAgs__kt_terminal(char **array, const int array_cnt, bool *mandaroryInputParams_are_set) {
  s_kt_terminal_t self; init__s_kt_terminal_t(&self);


#ifndef NDEBUG
  { //! Write out the call 'used in this':
    //! Note: this 'option' is (among others) used to simplify the 'dbirdge' between out test-code-equence and the terminal-bash-examples during debughggin (oekseth, 06. des. 2016).
    printf("(info)\t We now exuecte the hpLysis software based on:\t ");
    for(uint i = 1; i < array_cnt; i++) { //! ie, as we start at index=1 iot. 'avoid' testing/evlauating the name of the software.
      fprintf(stdout, "%s ", array[i]);
    }
    printf("\t # for questions wrt. the latter see %s:%d\n", __FILE__, __LINE__);
  }
#endif  

  //! ---------------------------------------------------------
  //! Iterate through the variables provided in the users call:
  uint cnt_updated = 0;  uint cnt_inputParamsData_matched = 0; uint cnt_inputParamsData_matched_syntetic = 0;
  for(uint i = 1; i < array_cnt; i++) { //! ie, as we start at index=1 iot. 'avoid' testing/evlauating the name of the software.
    
    t_float updated_float = 0; uint updated_number = 0;
    const char *subString = NULL; //    FILE *stream_tmp = NULL; 

#include "autoGenerated_hpLysisTerminal__cmdParser.c"	   

    { //! Parse noise-inclusion-paraemters:
      const char *stringOf_sampleData_type_tmp = get_subString_fromString_ifSet(array[i], "-sample-data-distribution=");
      if(stringOf_sampleData_type_tmp) {
	// printf("\t stringOf_sampleData_type=\"%s\", at %s:%d\n", stringOf_sampleData_type_tmp, __FILE__, __LINE__);
	self.stringOf_sampleData_type = stringOf_sampleData_type_tmp;  //! where the 'if-clause' is used to avoid clearing 'earlier matches'. 
	cnt_inputParamsData_matched_syntetic++;
      }
      updated_number = get_number_fromString(array[i], "-percent-toAppendWithSampleDistributionFor-rows="); //! where "i" --> "intrisnitics"
      if(updated_number != UINT_MAX) {self.fractionOf_toAppendWith_sampleData_type_rows = updated_number; cnt_updated++;}
      updated_number = get_number_fromString(array[i], "-percent-toAppendWithSampleDistributionFor-columns="); //! where "i" --> "intrisnitics"
      if(updated_number != UINT_MAX) {self.fractionOf_toAppendWith_sampleData_type_columns = updated_number; cnt_updated++;}
      /* const char *stringOf_sampleData_type_tmp = get_subString_fromString_ifSet(array[i], "-sample-data-distribution="); */
      /* if(stringOf_sampleData_type_tmp) { */
      /*   // printf("\t stringOf_sampleData_type=\"%s\", at %s:%d\n", stringOf_sampleData_type_tmp, __FILE__, __LINE__); */
      /*   stringOf_sampleData_type = stringOf_sampleData_type_tmp;  //! where the 'if-clause' is used to avoid clearing 'earlier matches'.  */
      /*   cnt_inputParamsData_matched_syntetic++; */
      /* } */
    
      const char *stringOf_sampleData_type_realLife_tmp = get_subString_fromString_ifSet(array[i], "-sample-data-distribution-realLife=");
      if(stringOf_sampleData_type_realLife_tmp) {
	// printf("\t stringOf_sampleData_type_realLife=\"%s\", at %s:%d\n", stringOf_sampleData_type_realLife_tmp, __FILE__, __LINE__);
	self.stringOf_sampleData_type_realLife = stringOf_sampleData_type_realLife_tmp;  //! where the 'if-clause' is used to avoid clearing 'earlier matches'. 
	// cnt_inputParamsData_matched_syntetic++;
      }
    }
  }
  
  //assert(config_est <= 1); //! where config_est is have macro-value "EST_MIC_APPROX" or macro-value "EST_MIC_E"



  //if(!cnt_inputParamsData_matched && !cnt_inputParamsData_matched_syntetic) {
  if(self.object_is_usedInternally_resultdoesThereforeNotNeedBeExported == false) {
    if(!self.result_format || !strlen(self.result_format)) { //! then it imight be that a user had dofortten to specify the input-dat, ie, we then print a warning 'giving the use an heads-up'.
      printf("(info)\t no configuraitons-options provided wrt. the \"result\" parameter-option. Therefore make use of internal syntactic data to evaluate your results. For questions please cotnact the dveloepr at [oekseth@gmail.com]. Observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    }
  }
  //  }


  //! Then update the configurations:
  if(cnt_updated != 0) {  *mandaroryInputParams_are_set = true;
  } else { *mandaroryInputParams_are_set = false; }
  return self;
}

/**
   @brief relate the terminal-bashs-string-parameters to Knitting-Tools hpLysis-software (oekseth, 06. opkt. 2016)
   @remarks is a terminal-based front-end to our hpLysis software: 
   - handle both configuraiton-string-parsing, euxeciton of algorithms and resutl-generationi
 **/
int add_params__kt_terminal(int array_cnt, char **array) {
  if(array_cnt == 1) { //! then no input-parameters have been provided: generate a informative erorr-message, and then abort the exuection:
    //printf("(info)\t command-line-options were specified wrt. the inptu-data. In below we list the configuration-options:\n");,
    // TODO: if ["elow] is updatd, then remember to also update our wiki 'throgu' our "bashHelp_toWiki.pl" parsser (oekseth, 06. aug. 2016).
    printf(
#include "autoGenerated_hpLysisTerminal__help.c"	   
	   "\n"	   
	   "\n"	   

	   "Special syntax:\n"  // ie, list a set of examples:
	   "----------------------------------\n"
	   "<>\t describes an either-or option: if you do Not specify an option then the option is assumed to 'not be used; an example concerns the \"help--printAll-correlationMetrics=\" option, which is 'accepted' as long as you include the '=' sign after the argument; \n"
	   "<0|1>\t similar to the \"<>\" option, with difference that you may explicitly set an option to 'not be used' (ie, where the \"<>\" is a 'syntactic sugar' for the \"<0|1>\" option);\n"
	   "<0, .....>\t the value need to be a positive number without decimal-places: an example concerns the specification of column-numbers in your input-file (where 'setting' 5.4 columns would indicate errors in your results, ie, hence the need to write numbers without decimals);"
	   "<1, .....>\t similar to \"<0, .....>\" with different that a '0' is not accepted as an input;\n"

	   "\n"	   
	   "\n"	   


	   "Examples:\n"  // ie, list a set of examples:
	   "----------------------------------\n"
	   // FIXME: add inptu-lgoic-support for [below] ... ie, to (a) write a warning of no input-set is provided and therafter use a 'random' input-data-set.
	   "./x_hpLysis -cnt-rows=100 -cnt-cols=8000" "\t\t# compute the HpLysis-correlations for a syntetic data-set with 100 rows and 8,000 features;\n"
	   "./x_hpLysis -result-format=matrix -exportInto-javaScript-syntax=1 -input-file=sample_data/sample_rs_testData.tsv -before-filter-correlation-type-isTo-export \t\t# compute results and export the correlation-matrix into a Java-Script matrix-format;\n"
	   //"\t\t#"
	   "./x_hpLysis result-format=matrix -result-isTo-export-input-file=1 -input-file=sample_data/lib_mine.knittingTools.tsv -result-isTo-exportInputFile-1" "\t\t# compute the HpLysis-correlation-scores, and include the input-data in the result-data-set;\n"
	   "./x_hpLysis -result-format=matrix -result-isTo-export-input-file=1 -input-file=sample_data/lib_mine.knittingTools.tsv -result-isTo-exportInputFile=1 -percent-toAppendWithSampleDistributionFor-rows=150 -sample-data-distribution=uniform" "\t\t# compare signficance of correlation-scores through inclusion of valeus with uniform distribution;\n"
"./x_hpLysis -result-format=matrix -result-isTo-export-input-file=1 -input-file=sample_data/lib_mine.knittingTools.tsv -result-isTo-exportInputFile=1 -percent-toAppendWithSampleDistributionFor-rows=150 -percent-toAppendWithSampleDistributionFor-columns=30 -sample-data-distribution=uniform" "\t\t# introduce noise into both the columns and rows;\n"
	   "./x_hpLysis_fast_singleThreaded -cnt-rows=16 -cnt-cols=2000 -sample-data-distribution=random -result-format=relation -before-filter-correlation-type-isTo-export" "\t\t# comptue the hpLysis-correlation-metric-scores for a random data-set;\n"
	   "./x_hpLysis_fast_singleThreaded -cnt-rows=16 -cnt-cols=2000 -sample-data-distribution=random -result-format=relation -hca_method=single" "\t\t# comptue the single-linkage dendograms for a random data-set;\n"
	   "./x_hpLysis -cnt-rows=4 -cnt-cols=10 -sample-data-distribution=sinus -exportInto-javaScript-syntax=1" "\t\t# infer HpLysis-correlation-scores for Sinusoidal relationships, and then store the relationships into a Java-Script-compliant syntax;\n"
	   "cat sample_data/sample_overlappingLinex.tsv |./x_hpLysis -c=1 -input-use-stream-explicit-size=1 -exportInto-javaScript-syntax=1  -cnt-cols=100 -cnt-rows=100" "\t\t# exemplifies how input may be 'piped', eg, as part of a tool-chain: note in this context that the max-row-size and max-column-size need to be specified (and where the size of each string-identifier is expected to be less than 100 chars);\n"

	   "\n"

	   "The software has been developed by Ole Kristian Ekseth [oekseth@gmail.com] et al.: to simplify usage and data-interpretation a web-based interface has been developed: see \"http://knittingTools.org/gui_lib_hpLysis.cgi\". For an up-to-date version of this software please see \"https://bitbucket.org/hplysis-cluster-analysis-software/\". As always feedbacks would be appreciated.\n"
	   "\n"
	   /*
	     FIXME: include [below] examoples when we have validated that they work as they should.
	     "./x_test_getNodeList_client -f data/step_1_queries/get_NTCs_ex1.tsv" "\t\t# which get the complete set of vertex-classificaitons (NTCs) for all vertices in the SKB, using an optimized procedure"
	     "\n"
	     "./x_test_getNodeList_client -f data/step_1_queries/get_NTCs_ex2.tsv" "\t\t# which extends the aboe NTC-example to return (in the result) both the set of vertices matchin a given pattern, and the NTC-anntoations (for each vertex labeled with an NTC)."
	     "\n"
	     "./x_test_getNodeList_client -f data/step_1_queries/explictGetAll_relationTypes.tsv" "\t\t# Get the complete set of relation-types in the SKB."
	     "\n"
	     "./x_test_getNodeList_client -f data/step_1_queries/explictGetAll_rtQueryClasses.tsv" "\t\t# Get the complete set of rt-query-classes in the SKB."
	     "\n"
	     "./x_test_getNodeList_client -f data/step_1_queries/rtQueryClasses_specificSubset.tsv" "\t\t# Places the queries performed without a topological context in the context of a contextual query, examplified through the investigation of rt-query-classes at various 'resolutions'"
	     "\n"
	     // FIXME: for the ["elow] example validate that 'stdin' does not capture the set of inptu-arguments.
	     "echo \"wnt\tregex;activate\t*;1\" \| ./x_test_getNodeList_client -s" "\t\t# PArse queries from a stream, thereby aoviding the need to first construct/generate a file contianing the query: of (special) interest when the client-code is bundlded/including in other software as part of their pipelines; for an example, see our \"standAlone_client.pm\" Perl module."
	     "\n"

	   */
	   );
      

    return false;
  }

  
  bool mandaroryInputParams_are_set;
  s_kt_terminal_t self = parse_cmdTerminalInputAgs__kt_terminal(array, array_cnt, &mandaroryInputParams_are_set);
  if(mandaroryInputParams_are_set) {
    //! From the configuraiton-object perform the cluster-analysis, eg, wrt. appluication of correlation-metics and k-means clustering.
    const int ret_val = hpLysis_computeCorrelations__kt_terminal(&self);
    free__s_kt_terminal_t(&self);
    return ret_val;
  }


  return 1;
}
  


