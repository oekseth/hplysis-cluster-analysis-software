#ifndef hp_api_ccm_h
#define hp_api_ccm_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

//#include "kt_centrality.h"
#include "hp_ccm.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "kt_list_2d.h" //! which among others define the "s_kt_list_2d_kvPair_t" struct.

//#include "hp_sim_dense.h"

/**
   @struct s_hp_api_ccm_config
   @brief provide a configuraiton interface for idnetifying data noramlizaiton scores (oekseth, 06. mar. 2018).
 **/
typedef struct s_hp_api_ccm_config {
  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_matrix; //! ccm-matrix
  e_hpLysis_clusterAlg_t matrix_clusterAlg_preStep; 
  e_kt_matrix_cmpCluster_metric_t ccm_gold;
  e_kt_normType_t enum_preStep_norm;
  bool matrix_isTo_use_KD_treeInSimMetricComputations;
  bool matrix_simMetric_rankPreStep;
  /**
     // FIXME: write a detaield usage guide ... using [below]:

     #! 

     #! 


     #! 

     
     #! Construct comparison for all files in a given directory

     #! 

   **/
  /* uint config_cntMin_afterEach; */
  /* uint config_cntMax_afterEach; */
  /* t_float config_ballSize_max; */
  bool isTo_ingore_zeroScores;
} s_hp_api_ccm_config_t;

//! @returm a default itniated vieorns of the s_hp_api_ccm_config_t cofnig object.
s_hp_api_ccm_config_t initAndReturn__s_hp_api_ccm_config_t();
//! Comptue sparse simalrity through applciaotn fo sense siamrlity-emtrics.
bool apply__simMetric_dense__extraArgs__hp_api_norm(const s_kt_correlationMetric_t obj_metric__, s_kt_matrix_t *mat_1,  s_kt_matrix_t *mat_result, s_hp_api_ccm_config_t config);
/* //! Applies a dense matrix-based simliarty-computation based on data-input, and store result in "mat_result" (oekseth, 06. mar. 2017). */
/* bool apply__simMetric_dense__hp_api_norm(const e_kt_correlationFunction_t enum_id, s_kt_matrix_t *mat_1,  s_kt_list_2d_kvPair_t *mat_result); */
//! Read data from input-file(s) and then export the result (oekseth, 06. mar. 2017).
bool readFromFile__stroreInStruct__exportResult__hp_api_norm(const char *stringOf_enum, s_kt_matrix_t *mat_1,  s_kt_matrix_t *mat_result, 
							     //s_kt_list_1d_uint_t *map_clusters, 
							     const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
							     //	  const char *stringOf_exportFile_clusters,  const char *stringOf_exportFile__format_clusters, 
							     const bool isTo_transpose,
							     s_hp_api_ccm_config_t config);

//! Provide a generic interface to the "readFromFile__stroreInStruct__exportResult__hp_api_norm(..)" funciton.
bool readFromFile__exportResult__hp_api_norm(const char *stringOf_enum, const char *stringOf_inputFile_1,  const bool isTo_transpose,
					     const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
					     //const char *stringOf_exportFile_clusters,  const char *stringOf_exportFile__format_clusters, 
						  // const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores, const bool inputFile_isSparse
					     s_hp_api_ccm_config_t config);

//! Build a char-string for a set of input-paremters, and return this string-list (oekseth, 06. mar. 2017).
char *construct__terminalArgString__fromInput__hp_api_norm(const bool debug_config__isToCall, const char *stringOf_enum, const char *stringOf_inputFile_1, 
								const bool isTo_transpose,
								const char *stringOf_exportFile, const char *stringOf_exportFile__format, 
							   // const char *stringOf_exportFileClust,  const char *stringOf_exportFileClust__format, 
								s_hp_api_ccm_config_t config,
								//const uint nclusters, const bool inputFile_isSparse, 
								uint *scalar_cntArgs, char **listOf_args__2d);
//bool readFromFile__exportResult__hp_api_norm(const char *stringOf_enum, const char *stringOf_inputFile_1,  const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores, const bool inputFile_isSparse); 
//bool readFromFile__exportResult__hp_api_norm(const char *stringOf_enum, const char *stringOf_inputFile_1, const char *stringOf_inputFile_2, const char *stringOf_exportFile, const char *stringOf_exportFile__format, const uint nclusters, const bool inputFile_isSparse);

/**
   @brief parse a set of terminal-inptu-argumetns (oekseth, 06. mar. 2017)
   @param <arg> is the termianl-input argumetns, where arg[0] is epxected to be the program-nbame
   @param <arg_size> is the number of arguments in "arg"
   @return true upon success.
 **/
bool fromTerminal__hp_api_norm(char **arg, const uint arg_size);
//static void printOptions____hp_api_norm();




#endif //! EOF
