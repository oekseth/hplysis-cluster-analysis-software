#include "alg_dbScan_brute.h"
#include "alg_dbScan_brute_sciKitLearn.h"
#include "kd_tree.h"
#include "kt_forest_findDisjoint.h"

static bool  __collectResults(const s_kt_list_1d_uint_t *clusters, s_kt_clusterAlg_fixed_resultObject_t *obj_result) {
  assert(clusters); assert(obj_result);
  //! 
  //! Find max-forest-count:
  uint max_cntForests = 0;
  for(uint row_id = 0; row_id < clusters->list_size; row_id++) {
    if(clusters->list[row_id] != UINT_MAX) {
      max_cntForests = macro_max(max_cntForests, clusters->list[row_id]);
    }
  }
  max_cntForests++; //! ie, the total count.    
  //!
  //! Update the result-object: 
  init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, max_cntForests, clusters->list_size);
  for(uint row_id = 0; row_id < clusters->list_size; row_id++) {
    if(clusters->list[row_id] != UINT_MAX) {
      obj_result->vertex_clusterId[row_id] = clusters->list[row_id];
    }
  }
  //!
  //! @return true, ie, as we asusem the oepration was a success.
  return true;  
}

static s_kt_list_1d_uint_t __collectResults_fromKMeansObject(const s_kt_clusterAlg_fixed_resultObject_t *obj_result) {
  assert(obj_result);
  s_kt_list_1d_uint_t clusters = setToEmpty__s_kt_list_1d_uint_t();
  if( (obj_result->cnt_vertex != 0) && (obj_result->vertex_clusterId != NULL) ) {
    uint nrows = obj_result->cnt_vertex; uint ncols = 1;
    //! Allocate:
    clusters = init__s_kt_list_1d_uint_t(nrows);
    //! Set cluster-memberships:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      clusters.list[row_id] = obj_result->vertex_clusterId[row_id];
    }
  }
  //!
  //! @return
  return clusters;
}

/**
   @brief comptues disjoint forests (eg, "DBSCAN" or "hp-cluster").
   @param <alg_id> is the algorithm to be sued for a brute non-optmzied coptmaution.
   @param <obj_1> is the covariance simalrity matrix.
   @param <epsilon> where scores less than eplsion is used.
   @param <minpts> the maximum number of poitns which needs to be related to the given vertex 'for it to be sued as a straverse starting point'.
   @return the list of identifed clusters.
 **/
s_kt_list_1d_uint_t computeAndReturn__alg_dbScan_brute(const e_alg_dbScan_brute_t alg_id, const s_kt_matrix_base_t *obj_1, const t_float epsilon, const uint minpts) {
  if(alg_id == e_alg_dbScan_brute_linkedListSet) {
    s_kt_clusterAlg_fixed_resultObject_t obj_result = initAndReturn__s_kt_clusterAlg_fixed_resultObject_t();
    const bool is_ok = compute__alg_dbScan_brute_linkedListSet(obj_1, &obj_result, epsilon, minpts);
    assert(is_ok);
    s_kt_list_1d_uint_t clusters = __collectResults_fromKMeansObject(&obj_result);
    return clusters;
  } else if(alg_id == e_alg_dbScan_brute_sciKitLearn) {
    s_kt_list_1d_uint_t clusters = computeFor_filteredMatrix__alg_dbScan_brute_sciKitLearn(obj_1);
    return clusters;
    //const bool is_ok = __collectResults(&clusters, obj_result);
    //free__s_kt_list_1d_uint_t(&clusters);
    //return is_ok;
    //#if false
  } else if( 
	    (alg_id == e_alg_dbScan_brute_splitData_kdTree)
	    || (alg_id == e_alg_dbScan_brute_splitData_brute_slow) 
	    || (alg_id == e_alg_dbScan_brute_splitData_brute_fast) 
	     ) {
    //! Build the kd-tree:
    //start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
    s_kt_matrix_t mat_shallow = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(obj_1);
    s_kd_tree_t obj_kd = build_tree__s_kd_tree(&mat_shallow);
    // end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
    //! ---------------------------
    //start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
    s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
    s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest, &obj_kd, /*nn_numberOfNodes=*/mat_shallow.nrows, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
    if(alg_id == e_alg_dbScan_brute_splitData_brute_fast) {
      obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/mat_shallow.nrows, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
    } else if(alg_id == e_alg_dbScan_brute_splitData_brute_slow) {
      obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_slow, &obj_kd, /*nn_numberOfNodes=*/mat_shallow.nrows, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
    }
    //! Compute the clusters:
    s_kt_list_1d_uint_t clusters = dbScan__s_kd_searchConfig_t(&obj_search, /*config_minCnt=*/1);
    //! De-allocate:
    free__s_kd_searchConfig_t(&obj_search);
    free__s_kd_tree(&obj_kd);
    //const bool is_ok = __collectResults(&clusters, obj_result);
    //free__s_kt_list_1d_uint_t(&clusters);
    //! 
    return clusters;
    //return is_ok;
  } else if(alg_id == e_alg_dbScan_brute_hpCluster) {
    //! A similar test where we use a dense matrix as nput:
    s_kt_forest_findDisjoint self; allocate__denseMatrix__s_kt_forest_findDisjoint(&self, obj_1->nrows, obj_1->ncols, obj_1->matrix, NULL, /*default_value_float=*/T_FLOAT_MAX);
    //! -----------------------------
    //!
    //! Infer the disjotin sets:
    graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/true);          
    s_kt_list_1d_uint_t clusters = init__s_kt_list_1d_uint_t(obj_1->nrows);
    { //! Update the result-set:
      for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
	clusters.list[row_id] = get_foreestId_forVertex__s_kt_forest_findDisjoint(&self, row_id);
      }
    }
    // __collectResults(&clusters, obj_result);
    //free__s_kt_list_1d_uint_t(&(clusters));
    //! -----------------------------
    //! De-allocate:
    free_s_kt_forest_findDisjoint(&self);    
    //! 
    return clusters;
  } else {
    if(alg_id != e_alg_dbScan_brute_undef) {
      fprintf(stderr, "!!\t algorithm-id Not supported. Please request the otpion to be supported, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    } else {
      fprintf(stderr, "!!\t algorithm-id Not supported: you requested an 'undefined' algorithm-id to be used, an option which we do not see the point of, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    }
    assert(false);
    return setToEmpty__s_kt_list_1d_uint_t();
  }
}

/**
   @brief comptues disjoint forests (eg, "DBSCAN" or "hp-cluster").
   @param <alg_id> is the algorithm to be sued for a brute non-optmzied coptmaution.
   @param <obj_1> is the covariance simalrity matrix.
   @param <obj_result> holds the resutls of the clustering
   @param <epsilon> where scores less than eplsion is used.
   @param <minpts> the maximum number of poitns which needs to be related to the given vertex 'for it to be sued as a straverse starting point'.
   @return true upon success.
 **/
bool compute__alg_dbScan_brute(const e_alg_dbScan_brute_t alg_id, const s_kt_matrix_base_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const t_float epsilon, const uint minpts) {
  s_kt_list_1d_uint_t clusters = computeAndReturn__alg_dbScan_brute(alg_id, obj_1, epsilon, minpts);
  const bool is_ok = __collectResults(&clusters, obj_result);
  assert(is_ok);
  free__s_kt_list_1d_uint_t(&clusters);
  return is_ok;
}
