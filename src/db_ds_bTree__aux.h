#ifndef db_ds_bTree__aux_h
#define db_ds_bTree__aux_h

/**
   @file db_ds_bTree__aux
   @brief includes gneeirc auxialiary-fucntiosn to our B-tree-implementaiotns (oekseth, 06. mar. 2017).
 **/


#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"

#define __M__constant__case_4 4
#define __M__constant__case_16 16
#define __M__constant__case_32 32

/**
   @enum e_db_cmpResult
   @brief provide a geneirc appraoch to cmpare diffneret and complex objects (oekseth, 06. mar. 2017).
 **/
typedef enum e_db_cmpResult {
  e_db_cmpResult_Duplicate,
  //e_db_cmpResult_SearchFailure,
  e_db_cmpResult_Success,
  e_db_cmpResult_InsertIt,
  e_db_cmpResult_LessKeys 
} e_db_cmpResult_t;

/**
   @enum e_db_ds_bTree_cntChildren
   @brief idnetiies the 'fixed count' of childnre to be allocated for a given B-tree.
 **/
typedef enum e_db_ds_bTree_cntChildren {
  e_db_ds_bTree_cntChildren_4,
  e_db_ds_bTree_cntChildren_16,
  e_db_ds_bTree_cntChildren_32,
  //! --------------------
  e_db_ds_bTree_cntChildren_undef
} e_db_ds_bTree_cntChildren_t;


#endif //! EOF
