#ifndef kt_clusterAlg_hca__node_h
#define kt_clusterAlg_hca__node_h


/*
 * A Node struct describes a single node in a tree created by hierarchical
 * clustering. The tree can be represented by an array of n Node structs,
 * where n is the number of elements minus one. The integers left and right
 * in each Node struct refer to the two elements or subnodes that are joined
 * in this node. The original elements are numbered 0..nelements-1, and the
 * nodes -1..-(nelements-1). For each node, distance contains the distance
 * between the two subnodes that were joined.
 */
typedef struct Node {
  int left; int right; t_float distance;
  //Node() : left(UINT_MAX), right(UINT_MAX), distance(0) {};
} Node_t;
//! Iniates the Node_t object
static void init_Node_t(Node_t *self) { self->left = INT_MAX; self->right = INT_MAX; self->distance = 0;} 
//! Iniates the Node_t object
static Node_t initAndReturn_Node_t(const int left, const int right, const t_float distance) { 
  Node_t self;
  self.left = left; self.right = right; self.distance = distance;
  //! @return:
  return self;
} 

//! @brief compare the distance assicated to two nodes.
//! @remarks Helper function for qsort.
static int nodecompare__Node_t(const void* a, const void* b) {
  const Node_t* node1 = (const Node_t*)a;
  const Node_t* node2 = (const Node_t*)b;
  const float term1 = node1->distance;
  const float term2 = node2->distance;
  if(term1 < term2) return -1;
  if(term1 > term2) return +1;
  return 0;
}



#endif //! EOF
