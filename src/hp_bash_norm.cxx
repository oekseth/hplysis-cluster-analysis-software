#include "hp_api_norm.h"
#include "hpLysis_api.h"

int main(const int array_cnt, char **array) {
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  //hpLysis__globalInit__kt_api();
  //! *************************************************************************
  //! Apply logics:
  const bool is_ok = fromTerminal__hp_api_norm(array, array_cnt);
  assert(is_ok);
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  //hpLysis__globalFree__kt_api();

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  //! *************************************************************************  
  //! @return:
  return 0;
}
