#ifndef _LIBMINE_H
#define _LIBMINE_H

/* #ifndef LIBMINE_VERSION */
/* #define LIBMINE_VERSION "2.0.0" */
/* #endif */


#ifndef LIBMINE_VERSION
#define LIBMINE_VERSION "2.0.0"
#endif

#ifndef EST_MIC_APPROX
#define EST_MIC_APPROX 0
#endif

#ifndef EST_MIC_E
#define EST_MIC_E 1
#endif

#ifndef function_prefix
#define function_prefix static
#endif 
//#define function_prefix 


#include "configure_optimization.h" //! Whcih specifies the optmizaitons and macro-configurations

/**
   @enum e_mine_typeOf_scores
   @brief iden tify the differnet scores to be computed wrt. Mine scores.
 **/
typedef enum e_mine_typeOf_scores {
  e_mine_typeOf_scores_mic,
  e_mine_typeOf_scores_mas,
  e_mine_typeOf_scores_mev,
  e_mine_typeOf_scores_mcn,
  e_mine_typeOf_scores_mcn_general,
  e_mine_typeOf_scores_gmic,
  e_mine_typeOf_scores_tic,
  //! Support cases wrt. correlation-scores in the "cluster.c" library:
  e_mine_typeOf_scores_clusterC_euclid,
  e_mine_typeOf_scores_clusterC_cityblock,
  e_mine_typeOf_scores_clusterC_correlation,
  e_mine_typeOf_scores_clusterC_correlation_absolute,
  e_mine_typeOf_scores_clusterC_correlation_uncentered,
  e_mine_typeOf_scores_clusterC_correlation_uncentered_absolute,
  e_mine_typeOf_scores_clusterC_spearman,
  e_mine_typeOf_scores_clusterC_kendall,
  //! ---
  e_mine_typeOf_scores_undef
} e_mine_typeOf_scores_t;


//function_prefix char *libmine_version = LIBMINE_VERSION;
#ifndef MAX
#define MAX(a, b) (((a) > (b) ? (a) : (b)))
#endif
#ifndef MIN
#define MIN(a, b) (((a) < (b) ? (a) : (b)))
#endif

//#include "fast_log.h" //! which provide access to a 'fast-log' macro




#ifdef __cplusplus
extern "C" {
#endif
  
extern char *libmine_version;



/* The mine_problem structure describes the problem. */
/* x and y are the two variables of length n. */
typedef struct mine_problem
{
  int n;
  t_float *x;
  t_float *y;
} mine_problem;

  /**
     The mine_parameter structure describes the MINE parameters.
     # "alpha" is the exponent in B(n) = n^alpha and must be in (0,1], and 
     # "c" determines how many more clumps there will be than columns in every partition. c = 15 meaning that when trying to draw x grid lines on the x-axis, the algorithm will start with at most 15*x clumps. c must be > 0.
  **/
typedef struct mine_parameter
{
  t_float alpha;
  t_float c;
  int est;
} mine_parameter;


/* The mine_score structure describes the maximum  */
/* normalized mutual information scores. I[i][j]  */
/* contains the score using a grid partitioning  */
/* x-values into i+2 bins and y-values into j+2 bins.  */
/* m and M are of length n and each M[i] is of length m[i]. */
typedef struct mine_score
{
  int n; /* number of rows of M */
  int *m; /* number of cols of M[i] for each i */
  t_float **M; /* the approx. characteristic matrix */
} mine_score;
  

  static int *argsort(t_float *a, int n);

/* Computes the maximum normalized mutual information scores and 
 * returns a mine_score structure. Returns NULL if an error occurs.
 * Algorithm 5, page 14, SOM
 */
  function_prefix mine_score *mine_compute_score_notAllocate_logValues(mine_problem *prob, mine_parameter *param, const int *sortedArr_prob_x, const int *sortedArr_prob_y);
  
/* Computes the maximum normalized mutual information scores and 
 * returns a mine_score structure. Returns NULL if an error occurs.
 */
  function_prefix mine_score *mine_compute_score(mine_problem *prob, mine_parameter *param);
  /* Computes the maximum normalized mutual information scores and 
   * returns a mine_score structure. Returns NULL if an error occurs.
   */
function_prefix mine_score *mine_compute_score_notInParallel(mine_problem *prob, mine_parameter *param); //, const int cnt_threads_requested = INT_MAX);
  
/*   /\** */
/*      @brief Compute the correaltion for vectors in a data-set. */
/*      @param <matrix> is the matrix we comptue for */
/*      @param <nrows> is the number of rows in the matrix. */
/*      @param <ncols> is the number of columns in the matrix. */
/*      @param <param> is the description of the parameter-space to use. */
/*      @param <idOf_function_char> identfies the coefficeitn-funciton to compute. */
/*      @param <meta> which if  used si epxcted to idnetify the 'meta' functio in the matrix. */
/*      @remarks In below we describe how the chars reference the functions: */
/*      # "i" : mine_mic(); */
/*      # "a" : mine_mas(); */
/*      # "e" : mine_mev(); */
/*      # "c" : mine_mcn(); */
/*      # "g" : mine_mcn_general(); */
/*      # "m" : mine_gmic(); */
/*      # "t" : mine_tic();      */
/*   **\/ */
/* function_prefix t_float **compute_2d_scoreMatrix(t_float **matrix, const int nrows, const int ncols, mine_parameter *param, const char idOf_function_char, t_float meta); */

/* This function checks the parameters. It should be called 
 * before calling mine_compute_score(). It returns NULL if 
 * the parameters are feasible, otherwise an error message is returned.
 */
function_prefix char *mine_check_parameter(mine_parameter *param);


/* Returns the Maximal Information Coefficient (MIC). */
function_prefix t_float mine_mic(mine_score *score);
/* Returns the Maximum Asymmetry Score (MAS). */
function_prefix t_float mine_mas(mine_score *score );


/* Returns the Maximum Edge Value (MEV). */
function_prefix t_float mine_mev(mine_score *score );


/* Returns the Minimum Cell Number (MCN), with eps >= 0. */
function_prefix t_float mine_mcn(mine_score *score, t_float eps);


/* Returns the Minimum Cell Number (MCN) with eps = 1 - MIC. */
function_prefix t_float mine_mcn_general(mine_score *score );


/* Returns the Generalized Mean Information Coefficient (GMIC) */
function_prefix t_float mine_gmic(mine_score *score, t_float p);


/* Returns the Total Information Coefficient (TIC). */
function_prefix t_float mine_tic(mine_score *score );


/* Frees the score structure. */
function_prefix  void mine_free_score(mine_score **score);

#ifdef __cplusplus
}
#endif

#endif /* _LIBMINE_H */
