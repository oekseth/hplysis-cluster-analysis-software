//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "tut_wrapper_image_segment.h"
#include "hp_image_segment.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "export_ppm.h"
#include <climits>
#include <cfloat>
//#endif



//! The main function for logics assotied to "image segmentation" (oekseth, 06. feb. 2018).
void tut_wrapper_image_segment(const int array_cnt, char **array) {

  //! ------------------------------------------------------  
  { //! Test: PPM-back-forth-conversion:
    //!
    //! Step: Construct a sytentic matrix:
    //! Note: we here construct a clsuter-shape. This to provide the stepping-stoens for future, and mroe compelx, examples
    const uint nrows = 100;
    s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/1000, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/20,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/0);
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/20,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/20);    
    buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, /*nrows=*/40,  /*cnt_clusters=*/4, /*cnt_clusterStartOffset=*/60);    
    //const char *result_file = "tmp.ppm";
    const char *result_file = "tmp-1.ppm";
    const char *result_file_2 = "tmp-2.ppm";    
    //!
    { //! Step: Export to PPM:
      //s_kt_matrix_t matrix = &(obj_shape.matrix); //! ie, a shallow copy.
      ppm_image_t *o_image = exportTo_image(obj_shape.matrix.matrix, obj_shape.matrix.nrows, obj_shape.matrix.ncols, NULL, NULL);
      ppm_image_write(result_file, o_image);
      //! De-allcote:
      ppm_image_finalize(o_image);
    }
    // 
    //!
    { //! Step: Import from PPM:
      const char *input_file = result_file;
      s_hp_image_segment_t o_img = ppm_import__s_hp_image_segment_t(input_file);
      //! Export to a different reuslt-file:
      export__s_hp_image_segment_t(&o_img, result_file_2);
      //! De-allcoate:      
      free__s_hp_image_segment_t(&o_img);
    }
    //!
    //! De-alloctate:
    free__s_hp_clusterShapes_t(&obj_shape);
  }
  
  { //! A simple test to validate the image-fetching locigcs:
    const char *sample_string = 
           "9,0: (138,139,146) #8a8b92 rgb(138,139,146)\n";
    //     "9,0: (138, 139, 146) #8A8A92 rgb(138, 139, 146)\n\0";
    s_hp_image_pixel_t self = extractFrom_imageMagicText_RGB__s_hp_image_pixel_t(sample_string);
    assert(self.pos_row == 9);
    assert(self.pos_col == 0);
    assert(self.RGB[0] == 138);
    assert(self.RGB[1] == 139);
    assert(self.RGB[2] == 146);
    { //! Test rotuines for converison/extration of the "Hue" from the data object.
      //! 
      //! Convert to HSV: 
      t_float hsv[3] = {0, 0, 0};
      RGBtoHSV__s_hp_image_pixel_t_(self, &hsv[0], &hsv[1], &hsv[2]);      
      printf("hsv = (%f, %f, %f), at %s:%d\n", 
	     hsv[0], 
	     hsv[1], 
	     hsv[2], 
	     __FILE__, __LINE__);
    }
    { //! Valide the exprot-routine, ie, a simpel round-trip: 
      const char *sample_file = "testFile_tutWrapper.tsv";
      FILE *file_out = fopen(sample_file, "wb");
      assert(file_out);
      //! Export the object:
      writeOut__toFile__format_imageMagicText_s_hp_image_pixel_t(self, file_out);
      fclose(file_out);
      //! 
      //! Open the file, and validte that the results corresponds to our exepctaitons:
      FILE *file_inp = fopen(sample_file, "rb");
      const uint buffer_size = strlen(sample_string) + 10;
      const char def_value = '\0';
      char *buffer = allocate_1d_list_char(buffer_size, def_value);
      const int cnt_chars_read = (int)fread(buffer, strlen(sample_string)+9, sizeof(char), file_inp);
      if(false) {
	printf("str(read) = \"%s\", cnt_chars_read=%u, length(strings)=(%u VS %u), at %s:%d\n", buffer, (uint)cnt_chars_read, 
	       (uint)strlen(sample_string) , (uint)strlen(buffer),
	       __FILE__, __LINE__);
      }
      // assert(cnt_chars_read == (int)strlen(sample_string));
      //!
      //!
      assert(strlen(sample_string) == strlen(buffer));
      assert(0 == strcmp(sample_string, buffer));
      //!
      //!
      fclose(file_inp);
    }
  }
  { //! Test simple (and basic) image--load--export routines.
    { //! Roundrip(simple): Construct an image 'from scratch', export the result, and then validae the input parsing:
      const char *sample_file = "testFile_tutWrapper.txt";
      const char *str_data = "# ImageMagick pixel enumeration: 3,2,255,rgb"
	/**
	 **/
	"\n"
	"0,0: (255,0,0) #ff0000 rgb(255,0,0)"
	"\n"
	"1,0: (0,255,0) #00ff00 rgb(0,255,0)"
	"\n"
	"2,0: (0,0,255) #0000ff rgb(0,0,255)"
	"\n"
	"0,1: (255,255,0) #ffff00 rgb(255,255,0)"
	"\n"
	"1,1: (255,255,255) #ffffff rgb(255,255,255)"
	"\n"
	"2,1: (0,0,0) #000000 rgb(0,0,0)"
	"\n"
	;
      FILE *file_out = fopen(sample_file, "wb");
      assert(file_out);
      fputs(str_data, file_out);
      fclose(file_out);
      //if(false)
      { //! Load an image into the data-structure:      
	s_hp_image_segment_t self = txt_import__s_hp_image_segment_t(sample_file);
	//! Export the imagE:
	const char *sample_file_backForth = "testFile_tutWrapper.backForth.txt";
	export__s_hp_image_segment_t(&self, sample_file_backForth);
	{ //! Read result file, and validate the results:
	  //! 
	  //! Open the file, and validte that the results corresponds to our exepctaitons:
	  FILE *file_inp = fopen(sample_file_backForth, "rb");
	  const uint buffer_size = strlen(str_data) + 10;
	  const char def_value = '\0';
	  char *buffer = allocate_1d_list_char(buffer_size, def_value);
	  const int cnt_chars_read = (int)fread(buffer, strlen(str_data)+1, sizeof(char), file_inp);
	  if(false) {
	    printf("str(read) = \"%s\", cnt_chars_read=%u, length(strings)=(%u VS %u), at %s:%d\n", buffer, (uint)cnt_chars_read, 
		   (uint)strlen(str_data) , (uint)strlen(buffer),
		   __FILE__, __LINE__);
	  }
	  // assert(cnt_chars_read == (int)strlen(sample_string));
	  //!
	  //!
	  assert(strlen(str_data) == strlen(buffer));
	  assert(0 == strcmp(str_data, buffer));
	  //!
	  //!
	  fclose(file_inp);
	}
	{ //! Export to an png image:
	  const char *sample_file_backForth = "testFile_tutWrapper.backForth.png";
	  export__s_hp_image_segment_t(&self, sample_file_backForth);	  
	  { //! Load an image into the data-structure:
	    s_hp_image_segment_t self_png = txt_import__s_hp_image_segment_t(sample_file_backForth);
	    assert(self_png.mat_RGB[0].nrows == self.mat_RGB[0].nrows);
	    assert(self_png.mat_RGB[0].ncols == self.mat_RGB[0].ncols);
	    //!
	    //! De-allocate: 
	    free__s_hp_image_segment_t(&self_png);
	  }
	}
	//      if(false)
	{ //! Test rotuines for converison/extration of the "Hue" from the data object.
	  s_kt_matrix_base_t mat_hue = fetch_Hue_s_hp_image_segment_t(&self);
	  assert(mat_hue.nrows == self.mat_RGB[0].nrows);
	  assert(mat_hue.ncols == self.mat_RGB[0].ncols);
	  free__s_kt_matrix_base_t(&mat_hue);
	}
	//!
	//! De-allocate: 
	free__s_hp_image_segment_t(&self);
      }
    }
  }

}
