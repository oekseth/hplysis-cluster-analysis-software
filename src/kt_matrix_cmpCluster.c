#include "kt_matrix_cmpCluster.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#define MiC__support_CCMs_clusterCountOne 1

#include "correlation_macros__distanceMeasures.h"
//#include "s_allAgainstAll_config.h"
#include "kt_distance.h"
#include "kt_distance_cluster.h"
#include "kd_tree.h"

//! @return a string-represetnation of the enum_id (oekseth, 06. des. 2016).
const char *getStringOf__e_kt_matrix_cmpCluster_metric_t(const e_kt_matrix_cmpCluster_metric_t enum_id) {
  //! Note: if [”elow] is udpatd, then remember updating our "hpLysisVisu_server.js" (eokseth, 06. arp. 2017).
  if(enum_id == e_kt_matrix_cmpCluster_metric_randsIndex) {return "Rands-index";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_randsIndex_alt2) {return "Rands-index (alternative-implementation)";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_randsAdjustedIndex) {return "Adjusted Rand Index (ARI)";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_chiSquared) {return "Chi-squared";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_FowlesMallows) {return "Fowles-Mallows";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Mirkin) {return "Mirkin";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Wallace) {return "Wallace";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard) {return "Jaccard";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard_seperate_sensitivtyAndSpecifity) {return "Jaccard (seperate sensivitiy and specifity)";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard_F1_score) {return "Jaccard (F1-score)";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Minkowski) {return "Minkowski";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_PR) {return "PR";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_PR_alt2) {return "PR (alternative implementation)";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_partitionDifference) {return "Partition-Difference";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_fMeasure) {return "fMeasure";} 
  else if(enum_id == e_kt_matrix_cmpCluster_metric_maximumMatchMeasure) {return "maximum-match-measure";}
  //! ...
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen) {return "van Dogen";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relative) {return "van Dogen (oekseth::relative)";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared) {return "van Dogen (oekseth::relative-squared)";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_min) {return "van Dogen (oekseth::relative-squared-min)";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_max) {return "van Dogen (oekseth::relative-squared-max)";}
  //! ----
  else if(enum_id == e_kt_matrix_cmpCluster_metric_mutualInforation_Strehl_Ghosh) {return "Strehl & Gosh";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_mutualInforation_Fred_Jain) {return "Fred & Jain";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_variationOfInformation) {return "Variation-of-information";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_variationOfInformation_oekseth_relative) {return "Variation-of-information (oekseth::relative)";}
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               K-means-cluster measures (oesketh, 06. juni 2017)                                        ----  
  else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {return "SSE-average-scores-centersOf-Euclid-within-by-between-sum";}
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenMax) {return "SSE-average-ranks-centersOf-Euclid-within-by-between-sum";} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {return "SSE-average-scores-centersOf-Euclid-within-by-between-max";} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenMax) {return "SSE-average-ranks-centersOf-Euclid-within-by-between-max";} */
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               Matrix-based measures                                        ----
  else if(enum_id == e_kt_matrix_cmpCluster_metric_silhouetteIndex) {return "Silhouette Index";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_DavisBouldin) {return "Davis Bouldin";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Dunn) {return "Dunn";}

  //! **************************************************************************************************'
  //! **************************************************************************************************'  
  else if(enum_id == e_kt_matrix_cmpCluster_metric_undef) {return "<undef>";}
  else {
    fprintf(stderr, "!!\t Add support for enum_id=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false);
    return "<an error>";
  }
}
//! @return a string-represetnation of the enum_id (oekseth, 06. des. 2016).
const char *getStringOf__e_kt_matrix_cmpCluster_metric_t__short(const e_kt_matrix_cmpCluster_metric_t enum_id) {
  //! Note: if [”elow] is udpatd, then remember updating our "hpLysisVisu_server.js" (eokseth, 06. arp. 2017).
  if(enum_id == e_kt_matrix_cmpCluster_metric_randsIndex) {return "Rands";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_randsIndex_alt2) {return "RandsAlt";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_randsAdjustedIndex) {return "ARI";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_chiSquared) {return "Chi";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_FowlesMallows) {return "FowlesMallows";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Mirkin) {return "Mirkin";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Wallace) {return "Wallace";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard) {return "Jaccard";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard_seperate_sensitivtyAndSpecifity) {return "Jaccard_sensitivty";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Jaccard_F1_score) {return "Jaccard_F1";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Minkowski) {return "Minkowski";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_PR) {return "PR";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_PR_alt2) {return "PR_alt";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_partitionDifference) {return "PartitionDifference";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_fMeasure) {return "fMeasure";} 
  else if(enum_id == e_kt_matrix_cmpCluster_metric_maximumMatchMeasure) {return "maximumMatchMeasure";}
  //! ...
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen) {return "vanDogen";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relative) {return "vanDogen_alt2";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared) {return "vanDogen_alt3";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_min) {return "vanDogen_alt4";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_max) {return "vanDogen_alt5";}
  //! ----
  else if(enum_id == e_kt_matrix_cmpCluster_metric_mutualInforation_Strehl_Ghosh) {return "StrehlGosh";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_mutualInforation_Fred_Jain) {return "FredJain";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_variationOfInformation) {return "VariationOfInformation";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_variationOfInformation_oekseth_relative) {return "VariationOfInformation_alt2";}
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               K-means-cluster measures (oesketh, 06. juni 2017)                                        ----  
  else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {return "SSEaveragescorescentersOfEuclidwithinbybetweensum";}
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenMax) {return "SSEaveragerankscentersOfEuclidwithinbybetweensum";} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {return "SSEaveragescorescentersOfEuclidwithinbybetweenmax";} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenMax) {return "SSEaveragerankscentersOfEuclidwithinbybetweenmax";} */
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               Matrix-based measures                                        ----
  else if(enum_id == e_kt_matrix_cmpCluster_metric_silhouetteIndex) {return "Silhouette";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_DavisBouldin) {return "DavisBouldin";}
  else if(enum_id == e_kt_matrix_cmpCluster_metric_Dunn) {return "Dunn";}

  //! **************************************************************************************************'
  //! **************************************************************************************************'  
  else if(enum_id == e_kt_matrix_cmpCluster_metric_undef) {return "undef";}
  else {
    fprintf(stderr, "!!\t Add support for enum_id=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false);
    return "<an error>";
  }
}

//! @return teh string-rpesentaiton of the e_kt_matrix_cmpCluster_clusterDistance__config_metric__t enum_id (oekseth, 06. des. 2016).
const char *getString__e_kt_matrix_cmpCluster_clusterDistance__config_metric__t(const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t enum_id ) {
  if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum) {return "sum";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) {return "avg";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) {return "min";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) {return "max";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {return "STD";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__config_metric__default) {return "<default>";}
  else {
    fprintf(stderr, "!!\t Add support for enum_id=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false);
    return "<an error>";
  }
}

//! Initiates the "s_kt_matrix_cmpCluster_clusterDistance_config_t" to default values (oekseth, 06. des. 2016).
//! @return a s_kt_matrix_cmpCluster_clusterDistance_config_t object which is set to empty.
s_kt_matrix_cmpCluster_clusterDistance_config_t setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t() {
  s_kt_matrix_cmpCluster_clusterDistance_config_t self;
  self.metric_clusterDistance__vertexVertex = e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum; //! ie, as defined in "e_kt_clusterComparison.h"
  self.metric_clusterDistance__cluster_cluster = e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum; 
  self.metric_complexClusterCmp_obj_isTo_use = false; //! ie, as defualt use the 'most simplictc approach'.
  self.metric_complexClusterCmp_vertexCmp_isTo_use = false; //! ie, as defualt use the 'most simplictc approach'.
  self.metric_complexClusterCmp_obj = setTo_default__andReturn__s_kt_correlationMetric_t(); //setTo_empty__andReturn__s_kt_correlationMetric_t();
  self.metric_complexClusterCmp_clusterMethod = e_kt_clusterComparison_avg; //! ie, as defined in "e_kt_clusterComparison.h"

  //!
  //! 'Advandced' strategies for 'cofngiruting' the center-point for both clusters and a network in general:  
  self.centroidMetric__vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef;
  //  self.centroidMetric__vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef;
  self.centroidMetric__vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef;
  self.centroidMetric__vertexVertex__center = e_kt_api_dynamicKMeans_undef; //! a 'center-poitn-algorithm' expected to be deinfed in our "kt_api.h"
  //! 
  //! 
  self.isTo_use_KD_treeInSimMetricComputations = false; //! which is used if the simrlairyt-metric is not a-propry comptued (oekseth, 06. aug. 2017).
  //! @return 
  return self;
}
//! Initiates the "s_kt_matrix_cmpCluster_clusterDistance_t" to default values (oekseth, 06. des. 2016).
s_kt_matrix_cmpCluster_clusterDistance_t setToEmpty__s_kt_matrix_cmpCluster_clusterDistance() {
  s_kt_matrix_cmpCluster_clusterDistance_t self;
  self.vertexTo_clusters = NULL;
  self.cluster_cluster = NULL;
  self.cluster_cluster__count = NULL;
  self.mapOf_vertexClusterIds = NULL;
  self.nrows = 0;
  self.ncols = 0;
  self.cnt_cluster = 0;
  setTo_Empty__s_kt_set_2dsparse_t(&(self.mappings_cluster_toVertex));
  self.input_matrix = NULL;
  //! 
  self.config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();

  //! @return 
  return self;
}


//! @return teh string-represetnation of the e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id in 1uesiton.
const char *getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id) {
  if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC) {return "CalinskiHarabasz(VRC)";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC_relative__oekseth) {return "CalinskiHarabasz(VRC:relative:oekseth)";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette) {return "Silhouette-Index";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin) {return "Davis-Bouldin-Index(Euclid)";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin__sumBetween) {return "Davis-Bouldin-Index(clusterClusterMetric=definedInObject)";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns_relative__oekseth) {return "Dunns-Index(relative:oekseth)";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns) {return "Dunns-Index";}
  // ----
  //else if(enum_id == ) {return "";}
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               K-means-cluster measures (oesketh, 06. juni 2017)                                        ----  
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE) {return "SSE";} //_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {return "SSEaveragescorescentersOfEuclidwithinbybetweensum";}
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum) {return "SSEaveragerankscentersOfEuclidwithinbybetweensum";} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_scores_Euclid_within_divBy_betweenMax) {return "SSEaveragescorescentersOfEuclidwithinbybetweenmax";} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_ranks_Euclid_within_divBy_betweenMax) {return "SSEaveragerankscentersOfEuclidwithinbybetweenmax";} */
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               generic CCMs (oesketh, 06. juni 2017)                                        ----  
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM) {return "PBM";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_cIndex) {return "cIndex";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_iIndex) {return "iIndex";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_pointBiserial) {return "pointBiserial";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD) {return "RMMSSTD";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_rSquared) {return "rSquared";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_gammaMod) {return "GammaMod";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_xieBeni) {return "XieBeni";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_sdValidity) {return "sdValidity";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_VNND) {return "VNND";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_FigureOfMerit) {return "FigureOfMerit";}
  // ----
  else {
    /* fprintf(stderr, "!!\t Add support for enum_id=%u, at %s:%d\n", enum_id, __FILE__, __LINE__); */
    /* assert(false); //! ie, as we then need adding support 'for this'. */
    return "<undef>";
  }
}

//! @remarks a mdoficaito of "getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(..)" where we 'return' shorter IDs, eg, to reduce clsutter in web-based viauzlaitions of the reuslts (oekseth, 06. mar. 2017)
const char *getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id) {
  if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC) {return "VRC";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC_relative__oekseth) {return "VRC::rel";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette) {return "Silhouette";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin) {return "DBI(Euc)";} //! ie, Davis-Bouldin-Index(Euclid)
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin__sumBetween) {return "DBI";} //! ie, Davis-Bouldin-Index(clusterClusterMetric=definedInObject)
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns_relative__oekseth) {return "Dunn:rel";} //! ie, Dunns-Index(relative:oekseth)
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns) {return "Dunn";} //! ie, Dunns-Index
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               K-means-cluster measures (oesketh, 06. juni 2017)                                        ----  
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE) {return "SSE";} // _vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {return "SSEaveragescorescentersOfEuclidwithinbybetweensum";}
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               generic CCMs (oesketh, 06. juni 2017)                                        ----  
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM) {return "PBM";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_cIndex) {return "cIndex";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_iIndex) {return "iIndex";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_pointBiserial) {return "pointBiserial";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD) {return "RMMSSTD";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_rSquared) {return "rSquared";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_gammaMod) {return "GammaMod";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_xieBeni) {return "XieBeni";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_sdValidity) {return "sdValidity";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_VNND) {return "VNND";}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_FigureOfMerit) {return "FigureOfMerit";}
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum) {return "SSEaveragerankscentersOfEuclidwithinbybetweensum";} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_scores_Euclid_within_divBy_betweenMax) {return "SSEaveragescorescentersOfEuclidwithinbybetweenmax";} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_ranks_Euclid_within_divBy_betweenMax) {return "SSEaveragerankscentersOfEuclidwithinbybetweenmax";} */
  else {
    /* fprintf(stderr, "!!\t Add support for enum_id=%u, at %s:%d\n", enum_id, __FILE__, __LINE__); */
    /* assert(false); //! ie, as we then need adding support 'for this'. */
    return "<undef>";
  }
}
//! @return the 'extreme-poor-score' assicated to the ccm_id (oekseht, 06. mar. 2017)
const t_float getWorstCase_score__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id) {
  //! Note: [”elow] observaiotns are based on "http://localhost/data__hpLysis/ccm_deviationFromIdeal__gradualIncreaseInClusterCnt.html"
  //! Note[interpreation]: in [”elow] we returnt he score which we asusme is the 'worst' for a given CCM.
  const t_float min = T_FLOAT_MIN_ABS;  const t_float max = T_FLOAT_MAX;
  if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC) {return max;} 
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC_relative__oekseth) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette) {return min;} //! as the Silhouette is observed to 'decrease in score' for increasing data-noise-distrubitons.
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin) {return max;} //! as "Davis-Dobuldins Index" (DBI) is observed to increase for increasing data-noise-distrubitons.
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin__sumBetween) {max;} //! ie, Davis-Bouldin-Index(clusterClusterMetric=definedInObject)
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns_relative__oekseth) {return min;} //! (simliar to Silhouette, though with a higher degree of itnernal vairance).
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns) {return min;} //! ie, Dunns-Index
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               K-means-cluster measures (oesketh, 06. juni 2017)                                        ----  
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE) { return min;} //_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) {return min;}
  //! **************************************************************************************************'
  //! **************************************************************************************************'
  //! --                               generic CCMs (oesketh, 06. juni 2017)                                        ----  
  // FIXME[conceptaul]: [belwo] is in-correct ... ie, update [”elow] based on evlauatiosn ... (oekseth, 06. jun. 2017).
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_cIndex) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_iIndex) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_pointBiserial) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_rSquared) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_gammaMod) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_xieBeni) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_sdValidity) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_VNND) {return max;}
  else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_FigureOfMerit) {return max;}
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum) {return min;} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_scores_Euclid_within_divBy_betweenMax) {return min;} */
  /* else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_ranks_Euclid_within_divBy_betweenMax) {return min;} */
  else {
    fprintf(stderr, "!!\t Add support for enum_id=%u=\"%s\", at %s:%d\n", enum_id, getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(enum_id), __FILE__, __LINE__);
    assert(false); //! ie, as we then need adding support 'for this'.
    return T_FLOAT_MAX;
  }
  return T_FLOAT_MAX;
}

//! Intalize the mins-core:
#define __clusterConsistency__initScore() ({				\
  t_float min_score = T_FLOAT_MAX; \
      if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum) { min_score = 0;} \
      else if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) { min_score = 0;} \
      else if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) { min_score = T_FLOAT_MAX;} \
      else if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) { min_score = T_FLOAT_MIN_ABS;} \
      else if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) { min_score = 0; assert_possibleOverhead(current1dList); assert_possibleOverhead(current1dList_size > 0); current1dList__currentPos = 0; } \
      else {assert(false);}						\
      min_score;})

//! @return the updated min-score.
#define __clusterConsistency__updateScore() ({	\
      if( (candidate_score != 0) &&  (candidate_score != T_FLOAT_MIN_ABS) && (candidate_score != T_FLOAT_MAX) ) { \
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum) { min_score += candidate_score;} \
  else if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) { min_score += candidate_score;} \
  else if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) { min_score = macro_min(min_score, candidate_score);} \
  else if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) { min_score = macro_max(min_score, candidate_score);} \
  else if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) { current1dList[current1dList__currentPos++] = candidate_score;} \
  else {assert(false);} \
  /*! Update the debug-statement:*/ \
  cnt_udpated++; \
  } /*! else we assume the score is not set.*/	\
  min_score;}) //! ie, retunr.

//! Post-process the score:
#define __clusterConsistency__postProcessScore() ({ \
  assert(cnt_udpated > 0); /*! ie, as we expect at least tow clusters 'to be in the set'.*/ \
  assert(min_score != T_FLOAT_MAX); \
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) { \
    if(min_score != 0) {min_score = min_score / (t_float)cnt_udpated;}	\
  } else if( (cnt_udpated > 0) && (cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) ) { \
  /*! Idenitfy the sample-STD variance: */ \
    assert(current1dList__currentPos > 0); assert(current1dList); min_score = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(current1dList, current1dList__currentPos);} \
    })

//! @return the post-processed result:
#define __clusterConsistency__postProcessScore__numeratorDenum() ({ \
  t_float sumOf_result_local = 0; \
    if( (numerator != 0) && (deNum != 0) ) { \
      const t_float local_sum = (numerator / deNum); \
      if(isTo_adjustEachRowScore_by_log == false) { \
	/*printf("numerator=%f, deNum=%f, local_sum=%f, at %s:%d\n", numerator, deNum, local_sum, __FILE__, __LINE__); */ \
	sumOf_result_local = local_sum; \
      } else {	 \
	sumOf_result_local = mathLib_float_log_abs(local_sum); \
	/*printf("sumOf_result=%f, numerator=%f, deNum=%f, local_sum=%f, at %s:%d\n", sumOf_result, numerator, deNum, local_sum, __FILE__, __LINE__); */ \
      }	\
    } sumOf_result_local;}) //! ie, return


//! @return an itnaited veriosn of the s_kt_matrix_ccm_matrixBased_t object.
static s_kt_matrix_ccm_matrixBased_t init__s_kt_matrix_ccm_matrixBased() {
  s_kt_matrix_ccm_matrixBased_t self;
  self.between_min = T_FLOAT_MAX;  self.between_max = T_FLOAT_MIN_ABS;  
  self.within_min = T_FLOAT_MAX;  self.within_max = T_FLOAT_MIN_ABS;  
  self.sumOf_result__between = T_FLOAT_MAX;  self.sumOf_result__within = T_FLOAT_MAX;
  self.sumScores_total = 0;
  return self;
}

static s_kt_matrix_ccm_matrixBased_t compute__s_kt_matrix_ccm_matrixBased(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric, const bool isTo__useInternalObjectSpec, e_kt_api_dynamicKMeans_t metric_centroid, e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t vertexVertex__networkReference__between, e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t vertexVertex__networkReference__within) {
  const uint nrows = self->nrows; assert(nrows > 0);
  const uint ncols = self->ncols; 
  //!
  //! Intiaite:
  s_kt_matrix_ccm_matrixBased_t ret_obj = init__s_kt_matrix_ccm_matrixBased();

  { //! Construc the score-sum (oekseth, 06. sept. 2017):
    t_float **input_matrix = self->input_matrix;
    assert(input_matrix);
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	const t_float score = input_matrix[row_id][col_id];
	if(isOf_interest(score)) {
	  ret_obj.sumScores_total += score;
	}
      }
    }
  }
  // objAdj_2d_fromKD
  // printf("ret_obj.sumScores_total=%f, at %s:%d\n", ret_obj.sumScores_total, __FILE__, __LINE__);
  /* if(nrows != ncols) { */
  /*   fprintf(stderr, "!!\t Option Not supporeted: pelase reuqest for comptuignt his CCM-metrcis for a non-adjcuecney-matrix, ie, given amtrix-diemsions=[%u, %u], at [%s]:%s:%d\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__); */
  /*   assert(false); */
  /*   return T_FLOAT_MAX; */
  /* } */
  //  const uint nrows = self->nrows; assert(nrows > 0);   const uint ncols = self->ncols; 
  //!
  //! Apply lgoics:
  t_float sumOf_result__between = 0;     t_float sumOf_result__within = 0;
  assert(self->cluster_cluster); //! ie, as we then asusem the latter ia allocated/set.
  //  assert(self->cluster_cluster != UINT_MAX); //! ie, as we then asusem the latter ia allocated/set.
#include "kt_matrix_cmpCluster__ccmMatrix__api.c"
  //!
  //! @return
  ret_obj.sumOf_result__between = sumOf_result__between;  ret_obj.sumOf_result__within = sumOf_result__within;  
  return ret_obj;
}

//! Compute the cluster-comparison-metric of a 'genriec itnerpation' wrt betweneens and within-ness, ie, a gnerlaized appraoch which may be sued to simplify consturction of deidcted/spelied fucnitons (oekseth, 06. jun2. 2017)
//! @rerturn true upon success.
bool compute__generic__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric, const bool isTo__useInternalObjectSpec, s_kt_matrix_ccm_matrixBased_t *ret_object) { //t_float *scalar_within, t_float *scalar_between)
  //bool compute__generic__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric, const bool isTo__useInternalObjectSpec, t_float *scalar_within, t_float *scalar_between) {
  assert(self); 
  // assert(scalar_between); assert(scalar_within);
  assert(self);
#if(MiC__support_CCMs_clusterCountOne == 0)
  if(self->cnt_cluster <= 1) {return T_FLOAT_MAX;} //! ie, as the 'score' is then Not defined
#endif
  //! --------------------------------------------------------
  //!
  //! configure the interptaiton of these reuslts:
  // FIXME: validate correnctess of [”elwo] assignmetns/correctness ... 
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__default) {
    cmp_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg; //! ie, the 'type' correspoding to this cluster-compairson-metric: used to select the "minDist" in [above] equation
  }
  e_kt_api_dynamicKMeans_t metric_centroid = self->config.centroidMetric__vertexVertex__center;
  if(metric_centroid == e_kt_api_dynamicKMeans_undef) {metric_centroid = e_kt_api_dynamicKMeans_AVG;}
  //! ------------------
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t vertexVertex__networkReference__between = self->config.centroidMetric__vertexVertex__networkReference__between;
  if(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef) {
    vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex; //! ie, 'centroid-->global'.     
    //vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid; //! ie, 'centroid-->global'.     
  }
  //! ------------------
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t vertexVertex__networkReference__within = self->config.centroidMetric__vertexVertex__networkReference__within;
  if(vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef) {
    vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex; //! ie, 'vertex-->centroid'.
  }


  //!
  //! Apply logics:
  s_kt_matrix_ccm_matrixBased_t result_obj = compute__s_kt_matrix_ccm_matrixBased(self, cmp_type, isTo__useInternalObjectSpec, distance_clusterCluster_internalSimMetric, isTo__useInternalObjectSpec, metric_centroid, vertexVertex__networkReference__between, vertexVertex__networkReference__within);
  t_float sumOf_result__between = result_obj.sumOf_result__between;     t_float sumOf_result__within = result_obj.sumOf_result__within;
  //#include "kt_matrix_cmpCluster__ccmMatrix__api.c"

  //!
  //! Combine the results:
  // printf("sumOf_result__within=%f, sumOf_result__between=%f, at %s:%d\n", sumOf_result__within, sumOf_result__between, __FILE__, __LINE__);
  if( isOf_interest(sumOf_result__between) && isOf_interest(sumOf_result__within) && (sumOf_result__within != 0) && (sumOf_result__between != 0) ) { //! @return:
    assert(self->cnt_cluster > 1);
    *ret_object = result_obj; 
    /* *scalar_within  = sumOf_result__within; */
    /* *scalar_between = sumOf_result__between; */
    return true;
  } else  {
    return false;
  }
  return true;
}

//! Compute the cluster-comparison-metric of: Calinski-Harabasz (VRC) (oekseth, 06. feb. 2017)
t_float compute__VRC__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric, const bool isTo__useInternalObjectSpec) {
  assert(self);
  if(self->cnt_cluster <= 1) {return T_FLOAT_MAX;} //! ie, as the 'score' is then Not defined 
  //! --------------------------------------------------------
  //!
  //! configure the interptaiton of these reuslts:
  // FIXME: validate correnctess of [”elwo] assignmetns/correctness ... 
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__default) {
    cmp_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg; //! ie, the 'type' correspoding to this cluster-compairson-metric: used to select the "minDist" in [above] equation
  }
  e_kt_api_dynamicKMeans_t metric_centroid = self->config.centroidMetric__vertexVertex__center;
  if(metric_centroid == e_kt_api_dynamicKMeans_undef) {metric_centroid = e_kt_api_dynamicKMeans_AVG;}
  //! ------------------
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t vertexVertex__networkReference__between = self->config.centroidMetric__vertexVertex__networkReference__between;
  if(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef) {
    vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex; //! ie, 'centroid-->global'.     
    //vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid; //! ie, 'centroid-->global'.     
  }
  //! ------------------
  e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t vertexVertex__networkReference__within = self->config.centroidMetric__vertexVertex__networkReference__within;
  if(vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef) {
    vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex; //! ie, 'vertex-->centroid'.
  }


  //!
  //! Apply logics:
  s_kt_matrix_ccm_matrixBased_t result_obj = compute__s_kt_matrix_ccm_matrixBased(self, cmp_type, isTo__useInternalObjectSpec, distance_clusterCluster_internalSimMetric, isTo__useInternalObjectSpec, metric_centroid, vertexVertex__networkReference__between, vertexVertex__networkReference__within);
  t_float sumOf_result__between = result_obj.sumOf_result__between;     t_float sumOf_result__within = result_obj.sumOf_result__within;
  //#include "kt_matrix_cmpCluster__ccmMatrix__api.c"

  //!
  //! Combine the results:
  //printf("sumOf_result__within=%f, sumOf_result__between=%f, at %s:%d\n", sumOf_result__within, sumOf_result__between, __FILE__, __LINE__);
  if( isOf_interest(sumOf_result__between) && isOf_interest(sumOf_result__within) && (sumOf_result__within != 0) && (sumOf_result__between != 0) ) { //! @return:
    assert(self->cnt_cluster > 1);
    /* if(nrows < self->cnt_cluster) { */
    /*   fprintf(stderr, "!!(odd-usage)\t Seems like you have requested an evlauation where nrows(%u) !> cnt_clusters(%u). If this is what you intended, then please ingore this observaiton. Oderwhise we suggesting invesitgiating the latter. OBservation at [%s]:%s:%d\n", nrows, self->cnt_cluster, __FUNCTION__, __FILE__, __LINE__); */
    /*   assert(false); // TODO: consider removing this */
    /* } */

    {
      const uint nrows = self->nrows; assert(nrows > 0);       const uint ncols = self->ncols; 
      int cnt_diff = (int)nrows - (int)self->cnt_cluster;
      if(cnt_diff < 1) {cnt_diff = cnt_diff * -1;} else if(cnt_diff == 0) {cnt_diff = 1;}
      const t_float frac = (t_float)(mathLib_float_abs((t_float)cnt_diff))/((t_float)self->cnt_cluster - 1.0);
      //assert(frac != 0.0); //! ie, what we expect.
      const t_float sumOf_result = (sumOf_result__between/sumOf_result__within)*frac; //! ie, the 'empty' score.
      //printf("sumOf_result=%f, at %s:%d\n", sumOf_result, __FILE__, __LINE__);
      //  assert(false); // FIXME: remove
      return sumOf_result;
    }/*  else { */
    /*   //return T_FLOAT_MAX; //! ie, as we then assume taht 'no itneresting values were evlauted'. */
    /* } */
  } else {return T_FLOAT_MAX;} //! ie, as we then assume taht 'no itneresting values were evlauted'.
}

//! A wrapper to comptue a gnerlaized matrix:
#define __MiF__ccmMatrix s_kt_matrix_ccm_matrixBased_t ret_object = init__s_kt_matrix_ccm_matrixBased(); const bool is_ok = compute__generic__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, /*isTo_adjustEachRowScore_by_log=*/false, /*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*isTo__useInternalObjectSpec=*/false, &ret_object); if(is_ok == false) {return T_FLOAT_MAX;} 



//const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_type, 
t_float compute__SSE__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric, const bool isTo__useInternalObjectSpec) {
  assert(self);
  //printf("at %s:%d\n", __FILE__, __LINE__);
    //! Note[equation]: $ ((between - within)/|V|) * \sqrt((between * within)/(sum-of-scores)) $, where ambiguty/unsertinty/intpretiaotn-dpeendcy depedns upon correctness of the $|V|$ and $sum-of-score$ de-numeraotrs ... an issue which we address/elvauate wrt. ....??... 
    //! 
    //! Comptue between--within relationship, ie, using a macor -fucntion to ease readibliy:
    __MiF__ccmMatrix;
    //! 
    //! Apply lgocis which is specific for this CCM-metric:
    t_float scalar_within = mathLib_float_abs(ret_object.sumOf_result__within); 
    t_float scalar_between = mathLib_float_abs(ret_object.sumOf_result__between);

    //    if(scalar_between != scalar_within) 
    // printf("SSE: score=%f, at %s:%d\n", scalar_within, __FILE__, __LINE__);
    if(isOf_interest(scalar_within)) {
      if(true) { //! then adjust the score to relative (oekseth, 06. sept. 2017): 
	// TODO: vlaidte correnctess of [”elow]:
	if(ret_object.sumScores_total != 0) {
	  assert(isOf_interest(ret_object.sumScores_total));
	  //	  sumOf_result__between /= result_obj.sumScores_total; 
	  scalar_within /= ret_object.sumScores_total;
	  //! 
	  return scalar_within; // / cnt_combinations;
	}  else {
	  return 0;
	}
      } else {
      //!
      //! Step(3): comptue teh score:
      // FIXME[conceptual]: validte [”elow] adjcuemtn-aprpaoch ... 
	const uint cnt_combinations = self->nrows * self->nrows;
      // FIXME[conceptaul]: validate correness of [”elow] interropeation wrt. the 'deviors':
      // printf("SSE: score=%f, at %s:%d\n", scalar_within, __FILE__, __LINE__);
	return scalar_within / cnt_combinations;
      }
      /* const t_float sum_total = scalar_within + scalar_between; */
      /* const t_float cnt_vertices = (t_float)self->nrows;       */
      /* t_float result_1 = (scalar_between - scalar_within) / cnt_vertices; */
      /* t_float result_2 = mathLib_float_sqrt(scalar_between * scalar_within / sum_total); */
      /* //! @return: */
      /* return result_1 * result_2; */
    } else {return T_FLOAT_MAX;} //! ie, as the scres where then Not set.        

  /* const uint nrows = self->nrows; assert(nrows > 0); */
  /* const uint ncols = self->ncols;  */
  /* if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__default) { */
  /*   cmp_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg; //! ie, the 'type' correspoding to this cluster-compairson-metric: used to select the "minDist" in [above] equation */
  /* } */

  /* /\* if(nrows != ncols) { *\/ */
  /* /\*   fprintf(stderr, "!!\t Option Not supporeted: pelase reuqest for comptuignt his CCM-metrcis for a non-adjcuecney-matrix, ie, given amtrix-diemsions=[%u, %u], at [%s]:%s:%s:%d\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__); *\/ */
  /* /\*   assert(false); *\/ */
  /* /\*   return T_FLOAT_MAX; *\/ */
  /* /\* } *\/ */
  /* assert(self->input_matrix != NULL); */
  /* // FIXME: comapre 'this appraoch' to other appraoches, eg, wrt. VRC. */

  /* //!  */
  /* //! Step(0): Find sum-of-scores: */
  /* t_float sumOf_scores = 0; */
  /* // FIXME: update [below] ... 'fidn out' how to correct 'set this' ... eg, by gnerlaizing the VRC-comtpautiosn ... into a new dat-struct ... udpating oru aritlce ... usign the vRC-appraoch for 'all' appraoche s.... impemtnaiton altter in a HPC-otpmzied 'manner' ....  */
  /* // FIXME: use a differnet 'aprpaoch' than [”elow] ... ientroduce/use/provide an 'adjcnecy-otpion' ...  */
  /* /\*  *\/ */
  /* /\* uint mappings_cluster2_size = 0; uint *mappings_cluster2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_out, &mappings_cluster2_size); *\/ */
  /* uint *mapOf_vertexClusterIds = self->mapOf_vertexClusterIds; */
  /* //if(mappings_cluster2_size && mappings_cluster1_size)  */
  /* { */
  /*   if(self->nrows == self->ncols) {  */
  /*     for(uint i = 0; i < self->nrows; i++) { */
  /* 	const uint ind1 = mapOf_vertexClusterIds[i]; //mappings_cluster1[i]; assert(ind1 != UINT_MAX); */
  /* 	//assert(ind1 < self->nrows);  */
  /* 	for(uint k = 0; k < self->ncols; k++) { */
  /* 	  const uint ind2 = mapOf_vertexClusterIds[k]; //mappings_cluster2[k]; assert(ind2 != UINT_MAX); */
  /* 	  //assert(ind2 < self->ncols); */
  /* 	  if(ind2 != ind1) { //! then the between-score is set: */
  /* 	    if(isOf_interest(self->input_matrix[i][k])) {sumOf_scores += self->input_matrix[i][k];} */
  /* 	  } */
  /* 	} */
  /*     } */
  /*   } else { */
  /*   // FIXME: how are we to handle this case? */
  /*     assert(false); // FIXME: then add support for this. */
  /*   } */
  /* } /\* else { *\/ */
  /* /\*   // FIXME: how are we to handle this case? *\/ */
  /* /\*   assert(false); // FIXME: then add support for this. *\/ */
  /* /\* } *\/ */
  /* //printf("sumOf_scores=%f at %s:%d\n", sumOf_scores, __FILE__, __LINE__); */
  /* s_kt_matrix_t obj_result_clusterToVertex; setTo_empty__s_kt_matrix_t(&obj_result_clusterToVertex); */

  /* //! */
  /* //! Step(1): find center-points:  */
  /* //if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef)  */
  /* e_kt_api_dynamicKMeans_t metric_centroid = self->config.centroidMetric__vertexVertex__center; */
  /* if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) { */
  /*   if(metric_centroid == e_kt_api_dynamicKMeans_undef) {     */
  /*     metric_centroid = e_kt_api_dynamicKMeans_AVG; */
  /*   } */
  /* } */
  /* if(metric_centroid != e_kt_api_dynamicKMeans_undef) { */
  /*   s_kt_matrix_t obj_1; setTo_empty__s_kt_matrix_t(&obj_1); */
  /*   obj_1.nrows = self->nrows;  obj_1.ncols = self->nrows; obj_1.matrix = self->input_matrix; */
  /*   uint *mapOf_vertexClusterIds = self->mapOf_vertexClusterIds; */
  /*   assert(mapOf_vertexClusterIds); */
  /*   assert(obj_result_clusterToVertex.nrows == 0); //! ie, as we expect 'this object' to Not have been intalized. */
  /*   assert(obj_result_clusterToVertex.matrix == NULL); //! ie, as we expect 'this object' to Not have been intalized. */
  /*   const bool is_ok = kt_matrix__centroids(&obj_1, mapOf_vertexClusterIds, &obj_result_clusterToVertex, self->cnt_cluster, /\*typeOf_centroidAlg=*\/metric_centroid); //! deifned in our "kt_api.h" */
  /*   assert(is_ok); */
  /*   setTo_empty__s_kt_matrix_t(&obj_1); //! ie, clear 'odl cofnigruaitons'. */
  /* } //! else we asusme the feuatres may be used 'direclty', ie, a saster though mroe 'crude' appraoch. */
  /* //!  */
  /* //t_float sumOf_scores_within_features = 0;   */
  /* //t_float sumOf_scores_within = 0;   */
  /* t_float sumOf_result__between = 0;     t_float sumOf_result__within = 0; */
  /* //! */
  /* //! Provide logics for the c'ase' where we are interested in describing/using the 'proerpties of the distirbution'. */
  /* // TODO[aritcle]: consider adding support for Student-t <--- how may be use knowledge/value of ""bothtails", "lefttail" and "righttail"? */
  /* // TODO[aritcle]:  cosndier adding support for different 'proerpteis' of dsitributions <-- how may we then 'translate' a dsitribution-of-valeus intoa  'proeprty'? */
  /* const long long int current1dList_size = (long long int)self->nrows * (long long int)self->nrows;  */
  /* const t_float empty_0 = 0;  t_float *current1dList = NULL; uint current1dList__currentPos = 0;  //! strucutres among others used in the post-procesisng-ops wr.t our "__clusterConsistency__postProcessScore(..)" macro-call. */
  /* if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {current1dList  = allocate_1d_list_float(current1dList_size, empty_0); assert(current1dList);} */
  /* //! */
  /* //! Step(2): Find sum of distances: */
  /* //  printf("at %s:%d\n", __FILE__, __LINE__); */
  /* for(uint cluster_id_in = 0; cluster_id_in < self->cnt_cluster; cluster_id_in++) { */
  /*   t_float *row_centroid = NULL; */
  /*   if(metric_centroid != e_kt_api_dynamicKMeans_undef) { */
  /*   //if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) { */
  /*     assert(obj_result_clusterToVertex.matrix); */
  /*     row_centroid = obj_result_clusterToVertex.matrix[cluster_id_in]; */
  /*     assert(row_centroid);	     */
  /*     // for(uint m = 0; m < self->ncols; m++) {printf("\t center[%u]=%f, at %s:%d\n", m, row_centroid[m], __FILE__, __LINE__);} */
  /*   } */
  /*   uint mappings_cluster1_size = 0; uint *mappings_cluster1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_in, &mappings_cluster1_size); */
  /*   if(mappings_cluster1_size > 0) { */
  /*     assert(mappings_cluster1); assert(self->input_matrix); */
  /*     uint cnt_udpated = 0; */
  /*     //! Identify the 'default' score:     */
  /*     t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score: */
  /*     //! */
  /*     //! Iterate: */
  /*     //printf("clust[%u]\tmappings_cluster1_size=%u, at %s:%d\n", cluster_id_in, mappings_cluster1_size, __FILE__, __LINE__); */
  /*     //  printf("at %s:%d\n", __FILE__, __LINE__); */
  /*     for(uint i = 0; i < mappings_cluster1_size; i++) { */
  /* 	const uint ind1 = mappings_cluster1[i]; assert(ind1 != UINT_MAX); */
  /* 	assert(ind1 < self->nrows);  */
  /* 	//t_float score = row_centroid[ind1];  */
  /* 	t_float score = 0; */
  /* 	if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) { */
  /* 	  assert(row_centroid); */
  /* 	  score = kt_compare__each__maskImplicit__useRowPointersAsInput(distance_clusterCluster_internalSimMetric, self->config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, self->ncols, self->input_matrix[ind1], row_centroid); */
  /* 	  //printf("\t\t score=%f, at %s:%d\n", score, __FILE__, __LINE__); */
  /* 	} else { //! then we 'for simplcity in oru lgoics' find the 'extreme-score' */

  /* 	  // FXIEM: vlaidte c */

  /* 	  assert(self->ncols != 0); */
  /* 	  for(uint k = 0; k < self->ncols; k++) { */
  /* 	    if(metric_centroid != e_kt_api_dynamicKMeans_undef) { */
  /* 	      // TODO[conceptaul]: cosnider dopping [”ellw] step. */
  /* 	      //! Udpate the score: */
  /* 	      assert(row_centroid); */
  /* 	      t_float min_score = score; t_float candidate_score = row_centroid[k]; */
  /* 	      if(isOf_interest(candidate_score)) {score += candidate_score;} */
  /* 	      printf("\t\t score=%f, at %s:%d\n", score, __FILE__, __LINE__); */
  /* 	      //score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used. */
  /* 	    } else { */
  /* 	      // TODO[conceptaul]: cosnider dopping [”ellw] step. */
  /* 	      //! Udpate the score: */
  /* 	      t_float min_score = score; t_float candidate_score = self->input_matrix[ind1][k]; */
  /* 	      if(isOf_interest(candidate_score)) {score += candidate_score;} */
  /* 	      // printf("\t\t score=%f, at %s:%d\n", score, __FILE__, __LINE__); */
  /* 	      //score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used. */
  /* 	    } */
  /* 	  } */
  /* 	} */
  /* 	//! Unfiy: */
  /* 	if(score != T_FLOAT_MAX) { */
  /* 	  const t_float candidate_score = score; */
  /* 	  //sumOf_result__between += innerClusterDist__between; */
  /* 	  //! Udpate the score: */
  /* 	  min_score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used. */
  /* 	} */
  /* 	//! Idnietyf the within-score, using a 'simple' appraoch: */
  /* 	/\* for(uint k = 0; k < self->ncols; k++) { *\/ */
  /* 	/\*   if(isOf_interest(self->input_matrix[i][k])) {sumOf_scores -= self->input_matrix[i][k];} //! ie, to 'remvoe' the cluster-within-scores. *\/ */
  /* 	/\* }	   *\/ */
  /*     } */
  /*     if(cnt_udpated > 0) { */
  /* 	if( (min_score != T_FLOAT_MAX) && (min_score != T_FLOAT_MIN_ABS) && (min_score != 0) ) { */
  /* 	  if(isTo_adjustEachRowScore_by_log) { */
  /* 	    if(min_score != 0) {	 */
  /* 	      min_score += mathLib_float_log_abs(min_score); */
  /* 	    } */
  /* 	  } */
  /* 	  //! Update the score: */
  /* 	  __clusterConsistency__postProcessScore();  */
  /* 	  assert(min_score != T_FLOAT_MAX); */
  /* 	  sumOf_result__within += min_score; */
  /* 	}  */
  /*     } */
  /*   } */
  /* } */
  /* //! De-allocate: */
  /* free__s_kt_matrix(&obj_result_clusterToVertex); */
  /* if(current1dList) {free_1d_list_float(&current1dList); current1dList = NULL;} //! then de-allcoat ethe lcoal list of scores. */

  /* printf("sumOf_result__within=%f, sumOf_scores=%f, at %s:%d\n", sumOf_result__within, sumOf_scores, __FILE__, __LINE__); */
  /* assert(false); // FIXME: rremove. */
  
  /* //! */
  /* //! Step(3): comptue teh score:  */
  /* if(sumOf_scores == 0) {return 0;} //! ie, as we then assuemt that all vertices agrree wrt. their simalirty, ie, a sepical case where SSE (seems for many sysntietc cases) to be optmial. (oekseht, 06. jun. 2017). */
  /* t_float scalar_result = T_FLOAT_MAX; */
  /* if( (sumOf_result__within != 0) && (sumOf_scores != 0) ) { */
  /*   return sumOf_result__within / sumOf_scores; */
  /* } */





  /* return scalar_result; */
}

/**
   @brief compute the cluster-comparison-metric of: Silhouette (oekseth, 06. des. 2016)
   @return the assicated cluster-cosnistency-score
 **/
t_float compute__Silhouette__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log) {
  //! Compute[Silhouette]: (1/cnt-clusters)*sum{(minDist - vDist[i][clustId(i)])/max{minDist, vDist[i][clustId(i)]}}, where "minDist = min{vDist[i][k != clustId(i)]"
  assert(self);
  assert(self->nrows != UINT_MAX);   assert(self->nrows > 0);
  assert(self->cnt_cluster != UINT_MAX);   assert(self->cnt_cluster > 0);
  const uint ncols = self->ncols; 
  if(self->nrows != ncols) {
    fprintf(stderr, "!!\t Option Not supporeted: pelase reuqest for comptuignt his CCM-metrcis for a non-adjcuecney-matrix, ie, given amtrix-diemsions=[%u, %u], at [%s]:%s:%d\n", self->nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return T_FLOAT_MAX;
  }
  if(self->cnt_cluster == 1) {
    // TODO[article]: consuder udpating our aritlce wr.t this 'exepction-case'.
    //printf("at %s:%d\n", __FILE__, __LINE__);
    return T_FLOAT_MAX; //! ie, as we then assume the 'clusters are consistnet' (oesketh, 06. des. 2016)
  }
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__default) {
    cmp_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__min; //! ie, the 'type' correspoding to this cluster-compairson-metric: used to select the "minDist" in [above] equation
  }
  t_float sumOf_result = 0; //! ie, to hold the 'combined sum'.
  
  //!
  //! Provide logics for the c'ase' where we are interested in describing/using the 'proerpties of the distirbution'.
  // TODO[aritcle]: consider adding support for Student-t <--- how may be use knowledge/value of ""bothtails", "lefttail" and "righttail"?
  // TODO[aritcle]:  cosndier adding support for different 'proerpteis' of dsitributions <-- how may we then 'translate' a dsitribution-of-valeus intoa  'proeprty'?
  const long long int current1dList_size = (long long int)self->nrows * (long long int)self->nrows; 
  const t_float empty_0 = 0;  t_float *current1dList = NULL; uint current1dList__currentPos = 0; 
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {current1dList  = allocate_1d_list_float(current1dList_size, empty_0); assert(current1dList);}

  // FIXME: write a eprmtaution of Silhouette CCM .... where we use Silhoutte as an altentive strategy in "k-means-mini-batch" ... 

  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    //! Identify the 'default' score:    
    t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score:

    //!
    //! Start the evlaution:
    const uint row_id_clusterId = self->mapOf_vertexClusterIds[row_id];
    if(row_id_clusterId != UINT_MAX) {
      assert(row_id_clusterId != UINT_MAX);
      //! Find the min-score for the 'other clsuters':
      uint cnt_udpated = 0;
      //printf("---------, at %s:%d\n", __FILE__, __LINE__);
      for(uint cluster_id = 0; cluster_id < self->cnt_cluster; cluster_id++) {
	//printf("\t Compare cluster-id %u VS %u, at %s:%d\n", cluster_id, row_id_clusterId, __FILE__, __LINE__);
	if(cluster_id != row_id_clusterId) {
	  const t_float candidate_score = self->vertexTo_clusters[row_id][cluster_id];
	  // printf("\t Compare cluster-id %u VS %u, candidate_score=%f, at %s:%d\n", cluster_id, row_id_clusterId, candidate_score, __FILE__, __LINE__);
	  //! Udpate the score:
	  min_score = __clusterConsistency__updateScore(); //! eg, for "min_score = min{min_score, candidate_score}" if the 'min-proeprty' is used.
	}
      }
      //printf("[%u] min_score=%f, at %s:%d\n", row_id, min_score, __FILE__, __LINE__);
      //! Post-process the score:
      //    sumOf_result +=
      if(cnt_udpated > 0) {
	if(min_score != T_FLOAT_MAX) {
	  __clusterConsistency__postProcessScore(); 
	  //!
	  /*! Update the 'local sum':*/					
	  const t_float innerScore_cluster = self->vertexTo_clusters[row_id][row_id_clusterId]; 
	  if(innerScore_cluster != T_FLOAT_MAX) {
	    /*! Note: in [”elow] we use the 'abs' to 'properly handle' our differnet permtuations of the "Silhouette index":*/ 
	    const t_float numerator = mathLib_float_abs(min_score - innerScore_cluster);  //! ie, 
	    const t_float deNum = macro_max(min_score, innerScore_cluster);	
	    const t_float sum_local = __clusterConsistency__postProcessScore__numeratorDenum();    
	    //assert_possibleOverhead(isinf(sum_local) == false);
	    assert(sum_local != T_FLOAT_MAX);
	    sumOf_result += sum_local;
	  }
	}
      } //! else we assume 'the cluster did Not have any elements with count != 0
    } //! else we asume the 'cluster-mbmership fo the vertex si abmuigious' (ie, Not defined).
  }
  if(sumOf_result != 0) { //! Adjust the score 'by the total count':
    // printf("[Silhouette]:\t sumOf_result=%f, nrows=%u, at %s:%d\n", sumOf_result, self->nrows, __FILE__, __LINE__);
    sumOf_result = sumOf_result / self->nrows;
  }
  
  if(current1dList) {free_1d_list_float(&current1dList); current1dList = NULL;} //! then de-allcoat ethe lcoal list of scores.

  // printf("sumOf_result=%f, nrows=%u, at %s:%d\n", sumOf_result, self->nrows, __FILE__, __LINE__);

  //! @return:
  return sumOf_result;
}
/**
   @brief compute the cluster-comparison-metric of: Davis-Bouldin (oekseth, 06. des. 2016)
   @return the assicated cluster-cosnistency-score
   @remarks 
   -- case="distance_clusterCluster_internalSimMetric == undef": then use t_float dist_between = self->cluster_cluster[cluster_id_in][cluster_id_out]; //! then then use the distance 'specifed' in the init-function of the "s_kt_matrix_cmpCluster_clusterDistance_t" object.
 **/
t_float compute__DavisBouldin__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustEachRowScore_by_log, const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric) {
  //! Compute[DavisBouldin]: (1/cnt-clusters)*sum_{cluster_id=i}(max{(c[i][i] + c[j][j])/c[i][j]})
  assert(self);
  assert(self->nrows != UINT_MAX);   assert(self->nrows > 0);
  assert(self->cnt_cluster != UINT_MAX);   assert(self->cnt_cluster > 0);
  assert(self->cluster_cluster);
  assert(self->cluster_cluster__count);
  const uint ncols = self->ncols; 
  if(self->nrows != ncols) {
    fprintf(stderr, "!!\t Option Not supporeted: pelase reuqest for comptuignt his CCM-metrcis for a non-adjcuecney-matrix, ie, given amtrix-diemsions=[%u, %u], at [%s]:%s:%d\n", self->nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return T_FLOAT_MAX;
  }
  if(self->cnt_cluster == 1) {
    // TODO[article]: consuder udpating our aritlce wr.t this 'exepction-case'.
    return T_FLOAT_MAX; //! ie, as we then assume the 'clusters are consistnet' (oesketh, 06. des. 2016)
  }
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__default) {
    cmp_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__max; //! ie, the 'type' correspoding to this cluster-compairson-metric: refers to the 'inner' "max{...}" in [ªbove] equation.
  }
  //!
  //! Provide logics for the c'ase' where we are interested in describing/using the 'proerpties of the distirbution'.
  const long long int current1dList_size = (long long int)self->nrows * (long long int)self->nrows; 
  const t_float empty_0 = 0;  t_float *current1dList = NULL; uint current1dList__currentPos = 0; 
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {current1dList  = allocate_1d_list_float(current1dList_size, empty_0); assert(current1dList); }

  //!
  //! Iterate in order to compute the result:
  t_float sumOf_result = 0; //! ie, to hold the 'combined sum'.  
  for(uint cluster_id_in = 0; cluster_id_in < self->cnt_cluster; cluster_id_in++) {
    //! Identify the 'default' score:    
    t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score:

    uint mappings_cluster1_size = 0; uint *mappings_cluster1 = NULL;
    if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) { //! then we assuem the user has 'epxlcitly' requested a compliation/usage 'of this'.
      //! Note: as [”elow] is a pointer to the objec t(and Not new-allocated) we do Not de-allcoate (in thsi code-chunk) the 'pointer-reference' 
      mappings_cluster1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_in, &mappings_cluster1_size);
    }

    //! Start the evlaution:
    //! Note: Find the min-score for the 'other clsuters':
    uint cnt_udpated = 0;
    for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
      if(cluster_id_out != cluster_id_in) {
	assert(self->cluster_cluster__count);
	const uint count_1 = self->cluster_cluster__count[cluster_id_in][cluster_id_in];
	const uint count_2 = self->cluster_cluster__count[cluster_id_out][cluster_id_out];
	if(count_1 && count_2) {
	  assert(count_1 >= 1); 	assert(count_2 >= 1);
	  // FIXME: cosnider to 'make' [”elow] optional ... testing the effect of this 'relative ajdustment wrt. weights'. .... and/or to Not divide by "count_1" and "count_2" ... 
	  const t_float innerClusterDist__in = self->cluster_cluster[cluster_id_in][cluster_id_in]/(t_float)count_1;
	  const t_float innerClusterDist__out = self->cluster_cluster[cluster_id_out][cluster_id_out]/(t_float)count_2;
	  //!
	  //! Compute: 
	  t_float dist_between = self->cluster_cluster[cluster_id_in][cluster_id_out]; //! then then use the distance 'specifed' in teh inti-function of the "s_kt_matrix_cmpCluster_clusterDistance_t" object.
	  if(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef) { //! then we assuem the user has 'epxlcitly' requested a compliation/usage 'of this'.
	    //! Note: as [”elow] is a pointer to the objec t(and Not new-allocated) we do Not de-allcoate (in thsi code-chunk) the 'pointer-reference' 
	    uint mappings_cluster2_size = 0; uint *mappings_cluster2 = NULL;
	    mappings_cluster2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_out, &mappings_cluster2_size);
	    if(mappings_cluster2_size && mappings_cluster1_size) {
	      //! The center-point:	      
	      /* s_kt_matrix_t obj_result_clusterToVertex; setTo_empty__s_kt_matrix_t(&obj_result_clusterToVertex); */
	      /* e_kt_api_dynamicKMeans_t metric_centroid = self->config.centroidMetric__vertexVertex__center; */
	      /* if(metric_centroid == e_kt_api_dynamicKMeans_undef) {metric_centroid = e_kt_api_dynamicKMeans_AVG;} */
	      /* s_kt_matrix_t obj_1; setTo_empty__s_kt_matrix_t(&obj_1); */
	      /* assert(self->input_matrix != NULL); */
	      /* obj_1.nrows = self->nrows;  obj_1.ncols = self->nrows; obj_1.matrix = self->input_matrix; */
	      /* uint *mapOf_vertexClusterIds = self->mapOf_vertexClusterIds; */
	      /* assert(obj_result_clusterToVertex.nrows == 0); //! ie, as we expect 'this object' to Not have been intalized. */
	      /* assert(obj_result_clusterToVertex.matrix == NULL); //! ie, as we expect 'this object' to Not have been intalized. */
	      /* //! */
	      /* //! Compute: */
	      /* const bool is_ok = kt_matrix__centroids(&obj_1, mapOf_vertexClusterIds, &obj_result_clusterToVertex, self->cnt_cluster, /\*typeOf_centroidAlg=*\/metric_centroid); //! deifned in our "kt_api.h" */
	      /* assert(is_ok); */
	      /* setTo_empty__s_kt_matrix_t(&obj_1); //! ie, clear 'odl cofnigruaitons'. */

	      // FIXME[conceptaul+code]: merge a permataution fo [ªbvboe] with the VRC proceudre ... 

	      //!
	      //! Compute:
	      // FIXMe[conceptaul]: add support for difnfere topitons wrt. the "e_kt_clusterComparison_mean_artimethic" option ... and 'merge this' with our "VRC" procuedre ... ie,w ehre we .....??....
	      assert(distance_clusterCluster_internalSimMetric != e_kt_correlationFunction_undef);
	      const t_float score = clusterdistance__extensiveParams__enumMethod__simplified(distance_clusterCluster_internalSimMetric, //config.metric_complexClusterCmp_obj.metric_id, 
											     e_kt_categoryOf_correaltionPreStep_none, //config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, 
											     self->nrows, self->nrows, 
											     self->input_matrix, /*mask=*/NULL, /*weight=*/NULL,
											     /*n1=*/mappings_cluster1_size,
											     /*n2=*/mappings_cluster2_size,
											     /*index1=*/mappings_cluster1,
											     /*index2=*/mappings_cluster2,
											     /*enum_id_method=*/
											     e_kt_clusterComparison_mean_artimethic //! as defined in our "e_kt_clusterComparison.h"
											     //config.metric_complexClusterCmp_clusterMethod
											     );
	    
	      //! Upodate:
	      dist_between = score;
	    } else {
	      dist_between = 0; //! ie, as we are then intersted in 'ingoring this'.
	    }
	  }

	  const t_float numerator = innerClusterDist__in + innerClusterDist__out;
	  if( (numerator != 0) && (dist_between != 0) ) {
	    const t_float candidate_score = numerator / dist_between;
	    //! Udpate the score:
	    //! Note: In [”elow] "max(..)" is comptue when the 'defualt' option is used: max
	    min_score = __clusterConsistency__updateScore();
	  }
	} //! else we assume one of the clsuters does not ahve any data, ie, potinelss to comapre
      }
    }
    //! Post-process the score:
    if( (cnt_udpated > 0) && (min_score != T_FLOAT_MAX) ) {
      __clusterConsistency__postProcessScore();
      //!
      /*! Update the 'local sum':*/					
      /*! Note: in [”elow] we use the 'abs' to 'properly handle' our differnet permtuations of the "Silhouette index":*/ 
      if(isTo_adjustEachRowScore_by_log) {
	if(min_score != 0) {	
	  sumOf_result += mathLib_float_log_abs(min_score);
	}
      } else {
	sumOf_result += min_score;
      }
    } //! else we assume 'the cluster did Not have any elements with count != 0
  }
  //!
  //! Complete:
  //printf("sumOf_result=%f, at %s:%d\n", sumOf_result, __FILE__, __LINE__);
  if(sumOf_result != 0) { //! Adjust the score 'by the total count':
    sumOf_result = sumOf_result / self->cnt_cluster;
  } else {sumOf_result = T_FLOAT_MAX;} //! ie, as we then assume the result is Not set.

  if(current1dList) {free_1d_list_float(&current1dList); current1dList = NULL;} //! then de-allcoat ethe lcoal list of scores.
  //  assert(false); // FIXME: rremove.
  //! @return:
  return sumOf_result;
}
/**
   @brief compute the cluster-comparison-metric of: Dunn's Validation index (oekseth, 06. des. 2016)
   @return the assicated cluster-cosnistency-score
   @remarks a 'non-configurable' property of our "Dunns Index" implementaiton conserns is the use of a 'max-proeprty' wrt. the 'result-sums', ie, as seen in our implementaiton wrt. the "T_FLOAT_MAX" and "macro_min(..)".
 **/
t_float compute__Dunns__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type, const bool isTo_adjustInternalByMax) {
  //! Compute[Dunn]: min{min{c[i][j] / max_clustSize}}, where "max_clustSize = max{c[i][i]}"
  assert(self);
  assert(self->nrows != UINT_MAX);   assert(self->nrows > 0);
  assert(self->cnt_cluster != UINT_MAX);   assert(self->cnt_cluster > 0);
  const uint ncols = self->ncols; 
  if(self->nrows != ncols) {
    fprintf(stderr, "!!\t Option Not supporeted: pelase reuqest for comptuignt his CCM-metrcis for a non-adjcuecney-matrix, ie, given amtrix-diemsions=[%u, %u], at [%s]:%s:%d\n", self->nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return T_FLOAT_MAX;
  }
  if(self->cnt_cluster == 1) {
    // TODO[article]: consuder udpating our aritlce wr.t this 'exepction-case'.
    // printf("\t return-score(Dunn)\t cnt_clusters=1, at %s:%d\n", __FILE__, __LINE__);
    return T_FLOAT_MAX; //! ie, as we then assume the 'clusters are consistnet' (oesketh, 06. des. 2016)
  }
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__default) {
    cmp_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__min; //! ie, the 'type' correspoding to this cluster-compairson-metric: refers to the 'inner' "min{...}" in [ªbove] equation.
  }
  // printf("# cmp_type=%u, at %s:%d\n", cmp_type, __FILE__, __LINE__);
  t_float max_inAGivenCluster = T_FLOAT_MIN_ABS;
  for(uint cluster_id_in = 0; cluster_id_in < self->cnt_cluster; cluster_id_in++) {
    const t_float innerClusterDist__in = self->cluster_cluster[cluster_id_in][cluster_id_in];
    // printf("\t(within)[%u]\t dist=%f, at %s:%d\n", cluster_id_in, innerClusterDist__in, __FILE__, __LINE__);
    if(innerClusterDist__in != 0) { max_inAGivenCluster = macro_max(max_inAGivenCluster, innerClusterDist__in);  }
  }
  if(max_inAGivenCluster == T_FLOAT_MIN_ABS) {
    // // TODO[article]: consuder udpating our aritlce wr.t this 'exepction-case'.
    if(false) {printf("\t return-score(Dunn)\t min-abs-is-max, given cnt_cluster=%u, at %s:%d\n", self->cnt_cluster, __FILE__, __LINE__);}
    return T_FLOAT_MAX; //! ie, as we then assume 'that all vertices ahve the exact same distance to each other'.
  }
  assert(max_inAGivenCluster != T_FLOAT_MIN_ABS); //! ie, as weotherwise have an incosnstecy.
  assert(max_inAGivenCluster != 0);

  //!
  //! Provide logics for the c'ase' where we are interested in describing/using the 'proerpties of the distirbution'.
  const long long int current1dList_size = (long long int)self->nrows * (long long int)self->nrows; 
  const t_float empty_0 = 0;  t_float *current1dList = NULL; uint current1dList__currentPos = 0; 
  if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {current1dList  = allocate_1d_list_float(current1dList_size, empty_0); assert(current1dList); }


  //!
  //! Iterate in order to compute the result:
  t_float sumOf_result = T_FLOAT_MAX; //__clusterConsistency__initScore(); //! ie, to hold the 'combined sum'.  
  //t_float sumOf_result = __clusterConsistency__initScore(); //! ie, to hold the 'combined sum'.  
  for(uint cluster_id_in = 0; cluster_id_in < self->cnt_cluster; cluster_id_in++) {
    //! Identify the 'default' score:    
    t_float min_score = __clusterConsistency__initScore(); //! ie, intalize the min-score:
    // printf("def_score=%f, at %s:%d\n", min_score, __FILE__, __LINE__);
    //! Start the evlaution:
    //! Note: Find the min-score for the 'other clsuters':
    uint cnt_udpated = 0;
    for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
      if(cluster_id_out != cluster_id_in) {
	/* const t_float innerClusterDist__in = self->cluster_cluster[cluster_id_in][cluster_id_in]; */
	/* const t_float innerClusterDist__out = self->cluster_cluster[cluster_id_out][cluster_id_out]; */
	t_float dist_between = self->cluster_cluster[cluster_id_in][cluster_id_out];
	const t_float deNum = max_inAGivenCluster;
	if( (deNum != 0) && (dist_between != 0) ) {
	  if(isTo_adjustInternalByMax) {
	    const t_float size_in = self->cluster_cluster[cluster_id_in][cluster_id_in];
	    const t_float size_out = self->cluster_cluster[cluster_id_out][cluster_id_out];
	    if(isOf_interest(size_in) && isOf_interest(size_out)) {
	      assert(size_in != T_FLOAT_MAX);
	      assert(size_out != T_FLOAT_MAX);
	      const t_float sizeMax_each_best = macro_max(mathLib_float_abs(size_in), mathLib_float_abs(size_out));
	      if(sizeMax_each_best != 0) {
		// FIXME[aritcle]: evlauate the 'effect' of [below] permtaution:
		dist_between = dist_between / sizeMax_each_best; //! ie, adjsut the score by the 'biggest center-gravitiy'.
	      }
	    }
	  }
	  const t_float candidate_score = dist_between / deNum;
	  //! Udpate the score:	  
	  min_score = __clusterConsistency__updateScore();
	  // printf("cluster[%u][%u] = %f, min_score=%f, at %s:%d\n", cluster_id_in, cluster_id_out, candidate_score, min_score, __FILE__, __LINE__);
	}
      }
    }
    if( (cnt_udpated > 0) && (min_score != T_FLOAT_MAX) ) {
      //printf("(before)sumOf_result=min(sum=%f, local=%f), cnt_udpated=%u, at %s:%d\n", sumOf_result, min_score, cnt_udpated, __FILE__, __LINE__);
      //! Post-process the score:
      __clusterConsistency__postProcessScore();
      //!
      /*! Update the 'local sum':*/					
      // printf("sumOf_result=min(sum=%f, local=%f), at %s:%d\n", sumOf_result, min_score, __FILE__, __LINE__);
      sumOf_result = macro_min(sumOf_result, min_score);
    }
  }
  // printf("\t return-score(Dunn)\t score=%f, at %s:%d\n", sumOf_result, __FILE__, __LINE__);
  //!
  //! Complete:
  //! Note:: <nothing in line, ie, iwaw. the algorithm>

  if(current1dList) {free_1d_list_float(&current1dList); current1dList = NULL;} //! then de-allcoat ethe lcoal list of scores.

  //! @return:
  return sumOf_result;  
}

/**
   @brief compute the cluster-comparison-metric of the matric specifid by enum_id (oekseth, 06. des. 2016)
   @param <self> is the intiated object which hold teh cluster-proerpties to use.
   @param <enum_id> is the type fo cluster-comparison-metric to apply.
   @param <cmp_type> describes different permtautions which may be used: if the 'naive appraoch' is to be used (ie, the appraoch often sued in researhc-littterature) we suggest: e_kt_matrix_cmpCluster_clusterDistance__config_metric__max
   @return the assicated cluster-cosnistency-score
 **/
t_float compute__s_kt_matrix_cmpCluster_clusterDistance_t(const s_kt_matrix_cmpCluster_clusterDistance_t *self, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_id, e_kt_matrix_cmpCluster_clusterDistance__config_metric__t cmp_type) {
  assert(self); // kt_matrix_cmpCluster.c:1470
#if(MiC__support_CCMs_clusterCountOne == 0)
  if( (self->cnt_cluster != UINT_MAX) ) {if(self->cnt_cluster <= 1) {return T_FLOAT_MAX;}} //! ie, as the clsuter-emmbershipw as then not set (oekseth, 06. jul. 2017).
  assert(self->cnt_cluster > 1);
#endif
  assert(self->cluster_cluster); //! ie, as we then asusem the latter ia allocated/set.
  // printf("## Comptue CCM-matrix-based, at %s:%d\n", __FILE__, __LINE__);
  if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC) {
    return compute__VRC__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, /*isTo_adjustEachRowScore_by_log=*/false, /*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*isTo__useInternalObjectSpec=*/false);
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC_relative__oekseth) {
    return compute__VRC__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, /*isTo_adjustEachRowScore_by_log=*/false, /*metric=*/e_kt_correlationFunction_undef, /*isTo__useInternalObjectSpec=*/true);
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette) {
    //printf("at %s:%d\n", __FILE__, __LINE__);
    return compute__Silhouette__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, /*isTo_adjustEachRowScore_by_log=*/false);
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin) {
    return compute__DavisBouldin__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, /*isTo_adjustEachRowScore_by_log=*/false, /*metric=*/e_kt_correlationFunction_undef); //! where the latter 'undef' is set as we expec tthe 'metric to have already been computed in the init-function'.,
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin__sumBetween) {
    return compute__DavisBouldin__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, /*isTo_adjustEachRowScore_by_log=*/false, /*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid);
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns_relative__oekseth) {
    const bool isTo_adjustInternalByMax = true;
    return compute__Dunns__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, isTo_adjustInternalByMax); //, /*isTo_adjustEachRowScore_by_log=*/false);
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns) {
    const bool isTo_adjustInternalByMax = false; //! ie, the defualt alternative
    return compute__Dunns__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, isTo_adjustInternalByMax); //, /*isTo_adjustEachRowScore_by_log=*/false);
  } else if(
	    (enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE)  //_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) 
	    /* ||  */
	    /* (enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum)  */
	    /* ||  */
	    /* (enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_scores_Euclid_within_divBy_betweenMax)  */
	    /* ||  */
	    /* (enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE_avg_ranks_Euclid_within_divBy_betweenMax)  */
	    //|| 
	    ) {
    return compute__SSE__s_kt_matrix_cmpCluster_clusterDistance_t(//enum_id, 
								  self, cmp_type, /*isTo_adjustEachRowScore_by_log=*/false,
								  /*metric=*/e_kt_correlationFunction_undef,
								  ///*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, 
								  /*isTo__useInternalObjectSpec=*/false);
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_PBM) {
    //! Note: one (of many) deinions of CCM=PBM is listed in 'https://me.iitp.ac.in/~sriparna/papers/naveen-cognitive.pdf':\cite{saini2019automatic}
    //t_float scalar_within = 0;     t_float scalar_between = 0;
    // FIXME[cocnpetual]: validte that [”elow] cofnigurion is correctly set ... ie, tindoect wrt. 'this' .... comapring to our euqiaotns/understiang ...  discuss with co-authrso ... 
    //! Note: equation: $max{d(c_i, c_k)} / (\sum\sum(x, c_k)*|C|)$
    s_kt_matrix_ccm_matrixBased_t ret_object = init__s_kt_matrix_ccm_matrixBased();
    const bool is_ok = compute__generic__s_kt_matrix_cmpCluster_clusterDistance_t(self, cmp_type, /*isTo_adjustEachRowScore_by_log=*/false, /*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*isTo__useInternalObjectSpec=*/false, &ret_object); // &scalar_within, &scalar_between);
    if(is_ok) {
      t_float scalar_within = ret_object.sumOf_result__within;     t_float scalar_between = ret_object.sumOf_result__between;
      t_float result_value = scalar_between / (scalar_within * self->cnt_cluster);
      return result_value;
    } else {return T_FLOAT_MAX;} //! ie, as the scres where then Not set.
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_cIndex) {
    // FIXME[cocnpetual]: validte that [”elow] cofnigurion is correctly set ... ie, tindoect wrt. 'this' .... comapring to our euqiaotns/understiang ...  discuss with co-authrso ... 
    // FIXME[norm]: udpate our nroamilziaotn-rotuine .. in our "kt_matrix.h" ... wrt. "c index" CCM ... ie, as latter ias 'acttully' a nraomziion-based appraoch to appraoximate the 'spread' in cluster-within-size ... 
    //! Note[equation]: $ within - min(withiin) / max(within) - min(within) $
    // FIXME: provide a detials ddesipciront of [”elow] ... udapitng oru aritlce-scaffold ... vlidting diffenre tappraoches for 'within' and 'between' .... where latter relatioinship is sued in ......??... to ....??...
    //! 
    //! Comptue between--within relationship, ie, using a macor -fucntion to ease readibliy:
    __MiF__ccmMatrix;
    //! 
    //! Apply lgocis which is specific for this CCM-metric:
    t_float scalar_within = 0.5*ret_object.sumOf_result__within; 
    t_float within_max = ret_object.within_min; 
    t_float within_min = ret_object.within_max; 
    if( (within_max != within_min) && (within_min != scalar_within) ) {
      if(true) { //! then adjust the score to relative (oekseth, 06. sept. 2017): 
	// TODO: vlaidte correnctess of [”elow]:
	if(ret_object.sumScores_total != 0) {
	  assert(isOf_interest(ret_object.sumScores_total));
	  //	  sumOf_result__between /= result_obj.sumScores_total; 
	  scalar_within /= ret_object.sumScores_total;
	  within_min /= ret_object.sumScores_total;
	  within_max /= ret_object.sumScores_total;
	} 
      }      
      //t_float scalar_between = ret_object.sumOf_result__between;
      assert(self->cnt_cluster > 1);      
      t_float result_value = (scalar_within - within_min) / (within_max - within_min);
      return result_value;
    } else {return T_FLOAT_MAX;} //! ie, as the scres where then Not set.    
    //  } else {return T_FLOAT_MAX;} //! ie, as the scres where then Not set.    
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_pointBiserial) {
    //! Note[equation]: $ ((between - within)/|V|) * \sqrt((between * within)/(sum-of-scores)) $, where ambiguty/unsertinty/intpretiaotn-dpeendcy depedns upon correctness of the $|V|$ and $sum-of-score$ de-numeraotrs ... an issue which we address/elvauate wrt. ....??... 
    //! 
    //! Comptue between--within relationship, ie, using a macor -fucntion to ease readibliy:
    __MiF__ccmMatrix;
    //! 
    //! Apply lgocis which is specific for this CCM-metric:
    t_float scalar_within = mathLib_float_abs(ret_object.sumOf_result__within); 
    t_float scalar_between = mathLib_float_abs(ret_object.sumOf_result__between);
    t_float sum_total = scalar_within + scalar_between;
    if(scalar_between != scalar_within) {
      if(true) { //! then adjust the score to relative (oekseth, 06. sept. 2017):
      	// TODO: vlaidte correnctess of [”elow]:
      	if(ret_object.sumScores_total != 0) {
      	  assert(isOf_interest(ret_object.sumScores_total));
      	  //	  sumOf_result__between /= result_obj.sumScores_total;
      	  /* scalar_within /= ret_object.sumScores_total; */
      	  /* scalar_between /= ret_object.sumScores_total; */
      	  /* sum_total /= ret_object.sumScores_total; */
	  scalar_within /= ret_object.sumScores_total;
	  scalar_between /= ret_object.sumScores_total;
	  sum_total /= ret_object.sumScores_total;
	  //      	  if(ret_val != 0) { ret_val /= (ret_object.sumScores_total); } // * ret_object.sumScores_total);}
      	}
      }
      // FIXME[conceptaul]: validate correness of [”elow] interropeation wrt. the 'deviors':
      const t_float cnt_vertices = (t_float)self->nrows;      
      t_float result_1 = (scalar_between - scalar_within) / cnt_vertices;
      t_float result_2 = mathLib_float_sqrt(scalar_between * scalar_within / sum_total);
      t_float ret_val =  result_1 * result_2;

      /* if(true) { //! then adjust the score to relative (oekseth, 06. sept. 2017):  */
      /* 	// TODO: vlaidte correnctess of [”elow]: */
      /* 	if(ret_object.sumScores_total != 0) { */
      /* 	  assert(isOf_interest(ret_object.sumScores_total)); */
      /* 	  //	  sumOf_result__between /= result_obj.sumScores_total;  */
      /* 	  /\* scalar_within /= ret_object.sumScores_total; *\/ */
      /* 	  /\* scalar_between /= ret_object.sumScores_total; *\/ */
      /* 	  /\* sum_total /= ret_object.sumScores_total; *\/ */
      /* 	  if(ret_val != 0) { ret_val /= (ret_object.sumScores_total); } // * ret_object.sumScores_total);} */
      /* 	}  */
      /* }       */
      //! @return:
      return ret_val;
    } else {return T_FLOAT_MAX;} //! ie, as the scres where then Not set.        
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_RMMSSTD) {
    //! Note[equation]: $ within / (cnt_vertices * cnt_vertices)$
    //! 
    //! Comptue between--within relationship, ie, using a macor -fucntion to ease readibliy:
    __MiF__ccmMatrix;
    //! 
    //! Apply lgocis which is specific for this CCM-metric:    
    const t_float cnt_vertices = (t_float)self->nrows;
    t_float scalar_within = mathLib_float_abs(ret_object.sumOf_result__within);     
    assert(isOf_interest(scalar_within));
    if(true) { //! then adjust the score to relative (oekseth, 06. sept. 2017): 
      // TODO: vlaidte correnctess of [”elow]:
      if(ret_object.sumScores_total != 0) {
	assert(isOf_interest(ret_object.sumScores_total));
	//	  sumOf_result__between /= result_obj.sumScores_total; 
	scalar_within /= (ret_object.sumScores_total );
	//	scalar_within /= (ret_object.sumScores_total * ret_object.sumScores_total);
      } 
      return scalar_within;
    } else {
      // FIXME[conceptaul]: validte correctness of [below] "cnt_vertices * cnt_vertices" ... ie, wrt. an [n, m] feature-matrix ... ie, as we 'compare' for/between vertex-memberships which are ...??...
      return (scalar_within / (t_float)(cnt_vertices * cnt_vertices));
    }
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_rSquared) {
    //! Note: in this example we illustrate/describe how a d(x, center) may be comptued using oru gneeric appraoches.
    //! Note[equation]: $ between - within/between $
    //! 
    //! Comptue between--within relationship, ie, using a macor -fucntion to ease readibliy:
    //__MiF__ccmMatrix;
    //! 
    //! Apply lgocis which is specific for this CCM-metric:    
    assert(self); 
    // assert(scalar_between); assert(scalar_within);
    assert(self);
    // printf("at %s:%d\n", __FILE__, __LINE__);
    if(self->cnt_cluster <= 1) {return T_FLOAT_MAX;} //! ie, as the 'score' is then Not defined 
    //! --------------------------------------------------------
    //!
    //! configure the interptaiton of these reuslts:
    // FIXME: validate correnctess of [”elwo] assignmetns/correctness ... 
    if(cmp_type == e_kt_matrix_cmpCluster_clusterDistance__config_metric__default) {
      cmp_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg; //! ie, the 'type' correspoding to this cluster-compairson-metric: used to select the "minDist" in [above] equation
    }
    e_kt_api_dynamicKMeans_t metric_centroid = self->config.centroidMetric__vertexVertex__center;
    if(metric_centroid == e_kt_api_dynamicKMeans_undef) {metric_centroid = e_kt_api_dynamicKMeans_AVG;}
    //! ------------------
    e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t vertexVertex__networkReference__between = self->config.centroidMetric__vertexVertex__networkReference__between;
    if(vertexVertex__networkReference__between == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef) {
      vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex; //! ie, 'vertex-->centroid'.
      // FIXME: cosndier setting [below] isntead of [ªbov€] as default ... where 'current' sie set to simplify comaprison between differnet CCMs ... 
      //vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexGlobal; //! ie, 'centroid-->global'.     
      //      vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex; //! ie, 'centroid-->global'.     
      //vertexVertex__networkReference__between = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_centroidCentroid; //! ie, 'centroid-->global'.     
    }
    //! ------------------
    e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_t vertexVertex__networkReference__within = self->config.centroidMetric__vertexVertex__networkReference__within;
    if(vertexVertex__networkReference__within == e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_undef) {
      vertexVertex__networkReference__within = e_kt_matrix_cmpCluster_typeOfNetworkReferencePoint_vertexVertex; //! ie, 'vertex-->centroid'.
    }
    

    //!
    //! Apply logics:
    const bool isTo__useInternalObjectSpec = false;
    // FIXME[api]: add support for using a user-deifned sim-metirc for [below]:
    const e_kt_correlationFunction_t distance_clusterCluster_internalSimMetric = e_kt_correlationFunction_groupOf_minkowski_euclid;
    //!
    s_kt_matrix_ccm_matrixBased_t result_obj = compute__s_kt_matrix_ccm_matrixBased(self, cmp_type, isTo__useInternalObjectSpec, distance_clusterCluster_internalSimMetric, isTo__useInternalObjectSpec, metric_centroid, vertexVertex__networkReference__between, vertexVertex__networkReference__within);
    t_float sumOf_result__between = result_obj.sumOf_result__between;     t_float sumOf_result__within = result_obj.sumOf_result__within;
    //#include "kt_matrix_cmpCluster__ccmMatrix__api.c"
    
    //!
    //! Combine the results:
    // printf("sumOf_result__within=%f, sumOf_result__between=%f, at %s:%d\n", sumOf_result__within, sumOf_result__between, __FILE__, __LINE__);
    if( isOf_interest(sumOf_result__between) && isOf_interest(sumOf_result__within) && (sumOf_result__within != 0) && (sumOf_result__between != 0) ) { //! @return:
      assert(self->cnt_cluster > 1);
      if(true) { //! then adjust the score to relative (oekseth, 06. sept. 2017): 
	// TODO: vlaidte correnctess of [”elow]:
	if(result_obj.sumScores_total != 0) {
	  assert(isOf_interest(result_obj.sumScores_total));
	  sumOf_result__between /= result_obj.sumScores_total; sumOf_result__within /= result_obj.sumScores_total;
	} 
      }      
      return sumOf_result__between - (sumOf_result__within / sumOf_result__between);
      /* *scalar_within  = sumOf_result__within; */
      /* *scalar_between = sumOf_result__between; */
      //return true;
    } else  {
      return T_FLOAT_MAX;
    }
    //! *****************************************************************
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_gammaMod) {   
    //! Note[equation]: $ $

    //    assert(false); 
// FIXMe[cocneptaul]: try to scafofld some genierc aprpaoche wrt. cluster-distnace-simalrity ... eg, [distance, count1/count2, count1*count2/cnt-vertices, ... ] ... 
    return T_FLOAT_MAX;assert(false); // FIXME: figure out how to 'generically handle' the $ d(x, y) * d(C_x, C_y) $ criteria/requiremtn ... eg, wrt. how the $d(C_x, C_y) $ is comptued ... eg, by adding/providing a new option to comptue a pre-distance-table ... ie, udpate our inti-funciotn ... cosntruct a new object ... then use a Silhoutte-permtaution-aproch when 'ermigng' reuslts ... 

    
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_iIndex) {
    //! Note[equation]: $ iIndex = \Huge(\frac{between}{within}*max\{d(C_x, C_y)\}\Huge)^2 $
    return T_FLOAT_MAX;assert(false); // FIXME: figure out how to 'generically handle' the $ max\{d(C_x, C_y)\}\Huge)^2 $ criteir/requirement .... eg, by pre-coptuignt eh "cluster_cluster" table ... then use/intrete the witin-score-distnace ... 

  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_xieBeni) {
    //! Note[equation]: $ XB = within / max(between) $
    return T_FLOAT_MAX;
    assert(false); // FIXMe[cocneptaul]: valdite/describe our 'max' rotuien wrt. the 'betwenees' comptautions .... and try to 'figure out' how/what the $ max\{d(C_x, C_y)\}\Huge)^2 $ criteir/requirement differns from $betweeness$ .... then update our "meta_ccm.tex" wrt. the "XB = " CCM-quation ... copy-pasting from our "i index" descrioption.
    //! 
    //! Comptue between--within relationship, ie, using a macor -fucntion to ease readibliy:
    __MiF__ccmMatrix;
    //! 
    //! Apply lgocis which is specific for this CCM-metric:    
    const t_float cnt_vertices = (t_float)self->nrows;
    t_float scalar_within = ret_object.sumOf_result__within;   t_float scalar_between = ret_object.sumOf_result__between;    // const uint cnt_vertices = self->nrows;
    //!
    assert(false); // FIXME[concpetaul]: vlaidte [below] wr.t the "between" ... tehn udpat eoru XB-deispion in our "meta_ccm.tex" ... 
    return (scalar_within / ((t_float)cnt_vertices * scalar_between));    
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_sdValidity) {
    return T_FLOAT_MAX;
    assert(false); // FIXME: update/add support for $\sum(1/d(Cl)$ ... ie, a new enum for the latter ... 
    assert(false); // FIXME: simlair to [ªbove] use/comptue for two different amtrix-CCM-objects ... ie, to corrreclty copmtue wrt. $\sigma(..)$ ... 
    assert(false); // FIXMe[artilce::"meta_ccm.tex"]: updat eour ariltce ... when [below] issues are addressed/ansewered .... 
    assert(false); // FIXMe[related-work]: validte our interptation wrt. "SD validity index" ... figure out correct interptaitons wrt. teh "scatter" emtric/element ... conceptally undersaitng why/hwo the latter is sued/evlautaed ... 
    //! Note[equation]: $max(between)/(min(between)*within)(1 + scatter)  $
    assert(false); // FIXMe[cocneptaul]: valdite/describe our 'max' and 'min' ... ie, simalrit to the [ªbov€] "XS" CCM ... then update our "meta_ccm.tex" ... figure out hwo to compuate/handle the "scatter" sub-equation/appraoch ... 
    return T_FLOAT_MAX; assert(false); // FIXMe[cocneptaul]: valdite/describe our ... 
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_VNND) {
    return T_FLOAT_MAX;
    //! Note[equation]: $ $
    assert(false); // FIXMe[related-work]: validte our interptation ... ie, to try resolving [below] observaiton ... 
    return T_FLOAT_MAX; assert(false); // FIXME: try genrelaiing this euqaiotn ... seems odd/strate to sum for $\sum_{x \in X_l}(x_{min} avg(C_l))$ .... and update our rpcoeudr to 'hanlde' the/thsi gnerlizion ... where the number of itnerepioatns wrt. Silhouette and VRC argues for a gnelizaiton ... ie, givne the ucnerinty in estlibhed/correct interptioants .... 
    assert(false); // FIXME: figure out how ... to correctly comptue $d(C_l, C_l)$ ... ie, simalri to [above] "max" case/issue ... 
    assert(false); // FIXME: figure out how 'genrlaisze' this fucntion .... ie, add a new enum-topion to multily the 'vertex-score' with an optional function .... 
  } else if(enum_id == e_kt_matrix_cmpCluster_clusterDistance__cmpType_FigureOfMerit) {
    return T_FLOAT_MAX; // FIXME: remove.
    //! Note[equation]: $ $
    assert(false); // FIXME[conceptaul]: try/manage to stdanrize the FOM-computation ... ie, to re-rite the equation ... eg, wrt. the $ corr[\sum input[k][*]*corelation[*][m] $
    assert(false); // FIXME: compare/udpate our FOM-equaiton wr.t "https://www.biomedcentral.com/content/supplementary/1477-5956-9-30-S4.PDF" ... handle case/issue where 'raw-input-matirx' is Not provided ... 
    assert(false); // FIXMe[artilce::"meta_ccm.tex"]: updat eour ariltce ... when [below] issues are addressed/ansewered .... 
    assert(false); // FIXME[concpetaul]: ... try to descirptioe/summarise our 'arleady implemtend' generic-aprpaoch ... udapteing oru "meta_ccm.tex" wrt. laltter ..f.d ignin the min-imrpvoemetns 'to be added' iot. support the "FOM" CCM \cite{} .... 
    assert(false); // FIXME[concpetaul]: ... 
    assert(false); // FIXME[concpetaul]: ... what is the point/purpose of the $y \in V$ equation-stpe ... ie, may latter be dropped/omtied ... ie, wehre opurpose/meaning/back-thoguh wrt. the latter is seen wrt. .....??... 
    assert(false); // FIXME[concpetaul]: ...  consider to add support for storing temprocal/intemridate results in a matrix ... eg, wrt. a "matrix[vertex][clusters] = distance" .... where the latter matrix may be used in our latneitve/other CCMs to compteu/evlaute ......??... 
    return T_FLOAT_MAX; assert(false); // FIXMe[cocneptaul]: try to genralize the "FOM" function ... challenge conserns .... the fucntion use multipel steps: step(1) copmute $d(x, C_i)$; step(2) copmute $d(x, y)$; step(3) copmute $\sum(d(x, C_i) * d(x, y))/C_i$; step(4) copmute $step(1)*step(2)*step(3)$; step(5) compute $\sum_{y \in V}\sqrt{\sum_{C_i\in C}\sum_{x_\in C} step(4)} $; ... 
  } else {
    fprintf(stderr, "!!\t Add support for enum_id=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false); //! ie, as we then need adding support 'for this'.
    return T_FLOAT_MAX;
  }
}

static void __setInternal_dataSTruct__init__s_kt_matrix_cmpCluster_clusterDistance(s_kt_matrix_cmpCluster_clusterDistance_t *self, 
										   //const uint *map_vertexToCluster__, const uint cnt_vertices__, uint __cnt_features, const uint __cnt_clusters, t_float **matrix__, 
										   s_kt_matrix_cmpCluster_clusterDistance_config_t config) {
  /* const uint cnt_vertices = self->nrows; */
  /* const uint cnt_clusters = self->cnt_cluster; */
  t_float **matrix = self->input_matrix;
  const uint *map_vertexToCluster = self->mapOf_vertexClusterIds;

    // FIXME[concpetual+aritlce]: ... 
    // FIXME[concpetual+aritlce]: ... 
    s_kt_list_2d_kvPair_t objAdj_2d_fromKD = setToEmpty__s_kt_list_2d_kvPair_t();
    // printf("at %s:%d\n", __FILE__, __LINE__);
    if(config.isTo_use_KD_treeInSimMetricComputations && config.metric_complexClusterCmp_vertexCmp_isTo_use) { //! Then intitae/constuct a KD-tree where we compute for: ["vertex x vertex"].
      // printf("at %s:%d\n", __FILE__, __LINE__);
      // FIXME[tut+eval]: dientify the correctne number of ndoes for [below]:
      uint nn_numberOfNodes_min = 1;
      uint nn_numberOfNodes_max = macro_min(20, self->nrows);
      //! Then comptue the adjcuency-matrix using the kd-tree-appraoch:
      s_kt_matrix_t mat_tmp = setToEmptyAndReturn__s_kt_matrix_t();
      mat_tmp.matrix = matrix; mat_tmp.nrows = self->nrows; mat_tmp.ncols = self->ncols;
      //      s_kt_matrix_base mat_tmp = initAndReturn__notAllocate__s_kt_matrix_base_t(self->nrows, self->ncols, matrix);
      objAdj_2d_fromKD = get_sparse2d_fromKD__kd_tree(&mat_tmp, nn_numberOfNodes_min, nn_numberOfNodes_max, T_FLOAT_MAX, config.metric_complexClusterCmp_obj);
    }

  
  {
    self->comptued_subDataStrucutres = true; //! ie, givne [”elow] logics:
    
    if(config.metric_complexClusterCmp_vertexCmp_isTo_use == true) { //! then we apply a 'complex comparison-strategy':

      // FIXME: instead consider to 'add support' for a pre-step where the simalrity-amtrix is used/comptued
      // assert(false); // FIXME: add support for this.
    }


    const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_selection = config.metric_clusterDistance__vertexVertex;    
    t_float sumOf_vertices__inSame = 0;     t_float sumOf_vertices__differentClusters = 0; t_float relativeImportance = 0; 
    // printf("#\t init-cluster-cluster-sums, at %s:%d\n", __FILE__, __LINE__);
    //    printf("metric_selection=%u, at %s:%d\n", metric_selection, __FILE__, __LINE__);

    if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {
      //!
      //! Udpate teh "self->cluster_cluster" and "self->cluster_cluster__count" matrices:
      //! Note: we also udpate for the "self->vertexTo_clusters" structure. 
      //      if(config.isTo_use_KD_treeInSimMetricComputations == false) 
      {
#define __MiFd const t_float score = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(tmpArr, tmpArr__size);
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster.c"      
      } /* else { */
      /* 	fprintf(stderr, "!!\t option Not supported: please request supprot for this option (which we due to funding-issue have not yet provided/added support for). If the latter is of tinerest then pelase send an heads-up to senior delvoepr [oekseth@gmail.com]. OBserviaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
      /* 	assert(false); */
      /* } */
    } else {

      // FIXME[code::SSE]: consider adding supprot for [”elow] SEE-otpmzioant-stratgye ... ie, if 'scuh compelxity' is of improtance ... 
    // FIXME[concpetual+aritlce]: ... describe the poitn/motviation beidn [”elo]ł seperate evalaution-step ... where we in step(1) evalaute $n^2$ cases/observiaotns ... challenge wrt. SSE-intrisinistics consersn/is the need to idnietfy/fetch knowledge of "map_vertexToCluster" (ie, withotu exeucitont-iem-lost) ... by seperately traversign through [cluster(k) w/nrows=[min, max]] ... 

      //! Step(1): comptue $d(v_i, v_k)$, updating both the "vertex-cluster" and "cluster-cluster" with observaiotns/attributes:
      //! Note: for "if(config.metric_complexClusterCmp_obj_isTo_use == false)"  we use knwoeldge from our vertex-cluster-comaprsion-aprpaoch to update the/our "vertex-vertex" aprpaoch (ie, where we otherise comptue the latter in a seprate step).
      if( (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum) 
		//|| (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) 
	  ) {
#define __MiFd__score self->vertexTo_clusters[row_id][clus_out] += score;
	//! 
	//! Set score-type and then call:
	if(config.metric_complexClusterCmp_obj_isTo_use == false) {  //! then update for the clusters:
	  const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_selection = config.metric_clusterDistance__cluster_cluster;
	  if( (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum) 
	      //|| (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) 
	      ) {
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
	    //    printf("metric_selection=%u, at %s:%d\n", metric_selection, __FILE__, __LINE__);
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) {
#define __MiFd__score_updateResultContainer  self->cluster_cluster[clus_in][clus_out] = macro_max(score, self->cluster_cluster[clus_in][clus_out]); 		self->cluster_cluster__count[clus_in][clus_out] = 1; //! ie, our asusmpiton.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) {
#define __MiFd__score_updateResultContainer  self->cluster_cluster[clus_in][clus_out] = macro_min(score, self->cluster_cluster[clus_in][clus_out]); 		self->cluster_cluster__count[clus_in][clus_out] = 1; //! ie, our asusmpiton.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {
	    assert(false); // FIXME: figure out how to 'handle this'.
	  } else { //! then a 'fall-back':
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
	    //    printf("metric_selection=%u, at %s:%d\n", metric_selection, __FILE__, __LINE__);
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	    //assert(false); //! ie, as we then need to add support 'for this case'.
	  }
	} else {//! else we need to udpate 'this' in a sperate step.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	}	
#undef __MiFd__score
      } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) {
#define __MiFd__score self->vertexTo_clusters[row_id][clus_out] = macro_max(score, self->vertexTo_clusters[row_id][clus_out]);
	//! 
	//! Set score-type and then call:
	if(config.metric_complexClusterCmp_obj_isTo_use == false) {  //! then we use knwoeldge from our vertex-cluster-comaprsion-aprpaoch to update the/our "vertex-vertex" aprpaoch (ie, where we otherise comptue the latter in a seprate step).
	  const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_selection = config.metric_clusterDistance__cluster_cluster;
	  if( (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum) 
	      //|| (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) 
	      ) {
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) {
#define __MiFd__score_updateResultContainer  self->cluster_cluster[clus_in][clus_out] = macro_max(score, self->cluster_cluster[clus_in][clus_out]); 		self->cluster_cluster__count[clus_in][clus_out] = 1; //! ie, our asusmpiton.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) {
#define __MiFd__score_updateResultContainer  self->cluster_cluster[clus_in][clus_out] = macro_min(score, self->cluster_cluster[clus_in][clus_out]); 		self->cluster_cluster__count[clus_in][clus_out] = 1; //! ie, our asusmpiton.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {
	fprintf(stderr, "!!\t Use a fallback-metric, as the 'metric-seleciotn' inq eusiton is Not supported (where 'sum') is used. If 'this support' is of interest, then pelase contact the senior developer [oekseth@gmail.co]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	    //assert(false); // FIXME: figure out how to 'handle this'.
	  } else {
	fprintf(stderr, "!!\t Use a fallback-metric, as the 'metric-seleciotn' inq eusiton is Not supported (where 'sum') is used. If 'this support' is of interest, then pelase contact the senior developer [oekseth@gmail.co]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	    //assert(false); //! ie, as we then need to add support 'for this case'.
	  }
	} else {//! else we need to udpate 'this' in a sperate step.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	}
#undef __MiFd__score
      } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) {
#define __MiFd__score self->vertexTo_clusters[row_id][clus_out] = macro_min(score, self->vertexTo_clusters[row_id][clus_out]);
	//! 
	//! Set score-type and then call:
	if(config.metric_complexClusterCmp_obj_isTo_use == false) {  //! then update for the clusters:
	  const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_selection = config.metric_clusterDistance__cluster_cluster;
	  if( (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum) 
	      //|| (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) 
	      ) {
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) {
#define __MiFd__score_updateResultContainer  self->cluster_cluster[clus_in][clus_out] = macro_max(score, self->cluster_cluster[clus_in][clus_out]); 		self->cluster_cluster__count[clus_in][clus_out] = 1; //! ie, our asusmpiton.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) {
#define __MiFd__score_updateResultContainer  self->cluster_cluster[clus_in][clus_out] = macro_min(score, self->cluster_cluster[clus_in][clus_out]); 		self->cluster_cluster__count[clus_in][clus_out] = 1; //! ie, our asusmpiton.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	  } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {
	    assert(false); // FIXME: figure out how to 'handle this'.
	  } else {
	    assert(false); //! ie, as we then need to add support 'for this case'.
	  }
	} else {//! else we need to udpate 'this' in a sperate step.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	}
      } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) { //! then a 'fall-back':
	fprintf(stderr, "!!\t Use a fallback-metric, as the 'metric-seleciotn' inq eusiton is Not supported (where 'sum') is used. If 'this support' is of interest, then pelase contact the senior developer [oekseth@gmail.co]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	//assert(false); // FIXME: figure out how to 'handle this'.
      } else { //! thena  'fall-back':
	fprintf(stderr, "!!\t Use a fallback-metric, as the 'metric-seleciotn' inq eusiton is Not supported (where 'sum') is used. If 'this support' is of interest, then pelase contact the senior developer [oekseth@gmail.co]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score; 	self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__cluster_cluster_nonSTD.c"      	  
	//	assert(false); //! ie, as we then need to add support 'for this case'.
      }
#undef __MiFd__score
    }
  } //! else we assuemt ehe non-adjcency-amtrix 'inhbitcs' such a comaprsion (oekseth, 06. jan. 2017).
  //! ------------------------
  //!
  //! Compute: distance between clusters
  //! Remember the cluster-memberships wrt. the input-matrix, eg, when users 'requests' for both Minkowski and Cityblock usign the same 'cofngiuration-input-objec't.
  const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_selection = config.metric_clusterDistance__cluster_cluster;
  // printf("at %s:%d\n", __FILE__, __LINE__);
  if(config.metric_complexClusterCmp_obj_isTo_use == true) {
    //printf("nrows=%u, ncols=%u, at %s:%d\n", self->nrows, self->ncols, __FILE__, __LINE__);
    // FIXME: add support ofr [below] .... ie, to 'use' a KD-tree-appraoch (oekseth, 06. aug. 2017).
    assert(self->nrows == self->ncols); //! ie, to simplify [”elow] logics (oekseth, 06. arp. 2017).
    // printf("compute distance-between-clusters, at %s:%d\n", __FILE__, __LINE__);
    /* if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) { */
    /*   fprintf(stderr, "!!\t(option-not-supported)\t Please figure out how to compute STD when the result is a scalar describing the simliarty between two clusters (both for the case where cluster1==cluster2 and for cluster1!=cluster2). In brief plese givne the senior delvoeper [oekseth@gmail.com] an heads up wrt. your proposed appraoch (ie, provindg extensive documentaiton and soruce-code for this). Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
    /*   assert(false); // TODO:: figure out how to 'handle this'. */
    /* } */
    //! 
    //! Comptue the distande seperately betweent eh clusters ... and update the clsuter-distance-score.
    if( (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__sum) 
	//|| (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__avg) 
	) {
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] += score;    self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__centroidCentroid.c"
    } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) {
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] = macro_max(score, self->cluster_cluster[clus_in][clus_out]); 	      self->cluster_cluster__count[clus_in][clus_out] = 1; //! ie, our asusmpiton.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__centroidCentroid.c"
    } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) {
#define __MiFd__score_updateResultContainer self->cluster_cluster[clus_in][clus_out] = macro_min(score, self->cluster_cluster[clus_in][clus_out]); 	      self->cluster_cluster__count[clus_in][clus_out] = 1; //! ie, our asusmpiton.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__centroidCentroid.c"
    } else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__STD) {
	fprintf(stderr, "!!\t Use a fallback-metric, as the 'metric-seleciotn' inq eusiton is Not supported (where 'sum') is used. If 'this support' is of interest, then pelase contact the senior developer [oekseth@gmail.co]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      //assert(false); // TODO:: figure out how to 'handle this'.
      //! then 'apply' a simple work-around.
#define __MiFd__score_updateResultContainer	      self->cluster_cluster[clus_in][clus_out] += score;    self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__centroidCentroid.c"
    } else {
	fprintf(stderr, "!!\t Use a fallback-metric, as the 'metric-seleciotn' inq eusiton is Not supported (where 'sum') is used. If 'this support' is of interest, then pelase contact the senior developer [oekseth@gmail.co]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	//assert(false); //! ie, as we then need to add support 'for this case'.
      //! then 'apply' a simple work-around.
#define __MiFd__score_updateResultContainer	      self->cluster_cluster[clus_in][clus_out] += score;    self->cluster_cluster__count[clus_in][clus_out]++; //! ie, incremen the count.
#include "kt_matrix_cmpCluster__stub__ccmMatrix__init__centroidCentroid.c"
    }

    /* //!  */
    /* //! De-allcoate: */
    /* free_s_kt_set_2dsparse_t(&mappings_cluster_toVertex); */
  }
  //!
  //! De-allcoate:
  free__s_kt_list_2d_kvPair_t(&objAdj_2d_fromKD);
}

/**
   @brief initates the s_kt_matrix_cmpCluster_clusterDistance_t object
   @param <self> is the object to intiate.
   @param <map_vertexToCluster> is the vertex--cluster mappings for the cnt_vertices
   @param <cnt_vertices> is the number of vertices in the adjcency-matrix matrix
   @param <cnt_cluster> is the number of clusters (ie, the number of unique clsuter-ids in map_vertexToCluster)
   @param <matrix> is the distance-matrix (which is to be used when assessing the distance between the clsuters).
   // @param <mapOf_vertexClusterIds> hold the cluster-ids' which is assicated to a particualr vertex.
   @param <config> is used to idneityf the type of centrlaity-approach to use.
 **/
void init__s_kt_matrix_cmpCluster_clusterDistance(s_kt_matrix_cmpCluster_clusterDistance_t *self, const uint *map_vertexToCluster, const uint cnt_vertices, uint cnt_features, const uint cnt_clusters, t_float **matrix, s_kt_matrix_cmpCluster_clusterDistance_config_t config) {
  //! What we expect:
  //printf("at %s:%d\n", __FILE__, __LINE__);
  assert(self);
  self->config = config;
  assert(map_vertexToCluster);
  assert(cnt_clusters > 0);
  assert(cnt_vertices > 0);
  assert(matrix);
  assert(map_vertexToCluster);
  self->comptued_subDataStrucutres = false; 
  if(cnt_features == UINT_MAX) {cnt_features = cnt_vertices;}
  // printf("\n\n #! Init, at %s:%d\n", __FILE__, __LINE__);  
  //! ------------------------
  //!
  //! Allocate:
  const t_float default_value = 0;
  self->cnt_cluster = 0;
  self->nrows = 0;
  self->ncols = 0;
  assert(cnt_vertices > 0); assert(cnt_clusters > 0);
  const uint default_value_uint = 0;
  self->mapOf_vertexClusterIds = allocate_1d_list_uint(cnt_vertices, default_value_uint);
  self->vertexTo_clusters = NULL;
  self->cluster_cluster  = NULL;
  self->cluster_cluster__count  = NULL;
  { //! Investigate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
    assert(matrix); assert(cnt_vertices > 0);
    long long int cnt_interesting = 0;
    const uint nrows = cnt_vertices;
    const uint ncols = cnt_features;
    for(uint i = 0; i < nrows; i++) {
      for(uint j = 0; j < ncols; j++) {
	cnt_interesting += isOf_interest(matrix[i][j]); } }
    if(cnt_interesting == 0) {      
      if(true) {
	fprintf(stderr, "!!\t No cells are of interest: matrix (with dims %u,%u) is without any interestign cells: if the latter soudns surpsiing then pelase contact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_vertices, cnt_features, __FUNCTION__, __FILE__, __LINE__);
	fprintf(stdout, "!!\t No cells are of interest: matrix (with dims %u,%u) is without any interestign cells: if the latter soudns surpsiing then pelase contact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_vertices, cnt_features, __FUNCTION__, __FILE__, __LINE__);

      }
      //assert(false); //! ie, an heads-up
      return; //! ie, as we otherwse have a poitnless input to the clsutering
    }
  }
  //!
  if( (cnt_clusters == 0) || (cnt_clusters == UINT_MAX) ) {return;}
  if( (cnt_vertices == 0) || (cnt_vertices == UINT_MAX) ) {return;}
  //!
  self->cnt_cluster = cnt_clusters;
  self->nrows = cnt_vertices;
  self->ncols = cnt_features;
  //!
  self->vertexTo_clusters = allocate_2d_list_float(cnt_vertices, cnt_clusters, default_value);
  self->cluster_cluster = allocate_2d_list_float(cnt_clusters, cnt_clusters, default_value);
  self->cluster_cluster__count = allocate_2d_list_uint(cnt_clusters, cnt_clusters, default_value);



  // --- 
  //! Initiate the data-structure(s): 
  { //! Copy the clsuter-mappigns, and test that each mapping is correctly set:
    for(uint row_id = 0; row_id < cnt_vertices; row_id++) {
      const uint cluster_id = map_vertexToCluster[row_id];
      if(cluster_id != UINT_MAX) {
	assert(cluster_id != UINT_MAX);
	// printf("cluster_id=%u < |%u|, at %s:%d\n", cluster_id, cnt_clusters, __FILE__, __LINE__);
	assert(cluster_id < cnt_clusters);
	//! Update:
	self->mapOf_vertexClusterIds[row_id] = cluster_id;
      } else {
	self->mapOf_vertexClusterIds[row_id] = UINT_MAX;
      }
    }
  }
  { //! Udpate for: vertexTo_clusters:
    const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_selection = config.metric_clusterDistance__vertexVertex;
    if( (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) || (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) ) {
      // printf("compute min--or--max, at %s:%d\n", __FILE__, __LINE__);
      t_float default_value_spec = T_FLOAT_MAX;
      if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) {default_value_spec = T_FLOAT_MIN_ABS;}
      else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) {default_value_spec = T_FLOAT_MAX;}
      else {assert(false);} //! ie, as we then need addign support 'for this'.
      for(uint row_id = 0; row_id < cnt_vertices; row_id++) {
	for(uint cluster_id = 0; cluster_id < cnt_clusters; cluster_id++) {
	  self->vertexTo_clusters[row_id][cluster_id] = default_value_spec;
	}
      }
    } //! else we assume 'intalizing to 0 is correct'.
  }
  { //! Udpate for: cluster_cluster
    const e_kt_matrix_cmpCluster_clusterDistance__config_metric__t metric_selection = config.metric_clusterDistance__cluster_cluster;
    if( (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) || (metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) ) {
      // printf("compute min--or--max, at %s:%d\n", __FILE__, __LINE__);
      t_float default_value_spec = T_FLOAT_MAX;
      if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__max) {default_value_spec = T_FLOAT_MIN_ABS;}
      else if(metric_selection == e_kt_matrix_cmpCluster_clusterDistance__config_metric__min) {default_value_spec = T_FLOAT_MAX;}
      else {assert(false);} //! ie, as we then need addign support 'for this'.
      for(uint cluster_id_in = 0; cluster_id_in < cnt_clusters; cluster_id_in++) {
	for(uint cluster_id = 0; cluster_id < cnt_clusters; cluster_id++) {
	  self->cluster_cluster[cluster_id_in][cluster_id] = default_value_spec;
	}
      }
    } //! else we assume 'intalizing to 0 is correct'.
  }

  assert(cnt_clusters > 0);
  {//! Initate an internal 2d-list to hold the vertex-ids in each cluster:
    self->input_matrix = matrix;
    allocate__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cnt_clusters, cnt_vertices, /*matrix=*/NULL, /*mask=*/NULL, /*isTo_useImplictMask=*/false, /*isTo_allocateFor_scores=*/false);
    //!
    //! Build a 'local map' of of [cluster][] <.. figure otu what 'input is expected'.
    for(uint row_id = 0; row_id < cnt_vertices; row_id++) {
      const uint clus_in  = map_vertexToCluster[row_id];
      if( (clus_in >= cnt_clusters)) {continue;} //! ie, as we then asusme that no well-defiend clsuters were foudn for these two vertices.
      push__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), clus_in, row_id); //! ie, add the row_id to the cluster.      
    }
  }

  //! ------------------------
  //!  
  // FIXME: remvoe [below] 'ctorantinb' ... instead supprot for de-activing' this defualt itantion-stpe ... mvoign 'this' to the CCM-fucntion-call ... using insted "config.metric_complexClusterCmp_vertexCmp_isTo_use" .... 
  // printf("at %s:%d\n", __FILE__, __LINE__);
  if( (cnt_vertices == cnt_features)  || (config.metric_complexClusterCmp_vertexCmp_isTo_use == true)  ) { //! Compute: the distance from each each vertex to vertices in each of the clusters
    assert(config.metric_complexClusterCmp_obj.metric_id != e_kt_correlationFunction_undef);
    // printf("at %s:%d\n", __FILE__, __LINE__);
    __setInternal_dataSTruct__init__s_kt_matrix_cmpCluster_clusterDistance(self, 
									   //map_vertexToCluster, cnt_vertices, cnt_features, cnt_clusters, matrix, 
									   config);
  } else {//! else we assume 'this' has been 'supported' wehn teh "vertexTo_clusters" was udpated.
    if(config.isTo_use_KD_treeInSimMetricComputations) {
      fprintf(stderr, "!!\t(not supported)\t requested KD-tree for cases where 'vertexCmp_isTo_use == false'. If he latter is of itnerest, it may be supported, ie, for which contacting Dr. Ekseth at [oekseth@gmail.com] may resolve the issue. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); //! (oekseth, 06. apr. 2018).
      assert(false);
    }
  }


  assert(self->nrows > 0);   assert(self->cnt_cluster > 0);
  assert(self->nrows == cnt_vertices);    assert(self->cnt_cluster == cnt_clusters);
}
//! De-allcoates the "s_kt_matrix_cmpCluster_clusterDistance_t" self object.
void free__s_kt_matrix_cmpCluster_clusterDistance_t(s_kt_matrix_cmpCluster_clusterDistance_t *self) {
  assert(self);
  if(self->vertexTo_clusters) {
    free_2d_list_float(&(self->vertexTo_clusters), self->nrows);  
    self->vertexTo_clusters = NULL;
  }
  if(self->mapOf_vertexClusterIds) {free_1d_list_uint(&(self->mapOf_vertexClusterIds)); self->mapOf_vertexClusterIds = NULL;}
  if(self->cluster_cluster) {
    free_2d_list_float(&(self->cluster_cluster), self->cnt_cluster);
    self->cluster_cluster = NULL;
  }
  if(self->cluster_cluster__count) {
    free_2d_list_uint(&(self->cluster_cluster__count), self->cnt_cluster);
    self->cluster_cluster__count = NULL;
  }
  free_s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex));
  self->input_matrix = NULL;
}
//void init__s_kt_matrix_cmpCluster_clusterDistance_t(const uint 


//! Set the s_kt_matrix_cmpCluster_t object to empty.
void setTo_empty__s_kt_matrix_cmpCluster_t(s_kt_matrix_cmpCluster_t *self) {
  assert(self);
  setTo_empty__s_kt_matrix_t(&(self->matrixOf_coOccurence));
  setTo_empty__s_kt_matrix_t(&(self->matrixOf_coOccurence_or));
  self->cnt_vertices = 0;
  //!
  //! Remember the number of elements in each cluster:
  self->mapOfCluster1_size = 0;   self->mapOfCluster2_size = 0; 
  self->mapOfCluster1 = NULL;     self->mapOfCluster2 = NULL;
  //!
  //! Set the data-sets to empty:
  self->clusterDistances_cluster1 = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
  self->clusterDistances_cluster2 = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
  self->external_matrixOf_coOccurence_1 = NULL; //! ie, as we exect we are Not to allcoate mmroy 'for this'.
  self->external_matrixOf_coOccurence_2 = NULL; //! ie, as we exect we are Not to allcoate mmroy 'for this'.
}
//! Initates an object
void init__s_kt_matrix_cmpCluster(s_kt_matrix_cmpCluster_t *self, const uint nrows, const uint ncols, const bool isTo_allocateWeightColumns) {
  assert(self);
  init__s_kt_matrix(&(self->matrixOf_coOccurence), nrows, ncols, isTo_allocateWeightColumns);
  init__s_kt_matrix(&(self->matrixOf_coOccurence_or), nrows, ncols, isTo_allocateWeightColumns);
  self->cnt_vertices = 0;
  self->mapOfCluster1_size = 0;   self->mapOfCluster2_size = 0; 
  self->mapOfCluster1 = NULL;     self->mapOfCluster2 = NULL;
  //!
  //! Set the data-sets to empty:
  self->clusterDistances_cluster1 = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
  self->clusterDistances_cluster2 = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
  self->external_matrixOf_coOccurence_1 = NULL; //! ie, as we exect we are Not to allcoate mmroy 'for this'.
  self->external_matrixOf_coOccurence_2 = NULL; //! ie, as we exect we are Not to allcoate mmroy 'for this'.
}
//! De-allocates the object
void free__s_kt_matrix_cmpCluster(s_kt_matrix_cmpCluster_t *self) {
  assert(self);
  free__s_kt_matrix(&(self->matrixOf_coOccurence));
  free__s_kt_matrix(&(self->matrixOf_coOccurence_or));
  self->cnt_vertices = 0;
  self->mapOfCluster1_size = 0;   self->mapOfCluster2_size = 0; 
  if(self->mapOfCluster1) {free_1d_list_float(&(self->mapOfCluster1));} self->mapOfCluster1 = NULL; 
  if(self->mapOfCluster2) {free_1d_list_float(&(self->mapOfCluster2));} self->mapOfCluster2 = NULL;
  free__s_kt_matrix_cmpCluster_clusterDistance_t(&(self->clusterDistances_cluster1));
  free__s_kt_matrix_cmpCluster_clusterDistance_t(&(self->clusterDistances_cluster2));
  self->external_matrixOf_coOccurence_1 = NULL; //! ie, as we exect we are Not to allcoate mmroy 'for this'.
  self->external_matrixOf_coOccurence_2 = NULL; //! ie, as we exect we are Not to allcoate mmroy 'for this'.
}

//! Build a co-occurence-matrix of entites between the two clsuter-sets
//! @remarks the result is stored in the "self->matrixOf_coOccurence" parameter and may be 'used' in/from our "computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(..)" function
void build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(s_kt_matrix_cmpCluster_t *self, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint cnt_vertices, const bool isTo_includeWeight, const uint max_cntClusters, t_float **matrixOf_coOccurence_1, t_float **matrixOf_coOccurence_2, s_kt_matrix_cmpCluster_clusterDistance_config_t config) {
  assert(self); assert(cnt_vertices > 0); 
  //assert(cnt_vertices >= max_cntClusters); 
  assert(max_cntClusters != 0); assert(max_cntClusters != UINT_MAX);
  assert(self->cnt_vertices == 0); //! ie, as we expec tthe option to have 'been intalised to empty'.
  self->external_matrixOf_coOccurence_1 = matrixOf_coOccurence_1;
  self->external_matrixOf_coOccurence_2 = matrixOf_coOccurence_2;
  if(isTo_includeWeight) {
    assert(false); // FIXME: then set the "self->cnt_vertices" = ...??... <-- how are we then to 'set the weight for each vertex'? <-- first 'figure out how the weight is used in hte matrix-update'.
  } 
  { //! Validate that at least 'one vertex' has a known clsuter/dcneotrid memrbership:
    {
      assert(mapOf_vertexToCentroid1);
      uint cnt_found = 0;
      for(uint i = 0; i < cnt_vertices; i++) {
	if(mapOf_vertexToCentroid1[i] != UINT_MAX) {cnt_found++;}
      }
      assert(cnt_found > 0);
    }
    {
      assert(mapOf_vertexToCentroid2);
      uint cnt_found = 0;
      for(uint i = 0; i < cnt_vertices; i++) {
	if(mapOf_vertexToCentroid2[i] != UINT_MAX) {cnt_found++;}
      }
      assert(cnt_found > 0);
    }
  }

  if(matrixOf_coOccurence_1 && matrixOf_coOccurence_2) { //! Investigate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
      assert(cnt_vertices > 0);
      long long int cnt_interesting_1 = 0;
      long long int cnt_interesting_2 = 0;
      const uint nrows = cnt_vertices;
      const uint ncols = cnt_vertices;
      for(uint i = 0; i < nrows; i++) {
	for(uint j = 0; j < ncols; j++) {
	  cnt_interesting_1 += isOf_interest(matrixOf_coOccurence_1[i][j]); 
	  cnt_interesting_2 += isOf_interest(matrixOf_coOccurence_2[i][j]);
	} }	  
      // printf("(cnt-interesting(%llu and %llu), at %s:%d\n", cnt_interesting_1, cnt_interesting_2, __FILE__, __LINE__); // FIXME: remoe.
      if(cnt_interesting_1 == 0) {      
	if(true) {fprintf(stderr, "!!\t No cells are of interest (for matrix=1): matrix is without any interestign cells: if the latter soudns surpsiing then pelase contact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);}
	//assert(false); //! ie, an heads-up
	//*scalar_score = T_FLOAT_MAX;
	//return true; //! ie, as we otherwse have a poitnless input to the clsutering
      }
      if(cnt_interesting_2 == 0) {      
	if(true) {fprintf(stderr, "!!\t No cells are of interest (for matrix=2): matrix is without any interestign cells: if the latter soudns surpsiing then pelase contact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);}
	//assert(false); //! ie, an heads-up
	//*scalar_score = T_FLOAT_MAX;
	//return true; //! ie, as we otherwse have a poitnless input to the clsutering
      }      
    }
  
  self->cnt_vertices = cnt_vertices;
  // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
#include "kt_matrix_cmpCluster__stub__metric__buildCount.c" //! ie, apply logics.
  // fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
  //! A set of asserts to 'catch' any 'simplcictc' (thoguh cirticla) errors from [ªbove]:
  assert(self->mapOfCluster1_size != UINT_MAX);
  assert(self->mapOfCluster2_size != UINT_MAX);
  if( (matrixOf_coOccurence_1 != NULL) && (matrixOf_coOccurence_2 != NULL) ) {
    /* if(!( (self->clusterDistances_cluster1.nrows != 0)  && (self->clusterDistances_cluster2.nrows != 0) )) { */
    /*   assert(false); //! ie, as we then have an in-cosnsitency. */
    /* } */
    /* if(!( (self->clusterDistances_cluster1.cnt_cluster != 0)  && (self->clusterDistances_cluster2.cnt_cluster != 0) )) { */
    /*   assert(false); //! ie, as we then have an in-cosnsitency. */
    /* } */
    //printf("cnt_cluster=[%u, %u], at %s:%d\n", self->clusterDistances_cluster1.cnt_cluster, self->clusterDistances_cluster2.cnt_cluster, __FILE__, __LINE__);
  }
}

//! Initaites the s_kt_matrix_cmpCluster_result_scalarScore_t strucutre
//! @return a structure where both valeus are set to '0' (oekseth@ des. 201&).
s_kt_matrix_cmpCluster_result_scalarScore_t setTo_empty__s_kt_matrix_cmpCluster_result_scalarScore_t() {
  s_kt_matrix_cmpCluster_result_scalarScore_t self;
  self.score_1 = 0;
  self.score_2 = 0;
  //! @return:
  return self;
}


//! Compute "number C 2", a comptuation which 'translates' into "number * (number-1)/2" (oekseth, 06. des. 201&).
#define __computeCombinatorics__by2(number) ({ t_float result_local = 0; if(number > 1) { result_local = number*(number-1)*0.5;} result_local;})

//! @return the abs(min/max), thereby ensuring that we have a number in range [0, 1]
#define __cmpClusterMacro__minByMax() ({	\
  t_float score_local = 0;			\
  if( (sum_max != 0) && (sum_min != 0) ) { \
    score_local = mathLib_float_abs(macro_min(sum_min, sum_max) / macro_max(sum_max, sum_min)); \
  } score_local;})

//! @brief Compute the cluster-compiarson-metric score for a given cluster-comparison-metric cmp_type (oekseth, 06. des. 2016).
//! @return a cluster-comparison-metric-score which describes the simliarty between the rows and volumsn in self->matrixOf_coOccurence
//! @remarks the "matrix_usedInClustering" parameter is only used for metrics=[e_kt_matrix_cmpCluster_metric_silhouetteIndex,]
void computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(const s_kt_matrix_cmpCluster_t *self, const e_kt_matrix_cmpCluster_metric_t cmp_type, s_kt_matrix_cmpCluster_result_scalarScore_t *scalar_result) {
  assert(self);
  //t_float **matrix_usedInClustering = self->external_matrixOf_coOccurence;
  *scalar_result = setTo_empty__s_kt_matrix_cmpCluster_result_scalarScore_t();
  t_float score = 0;
  //! ----------------------
  //!
  //! Simplfiy our acccess:
  const uint cnt_vertices = self->cnt_vertices;
  assert(cnt_vertices > 0); assert(cnt_vertices != UINT_MAX); //! ie, as the call will otherwise be pointless.
  //! ---
  const uint nrows = self->matrixOf_coOccurence.nrows; //! ie, the number of clusters in "cluster-prediction(1)"
  const uint ncols = self->matrixOf_coOccurence.ncols; //! ie, the number of clusters in "cluster-prediction(2)"
  assert(nrows != 0);   assert(nrows != UINT_MAX);
  assert(ncols != 0);   assert(ncols != UINT_MAX);
  t_float **matrix = self->matrixOf_coOccurence.matrix;
  // assert(matrix);

  //! ----------------------
  //!
  //! Identify the type-of-compairon, specify the metric, and thereafter compute:  
  if(cmp_type == e_kt_matrix_cmpCluster_metric_randsIndex) {     //! ie, "Rands index", where the quation uses the 'formualte' specified [<in Rands orginal artilce>]
#include "kt_matrix_cmpCluster__stub__metric__Rand.c" //! ie, compute
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_randsIndex_alt2) {     //! an alternative implementaiton, using the equation 'specified' in ["http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.164.6189&rep=rep1&type=pdf" w/title "Comparing Clusterings - An Overview"]:
#include "kt_matrix_cmpCluster__stub__metric__subset__n00_n11.c" //! compute "sumOf_not" and "sumOf_and"
    const t_float numerator = 2*(sumOf_not + sumOf_and);
    if( (numerator != 0) ) {
      score = numerator / (cnt_vertices*(cnt_vertices-1));
    }
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_randsAdjustedIndex) {
#include "kt_matrix_cmpCluster__stub__metric__Rand_adjusted.c" //! ie, compute
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_chiSquared) {
    const t_float cnt_vertices_inv = 1/(t_float)cnt_vertices;
    //! 
    //! Compute for the 'total count'
    t_float cnt_totalOverlaps = 0;
    assert(self->mapOfCluster1);
    assert(self->mapOfCluster2);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      assert_possibleOverhead(matrix[row_id]);
      for(uint col_id = 0; col_id < ncols; col_id++) {
	const t_float score = matrix[row_id][col_id];
	const t_float cntInCluster_1 = self->mapOfCluster1[row_id];
	const t_float cntInCluster_2 = self->mapOfCluster2[col_id];
	//! What we expect:
	assert_possibleOverhead(score != T_FLOAT_MAX);    
	assert(cntInCluster_1 != 0); 	assert(cntInCluster_1 != T_FLOAT_MAX);
	assert(cntInCluster_2 != 0); 	assert(cntInCluster_2 != T_FLOAT_MAX);
	const t_float cnt_maxComb = (cnt_vertices_inv * cntInCluster_1 * cntInCluster_2);
	// printf("cnt_maxComb=%f, score_and=%f, cntInCluster_1=%f, cntInCluster_2=%f, cnt_vertices_inv=%f, at %s:%d\n", cnt_maxComb, score, cntInCluster_1, cntInCluster_2, cnt_vertices_inv, __FILE__, __LINE__);
	if(cnt_maxComb != 0) {
	  t_float diff_scores = score - cnt_maxComb;
	  // printf("diff_scores=%f, cnt_maxComb=%f, score_and=%f, cntInCluster_1=%f, cntInCluster_2=%f, cnt_vertices_inv=%f, at %s:%d\n", diff_scores, cnt_maxComb, score, cntInCluster_1, cntInCluster_2, cnt_vertices_inv, __FILE__, __LINE__);
	  if(diff_scores != 0) {
	    cnt_totalOverlaps += ( (diff_scores*diff_scores)/cnt_maxComb); //! ie, the realtive adjusted importance of the cluster-overlap: from the equation we observe a simliarty to Perasons diffneret corr-metrics, ie, which may be esplaind by the fact that Person orginally proposed this metric.
	  }
	}
      }
    }
    //!
    //! Combine, ie, udpate the 'score to return':
    score = cnt_totalOverlaps;
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_FowlesMallows) {
#include "kt_matrix_cmpCluster__stub__metric__subset_squaredCoverage.c" //! which is used to compute cnt_totalOverlaps, sumOf_clusterCountSquares_1, and sumOf_clusterCountSquares_2

    //!
    //! Combine, ie, udpate the 'score to return':
    const t_float diff_poss_max = cnt_totalOverlaps - cnt_vertices;
    if(diff_poss_max != 0) {
      const t_float max_cnt = (sumOf_clusterCountSquares_1 - cnt_vertices)*(sumOf_clusterCountSquares_2 - cnt_vertices);
      if(max_cnt != 0) {
	score = diff_poss_max / mathLib_float_sqrt_abs(max_cnt);
      }
    }
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_Mirkin) {
#include "kt_matrix_cmpCluster__stub__metric__subset_squaredCoverage.c" //! which is used to compute cnt_totalOverlaps, sumOf_clusterCountSquares_1, and sumOf_clusterCountSquares_2
    //!
    //! Combine, ie, udpate the 'score to return':
    score = sumOf_clusterCountSquares_1 + sumOf_clusterCountSquares_2 - 2*cnt_totalOverlaps;
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_Wallace) { //! which is a permtuation of "e_kt_matrix_cmpCluster_metric_FowlesMallows" where we 'keep' the two seperate cases.
#include "kt_matrix_cmpCluster__stub__metric__subset_squaredCoverage.c" //! which is used to compute cnt_totalOverlaps, sumOf_clusterCountSquares_1, and sumOf_clusterCountSquares_2
    //!
    //! Combine, ie, udpate the 'score to return':
    t_float score_2 = 0;
    const t_float diff_poss_max = cnt_totalOverlaps - cnt_vertices;
    if(diff_poss_max != 0) {
      if(sumOf_clusterCountSquares_1 != cnt_vertices) {
	const t_float diff_denum = sumOf_clusterCountSquares_1 - cnt_vertices;
	assert(diff_denum != 0);
	score = diff_poss_max / diff_denum;
      }
      if(sumOf_clusterCountSquares_2 != cnt_vertices) {
	const t_float diff_denum = sumOf_clusterCountSquares_2 - cnt_vertices;
	assert(diff_denum != 0);
	score_2 = diff_poss_max / diff_denum;
      }
    }
    scalar_result->score_2 = score_2;
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_silhouetteIndex) {
    //printf("cnt_cluster=[%u, %u], at %s:%d\n", self->clusterDistances_cluster1.cnt_cluster, self->clusterDistances_cluster2.cnt_cluster, __FILE__, __LINE__);
    //! Note: In this metric we cous on vertices difference to clusters (ie, in cotnrast to [ªbove] metrics which are set-based 'studies'): the vertices 'inferred attributes' are unifed/combined through 'avg', 'min' and 'max'.
    //! Note: for an informative description of the "Silhouette index" see "https://cs.fit.edu/~pkc/classes/ml-internet/silhouette.pdf" and "https://en.wikipedia.org/wiki/Silhouette_(clustering)"
    if( (self->clusterDistances_cluster1.nrows != 0)  && (self->clusterDistances_cluster2.nrows != 0) ) {
      //printf("-----, at %s:%d\n", __FILE__, __LINE__);
      t_float prediction_cluster1 = compute__Silhouette__s_kt_matrix_cmpCluster_clusterDistance_t(&(self->clusterDistances_cluster1), e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, /*isTo_adjustEachRowScore_by_log=*/false);
      //printf("-----, at %s:%d\n", __FILE__, __LINE__);
      t_float prediction_cluster2 = compute__Silhouette__s_kt_matrix_cmpCluster_clusterDistance_t(&(self->clusterDistances_cluster2), e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, /*isTo_adjustEachRowScore_by_log=*/false);
      //!
      //! Compute:
      // FIXME: try improving [below] ... using an 'emprical appraoch' to 'figure out how'. <-- thereafter wrtie different permtautison 'of this' using the 'classifcial distnace-correlation-emtric-permtautiosn'      
      const t_float num   = macro_min(prediction_cluster1, prediction_cluster2);
      const t_float denum = macro_max(prediction_cluster1, prediction_cluster2);
      if( (num != 0) && (denum != 0) ) {score = num / denum;}
    } else {
      if(false) {fprintf(stderr, "!!\t The cluster-distance-objects were Not build: please validate wrt. your API that a distance-matrix was used 'in the input' when building the distance-comarpison-scores. For quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);  } // FIXME: consider instead moving this to the documetnaiton .... to avoiud all to many erorrs during bathc-copmptautiosn.
      return;
    }
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_DavisBouldin) {
    if( (self->clusterDistances_cluster1.nrows != 0)  && (self->clusterDistances_cluster2.nrows != 0) ) {
      t_float prediction_cluster1 = compute__DavisBouldin__s_kt_matrix_cmpCluster_clusterDistance_t(&(self->clusterDistances_cluster1), e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, /*isTo_adjustEachRowScore_by_log=*/false, e_kt_correlationFunction_undef);
      t_float prediction_cluster2 = compute__DavisBouldin__s_kt_matrix_cmpCluster_clusterDistance_t(&(self->clusterDistances_cluster2), e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, /*isTo_adjustEachRowScore_by_log=*/false, e_kt_correlationFunction_undef);
      //!
      //! Compute:
      // FIXME: try improving [below] ... using an 'emprical appraoch' to 'figure out how'. <-- thereafter wrtie different permtautison 'of this' using the 'classifcial distnace-correlation-emtric-permtautiosn'      
      const t_float num   = macro_min(prediction_cluster1, prediction_cluster2);
      const t_float denum = macro_max(prediction_cluster1, prediction_cluster2);
      if( (num != 0) && (denum != 0) ) {score = num / denum;}
    } else {
      if(false) {fprintf(stderr, "!!\t The cluster-distance-objects were Not build: please validate wrt. your API that a distance-matrix was used 'in the input' when building the distance-comarpison-scores. For quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);      } // FIXME: consider instead moving this to the documetnaiton .... to avoiud all to many erorrs during bathc-copmptautiosn.
      return;
    }
  } else if( (cmp_type == e_kt_matrix_cmpCluster_metric_Dunn) ) {
    if( (self->clusterDistances_cluster1.nrows != 0)  && (self->clusterDistances_cluster2.nrows != 0) ) {
      const bool isTo_adjustInternalByMax = false; //! ie, the defualt alternative
      t_float prediction_cluster1 = compute__Dunns__s_kt_matrix_cmpCluster_clusterDistance_t(&(self->clusterDistances_cluster1), e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, isTo_adjustInternalByMax); //, /*isTo_adjustEachRowScore_by_log=*/false);
      t_float prediction_cluster2 = compute__Dunns__s_kt_matrix_cmpCluster_clusterDistance_t(&(self->clusterDistances_cluster2), e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, isTo_adjustInternalByMax); //, /*isTo_adjustEachRowScore_by_log=*/false);
      //!
      //! Compute:
      // FIXME: try improving [below] ... using an 'emprical appraoch' to 'figure out how'. <-- thereafter wrtie different permtautison 'of this' using the 'classifcial distnace-correlation-emtric-permtautiosn'      
      const t_float num   = macro_min(prediction_cluster1, prediction_cluster2);
      const t_float denum = macro_max(prediction_cluster1, prediction_cluster2);
      if( (num != 0) && (denum != 0) ) {score = num / denum;}
    } else {
      if(false) {fprintf(stderr, "!!\t The cluster-distance-objects were Not build: please validate wrt. your API that a distance-matrix was used 'in the input' when building the distance-comarpison-scores. For quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);}      // FIXME: consider instead moving this to the documetnaiton .... to avoiud all to many erorrs during bathc-copmptautiosn.
      return;
    }
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_Jaccard) {
#include "kt_matrix_cmpCluster__stub__metric__subset_squaredCoverage.c" //! which is used to compute n11=cnt_totalOverlaps, n10=sumOf_clusterCountSquares_1, and n01=sumOf_clusterCountSquares_2
    //!
    //! Combine, ie, udpate the 'score to return':    
    //const t_float sum_possibleComb = (cnt_totalOverlaps + sumOf_clusterCountSquares_1 + sumOf_clusterCountSquares_2) - 3*cnt_vertices;
    const t_float sum_possibleComb = (cnt_totalOverlaps + sumOf_clusterCountSquares_1 + sumOf_clusterCountSquares_2); // - 3*cnt_vertices;
    if( (cnt_vertices != cnt_totalOverlaps) && (sum_possibleComb != 0) ) {
      score = (cnt_totalOverlaps - cnt_vertices) / sum_possibleComb;
    }
  } else if(
	    (cmp_type == e_kt_matrix_cmpCluster_metric_Jaccard_seperate_sensitivtyAndSpecifity)
	    || (cmp_type == e_kt_matrix_cmpCluster_metric_Jaccard_F1_score)
	    ){
#include "kt_matrix_cmpCluster__stub__metric__subset_squaredCoverage.c" //! which is used to compute n11=cnt_totalOverlaps, n10=sumOf_clusterCountSquares_1, and n01=sumOf_clusterCountSquares_2
    //!
    //! Combine, ie, udpate the 'score to return':    
    const t_float pred_TP = cnt_totalOverlaps;
    //! Note: in the [”elow] 'assumption' to FN and FP we use the 'assumption' in "e_kt_matrix_cmpCluster_metric_Wallace":
    const t_float pred_FN = sumOf_clusterCountSquares_1;
    const t_float pred_FP = sumOf_clusterCountSquares_2;
    const t_float num = pred_TP;
    { //! First compute for the "index sensitivty":
      const t_float denum = pred_TP + pred_FN;
      if( (num != 0) && (denum != 0) ) {
	score = num / denum;
	score = mathLib_float_sqrt_abs(score);
      }
    }
    { //! Thereafter compute for the "index specifity":, ie, the "recall":
      const t_float denum = pred_TP + pred_FP; //! ie, use "FP" instead of "FN":
      if( (num != 0) && (denum != 0) ) {
	const t_float tmp_score = num / denum;
	scalar_result->score_2 = mathLib_float_sqrt_abs(tmp_score);
      }
    }
    if(cmp_type == e_kt_matrix_cmpCluster_metric_Jaccard_F1_score) { //! then we combine the 'result' (ie, instead of 'returning' a pair ("https://clusteval.sdu.dk/1/clustering_quality_measures/18"):
      const t_float precision = score; score = 0; //! ie, reset.
      const t_float recall = scalar_result->score_2; scalar_result->score_2 = 0; //! ie, reset.
      score = 0; //! ie, reset.
      if( (precision != 0) && (recall != 0) ) {
	score = 2*precision*recall/(precision + recall);
      }
    }
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_Minkowski) {
    //! Note: for an example-case wrt. this metric we suggest amogn other to read the work of ["http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0541-0"].
#include "kt_matrix_cmpCluster__stub__metric__subset_squaredCoverage.c" //! which is used to compute n11=cnt_totalOverlaps, n10=sumOf_clusterCountSquares_1, and n01=sumOf_clusterCountSquares_2
    //!
    //! Combine, ie, udpate the 'score to return':    
    const t_float pred_TP = cnt_totalOverlaps;
    //! Note: in the [”elow] 'assumption' to FN and FP we use the 'assumption' in "e_kt_matrix_cmpCluster_metric_Wallace":
    const t_float pred_FN = sumOf_clusterCountSquares_1;
    const t_float pred_FP = sumOf_clusterCountSquares_2;
    const t_float num =   pred_FP + pred_FN;
    const t_float denum = pred_TP + pred_FN;
    if( (num != 0) && (denum != 0) ) {
      score = num / denum;
      score = mathLib_float_sqrt_abs(score);
    }
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_PR) {
    //! Note: [below] differents from "kt_matrix_cmpCluster__stub__metric__subset_squaredCoverage.c" wrt. our use of "sum(C[i])" (instead of "sum(C[i]^2)" in [above]).
#include "kt_matrix_cmpCluster__stub__metric__PR.c" //! which is used to compute cnt_totalOverlaps, sumOf_clusterCountSquares_1, and sumOf_clusterCountSquares_2

    //!
    //! Combine, ie, udpate the 'score to return':    
    //! Note: we compute (n11/|C1|)*(n11/|C1|)
    // FIXME: add a citation-reference to this measurement, ie, an aritlce 'which the PR measurement is used in'.
    //printf("cnt_totalOverlaps=%f, cnt_vertices=%u, sumOf_clusterCountSquares_1=%f, at %s:%d\n", cnt_totalOverlaps, cnt_vertices, sumOf_clusterCountSquares_1, __FILE__, __LINE__);
    if( (cnt_totalOverlaps != (t_float)cnt_vertices) && (sumOf_clusterCountSquares_1 != cnt_vertices) ) {
      const t_float fac_1 = (cnt_totalOverlaps - cnt_vertices) / (sumOf_clusterCountSquares_1 - cnt_vertices);
      //const t_float fac_1 = (cnt_totalOverlaps - cnt_vertices) / (sumOf_clusterCountSquares_1 - cnt_vertices);
      //printf("fac_1=%f, cnt_totalOverlaps=%f, cnt_vertices=%u, sumOf_clusterCountSquares_1=%f, sumOf_clusterCountSquares_2=%f,  at %s:%d\n", fac_1, cnt_totalOverlaps, cnt_vertices, sumOf_clusterCountSquares_1, sumOf_clusterCountSquares_2, __FILE__, __LINE__);
      if( (sumOf_clusterCountSquares_2 != cnt_vertices) ) {
	const t_float fac_2 = (cnt_totalOverlaps - cnt_vertices) / (sumOf_clusterCountSquares_2 - cnt_vertices);
	score = fac_1 * fac_2;
      }
    }
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_PR_alt2) {
    //! Note: [below] differents from "kt_matrix_cmpCluster__stub__metric__subset_squaredCoverage.c" wrt. our use of "sum(C[i])" (instead of "sum(C[i]^2)" in [above]).
#include "kt_matrix_cmpCluster__stub__metric__PR.c" //! which is used to compute cnt_totalOverlaps, sumOf_clusterCountSquares_1, and sumOf_clusterCountSquares_2
    if( (cnt_totalOverlaps != 0) && (sumOf_clusterCountSquares_1 != 0) && (sumOf_clusterCountSquares_2 != 0) ) {
      const t_float fac_1 = (cnt_totalOverlaps) / (sumOf_clusterCountSquares_1);
      const t_float fac_2 = (cnt_totalOverlaps) / (sumOf_clusterCountSquares_2);
      score = fac_1 * fac_2;
    }

  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_partitionDifference) {
    // FIXME: validate that we in [”elow] apply a correct approach, ie, that "sum-of-no-match = <sum-of-or> - <sum-of-and>" (oekseth, 06. des. 2016).
#include "kt_matrix_cmpCluster__stub__metric__subset__n00_n11.c" //! compute "sumOf_not" and "sumOf_and"

    //!
    //! Combine, ie, udpate the 'score to return':
    score = sumOf_not;    
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_fMeasure) {
    //! Note: if the [below] meausre we instead of the 'sum' seeks/uses the 'max' porperty between the case. In this comptuation we use the 'inner metric' of "2*C[i]*C_m[j]/(C[i] + C_m[j])", ie, where we 'give premimum' to cluster with 'number of vertices found in two large clusters'. Idea to 'find the maximized intersection between vertices'.
    //!Note: the "fMeasure" (as comptued in [below]) does Not make use of the "matrixOf_coOccurence_or.matrix" nor the "matrixOf_coOccurence.matrix", ie, 'distingiuishes' itself from the [above] set-based-measurements.
#include "kt_matrix_cmpCluster__stub__metric__fMeasure.c" //! ie, copmtue the measure.
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_maximumMatchMeasure) {
    //! Note: in contrast to teh "fMeasure" we use the 'max' proeprty on the "matrixOf_coOccurence.matrix", ie, 'focusing' on the max-intersections between different clusters.
    //! Note: in the iteraiton we use the 'exact simliar max-proceusdre' as in the "fMeasure": difference is found wrt. how the 'clusters are weighted', ie, "max possible pairs between clusters" VS "observed number of paris between clusters" (where the lattr is used in the maximum-match-measure, ie, 'this').
    //! Note: a 'second' differece between "maximum-match-measure" and "f-measure" is thatwe 'in this' does Not muliply by/with the 'number of elements in each cluster'.
    t_float sumOf_1_sizes = 0;
    for(uint head_id = 0; head_id < nrows; head_id++) {
      assert(head_id < self->mapOfCluster1_size);
      const t_float cntInCluster_1 = self->mapOfCluster1[head_id]; 
      t_float max_sum = T_FLOAT_MIN_ABS;
      for(uint tail_id = 0; tail_id < ncols; tail_id++) {
	const t_float sum_and = self->matrixOf_coOccurence.matrix[head_id][tail_id];    
	assert(sum_and != T_FLOAT_MAX);
	//! update the max-score-property:
	max_sum = macro_max(max_sum, sum_and);
      }
      //! Update the global-score-property:
      if(max_sum != T_FLOAT_MIN_ABS) {
	score += max_sum;
	//score += cntInCluster_1*max_sum;
      } 
      sumOf_1_sizes += cntInCluster_1;
    }
    //!
    //! Adjust the sum:
    //printf("sumOf_1_sizes=%f, score=%f, at %s:%d\n", sumOf_1_sizes, score, __FILE__, __LINE__);
    if( //(sumOf_1_sizes != 0) &&
       (score != 0) ) {
      score = score / (t_float)cnt_vertices; //! ie, the relative 'importance' of the clusters.o
      //score = score / sumOf_1_sizes; //! ie, the relative 'importance' of the clusters.o
    }
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_vanDogen) {
    //! Note: we extend the [above] "maximum-match-measure" to adjust the score by the 'min-values' (instead of diviging wrt. the total sum of elements).
#include "kt_matrix_cmpCluster__stub__metric__vanDogen.c"
    //!
    //! Adjust the sum:
	score = (sumOf_1_sizes + sumOf_2_sizes) - (sumOf_scores_max_1 + sumOf_scores_max_2); //! ie, $|P_1| + |P_2| - (d(P_1) + d(P_2))$, for cluster-partiions $P_1, P_2$
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relative) {
    //! ------------------------------------------------------------------------------------------------------------ 
    //! Note: we here copmute the 'relative signficance' between the min-case and max-case
    // fIXME: evlauate the signifcance of this adjustment. <-- try to suggest/hyptoehse how 'this relative adjsutment' may 'afffect' teh distance-metic-comptautiosn.
    // FIXME: try describing a 'case' where 'this oekseth-permtuation' may be sued also for other metrics
#include "kt_matrix_cmpCluster__stub__metric__vanDogen.c"
    //!
    //! Adjust the sum:
    const t_float sum_min = (sumOf_scores_min_1 + sumOf_scores_min_2);
    const t_float sum_max = (sumOf_scores_max_1 + sumOf_scores_max_2);
    //! Apply the genralized formula:
    score = __cmpClusterMacro__minByMax(); //! ie, $ (max\{d(C_x \in P_1, C_k \in P_1) \} + max\{d(C_x \in P_2, C_k \in P_2) \}) / (min\{d(C_x \in P_1, C_k \in P_1) \} + max\{d(C_x \in P_2, C_k \in P_2) \}), for cluster-partiions $P_1, P_2$ and clusters $C_x, C_k$.
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared) {
    //! Note: we here copmute the 'relative signficance' between the min-case and max-case
    // fIXME: evlauate the signifcance of this adjustment.
#include "kt_matrix_cmpCluster__stub__metric__vanDogen.c"
    //!
    //! Adjust the sum:
    const t_float sum_min = (sumOf_scores_min_1 * sumOf_scores_min_2);
    const t_float sum_max = (sumOf_scores_max_1 * sumOf_scores_max_2);
    //! Apply the genralized formula:
    score = __cmpClusterMacro__minByMax(); //! ie, $ (max\{d(C_x \in P_1, C_k \in P_1) \} * max\{d(C_x \in P_2, C_k \in P_2) \}) / (min\{d(C_x \in P_1, C_k \in P_1) \} * max\{d(C_x \in P_2, C_k \in P_2) \}), for cluster-partiions $P_1, P_2$ and clusters $C_x, C_k$.
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_min) {
    //! Note: we here copmute the 'relative signficance' between the min-case and max-case
    // fIXME: evlauate the signifcance of this adjustment.
#include "kt_matrix_cmpCluster__stub__metric__vanDogen.c"
    //!
    //! Adjust the sum:
    const t_float sum_min = (sumOf_scores_min_1 * sumOf_scores_min_2);
    const t_float sum_max = (sumOf_scores_max_1 + sumOf_scores_max_2);
    //! Apply the genralized formula:
    score = __cmpClusterMacro__minByMax();
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_vanDogen_permtuation_oekseth_relativeSquared_max) {
    //! Note: we here copmute the 'relative signficance' between the min-case and max-case
    // fIXME: evlauate the signifcance of this adjustment.
#include "kt_matrix_cmpCluster__stub__metric__vanDogen.c"
    //!
    //! Adjust the sum:
    const t_float sum_min = (sumOf_scores_min_1 + sumOf_scores_min_2);
    const t_float sum_max = (sumOf_scores_max_1 * sumOf_scores_max_2);
    //! Apply the genralized formula:
    score = __cmpClusterMacro__minByMax();
    //! ------------------------------------------------------------------------------------------------------------ 
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_mutualInforation_Strehl_Ghosh) {
#include "kt_matrix_cmpCluster__stub__metric__mutualInformation.c" //! where we compute: "entropy_cluster1", "entropy_cluster2", "entry_both", "sumOf_1_sizes"
    //!
    //! Update the score:
    const t_float numerator = entry_both;
    const t_float denum = entropy_cluster1 * entropy_cluster2;
    if( (numerator != 0) && (denum != 0) ) {
      score = entry_both / mathLib_float_sqrt_abs(denum);
    }    
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_mutualInforation_Fred_Jain) {
#include "kt_matrix_cmpCluster__stub__metric__mutualInformation.c" //! where we compute: "entropy_cluster1", "entropy_cluster2", "entry_both", "sumOf_1_sizes"
    //!
    //! Update the score:
    const t_float numerator = 2*entry_both;
    const t_float denum = entropy_cluster1 + entropy_cluster2;
    if( (numerator != 0) && (denum != 0) ) {
      score = entry_both / denum;
    }
  } else if(
	    (cmp_type == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenSum) 
	    /* ||  */
	    /* (cmp_type == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenSum)  */
	    /* ||  */
	    /* (cmp_type == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_scores_Euclid_within_divBy_betweenMax)  */
	    /* ||  */
	    /* (cmp_type == e_kt_matrix_cmpCluster_metric_SSE_vertex_defineCenterBy_avg_ranks_Euclid_within_divBy_betweenMax)  */
	    //|| 
	    ) {
    //! Apply logics: 'each clsuter':
    //printf("cnt_cluster=[%u, %u], at %s:%d\n", self->clusterDistances_cluster1.cnt_cluster, self->clusterDistances_cluster2.cnt_cluster, __FILE__, __LINE__);
    //! Note: In this metric we cous on vertices difference to clusters (ie, in cotnrast to [ªbove] metrics which are set-based 'studies'): the vertices 'inferred attributes' are unifed/combined through 'avg', 'min' and 'max'.
    //! Note: for an informative description of the "Silhouette index" see "https://cs.fit.edu/~pkc/classes/ml-internet/silhouette.pdf" and "https://en.wikipedia.org/wiki/Silhouette_(clustering)"
    if( (self->clusterDistances_cluster1.nrows != 0)  && (self->clusterDistances_cluster2.nrows != 0) ) {
      const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t local_enumId = map_setBased_SSE_toMatrixBased__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(cmp_type);
      //printf("-----, at %s:%d\n", __FILE__, __LINE__);
      t_float prediction_cluster1 = compute__SSE__s_kt_matrix_cmpCluster_clusterDistance_t(//local_enumId, 
											   &(self->clusterDistances_cluster1), e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, /*isTo_adjustEachRowScore_by_log=*/false, /*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*isTo__useInternalObjectSpec=*/false);
      //printf("-----, at %s:%d\n", __FILE__, __LINE__);
      t_float prediction_cluster2 = compute__SSE__s_kt_matrix_cmpCluster_clusterDistance_t(//local_enumId, 
											   &(self->clusterDistances_cluster2), e_kt_matrix_cmpCluster_clusterDistance__config_metric__default, /*isTo_adjustEachRowScore_by_log=*/false, /*metric=*/e_kt_correlationFunction_groupOf_minkowski_euclid, /*isTo__useInternalObjectSpec=*/false);
      //!
      //! Compute:
      // FIXME: try improving [below] ... using an 'emprical appraoch' to 'figure out how'. <-- thereafter wrtie different permtautison 'of this' using the 'classifcial distnace-correlation-emtric-permtautiosn'      
      const t_float num   = macro_min(prediction_cluster1, prediction_cluster2);
      const t_float denum = macro_max(prediction_cluster1, prediction_cluster2);
      if( (num != 0) && (denum != 0) ) {score = num / denum;}
    } else {
      if(false) {fprintf(stderr, "!!\t The cluster-distance-objects were Not build: please validate wrt. your API that a distance-matrix was used 'in the input' when building the distance-comarpison-scores. For quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);      } // FIXME: consider instead moving this to the documetnaiton .... to avoiud all to many erorrs during bathc-copmptautiosn.
      return;
    }

  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_variationOfInformation) {
#include "kt_matrix_cmpCluster__stub__metric__mutualInformation.c" //! where we compute: "entropy_cluster1", "entropy_cluster2", "entry_both", "sumOf_1_sizes"
    //!
    //! Update the score:
    score = (entropy_cluster1 + entropy_cluster2) - 2*entry_both;
  } else if(cmp_type == e_kt_matrix_cmpCluster_metric_variationOfInformation_oekseth_relative) {
#include "kt_matrix_cmpCluster__stub__metric__mutualInformation.c" //! where we compute: "entropy_cluster1", "entropy_cluster2", "entry_both", "sumOf_1_sizes"
    //!
    //! Update the score:
    // FIXME: write an 'invesitgation' wrt. this metric ... and vlaidate/test if 'this permtuation' may provide mroe inforamtion wrt. the 'ideal case' VS the 'non-ideal case'.
    const t_float numerator = entropy_cluster1 - entry_both;
    const t_float denum = entropy_cluster2 - entry_both;
    if( (numerator != 0) && (denum != 0) ) {
      score = entry_both / denum;
    }
  } else { //! then we have 'forgot' to update this fucntion 'with such a support':
    fprintf(stderr, "!!(metric-not-yet-supported)\t Please send a request to the devleoper [oekseth@gmail.com] for metric='%u'. Observation at [%s]:%s:%d\n", cmp_type, __FUNCTION__, __FILE__, __LINE__);
    assert(false); // ie, as we then need to 'add support for this'.
  }



  //! ----------------------
  //!
  //! Retunr the score:
  scalar_result->score_1 = score;
}
