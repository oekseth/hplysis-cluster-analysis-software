#ifndef kt_set_2dsparse_h
#define kt_set_2dsparse_h

 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_set_2dsparse
   @brief a data-structure for holding a spase matrix of elements
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/
#include "types.h"
#include "def_intri.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "e_kt_set_sparse_colAttribute.h"
//#include "log_clusterC.h"
#include "kt_list_1d.h"

/**
   @struct s_kt_set_2dsparse
   @brief defines a 2d-struct to hold sparse data-sets
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2016)
 **/
typedef struct s_kt_set_2dsparse {
  uint _nrows; //! ie, the size of the matrixOf_scores and matrixOf_id matrices
  uint biggestInserted_row_id;
  t_float **matrixOf_scores;
  uint **matrixOf_id;
  //uint *mapOf_columnSize; //! ie, the number of elments in "mapOf_columnSize" (and matrixOf_scores is the latter is used);  
} s_kt_set_2dsparse_t;


//! @return the max number of inserted row-ids.
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
uint get_maxInsertedRowId(const s_kt_set_2dsparse_t *self) {
  assert(self);
  if( (self->biggestInserted_row_id != UINT_MAX)) {
    assert(self->biggestInserted_row_id < self->_nrows);
    return self->biggestInserted_row_id;
  } else {return self->_nrows;}
}

#ifdef SWIG
#undef assert
#define assert(v) ({;}) //! ie, a dummy construct
#endif // #ifndef SWIG
 

//! Intiates an empty object
void setTo_Empty__s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *self);
//! Intiates an empty object
static s_kt_set_2dsparse_t setToEmptyAndReturn__s_kt_set_2dsparse_t() {
  s_kt_set_2dsparse_t self; setTo_Empty__s_kt_set_2dsparse_t(&self); return self;
}

/**
   @brief allocates an s_kt_set_2dsparse_t object
   @param <self> is the object to allocate
   @param <nrows> is the number of rows to allocate
   @param <ncols> is the number of columsn in the matrix input, ie, ignroed if matrix==NULL
   @param <matrix> if set then we itnaite the object using 'this' input-parameter
   @param <mask> if set then we assume the makss are eplxict, ie, only inlcude for "mask[i][j] == 0"
   @param <isTo_useImplictMask> which if set implies that we investigate for T_FLOAT_MAX
   @param <isTo_allocateFor_scores> which if set to false implies that we do Not expect the scores to be of itnerest, ie, an parameter used to reduce the memory-consutmpion
 **/
void allocate__s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *obj, const uint nrows, const uint ncols, t_float **matrix, char **mask, const bool isTo_useImplictMask, const bool isTo_allocateFor_scores);
//! intialize and return an s_kt_set_2dsparse_t objec t(oekseth, 06. mar. 2017).
static s_kt_set_2dsparse_t initAndReturn__allocate__s_kt_set_2dsparse_t(const uint nrows, const bool isTo_allocateFor_scores) {
  s_kt_set_2dsparse_t self; setTo_Empty__s_kt_set_2dsparse_t(&self);
  allocate__s_kt_set_2dsparse_t(&self, nrows, /*ncols=*/100, NULL, NULL, false, isTo_allocateFor_scores);
  //! @return
  return self;
}

//! @return an empty object of this type
static s_kt_set_2dsparse_t get_empty__s_kt_set_2dsparse_t() {
  s_kt_set_2dsparse_t obj;
  setTo_Empty__s_kt_set_2dsparse_t(&obj);
  return obj;
}

//! De-allcoates an object of type s_kt_set_2dsparse_t
static void free_s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *self) {
  assert(self);
  if(self->matrixOf_scores) {
    for(uint row_id = 0; row_id < self->_nrows; row_id++) {
      if(self->matrixOf_scores[row_id]) {
	free_1d_list_float(&(self->matrixOf_scores[row_id]));
	self->matrixOf_scores[row_id] = NULL;
      }
    }
    free_generic_type_2d(self->matrixOf_scores); self->matrixOf_scores = NULL;
    //free_2d_list_float(&(self->matrixOf_scores));
  }
  if(self->matrixOf_id) {
    for(uint row_id = 0; row_id < self->_nrows; row_id++) {
      if(self->matrixOf_id[row_id]) {
	free_1d_list_uint(&(self->matrixOf_id[row_id]));
	self->matrixOf_id[row_id] = NULL;
      }
    }
    free_generic_type_2d(self->matrixOf_id); self->matrixOf_id = NULL;
    //free_2d_list_uint(&(self->matrixOf_id));
  }
  //if(self->mapOf_columnSize) {free_1d_list_uint(&(self->mapOf_columnSize));}
}

/**
   @brief Sort the complete number opf tiems in the sparse matrix
   @param <self> is the object to sort
   @param <isTo_sortKeys> which fi set to false impleis that we only sort for the socre-weights, an option which is of interst/importance wrt. builidng of sparse non-rank correlation-matrices.
**/
void sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const bool isTo_sortKeys);

/**
   @brief push an element to the row, ie, to the stack
   @param <self> is the object to udpate
   @param <row_id> is the row to udpate for
   @param <vertex_to_add> is the element to push.
**/
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
void push__s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *self, const uint row_id, const uint vertex_to_add) {
  assert(self);
  //assert(row_id < self->_nrows);
  assert(self->matrixOf_id);
  assert(vertex_to_add != UINT_MAX);
  //! ----------------------------------------------------------
  //!
  //! Generalize the data-access:
  assert(row_id < self->_nrows);
  uint *rowOf_id = self->matrixOf_id[row_id]; t_float *rowOf_scores = (self->matrixOf_scores == NULL) ? NULL : self->matrixOf_scores[row_id];
  if( (self->biggestInserted_row_id == UINT_MAX) || (row_id > self->biggestInserted_row_id) ) {self->biggestInserted_row_id = row_id;}
#ifndef NDEBUG
  if(rowOf_id) {
    uint size        = rowOf_id[e_kt_set_sparse_colAttribute_size]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
    if(size == UINT_MAX) {
      fprintf(stderr, "!!\t current-pos=%u, at %s:%d\n", rowOf_id[e_kt_set_sparse_colAttribute_currentPos], __FILE__, __LINE__);
    }
    assert(size != UINT_MAX);
  }
#endif
  //  assert(row_id);
  //!
  //! Apply the logics:
  const uint row_size = 1; //! ie, as we are only to add one elemnet
#define __localConfig__isTo__addRow 0
  //  printf("current_pos=%u, at %s:%d\n", (rowOf_id) ? rowOf_id[e_kt_set_sparse_colAttribute_currentPos] : 0, __FILE__, __LINE__);
#include "kt_set_parse__func__add_rowSparse_andEnlargeIfNeccecary.c" //! ie, the udpate-logics for insertion and memory-enarlgement.
  //  printf("at %s:%d\n", __FILE__, __LINE__);
#undef __localConfig__isTo__addRow
  self->matrixOf_id[row_id] = rowOf_id;
  //  rowOf_scores[2+current_pos] = scoreTo_set;
  if(self->matrixOf_scores)  {self->matrixOf_scores[row_id] = rowOf_scores;}
#ifndef NDEBUG
  if(rowOf_id) {
    assert(rowOf_id[e_kt_set_sparse_colAttribute_currentPos] != UINT_MAX);
    assert(rowOf_id[e_kt_set_sparse_colAttribute_size] != UINT_MAX);
  }
#endif
#ifndef NDEBUG
  if(rowOf_id) {
    uint size        = rowOf_id[e_kt_set_sparse_colAttribute_size]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
    assert(size != UINT_MAX);
  }
#endif
  /* if(true) { */
  /*   const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos] - 1; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size. */
  /*   const uint last_inserted = rowOf_id[2+current_pos]; */
  /*   printf("(top-element) vertex=%u, (given current-pos=%u) at %s:%d\n", last_inserted, current_pos, __FILE__, __LINE__); */
  /*   assert(last_inserted == vertex_to_add); //! ie, what we expect. */
  /* } */
}

/**
   @brief push an (id, score) element to the row, ie, to the stack (oekseth, 06. feb. 2017)
   @param <self> is the object to udpate
   @param <row_id> is the row to udpate for
   @param <vertex_to_add> is the element to push.
   @param <scoreTo_set> is the score to set.
**/
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
void push__idScorePair__s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *self, const uint row_id, const uint vertex_to_add, const t_float scoreTo_set) {
  assert(self);
  //assert(row_id < self->_nrows);
  assert(self->matrixOf_id);
  assert(vertex_to_add != UINT_MAX);
  //! ----------------------------------------------------------
  //!
  //! Generalize the data-access:
  uint *rowOf_id = self->matrixOf_id[row_id]; t_float *rowOf_scores = (self->matrixOf_scores == NULL) ? NULL : self->matrixOf_scores[row_id];
  if( (self->biggestInserted_row_id == UINT_MAX) || (row_id > self->biggestInserted_row_id) ) {self->biggestInserted_row_id = row_id;}
  assert(self->matrixOf_scores);
  //!
  //! Apply the logics:
  const uint row_size = 1; //! ie, as we are only to add one elemnet
#define __localConfig__isTo__addRow 0
#define __localConfig__isTo__setScore 1
#include "kt_set_parse__func__add_rowSparse_andEnlargeIfNeccecary.c" //! ie, the udpate-logics for insertion and memory-enarlgement.
#undef __localConfig__isTo__addRow
#undef __localConfig__isTo__setScore
  self->matrixOf_id[row_id] = rowOf_id;
  if(self->matrixOf_scores)  {self->matrixOf_scores[row_id] = rowOf_scores;}
  /* if(true) { */
  /*   const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos] - 1; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size. */
  /*   const uint last_inserted = rowOf_id[2+current_pos]; */
  /*   printf("(top-element) vertex=%u, (given current-pos=%u) at %s:%d\n", last_inserted, current_pos, __FILE__, __LINE__); */
  /*   assert(last_inserted == vertex_to_add); //! ie, what we expect. */
  /* } */
}

/**
   @brief pop an element to the row, ie, remove an elment.
   @param <self> is the object to udpate
   @param <row_id> is the row-id to pop the elmenet for.
   @param <scalar_score> is the assicated score (ie any) to set.
   @return the previous/removed/old top-element.
**/
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
 uint pop__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const uint row_id, t_float *scalar_score) {
  assert(self);
  assert(row_id < self->_nrows);
  assert(self->matrixOf_id);
  //! ----------------------------------------------------------
  //!
  //uint *rowOf_id = self->rowOf_id;   t_float *rowOf_scores = self->rowOf_scores;
  uint *rowOf_id = self->matrixOf_id[row_id]; t_float *rowOf_scores = (self->matrixOf_scores == NULL) ? NULL : self->matrixOf_scores[row_id];
  *scalar_score = T_FLOAT_MAX;
  //!
  //! Apply the logics:
  if(rowOf_id[e_kt_set_sparse_colAttribute_currentPos] > 0) { 
    rowOf_id[e_kt_set_sparse_colAttribute_currentPos]--;
    const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
    *scalar_score = (rowOf_scores) ? rowOf_scores[current_pos] : T_FLOAT_MAX;
    assert(rowOf_id[e_kt_set_sparse_colAttribute_currentPos] != UINT_MAX);
    return rowOf_id[2+current_pos];
  } else {return UINT_MAX;} //! ie, as the stack-element was then 'not found'.
}

//! @return the current-insert-positon of the "row_id" (eosketh, 06. au.2017)
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
 uint get_currrentPos__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const uint row_id) {
  assert(self);
  assert(row_id < self->_nrows);
  assert(self->matrixOf_id);
  const uint *rowOf_id = self->matrixOf_id[row_id]; 
  if(rowOf_id) {
    //    printf("get(currentPos:\"%p\"): %u, row_id=%u, at %s:%d\n", self, rowOf_id[e_kt_set_sparse_colAttribute_currentPos], row_id,  __FILE__, __LINE__);
    return rowOf_id[e_kt_set_sparse_colAttribute_currentPos];
  } else {return 0;}
}

//! Clear a givne forest-id (though Not de-allcoate memory) (eosketh, 06. des. 2016).
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
 void clear_row__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const uint row_id) {
  assert(self);
  assert(row_id < self->_nrows);
  assert(self->matrixOf_id);
  uint *rowOf_id = self->matrixOf_id[row_id]; 
  //  printf("set(currentPos:\"%p\"): %u, try for row_id=%u, at %s:%d\n", self, 0, row_id, __FILE__, __LINE__);
  if(rowOf_id) {
    const uint prev_size = rowOf_id[e_kt_set_sparse_colAttribute_size]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.;
    const uint current_pos = rowOf_id[e_kt_set_sparse_colAttribute_currentPos];
    assert(current_pos != UINT_MAX);
    assert(current_pos <= prev_size);
    for(uint i = 0; i <  current_pos; i++) {
      rowOf_id[e_kt_set_sparse_colAttribute_undef+i] = UINT_MAX;
      //      rowOf_id[e_kt_set_sparse_colAttribute_currentPos+i+current_pos] = UINT_MAX;
    }
    //    printf("set(currentPos:\"%p\"): %u, at %s:%d\n", self, 0, __FILE__, __LINE__);
    rowOf_id[e_kt_set_sparse_colAttribute_currentPos] = 0;  
    rowOf_id[e_kt_set_sparse_colAttribute_size] = prev_size; //! ie, to 'safe-guard' against any errnous 'settings'.
    const uint size        = rowOf_id[e_kt_set_sparse_colAttribute_size]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
    assert(size != UINT_MAX);
  }
}

/**
   @brief Update the row with the defined set of values, ie, insert a new condensed row
   @param <self> is the object to udpate
   @param <row_id> is the row to udpate for
   @param <row> ist eh sparse row (ie, where alle lements are of itnerest)
   @param <row_size> is the nubmer of itnerestign elements in row
   @param <row_score> which if set is the set of scores to allocate: should reflect the "isTo_allocateFor_scores" object used when intiated the self object.
**/
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
 void add_rowSparse__s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *self, const uint row_id, const uint *__restrict__ row, const uint row_size, const t_float *__restrict__ row_score) {
  assert(self);
  assert(row_id < self->_nrows);
  assert(self->matrixOf_id);
  assert(row_size);
  if( (self->biggestInserted_row_id == UINT_MAX) || (row_id > self->biggestInserted_row_id) ) {self->biggestInserted_row_id = row_id;}
  //if(row_id > self->biggestInserted_row_id) {self->biggestInserted_row_id = row_id;}
  /* const uint size        = self->matrixOf_id[row_id][e_kt_set_2dsparse_colAttribute_size]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size. */
  /* const uint current_pos = self->matrixOf_id[row_id][e_kt_set_2dsparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size. */
  /* const uint new_size = row_size + current_pos;   */
  //! ----------------------------------------------------------
  //! Generalize the data-access:
  uint *rowOf_id = self->matrixOf_id[row_id];
  t_float *rowOf_scores = (self->matrixOf_scores == NULL) ? NULL : self->matrixOf_scores[row_id];

  
  //!
  //! Apply the logics:
#define __localConfig__isTo__addRow 1
#include "kt_set_parse__func__add_rowSparse_andEnlargeIfNeccecary.c" //! ie, the udpate-logics for insertion and memory-enarlgement.
  //! Ensure that the itnernal poitners are up-to-date:
  self->matrixOf_id[row_id] = rowOf_id;
  if(self->matrixOf_scores)  {self->matrixOf_scores[row_id] = rowOf_scores;}
#undef __localConfig__isTo__addRow
}

/**
   @brief Update the row with the defined set of values, ie, insert a new condensed row
   @param <self> is the object to udpate
   @param <row_id> is the row to udpate for
   @param <row> is a row of values which intersting valeus are Not set to T_FLOAT_MAX
   @param <ncols> is the nubmer of itnerestign elements in row
   @remarks we only insert wrt. weights if the "isTo_allocateFor_scores" was set to trie when intiated the self object.
**/
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
 void add_row_maskImplicit__s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *self, const uint row_id, const t_float *__restrict__ row, const uint ncols) {
  assert(self); assert(row);
#define __localConfig__mask__useMaskExplciit 0
#define __localConfig__isCalledFrom_2dObject 1
  const t_float *rowOf_scores = (self->matrixOf_scores == NULL) ? NULL : self->matrixOf_scores[row_id];
#include "kt_set_sparse__func__add_maskRow.c" //! ie, the udpate-logics.
#undef __localConfig__mask__useMaskExplciit
#undef __localConfig__isCalledFrom_2dObject
}


/**
   @brief Update the row with the defined set of values, ie, insert a new condensed row
   @param <self> is the object to udpate
   @param <row_id> is the row to udpate for
   @param <row> is a row of values which intersting valeus are Not set to T_FLOAT_MAX
   @param <ncols> is the nubmer of itnerestign elements in row
   @param <mask> is the mask to be used wrt. idneitfying/knowing fi the row is of interest.
   @remarks we only insert wrt. weights if the "isTo_allocateFor_scores" was set to trie when intiated the self object.
**/
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
 void add_row_maskExplicit(s_kt_set_2dsparse_t *self, const uint row_id, const t_float *__restrict__ row, const uint ncols, const char *__restrict__ mask) {
  assert(self); assert(row);
#define __localConfig__mask__useMaskExplciit 1
#define __localConfig__isCalledFrom_2dObject 1
  const t_float *rowOf_scores = (self->matrixOf_scores == NULL) ? NULL : self->matrixOf_scores[row_id];
#include "kt_set_sparse__func__add_maskRow.c" //! ie, the udpate-logics.
#undef __localConfig__mask__useMaskExplciit
#undef __localConfig__isCalledFrom_2dObject
}


//! @return a sparse row of related entitee assicated to row_id
#ifndef SWIG
static  inline __attribute__((always_inline)) uint *get_sparseRow_ofIds__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const uint row_id, uint *scalar_rowSize)
#else
static uint *get_sparseRow_ofIds__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const uint row_id, uint *scalar_rowSize)
#endif // #ifndef SWIG
{
/* #ifndef SWIG */
/*    assert(self); */
/* #endif  */
   
  //printf("get row_id=%u <? %u, at %s:%d\n", row_id, self->_nrows, __FILE__, __LINE__);

  if(row_id < self->_nrows) {
#ifndef SWIG
    assert(row_id < self->_nrows);
    assert(self->matrixOf_id);
    assert(self->matrixOf_id[row_id]);
#endif // #ifndef SWIG
    *scalar_rowSize = self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
    //*scalar_rowSize = self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
    return &self->matrixOf_id[row_id][2];
  } else {
    *scalar_rowSize = 0;
    return NULL;
  }
}
//! @return a sparse row of related entitee assicated to row_id
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
 t_float *get_sparseRow_ofScores__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const uint row_id, uint *scalar_rowSize) {
  /* assert(self); */
  /* assert(row_id < self->_nrows); */
  /* assert(self->matrixOf_scores); */
  if(row_id < self->_nrows) {
    *scalar_rowSize = self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_currentPos]; //! ie, as we asusme teh first elmenet is used to 'reprsent' the row-size.
    return self->matrixOf_scores[row_id];
  } else {
    *scalar_rowSize = 0;
    return NULL;
  }
  //return &self->matrixOf_scores[row_id][2];
}

//! @return true if the pari is indie teh data-set.
static
#ifndef SWIG
inline __attribute__((always_inline))
#endif // #ifndef SWIG
 bool has_pair__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const uint row_id, const uint col_id) {
  assert(self);
  assert(row_id != UINT_MAX);  assert(col_id != UINT_MAX);

  if(row_id < self->_nrows) {
    //! Iterate:
    const uint size = self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_size];
    const uint *__restrict__ row = &(self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_currentPos]);
    assert(row);
    const uint search_id = col_id;
    for(uint i = 0; i <= size; i++) {
      if(row[i] == search_id) {return true;}
    }
  }
  if(col_id < self->_nrows) {
    //! Iterate:
    const uint size = self->matrixOf_id[col_id][e_kt_set_sparse_colAttribute_size];
    const uint *__restrict__ row = &(self->matrixOf_id[col_id][e_kt_set_sparse_colAttribute_currentPos]);
    assert(row);
    const uint search_id = row_id;
    for(uint i = 0; i <= size; i++) {
      if(row[i] == search_id) {return true;}
    }
  }

  return false;
}

/**
   @brief converts an "s_kt_list_1d_pairFloat_t" object into our "s_kt_set_2dsparse_t" object (oekseth, 06. mar. 2017).
   @param <obj> is the object to covnert from.
   @return the new-allcoated s_kt_set_2dsparse_t object.
 **/
static s_kt_set_2dsparse_t convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(s_kt_list_1d_pairFloat_t *obj) {
  assert(obj); assert(obj->list_size > 0); assert(obj->list);
  //!
  //! Find the dimesnions:
  uint max_head = 0;   
  // uint max_tail = 0;
  for(uint i = 0; i < obj->list_size; i++) {
    if(obj->list[i].head != UINT_MAX) {
      const uint head_id = obj->list[i].head;
      assert(head_id < (1000*1000)); // TODO: cosnider rmeoving this assert ... which is incldued iot. simplify idnetificioant of parsing-errors.
      max_head = macro_max(max_head, head_id);
    }
    /* if(obj->list[i].tail != UINT_MAX) { */
    /*   max_tail = macro_max(max_tail, obj->list[i].tail); */
    /* } */
  }
  max_head++; 
  
  //printf("max_head=%u, at %s:%d\n", max_head, __FILE__, __LINE__);

  s_kt_set_2dsparse_t self; 
  /* //max_tail++; */
  //!
  //! Allocate:
  setTo_Empty__s_kt_set_2dsparse_t(&self);
  allocate__s_kt_set_2dsparse_t(&self, max_head, 100, NULL, NULL, false, /*isTo_allocateFor_scores=*/true);
  //!
  //! Add values:
  for(uint i = 0; i < obj->list_size; i++) {
    if(false == MF__isEmpty__s_ktType_pairFloat(obj->list[i])) { //! then we asusme the pari is of interst:
      assert(obj->list[i].head < max_head);
      assert(obj->list[i].head < self._nrows);
      //! Insert:
      push__idScorePair__s_kt_set_2dsparse_t(&self, obj->list[i].head, obj->list[i].tail, obj->list[i].score);
    }
  }
  //!
  //! @return
  return self;
}


/**
   @brief converts an "s_kt_list_1d_pair_t" object into our "s_kt_set_2dsparse_t" object (oekseth, 06. mar. 2017).
   @param <obj> is the object to covnert from.
   @return the new-allcoated s_kt_set_2dsparse_t object.
 **/
static s_kt_set_2dsparse_t convertFrom__s_kt_list_1d_pair__s_kt_set_2dsparse_t(s_kt_list_1d_pair_t *obj) {
  assert(obj); assert(obj->list_size > 0); assert(obj->list);
  //!
  //! Find the dimesnions:
  uint max_head = 0;   
  // uint max_tail = 0;
  for(uint i = 0; i < obj->list_size; i++) {
    if(obj->list[i].head != UINT_MAX) {
      max_head = macro_max(max_head, obj->list[i].head);
    }
    /* if(obj->list[i].tail != UINT_MAX) { */
    /*   max_tail = macro_max(max_tail, obj->list[i].tail); */
    /* } */
  }
  max_head++; 
  //max_tail++;
  //!
  //! Allocate:
  s_kt_set_2dsparse_t self; setTo_Empty__s_kt_set_2dsparse_t(&self);
  allocate__s_kt_set_2dsparse_t(&self, max_head, 100, NULL, NULL, false, /*isTo_allocateFor_scores=*/false);
  //!
  //! Add values:
  //printf("at %s:%d\n", __FILE__, __LINE__);
  for(uint i = 0; i < obj->list_size; i++) {
    if(false == MF__isEmpty__s_ktType_pair(obj->list[i])) { //! then we asusme the pari is of interst:
      assert(obj->list[i].head != UINT_MAX);
      assert(UINT_MAX != get_currrentPos__s_kt_set_2dsparse_t(&self, obj->list[i].head)); //! ie, validte that the list-set does not have inconditencies (oekseth, 06. sept. 2017).
      push__s_kt_set_2dsparse_t(&self, obj->list[i].head, obj->list[i].tail);
    }
  }
  //printf("at %s:%d\n", __FILE__, __LINE__);
  //!
  //! @return
  return self;
}


#endif //! EOF
