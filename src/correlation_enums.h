#ifndef correlation_enums_h
#define correlation_enums_h

#include "e_kt_correlationFunction.h"

/**
   @brief Specify the type (if any) of post-processing-operations to be taken place on the comptued distances when/after the result is generated (oekseth, 06. juni 2016).
 **/
typedef enum e_allAgainstAll_SIMD_inlinePostProcess {
  e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights, // <-- FIXME: update the name/description of this when we understand the purpose/logics behind the "calculate weights" function/lgoics.
  //e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_inverted,
  e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min,
  e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max,
  e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum,
  //  e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum_dividedBy_sumOf_elements,
  e_allAgainstAll_SIMD_inlinePostProcess_none_storeIntermediateResults_forMPI,
  e_allAgainstAll_SIMD_inlinePostProcess_undef
} e_allAgainstAll_SIMD_inlinePostProcess_t;


//! Get the 'defualt value' to inite a list with (eosekth, 06. des. 2016).
//! @return a value which is to be used as a 'start-point' wrt. using our "e_allAgainstAll_SIMD_inlinePostProcess_t" selection-proerpty.
static const t_float get_defaultValue__e_allAgainstAll_SIMD_inlinePostProcess_t(const e_allAgainstAll_SIMD_inlinePostProcess_t cmp_type) {
  t_float empty_value = 0;
  if(cmp_type == e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights) {empty_value = 0;}
  else if(cmp_type == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min) {empty_value = T_FLOAT_MAX;}
  else if(cmp_type == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max) {empty_value = T_FLOAT_MIN_ABS;}
  else if(cmp_type == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum) {empty_value = 0;}
  else if(cmp_type == e_allAgainstAll_SIMD_inlinePostProcess_none_storeIntermediateResults_forMPI) {empty_value = 0;}
  else if(cmp_type == e_allAgainstAll_SIMD_inlinePostProcess_undef) {empty_value = 0;}
  else {assert(false);} //! ie, as we then ened to add support 'for this case'.
    
  return empty_value;
}

/**
   @brief Define a set of macros to be used to simplifyi the 'setting' of the "e_allAgainstAll_SIMD_inlinePostProcess" enums (oekseth, 06. otk. 201&)
**/
#define macro__e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights                 0
#define macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min                      1
#define macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max                      2
#define macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum                      3
#define macro__e_allAgainstAll_SIMD_inlinePostProcess_none_storeIntermediateResults_forMPI  4
#define macro__e_allAgainstAll_SIMD_inlinePostProcess_undef                                 5
#define macro__e_allAgainstAll_SIMD_inlinePostProcess_nonOptimizedEvaluateAllCases          6 //! which if used in our "func_template__allAgainstAll.c" to tes tthe perofmrance-overhead wrt. notn inluing the psot-prcoesisng options ... ie, for whcih we may infer/assert the compilers 'ability' to optmize code if 'the compiler knows that the temorary resutls may be used directly, ie, without storing into a temproary list' (oekseth, 06. otk. 2016).



//! Specify the type of correlation-metric to use (ie, if any).
typedef enum e_typeOf_metric_correlation_pearson {
  e_typeOf_metric_correlation_pearson_basic,
  e_typeOf_metric_correlation_pearson_absolute,
  e_typeOf_metric_correlation_pearson_uncentered,
  e_typeOf_metric_correlation_pearson_uncentered_absolute,
  e_typeOf_metric_correlation_pearson_spearMan,
  e_typeOf_metric_correlation_pearson_undef
} e_typeOf_metric_correlation_pearson_t;

//! Specify if the masks are evaluated and set:
//! @remarks with masks we refer both to explict "mask" matrices and implictt (though the T_FLOAT_MAX variable).
typedef enum e_cmp_masksAre_used {
  e_cmp_masksAre_used_true,
  e_cmp_masksAre_used_false,
  e_cmp_masksAre_used_undef
} e_cmp_masksAre_used_t;

//! An enum which is sued to test/ivnestigate if it is safe to use an optmized version of an all-against-all Spearman comptuation.
typedef enum e_typeOf_measure_spearman_masksAreAligned {
  e_typeOf_measure_spearman_masksAreAligned_always, //! 
  // FIXME: update our code wrt. [”elow] ... describe applicatiosn ... and 'examplein' why the ltter 'strategy' is not by default used in the 'orignal' "c clsuter" inmplementaiton.
  e_typeOf_measure_spearman_masksAreAligned_always_useFilters_duringRankApplication, //! ie, when evlauating the ranks.
  e_typeOf_measure_spearman_masksAreAligned_compare,
  e_typeOf_measure_spearman_masksAreAligned_never,
  e_typeOf_measure_spearman_masksAreAligned_undef
} e_typeOf_measure_spearman_masksAreAligned_t;

//! Specify the type of optimizations to use
typedef enum e_typeOf_optimization_distance {
  e_typeOf_optimization_distance_none,
  // FIXME: ensure that we test all of [”elow] conbinations in our perofrmance benchmar/evalaution/test/comparison
  e_typeOf_optimization_distance_none_xmt_functionInlined, //! which is spected to go faster than "e_typeOf_optimization_distance_none"
  e_typeOf_optimization_distance_none_xmtOptimizeTransposed, //! which is spected to go faster than "e_typeOf_optimization_distance_none"
  e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal, //! which permutates "e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal" iot. validate ssumptions of 'optimlity' in the latter.
  // FIXME: in [below] "e_typeOf_optimization_distance_asFastAsPossible" add a 'macro' as in Jan-Christans updated code.
  e_typeOf_optimization_distance_asFastAsPossible,  //! ie, the 'best'/preferred application/strategy.
  // FIXME: amke use of [below] 'using' Jan-Christans code-update ... and then update our benchmark ... wrt. the isOf_interest(..) VS isOf_interest_func(..)
  e_typeOf_optimization_distance_asFastAsPossible_subOptimal_wrt_instructionCache_useFunctionInsteadOfMacro,  
  e_typeOf_optimization_distance_asFastAsPossible_subOptimal_wrt_instructionCache_seperateMaskAlternatives,  
  e_typeOf_optimization_distance_asFastAsPossible_subOptimal_targetSpecific_code_instructionRewriting,  
  e_typeOf_optimization_distance_undef
} e_typeOf_optimization_distance_t;



#endif //! EOF
