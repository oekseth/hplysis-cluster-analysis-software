
  //!
  //! Handle pre-processing cases (ie, on input before applying sim-metric):
  s_kt_set_2dsparse_t *cpy_obj_1 = obj_1; s_kt_set_2dsparse_t *cpy_obj_2 = obj_2;
  s_kt_set_2dsparse_t localData__1 = setToEmptyAndReturn__s_kt_set_2dsparse_t();
  s_kt_set_2dsparse_t localData__2 = setToEmptyAndReturn__s_kt_set_2dsparse_t();
  if( (self->sim_metric.typeOf_correlationPreStep != e_kt_categoryOf_correaltionPreStep_none) || (obj_1->matrixOf_scores == NULL) ) {
    //!
    //! Idneitfy and hancle cases where we need to 'fetch' the wegiht
    //! 
    //! Allocate:
    allocate__s_kt_set_2dsparse_t(&localData__1, obj_1->_nrows, 100, NULL, NULL, /*isTo_useImplictMask=*/false, /*isTo_allocateFor_scores=*/true); //(obj_1->matrixOf_scores));
    cpy_obj_1 = &localData__1; cpy_obj_2 = &localData__1;
    if(obj_1 != obj_2) {
      allocate__s_kt_set_2dsparse_t(&localData__2, obj_2->_nrows, 100, NULL, NULL, /*isTo_useImplictMask=*/false, /*isTo_allocateFor_scores=*/true); //(obj_1->matrixOf_scores));
      cpy_obj_2 = &localData__2;
    }
    //! 
    //! 
    const bool isTo_sortKeys = self->config.forMetricPreStep__sortUsingKeysAndNotVal;
    {
#define __MiV__2d_from obj_1
#define __MiV__2d_to localData__1
      //!
      //! Apply logics:
#include "kt_sparse_sim__stub__metricPreProcess.c"      
      if(obj_1 != obj_2) { //! then 'seprately' add for the 'second data-input':
#define __MiV__2d_from obj_2
#define __MiV__2d_to localData__2
      //!
      //! Apply logics:
#include "kt_sparse_sim__stub__metricPreProcess.c"
      }
    }
  } else {
    assert(obj_2->matrixOf_scores != NULL); //! ie, as we exect a consistency wrt. the data-cases.
  }

//!
//!
//! Apply sorting: is 'needed' to 'allow' a parallel evalaution/applicaiton:
sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(cpy_obj_1, /*isTo_sortKeys=*/true);
if(cpy_obj_1 != cpy_obj_2) {
  sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(cpy_obj_2, /*isTo_sortKeys=*/true);
 }
const bool config__listIsAlreadySorted = true;
//!
//!
//! 

  //! ---------------------
  const uint nrows_1 = obj_1->_nrows;   const uint nrows_2 = obj_2->_nrows;
  //!
  //! Invesetigate if parallsiaiton may 'effectily' be applied:
  const uint __maxCntRows = macro_max(nrows_1, nrows_2);
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
  const uint cnt_threads = omp_get_max_threads(); //OMP_NUM_THREADS; 
#else
const uint cnt_threads = 1;
#endif
  uint cnt_blocks_1 = 0; uint cnt_blocks_2 = 0;
  cnt_blocks_1 = cnt_blocks_2 = (cnt_threads * self->config.advanced__parallelConfig__maxCntBlocks__eachThread);
  //! First row:
  uint cntRows__each_1 = nrows_1 / cnt_blocks_1; uint cntRows__each_2 = nrows_2 / cnt_blocks_2;
  const uint __minRowSize = self->config.advanced__parallelConfig__maxCntBlocks__minRowCount;;
  //! Apply threshold:
  if(cntRows__each_1 < __minRowSize) {cntRows__each_1 = nrows_1; cnt_blocks_1 = 1;}
  if(cntRows__each_2 < __minRowSize) {cntRows__each_2 = nrows_2; cnt_blocks_2 = 1;}
  //! ---------------------
  //!
  //! Compute:
  if(self->config.isTo__useParallelExeuction && 
     ( (cnt_blocks_1 >= 4)  || (cnt_blocks_2 >= 4) ) //! where '4' is a wiled-guess wrt. effcient applicaiotn of parllsiaiotn.
     //(__maxCntRows > self->config.advanced__parallelConfig__maxCntBlocks__minRowCount) 
     ) {

    /* uint cnt_bl =  */
    /* if(cntRows__each_1 < cnt_blocks_1) {cntRows__each_1 = nrows_1;} // TODO: validet this asusmption. */
    /* //! Second row: */
    /* uint cntRows__each_2 = nrows_2 / self->config.advanced__parallelConfig__maxCntBlocks__eachThread; */
    /* if(cntRows__each_2 < self->config.advanced__parallelConfig__maxCntBlocks__minRowCount) {cntRows__each_2 = nrows_2;} // TODO: validet this asusmption. */
    //! Allocate a tmeproary space:
    const bool __useSymmetricOptimization = (cpy_obj_1 == cpy_obj_2);
    if(__useSymmetricOptimization) {assert(nrows_1 == nrows_2);} //! ie, as our [below] "uint size_2 = (__useSymmetricOptimization) ? block_id_1 : cnt_blocks_2;" wouyld otherwise be wrong.
    uint cnt_blocks = (cnt_blocks_1 * cnt_blocks_2);
    if(__useSymmetricOptimization) {cnt_blocks = (uint)((t_float)cnt_blocks * 0.5);}
    assert(cnt_blocks >= 2); //! ie, as we otehrwise have a aprallsaition with a large voerhead, ie, uncneccsary performance-time-cost-overhead.
    //!
    //! Allocate space:
    s_kt_list_1d_pair_t obj_alloc = init__s_kt_list_1d_pair_t(cnt_blocks);
    //!
    //! Specify the blocks:
    uint block__currPos = 0;
    uint row_pos_1 = 0;
    for(uint block_id_1 = 0; block_id_1 < cnt_blocks_1; block_id_1++) {
      assert(row_pos_1 < nrows_1); //! ie, a s[below] call is otehrwise pointless.
      uint row_pos_2 = 0;
      uint size_2 = (__useSymmetricOptimization) ? block_id_1 : cnt_blocks_2;
      //assert(size_2 >= 1);      
      for(uint block_id_2 = 0; block_id_2 < size_2; block_id_2++) {
	assert(row_pos_2 < nrows_2); //cntRows__each_2); //! ie, a s[below] call is otehrwise pointless.
	assert(block__currPos < cnt_blocks);
	//!
	//! Sepcfiy the block-size:
	const uint row_start = row_pos_1;
	const uint col_start = row_pos_2;
	//! Update:
	obj_alloc.list[block__currPos] = MF__initVal__s_ktType_pair(row_start, col_start);
	assert(obj_alloc.list[block__currPos].head == row_start);
	assert(obj_alloc.list[block__currPos].tail == col_start);
	//! Increment:
	row_pos_2 += cntRows__each_2;
	block__currPos++;
      }
      //! Note: [”elow] is 'commented out' as we asusme our 'arpalizaiton-strategy' to correclty set the "((block_id_2 +1) == size_2) { lastPos = nrows_2;}" (oesketh, 06. mar. 2017)
      //if(__useSymmetricOptimization == false) {assert((row_pos_2 + cntRows__each_2) >= nrows_2);} //! ie, to vlaidate that we have  'covered all' of blocks 'of interest'.
      row_pos_1 += cntRows__each_1;
    }
    // if(__useSymmetricOptimization == false) {assert((row_pos_1 + cntRows__each_1) >= nrows_1);} //! ie, to vlaidate that we have  'covered all' of blocks 'of interest'.


    //!
    //! Apply parallisaiotn:
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#pragma omp parallel for schedule(static) //schedule(dynamic,3) //! where "3" is based on observaitons
#endif
    // #pragma omp parallel //! which we have observe for soem cases results in a signifcnat slow-down.
    for(uint block_id = 0; block_id < cnt_blocks; block_id++) {
      const uint startPos__obj_1 = obj_alloc.list[block_id].head;
      assert(startPos__obj_1 != UINT_MAX);
      assert(startPos__obj_1 < nrows_1);
      uint endPos__obj_1 = startPos__obj_1 + cntRows__each_1;
      assert(nrows_1 <= obj_1->_nrows);
      endPos__obj_1 = macro_min(endPos__obj_1, nrows_1);
      assert(endPos__obj_1 <= obj_1->_nrows);
      //const uint endPos__obj_1 = obj_alloc[block_id].tail;
      //! --- 
      const uint startPos__obj_2 = obj_alloc.list[block_id].tail;
      uint endPos__obj_2 = startPos__obj_2 + cntRows__each_2;
      assert(nrows_2 <= obj_2->_nrows);
      endPos__obj_2 = macro_min(endPos__obj_2, nrows_2);
      assert(startPos__obj_2 != UINT_MAX);
      assert(startPos__obj_2 < nrows_2);
      assert(endPos__obj_2 <= obj_2->_nrows);
      //!
      //! Construct a 'sahllwo copy, ie, to 'set' a different "_nrows" size.
      s_kt_set_2dsparse_t shallow_1 = *cpy_obj_1;  shallow_1._nrows = endPos__obj_1;
      s_kt_set_2dsparse_t shallow_2 = *cpy_obj_2;  shallow_2._nrows = endPos__obj_2;
      //!
      //! Apply logics:
      self->func_sim(&(self->config), &shallow_1, &shallow_2, __MiV__result_mat, __MiV__result_sparse, weight, startPos__obj_1, startPos__obj_2, config__listIsAlreadySorted);      
    }
    //!
    //! De-allcoate:
    free__s_kt_list_1d_pair_t(&obj_alloc);
  } else {
    uint startPos__obj_1 = 0; uint startPos__obj_2 = 0;
    self->func_sim(&(self->config), cpy_obj_1, cpy_obj_2, __MiV__result_mat, __MiV__result_sparse, weight, startPos__obj_1, startPos__obj_2, config__listIsAlreadySorted);
    //self->func_sim(&(self->config), obj_1, obj_2, &obj_result, NULL, weight);
  }

  //!
  //! De-allcoate:
  free_s_kt_set_2dsparse_t(&localData__1);
  free_s_kt_set_2dsparse_t(&localData__2);

  //!
  //! Clear tempalte-varialbes:
#undef __MiV__result_mat
#undef  __MiV__result_sparse
