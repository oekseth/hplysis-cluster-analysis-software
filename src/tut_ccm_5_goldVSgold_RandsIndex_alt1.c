#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_ccm.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.

/**
   @brief demonstrates how a 'matrix' may be comapred to a gold-case.
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks general: use logics in our "hp_ccm" to comptue the CCM, while our "hp_clustershapes.h" provide lgoics to build a sysntitc data-set (eg, to simplfiy a strucutred appraoch for evlauaiton).
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#endif
{
  const uint nrows = 100;  const uint cnt_clusters = 5;
  e_kt_matrix_cmpCluster_metric_t ccm_enum = e_kt_matrix_cmpCluster_metric_randsIndex;
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  s_hp_clusterShapes_t obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/10, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
  s_hp_clusterShapes_t obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
  bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
  assert(is_ok);

  //!
  //! Apply logics:
  uint *map_clusterMembers = obj_shape.map_clusterMembers;
  uint *map_clusterMembers_2 = obj_shape_2.map_clusterMembers;
  t_float scalar_result = T_FLOAT_MAX;
  is_ok = ccm__twoHhypoThesis__hp_ccm(ccm_enum, map_clusterMembers, map_clusterMembers_2, nrows, obj_shape.matrix.matrix, obj_shape_2.matrix.matrix, &scalar_result); //! ie, call our "hp_ccm.h".
  assert(is_ok);
  //!
  //! Write out the result:
  fprintf(stdout, "# Result: sim-score=\"%f\", at [%s]:%s:%d\n", scalar_result, __FUNCTION__, __FILE__, __LINE__);
  //!
  //! De-allocate:
  free__s_hp_clusterShapes_t(&obj_shape);
  free__s_hp_clusterShapes_t(&obj_shape_2);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
