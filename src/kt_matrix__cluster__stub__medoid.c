assert(isTo_transposeMatrix == false); // ie, to simplify our lgoics.

const uint size_inner = local_matrix->nrows;
assert(size_inner > 0);
  //! Intaite the result-opbject:
uint nclusters_adjusted = (nclusters/max_cntForests); // + 1;
// FIXME: 'figure out' an appraoch 'to handle this case proeprly'.
if(nclusters_adjusted <= 1) {nclusters_adjusted = 2;}
if(nclusters_adjusted == 1) {
  fprintf(stderr, "!!\t How are we to correctly handle this case ... ie, 'shall' we then instead use djsijoint-forest-clustering? OBserviaont at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  assert(false); // TODO: thereafter update our "kt_matrix__cluster__stub__avgOrRank.c"
  assert(false); // TODO: handle this case.
 }

//printf("##\t nclusters=%u, nclusters_adjusted=%u, max_cntForests=%u, at %s:%d\n", nclusters, nclusters_adjusted, max_cntForests, __FILE__, __LINE__);
s_kt_clusterAlg_fixed_resultObject_t obj_local_result; init__s_kt_clusterAlg_fixed_resultObject_t(&obj_local_result, nclusters_adjusted, size_inner);


// printf("##\t at %s:%d\n", __FILE__, __LINE__);
//!
//! Handle cases where a user has defined/idneitifed a 'sepcific' set of centrlaity-measures:
s_kt_randomGenerator_vector_t *obj_rand__init__local = NULL;
s_kt_randomGenerator_vector_t obj_rand__init__local__ = initAndReturn__defaultSettings__s_kt_randomGenerator_vector_t();
s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(local_matrix->nrows, local_matrix->ncols, local_matrix->matrix);
if(obj_rand__init != NULL) {
  //assert(mapOf_clusterVertices__toClusterIds__local != NULL);
  //! Construct a subset of the vertices using the [abov€] mapping-table (oekseth, 06. feb. 2017):
  obj_rand__init__local__ = initAndReturn__fromSuper__s_kt_randomGenerator_vector_t(mapOf_clusterVertices__toClusterIds__local, /*superset=*/obj_rand__init, &obj_matrix_input, nclusters_adjusted); //local_matrix);

  obj_rand__init__local = &obj_rand__init__local__;
 }

//!
//! The Call:
assert(obj_local_result.cluster_vertexCentroid); //! ie, as we expect this to be allocated.
kmedoids__extensiveInputParams(nclusters_adjusted, local_matrix->nrows, local_matrix->matrix, npass, 
			       /*clusterid=*/obj_local_result.vertex_clusterId,
			       /*centroid-cluster-to-vertex=*/obj_local_result.cluster_vertexCentroid, 
			       /*error=*/&(obj_local_result.sumOf_errors), 
			       /*ifound=*/&(obj_local_result.cntTotal_clusterAnswerFound),
			       obj_local_result.matrix2d_vertexTo_clusterId,
			       obj_rand__init__local
			       ); 


if(obj_rand__init__local) {
  free__s_kt_randomGenerator_vector_t(&obj_rand__init__local__);
 }

//!
//! Updat ehte result:
  //!
  //! Iterate through the vertices and udpate the membership:
#ifndef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
if(arrOf_result_vertexMembers_size > 0) {
  //printf("arrOf_result_vertexMembers=%p, at %s:%d\n", arrOf_result_vertexMembers, __FILE__, __LINE__);
  mergeResult__s_kt_clusterAlg_fixed_resultObject_t(obj_result, &obj_local_result, arrOf_result_vertexMembers);
 }
#else //! then we 'make' a direct-copy of the object:
mergeResult__s_kt_clusterAlg_fixed_resultObject_t(obj_result, &obj_local_result, /*arrOf_result_vertexMembers=*/NULL);
#endif
//!
//! De-allcoate:
#ifndef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
if(arrOf_result_vertexMembers) {
  free_1d_list_uint(&arrOf_result_vertexMembers); arrOf_result_vertexMembers = NULL;
 }	
#endif
free__s_kt_clusterAlg_fixed_resultObject_t(&obj_local_result);
