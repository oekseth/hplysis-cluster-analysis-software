#include "kt_distance_slow.h"


//! --------------------------------------------------------------------------------------------------------------
//! -------------------------------------------------------------------------------------------------------------
//! --------------------------------------------------------------------------------------------------------------
//! --------------------------------------------------------------------------------------------------------------

//! @return true if we asusem teh t_float-value is of interest.
/* static t_float isOf_interest_func(const t_float value) { */
/*   return ( (value != T_FLOAT_MAX) && (value != T_FLOAT_MIN_ABS) ); */
/* } */
#ifndef isOf_interest
#define isOf_interest(value) ( (value != T_FLOAT_MAX) && (value != T_FLOAT_MIN_ABS) )
#endif


//! Adjust the score by the sum of weights for the 'variable' step:
static void __adjustMatrix_byWeight_naive(const uint sizeOf_i, const t_float weight[], t_float **resultMatrix, const uint nrows, const uint ncols) {
  assert(sizeOf_i > 0);
  assert(weight);
  assert(resultMatrix);
  t_float sumOf_weights = 0;
  for(uint i = 0; i < sizeOf_i; i++) {
    const t_float weight_local = (weight) ? weight[i] : 1;
    sumOf_weights += weight_local;	
  }
  // TODO: validate correctness of [below] calsue.
  if(sumOf_weights != 0) {
    //! Update 
    for(uint index1 = 0; index1 < nrows; index1++) {
      for(uint index2 = 0; index2 < ncols; index2++) {
	// FIXME: instead of [”elow] use _mm_storeu_ps .... ite, to dealy the "resultMatrix[index1][index2]" update
	t_float result = resultMatrix[index1][index2];
	if(result != 0) {result /= sumOf_weights;} //! else the alternative case may be explained by an empty cluster.
	resultMatrix[index1][index2] = result;
      }
    }
  }
}

static void __adjustMatrix_byWeight_optimized(const uint sizeOf_i, const t_float weight[], t_float **resultMatrix, const uint nrows, const uint ncols) {
  assert(sizeOf_i > 0);
  //assert(weight);
  assert(resultMatrix);
  const uint SM = 4; //! ie, due to the 16 bytes in "_mm_storeu_ps(..)" and the sizeof(t_float)==4, ie, 4 'chunks'.
  if(sizeOf_i > SM) {
    t_float sumOf_weights = sizeOf_i;
    if(weight) {
      uint k = 0; uint numberOf_chunks = 0; uint chunkSize_row_last = sizeOf_i;
      if(sizeOf_i >= SM) {
	// FIXME: in [”elow] why do we need "length-1"?
	__get_updatedNumberOF_lastSizeChunk_entry(/*nrows=*/(uint)sizeOf_i, SM, &numberOf_chunks, &chunkSize_row_last);
	assert(numberOf_chunks != UINT_MAX); assert(numberOf_chunks != 0);
	const uint k_group_size = (chunkSize_row_last == SM) ? numberOf_chunks : numberOf_chunks - 1;	          
	assert(k_group_size > 0); //! ie, what we expect.
	// printf("k=%u, k_group_size=%u, end_pos=%u, length=%d, at %s:%d\n", k, k_group_size, end_pos, length);
	t_float result[4] = {0, 0, 0, 0}; 
	for(uint k_group = 0; k_group < k_group_size; k += 4, k_group++) {
	  //! then we move the four elements 'forward' with step=1.
	  __m128 term1  = _mm_load1_ps(&weight[k]);		
	  //__m128 arr_result_tmp = _mm_loadu_ps(&list[k+1]); //! ie, where "+1" is to move aa bload of foru t_floats forward.
	  //! Load the result-vector
			// FIXME: instead of [”elow] try to use _mm_load_ps ... and then udpate our test-restuls
	  __m128 arr_result_tmp = _mm_loadu_ps(&result[0]);
	  //! Add the values:
	  arr_result_tmp = _mm_add_ps(term1, arr_result_tmp);
	  // fixme: valdiate orrectenss of [below].
	  //! Store the result:
	  _mm_store_ps(result, arr_result_tmp);
	  //_mm_storeu_ps(&list[k], arr_result_tmp);
	}
	//! Merge the values:
	for(uint i = 0; i < 4; i++) {sumOf_weights += result[i];}
      }
      for(; k < sizeOf_i; k++) {
	const t_float weight_local = (weight) ? weight[k] : 1;
	sumOf_weights += weight_local;	
      }     
    }
    if(sumOf_weights != 0) { //! Then update the result:
      if(ncols >= SM) {

	// Set to zero when the original length is zero.
	__m128 zero = _mm_setzero_ps();
	// Compute the maximum absolute value component.
	// __m128 maxComponent = MaximumAbsoluteComponent(v);
	// __m128 empty_values = _mm_cmpneq_ps(zero, maxComponent);


	uint numberOf_chunks = 0; uint chunkSize_row_last = ncols;
	__get_updatedNumberOF_lastSizeChunk_entry(/*nrows=*/(uint)ncols, SM, &numberOf_chunks, &chunkSize_row_last);
	const uint k_group_size = (chunkSize_row_last == SM) ? numberOf_chunks : numberOf_chunks - 1;	          
	//! Start the iteraiton:
	t_float arr_length[4] = {sumOf_weights, sumOf_weights, sumOf_weights, sumOf_weights};
	for(uint index1 = 0; index1 < nrows; index1++) {
	  uint index2 = 0; 


	  //normalized = _mm_and_ps(mask, normalized);
	  for(uint k_group = 0; k_group < k_group_size; index2 += 4, k_group++) {
	    //! Load the data:
	    const __m128 vec_data1 = _mm_load1_ps(&resultMatrix[index1][index2]);		
	      // FIXME: ask jan-crhistain to update this code-chunk ... and then update the comments in our tetsts.
	      const __m128 vec_empty_1 = _mm_set1_ps(T_FLOAT_MAX);
	      //! Idenitfyt he cases where the values is set to T_FLOAT_MAX:
	      __m128 vec_cmp = _mm_cmpneq_ps(vec_empty_1, vec_data1);              // identify the cases where the vlaeus is not T_FLOAT_MAX    
	      // FIXME: figure out why [”elow] 'caseus' a reverse-order of the array ... and the drop the use of the 'hsuffle' calls.
	      __m128 vec_masked1 = _mm_and_ps(vec_cmp, vec_data1); //! where T_FLOAT_MAX => '0'
	      __m128 vec_masked1_shuffled = _mm_shuffle_ps(vec_masked1, vec_masked1, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
	      //! Intialise the vector to be used dividing the values:
	      const __m128 vec_div = _mm_set1_ps(sumOf_weights);
	      //! Comptue the result:
	      __m128 vec_result = _mm_div_ps(vec_masked1_shuffled, vec_div); //! where we sue a 'reverse-ordered' access.
	      // // FIXME: is [below] correct?
	      // __m128 vec_result_shuffled = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); //! ie, revert the order.
	      
	      //! FIXME: Manage to 'revert back' T_FLOAT_MAX values. <-- until then we assuem that '0' is an 'accpetable' valeu to use.
	      
	      //! Store the result:
	      _mm_storeu_ps(&resultMatrix[index1][index2], vec_result);
	    //_mm_storeu_ps(&resultMatrix[index1][index2], vec_result_shuffled);
	  }
	  //! Then complete the oepraiton, ie, for those valeus which is not the mulitpler of '4':
	  for(; index2 < ncols; index2++) {
	    t_float result = resultMatrix[index1][index2];
	    if(result != 0) {result /= sumOf_weights;} //! else the alternative case may be explained by an empty cluster.
	    resultMatrix[index1][index2] = result;
	  }
	}
      } else {
	for(uint index1 = 0; index1 < nrows; index1++) {
	  for(uint index2 = 0; index2 < ncols; index2++) {
	    // FIXME: instead of [”elow] use _mm_storeu_ps .... ite, to dealy the "resultMatrix[index1][index2]" update
	    t_float result = resultMatrix[index1][index2];
	    if(result != 0) {result /= sumOf_weights;} //! else the alternative case may be explained by an empty cluster.
	    resultMatrix[index1][index2] = result;
	  }
	}
      }
    }
  } else { //! Then we use the 'fallback' prcoedure:
    __adjustMatrix_byWeight_optimized(sizeOf_i, weight, resultMatrix, nrows, ncols);
  }
}

//! Adjust the score by the sum of weights for the 'variable' step:
static void __adjustMatrix_byWeight(const uint sizeOf_i, const t_float weight[], t_float **resultMatrix, const uint nrows, const uint ncols) {
  if(true) {__adjustMatrix_byWeight_optimized(sizeOf_i, weight, resultMatrix, nrows, ncols);}
  else {__adjustMatrix_byWeight_naive(sizeOf_i, weight, resultMatrix, nrows, ncols);}
}



//! Compute the weighted euclidian distance for a given amtrix, storing the result in the resultMatrix matrix.
//! @remarks
//!      # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
//!      # "CLS" describes the level1-memory-cache, which may either be laoded 'in compile-time', or hard-coded: "gcc -DCLS=$(getconf LEVEL1_DCACHE_LINESIZE)", or "getconf LEVEL1_DCACHE_LINESIZE" from the terminal-line.
void compute_allAgainstAll_distanceMetric_euclid_or_cityBlock(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const bool isTo_use_cityBlock, bool transpose, const enum e_typeOf_optimization_distance typeOf_optimization,  const uint CLS, const bool isTo_use_continousSTripsOf_memory, const uint iterationIndex_2, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const e_cmp_masksAre_used_t  masksAre_used ) { 
  // FIXME: update our distance-matrix-code to 'handle' [below] cae, ie, pritn a warning and then use a non-cahce-effective strategy for comptuation.
  //assert(CLS <= nrows);    assert(CLS <= ncols);
  assert( (CLS % (uint)sizeof(t_float)) == 0); //! ie, as the 'chunks' need to be divided into 'feasible parts' for the memory-t_float-cahce-loading.
  assert(nrows > 0);
  assert(ncols > 0);
  assert(data1);
  assert(data2);
  assert(resultMatrix);
  
  //! Note: in [below] code one (of many) compelxitites consderns the need/improtance to 'remmeber' that the "euclidiaon" and "city-block" distance-meausrements 'operate' on eitehr "nrow*nrows" or "ncols*ncols" index-space, ie, given our interest in comparaing 'parallel' columns of 'parallel' rows. The 'complexity' raises due tot eh 'transpsoe-parameter', ie, a copy-paste of the for-loops would result in errors.
  // FIXME: validate that we have reosvled [abov€] issue for all of [”elow] cases/ciationts <-- [above] erro was not captrued by Jan-Christain, ie, assumes 'it is not an obvious one'.

  { //! Validate taht the result-matrix is set to a count of "0"
    if(transpose == 0) {
      for(uint index1 = 0; index1 < nrows; index1++) {
	for(uint index2 = 0; index2 < nrows; index2++) {
	  assert(resultMatrix[index1][index2] == 0);
	}
      }
    } else {
      for(uint index1 = 0; index1 < ncols; index1++) {
	for(uint index2 = 0; index2 < ncols; index2++) {
	  assert(resultMatrix[index1][index2] == 0);
	}
      }
    }
  }
  // FIXME: call this function ... and write time-benchmarks for this funciton
    // FIXME: wrtie a benchmark-test to test the effect of using "mask" ... eg, wrt. memory-overhead in our optimized functions/approach.
  // FIXME: ensure that the for-loops are easily seperatble ... ie, that parallisaiton is easy/trivial. <-- remember to update our documentaiton wrt. the "OMP_NUM_THREADS" macro-option, ie, wrt. "export OMP_NUM_THREADS=1" from the terminal/bash/shell. 
  // FIXME: wrt. parallisaiton ... consider to 'temproaly transpose a matrix' if the 'first for-loop' has fewer than 4 vertices <-- will such a case happend? 
  // FIXME: wrt. parallisaiton ... 


  //! Indefy the 'boundaries' of our operation:
  const char *__restrict__ rmask1 = NULL; const char *__restrict__ rmask2 = NULL; t_float *__restrict__ rres;  const t_float *__restrict__ rmul1; const t_float *__restrict__  rmul2; const uint SM = (uint)(CLS / (uint)sizeof (t_float)); // const t_float *__restrict__ rweight = weight;
  assert(nrows > 0); assert(ncols > 0); assert(SM > 0);
  uint chunkSize_row_last = SM; uint chunkSize_col_last = SM; __get_updatedNumberOF_lastSizeChunk(nrows, ncols, SM, &chunkSize_row_last, &chunkSize_col_last);
  uint cnt_iterations = 0; const uint numberOf_chunks_rows = nrows / SM; const uint numberOf_chunks_cols = ncols / SM;


  if( (typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible) || (typeOf_optimization == e_typeOf_optimization_distance_undef) ) {
    assert(isTo_use_continousSTripsOf_memory == true); //! ie, as we have simplfied our code wrt. this asusmption
    //! Note: we have observed that this approach (when compared to the non-optimilized approaches) increases code-efficencty by 1.4x for an "[800][800]" matrix compared to an 'oterhwise ideal' approach and "2.6" when compared to a 'non-ideal matrix which is investigated wrt. transpostions'  ... and where the 'efficency' incrases approx linearly wrt. incrased matrix-size. To examplify the latter, while we observe a 'lienar incrase' between the latter two cases, we for the 'slow' case observe a difference of approx. 8.9x when compared to a summation over an 'transposed' matrix, ie, we asset that our approxh (wrt. matrix-accesses) results in a performance-speed-up of more than 20x, a difference which is a consdierably understatement when we compare large non-optmilized matrix-operaitons VS optimized matrix-operations.
    //! Note(2): Correctness of this impelmetnation si validated through the slwoer implementaitons (ie, the otehr enum-alterantvies).
    // FIXME: update [above] based on [”elow] comment/observation.


    struct s_allAgainstAll_config config_allAgainstAll; init__s_allAgainstAll_config(&config_allAgainstAll, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, /*typeOf_postProcessing_afterCorrelation=*/typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, masksAre_used, /*isTo_use_SIMD=*/true, /*typeOf_optimization = */e_typeOf_optimization_distance_asFastAsPossible);

    //! Then comptue the optmial distance-matrix:
    if(isTo_use_cityBlock) {
      // typeOf_postProcessing_afterCorrelation, s_inlinePostProcess,
      ktCorr_matrixBase__optimal_compute_allAgainstAll(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*type=*/e_kt_correlationFunction_groupOf_minkowski_cityblock, transpose, /*typeOf_metric_correlation=*/e_typeOf_metric_correlation_pearson_undef, &config_allAgainstAll); //e_typeOf_metric_correlation_pearson_undef, masksAre_used);
      //ktCorr_matrixBase__optimal_compute_allAgainstAll(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*type=*/e_kt_correlationFunction_cityblock, transpose, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2,  /*isTo_use_SIMD=*/true, typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, e_typeOf_metric_correlation_pearson_undef, masksAre_used);
    } else {
      ktCorr_matrixBase__optimal_compute_allAgainstAll(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*type=*/e_kt_correlationFunction_groupOf_minkowski_euclid, transpose, /*typeOf_metric_correlation=*/e_typeOf_metric_correlation_pearson_undef, &config_allAgainstAll); //e_typeOf_metric_correlation_pearson_undef, masksAre_used);
      //ktCorr_matrixBase__optimal_compute_allAgainstAll(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*type=*/e_kt_correlationFunction_euclid, transpose, typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, &config_allAgainstAll); //e_typeOf_metric_correlation_pearson_undef, masksAre_used);
      //ktCorr_matrixBase__optimal_compute_allAgainstAll(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*type=*/e_kt_correlationFunction_euclid, transpose, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2,  /*isTo_use_SIMD=*/true, typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, e_typeOf_metric_correlation_pearson_undef, masksAre_used);
    }

    //! Finalize:
    if(transpose == 0) {
      //if(false) {printf("cnt_iterations=%u, total_cnt_iterations=%u, at %s:%d\n", cnt_iterations, total_cnt_iterations, __FILE__, __LINE__);}
      //! Adjsut the matrix:
      if(weight) {__adjustMatrix_byWeight(/*n=*/numberOf_chunks_cols, weight, resultMatrix, nrows, ncols);} //! else we assume the case is not of interest.
    } else { //! Then teh ransposed is computed:
      //! Note: the idea (in the comptuation of the distance in a transposed matrix) is to 'move' "i" and "index1" vertical and "index1"  along the vertical axis. The implicaiton is that 'the sum of columns' is paritally updated for each row: instead of seperately coputing "[i][index1] x [i][index2]" for each pair of ("index1", "index2"), we for each "i" update ("index1", "index2").
      //! Note(2): in this approxh we order the for-loops based on comparison of results from "e_typeOf_optimization_distance_none_xmtOptimizeTransposed" and "e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal"
      //! Adjsut the matrix:
      // FIXME: validate [below] param
      if(weight) {__adjustMatrix_byWeight(/*n=*/numberOf_chunks_rows, weight, resultMatrix, nrows, ncols);} //! else we assume the case is not of interest.
    }
    
    //! Then we update the result, ie, as we only computed [above] for 'half of the entries':
    // FIXME: update [”elow] call when we know the 'best' perofrmacne-result-strategy.
    if(data1 == data2) {
      const uint size_ncols_nrows = (transpose == 0) ? /*size=*/nrows : /*size=*/ncols;
      const uint size_ncols_nrows_2 = (iterationIndex_2 != UINT_MAX) ? iterationIndex_2 : size_ncols_nrows;
      update_cells_above_firstFoorLoop( size_ncols_nrows, size_ncols_nrows_2, resultMatrix, transpose, /*isTo_use_speedTest_updateOrder_in_out=*/true, /*isTo_use_speedTest_inrisnistics=*/false);
    }
  } else if(typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible_subOptimal_targetSpecific_code_instructionRewriting) {
    //! Note: in [belwo] we: (a) call 'in jointship' "if(isOf_interest(rmul1[j2]) && isOf_interest(rmul2[j2]))" istead of seperateing both iwawe the variables, ie, a possible slow-down; (b) use a funciton-call "isOf_interest_func(..)" instead of the mroe effective "isOf_interest(..)" macro-call, where we have observed tha tthe "C++" "inline" keyword does not yeald the expected perofrmance-optimization, ie, a macro-funciton increase the performance
  assert(isTo_use_continousSTripsOf_memory == true); //! ie, as we have simplfied our code wrt. this asusmption
    if(transpose == 0) {
      bool debug_lastRow_seen = false;  
      const uint total_cnt_iterations = nrows * nrows * ncols;
      const t_float *__restrict__ pointer_dataLastPos1 = (isTo_use_continousSTripsOf_memory) ? data1[0] + total_cnt_iterations : NULL; //! which is used to validate that we do not overwrite memory.
      const t_float *__restrict__ pointer_resultLastPos1 = (isTo_use_continousSTripsOf_memory) ? resultMatrix[0] + total_cnt_iterations : NULL; //! which is used to validate that we do not overwri
      //! Iterate:
      for(uint cnt_index1 = 0, index1 = 0; cnt_index1 < numberOf_chunks_rows; cnt_index1++, index1 += SM) {
	const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_rows) ? SM : chunkSize_row_last;
	if(chunkSize_index1 != SM) {
	  if(false) {printf("(info)\t last-row for i=%u, cnt_i=%u, and numberOf_chunks_rows=%u, at %s:%d\n", index1, cnt_index1, numberOf_chunks_rows, __FILE__, __LINE__);}
	  assert(debug_lastRow_seen == false); debug_lastRow_seen = true;
	}
	for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_rows; cnt_index2++, index2 += SM) {
	  const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_rows) ? SM : chunkSize_row_last;
	  uint i = 0;
	  for(uint cnt_i = 0; cnt_i < numberOf_chunks_cols; cnt_i++, i += SM)  {
	    const uint chunkSize_cnt_i = (cnt_i < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	    if(true) { //! Validate correctness:
	      const uint sumOf_internalChunk = chunkSize_index1 * chunkSize_index2 * chunkSize_cnt_i;
	      assert(total_cnt_iterations >= (cnt_iterations + sumOf_internalChunk));
	    }
	    //! --------------------------------------------------------------------------------------
	    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
	    uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; uint debug_cntIterations_inner = 0;
	    //! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
	    for (i2 = 0, //! ie, for "index1" 
		   rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		   // FIXME: validate [below]
		   rmul1 = &data1[index1][i], rmask1 = (!mask1) ? NULL : &mask1[index1][i];
		 i2 < chunkSize_index1;
		 i2++, 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"
		   rres += ncols, 
		   rmask1 += (!rmask1) ? 0 : ncols, rmul1 += ncols
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {

	      //! Prefetch the data:
	      // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      if(mask1) {_mm_prefetch (&mask1[8], _MM_HINT_NTA);}
	      
	      uint k2 = 0; 
	      /**
	       //! Note: even though we describe ourself as seasoned programmers, we find this/our approach rather confusing. Therefore, we in below depict how we access the cells in memory:
	       ----------------------------------------------------------------------------------------
	       row[index1 + i2*ncols] ... column[index2] | | | | | | column[index2 + chunkSize_index2] // describes the above iteration, where the "i2" for-loop identities the row and "k2" the column
	       row[index1 + i2*ncols] ... column[i] | | | | | | column[i + chunkSize_cnt_i] //! where "j2" is used as a 'column-counter'
	       row[index2 + k2*ncols] ... column[i] | | | | | | column[i + chunkSize_cnt_i] //! where "j2" is used as a 'column-counter'
	       ----------------------------------------------------------------------------------------
	      **/
	      assert(index2 < nrows); //! ie, given [below]
	      for (k2 = 0, //! ie, for "index2" 
		     // FIXME: validate [below]
		     rmul2 = &data2[index2][i], 
		     rmask2 = (!mask2) ? NULL : &mask1[index2][i];
		   k2 < chunkSize_index2; k2++, // , rmul2 += chunkSize_cnt_i
		     //! Move to the same psotion in the next row:
		     // FIXME: validte [below]
		     rmask2 += (!rmask2) ? 0 : ncols, rmul2 += ncols
		   ) {		
		
		//! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		// _mm_prefetch (&data2[8], _MM_HINT_NTA);

		//! The operation:
		for (uint j2 = 0; j2 < chunkSize_cnt_i; ++j2) {
		  if( ( (!rmask1 || rmask1[j2]) && (!rmask2 || rmask2[j2]) ) ) {	
		    if(pointer_dataLastPos1) {assert((rmul1 + j2) < pointer_dataLastPos1);} //! ie, validate that we are isndiet he range
		    if(pointer_resultLastPos1) {assert((rres + k2) < pointer_resultLastPos1);} //! ie, validate that we are isndiet he range
		    if(isOf_interest(rmul1[j2]) && isOf_interest(rmul2[j2])) {
		      // FIXME: validate:
		      // FIXME: include
		      //const t_float term = rmul1[j2]; // - rmul2[j2]; //! ie, data1[index1][i] - data2[index2][i];
		      const t_float term = rmul1[j2] - rmul2[j2]; //! ie, data1[index1][i] - data2[index2][i];
		      // FIXME: validate that [”elow] weight_local results in a correct number.
		      assert((i2 + j2) < ncols); //! and if wrong then udpate [below].
		      const t_float weight_local = (weight) ? weight[i + i2] : 1;
		      const t_float result_local = (isTo_use_cityBlock) ? (weight_local*mathLib_float_abs(term)) : (weight_local *term*term); //! (where "*term" does not de-reference a pointer ;)
		      rres[k2] += result_local; //! ie, update resultMatrix + (index1 *index2 + i2*ncols + k2
		      //! Validate:
		      assert(cnt_iterations <= total_cnt_iterations); cnt_iterations++;
		      assert(debug_cntIterations_inner++ <= (chunkSize_index1 * chunkSize_index2 * chunkSize_cnt_i));
		    }
		  }
		}
		// printf("cnt_iterations=%u, debug_max_inner=%u, at %s:%d\n", cnt_iterations, debug_max_inner, __FILE__, __LINE__);
	      }
	      assert(debug_cntIterations_inner <= (chunkSize_index1 * chunkSize_index2 * chunkSize_cnt_i));
	    }
	    assert(debug_cntIterations_inner == (chunkSize_index1 * chunkSize_index2 * chunkSize_cnt_i));
	  }
	}
      }
      if(false) {printf("cnt_iterations=%u, total_cnt_iterations=%u, at %s:%d\n", cnt_iterations, total_cnt_iterations, __FILE__, __LINE__);}
      assert(cnt_iterations == total_cnt_iterations);
      //! Adjsut the matrix:
      if(weight) {__adjustMatrix_byWeight(/*n=*/numberOf_chunks_cols, weight, resultMatrix, nrows, ncols);} //! else we assume the case is not of interest.
    } else { //! Then teh ransposed is computed:
      //! Note: the idea (in the comptuation of the distance in a transposed matrix) is to 'move' "i" and "index1" vertical and "index1"  along the vertical axis. The implicaiton is that 'the sum of columns' is paritally updated for each row: instead of seperately coputing "[i][index1] x [i][index2]" for each pair of ("index1", "index2"), we for each "i" update ("index1", "index2"). The implication is that we accessess different column-index for "data1" and "data2": otherwise the 'space' [data2[i][index1] .... data2[i][index2]] would never have been fixed, ie, we would not have computed an 'all-against-all-sum'. From the latter we infer/observe that "index1" may be 'seen' as 'axis0' in the interprreation/comptuation, while "index" is interpreted as 'axix2' (in the comptuation/'loop'). 
      //! Note(2): to understand why "j2" is used to uupdate/identify "resultMatrix[index1][index2]" and "data1[i][index1]" note that if "k2" had been used, then we would have computed the 'non-transposed' matrix, ie, the results would have been wrong: in order to update different resultMatrix[index1][index2] we need to 'shift' (the columns) between each new-identifed value, hence the need to use the inner "j2" counter. Wrt. the "rmul1[j2]" we observe that the counters need to be different (between "rmul1" and "rmul2"), ie, as we otehrwise would compute ....
      //! Note(3): in this approxh we order the for-loops based on comparison of results from "e_typeOf_optimization_distance_none_xmtOptimizeTransposed" and "e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal"
      // FIXME: update [abov€] and [”elow] based on results from measurements of [ªbov€] (eg, wrt. "e_typeOf_optimization_distance_none_xmtOptimizeTransposed").
      bool debug_lastRow_seen = false;
      const uint total_cnt_iterations = nrows * ncols * ncols;
      const t_float *__restrict__ pointer_dataLastPos1 = (isTo_use_continousSTripsOf_memory) ? data1[0] + total_cnt_iterations : NULL; //! which is used to validate that we do not overwrite memory.
      const t_float *__restrict__ pointer_resultLastPos1 = (isTo_use_continousSTripsOf_memory) ? resultMatrix[0] + total_cnt_iterations : NULL; //! which is used to validate that we do not overwri
      //! Iterate:

      // FIXME: update [below] adn [ªbove] by 'computing only for index2 <= index1'

      // FIXME[jan-christian]: may you verify correctness of [below] 'euclidiaon transpsoe through memory-tiling'?

      for(uint cnt_i = 0, i=0; cnt_i < numberOf_chunks_rows; cnt_i++, i += SM)  {
	const uint chunkSize_cnt_i = (cnt_i < numberOf_chunks_rows) ? SM : chunkSize_row_last;
	
	if(chunkSize_cnt_i != SM) {
	  if(false) {printf("(info)\t last-row for i=%u, cnt_i=%u, and numberOf_chunks_rows=%u, at %s:%d\n", i, cnt_i, numberOf_chunks_cols, __FILE__, __LINE__);}
	  assert(debug_lastRow_seen == false); debug_lastRow_seen = true;
	}
	for(uint cnt_index1 = 0, index1 = 0; cnt_index1 < numberOf_chunks_cols; cnt_index1++, index1 += SM) {
	  const uint chunkSize_index1 = (cnt_index1 < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	  for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < numberOf_chunks_cols; cnt_index2++, index2 += SM) {
	    const uint chunkSize_index2 = (cnt_index2 < numberOf_chunks_cols) ? SM : chunkSize_col_last;
	    //! --------------------------------------------------------------------------------------
	    //! Start the memory-cache-optimized part: select sub-squares, and comptue seperately for each of these s'quares':
	    uint i2 = 0; const uint numberOf_traversals_inner = chunkSize_index2 * chunkSize_cnt_i; uint debug_cntIterations_inner = 0;
	    //! Note: if [below] we in the first loops 'explore' the space of [ index1 ... (index1+chunkSize_index1) ][ index2 ... (index2+chunkSize_index2) ]
	    /**
	       Note: similar to for "transposed == 0" we find this procedure/code rather complex/non-intutive. To help the understanding (and identificaiton of bugs: sadly it is diffuclt to rwrite code without any bugs, eg, wrt. splelling errors).  In below code we:
	       ----------------------------------------------------------------------------------------
	       data1-row[index1 + i2*ncols] ... column[index2] | | | | | | column[index2 + chunkSize_index1] 
	       .                        result[index1+0][...]          +=    data2-row[index1 + 0]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+1][...]          +=    data2-row[index1 + 1]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+2][...]          +=    data2-row[index1 + 2]     ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       .                        result[index1+k2][...]         +=    data2-row[index1 + k2]    ... column[index2] | | | | | | column[index2 + chunkSize_index2] 
	       ----------------------------------------------------------------------------------------
	       From the above depidction of 'comptuation for each matrix-tile' we infer that the column-index for "result" is be the same as the column-index in is to be the same for "result" as for "data2": when 'overlapping' the rows of "data1" and "data2" "index2" is used in "result" to update the 'current estimates/comptuations'.
	     **/
	    for (i2 = 0, //! ie, for "index1" 
		   rres  = &resultMatrix[index1][index2], //! ie, the 'upper square' of the matrix we update.		   		   
		   // FIXME: validate [below]
		   rmul1 = &data1[i][index1], rmask1 = (!mask1) ? NULL : &mask1[i][index1];
		 i2 < chunkSize_cnt_i;
		 i2++, 
		   //! Then move to the same position in the next row, ie, [index1++][index2], which expalins why we acccess the local list through "rres[k2]"		   
		   rres += ncols, 		   
		   rmask1 += (!rmask1) ? 0 : ncols, rmul1 += ncols
		   //rres += numberOf_traversals_inner, rmul1 += numberOf_traversals_inner
		 ) {

	      //! Prefetch the data:
	      // FIXME: in [”elow] is "8" duet ot hte sizeof(t_float) ??
	      // FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      //	      _mm_prefetch (&data1[8], _MM_HINT_NTA);
	      if(mask1) {_mm_prefetch (&mask1[8], _MM_HINT_NTA);}
	      
	      uint k2 = 0; 
	      assert(index2 < ncols); //! ie, given [below]
	      for (k2 = 0, //! ie, for "index2" 
		     // FIXME: validate [below]
		     rmul2 = &data2[i][index2], 
		     rmask2 = (!mask2) ? NULL : &mask1[i][index2];
		   k2 < chunkSize_index1; k2++, // , rmul2 += chunkSize_cnt_i
		     //! Move to the same psotion in the next row:
		     // FIXME: validte [below]
		     rmask2 += (!rmask2) ? 0 : ncols, rmul2 += ncols
		   ) {		
		
		// FIXME: experiement with [”elow] wrt. our 'transposed' appraoch.
		//! Note: [”elow] "_mm_prefetch(..)" for "data2" is commented out as we have observed that software on averge use longer time if the 'call' is included.
		// _mm_prefetch (&data2[8], _MM_HINT_NTA);

		// assert(false); // FIXME: is [below] correct ... ie, as we in [below] iterate through 'number of eows' threshold-block-size .... ??
		// assert(false); // FIXME: tehreafter update our 'optimzal' approach.

		//! The operation:
		for (uint j2 = 0; j2 < chunkSize_index2; ++j2) { //! which is used to 'move' the poiner to a new column:
		  if( ( (!rmask1 || rmask1[k2]) && (!rmask2 || rmask2[j2]) ) ) {			    


		    // assert(false); // FIXME: why is "k2" fixed? <-- thereafter update [above] for 'transposed==1'

		    if(pointer_dataLastPos1) {assert((rmul1 + k2) < pointer_dataLastPos1);} //! ie, validate that we are isndiet he range
		    if(pointer_resultLastPos1) {assert((rres + j2) < pointer_resultLastPos1);} //! ie, validate that we are isndiet he range
		    // ... 
		    if(isOf_interest(rmul1[k2]) && isOf_interest(rmul2[j2])) {
		      
		      if(false) {printf("[%u][%u][%u] + [%u, %u, %u], at %s:%d\n", i, index1, index2, i2, k2, j2, __FILE__, __LINE__);}


		      const t_float term = rmul1[k2] - rmul2[j2]; //! ie, data1[i][index1 + i2*ncols ] - data2[i][index2 + k2*ncols + j2];
		      // FIXME: validate that [”elow] weight_local results in a correct number.
		      assert((i + i2) < ncols); //! and if wrong then udpate [below].
		      const t_float weight_local = (weight) ? weight[i + i2] : 1;
		      const t_float result_local = (isTo_use_cityBlock) ? (weight_local*mathLib_float_abs(term)) : (weight_local *term*term); //! (where "*term" does not de-reference a pointer ;)
		      rres[j2] += result_local; //! ie, as we are itnerested in updating at "index2", ie, update resultMatrix[index1 = i2*ncols][index2 + j2], ie, for each iteration we update a new 'index2 +=1' variable.
		    }
		    // if(cnt_iterations > 20) {assert(false);} // FIXME: remove.
		    //! Validate:
		    assert(cnt_iterations <= total_cnt_iterations); cnt_iterations++;
		    assert(debug_cntIterations_inner++ <= (chunkSize_index1 * chunkSize_index2 * chunkSize_cnt_i));
		  }
		}
		// printf("cnt_iterations=%u, debug_max_inner=%u, at %s:%d\n", cnt_iterations, debug_max_inner, __FILE__, __LINE__);
	      }
	      assert(debug_cntIterations_inner <= (chunkSize_index1 * chunkSize_index2 * chunkSize_cnt_i));
	    }
	  }
	}
      }
      //printf("cnt_iterations=%u, total_cnt_iterations=%u, at %s:%d\n", cnt_iterations, total_cnt_iterations, __FILE__, __LINE__);
      assert(cnt_iterations == total_cnt_iterations);
      // FIXME: validate [below] param
      if(weight) {__adjustMatrix_byWeight(/*n=*/numberOf_chunks_rows, weight, resultMatrix, nrows, ncols);} //! else we assume the case is not of interest.     
    }
  } else if( (typeOf_optimization == e_typeOf_optimization_distance_none_xmt_functionInlined) ) {
    //! Ntoe: test the effect of 'inlining' both the "transpose" caluse and the fucntion-code itself.
    if(transpose == 0) {
      for(uint index1 = 0; index1 < nrows; index1++) {
	for(uint index2 = 0; index2 < nrows; index2++) {
	  if(isTo_use_cityBlock) {
	    resultMatrix[index1][index2] = cityblock(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose);
	  } else {
	    resultMatrix[index1][index2] = euclid(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose);
	  }
	}
      }
    } else { //! then we compare the columns:
      for(uint index1 = 0; index1 < ncols; index1++) {
	for(uint index2 = 0; index2 < ncols; index2++) {
	  if(isTo_use_cityBlock) {
	    resultMatrix[index1][index2] = cityblock(nrows, data1, data2, mask1, mask2, weight, index1, index2, transpose);
	  } else {
	    resultMatrix[index1][index2] = euclid(nrows, data1, data2, mask1, mask2, weight, index1, index2, transpose);
	  }
	}
      }
    }
  } else if( (typeOf_optimization == e_typeOf_optimization_distance_none) || (!transpose && (
											     (typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed) ||  (typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal)					
											     )) ) {
    //printf("\t nrows=%u, ncols=%u, at %s:%d\n", nrows, ncols, __FILE__, __LINE__);
    //! Note: this approach is the 'default approach' which is seen in life-science research-software, eg, in the popular "C clustering software" lbirary/tool.
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare two lists: same number of cache-misses for both the 'default' "C-clustering-library" memorya-allcoation-rotuien and KnittingTools 'improved'/'ideal' memory-allocation-scheme.
    if(transpose == 0) { /* Calculate the distance between two rows */
      for(uint index1 = 0; index1 < nrows; index1++) {
	for(uint index2 = 0; index2 < nrows; index2++) {
	  t_float result = 0.; 	t_float tweight = 0;
	  //printf("[%u][%u], at %s:%d\n", index1, index2, __FILE__, __LINE__);
	  //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare the same column in mulitple row: if the column-size does not signficantly exceed the "L2" memory-cache-size (for a given computer-memory-hardware) then we expect an 'ideal' memory-allocation-scheme to provide/give a "linear" "n" speed-up (for every call to this function).
	  const uint sizeOf_i = ncols;
	  for(uint i = 0; i < sizeOf_i; i++)  {
	    if( ( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) ) {	
	      if(isOf_interest(data1[index1][i]) && isOf_interest(data2[index2][i])) {
		const t_float term = data1[index1][i] - data2[index2][i];
		const t_float weight_local = (weight) ? weight[i] : 1;
		result += weight_local*term*term;
		tweight += weight_local;
	      }
	    }
	  } 
	  //! Adjust the score by the weight
	  if(result != 0) {
	    if(tweight != 0) {result /= tweight;} else {result = 0;} //! which may be explained by an empty cluster.
	  }
	  resultMatrix[index1][index2] = result;
	}
      }
    } else { //! then we expect the code to go slower.
      for(uint index1 = 0; index1 < ncols; index1++) {
	for(uint index2 = 0; index2 < ncols; index2++) {
	  t_float result = 0.; 	t_float tweight = 0;
	  //printf("[%u][%u], at %s:%d\n", index1, index2, __FILE__, __LINE__);
	  assert(index2 < ncols);
	  const uint sizeOf_i = nrows;
	  for(uint i = 0; i < sizeOf_i; i++) {
	    // printf("i=[%u], at %s:%d\n", i, __FILE__, __LINE__);
	    if( ( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) ) {
	      if(isOf_interest(data1[i][index1]) && isOf_interest(data2[i][index2])) {
		t_float term = data1[i][index1] - data2[i][index2];
		const t_float weight_local = (weight) ? weight[i] : 1;
		result += weight_local *term*term;
		tweight += weight_local;
	      }
	    }
	  }
	  //! Adjust the score by the weight
	  if(result != 0) {
	    if(tweight != 0) {result /= tweight;} else {result = 0;} //! which may be explained by an empty cluster.
	  }
	  resultMatrix[index1][index2] = result;
	}
      }
    }
  } else if(transpose && 
	    ( (typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed) ||  (typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal) ) ) {
    //! Then we optimize the matrix-sum-operation wrt. transposed matrix-computation:
    //! Note: idea is to mvoe the 'variable step' in the 'middle of the comptuation', ie, instead of 'as the last/inner comptaution'. When studying the improved/udpated memroy-access-pattersn (of below implementaiton) we observe that instead of having "nrows * nrows * ncols"  'potential' cache-misses, the number of 'potential' cache-misses are reduced to ...??..
    //! Note(2): when compared to [above] "e_typeOf_optimization_distance_none" we observe an optimization-effect of ...??..
    const uint sizeOf_i = nrows;
    // FIXME: validate correctness of [below].
    if(typeOf_optimization == e_typeOf_optimization_distance_none_xmtOptimizeTransposed) {
      for(uint i = 0; i < sizeOf_i; i++) {
	const t_float weight_local = (weight) ? weight[i] : 1;
	//! Note: the order of [below] for-loops is due to the assumption/assertion/observation that the rows of "data1[i]" and "data2[i]" are accessed mulitple times, ie, we are itnerested in 'having the latter as long as possible in memory-cache'.
	//! Note(2): wrt. memory-cache-misses, we expect the data-sets to be assicated to "nrows * 3" memory-cache-misses.
	for(uint index1 = 0; index1 < ncols; index1++) {
	  if(!mask1 || mask1[i][index1]) {
	    if(isOf_interest(data1[i][index1])) {
	      for(uint index2 = 0; index2 < ncols; index2++) {
		if(!mask2 || mask2[i][index2]) {
		  if(isOf_interest(data2[i][index2])) {
		    const t_float term = data1[i][index1] - data2[i][index2];
		    const t_float result_local = weight_local *term*term;
		    resultMatrix[index1][index2] += result_local;
		  }
		}
	      }
	    }
	  }
	}
      }
    } else { //! Then we evlaute/test the effect of having to load the data1[i] row into mmemory mulitple tiems, ie, the importance of order wrt. mulitiple for-loops.
      for(uint index1 = 0; index1 < ncols; index1++) {
	for(uint i = 0; i < sizeOf_i; i++) {
	  if(!mask1 || mask1[i][index1]) {
	    if(isOf_interest(data1[i][index1])) {
	      const t_float weight_local = (weight) ? weight[i] : 1;
	      for(uint index2 = 0; index2 < ncols; index2++) {
		if(!mask2 || mask2[i][index2]) {
		  if(isOf_interest(data2[i][index2])) {
		    t_float term = data1[i][index1] - data2[i][index2];
		    const t_float result_local = weight_local *term*term;
		    resultMatrix[index1][index2] += result_local;
		  }
		}
	      }
	    }
	  }
	}
      }
    }

    if(weight) {__adjustMatrix_byWeight(/*n=*/sizeOf_i, weight, resultMatrix, nrows, ncols);} //! else we assume the case is not of interest.
  } else {
    assert(false); //! then we need to add support for this.
  }  
}

//! Infer an all-against-all distance-matrix for the set of input-data.
void compute_allAgainstAll_distanceMetric_slow(const char dist, const uint nrows, const uint ncols, t_float **data1, t_float **data2, t_float weight[], t_float **resultMatrix, char** mask1, char** mask2, uint transpose, const bool isTo_invertMatrix_transposed,  const uint CLS, const bool isTo_use_continousSTripsOf_memory, const uint iterationIndex_2, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const e_cmp_masksAre_used_t  masksAre_used ) {

  assert(CLS > 0);
  const enum e_typeOf_optimization_distance typeOf_optimization  = e_typeOf_optimization_distance_asFastAsPossible;

  if(isTo_invertMatrix_transposed && transpose) {
    t_float **data1_transposed = allocate_2d_list_float(ncols, nrows, /*default-value=*/0);
    char **data1_transposed_mask = (mask1) ? allocate_2d_list_char(ncols, nrows, /*default-value=*/0) : NULL;
    t_float **data2_transposed = data1_transposed;
    char **data2_transposed_mask = NULL;
    matrix__transpose__compute_transposedMatrix(nrows, ncols, data1, mask1, data1_transposed, data1_transposed_mask, /*isTo_useIntrisinistic=*/true);
    if(data1 != data2) {
      data2_transposed = allocate_2d_list_float(ncols, nrows, /*default-value=*/0);
      data2_transposed_mask = (mask2) ? allocate_2d_list_char(ncols, nrows, /*default-value=*/0) : NULL;
      // FIXME: udpate [”elow] boolean aprameter after/hwen we have doe some benchmarkt-esting.
      matrix__transpose__compute_transposedMatrix(nrows, ncols, data2, mask2, data2_transposed, data2_transposed_mask, /*isTo_useIntrisinistic=*/true);
    }
    //! Compute the result, calling this funciton once more:
    //! Note: the 'two-step' call is sued to simplify the logics fo this fucntion, ie, to ese maitnainance and verifciaiton of crrectness (wrt. the implemnetiaton).
    compute_allAgainstAll_distanceMetric_slow(dist, ncols, nrows, data1_transposed, data2_transposed, weight, resultMatrix, data1_transposed_mask, data2_transposed_mask, /*transpose=*/0, /*isTo_invertMatrix_transposed=*/false, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, masksAre_used);
    //! De-allcoat ehte locally stored matrices:
    free_2d_list_float(&data1_transposed, ncols);
    if(data1_transposed_mask) {free_2d_list_char(&data1_transposed_mask, ncols);}
    if(data1 != data2) {
      free_2d_list_float(&data2_transposed, ncols);
      if(data2_transposed_mask) {free_2d_list_char(&data2_transposed_mask, ncols);}
    }
    //! Allocate a temporal result-matrix
    t_float **result_transposed = allocate_2d_list_float(ncols, ncols, /*default-value=*/0);    

    //! Tranpose the result:
    matrix__transpose__compute_transposedMatrix(ncols, ncols, resultMatrix, NULL, result_transposed, NULL, /*isTo_useIntrisinistic=*/true);
    //! Copy the result to the output:
    if(isTo_use_continousSTripsOf_memory) {
      memcpy(resultMatrix[0], result_transposed[0], sizeof(t_float)*ncols*ncols);
    } else { //! then we need to tierate seperately through each 'of the cases':
      for(uint i = 0; i < ncols; i++) {
	memcpy(resultMatrix[i], result_transposed[i], sizeof(t_float)*ncols);
      }
    }
    //! De-allcoate the temoprarily reserved result-matrix:
    free_2d_list_float(&result_transposed, ncols);
    return; //! ie, as we at this eceuction-poitn has completed out 'task'.
  }
  
  
  
  //! Note: difference between "tranpose==1" and "transpose==0" is found not wrt. the 'mulitplication of entites': instead the difference is seen/found wrt. how the elments are summed/lumbed together. To examplify the latter, we for ...??... 

  // FIXME: ensure that the for-loops are easily seperatble ... ie, that parallisaiton is easy/trivial.

  // FIXME: given teh symmetric property of summing ... consider to use/limit the summation to " J <= i"
  
  //printf("dist='%c', at %s:%d\n", dist, __FILE__, __LINE__);
  
  struct s_allAgainstAll_config config_allAgainstAll; init__s_allAgainstAll_config(&config_allAgainstAll, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, masksAre_used, /*isTo_use_SIMD=*/true, typeOf_optimization);
  

switch(dist) 
    {
    case 'e': {compute_allAgainstAll_distanceMetric_euclid_or_cityBlock(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*isTo_use_cityBlock=*/false, transpose, typeOf_optimization, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, masksAre_used); return;}
    case 'b': {compute_allAgainstAll_distanceMetric_euclid_or_cityBlock(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*isTo_use_cityBlock=*/true, transpose, typeOf_optimization, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, masksAre_used);  return;}
    case 'c': {ktCorr_compute_allAgainstAll_distanceMetric_correlation(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, e_typeOf_metric_correlation_pearson_basic, transpose,  &config_allAgainstAll);  return;}
    case 'a': {ktCorr_compute_allAgainstAll_distanceMetric_correlation(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, e_typeOf_metric_correlation_pearson_absolute, transpose, &config_allAgainstAll);  return;}
    case 'u': {ktCorr_compute_allAgainstAll_distanceMetric_correlation(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, e_typeOf_metric_correlation_pearson_uncentered, transpose, &config_allAgainstAll);  return;}
    case 'x': {ktCorr_compute_allAgainstAll_distanceMetric_correlation(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, e_typeOf_metric_correlation_pearson_uncentered_absolute, transpose, &config_allAgainstAll); return;}
      // FIXME: validate cornrectees of [”elow] "e_typeOf_measure_spearman_masksAreAligned_always," param wrt. the callers of this funciton.
    case 's': {ktCorr_compute_allAgainstAll_distanceMetric_spearman(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*masksAre_aligned=*/e_typeOf_measure_spearman_masksAreAligned_always, transpose, &config_allAgainstAll);  return;}
    case 'k': {
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;
	ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, transpose, metric_id, &config_allAgainstAll); return;}
    default: compute_allAgainstAll_distanceMetric_euclid_or_cityBlock(nrows, ncols, data1, data2, mask1, mask2, weight, resultMatrix, /*isTo_use_cityBlock=*/false, transpose, typeOf_optimization, CLS, isTo_use_continousSTripsOf_memory, iterationIndex_2, typeOf_postProcessing_afterCorrelation, s_inlinePostProcess, masksAre_used);
    }
}


//! Infer an all-against-all distance-matrix for the set of input-data.
void compute_allAgainstAll_distanceMetric_slow_configObject(const char dist, const uint nrows, const uint ncolumns, t_float **data1, t_float **data2, t_float weight[], t_float **resultMatrix, char** mask1, char** mask2, uint transposed, const uint iterationIndex_2, const s_allAgainstAll_config_t config) {
  compute_allAgainstAll_distanceMetric_slow(dist, nrows, ncolumns, data1, data2, weight, resultMatrix, mask1, mask2, transposed, config.isTo_invertMatrix_transposed, config.CLS, config.isTo_use_continousSTripsOf_memory,  iterationIndex_2, config.typeOf_postProcessing_afterCorrelation, config.s_inlinePostProcess, config.masksAre_used);
}

