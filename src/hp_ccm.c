#include "hp_ccm.h"
#include "hp_distance.h"

/** @brief idniety the CCM-score (oekseth, 06. feb. 2017). **/
bool ccm__advanced__singleMatrix__hp_ccm(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum, const s_kt_matrix_base_t *matrix, const uint *mapOf_vertexToCentroid1, s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config, t_float *scalar_score) { //, const s_kt_matrix_t *matrix) {   
  assert(scalar_score);
  *scalar_score = T_FLOAT_MAX; //! ie, itnalise.
  if(!matrix || !matrix->matrix || (matrix->nrows == 0) ) {
    fprintf(stderr, "!!\t The inptu-amtrix was Not set, ie, please provide a symmetitrc iniput-matrix. For quesitons  pelase cotnact the senior delvoeper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false);
    return false;
  }
  if(ccm_config.isTo_use_KD_treeInSimMetricComputations == false) {
    if(matrix->nrows != matrix->ncols) {
      fprintf(stderr, "!!\t The inptu-amtrix is Not symmetirc, ie, please provide a symmetitrc iniput-matrix. For quesitons  pelase cotnact the senior delvoeper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false);
      return false;
    }
  }
  if(ccm_config.isTo_use_KD_treeInSimMetricComputations) {
    if(ccm_config.metric_complexClusterCmp_vertexCmp_isTo_use == false) {
      fprintf(stderr, "!!\t(not supported)\t Ignores setting: requested KD-tree for cases where 'vertexCmp_isTo_use == false'. If he latter is of itnerest, it may be supported, ie, for which contacting Dr. Ekseth at [oekseth@gmail.com] may resolve the issue. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); //! (oekseth, 06. apr. 2018).
      ccm_config.metric_complexClusterCmp_vertexCmp_isTo_use = true; //! ie, sets the 'current supported'.
    }
  }
  uint k_clusterCount = 0;

  { uint cnt_interesting = 0;
    for(uint i = 0; i < matrix->nrows; i++) {
      if(mapOf_vertexToCentroid1[i] != UINT_MAX) {
	k_clusterCount = macro_max(k_clusterCount, mapOf_vertexToCentroid1[i]);
	cnt_interesting++; //! which is used to handle the ase where all verices are member of the same cluster (oekseth, 06. par. 2018).
      }
    }
    //printf("at %s:%d\n", __FILE__, __LINE__);
    if(cnt_interesting == 0) {return true;} //! ie, as not clsutermemberships are known.
  }
  //printf("k_clusterCount=%u, at %s:%d\n", k_clusterCount, __FILE__, __LINE__);
  //if(k_clusterCount == 0) {*scalar_score = T_FLOAT_MAX; return true;} //! ie, as all vertices are in the same clsuter.
  //else
  {k_clusterCount++;}
  s_kt_matrix_cmpCluster_clusterDistance_t obj_local = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
  //e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC;;
  e_kt_matrix_cmpCluster_clusterDistance__config_metric__t ccm_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__default;;
  //! Initiate the object:
  long long int cnt_cellsOf_interest = 0;
  for(uint i = 0; i < matrix->nrows; i++) {
    for(uint k = 0; k < matrix->ncols; k++) {
      cnt_cellsOf_interest += isOf_interest(matrix->matrix[i][k]);}}
  //printf("cnt_cellsOf_interest=%lld, k_clusterCount=%u, at %s:%d\n", cnt_cellsOf_interest, k_clusterCount, __FILE__, __LINE__);
  if(cnt_cellsOf_interest > 0) {
    //printf("at %s:%d\n", __FILE__, __LINE__);
    init__s_kt_matrix_cmpCluster_clusterDistance(&obj_local, mapOf_vertexToCentroid1, matrix->nrows, matrix->ncols, k_clusterCount, matrix->matrix, ccm_config);
    //!
    //! Compute: 
    *scalar_score = compute__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local, /*enum_id=*/ccm_enum,  /*cmp_type=*/ccm_type);
    
    /* s_kt_matrix_cmpCluster_clusterDistance_config_t config_local  = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t(); */
    /* s_kt_matrix_cmpCluster_clusterDistance_t obj_local = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance(); */
    /* assert(obj_dataConfig.k_clusterCount != 0); assert(obj_dataConfig.k_clusterCount != UINT_MAX); */
    /* *scalar_score = compute__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local, config__ccmMatrixBased__toUse, /\*cmp_type=*\/e_kt_matrix_cmpCluster_clusterDistance__config_metric__default); */
    //!
    //! De-allcoate:
    free__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local);		  
  } else {
    *scalar_score = T_FLOAT_MAX; //! ie, as the inptu-amtrix was empty.
  }
  //!
  //! @return:
  return true;
}

/** @brief idniety the CCM-score (oekseth, 06. feb. 2017). **/
bool ccm__singleMatrix__hp_ccm(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum, const s_kt_matrix_base_t *matrix, const uint *mapOf_vertexToCentroid1, t_float *scalar_score) { //, const s_kt_matrix_t *matrix) {   
  assert(scalar_score);
  s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
  return ccm__advanced__singleMatrix__hp_ccm(ccm_enum, matrix, mapOf_vertexToCentroid1, ccm_config, scalar_score);
}

//! First copmtue simalrity, and thereafter identify the CCM-score (oekseth, 06. otk. 2017).
//! @remarks extends the "ccm__singleMatrix__hp_ccm(..)" with logics to first apply/use a defulat simalrity-matrix
bool ccm__singleMatrix__applyDefSimMetricBefore__hp_ccm(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum, const s_kt_matrix_base_t *matrix, const uint *mapOf_vertexToCentroid1, t_float *scalar_score) {
  assert(scalar_score);
  //! SImialrty 
  s_kt_matrix_base_t obj_result =initAndReturn__empty__s_kt_matrix_base_t(); 
  {
    s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t(); 
    const bool is_ok = apply__hp_distance(obj_metric, matrix, NULL, &obj_result, init__s_hp_distance__config_t()); 
    assert(is_ok);    
  }
  //! CCM:
  s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
  const bool is_ok = ccm__advanced__singleMatrix__hp_ccm(ccm_enum, &obj_result, mapOf_vertexToCentroid1, ccm_config, scalar_score);
  free__s_kt_matrix_base_t(&obj_result);
  //! @return
  return is_ok;
}

//! A wrapper to our "ccm__singleMatrix__hp_ccm(..)" where we comptue for the complete set of matrix-based CCMs
bool ccm__singleMatrix__completeSet__hp_ccm(const s_kt_matrix_base_t *matrix, const uint *mapOf_vertexToCentroid1, s_kt_list_1d_float *obj_result) { //, const s_kt_matrix_t *matrix) {   
  assert(obj_result);
  assert(matrix);
  assert(mapOf_vertexToCentroid1);
  assert(matrix->nrows != 0); 
  if(obj_result->list != NULL) {
    assert(obj_result->list_size == (uint)e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
  } else {
    assert(obj_result->list == NULL); //! ie, as we expect the latter object to Not be itnalised.
    *obj_result = init__s_kt_list_1d_float_t(e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
  }
  for(uint i = 0; i < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; i++) {
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i;
    t_float scalar_score = T_FLOAT_MAX;
    const bool is_ok = ccm__singleMatrix__hp_ccm(ccm_enum, matrix, mapOf_vertexToCentroid1, &scalar_score);
    assert(is_ok);
    obj_result->list[i] = scalar_score;
  }
  //!
  //! @return:
  return true;
}

//! PRovide support for both matrix-based and 'direct clsuterbased comaprsion' using CCMs (oesketh, 06. feb. 2017).
bool ccm__twoHhypoThesis__hp_ccm(const e_kt_matrix_cmpCluster_metric_t ccm_enum, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint cnt_vertices, t_float **matrixOf_coOccurence_1, t_float **matrixOf_coOccurence_2, t_float *scalar_score) {   
  //! Approximate number of max-clusters:
  uint k_clusterCount = 0;
  { //! Validate taht clsutere-mberships are set (oesketh, 06. mar. 2017):
    uint debug_cntFound_1 = 0;    uint debug_cntFound_2 = 0;
    for(uint i = 0; i < cnt_vertices; i++) {
      if(mapOf_vertexToCentroid1[i] != UINT_MAX) {
	k_clusterCount = macro_max(k_clusterCount, mapOf_vertexToCentroid1[i]);
	debug_cntFound_1++;
      }
      if(mapOf_vertexToCentroid2[i] != UINT_MAX) {
	k_clusterCount = macro_max(k_clusterCount, mapOf_vertexToCentroid2[i]);
	debug_cntFound_2++;
      }
    }
    //! if not any clsuter-e,mershisp are knwon then we asusme a bug is found wrt. the input-data':
    if(!debug_cntFound_1 || !debug_cntFound_2) {
      fprintf(stderr, "!!\t We expected clsuter (or centorid) membershisp to be set for both the 'gold-1' and 'gold-2', which is Not the case. In brief we sugget updating your API and/or your input-files. If latter does Not help (or soudns all to comples) then please contact senior delvoper [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      //! Given teh compielr somehints:
      assert(debug_cntFound_1 > 0);
      assert(debug_cntFound_2 > 0);
      //! Provide a 'fall-back':
      *scalar_score = T_FLOAT_MAX; return true; //! ie, as all vertices are in the same clsuter.
    }
    if(matrixOf_coOccurence_1 && matrixOf_coOccurence_2) { //! Investigate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
      assert(matrixOf_coOccurence_1 != NULL);
      assert(matrixOf_coOccurence_2 != NULL);      
      assert(cnt_vertices > 0);
      long long int cnt_interesting_1 = 0;
      long long int cnt_interesting_2 = 0;
      const uint nrows = cnt_vertices;
      const uint ncols = cnt_vertices;
      for(uint i = 0; i < nrows; i++) {
	assert(matrixOf_coOccurence_2[i] != NULL);	
	assert(matrixOf_coOccurence_1[i] != NULL);
	for(uint j = 0; j < ncols; j++) {
	  cnt_interesting_1 += isOf_interest(matrixOf_coOccurence_1[i][j]); 
	  cnt_interesting_2 += isOf_interest(matrixOf_coOccurence_2[i][j]);
	} }	  
      // printf("(cnt-interesting(%llu and %llu), at %s:%d\n", cnt_interesting_1, cnt_interesting_2, __FILE__, __LINE__); // FIXME: remoe.
      if(cnt_interesting_1 == 0) {      
	if(true) {fprintf(stderr, "!!\t No cells are of interest (for matrix=1): matrix is without any interestign cells: if the latter soudns surpsiing then pelase contact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);}
	//assert(false); //! ie, an heads-up
	*scalar_score = T_FLOAT_MAX;
	return true; //! ie, as we otherwse have a poitnless input to the clsutering
      }
      if(cnt_interesting_2 == 0) {      
	if(true) {fprintf(stderr, "!!\t No cells are of interest (for matrix=2): matrix is without any interestign cells: if the latter soudns surpsiing then pelase contact the senior develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);}
	//assert(false); //! ie, an heads-up
	*scalar_score = T_FLOAT_MAX;
	return true; //! ie, as we otherwse have a poitnless input to the clsutering
      }      
    }
  }
  if(k_clusterCount == 0) {*scalar_score = T_FLOAT_MAX; return true;} //! ie, as all vertices are in the same clsuter.
  else {k_clusterCount++;}
  // printf("k_clusterCount=%u, at %s:%d\n", k_clusterCount, __FILE__, __LINE__);
  //! ---- 
  s_kt_matrix_cmpCluster_t obj_cmp; setTo_empty__s_kt_matrix_cmpCluster_t(&obj_cmp);
  //const uint max_cntClusters = macro_max(cnt_clusters_1, cnt_clusters_2);
  s_kt_matrix_cmpCluster_clusterDistance_config config_intraCluster = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
  build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, cnt_vertices, /*isTo_includeWeight=*/false, k_clusterCount, matrixOf_coOccurence_1, matrixOf_coOccurence_2, config_intraCluster);
  
  //! Comptue for the 'specific' cluster-comparison-score:
  s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
  computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, ccm_enum, &metric_result); 
  const t_float scalar_score_altTest = metric_result.score_1;
  *scalar_score = scalar_score_altTest;
  //!
  //! De-allcoate:
  free__s_kt_matrix_cmpCluster(&obj_cmp);
  //!
  //! @return:
  return true;
}

//! @remarks wrt. input-paramters see "ccm__twoHhypoThesis__hp_ccm(..)"
//! @remarks if performance is of importance, then we suggest using routines defined in our "kt_matrix_cmpCluster.h" (ie, where 'this function' is a wrapper for the latter).

//! Comptue for all 'naive defnions of CCMs' supported in Knitting-Tools.
bool ccm__twoHhypoThesis__completeSet__hp_ccm(uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint cnt_vertices, t_float **matrixOf_coOccurence_1, t_float **matrixOf_coOccurence_2, s_kt_list_1d_float *obj_result) {
  assert(obj_result);
  assert(mapOf_vertexToCentroid1);
  assert(mapOf_vertexToCentroid2);
  assert(cnt_vertices != UINT_MAX); assert(cnt_vertices > 0);
  assert(obj_result->list == NULL); //! ie, as we expect the latter object to Not be itnalised.
  *obj_result = init__s_kt_list_1d_float_t(e_kt_matrix_cmpCluster_metric_undef);
  for(uint i = 0; i < e_kt_matrix_cmpCluster_metric_undef; i++) {
    const e_kt_matrix_cmpCluster_metric_t ccm_enum = (e_kt_matrix_cmpCluster_metric_t)i;
    t_float scalar_score = T_FLOAT_MAX;
    const bool is_ok = ccm__twoHhypoThesis__hp_ccm(ccm_enum, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, cnt_vertices, matrixOf_coOccurence_1, matrixOf_coOccurence_2, &scalar_score);
    assert(is_ok);
    obj_result->list[i] = scalar_score;
  }
  //!
  //! @return:
  return true;
}

//! Supports for non-matrix based CCM-comptuations (oekseth, 06. feb. 2017)
bool ccm__twoHhypoThesis__xmtMatrixBased__hp_ccm(const e_kt_matrix_cmpCluster_metric_t ccm_enum, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint cnt_vertices, t_float *scalar_score) { 
  if(ccm_enum >= e_kt_matrix_cmpCluster_metric_silhouetteIndex) {
    fprintf(stderr, "(input-error)\t You specified a matrix-based CCM, ie, please call our \"ccm__twoHhypoThesis__hp_ccm(..)\" funciton. For queisotns please contact senior developer [oekseth@gmail.com]. Observaiotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    *scalar_score = T_FLOAT_MAX;
    return false;
  }
  t_float **matrixOf_coOccurence_1 = NULL; t_float **matrixOf_coOccurence_2 = NULL;
  return ccm__twoHhypoThesis__hp_ccm(ccm_enum, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, cnt_vertices, matrixOf_coOccurence_1, matrixOf_coOccurence_2, scalar_score);
}
