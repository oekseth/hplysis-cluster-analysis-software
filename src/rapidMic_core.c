#include "rapidMic_core.h"

#include "func_hp3.h" //! included to support call to the "__compute_function_F(..)" call (oekseth, 06. july 2016).

 /*
 Dr. Tang rewrite the core MIC implementation based on Davide Albanese
 The following code support rapidly computing MIC.
 We presented a new rapidly computing maximal information-based nonparametric exploration tool for 
 statistical analysis of large-scale dataset.  By parallel processing of MIC algorithm, the algorithm 
 can well analyze large-scale dataset and greatly reduce the coputing time.
 下面的代码由西南交通大学 Dr唐 改写
 本代码采用多线程的方式快速计算MIC 值
 
 Southwest Jiaotong University
 
 */

/*
 This code is written by Davide Albanese <davide.albanese@gmail.com>.
 (C) 2012 Davide Albanese, (C) 2012 Fondazione Bruno Kessler.
 
 This program is free software only for academic use.   you can redistribute it and/or modify
 it under the terms of the hpLysis documentation 
 
 
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 hpLysis documentation for more details.
 
 You should have received a copy of the hpLysis documentation
 along with this program.  
 */
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */





#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
//#include "core.h"
#include <string.h>
#define MAX(a, b) ((a) > (b) ? (a):(b))
#define MIN(a, b) ((a) < (b) ? (a):(b))


/* HQ
 * Returns the entropy induced by the points on the
 * partition Q.
 * See section 3.2.1 at page 10 in SOM.
 *
 * Parameters
 *   N : q x p matrix containing the number of points
 *       for each cell of the grid formed by P and Q
 *       partitions.求每一行的熵，最后累加
 *   q : number of rows of N (number of partitions in Q)
 *   p : number of cols of N (number of partitions in P)
 *   total : total number of points
 */

static t_float HQ(t_sparseVec **N, int q, int p, int total) {
  int i, j;
  t_float prob, H;


#if optmize_use_multInsteadOf_div == 1
  const t_float total_inv = 1.0/(t_float)total;
#endif
  


  H = 0.0;
  for (i=0; i<q; i++) {
    t_sparseVec sum = 0;
#if (defaultCompilation_alwaysUse_SSE == 0)
    for (j=0; j<p; j++) {
      sum += N[i][j];
    }
#else //! then we make use of intristnicts.
    //! Note: to reduce the compelxity of our logics we assuem that the vectors are padded with zeros, ie, for which we do not need a 'trailing' for-loop.
    SPARSEVEC_TYPE vec_sum = SPARSEVEC_SET1(0);
    for(j = 0; j < p; j += SPARSEVEC_ITER_SIZE) {
      vec_sum  = SPARSEVEC_ADD(vec_sum, SPARSEVEC_LOAD(&N[i][j]));
    }

#if(0 == __lcoalConfig__useSparseVecInCmp)
    sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);
#else
    sum = sparseVec_VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);
#endif
#endif

#if optmize_use_multInsteadOf_div == 1
    prob = (t_float) sum * total_inv;
#else
    prob = (t_float) sum / (t_float) total;
#endif
    assert(sum > 0); //! and if this does not hold .... then [below] "Rapid-mine" implementaiton is errnous, ie, as 'one' does not evalaute the case where 
#if optmize_use_logValues_fromLocalTable == 1
    const t_float prob_log = log_index_fast(sum) - log_index_fast(total); 
#else
    t_float prob_log = 0;
    if (prob != 0)   {
      prob_log = log_range_0_one_abs(prob);
    } //else {prob_log = 0;}
#endif
    //! Then combine the reuslts, ie, aka. Shannons entropy:
    H += prob * prob_log;
  }
  return -H;
}

#if (defaultCompilation_alwaysUse_SSE == 1) //! then we define functions computing the sumf ov faleus

#if(0 == __lcoalConfig__useSparseVecInCmp)
//! IDentify the sum of values.
#define macro_sumOf_values_sparseVec(arrOf, arrOf_size) ({\
  SPARSEVEC_TYPE vec_sum = SPARSEVEC_SET1(0) ; \
  const uint arrOf_size_intri = (uint)SPARSEVEC__get_maxIntriLength(arrOf_size); \
  uint j = 0;								\
  for(j = 0; j < arrOf_size_intri; j += SPARSEVEC_ITER_SIZE) {		\
    vec_sum  = SPARSEVEC_ADD(vec_sum, SPARSEVEC_LOAD(&arrOf[j]));	\
  }									\
  t_sparseVec sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);	\
  for(; j < arrOf_size; j++) {sum += arrOf[j];} sum;}) //! @return the sum of values
#else
//! IDentify the sum of values.
#define macro_sumOf_values_sparseVec(arrOf, arrOf_size) ({\
  SPARSEVEC_TYPE vec_sum = SPARSEVEC_SET1(0) ; \
  const uint arrOf_size_intri = (uint)SPARSEVEC__get_maxIntriLength(arrOf_size); \
  uint j = 0;								\
  for(j = 0; j < arrOf_size_intri; j += SPARSEVEC_ITER_SIZE) {		\
    vec_sum  = SPARSEVEC_ADD(vec_sum, SPARSEVEC_LOAD(&arrOf[j]));	\
  }									\
  t_sparseVec sum = sparseVec_VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);	\
  for(; j < arrOf_size; j++) {sum += arrOf[j];} sum;}) //! @return the sum of values
#endif

#if(0 == __lcoalConfig__useSparseVecInCmp)
//! Similar to "macro_sumOf_values_sparseVec(..), with diference that we use a differnet start-pos than "0".
#define macro_sumOf_values_sparseVec_adjustedStartPos(arrOf, arrOf_size, arrOf_adjustedStartPos) ({	\
  SPARSEVEC_TYPE vec_sum = SPARSEVEC_SET1(0) ; \
  const int new_size = arrOf_size - arrOf_adjustedStartPos; \
  const uint arrOf_size_intri = (uint)arrOf_adjustedStartPos + (uint)SPARSEVEC__get_maxIntriLength(new_size); \
  uint j = 0; 								\
  for(j = arrOf_adjustedStartPos; j < arrOf_size_intri; j += SPARSEVEC_ITER_SIZE) { \
    vec_sum  = SPARSEVEC_ADD(vec_sum, SPARSEVEC_LOAD(&arrOf[j]));	\
  }									\
  t_sparseVec sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);	\
  for(; j < arrOf_size; j++) {sum += arrOf[j];} sum;}) //! @return the sum of values
#else
//! Similar to "macro_sumOf_values_sparseVec(..), with diference that we use a differnet start-pos than "0".
#define macro_sumOf_values_sparseVec_adjustedStartPos(arrOf, arrOf_size, arrOf_adjustedStartPos) ({	\
  SPARSEVEC_TYPE vec_sum = SPARSEVEC_SET1(0) ; \
  const int new_size = arrOf_size - arrOf_adjustedStartPos; \
  const uint arrOf_size_intri = (uint)arrOf_adjustedStartPos + (uint)SPARSEVEC__get_maxIntriLength(new_size); \
  uint j = 0; 								\
  for(j = arrOf_adjustedStartPos; j < arrOf_size_intri; j += SPARSEVEC_ITER_SIZE) { \
    vec_sum  = SPARSEVEC_ADD(vec_sum, SPARSEVEC_LOAD(&arrOf[j]));	\
  }									\
  t_sparseVec sum = sparseVec_VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);	\
  for(; j < arrOf_size; j++) {sum += arrOf[j];} sum;}) //! @return the sum of values
#endif

#endif



/* HP3 x在最大p格划分的情况，给定s和t将序列取2份，重新计算熵
   例如目前x现在划分成7个格子，按数据值从小到大为顺序落在每个格子中的数据的个数如下
   [3 2 2 2 1 2 3]
   那么现在给定s=1，t=2则相当于将上面的序列切分成3部分
   [3][2][2 2 1 2 3]
   计算前两部分的熵
   * Returns the entropy induced by the points on the
   * partition <c_0, c_s, c_t>.
   * See line 5 of Algorithm 2 in SOM.
   *
   * Parameters
   *   np : p vector containing the number of points
   *        for each cell of the grid formed by P
   *   p : length of np (number of partitions in P)
   *   s : s in c_s
   *   t : t in c_t
   */
static t_float HP3(t_sparseVec *np, int p, int s, int t) {
  int i;
  t_float prob1, prob2;
  t_float H = 0;
	


  if (s == t)
    return 0.0;

#if (defaultCompilation_alwaysUse_SSE == 0)	
  int sum1 = 0;   int sum2 = 0;
  for (i=0; i<s; i++)
    sum1 += np[i];
  for (i=s; i<t; i++)
    sum2 += np[i];
#else //! then we make use of intristnicts.
  const t_sparseVec sum1 = macro_sumOf_values_sparseVec(np, /*length=*/s);
  const t_sparseVec sum2 = macro_sumOf_values_sparseVec_adjustedStartPos(np, /*length=*/t, /*start-pos=*/s);
#endif

  //! Compute the sum:
  t_sparseVec tot = (t_sparseVec)sum1 + (t_sparseVec)sum2;

#if optmize_use_multInsteadOf_div == 1
  const t_float tot_inverse = 1.0 / (t_float)tot;
  prob1 = (t_float) sum1 * (t_float) tot_inverse;
  prob2 = (t_float) sum2 * (t_float) tot_inverse;
#else
  prob1 = (t_float) sum1 / (t_float) tot;
  prob2 = (t_float) sum2 / (t_float) tot;
#endif	


#if optmize_use_logValues_fromLocalTable == 1
  //! Comput hte logarihms:
  const t_float log_tot = log_index_fast(tot);
  const t_float prob_log_1 = log_index_fast(sum1) - log_tot;
  const t_float prob_log_2 = log_index_fast(sum2) - log_tot;
  //! Update the sums:
  H -= (prob1 * prob_log_1);
  H -= (prob2 * prob_log_2);
  return H; //! ie, to aovid 'muliplcaiton by -1' wrt. the value.
#else	
  H = 0.0;
  if (prob1 != 0)
    H += prob1 * log_range_0_one_abs(prob1);
	
  if (prob2 != 0)
    H += prob2 * log_range_0_one_abs(prob2);
  return -H;
#endif	
}


/* HPQ3
 x在最大p格划分的情况，给定s和t将序列取2份，重新计算熵
 例如目前x现在划分成7个格子，按数据值从小到大为顺序落在每个格子中的数据的个数如下
 [3 2 2 2 1 2 3]
 那么现在给定s=1，t=2则相当于将上面的序列切分成3部分,第一部分为c_0,即从第0位开始
 第二部分为c_s，即从s位开始，第三部分为c_t，即从t位开始
 [3][2][2 2 1 2 3]
 计算在y固定划分，也就是Q划分情况下，Q划分和上面前两部分组成的划分之间的联合熵
 * Returns the entropy induced by the points on the
 * partition <c_0, c_s, c_t> and Q.
 * See line 5 of Algorithm 2 in SOM.
 *
 * Parameters
 *   N : q x p matrix containing the number of points
 *       for each cell of the grid formed by P and Q
 *       partitions.
 *   np : p vector containing the number of points
 *        for each cell of the grid formed by P
 *   q : number of rows of N (number of partitions in Q)
 *   p : number of cols of N and length of np (number of partitions in P)
 *   s : s in c_s
 *   t : t in c_t
 */
static t_float HPQ3(t_sparseVec **N, t_sparseVec *np, int q, int p, int s, int t) {
  int i, j;
  t_float prob1, prob2, H;
  
#if (defaultCompilation_alwaysUse_SSE == 0)
  int tot = 0;
  for (i=0; i<t; i++)
    tot += np[i];
#else //! then we make use of intristnicts.
  const t_float tot = (t_float)macro_sumOf_values_sparseVec(np, /*length=*/t);
#endif

#if optmize_use_multInsteadOf_div == 1
  const t_float tot_inverse = 1.0 / (t_float)tot;
#endif

#if optmize_use_logValues_fromLocalTable == 1
  //! Comput hte logarihms:
  const t_float log_tot = log_index_fast(tot);
#endif

    H = 0.0;
  for (i=0; i<q; i++) {
#if (defaultCompilation_alwaysUse_SSE == 0)	
    int sum1 = 0; int sum2 = 0;
    for (j=0; j<s; j++)
      sum1 += N[i][j];        
    for (j=s; j<t; j++)
      sum2 += N[i][j];
#else //! then we make use of intristnicts.
    t_sparseVec sum1 = macro_sumOf_values_sparseVec(N[i], /*length=*/s);
    t_sparseVec sum2 = macro_sumOf_values_sparseVec_adjustedStartPos(N[i], /*length=*/t, /*start-pos=*/s);
#endif


#if optmize_use_multInsteadOf_div == 1
    prob1 = (t_float) sum1 * (t_float) tot_inverse;
    prob2 = (t_float) sum2 * (t_float) tot_inverse;
#else
    prob1 = (t_float) sum1 / (t_float) tot;
    prob2 = (t_float) sum2 / (t_float) tot;  
#endif  

#if optmize_use_logValues_fromLocalTable == 1
    const t_float prob_log_1 = log_index_fast(sum1) - log_tot;
    const t_float prob_log_2 = log_index_fast(sum2) - log_tot;
    //! Update the sums:
    H -= (prob1 * prob_log_1);
    H -= (prob2 * prob_log_2);
#else
    if (prob1 != 0) {
      H += prob1 * log_range_0_one_abs(prob1);
    }
    if (prob2 != 0) {
      H += prob2 * log_range_0_one_abs(prob2);
    } 
#endif
  }
#if optmize_use_logValues_fromLocalTable == 1  
  return H;
#else
  return -H;
#endif
}


/* HPQ2
 x在最大p格划分的情况，给定s和t将序列取1份，重新计算熵
 例如目前x现在划分成7个格子，按数据值从小到大为顺序落在每个格子中的数据的个数如下
 [3 2 2 2 1 2 3]
 那么现在给定s=2，t=3则取第三个格子组成一个划分格子
 [2]
 计算新组成的划分和Q的联合熵
 * Returns the entropy induced by the points on the
 * partition <c_s, c_t> and Q.
 * See line 13 of Algorithm 2 in SOM.
 *
 * Parameters
 *   N : q x p matrix containing the number of points
 *       for each cell of the grid formed by P and Q
 *       partitions.
 *   np : p vector containing the number of points
 *        for each cell of the grid formed by P
 *   q : number of rows of N (number of partitions in Q)
 *   p : number of cols of N and length of np (number of partitions in P)
 *   s : s in c_s
 *   t : t in c_t
 */
static t_float HPQ2(t_sparseVec **N, t_sparseVec *np, int q, int p, int s, int t) {
  int i, j;
  t_float prob, H;
  
  if (s == t)
    return 0.0;

  

   #if (defaultCompilation_alwaysUse_SSE == 0)	 
  int tot = 0;
  for (i=s; i<t; i++)
    tot += np[i];
#else //! then we make use of intristnicts.
  const t_float tot = (t_float)macro_sumOf_values_sparseVec_adjustedStartPos(np, /*length=*/t, /*start-pos=*/s);
#endif

#if optmize_use_multInsteadOf_div == 1
  const t_float tot_inverse = 1.0 / (t_float)tot;
#endif
#if optmize_use_logValues_fromLocalTable == 1
  const t_float log_tot = (t_float)log_index_fast(tot); 
#endif
  
  H = 0.0;
  for (i=0; i<q; i++) {
#if (defaultCompilation_alwaysUse_SSE == 0)
    int sum = 0;
    for (j=s; j<t; j++) {
      sum += N[i][j];
    }
#else //! then we make use of intristnicts.
    /* const int diff = s-t; */
    int sum = 0;
    /* if(diff < 20) { */
    /*   for (j=s; j<t; j++) { */
    /* 	sum += N[i][j]; */
    /*   } */
    /* } else { */
    sum = macro_sumOf_values_sparseVec_adjustedStartPos(N[i], /*length=*/t, /*start-pos=*/s);
      //}
#endif
#if optmize_use_multInsteadOf_div == 1
    prob = (t_float) sum * (t_float) tot_inverse;
#else
    prob = (t_float) sum / (t_float) tot;
#endif
    
#if optmize_use_logValues_fromLocalTable == 1
    const t_float prob_log_1 = log_index_fast(sum) - log_tot;
    H -= (prob * prob_log_1);
#else
    if (prob != 0) {
      H += prob * log(prob);
    }
#endif
  }
  
#if optmize_use_logValues_fromLocalTable == 1  
  return H;
#else
  return -H;
#endif
}


/* EquipartitionYAxis
 * Returns the map Q: D -> {0, ...,y-1}. See Algorithm 3 in SOM.
 *
 * Parameters
 *   Dy (IN): y-data sorted in increasing order
 *   n (IN): length of Dy
 *   y (IN): an integer greater than 1 预计平均划分成多少格
 *   Qm (OUT): the map Q. Qm must be a preallocated vector
 *       of size n.
 * Return
 *   q : the real value of y. It can be < y.
 */
static int EquipartitionYAxis(t_float *Dy, int n, int y, t_sparseVec_counts *Qm) {
  int i, j, s, h, curr;
  int currrowbeginindex;
  t_float rowsize;
  currrowbeginindex=-1;
  i=0;
  h=0;
  curr = 0;
  rowsize =  (t_float) n / (t_float) y;
  while (i < n) {
    s = 1;
    //优化
    for (j=i+1; j<n; j++)
      if (Dy[i] == Dy[j])//后面有多少个数据和当前值相同
	s++;
      else
	break;
    if (currrowbeginindex!=-1) {
      h=i-currrowbeginindex;
    } else {
      currrowbeginindex=0;
      h=0;
    }
    if ((h != 0) && (fabs(h+s-rowsize) >= fabs(h-rowsize)))//一格数据满了
      {
	curr++;
	currrowbeginindex=i;
	rowsize = (t_float) (n-i) / (t_float) (y-curr);//因为上一格数据可能由于刚好截断的时候有相同值的数据，因此剩余的数据可能就要少了，所以需要重新计算剩余数据平均每格放多少个数据
      }

    /* if(isTo_use_codeRewritingTo_reduceExeuctionTime()) { */
    /*   // FIXME: validate that ["elow] resutls in a performanc-eicnrease */
    /*   memset(Qm + i, curr, s); */
    /* } else */ 
    {
      for (j=0; j<s; j++)      {
	Qm[i+j] = curr;
      }
    }
    
    i += s;
  }
  
    
  //PrintArrayd(Dy, n);
  //PrintArrayi(Qm, n);
  return curr + 1;
}


/* /\* GetClumpsPartition */
/*  * Returns the map P: D -> {0, ...,k-1}. */
/*  * */
/*  * Parameters */
/*  *   Dx (IN) : x-data sorted in increasing order */
/*  *   n (IN) : length of Dx */
/*  *   Qm (IN) : the map Q computed by EquipartitionYAxis sorted in increasing */
/*  *        order by Dx-values. */
/*  *   Pm (OUT) : the map P. Pm must be a preallocated vector of size n. */
/*  * Return */
/*  *   k : number of clumps in Pm. */
/*  *\/ */
/* static int GetClumpsPartition(t_float *Dx, int n, t_sparseVec *Qm) { */
/* 	int i, j, s, c, flag,cstart; */
/* 	int preQ; */
/* 	//t_sparseVec *Qm; */

/* 	// FIXME: update our "mine.c" to make an 'alternative' call to this funciton. */
	
/* 	i = 0; */
/* 	c = 0; */
/* 	s=0; */
/* 	flag=0; */
/* 	cstart=0; */

/* 	// FIXME: try to re-order ["elow] if-stencnes .... wrt. the 'most likely case' ... and if the results increase perofmrance ... then update our "mine.c" */

/* 	while (i < n)//排序后数据序列中相同值的数中至少有一个如果没有划分到一个格子中，则相同的数重新划分到一个跟现有所有格子不同的新格子中 */
/* 	{ */
		
/* 		if (i==0) */
/* 		{ */
/* 			preQ=Qm[i]; */
/* 			Qm[i]=c; */
/* 		}else{ */
/* 			if (Dx[i]==Dx[i-1]) */
/* 			{ */
/* 				if (flag==1) */
/* 				{ */
/* 					preQ=Qm[i]; */
/* 					Qm[i]=Qm[i-1]; */
/* 				}else if (preQ!=Qm[i]) */
/* 				{ */
/* 					flag=1; */
/* 					preQ=Qm[i]; */
/* 					if (cstart!=s) */
/* 					{ */
/* 						c++; */
/* 						cstart=s; */
                        
/* 					} */
/* 					for (j=s;j<=i;j++) */
/* 					{ */
/* 						Qm[j]=c; */
/* 					} */
/* 				} */
/* 				if (!flag)//如果本来就是新格子 */
/* 				{ */
/* 					Qm[i]=Qm[i-1]; */
/* 				} */
/* 			}else{ */
/* 				s=i; */
/* 				if (flag==1) */
/* 				{ */
/* 					//复原 */
/* 					c++; */
/* 					preQ=Qm[i]; */
/* 					Qm[i]=c; */
/* 					cstart=i; */
/* 					flag=0; */
/* 				}else{ */
/* 					if (Qm[i]==preQ) */
/* 					{ */
/* 						preQ=Qm[i]; */
/* 						Qm[i]=Qm[i-1]; */
/* 					}else{ */
/* 						preQ=Qm[i]; */
/* 						c++; */
/* 						Qm[i]=c; */
/* 						cstart=i; */
/* 					} */
/* 				} */
				
/* 			} */
/* 		} */
/* 		i++; */
        
/* 	} */
	
	
/* 	return c+1; */
/* } */


/* /\* GetSuperclumpsPartition */
/*  * Returns the map P: D -> {0, ...,k-1}. */
/*  * */
/*  * Parameters */
/*  *   Dx (IN) : x-data sorted in increasing order */
/*  *   n (IN) : length of Dx */
/*  *   Qm (IN) : the map Q computed by EquipartitionYAxis sorted */
/*  *       in increasing order by Dx-values. */
/*  *   k_hat (IN) : maximum number of clumps */
/*  *       Pm (IN): the map P. Pm must be a preallocated vector */
/*  *       of size n. */
/*  * Return */
/*  *   k : number of clumps in Pm. */
/*  *\/ */
/* static int GetSuperclumpsPartition(t_float *Dx, int n, t_sparseVec *Qm, int k_hat) { */
/*   int i, k, p; */
/*   t_float *Dp; */
/*   //Qm */
/*   /\* compute clumps *\/ */
/*   k = GetClumpsPartition(Dx, n, Qm); */
  
/*   if (k > k_hat) { /\* superclumps *\/ */
/*       const t_float default_value_float = 0; */
/*       Dp = allocate_1d_list_float(n, default_value_float); //       //Dp = (t_float *) malloc (n * sizeof(t_float)); */
/*       for (i=0; i<n; i++) */
/* 	Dp[i] = (t_float) Qm[i]; */
/*       p = EquipartitionYAxis(Dp, n, k_hat, Qm); */
/*       free(Dp); */
/*       return p; */
/*     } */
/*   else */
/*     return k; */
/* } */


/* ApproxOptimizeXAxis
 * Returns the map P: D -> {0, ...,k-1}. See Algorithm 2 in SOM.
 *
 * Parameters
 *   Dx (IN) : x-data sorted in increasing order by Dx-values
 *   Dy (IN) : y-data sorted in increasing order by Dx-values
 *   n (IN) : length of Dx and Dy
 *   Qm (IN) : the map Q computed by EquipartitionYAxis sorted
 *   in increasing order by Dx-values.
 *   q (IN) : number of clumps in Qm
 *   Pm (IN) : the map P computed by GetSuperclumpsPartition
 *   sorted in increasing order by Dx-values.
 *   p (IN) : number of clumps in Pm
 *   x (IN) : grid size on x-values
 *   I (OUT) : the normalized mutual information vector. It
 *   will contain I_{k,2}, ..., I_{k, x}. I must be a
 *   preallocated array of dimension x-1.
 */
int ApproxOptimizeXAxis(t_float *Dx, t_float *Dy, int n, t_sparseVec_counts *Qm, int q, t_sparseVec_counts *Pm, int p, int x, t_float *I)
{

  // FIXME: xmt from the funciton-calls this funct is equal "OptimizeXAxis(..)" in 'mine' ... ie, copy-paste

	int i, j, s, t, L;
	t_sparseVec **N; /* contains the number of samples for each cell Q x P*/
	t_sparseVec *np; /* contains the number of samples for each cell P */
	t_sparseVec *c; /* contains c_1, ..., c_k */
	t_float **IM; /* mutual information matrix, I_t,l */
	t_float f, fmax, r1, r2;
	t_float hq;
	t_float **hpq2;
    
    
	/* if p==1 return I=0 */
	if (p == 1) {
	  for (i=0; i<x-1; i++)
	    I[i] = 0.0;
	  return 0;
	}
	
	const t_sparseVec_counts default_value_sparseVec = 0;
	if(isTo_use_continousSTripsOf_memory()) {
	  N = allocate_2d_list_sparseVec(q, p, default_value_sparseVec);
	} else {
	  /* alloc the N matrix */
	  N = (t_sparseVec **) malloc (q * sizeof(t_sparseVec *));
	  for (i=0; i<q; i++) {
	      N[i] = (t_sparseVec *) malloc (p * sizeof(t_sparseVec_counts));
	      // FIXME: is ["elow] correct ? <-- write a correctness-test in our "assert_intri.cxx" <-- if so, then cosnider to update our "def_memAlloc.h" using "memset(..)".
	      memset(N[i],0,p * sizeof(t_sparseVec_counts));
	      //for (j=0; j<p; j++)
	      //N[i][j] = 0;
	    }
	}
    
	/* alloc the np vector */
	np = allocate_1d_list_sparseVec(p, default_value_sparseVec); //(t_sparseVec_counts *) malloc (p * sizeof(t_sparseVec));
	  // FIXME: is ["elow] correct ? <-- write a correctness-test in our "assert_intri.cxx"
	//memset(np,0,p * sizeof(t_sparseVec));
	//for (j=0; j<p; j++)
	//	np[j] = 0;
    
	/* fill N and np */
	for (i=0; i<n; i++)
	{
	  N[(int)Qm[i]][(int)Pm[i]] += 1;
	  np[(int)Pm[i]] += 1;
	}
    
	/* compute c_1, ..., c_k */
	c = allocate_1d_list_sparseVec(p, default_value_sparseVec); // c = (t_sparseVec *) malloc (p * sizeof(t_sparseVec));
	c[0] = np[0];

	/* alloc the IM matrix */
	if(isTo_use_continousSTripsOf_memory()) {
	  const int x_plussOne = x+1; const int p_plussOne = p+1;
	  const t_float default_value_float = 0;
	  IM = allocate_2d_list_float(p_plussOne, x_plussOne, default_value_float);
	  hpq2 = allocate_2d_list_float(p_plussOne, p_plussOne, default_value_float);
	} else {
	  IM = (t_float **) malloc ((p+1) * sizeof(t_float *));
	  /* precomputed H(<c_s, c_t>, Q) matrix */
	  hpq2 = (t_float **) malloc ((p+1) * sizeof(t_float *));

	  for (i=0; i<=p; i++) {
	    /* if ( (i!=0) && (i!=p) ) { */
	    /* 	c[i] = np[i] + c[i-1];//累加序列 */
	    /* } */
	    IM[i] = (t_float *) malloc ((x+1) * sizeof(t_float));
	    for (j=0; j<=x; j++)
	      IM[i][j] = 0.0;
	    hpq2[i] = (t_float *) malloc ((p+1) * sizeof(t_float));
	    /*for (j=0; j<=p; j++)
	      hpq2[i][j] = 0.0;*/
	  }
	}    

	for (i=0; i<=p; i++) {
	  if ( (i!=0) && (i!=p) ) {
	    c[i] = np[i] + c[i-1];//累加序列
	  }
	}
	/* compute H(Q)*/
	hq = HQ(N, q, p, n);
    
	/* Find the optimal partitions of size 2 */
	/* Algorithm 2 in SOM, lines 4-8 */
	for (t=2; t<=p; t++) {

	  fmax = mineAccuracy_threshold_minValue_float;
	  for (s=1; s<=t; s++) {
	    // FIXME: write an 'intri' permutation. <-- ie, wrt. the caller 'of this'.
	    f = HP3(np, p, s, t) - HPQ3(N, np, q, p, s, t);
	    if (f > fmax) {
	      IM[t][2] = hq + f;
	      fmax = f;
	    }
	  }
	}
    
	
	for (t=3; t<=p; t++) {
	  for (s=2; s<=t; s++) {
	    hpq2[s][t] = HPQ2(N, np, q, p, s, t);
	  }
	}
	/* inductively build the rest of the table of optimal partitions */
	/* Algorithm 2 in SOM, lines 11-17 */
	for (L=3; L<=x; L++)
	  for (t=L; t<=p; t++)  {
	    const t_float ct = (t_float) c[t-1]; const t_float ct_inv = (ct != 0) ? (1.0 / ct) : 0;
	    
	    fmax = mineAccuracy_threshold_minValue_float;
	    for (s=L-1; s<=t; s++) {
#if optmize_use_multInsteadOf_div == 1
	      const int l = L; //! ie, to 'reflect' the expectiaotns in our "__compute_function_F(..)" macro.
	      const t_float F_local = __compute_function_F(c, IM, hpq2, t, s, ct, ct_inv, hq);	
#else
	      r1 = (t_float) c[s-1] / (t_float) c[t-1];
	      r2 = (t_float) (c[t-1] - c[s-1]) / (t_float) c[t-1];
	      const t_float F_local = (r1 * (IM[s][L-1] - hq)) - (r2 * hpq2[s][t]);
#endif		  
	      if (F_local > fmax) {
		IM[t][L] = hq + F_local;
		fmax = F_local;
	      }
	    }
	  }
	
    /* Algorithm 2 in SOM, line 19 */

	// FIXME: incldue ["elow] if-stence in our code.

    if (x > p)
    {
        for (i=p+1; i<=x; i++)
            IM[p][i] = IM[p][p];
    }
    
    /* fill I */
    for (i=2; i<=x; i++)
        I[i-2] = IM[p][i] / MIN(log(i), log(q));
    
    if(isTo_use_continousSTripsOf_memory()) {
      free(N[0]);
    } else {
      /* free */
      for (i=0; i<q; i++)
	free(N[i]);
    }
    free(N);
    free(np);
    free(c);
    if(isTo_use_continousSTripsOf_memory()) {
      free(IM[0]);     free(hpq2[0]);
    } else {
      for (i=0; i<=p; i++)
	{
	  free(IM[i]);
	  free(hpq2[i]);
	}
    }
    free(IM);
    free(hpq2);
    
  return 0;
}

