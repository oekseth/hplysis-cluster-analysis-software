
/**
   @brief provide logics for comptuating the "disrectScore" correlation-metrics, a correlation-group formualted/described by oekseth (oekseth, 06. des. 2016).
   @remarks we assume that the provided matrix is an adjcency-matrix (oekseth, 06. des. 2016)
**/

#if( ( __stub_metricGroup__allAgainstAll == 1)  )
//|| (__stub_metricGroup__oneToMany == 1)
if(nrows_1 != nrows_2) {
  fprintf(stderr, "!!\t Ivneistgate the correctness of your call: we expected the athe \"directScore\" metric-group are only to be sued for an ajdcencty-matrix: as your inpudat-matrix is set to the dimeinos [%u, %u] we assume the latter is not the case: if the latter asseriton is wrong then we suggest you updating your API calls. For quesitons please contact the senionr delveoper [oekseth@gmail.com]. Observaitoni at [%s]:%s:%d\n", nrows_1, nrows_2, __FUNCTION__, __FILE__, __LINE__);
 }
#endif
#ifndef __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse
#warning "!!\t You have Not defined the metric to be used: please choose a metric listed in our 'metricMacro_directScore.h'. To get an example-use-case please see our 'kt_distance__stub__metricGroup__directScore__main.c' (oekseth, 06. des. 2016)" 
#endif




#if( __stub_metricGroup__allAgainstAll == 1)
//! Then we assume an all-against-all-computation:
for(uint row_id = 0; row_id < nrows_1; row_id++) {
#endif
#if( ( __stub_metricGroup__allAgainstAll == 1) || (__stub_metricGroup__oneToMany == 1) )
  for(uint col_id = 0; col_id < nrows_2; col_id++) {
#endif

    if(
       //! start: ----------------- invesitgate if the result is of interest:  ---------
#if(__stub_metricGroup__maskType == 0)
       true //! ie, as we then assume all metrics are Not set to T_FLOAT_MAX
#elif(__stub_metricGroup__maskType == 1) //! then we assume that 'implcit masks are used':
       //! start::implicitc--masks-----------------------------------------------        
#if(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_direct) //! then we compare the 'direct methoids':
       //! start:metric-----------------------------------------------
       isOf_interest(data_1[row_id][col_id]) && isOf_interest(data_2[row_id][col_id])
       //! finish:metric-----------------------------------------------
#else 
       //! start:metric-----------------------------------------------
       isOf_interest(data_1[row_id][col_id]) && isOf_interest(data_2[row_id][col_id])
       && isOf_interest(data_1[col_id][row_id]) && isOf_interest(data_2[col_id][row_id]) //! ie, 'inverse'
       //! finish:metric-----------------------------------------------
#endif
       //! finish::implicitc--masks----------------------------------------------- 
       //! -----------------------------------------------------------------------
#elif(__stub_metricGroup__maskType == 2) //! then we assume taht 'explcit masks are used':
       //! start::direct--masks----------------------------------------------- 
#if(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_direct) //! then we compare the 'direct methoids':
       //! start:metric-----------------------------------------------
       mask1[row_id][col_id]   && mask2[row_id][col_id]
       //! finish:metric-----------------------------------------------
#else 
       //! start:metric-----------------------------------------------
       mask1[row_id][col_id]    && mask2[row_id][col_id]
       && mask1[col_id][row_id] && mask2[col_id][row_id] //! ie, 'inverse'
       //! finish:metric-----------------------------------------------
#endif
       //! -----------------------------------------------------------------------
       //! finish::direct--masks----------------------------------------------- 
#else
#error "Investigate this case"       
#endif
       //! finish: ----------------- invesitgate if the result is of interest:  ---------
       ) {

      //printf("\t\t __stub_metricGroup__maskType=%u, at %s:%d\n", __stub_metricGroup__maskType, __FILE__, __LINE__);
      
      assert_possibleOverhead(data_1[row_id][col_id] != T_FLOAT_MAX);
      assert_possibleOverhead(data_2[row_id][col_id] != T_FLOAT_MAX);
      
      //! start: ----------------- idneitfy the groups: specify the macros to be used: ------------------
#if(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_direct) //! then we compare the 'direct methoids':
      //! ----------------------------------------------- 
      t_float score_1 = data_1[row_id][col_id];
      t_float score_2 = data_2[row_id][col_id];
      //! ----------------------------------------------- 
      //printf("\t (before-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
#elif(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_mergeMatrices__sum) //! then we compare the 'partly-symmetric methods' wrt.: head-and-tailsSeperately, using "sum"
      t_float score_1 = data_1[row_id][col_id] + data_2[row_id][col_id];
      t_float score_2 = data_1[col_id][row_id] + data_2[col_id][row_id];
      //printf("\t (before-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
#elif(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_mergeMatrices__mul) //! then we compare the 'partly-symmetric methods' wrt.: head-and-tailsSeperately, using "mul"
      t_float score_1 = data_1[row_id][col_id] * data_2[row_id][col_id];
      t_float score_2 = data_1[col_id][row_id] * data_2[col_id][row_id];
      // printf("\t (before-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
#elif(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_mergeMatrices__avg) //! then we compare the 'partly-symmetric methods' wrt.: head-and-tailsSeperately, using "avg"
      t_float score_1 = 0.5*(data_1[row_id][col_id] + data_2[row_id][col_id]);
      t_float score_2 = 0.5*(data_1[col_id][row_id] + data_2[col_id][row_id]);
      //printf("\t (before-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
#elif(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_symmetricProperty__sum) //! then we compare the 'explicit-symmetric methods' wrt.: 'summation of the sores before comparison', using "sum"
      t_float score_1 = data_1[row_id][col_id] + data_1[col_id][row_id];
      t_float score_2 = data_2[row_id][col_id] + data_2[col_id][row_id];
      //printf("\t (before-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
#elif(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_symmetricProperty__mul) //! then we compare the 'explicit-symmetric methods' wrt.: 'summation of the sores before comparison', using "mul"
      t_float score_1 = data_1[row_id][col_id] * data_1[col_id][row_id];
      t_float score_2 = data_2[row_id][col_id] * data_2[col_id][row_id];
      //printf("\t (before-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
#elif(__stub_metricGroup__scoreFetchTypeIndex == __macro__e_kt_correlationFunction_groupClass__directScore_symmetricProperty__avg) //! then we compare the 'explicit-symmetric methods' wrt.: 'summation of the sores before comparison', using "avg"
      t_float score_1 = 0.5*(data_1[row_id][col_id] * data_1[col_id][row_id]);
      t_float score_2 = 0.5*(data_2[row_id][col_id] * data_2[col_id][row_id]);
      //printf("\t (before-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
#else
#error "!!\t Investigate this case"
#endif
      //! finish: ----------------- idneitfy the groups: ---------

      //! Apply the weight-table to the score(s):
#if(__stub_metricGroup__useWeights == 1)
      // printf("(before-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
      assert_possibleOverhead(weight[row_id] != T_FLOAT_MAX);
      assert_possibleOverhead(weight[col_id] != T_FLOAT_MAX);
      score_1 *= weight[row_id];   score_2 *= weight[col_id]; 
      //printf("(after-weight) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
#endif

      //! 
      //! Apply the metic-comptaution:      
      //printf("(before-macro) ops(%f, %f), and input=(%f, %f), at %s:%d\n", score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
      const t_float score = __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse(score_1, score_2);
      //printf("score=%f given ops(%f, %f), and input=(%f, %f), at %s:%d\n", score, score_1, score_2, data_2[row_id][col_id], data_2[col_id][row_id], __FILE__, __LINE__);
      //! Then update the score:
#if( __stub_metricGroup__allAgainstAll == 1)
      matrix_result[row_id][col_id] = score;
#elif  (__stub_metricGroup__oneToMany == 1)
      list_result[col_id] = score;
#else
      ret_val = score;
#endif
    }
#if( ( __stub_metricGroup__allAgainstAll == 1) || (__stub_metricGroup__oneToMany == 1) ) 
  }
#endif

#if( __stub_metricGroup__allAgainstAll == 1)
 }
#endif


#undef __stub_metricGroup__scoreFetchType__typeOf_innerMetricToUse
#undef __stub_metricGroup__useWeights
