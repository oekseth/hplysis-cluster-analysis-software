#ifndef kt_api__centerPoints_h
#define kt_api__centerPoints_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_api__centerPoints
   @brief a subset of our "kt_api.h" which provide lgocis to comptue centroids of matricers (oekseth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "e_kt_correlationFunction.h"
// FIXME: make use of [”elow] ... ie, provide access/support for computign the 'eigen-vector-centrliaty'
#include "e_kt_eigenVectorCentrality.h"
#include "kt_centrality.h"
#include "kt_clusterAlg_hca__node.h"
#include "kt_matrix.h"
#include "e_kt_clusterComparison.h"
#include "kt_clusterAlg_fixed_resultObject.h"
#include "kt_clusterAlg_hca_resultObject.h"
#include "kt_clusterAlg_SOM_resultObject.h"
#include "e_kt_clusterAlg_hca_type.h"
#include "kt_clusterAlg_fixed__alg__miniBatch.h"


/**
   @brief assicated the centriods (eg, from clustering) to the vertices.
   @remarks a wrapper-method to compute/calculcate the cluster centroids, given to which cluster each element belongs. Depending on the argument method, the centroid is defined as either the mean or the median for each dimension over all elements belonging to a cluster.
   @remarks a subset of the parameters are described below:
   # "cdata":  Result: this array contains the cluster centroids.
   # "cmask": Result: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing.
   # "method": Input: 
   - For method=='a', the centroid is defined as the mean over all elements belonging to a cluster for each dimension.
   - For method=='m', the centroid is defined as the median over all elements belonging to a cluster for each dimension.
 **/
int getclustercentroids(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, const uint transpose, const char method);
/**
   @brief assicated the centriods (eg, from clustering) to the vertices.
   @param <obj_1> is the data-matrix to evalaute
   @param <obj_result_clusterId> hold the set of mappings between row-IDs and cluster-IDs 
   @param <obj_result_clusterId_columnValue> a 2d-mapping between the cluster-ids and the column-ids, where value is either the "mean" (or: average) or the "median", ie, depending upon the isTo_useMean inptu-parameter.
   @param <nclusters> is the number of clusters to evaluate
   @param <isTo_transposeMatrix> which is set to true if the matrix-object is to be transposed
   @param <isTo_useMean> which if set to flase implies that we use the "median" (ie, the mid-point), which wrt. exeuciton-time results 'in an increased exeuciton-tiem for the same data-set wehn compared to the mean (or: average) option.
   @return true if the operation is assumed to have been a success.
   @remarks 
   - the cluster-ids may be accessed through the "get_row_member__s_kt_matrix(...)" function, a function defined in out "kt_matrix.h" header-file.
   - if the cluster-id or column-vlaues are Not of itnerest the set the 'return-object in quesiton' to NULL
 **/
bool kt_matrix__getclustercentroids(const s_kt_matrix_t *obj_1, s_kt_matrix_t *obj_result_clusterId, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint nclusters, const bool isTo_transposeMatrix, const bool isTo_useMean);


#include "e_kt_api_dynamicKMeans.h" //! whioch includes the "e_kt_api_dynamicKMeans_t" enum used in [”elow] (eosekth, 60. feb. 2017).

/**
   @brief assicated the centriods (eg, from clustering) to the vertices (oekseth, 06. feb. 2017).
   @param <obj_1> is the data-matrix to evalaute
   @param <mapOf_vertexToCluster> hold the set of mappings between row-IDs and cluster-IDs 
   @param <obj_result_clusterToVertex> a 2d-mapping between the cluster-ids and the column-ids, where value is either the "mean" (or: average) or the "median", ie, depending upon the isTo_useMean inptu-parameter.
   @param <nclusters> is the number of clusters to evaluate
   @param <typeOf_centroidAlg> which idnetifeis the type of centrodi-algorithm to be used when 'idneitfying' the centrodis for the cluster-memberships: defined in our "e_kt_api_dynamicKMeans.h" for "avg", "rank", "medoid".
   @return true if the operation is assumed to have been a success.
   @remarks 
   - the cluster-ids may be accessed through the "get_row_member__s_kt_matrix(...)" function, a function defined in out "kt_matrix.h" header-file.
   - if the cluster-id or column-vlaues are Not of itnerest the set the 'return-object in quesiton' to NULL
 **/
bool kt_matrix__centroids(const s_kt_matrix_t *obj_1, uint *mapOf_vertexToCluster, s_kt_matrix_t *obj_result_clusterToVertex, const uint nclusters, const e_kt_api_dynamicKMeans_t typeOf_centroidAlg);


  /**
     @brief identify the center-vertex fo each cluster: calculates the cluster centroids, given to which cluster each element belongs.
     @remarks The  The centroid is defined as the mean over all elements for each dimension.
     @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
     @remarks a subset of the parameters are described below:
     # mask:       (input) int[nrows][ncolumns] This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
     # clusterid: The cluster number to which each element belongs. Iftranspose == 0, then the dimension of clusterid is equal to nrows (the number of genes). Otherwise, it is equal to ncolumns (the number of microarrays).
     # cdata: used 'in funciton-return': updated with the cluster centroids.
     # cmask: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid i fall corresponding data values of the cluster members are missing.
  **/
void getclustermeans(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, float **cmask_tmp,  uint transpose);
  /**
     @brief idetnfy the median scores/weights of each cluster
     @remarks Calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the median over all elements for each dimension.
     @remarks a subset of the parameters are described below:
     # mask:       (input) int[nrows][ncolumns] This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
     # clusterid: The cluster number to which each element belongs. Iftranspose == 0, then the dimension of clusterid is equal to nrows (the number of genes). Otherwise, it is equal to ncolumns (the number of microarrays).
     # cdata: used 'in funciton-return': updated with the cluster centroids.
     # cmask: an array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing.
  **/
void getclustermedians(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, float **cmask_tmp, uint transpose);
/**
   @brief assicated the centriods (eg, from clustering) to the vertices: use "mean" (or: average)
   @param <obj_1> is the data-matrix to evalaute
   @param <obj_result_clusterId> hold the set of mappings between row-IDs and cluster-IDs 
   @param <obj_result_clusterId_columnValue> a 2d-mapping between the cluster-ids and the column-ids, where value is either the "mean" (or: average) or the "median", ie, depending upon the isTo_useMean inptu-parameter.
   @param <nclusters> is the number of clusters to evaluate
   @param <isTo_transposeMatrix> which is set to true if the matrix-object is to be transposed
   @return true if the operation is assumed to have been a success.
   @remarks 
   - the cluster-ids may be accessed through the "get_row_member__s_kt_matrix(...)" function, a function defined in out "kt_matrix.h" header-file.
   - if the cluster-id or column-vlaues are Not of itnerest the set the 'return-object in quesiton' to NULL
 **/
bool kt_matrix__getclustermeans(const s_kt_matrix_t *obj_1, s_kt_matrix_t *obj_result_clusterId, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint nclusters, const bool isTo_transposeMatrix);
/**
   @brief assicated the centriods (eg, from clustering) to the vertices: use "Median"
   @param <obj_1> is the data-matrix to evalaute
   @param <obj_result_clusterId> hold the set of mappings between row-IDs and cluster-IDs 
   @param <obj_result_clusterId_columnValue> a 2d-mapping between the cluster-ids and the column-ids, where value is either the "mean" (or: average) or the "median", ie, depending upon the isTo_useMean inptu-parameter.
   @param <nclusters> is the number of clusters to evaluate
   @param <isTo_transposeMatrix> which is set to true if the matrix-object is to be transposed
   @return true if the operation is assumed to have been a success.
   @remarks 
   - the cluster-ids may be accessed through the "get_row_member__s_kt_matrix(...)" function, a function defined in out "kt_matrix.h" header-file.
   - if the cluster-id or column-vlaues are Not of itnerest the set the 'return-object in quesiton' to NULL
**/
bool kt_matrix__getclustermedians(const s_kt_matrix_t *obj_1, s_kt_matrix_t *obj_result_clusterId, s_kt_matrix_t *obj_result_clusterId_columnValue, const uint nclusters, const bool isTo_transposeMatrix);


/*   /\** */
/*      @brief identify the cluster centroids. */
/*      @remarks a wrapper-method to compute/calculcate the cluster centroids, given to which cluster each element belongs. Depending on the argument method, the centroid is defined as either the mean or the median for each dimension over all elements belonging to a cluster. */
/*      @remarks a subset of the parameters are described below: */
/*      # "cdata":  Result: this array contains the cluster centroids. */
/*      # "cmask": Result: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing. */
/*      # "method": Input:  */
/*      - For method=='a', the centroid is defined as the mean over all elements belonging to a cluster for each dimension. */
/*      - For method=='m', the centroid is defined as the median over all elements belonging to a cluster for each dimension. */
/*   **\/ */
/* int getclustercentroids(const uint nclusters, const uint nrows, const uint ncolumns, float** data, char** mask, uint clusterid[], float** cdata, char **cmask, float **cmask_tmp, uint transpose, const char method); */
  /**
     @brief idnetify the centroids of each cluster.
     @param <nclusters> The number of clusters.
     @param <nelements> The total number of elements.
     @param <distance> Number of rows is nelements, number of columns is equal to the row number: the distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
     @param <clusterid> The cluster number to which each element belongs.
     @param <centroids> The index of the element that functions as the centroid for each cluster.
     @param <errors> The within-cluster sum of distances between the items and the cluster centroid.
     @remarks 
     - idea: identify the sum of distances to all members in a given cluster: for each clsuter 'allcoate' the centorid which has the minimum/lowest 'sum of distance to other members' (in the same cluster).
     - this function calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the element with the smallest sum of distances to the other elements.
  **/
void getclustermedoids(const uint nclusters, const uint nelements, float** distance, uint clusterid[], uint centroids[], float errors[]);
/**
   @brief idnetify the cenotrids (and assicated errors) of each cluster.
   @param <obj_1> hold the inptu-data
   @param <obj_result> hold the result-data, eg, wrt. cluster-memberships and accuracy--error of each cluster: expected Not to be intaliseed (though Not set to NULL);
   @param <nclusters> is the number of clusters to sub-divide the data-set into.
   @return true upon success.
**/
bool kt_matrix__getclustermedoids(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const uint nclusters);

#endif //! eof
