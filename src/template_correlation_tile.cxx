#include "config_nonRank_each.h"
#include "config_nonRank_oneToMany.h"
#include "config_nonRank_manyToMany.h"
#include "aux_findPartitionFor_tiles.h"

//#include "e_template_correlation_tile.h"

/**
   @brief a tempalte to build tile-functions (oekseth, 06. sept. 2016)
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC> is the name of the all-agianst-all-tile function to construct/generate
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows> is the name fot eh funiocnt which is sued to comapre two rows
   @tparam<TEMPLATE_distanceConfiguration_useWeights> specificy if we are to comptue weights: set to "0" or "1", eg,: #define TEMPLATE_distanceConfiguration_useWeights 1
   @tparam<TEMPLATE_distanceConfiguration_macroFor_metric> which identifies the macro to be used wrt. distance-computation.
   @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_SSE> which is the SSE 'permtuation' of the "TEMPLATE_distanceConfiguration_macroFor_metric" macro-configuraiton-parameter.
   @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_includePower> which if set to "1" implies that we make use of an andidtioanlø 'power' argumetn when calling the distance-function.
   @tparam<TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax> which is used to speerate between the case where we (a) "are itnersted in the sum of valeus" VS (b) are only itnerested in the amx-value
   @tparam<TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin> which is used to speerate between the case where we (a) "are itnersted in the sum of valeus" VS (b) are only itnerested in the min-value
   @tparam<TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate> which refers to teh "e_template_correlation_tile_typeOf_distanceUpdate" enum and is used to simplify the comptaution of partial sets wrt. SSE-computaiton: relates to "s_template_correlation_tile_temporaryResult_SSE", "s_template_correlation_tile_temporaryResult" and "s_kt_computeTile_subResults_t".
   @tparam<TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices> which if set to "1" is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
   @tparam<TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores> which is used wrt. descideing if the weight is to be copmtued either before or after the comptatuion, ie, an optmizaiton-appraoch to reduce the time-compelxity of dsitnace-metrics such as "Euclid" and "Mahalanobis"
   @tparam<TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction> which is used to specify the post-comptuation-functions to be used
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany> which is the name of the function used to store a 'one-to-many' distance-reasult compuation-result
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany> which is the name of the function used to store a 'many-to-many' distance-reasult compuation-result
   @tparam<>
   @tparam<>
 **/

/* definition to expand macro then apply to pragma message */
// #define VALUE_TO_STRING(x) #x
// #define VALUE(x) VALUE_TO_STRING(x)
// #define VAR_NAME_VALUE(var) #var "="  VALUE(var)

// #ifndef globalSpec_templateFunctionArguments__nonRankCorrelation__each
// #error "!!\t We expected the stanrdicese fucntion-parameter-configruation to have been set, which is not the case, ie, please udpate your call"
// #endif
//! -------------------------------------------
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! -------
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! -------
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! -------
// #ifndef TEMPLATE_NAMEOF_MAIN_FUNC__notTransposed_maskExplicit
// #error "Error: tempalte-parameter not set, ie, please udpate your calls"
// #endif 
//! -------
#ifndef TEMPLATE_distanceConfiguration_useWeights
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! -------
#ifndef TEMPLATE_distanceConfiguration_macroFor_metric
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! -------
#ifndef TEMPLATE_distanceConfiguration_macroFor_metric_SSE
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_distanceConfiguration_macroFor_metric_includePower
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! --------------
#ifndef TEMPLATE_iterationConfiguration_typeOf_mask //! an enum expected to be defined through our "e_template_correlation_tile_maskType" ennum in our "e_template_correlation_tile.h"
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#else
//! start: test value-range ----- 
#if(TEMPLATE_iterationConfiguration_typeOf_mask == 3) //e_template_correlation_tile_maskType_mask_undef) 
// #pragma message("Error: enum-property not proeprtly set" VAR_NAME_VALUE(TEMPLATE_iterationConfiguration_typeOf_mask))
#error "Error: the macro-parameter was set to 'undef', ie, please update your API-calls"
#endif
#endif
//! ------------
#ifndef TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax
#error "Error: the macro-parameter was set to 'undef', ie, please update your API-calls"
#endif
//! ------------
#ifndef TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin
#error "Error: the macro-parameter was set to 'undef', ie, please update your API-calls"
#endif
//! ---
#ifndef TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! end: test value-range ----- 


//! ***********************************************************************
//! start: ---- internal variables: ------
//! @brief: Speicfy a set of itnernal inptus to simplify the reading and validatioin of below mc-macro-logics (oekseth, 06. sept. 2016)
//! ---
#if( (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 1) && (TEMPLATE_iterationConfiguration_typeOf_mask != 0/* = e_template_correlation_tile_maskType_mask_none*/) )
#define __templateInternalVariable__isTo_updateCountOf_vertices 1
//#elif(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) 
#else //! then we assume that all columsn are of interst, ie, then no point in updating the count, ie, an opmiziation-strategy.
#define __templateInternalVariable__isTo_updateCountOf_vertices 0
#endif
// #define __templateInternalVariable__
//! ---
//! end: ---- internal variables: ------
//! ***********************************************************************


static void TEMPLATE_NAMEOF_MAIN_FUNC(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const uint SM, char **mask1, char**mask2, t_float **listOf_inlineResults, const s_allAgainstAll_config_t self, s_kt_computeTile_subResults_t *complexResult) {
//static void TEMPLATE_NAMEOF_MAIN_FUNC(const uint ncols, const uint index1, const uint index2, const uint chunkSize_index1, const uint numberOf_chunks_cols, const uint chunkSize_index2, const uint chunkSize_col_last, t_float **data1, t_float **data2, const t_float weight[], t_float **resultMatrix, const uint SM, char **mask1, char**mask2, t_float **listOf_inlineResults, const s_kt_correlationConfig self, s_kt_computeTile_subResults_t *complexResult) {
  //t_float *__restrict__ rres_startPos) {

  // s_aux_findPartitionFor_tiles_t tile_col = initAndGet__s_aux_findPartitionFor_tiles_t(ncols, SM);
  // assert(tile_col.

#define __localConfig__templateTile__rowSize_isKnown 0
#include "func_template__tile.c" //! ie, compute for teh tile.
#undef __localConfig__templateTile__rowSize_isKnown
}

//!*************************************************************************
static t_float TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows(const config_nonRank_each_t row_config) {
  const uint ncols = row_config.ncols;
  const t_float *__restrict__ rmul1 = row_config.rmul1;
  const t_float *__restrict__ rmul2 = row_config.rmul2;
  const t_float *__restrict__ weight = row_config.weight;
  const char *__restrict__ mask1 = row_config.mask1;
  const char *__restrict__ mask2 = row_config.mask2;
  const s_allAgainstAll_config_t self = row_config.config;  

  //!
  //! Investigate if we are to set the min-valeu or '0', ie, if (a) teh count' or (b) teh min-value is of itnerest, ie, is to be used.
#if( (__templateInternalVariable__isTo_updateCountOf_vertices == 1) && (TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) ) //! then we are to update the count of itnereesting cells.

  // assert(false); // FIXME:w rt the "isTo_findMaxValue" ... inveistae if tehre are macro-cases where 'parts of teh complex object is to be intiated to 'min' OR 'max'.

// #if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) )
//   const bool isTo_findMaxValue = false;
// #else
//   const bool isTo_findMaxValue = true;
// #endif
#endif	  

#if(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0)		
	const bool isTo_findMaxValue = false; const bool isTo_findMinValue = false;
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
	const bool isTo_findMaxValue = false; const bool isTo_findMinValue = true;
#else
	const bool isTo_findMaxValue = true; const bool isTo_findMinValue = false;
#endif


  //! ----
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
  //! Tehn we itnaite a temproary 'on-SSE' boject ot hold the results:
	s_template_correlation_tile_temporaryResult_SSE_t objLocalComplex_sse; setTo_empty__s_template_correlation_tile_temporaryResult_SSE(&objLocalComplex_sse, isTo_findMaxValue, isTo_findMinValue);	    
	s_template_correlation_tile_temporaryResult_t objLocalComplex_nonSSE; setTo_empty__s_template_correlation_tile_temporaryResult(&objLocalComplex_nonSSE, isTo_findMaxValue, isTo_findMinValue);
#elif(__templateInternalVariable__isTo_updateCountOf_vertices == 1) //! then we are to update the count of itnereesting cells.
	s_template_correlation_tile_temporaryResult_t objLocalComplex_nonSSE; setTo_empty__s_template_correlation_tile_temporaryResult(&objLocalComplex_nonSSE, isTo_findMaxValue, isTo_findMinValue);
#endif

  const uint i2 = 0; const uint k2 = 0;
  // TODO: vlaidate correnctess wrt. [”elow] 'settting' of "global_index1" and "global_index2" <-- seems like [”elow] is wrong, ie, 'udpate this' wehn we start work on imrpvoing the 'mina tiling-function' (which 'this' is wrapped inside).
  // const uint global_index1 = getRowIndex__s_kt_computeTile_subResults_t(complexResult, i2);
  // const uint global_index2 = getColumnIndex__s_kt_computeTile_subResults_t(complexResult, k2);
	

  uint chunkSize_cnt_i = ncols;
	
  //! Note: [below] 'itnri-lgoics' is similar tot eh funciton found in our "correlationType_spearman.cxx"
  uint chunkSize_cnt_i_intris = (ncols >= VECTOR_FLOAT_ITER_SIZE) ? ncols - VECTOR_FLOAT_ITER_SIZE : 0;
  if(((t_float)ncols/(t_float)VECTOR_FLOAT_ITER_SIZE) && ((t_float)VECTOR_FLOAT_ITER_SIZE)) {      
  //if(((t_float)ncols/(t_float)VECTOR_FLOAT_ITER_SIZE)*((t_float)VECTOR_FLOAT_ITER_SIZE)) {      
    chunkSize_cnt_i_intris = ncols; //! ie, as we then assume that "cnt_elements" is divisable by "VECTOR_FLOAT_ITER_SIZE"
  }
  assert(chunkSize_cnt_i_intris <= ncols); //! ie, as we otehrwise might have a 'zero' case wrt. t[ªbvoe] "uint" 'operation'


  //! ---------------------------------------------------------------
  //!                  The call:
  //! ---------------------------------------------------------------
  const uint /*column_index=*/i = 0; //! ie, the frist column to investigate
  t_float result = 0;
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) )
	result = 0;
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
	result = T_FLOAT_MAX;
#else
	result = T_FLOAT_MIN_ABS;
#endif
#endif

	//printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);
	//printf("(info)\t Comptue non-ranked correlation-matrix, at %s:%d\n", __FILE__, __LINE__);

  //! --------------------------------------
  //! Comptue for each column-index

#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit) /*ie, e_template_correlation_tile_maskType_mask_explicit */
  assert(mask1);   assert(mask2); //! ie, what we expect.
  const char *__restrict__ rmask1 = mask1; const char *__restrict__ rmask2 = mask2; 
#endif

  //result = 9.9; printf("FIXME: include [below], at %s:%d\n", __FILE__, __LINE__);  
  #define __config__template_correlation_tile_innerMostColumn__isToUpdateGlobalObject 0
#include "template_correlation_tile_innerMostColumn.c"

	// printf("\tstart:post-processing\t\t\t givenr result=%f, at [%s]:%s:%d\n", result, __FUNCTION__, __FILE__, __LINE__);

  

  //! --------------------------------------
  //! Post-process:
  t_float result_adjusted = T_FLOAT_MAX;

  
  //result_adjusted = 8; printf("FIXME: remove this at %s:%d\n", __FILE__, __LINE__);  


  //printf("FIXME: include [below], at %s:%d\n", __FILE__, __LINE__);  
  //printf("result-before-adjustment=%f, at %s:%d\n", result, __FILE__, __LINE__); //! ie, to investigate the 'complaint' about the valeu being used 'un-intalized'.
  const t_float configMetric_power = self.configMetric_power;
#include "template_correlation_tile_applyMetric_postProcessing.c"  
  

  // printf("\tcompleted:post-processing\t\t\t result_adjusted=%f, at [%s]:%s:%d\n", result_adjusted, __FUNCTION__, __FILE__, __LINE__);

  // assert(result_adjusted != T_FLOAT_MAX); //! ie, as we epxect the [ªbove] call to have udpated the variable in question.
  //! @return the result:
  return result_adjusted;
}

//!*************************************************************************


//void TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany(const uint index1, const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], const e_kt_correlationFunction_t typeOf_metric, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, s_allAgainstAll_config *config_allAgainstAll, t_float *arrOf_result) {
void TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany(const config_nonRank_oneToMany_t obj_config) {
  //! --------------------------------------------------------------
  const uint index1 = obj_config.index1;
  const uint nrows = obj_config.nrows;
  const uint ncols = obj_config.ncols;
  t_float **data1 = obj_config.data1;
  t_float **data2 = obj_config.data2;
  char** mask1  = obj_config.mask1;
  char** mask2 = obj_config.mask2;
  const t_float *weight = obj_config.weight;
  const e_kt_correlationFunction_t typeOf_metric = obj_config.typeOf_metric;
  const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation = obj_config.typeOf_metric_correlation;
  s_allAgainstAll_config_t *config_allAgainstAll = obj_config.config_allAgainstAll;
  assert_possibleOverhead(config_allAgainstAll);
  t_float *arrOf_result = obj_config.arrOf_result;
  //! --------------------------------------------------------------


#if(optimize_parallel_useNonSharedMemory_MPI == 1)
  #error "Update this code-chunk with new/updated code" 
#endif
#if(optimize_parallel_2dLoop_useFast == 1)
#error "Add and test the effoect of parllel wrappers for this ... ie, after we have completed hte 'ordinary' perofmrance-tests"
#endif

  const uint nrows_adjusted = (config_allAgainstAll->iterationIndex_2 == UINT_MAX) ? nrows : config_allAgainstAll->iterationIndex_2;

  //! Compute:
  //printf("index1=%u, at %s:%d\n", index1, __FILE__, __LINE__);
  assert_possibleOverhead(config_allAgainstAll); 
  if(mask1) {assert(mask1[index1]);}
  assert(data1[index1]);
  //  if(config_allAgainstAll->s_inlinePostProcess == NULL) 
  {  assert_possibleOverhead(arrOf_result); } 
  //assert(arrOf_result);
  // printf("iterate throug [0....%u], at %s:%d\n", nrows_adjusted, __FILE__, __LINE__);
  for(uint index2 = 0; index2 < nrows_adjusted; index2++) {
  //for(uint index2 = 0; index2 < ncols; index2++) {
    assert(data2[index2]); // FIXME: remove
    config_nonRank_each_t row_config; init__config_nonRank_each(&row_config, ncols,  data1[index1], data2[index2], weight, 
								(mask1) ? mask1[index1] : NULL,
								(mask2) ? mask2[index2] : NULL,
								*config_allAgainstAll);
    
    const t_float result_local = TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows(row_config); //! where we 'seprate' teh 'call' and the 'result' iot. simplify the 'idnieeficaiton' of memory-errors
    if(result_local != T_FLOAT_MIN) {
      arrOf_result[index2] = result_local;
    } else {arrOf_result[index2] = T_FLOAT_MAX;}  //! ie, as wew tehn assume the value 'is Not set' (oekseth, 06. jan. 2017).
    //printf("(result::[%u])\t %f, at %s:%d\n", index2, arrOf_result[index2], __FILE__, __LINE__);
  }  

  {
    //!
    //! FInalize:
    bool postProcessingNeedsTo_beEvaluated = true;
    s_allAgainstAll_config_t *self = config_allAgainstAll;
    t_float *arrOf_tmpResult = arrOf_result;
  assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = obj_config.config_allAgainstAll->configMetric_power;
#include "func_postProcessing.c"
  }
}

//! **********************************'
//! **********************************'
//! **********************************'
#ifndef __templateCorrelation_defineFuncNAme__manyToMany
#define __templateCorrelation_defineFuncNAme__internal_inner(fun,suffix) fun ## _ ## suffix
#define __templateCorrelation_defineFuncNAme__internal(fun,suffix) __templateCorrelation_defineFuncNAme__internal_inner(fun, suffix) 
#define __templateCorrelation_defineFuncNAme__manyToMany(suffix) __templateCorrelation_defineFuncNAme__internal(TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany, suffix)
#endif //! ie, as we otehrwise assuemt aht these 'fucntion-macro-wrapeprs' are defined (oekseth, 06. otk. 2016).

#include "func_template__allAgainstAll_aux.h"
#include "correlation_inCategory_delta.h"
#include "s_allAgainstAll_config.h"


#if( (configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany == 1) || (configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany__forSubset == 1) )  //! start::subset-of-specialied-functions:

//! ************************ build for complex cases: -----------------------------------------
//! start: -------------------------------------------------------------------------------------------
//! A Function which updat ehte result-matrix, ie, does not apply any 'summations' wrt. the result-set.
void __templateCorrelation_defineFuncNAme__manyToMany(_asIs)(const config_nonRank_manyToMany_t obj_config) {
  //! Get the parameters for the fucntion ... ie, 'expand' the "obj_config" function
#include "TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany__fetchParams.c"
  //!
  //! Configure:
#define __config__funcTemplateAllAgainstAll__storeIntermeidateResults 0 //  ie, as we are Not interested in 'keeping' the tmeproray results
#define __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes 0 //! ie, as we are interested in using efficent data-type rpocessing.
#define __config__funcTemplateAllAgainstAll__inlinePostProcesType macro__e_allAgainstAll_SIMD_inlinePostProcess_undef
  //!
  //! Compute:
  assert(data1); assert(data2);
  //printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);
  assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = obj_config.config_allAgainstAll->configMetric_power;
  #include "func_template__allAgainstAll.c"
}


//! Extends "_asIs" with the lgoics to 'keep' the intermediate results, a fucntion which is of importance3 when computing seperate results, resutls which are/ latter unified (eg, through use of non-shared-memory parallsiation through the MPI library) (oekseth, 06. setp. 2016).
//! @remars in this fucntion we: (a) we do Not apply post-proecessing-routiens, (b) use a 'compelte' result-matrix for tmeproary values (ie, not a compressed/local/temproary strucutre for the lattER) and (c) update/return the 'temporay/intermeidate correlation-results' (ie, for th case where the psot-procesisng of a dsitnac-emtric need more than oen varialbe);
void __templateCorrelation_defineFuncNAme__manyToMany(_asIs_MPI)(const config_nonRank_manyToMany_t obj_config) {
  //! Get the parameters for the fucntion ... ie, 'expand' the "obj_config" function
#include "TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany__fetchParams.c"
  //!
  //! Configure:
#define __config__funcTemplateAllAgainstAll__storeIntermeidateResults 1 //  ie, as we are Not interested in 'keeping' the tmeproray results
#define __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes 0 //! ie, as we are interested in using efficent data-type rpocessing.
#define __config__funcTemplateAllAgainstAll__inlinePostProcesType macro__e_allAgainstAll_SIMD_inlinePostProcess_undef
  //!
  //! Compute:
  assert(data1); assert(data2);
  assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = obj_config.config_allAgainstAll->configMetric_power;
  //printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);
  #include "func_template__allAgainstAll.c"
}
#endif //! ("configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany__forSubset") //! end::subset-of-specialied-functions.


#if(configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany == 1)

//! ************************ build for complex cases: -----------------------------------------
//! start: -------------------------------------------------------------------------------------------
//! A Function which updat ehte result-matrix, ie, does not apply any 'summations' wrt. the result-set.
//! ------

//! specfilized function of the "manyToMany" function: calcuate log-weights
//! @remarks Compute the weights of a correlation-matrix, using temporal resutls (eg, from tiling) as input.
//! @remarks for a 'simplfied verison' of the logics assicated tot his funciton, see application of our "macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(..)" macro-function.
void __templateCorrelation_defineFuncNAme__manyToMany(_calculateWeights)(const config_nonRank_manyToMany_t obj_config) {
  //! Get the parameters for the fucntion ... ie, 'expand' the "obj_config" function
#include "TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany__fetchParams.c"
  assert(data1); assert(data2);
  //!
  //  assert(false); // FIXME: include
  //! Configure:
#define __config__funcTemplateAllAgainstAll__storeIntermeidateResults 0 //  ie, as we are Not interested in 'keeping' the tmeproray results
#define __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes 0 //! ie, as we are interested in using efficent data-type rpocessing.
#define __config__funcTemplateAllAgainstAll__inlinePostProcesType macro__e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights
  //!
  //! Compute:
  assert(data1); assert(data2);
  assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = obj_config.config_allAgainstAll->configMetric_power;
  //printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);
#include "func_template__allAgainstAll.c"
}
//! specfilized function of the "manyToMany" function: calcuates min-value
void __templateCorrelation_defineFuncNAme__manyToMany(_scalar_min)(const config_nonRank_manyToMany_t obj_config) {
  //! Get the parameters for the fucntion ... ie, 'expand' the "obj_config" function
#include "TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany__fetchParams.c"


  //assert(false); // FIXME: include
  //!
  //! Configure:
#define __config__funcTemplateAllAgainstAll__storeIntermeidateResults 0 //  ie, as we are Not interested in 'keeping' the tmeproray results
#define __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes 0 //! ie, as we are interested in using efficent data-type rpocessing.
#define __config__funcTemplateAllAgainstAll__inlinePostProcesType macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min
  //!
  //! Compute:
  assert(data1); assert(data2);
  assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = obj_config.config_allAgainstAll->configMetric_power;
  // printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);
  #include "func_template__allAgainstAll.c"
}
//! specfilized function of the "manyToMany" function: calcuates max-value 
void __templateCorrelation_defineFuncNAme__manyToMany(_scalar_max)(const config_nonRank_manyToMany_t obj_config) {
  //! Get the parameters for the fucntion ... ie, 'expand' the "obj_config" function
#include "TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany__fetchParams.c"
  //  assert(false); // FIXME: include

  //!
  //! Configure:
#define __config__funcTemplateAllAgainstAll__storeIntermeidateResults 0 //  ie, as we are Not interested in 'keeping' the tmeproray results
#define __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes 0 //! ie, as we are interested in using efficent data-type rpocessing.
#define __config__funcTemplateAllAgainstAll__inlinePostProcesType macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max
  //!
  //! Compute:
  assert(data1); assert(data2);
  assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = obj_config.config_allAgainstAll->configMetric_power;
  // printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);
  #include "func_template__allAgainstAll.c"
}
//! specfilized function of the "manyToMany" function: calcuates the sum of valeus
void __templateCorrelation_defineFuncNAme__manyToMany(_scalar_sum)(const config_nonRank_manyToMany_t obj_config) {
  //! Get the parameters for the fucntion ... ie, 'expand' the "obj_config" function
#include "TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany__fetchParams.c"
  //  assert(false); // FIXME: include

  //!
  //! Configure:
#define __config__funcTemplateAllAgainstAll__storeIntermeidateResults 0 //  ie, as we are Not interested in 'keeping' the tmeproray results
#define __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes 0 //! ie, as we are interested in using efficent data-type rpocessing.
#define __config__funcTemplateAllAgainstAll__inlinePostProcesType macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum
  //!
  //! Compute:
  assert(data1); assert(data2);
  assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = obj_config.config_allAgainstAll->configMetric_power;
  // printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);
  #include "func_template__allAgainstAll.c"
}


//! ************************ build for complex cases: -----------------------------------------
//! A Function which updat ehte result-matrix, ie, does not apply any 'summations' wrt. the result-set.
//! end: -------------------------------------------------------------------------------------------
#endif //! for "if(configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany == 1)".


//! specfilized function of the "manyToMany" function: a generic fucntion, which is used (among others) to test teh 'ability' of teh otpmzied comiler to avoidign 'non-neccesary' memory-storage fucntiosn  from reducing the perfomrance, ie, wehre "_scalar_generic" is expected to result in a slwoer/lower perofmrance (okeseth, 06. otk. 2016).
void __templateCorrelation_defineFuncNAme__manyToMany(_scalar_generic)(const config_nonRank_manyToMany_t obj_config) {
  //! Get the parameters for the fucntion ... ie, 'expand' the "obj_config" function
#include "TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany__fetchParams.c"
  //  assert(false); // FIXME: include

  // printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__);

  //!
  //! Configure:
#define __config__funcTemplateAllAgainstAll__storeIntermeidateResults 0 //  ie, as we are Not interested in 'keeping' the tmeproray results
#define __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes 0 //! ie, as we are interested in using efficent data-type rpocessing.
#define __config__funcTemplateAllAgainstAll__inlinePostProcesType macro__e_allAgainstAll_SIMD_inlinePostProcess_nonOptimizedEvaluateAllCases
  //!
  //! Compute:
  assert_possibleOverhead(data1); assert_possibleOverhead(data2);
  // printf("default-value=%f, at %s:%d\n", result, __FILE__, __LINE__)
  assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = obj_config.config_allAgainstAll->configMetric_power;
#include "func_template__allAgainstAll.c"
}



void TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany(const config_nonRank_manyToMany_t obj_config) {
//void TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, s_allAgainstAll_config *config_allAgainstAll) {

  // COMPILESTATUS("build for many-to-many function")

#if(optimize_parallel_2dLoop_useFast == 1)
#error "Add and test the effoect of parllel wrappers for this ... ie, after we have completed hte 'ordinary' perofmrance-tests"
#endif
#if(optimize_parallel_useNonSharedMemory_MPI == 1)
  #error "Update this code-chunk with new/updated code" 
#endif

  //! Get the parameters for the fucntion ... ie, 'expand' the "obj_config" function
#include "TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany__fetchParams.c"


  //!
  //! Use speilized fucntions for different cases, ie, to simplify the compielrs wrok wrt. optimizaiton:

#if(configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany__forSubset == 1)
  if(config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible) { //! then we use a spefucielied function:
    if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_undef) {
      __templateCorrelation_defineFuncNAme__manyToMany(_asIs)(obj_config);    
    } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_none_storeIntermediateResults_forMPI) {
      __templateCorrelation_defineFuncNAme__manyToMany(_asIs_MPI)(obj_config);    
    // } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights) {
    //   __templateCorrelation_defineFuncNAme__manyToMany(_calculateWeights)(obj_config);    	    
    // } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min) {
    //   __templateCorrelation_defineFuncNAme__manyToMany(_scalar_min)(obj_config);    	    
    // } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max) {
    //   __templateCorrelation_defineFuncNAme__manyToMany(_scalar_max)(obj_config);    	    
    // } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum) {
    //   __templateCorrelation_defineFuncNAme__manyToMany(_scalar_sum)(obj_config);    	    
    //   //#include "correlation__codeStub__allAgainstAllPostProcess__sum.c"
    } else {    
      __templateCorrelation_defineFuncNAme__manyToMany(_scalar_generic)(obj_config);
    }
  } else { //! then we use a genriec fucntion:
    __templateCorrelation_defineFuncNAme__manyToMany(_scalar_generic)(obj_config);
  }
#elif(configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany == 1)
//! ************************ build for complex cases: -----------------------------------------    
  if(config_allAgainstAll->typeOf_optimization == e_typeOf_optimization_distance_asFastAsPossible) { //! then we use a spefucielied function:
    if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_undef) {
      __templateCorrelation_defineFuncNAme__manyToMany(_asIs)(obj_config);    
    } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_none_storeIntermediateResults_forMPI) {
      __templateCorrelation_defineFuncNAme__manyToMany(_asIs_MPI)(obj_config);    
    } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights) {
      __templateCorrelation_defineFuncNAme__manyToMany(_calculateWeights)(obj_config);    	    
    } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min) {
      __templateCorrelation_defineFuncNAme__manyToMany(_scalar_min)(obj_config);    	    
    } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max) {
      __templateCorrelation_defineFuncNAme__manyToMany(_scalar_max)(obj_config);    	    
    } else if(config_allAgainstAll->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum) {
      __templateCorrelation_defineFuncNAme__manyToMany(_scalar_sum)(obj_config);    	    
      //#include "correlation__codeStub__allAgainstAllPostProcess__sum.c"
    } else {    
      fprintf(stderr, "!!\t Add support for this option, ie, please forward a request to the developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up in debug-mode.
      //__templateCorrelation_defineFuncNAme__manyToMany
    }
  } else { //! then we use a genriec fucntion:
    __templateCorrelation_defineFuncNAme__manyToMany(_scalar_generic)(obj_config);
  }
#else //! then we 'use the same fucntion' for each of the cases, ie, thereby 'preventing' the compilaer from otpmziaing each sub-case (oekseth, 06. otk. 2016).
    __templateCorrelation_defineFuncNAme__manyToMany(_scalar_generic)(obj_config);
#endif

#ifndef NDEBUG
// #if(configureDebug_useExntensiveTestsIn__tiling == 1)    //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
//     { //! then we vlaidate wrt. 'specific inptu-values':
//       const uint nrows_2 = (config_allAgainstAll->iterationIndex_2 != UINT_MAX) ? config_allAgainstAll->iterationIndex_2 : nrows;
//       if(resultMatrix != NULL) {
// 	for(uint row_id = 0; row_id < nrows; row_id++) {
// 	  for(uint col_id = 0; col_id < nrows_2; col_id++) {      
// 	    const t_float score = resultMatrix[row_id][col_id];
// 	    assert(isinf(score) == false);
// 	    assert(isnan(score) == false);
// 	  }
// 	}
//       }
//     }
// #endif
#endif
}


#ifdef TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC //! then we assuem 'this fucntion' has not been buidl at an earlier call.
void TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC(const uint nrows_to_evaluate, const uint nrows_outer_to_evaluate, const t_float *rres_startPos, s_kt_computeTile_subResults_t *complexResult, t_float **resultMatrix, s_allAgainstAll_config_t self) {

  const uint globalStartPos_head = 0; const uint globalStartPos_tail = 0; 

  // assert(false); // FIXME: consider to drop usign the 'æexplict' "ncols" vairalbe in [”elow] ... ie, to always update the "obj.cnt_interesting_cells" variable (oekseth, 06. otk. 2016)
  const uint ncols = nrows_to_evaluate;
  // COMPILESTATUS("build for MPI-post-process")


  // printf("\t\t at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  //! Apply the lgoics, ie update:
  // assert(false); // FIXME: include [”elow]
#define __macroConfig__postProcess__isToAlwaysUpdateResultMatrix 1//! ie, to 'satsify' the debug-statements in our "func_template__postProcess__completeCorrelationComputation_squaredDistanceMatrix.c" (oekseth, 06. des. 2016)
  //const uint SM = 60; //! ie, to 'satsify' the debug-statements in our "func_template__postProcess__completeCorrelationComputation_squaredDistanceMatrix.c" (oekseth, 06. des. 2016)
  //t_float **listOf_inlineResults = NULL; //! ie, to 'satsify' the debug-statements in our "func_template__postProcess__completeCorrelationComputation_squaredDistanceMatrix.c" (oekseth, 06. des. 2016)
  fprintf(stderr, "TODO: inveietste if we may assume that 'this' always describes a 'symmetric data-proeprty', a quesiton at %s:%d\n", __FILE__, __LINE__); //! (eosekth, 06. des. 2016).
  const bool globalProp__dataSetIsSymmeric =  false; // (data1 == data2) && (iterationIndex_2 == nrows);
  //assert(obj_config.config_allAgainstAll); //! where latter si defined in our "config_nonRank_manyToMany.h" (eosekth, 06. mar. 2017)
  const t_float configMetric_power = self.configMetric_power; //-obj_config.config_allAgainstAll->configMetric_power;
  t_float **listOf_inlineResults = NULL;
#include "func_template__postProcess__completeCorrelationComputation_squaredDistanceMatrix.c"
#undef __macroConfig__postProcess__isToAlwaysUpdateResultMatrix
  
  // printf("\t\t at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

}
#undef TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC //! ie, to avodit eh function from being added more than once
#endif

//! -------------------------------------------------------------------
//!            Reset template-parameters:
//! -------------------------------------------------------------------
#undef TEMPLATE_NAMEOF_MAIN_FUNC //__notTransposed_maskImplicit
#undef TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows
#undef TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany
#undef TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany
//#undef TEMPLATE_NAMEOF_MAIN_FUNC__notTransposed_maskExplicit
#undef TEMPLATE_distanceConfiguration_useWeights
#undef TEMPLATE_distanceConfiguration_macroFor_metric
#undef TEMPLATE_distanceConfiguration_macroFor_metric_SSE
//#undef TEMPLATE_NAMEOF_MAIN_FUNC__notTransposed_notMask
#undef TEMPLATE_iterationConfiguration_typeOf_mask
#undef TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax
#undef TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin
#undef TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate
#undef TEMPLATE_distanceConfiguration_macroFor_metric_includePower
//! -----------
#undef __templateInternalVariable__isTo_updateCountOf_vertices
#undef TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#undef TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction
