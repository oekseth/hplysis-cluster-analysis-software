#ifndef hp_ccm_h
#define hp_ccm_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_ccm
   @brief a simplifed interface to our "kt_matrix_cmpCluster.h" (oekseth, 06. mar. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks a wrapper to provide simplfied non-complex utilizaiton and access to copmtuation fo Clsuter Compairson Metrics (CCMs) defined in our "kt_matrix_cmpCluster.h" interface (oekseth, 06. mar. 2017).
   @remarks
   - for an in-depth parametricded/advanced CCM-interface please see our "kt_matrix_cmpCluster.h";
   - for examples of usage, please see our "tut_ccm_*" examples;
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "kt_matrix_cmpCluster.h"
#include "kt_list_1d.h"

/**
   @brief idniety the CCM-score (oekseth, 06. feb. 2017).
   @param <ccm_enum>  the 'basic' CCM to apply
   @param <matrix> the matrix to comptue for
   @param <mapOf_vertexToCentroid1> a list of matrix->nrows elements: hold the clsuter-membershisp to invesigate
   @param <ccm_config> configures the applcaitoni of CCM-matrix operations, thereby enabling/supproting advanced configruaiton-calls.
   @param <scalar_score> is the result-CCM-score, ie, the result of the fucniton.call
   @return true if the operaiton was consiered a success.   
 **/
bool ccm__advanced__singleMatrix__hp_ccm(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum, const s_kt_matrix_base_t *matrix, const uint *mapOf_vertexToCentroid1, s_kt_matrix_cmpCluster_clusterDistance_config_t ccm_config, t_float *scalar_score);
/**
   @brief idniety the CCM-score (oekseth, 06. feb. 2017).
   @param <ccm_enum>  the 'basic' CCM to apply
   @param <matrix> the matrix to comptue for
   @param <mapOf_vertexToCentroid1> a list of matrix->nrows elements: hold the clsuter-membershisp to invesigate
   @param <scalar_score> is the result-CCM-score, ie, the result of the fucniton.call
   @return true if the operaiton was consiered a success.
 **/
bool ccm__singleMatrix__hp_ccm(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum, const s_kt_matrix_base_t *matrix, const uint *mapOf_vertexToCentroid1, t_float *scalar_score);
//! First copmtue simalrity, and thereafter identify the CCM-score (oekseth, 06. otk. 2017).
//! @remarks extends the "ccm__singleMatrix__hp_ccm(..)" with logics to first apply/use a defulat simalrity-matrix
bool ccm__singleMatrix__applyDefSimMetricBefore__hp_ccm(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum, const s_kt_matrix_base_t *matrix, const uint *mapOf_vertexToCentroid1, t_float *scalar_score);
//! A wrapper to our "ccm__singleMatrix__hp_ccm(..)" where we comptue for the complete set of matrix-based CCMs
//! @remarks if performance is of importance, then we suggest using routines defined in our "kt_matrix_cmpCluster.h" (ie, where 'this function' is a wrapper for the latter).
bool ccm__singleMatrix__completeSet__hp_ccm(const s_kt_matrix_base_t *matrix, const uint *mapOf_vertexToCentroid1, s_kt_list_1d_float_t *obj_result);
/**
   @brief PRovide support for both matrix-based and 'direct clsuterbased comaprsion' using CCMs (oesketh, 06. feb. 2017).
   @param <ccm_enum> is the CCM to compute.
   @param <mapOf_vertexToCentroid1> a list of cnt_vertices: hold the clsuter-membershisp to invesigate for: set(1);
   @param <mapOf_vertexToCentroid2> a list of cnt_vertices: hold the clsuter-membershisp to invesigate for: set(2);
   @param <cnt_vertices> the number of vertices to investigate clsuter-mebershisp/hypotehsis for: should correspodn to both nrows and ncols in matrixOf_coOccurence_1 and matrixOf_coOccurence_2 (ie, if the latter is set).
   @param <matrixOf_coOccurence_1> optional: should be set if a matrix-based CCM is used wrt. ccm_enum
   @param <matrixOf_coOccurence_2> optional: should be set if a matrix-based CCM is used wrt. ccm_enum
   @param <scalar_score> is the result-CCM-score, ie, the result of the fucniton.call
   @return true if the operaiton was consiered a success.
**/
bool ccm__twoHhypoThesis__hp_ccm(const e_kt_matrix_cmpCluster_metric_t ccm_enum, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint cnt_vertices, t_float **matrixOf_coOccurence_1, t_float **matrixOf_coOccurence_2, t_float *scalar_score);

//! Comptue for all 'naive defnions of CCMs' supported in Knitting-Tools.
//! @remarks wrt. input-paramters see "ccm__twoHhypoThesis__hp_ccm(..)"
//! @remarks if performance is of importance, then we suggest using routines defined in our "kt_matrix_cmpCluster.h" (ie, where 'this function' is a wrapper for the latter).
bool ccm__twoHhypoThesis__completeSet__hp_ccm(uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint cnt_vertices, t_float **matrixOf_coOccurence_1, t_float **matrixOf_coOccurence_2, s_kt_list_1d_float_t *obj_result);
/**
   @brief Supports for non-matrix based CCM-comptuations (oekseth, 06. feb. 2017)
   @param <ccm_enum> is the CCM to compute.
   @param <mapOf_vertexToCentroid1> a list of cnt_vertices: hold the clsuter-membershisp to invesigate for: set(1);
   @param <mapOf_vertexToCentroid2> a list of cnt_vertices: hold the clsuter-membershisp to invesigate for: set(2);
   @param <cnt_vertices> the number of vertices to investigate clsuter-mebershisp/hypotehsis for: should correspodn to both nrows and ncols in matrixOf_coOccurence_1 and matrixOf_coOccurence_2 (ie, if the latter is set).
   @param <scalar_score> is the result-CCM-score, ie, the result of the fucniton.call
   @return true if the operaiton was consiered a success.
   @remarks is a wrapper-funciton for our "ccm__twoHhypoThesis__hp_ccm(..)"
**/
bool ccm__twoHhypoThesis__xmtMatrixBased__hp_ccm(const e_kt_matrix_cmpCluster_metric_t ccm_enum, uint *mapOf_vertexToCentroid1, uint *mapOf_vertexToCentroid2, const uint cnt_vertices, t_float *scalar_score);



#endif //! EOF
