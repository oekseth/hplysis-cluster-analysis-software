#ifndef kt_aux_matrix_norm_h
#define kt_aux_matrix_norm_h

/* 
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 * 
 * This file is part of the hpLysis machine learning software.
 * 
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 * 
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 * 
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_aux_matrix_norm.h
   @brief provides access to different data nroamziaotn routines (oekseth, 06. feb. 2018)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2018).
   @remarks among others used in algorithms and applications of image anlaysis. 
**/


//#include "type_2d_float_nonCmp_uint.h"
#include "kt_matrix_base.h"
#include "matrix_deviation.h"
#include "correlation_sort.h"

//! Normalise socres: score(i) = (score(i) - AVG(score))/(max(score)-AVG(score)) ... ie, to avodi 'large differences in scales' from clustteirng the 'ability' of our 'signature-comarpsion-appraoch' (oekseth, 06. mai. 2017).
void apply_norm__kt_aux_matrix_norm(const t_float *old_data, t_float *result, const uint ncols, const bool isTo_useMedian, const bool isTo_useAbs, const bool isTo_useSTD);

//! @return a nraomzlied veriosn of hte amtrix, ie, allcoating a enw matrix (oekseth, 06. mai. 2017)
s_kt_matrix_base_t initAndReturn__copy__normalized__kt_aux_matrix_norm(const s_kt_matrix_base_t *superset);

/**
   @enum e_kt_normType
   @brief provide logics for differnet normalizaiton-strategeis wrt. a data-set (oekseth, 06. jul. 2017).
 **/
typedef enum e_kt_normType {
  e_kt_normType_avgRelative,
  // --- 
  e_kt_normType_avgRelative_abs,
  e_kt_normType_rankRelative,
  e_kt_normType_rankRelative_abs,
  e_kt_normType_relative_STD,
  e_kt_normType_relative_STD_abs,
  // --- 
  e_kt_normType_avg_row,
  e_kt_normType_avg_row_abs,
  e_kt_normType_avg_col,
  // --- 
  e_kt_normType_max_row_relative,
  e_kt_normType_min_row_relative,
  // --- 
  e_kt_normType_STD_row,
  // --- 
  e_kt_normType_undef,
} e_kt_normType_t;

//! @return a humanr-eadalbe string-representition of the normalziiaotn-enum (oesketh, 60. jul. 2017).
const char *get_str__humanReadable__e_kt_normType_t(const e_kt_normType_t enum_id);

//! @return a humanr-eadalbe string-representition of the normalziiaotn-enum (oesketh, 60. jul. 2017).
const char *get_description__humanReadable__e_kt_normType_t(const e_kt_normType_t enum_id);


//! @return the "e_kt_normType_t" enum of the input string (ie, if any) (oekseth, 06. apr. 2018).
static const e_kt_normType_t get_enum_e_kt_normType_t(const char *str) {
  assert(str); assert(strlen(str));
  if(!str || !strlen(str)) {return e_kt_normType_undef;}
  for(uint enum_id = 0; enum_id < e_kt_normType_undef; enum_id++) {
    const e_kt_normType_t enum_type = (e_kt_normType_t)enum_id;
    const char *cmp = get_str__humanReadable__e_kt_normType_t(enum_type);
    assert(cmp); assert(strlen(cmp));
    if(cmp && strlen(cmp)) {
      if(strlen(cmp) == strlen(str)) {
	if(0 == strcasecmp(cmp, str)) {return enum_type;}
      }
    }
  }
  //!
  //! At this point the enum was Not found: 
  return e_kt_normType_undef;
}

//! @return a nraomzlied veriosn of hte amtrix, ie, allcoating a enw matrix (oekseth, 06. mai. 2017)
s_kt_matrix_base_t initAndReturn__copy__normalizedByEnum__kt_aux_matrix_norm(const s_kt_matrix_base_t *superset, e_kt_normType_t enum_id, const bool isTo_allocate);


//! Normalise socres: score(i) = (score(i) - AVG(score))/(max(score)-AVG(score)) ... ie, to avodi 'large differences in scales' from clustteirng the 'ability' of our 'signature-comarpsion-appraoch' (oekseth, 06. mai. 2017).
void apply_norm__list_matrix_norm(const t_float *old_data, t_float *result, const uint ncols,  e_kt_normType_t enum_id) ;

#endif //! EOF
