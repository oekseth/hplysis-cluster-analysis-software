#ifndef kt_sparse_sim_h
#define kt_sparse_sim_h
 

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_sparse_sim
   @brief an inteface to comptue silmiarty-metics for sparse data-sets.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
**/

//#include "type_2d_float_nonCmp_uint.h"
#include "kt_sparse_sim_config.h"
//#include "list_uint.h"
#include "kt_matrix_base.h"
#include "e_kt_correlationFunction.h"
#include "kt_set_2dsparse.h"
#include "kt_list_1d.h" //! which is used to simplify the 'wrappering' of these fucntiosn into/for other languages.
#include "kt_list_1d_string.h"
#include "kt_list_2d.h"

/**
   @struct s_kt_sparse_sim
   @brief provide a cofngiruaiton-object wrt. our dense API for comptuation of simliarty-metircs.
 **/
typedef struct s_kt_sparse_sim {
  s_kt_sparse_sim_config_t config;
  s_kt_correlationMetric_t sim_metric;
  void (*func_sim)(const s_kt_sparse_sim_config_t *self, s_kt_set_2dsparse_t *obj_1, s_kt_set_2dsparse_t *obj_2, s_kt_matrix_base_t *result_mat, s_kt_set_2dsparse_t *result_sparse, const t_float *weight, uint startPos__obj_1, uint startPos__obj_2, const bool config__listIsAlreadySorted);
} s_kt_sparse_sim_t;
//! @return an intated s_kt_sparse_sim_t object (oekseth, 06. mar. 2017).
static s_kt_sparse_sim_t init__s_kt_sparse_sim_t(const s_kt_correlationMetric_t sim_metric) {
  s_kt_sparse_sim_t self;
  self.config = init__s_kt_sparse_sim_config_t();
  self.sim_metric = sim_metric;
  self.func_sim = NULL;
  //! @return 
  return self;
}

//! De-allcoate the s_kt_sparse_sim_t object.
static void free__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self) {
  assert(self); //! and other wise an 'empty' call, ie, as we expect no memory to have been allcoated.
}
/**
   @param <self> hold the set of configuraitons to use (oekseth, 06. mar. 2017).
   @param <obj_1> is the first 2d-list to cmpare.
   @param <obj_2> is the second 2d-list to compare
   @return a matrix holding the simliarty-metics between the pairs.
   @param <obj_weight> which if set impleis that we use d a'weighted verison' of hte simalrity-metric.
 **/
s_kt_matrix_base_t computeStoreIn__matrix__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, s_kt_set_2dsparse_t *obj_1, s_kt_set_2dsparse_t *obj_2, const s_kt_list_1d_float_t *obj_weight);
/**
   @param <self> hold the set of configuraitons to use (oekseth, 06. mar. 2017).
   @param <obj_1> is the first 2d-list to cmpare.
   @param <obj_2> is the second 2d-list to compare   
   @param <isTo_rememberScores> which if set to false 'implies' that we only insert the row-indces 'which are inside the thresholds specified in oru s_kt_sparse_sim_config_t object'.
   @param <obj_weight> which if set impleis that we use d a'weighted verison' of hte simalrity-metric.
   @return a 2d-list which hold the result:
 **/
s_kt_set_2dsparse_t computeStoreIn__2dList__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, s_kt_set_2dsparse_t *obj_1, s_kt_set_2dsparse_t *obj_2, const bool isTo_rememberScores, const s_kt_list_1d_float_t *obj_weight);

/*
static bool swigTest_1__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2) {
  // TODO: deative this function when not in a SWIG-comialtion-step
  return true; // TODO: deative this function when not in a SWIG-comialtion-step
}

static void swigTest_2__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2) {
  // TODO: deative this function when not in a SWIG-comialtion-step
  s_kt_list_2d_kvPair_t *tmp;
}

static void swigTest_3__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2) {
  // TODO: deative this function when not in a SWIG-comialtion-step
  s_kt_list_2d_kvPair_t *tmp;
  const s_kt_list_2d_kvPair_filterConfig_t *config_filter = NULL;
}


static s_kt_list_2d_kvPair_t swigTest_4__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2) {
  // TODO: deative this function when not in a SWIG-comialtion-step
  s_kt_list_2d_kvPair_t *tmp;
  const s_kt_list_2d_kvPair_filterConfig_t *config_filter = NULL;
  s_kt_list_2d_kvPair_t tmp_r;
  return tmp_r;
}
static s_kt_list_2d_kvPair_t *swigTest_4__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, bool tmp2, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2) {
  // TODO: deative this function when not in a SWIG-comialtion-step
  s_kt_list_2d_kvPair_t *tmp;
  const s_kt_list_2d_kvPair_filterConfig_t *config_filter = NULL;
  s_kt_list_2d_kvPair_t tmp_r;
  return &tmp_r;
}

static s_kt_list_2d_kvPair_t swigTest_5__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, bool tmp2, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2) {
  // TODO: deative this function when not in a SWIG-comialtion-step
  s_kt_list_2d_kvPair_t *tmp;
  const s_kt_list_2d_kvPair_filterConfig_t *config_filter = NULL;
  s_kt_list_2d_kvPair_t tmp_r;
  return tmp_r;
}

static s_kt_list_2d_kvPair_t swigTest_6__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, const bool isTo_sortBy_char, const s_kt_list_2d_kvPair_filterConfig_t *config_filter, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2)  {
  // TODO: deative this function when not in a SWIG-comialtion-step
  s_kt_list_2d_kvPair_t tmp_r;
  return tmp_r;
}

s_kt_list_2d_kvPair_t swigTest_10__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, const bool isTo_sortBy_char, const s_kt_list_2d_kvPair_filterConfig_t *config_filter, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2); // TODO: deative this function when not in a SWIG-comialtion-step
*/

/**
   @param <self> hold the set of configuraitons to use (oekseth, 06. mar. 2017).
   @param <isTo_sortBy_char> which if set to true impleis taht we comapre the char-order (rahter than the index-oder): correctness wrt. the latter dpedns on the chosen pariwise-silimaryt-emtric.
   @param <obj_1> is the first 2d-list to cmpare.
   @param <obj_2> is the second 2d-list to compare   
   @param <config_filter> which if Not set tu "NULL" is used to filter the vertices whcih are to be 'remembered'.
   @return a 2d-list which hold the result:
 **/
#ifndef SWIG
s_kt_list_2d_kvPair_t strings__computeStoreIn__2dList__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, const bool isTo_sortBy_char, const s_kt_list_2d_kvPair_filterConfig_t *config_filter, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2);
#endif // #ifndef SWIG

/*
static s_kt_list_2d_kvPair_t strings__computeStoreIn__2dList__s_kt_sparse_sim_t(s_kt_sparse_sim_t *self, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2) {
  // FIXME: validate correctness of bleow permtuation:
  const bool isTo_sortBy_char = false;
  const s_kt_list_2d_kvPair_filterConfig_t *config_filter = NULL;
  return strings__computeStoreIn__2dList__s_kt_sparse_sim_t(self, isTo_sortBy_char, config_filter, obj_1, obj_2);
}
*/

#endif //! EOF
