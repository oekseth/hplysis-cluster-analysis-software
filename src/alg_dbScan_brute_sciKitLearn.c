#include "alg_dbScan_brute_sciKitLearn.h"
#include "kt_list_2d.h"
#include "kt_list_1d.h"
/**
   @brief computes DBSCAN
   @param <adj> is the set of pre-filtered relationshisp to icnlude
   @param <max_vertexCnt> is the max vertex-identify in "adj".
   @return the cluster-membership partiions:
   @remarks we assumes that all verteices which has relationship in "adj" is regarded as 'core' vertices.
 **/
static s_kt_list_1d_uint_t __compute(const s_kt_list_2d_uint_t *adj, const uint max_vertexCnt) {
  assert(adj);
  s_kt_list_1d_uint_t clusters = init__s_kt_list_1d_uint_t(max_vertexCnt);
  s_kt_list_1d_uint_t stack = init__s_kt_list_1d_uint_t(max_vertexCnt);
  s_kt_list_1d_uint_t seen = init__s_kt_list_1d_uint_t(max_vertexCnt);
  uint label_num = 0;
  //  printf("listSize=%u, at %s:%d\n", adj->list_size, __FILE__, __LINE__);
  for(uint row_id = 0; row_id < adj->list_size; row_id++) {
    //    for i in range(clusters.shape[0]):
    if( (adj->list[row_id].list_size > 0) && (clusters.list[row_id] != UINT_MAX) ) {
      uint vertex_id = row_id;
      //! Then the row has data to be eluvated (and has Not yet been cosnidered):
      //        if clusters[i] != UINT_MAX or not is_core[i]:            continue
      /**
	 # Depth-first search starting from i, ending at the non-core points.
	 # This is very similar to the classic algorithm for computing connected
	 # components, the difference being that we label non-core points as
	 # part of a cluster (component), but don't expand their neighborhoods.
      **/
      while(true) {
	if(clusters.list[vertex_id] == UINT_MAX) {
	  clusters.list[vertex_id] = label_num;
	  //if is_core[i]:
	  {
	    const s_kt_list_1d_uint_t neighb = adj->list[vertex_id];
	    for(uint i = 0; i < neighb.list_size; i++) {
	      const uint v = neighb.list[i];
	      assert(v != UINT_MAX);
	      if( (clusters.list[v] == UINT_MAX) && (seen.list[v] == UINT_MAX) ) {
		seen.list[v] = true; //! ie, to avoid muliple accesses to this vertex.
		push__s_kt_list_1d_uint_t(&stack, v); 
	      }
	    }
	  }
	}
	if(stack.list_size == 0) {
	  break;
	}
      } //! ie, then the tree is explored.
      label_num += 1;    
      // TODO: consider to 'clear' the "seen" list.
      vertex_id = pop__s_kt_list_1d_uint_t(&stack);
    }
  }
  //! De-allocate:
  free__s_kt_list_1d_uint_t(&(stack));
  free__s_kt_list_1d_uint_t(&(seen));
  //! @return the result:
  return clusters;
}

/**
   @brief computes DBSCAN
   @param <adj> is the set of pre-filtered relationshisp to icnlude
   @param <max_vertexCnt> is the max vertex-identify in "adj".
   @return the cluster-membership partiions:
   @remarks we assumes that all verteices which has relationship in "adj" is regarded as 'core' vertices.
 **/
static s_kt_list_1d_uint_t __compute_mat(const s_kt_matrix_base_t *mat) {
  assert(mat);
  assert(mat->nrows == mat->ncols);
  const uint max_vertexCnt = mat->nrows;
  s_kt_list_1d_uint_t clusters = init__s_kt_list_1d_uint_t(max_vertexCnt);
  assert(clusters.list[0] == UINT_MAX); //! ie, as we expxec tall tiesm to be set to 'empty'.
  s_kt_list_1d_uint_t stack = init__s_kt_list_1d_uint_t(max_vertexCnt);
  s_kt_list_1d_uint_t seen = init__s_kt_list_1d_uint_t(max_vertexCnt);
  uint label_num = 0;  
  //printf("listSize=%u, at %s:%d\n", mat->nrows, __FILE__, __LINE__);
  for(uint row_id = 0; row_id < mat->nrows; row_id++) {
    //    for i in range(clusters.shape[0]):
    if( (clusters.list[row_id] == UINT_MAX) ) {
      uint vertex_id = row_id;
      //printf("vertex:%u\n", vertex_id);
      //! Then the row has data to be eluvated (and has Not yet been cosnidered):
      //        if clusters[i] != UINT_MAX or not is_core[i]:            continue
      /**
	 # Depth-first search starting from i, ending at the non-core points.
	 # This is very similar to the classic algorithm for computing connected
	 # components, the difference being that we label non-core points as
	 # part of a cluster (component), but don't expand their neighborhoods.
      **/
      while(true) {
	//printf("-vertex:%u\n", vertex_id);
	if((vertex_id != UINT_MAX) && (clusters.list[vertex_id] == UINT_MAX)) {
	  assert(vertex_id < clusters.list_size);
	  clusters.list[vertex_id] = label_num;
	  //if is_core[i]:
	  {
	    for(uint v = 0; v < mat->ncols; v++) {
	      if(mat->matrix[vertex_id][v] != T_FLOAT_MAX) { //! then it is used:
		if( (clusters.list[v] == UINT_MAX) && (seen.list[v] == UINT_MAX) ) {
		  assert(v < clusters.list_size);
		  seen.list[v] = true; //! ie, to avoid muliple accesses to this vertex.
		  push__s_kt_list_1d_uint_t(&stack, v);
		  // printf("add(%u-->%u)\n", vertex_id, v);
		}
	      }
	    }
	  }
	}
	if(stack.list_size == 0) {
	  break;
	}
	// TODO: consider to 'clear' the "seen" list.
	assert(clusters.list[vertex_id] != UINT_MAX);
	vertex_id = pop__s_kt_list_1d_uint_t(&stack);
	if(vertex_id == UINT_MAX) {break;}
	if(vertex_id != UINT_MAX) {
	  assert(vertex_id < clusters.list_size);
	}
      } //! ie, then the tree is explored.
      label_num += 1;    
    }
  }
  //! De-allocate:
  free__s_kt_list_1d_uint_t(&(stack));
  free__s_kt_list_1d_uint_t(&(seen));
  //! @return the result:
  return clusters;
}

/**
   @brief comptues DBSCAN.
   @param <obj_1> is the data of relationships
   @return the result of the clustering
 **/
s_kt_list_1d_uint_t computeFor_filteredMatrix__alg_dbScan_brute_sciKitLearn(const s_kt_matrix_base_t *obj_1) {
  if(!obj_1  || (obj_1->nrows == 0) ) {
    fprintf(stderr, "!!\t Input Not properly set: aborts computation. For questions, context Dr. Ekseth at [oekseth@gmail.com]. Observaiotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return setToEmpty__s_kt_list_1d_uint_t();
  }
  if(obj_1->nrows != obj_1->ncols) {
    fprintf(stderr, "!!\t Input Not properly set: the matrix with dimensions=[%u, %u] does Not sem like an adjcency matrix; aborts computation. For questions, context Dr. Ekseth at [oekseth@gmail.com]. Observaiotn at [%s]:%s:%d\n", obj_1->nrows, obj_1->ncols, __FUNCTION__, __FILE__, __LINE__);
    return setToEmpty__s_kt_list_1d_uint_t();
  }
  // printf("at %s:%d\n", __FILE__, __LINE__);
  s_kt_list_1d_uint_t clusters = __compute_mat(obj_1);
  assert(clusters.list_size > 0);
  return clusters;
}
/**
   @brief comptues DBSCAN for a pre-aprttioing set of relationships.
   @param <obj_1> is the data of relationships
   @return the result of the clustering
 **/
s_kt_list_1d_uint_t  computeFor_relations__alg_dbScan_brute_sciKitLearn(const s_kt_list_2d_uint_t *obj_1) { 
  //, s_kt_clusterAlg_fixed_resultObject_t *obj_result) {
//bool computeFor_relations__alg_dbScan_brute_sciKitLearn(const s_kt_list_2d_uint_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result) {
  if(!obj_1  || (obj_1->list_size == 0) ) {
//  if(!obj_1 || !obj_result || (obj_1->list_size == 0) ) {
    fprintf(stderr, "!!\t Input Not properly set: aborts computation. For questions, context Dr. Ekseth at [oekseth@gmail.com]. Observaiotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return setToEmpty__s_kt_list_1d_uint_t();
  }
  //! Idneitfy the max-vertex-id:
  uint max_vertexCnt = obj_1->list_size;
  for(uint row_id = 0; row_id < obj_1->list_size; row_id++) {
    const s_kt_list_1d_uint_t neighb = obj_1->list[row_id];
    for(uint i = 0; i < neighb.list_size; i++) {
      const uint v = neighb.list[i];
      assert(v != UINT_MAX);
      max_vertexCnt = macro_max(max_vertexCnt, (v+1));
    }
  }
  s_kt_list_1d_uint_t clusters = __compute(obj_1, max_vertexCnt);
  assert(clusters.list_size > 0);
  return clusters;
}
/*   //! Convert the results: */
/*   const bool is_ok = __collectResults(&clusters); */
/*   free__s_kt_list_1d_uint_t(&clusters); */
/*   return is_ok; */
/* } */
