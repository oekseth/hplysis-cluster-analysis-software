#include "kt_clusterAlg_dbScan.h"
#include "alg_dbScan_brute.h"
#include "macros.h"
#ifndef macro_max
#define macro_max(x, y) ((x) > (y) ? (x) : (y))
#endif
#ifndef macro_min
#define macro_min(x, y) ((x) < (y) ? (x) : (y))
#endif


//! Comptue disjoint-forests for a pre-masked data-set
bool compute__maskedMatrix__kt_clusterAlg_dbScan(const s_kt_matrix_t *matrix_input, const s_kt_list_1d_uint_t *obj_mapOf_rootVertices_pointer, s_kt_clusterAlg_fixed_resultObject_t *obj_result, s_hp_distanceCluster_t *objMetric__postMerge, const s_kt_api_config_t config) {
const t_float __localConfig__specificEmptyValue = T_FLOAT_MAX;     
  //! -----------------------------
  //!
  //! Apply the dsijtoint-forest-routine:
  s_kt_forest_findDisjoint_t obj_disjoint; setTo_Empty__s_kt_forest_findDisjoint(&obj_disjoint);  const s_kt_matrix_t *matrix_disjointInput = matrix_input;
  uint *map_vertexGroups_cols = NULL; uint map_vertexGroups_cols_size = 0;
  obj_disjoint.obj_mapOf_rootVertices_pointer = obj_mapOf_rootVertices_pointer;
  if(objMetric__postMerge != NULL) {obj_disjoint.obj_mapOf_rootVertices_pointer__forBothHeadAndTail = true;} //! eg, as seen in the MCL-lcuster-algorithm (oekseth, 06. jul. 2017).
  bool data_isUpdated = false;
  //  if(config.isTo_applyDisjointSeperation) 
  {
    if(config.inputMatrix__isAnAdjecencyMatrix) {
      //! ---
      { 
	long int cnt_empty = 0; //uint cnt_interesting = 0;
	const long int cnt_possible = matrix_disjointInput->nrows * matrix_disjointInput->ncols;
	for(uint row_id = 0; row_id < matrix_disjointInput->nrows; row_id++) {
	  for(uint col_id = 0; col_id < matrix_disjointInput->ncols; col_id++) {
	    if(matrix_disjointInput->matrix[row_id][col_id] == __localConfig__specificEmptyValue) {cnt_empty++;}
	  }
	}
	if(cnt_empty == cnt_possible) {
	  if(false) {
	    fprintf(stderr, "!!\t Seems like None of the matrix-cells were of itnerest in a matrix [%u, %u], ie, where a call to this disjoitn-forest-procedure is poiintless: we asusmed theat the 'empty-value'='%f' (described throught ehe internal parameter of \"self->emptyValueIf_maskIsNotUsed\"). If the latter doudns odd, then please contact the senior devleoper at [oekseth@gmail.com]. Observiaotn at [%s]:%s:%d\n", matrix_disjointInput->nrows, matrix_disjointInput->ncols, __localConfig__specificEmptyValue, __FUNCTION__, __FILE__, __LINE__);	  
	  }
	  return true;
	  //assert(false);
	}
      }
      assert(matrix_disjointInput->nrows == matrix_disjointInput->ncols);     const uint nrows = matrix_disjointInput->nrows;
      // printf("__localConfig__specificEmptyValue=%f, at %s:%d\n", __localConfig__specificEmptyValue, __FILE__, __LINE__);
      allocate__denseMatrix__s_kt_forest_findDisjoint(&obj_disjoint, nrows, nrows, matrix_disjointInput->matrix, /*mask=*/NULL, /*emptyValueIf_maskIsNotUsed=*/__localConfig__specificEmptyValue);
      obj_disjoint.obj_mapOf_rootVertices_pointer = obj_mapOf_rootVertices_pointer; //! ie, a 'safe-guard'.
      //! Idneitfy the disjoint regiosn:
      graph_disjointForests__s_kt_forest_findDisjoint(&obj_disjoint, /*isTo_identifyCentrlaityVertex=*/false);
      //!
      //! Iterate through the set of disjotin sets      
      uint max_cntForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&obj_disjoint, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
      //printf("max_cntForests=%u, nrows=%u, at %s:%d\n", max_cntForests, nrows, __FILE__, __LINE__);
      {
	uint max_cntForests_tmp = 0; get_maxNumberOf_disjointForests(&obj_disjoint,&max_cntForests_tmp);
	assert(max_cntForests_tmp != UINT_MAX);
	// printf("max_cntForests_tmp=%u, max_cntForests=%u, at %s:%d\n", max_cntForests_tmp, max_cntForests, __FILE__, __LINE__);
	assert(max_cntForests == max_cntForests_tmp);
      }
      if( (max_cntForests > 1) && (max_cntForests != UINT_MAX) ) {    
	data_isUpdated = true;
	//! Intaite the result-opbject:
	const uint size_inner = matrix_disjointInput->nrows; //! ie, as we asusme only the 'nrows' name-space is sued.
	init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, max_cntForests, size_inner);
	//fprintf(stderr, "(info)\t nrows=%u=%u, ncols=%u, cnt_vertex=%u, max_cntForests=%u, size_inner=%u, at %s:%d\n", obj_cpy_ranksEachRow.nrows, matrix_input->nrows, matrix_input->ncols, obj_result->cnt_vertex, max_cntForests, size_inner, __FILE__, __LINE__);

	for(uint forest_id = 0; forest_id < max_cntForests; forest_id++) {
	  uint *arrOf_result_vertexMembers = NULL; uint arrOf_result_vertexMembers_size = 0;
	  cosntructlistOf_forestId_members__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, &arrOf_result_vertexMembers, &arrOf_result_vertexMembers_size);	
	  if(arrOf_result_vertexMembers_size > 0) {
	    //!
	    //! Iterate through the vertices and udpate the membership:
	    for(uint index = 0; index < arrOf_result_vertexMembers_size; index++) {
	      const uint row_id = arrOf_result_vertexMembers[index];
	      assert(row_id != UINT_MAX);
	      assert(row_id < matrix_disjointInput->nrows);
	      //! Update:
	      //printf("sets-vertex[%u]  = forest(%u), at %s:%d\n", row_id, forest_id, __FILE__, __LINE__);
	      obj_result->vertex_clusterId[row_id] = forest_id; //! ie, udpat ethe membership
	    }
	  }
	  //!
	  //! De-allcoate:
	  if(arrOf_result_vertexMembers) {
	    free_1d_list_uint(&arrOf_result_vertexMembers); arrOf_result_vertexMembers = NULL;
	  }	
	}
      }
      //!
      //! De-allcoate the reserved memory:
      //! <NIL>
    } else {
      //! ---
      //assert(matrix_disjointInput->nrows == matrix_disjointInput->ncols);    
      const uint nrows = matrix_disjointInput->nrows;     const uint ncols = matrix_disjointInput->ncols;
      assert(nrows > 0);     assert(ncols > 0);
      map_vertexGroups_cols_size = ncols;
      const t_float val_0 = 0; map_vertexGroups_cols = allocate_1d_list_uint(ncols, val_0);
      for(uint i = 0; i < ncols; i++) {map_vertexGroups_cols[i] = UINT_MAX;} //! ie, 'reset'.
      allocate__denseMatrix_extensive__s_kt_forest_findDisjoint(&obj_disjoint, nrows, ncols, matrix_disjointInput->matrix, /*mask=*/NULL, /*empty-value=*/__localConfig__specificEmptyValue, /*isTo_init=*/true, /*useSeperateNameSpace_rows=useMultipleInputMatrices=*/true,/*useSeperateNameSpace_columns=*/true, /*_totalCntUniqueVerticesToEvalauteForAllIterations=total-vertices=*/(nrows+ncols));
      obj_disjoint.obj_mapOf_rootVertices_pointer = obj_mapOf_rootVertices_pointer; //! ie, a 'safe-guard'.
      //! Idneitfy the disjoint regiosn:
      graph_disjointForests__s_kt_forest_findDisjoint(&obj_disjoint, /*isTo_identifyCentrlaityVertex=*/false);
      //!
      //! Iterate through the set of disjotin sets
      uint max_cntForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&obj_disjoint, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
      {
	uint max_cntForests_tmp = 0; get_maxNumberOf_disjointForests(&obj_disjoint, &max_cntForests_tmp);
	assert(max_cntForests_tmp != UINT_MAX);
	// printf("max_cntForests_tmp=%u, max_cntForests=%u, at %s:%d\n", max_cntForests_tmp, max_cntForests, __FILE__, __LINE__);
	assert(max_cntForests == max_cntForests_tmp);
      }
      if( (max_cntForests > 1) && (max_cntForests != UINT_MAX) ) {    
	data_isUpdated = true;
	//! Intaite the result-opbject:
	const uint size_inner = matrix_disjointInput->nrows; // + matrix_disjointInput->ncols; //! ie, as we asusme only the 'nrows' and 'ncols' name-space is sued.
	init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, max_cntForests, size_inner);
	//fprintf(stderr, "(info)\t nrows=%u=%u, ncols=%u, cnt_vertex=%u, max_cntForests=%u, size_inner=%u, at %s:%d\n", obj_cpy_ranksEachRow.nrows, matrix_input->nrows, matrix_input->ncols, obj_result->cnt_vertex, max_cntForests, size_inner, __FILE__, __LINE__);
      
	for(uint forest_id = 0; forest_id < max_cntForests; forest_id++) {
	  uint *arrOf_result_row = NULL;  uint arrOf_result_row_size = 0; //! where the latter is a temprao vriable:
	  uint *arrOf_result_col = NULL;  uint arrOf_result_col_size = 0; //! where the latter is a temprao vriable:
	  //! The test:
	  //! Build a selit of the forest-ids:
	  cosntructlistOf_forestId_members__seperatelyFor__rowsAndCols__s_kt_forest_findDisjoint(&obj_disjoint, forest_id, 
												 &arrOf_result_row, &arrOf_result_row_size,
												 &arrOf_result_col, &arrOf_result_col_size);
	  const uint arrOf_result_vertexMembers_size = arrOf_result_row_size;            

	  if(arrOf_result_row_size > 0) {
	    //!
	    //! Iterate through the vertices and udpate the membership:
	    //if(config.isTo_transposeMatrix == false) {
	    for(uint index = 0; index < arrOf_result_row_size; index++) {
	      const uint row_id = arrOf_result_row[index];
	      assert(row_id != UINT_MAX);
	      assert(row_id < matrix_disjointInput->nrows);
	      //! Update:
	      printf("sets-vertex[%u]  = forest(%u), at %s:%d\n", row_id, forest_id, __FILE__, __LINE__);
	      obj_result->vertex_clusterId[row_id] = forest_id; //! ie, udpat ethe membership
	    }
	    assert(map_vertexGroups_cols);
	    for(uint index = 0; index < arrOf_result_col_size; index++) {
	      const uint col_id = arrOf_result_col[index];
	      assert(col_id != UINT_MAX);
	      assert(col_id < matrix_disjointInput->ncols);
	      //! Update:
	      assert_possibleOverhead(map_vertexGroups_cols[col_id] == UINT_MAX); //! ie, as we expect 'this Not to have beens et'.
	      map_vertexGroups_cols[col_id] = forest_id; //! ie, udpat ethe membership
	    }
	    /* } else { //! then we use the column-values */
	    /*   for(uint index = 0; index < arrOf_result_col_size; index++) { */
	    /*     const uint row_id = arrOf_result_col[index]; */
	    /*     assert(row_id != UINT_MAX); */
	    /*     assert(row_id < matrix_disjointInput->ncols); */
	    /*     //! Update: */
	    /*     obj_result->vertex_clusterId[row_id] = forest_id; //! ie, udpat ethe membership */
	    /*   } */
	    /* } */
	  }
	  //!
	  //! De-allcoate:
	  if(arrOf_result_row) {
	    free_1d_list_uint(&arrOf_result_row); arrOf_result_row = NULL;
	  }	
	  if(arrOf_result_col) {
	    free_1d_list_uint(&arrOf_result_col); arrOf_result_col = NULL;
	  }	
	}
      }
      //!
      //! De-allcoate the reserved memory:
      //! <NIL>      
    }
  } 
  if( (objMetric__postMerge != NULL) && (obj_result->cnt_vertex > 0) ) { //! then we apply an MCL-post-step, ie, to unify entites: 
    //! Intiate a set of vertices:
    s_kt_list_1d_uint_t map_forestIds = init__s_kt_list_1d_uint_t(obj_result->cnt_vertex);
    s_kt_list_1d_uint_t map_forestIds_counts = init__s_kt_list_1d_uint_t(obj_result->cnt_vertex);
    //! Find max-cluster-count
    uint cnt_clusters = 0;     for(uint i = 0; i < obj_result->cnt_vertex; i++) {
      const uint cluster_id = obj_result->vertex_clusterId[i];
      if(cluster_id != UINT_MAX) {cnt_clusters = macro_max(cnt_clusters, cluster_id); increment_scalar__s_kt_list_1d_uint_t(&map_forestIds_counts, cluster_id, /*value=*/1);} } cnt_clusters++;
    //! 
    for(uint i = 0; i < obj_result->cnt_vertex; i++) {
      const uint cluster_id = obj_result->vertex_clusterId[i];
      if( (cluster_id != UINT_MAX) && (map_forestIds_counts.list[cluster_id] >= objMetric__postMerge->minCntVertex_inEachCluster) ) {
	map_forestIds.list[i] = cluster_id;
      } else { //! then the clsuter-isze is isolated, ie, ingore.
	map_forestIds.list[i] = UINT_MAX;
      }
    }
    //!
    //! Comptuee
    //! Note: an otpmziaotn-strategy in [below] is to comptue the all-againsta-all-matrix for only the rows which are not assigned to any cluster, ie, instead of re-comtputing for the compelte set of veritces.
    // FIXMe[aritlce]: udapte oru aritlce wrt. [”elo] optimizaiton-step.
    s_kt_list_1d_uint_t map_newClusterIds = setToEmpty__s_kt_list_1d_uint_t();
    assert(map_forestIds.list);
    assert(map_forestIds.list_size);
    const bool is_ok = apply__s_hp_distanceCluster_t(objMetric__postMerge, &map_forestIds, matrix_input, &map_newClusterIds);
    assert(is_ok);    
    //!
    //! Update cont:
    for(uint i = 0; i < obj_result->cnt_vertex; i++) {
      obj_result->vertex_clusterId[i] = map_newClusterIds.list[i];
    }
    //!
    //! De-allcoate:
    free__s_kt_list_1d_uint_t(&map_forestIds);
    free__s_kt_list_1d_uint_t(&map_forestIds_counts);
    free__s_kt_list_1d_uint_t(&map_newClusterIds);
  }
  

  free_s_kt_forest_findDisjoint(&obj_disjoint);
  if(map_vertexGroups_cols) {free_1d_list_uint(&map_vertexGroups_cols); map_vertexGroups_cols = NULL; map_vertexGroups_cols_size = 0;}
  //!
  //! @return true:
  return true;
}


bool  compute__matrixInput__extensiveArgs__kt_clusterAlg_dbScan(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, t_float threshold_min, t_float threshold_max, t_float nodeThres_min, t_float nodeThres_max, const bool nodeThresh__useDirectSCoreInteadOfCount, const bool useRelativeScore_betweenMinMax, const bool isTo_adjustToRanks_beforeTrehsholds_forRows, const bool forRankAdjustment_adjsutTo_0_100, const bool isTo_insertIntoCompressedMatrix_beforeComptautiosn, s_hp_distanceCluster_t *objMetric__postMerge, const s_kt_api_config_t config, const bool use_bruteDisjointAlg) {
const t_float __localConfig__specificEmptyValue = T_FLOAT_MAX;     
  { //! Only evlauate if ehre are there si a theortical chance of mroe than one clsuter:
    long int cnt_empty = 0; //uint cnt_interesting = 0;
    const s_kt_matrix_t *matrix_disjointInput = obj_1;
    const long int cnt_possible = matrix_disjointInput->nrows * matrix_disjointInput->ncols;
    for(uint row_id = 0; row_id < matrix_disjointInput->nrows; row_id++) {
      for(uint col_id = 0; col_id < matrix_disjointInput->ncols; col_id++) {
	if(matrix_disjointInput->matrix[row_id][col_id] == __localConfig__specificEmptyValue) {cnt_empty++;}
      }
    }
    if(cnt_empty == cnt_possible) {
      fprintf(stderr, "!!\t Seems like None of the matrix-cells were of itnerest in a matrix [%u, %u], ie, where a call to this disjoitn-forest-procedure is poiintless: we asusmed theat the 'empty-value'='%f' (described throught ehe internal parameter of \"self->emptyValueIf_maskIsNotUsed\"). If the latter doudns odd, then please contact the senior devleoper at [oekseth@gmail.com]. Observiaotn at [%s]:%s:%d\n", matrix_disjointInput->nrows, matrix_disjointInput->ncols, __localConfig__specificEmptyValue, __FUNCTION__, __FILE__, __LINE__);	  
      return true;
    }
  }
  const s_kt_matrix_t *matrix_input = obj_1;
  s_kt_matrix_t obj_cpy_ranksEachRow; setTo_empty__s_kt_matrix_t(&obj_cpy_ranksEachRow);

  if(isTo_adjustToRanks_beforeTrehsholds_forRows) {
    // printf("copy-using-ranks, at %s:%d\n", __FILE__, __LINE__);
    if(false) {
      for(uint row_id = 0; row_id < obj_1->nrows; row_id++) {
	uint cnt_interesting = 0;
	for(uint col_id = 0; col_id < obj_1->ncols; col_id++) {      
	  if(isOf_interest(obj_1->matrix[row_id][col_id])) {
	    printf("%f\t", obj_1->matrix[row_id][col_id]); //cpy_ranksEachRow.matrix[row_id][col_id]);
	  }
	}
	printf("\n");
      }
    }
    //! 
    //! Construct a 'ranked version' of [ªbove]:
    const bool isOk_rankCpy = init__copy__useRanksEachRow__s_kt_matrix(&obj_cpy_ranksEachRow, obj_1, /*isTo_updateNames=*/true);
    assert(isOk_rankCpy);     assert(obj_1->nrows == obj_cpy_ranksEachRow.nrows);
    //!
    //! Apply the ranksthresholds:
    if(forRankAdjustment_adjsutTo_0_100) { //! then we adjsut the rank-score to reflect the max-rank in the data-set
#define __MiF__isWithin_0_100(val) ((val >= 0) && (val <= 100) )
      if(__MiF__isWithin_0_100(threshold_min) && __MiF__isWithin_0_100(threshold_max)) {
	t_float max_rank = T_FLOAT_MIN_ABS; 
	t_float min_rank = T_FLOAT_MAX;
	for(uint row_id = 0; row_id < obj_cpy_ranksEachRow.nrows; row_id++) {
	  uint cnt_interesting = 0;
	  for(uint col_id = 0; col_id < obj_cpy_ranksEachRow.ncols; col_id++) {      
	    if(isOf_interest(obj_cpy_ranksEachRow.matrix[row_id][col_id])) {
	      printf("%f\t", obj_cpy_ranksEachRow.matrix[row_id][col_id]);
	      max_rank = macro_max(max_rank, obj_cpy_ranksEachRow.matrix[row_id][col_id]);
	      min_rank = macro_min(min_rank, obj_cpy_ranksEachRow.matrix[row_id][col_id]);
	    }
	  }
	  printf("\n");
	}
	assert(max_rank != T_FLOAT_MIN_ABS); assert(max_rank != T_FLOAT_MAX);
	assert(min_rank != T_FLOAT_MAX);     assert(min_rank != T_FLOAT_MIN_ABS);
	//!
	//! Adjust the rank-scores:
	t_float rank_diff_0_100 = (max_rank - min_rank)*0.01;
	if( (threshold_min != T_FLOAT_MIN_ABS) && (threshold_min != T_FLOAT_MAX) ) {
	  threshold_min = rank_diff_0_100 * threshold_min;
	}
	if( (threshold_max != T_FLOAT_MIN_ABS) && (threshold_max != T_FLOAT_MAX) ) {
	  threshold_max = rank_diff_0_100 * threshold_max;
	}
	// printf("threshold=[%f, %f], given scoreRange=[%f, %f], rank_diff_0_100=%f=(%f*0.01), at %s:%d\n", threshold_min, threshold_max, min_rank, max_rank, rank_diff_0_100, (max_rank - min_rank), __FILE__, __LINE__);
	// assert(false); // FIXME: remve!
      } else {
	fprintf(stderr, "!!(input-paramter-error)\t Expected threshodl-paramters to be in range=[0, 100], though the thresohld-paramters is in range [%f, %f], ie, please udpate your API-inptu-call. Note: if this message seems confusing, then pelase cotnast senior devleopr [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", threshold_min, threshold_max, __FUNCTION__, __FILE__, __LINE__);
      }
    }

    //!
    //! Apply masking: 
    const bool is_ok = kt_matrix__filter__simpleMask__kt_api(&obj_cpy_ranksEachRow, threshold_min, threshold_max, useRelativeScore_betweenMinMax);
    assert(is_ok);
    if(is_ok == false) {free__s_kt_matrix(&obj_cpy_ranksEachRow); return false;}
    //!
    //! --------------------
    if(config.isTo_transposeMatrix == true) { //! then transpseo the matrix:
      s_kt_matrix_t tmp;       const bool is_ok = init__copy_transposed__s_kt_matrix(&tmp, &obj_cpy_ranksEachRow, /*isTo_updateNames=*/true);
      assert(is_ok);
      //!
      //! De-allocate
      free__s_kt_matrix(&obj_cpy_ranksEachRow);
      //! Then  and 'steal' the poitners:
      obj_cpy_ranksEachRow = tmp;
    }
    //!
    //! Udpate the 'matrix to use':
    matrix_input = &obj_cpy_ranksEachRow;    
  } else {
    // printf("copy-as-is, threshold=[%f, %f], use-relative-scores='%d', at %s:%d\n", threshold_min, threshold_max, useRelativeScore_betweenMinMax, __FILE__, __LINE__);
    const bool is_ok = init__copy__s_kt_matrix(&obj_cpy_ranksEachRow, obj_1, /*isTo_updateNames=*/true);
    assert(is_ok);
    //!
    //! Apply the ranksthresholds:
    kt_matrix__filter__simpleMask__kt_api(&obj_cpy_ranksEachRow, threshold_min, threshold_max, useRelativeScore_betweenMinMax);
    //!
    //! Udpate the 'matrix to use':
    matrix_input = &obj_cpy_ranksEachRow;    
  }

  //! 
  //! Construct a boolean mapping-table/hold the set of interesting of 'root-vertices', eg, wrt. the dB-scan-strategy for idneitficiaont of best-fit candidates.
  s_kt_list_1d_uint_t obj_mapOf_rootVertices = init__s_kt_list_1d_uint_t(matrix_input->nrows);
  const s_kt_list_1d_uint_t *obj_mapOf_rootVertices_pointer = &obj_mapOf_rootVertices;
  { //! Indetify the range of interesting veritces:
    //! Iterate throught eh matrix and set the vertices which are to be udnerstood/interpted as root-vertices:
    if(nodeThres_min != nodeThres_max) {
      //! Set all vlaues to false:
      for(uint i = 0; i < obj_mapOf_rootVertices.list_size; i++) {obj_mapOf_rootVertices.list[i] = false;}
      if(nodeThres_min > nodeThres_max) {nodeThres_min = T_FLOAT_MIN_ABS;}
      for(uint row_id = 0; row_id < matrix_input->nrows; row_id++) {
	if(nodeThresh__useDirectSCoreInteadOfCount == false) {
	  uint cnt_interesting = 0;
	  for(uint col_id = 0; col_id < matrix_input->ncols; col_id++) {
	    const t_float score = matrix_input->matrix[row_id][col_id];
	    cnt_interesting += isOf_interest(score);
	  }
	  //! Then set the score:
	  obj_mapOf_rootVertices.list[row_id] = ( (cnt_interesting > nodeThres_min) && (cnt_interesting < nodeThres_max));
	} else {
	  assert(matrix_input->nrows == matrix_input->ncols); //! ie, as we expect an adjcency-atmrix (oekseth, 06. jul. 2017).
	  const t_float score = matrix_input->matrix[row_id][row_id];
	  //! Mark the ndoe is the score-value is inside the thresohld:
	  obj_mapOf_rootVertices.list[row_id] = ( (score > nodeThres_min) && (score < nodeThres_max) );
	}
      }
    } else { //! we assume that all vertices are of interest:
      obj_mapOf_rootVertices_pointer = NULL; //! ie, as we are then not interested in using this list.
    }
  }




  


  // TODO: add support for/of: forRankAdjustment_adjsutTo_0_100 .... for root-vertex-filters ... 
  // TODO: consideradd support for/of: isTo_insertIntoCompressedMatrix_beforeComptautiosn
    // TODO: add support for/of: wrt. data-compression .... 'setting' the index-order iaw. the out-degree-centrlaity (ie, as we assuem, the latter may sigcinatly increase the perofmrance of memory-cace-accesses)
    // TODO: add support for/of: wrt. data-compression .... 


  

  //! -----------------------------
  //!
  //! Apply the dsijtoint-forest-routine:
  //  bool data_isUpdated = false;
  if(use_bruteDisjointAlg == false) {
    compute__maskedMatrix__kt_clusterAlg_dbScan(matrix_input, obj_mapOf_rootVertices_pointer, obj_result, objMetric__postMerge, config); //, &data_isUpdated);
  } else { //! then we apply the appraoch which does Not manage to dientify disjoint-regions (oekseth, 06. jan. 2019).
    // TODO: cosnider provdiing support which does Not require the copy of 'odl data' (oesketh, 06. jan. 2019).
    s_kt_matrix_base_t obj_copy = initAndReturn__allocate__s_kt_matrix_base_t(matrix_input->nrows, matrix_input->ncols, matrix_input->matrix);
    t_float score_max = T_FLOAT_MIN_ABS;
    for(uint row_id = 0; row_id < matrix_input->nrows; row_id++) {
      if(obj_mapOf_rootVertices_pointer->list[row_id] == false) {
	for(uint col_id = 0; col_id < matrix_input->ncols; col_id++) {
	  matrix_input->matrix[row_id][col_id] = T_FLOAT_MAX; //! ie, ignore.
	}
      } else {
	for(uint col_id = 0; col_id < matrix_input->ncols; col_id++) {
	  if(matrix_input->matrix[row_id][col_id] != T_FLOAT_MAX) {
	    score_max = macro_max(score_max, matrix_input->matrix[row_id][col_id]);
	  }
	}
      }
    }
    if(score_max != T_FLOAT_MIN_ABS) { //! then values were found:
      //get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_tmp);
      const t_float epsilon = score_max + 1; // self->config.kdConfig.ballsize; // FIXME: olek: validate this.
      const uint minpts  = 0; // self->config.kdConfig.nn; // FIXME: olek: validate this.
      const bool is_ok = compute__alg_dbScan_brute(e_alg_dbScan_brute_sciKitLearn, &obj_copy, obj_result, epsilon, minpts);
      assert(is_ok);
    }
    free__s_kt_matrix_base_t(&obj_copy);    
  }
  /* //! ----------------------------------------------------- */
  /* //!  */
  /* if(config.stringOf_exportResult__heatMapOf_clusters != NULL) { //! then expor thte matrix as a 'ehat-map' wr.t the clsuter-meberships: */
  /*   assert(obj_cpy_ranksEachRow.ncols > 0);     assert(obj_cpy_ranksEachRow.nrows > 0); */
  /*   if(config.inputMatrix__isAnAdjecencyMatrix) { */
  /*     if(data_isUpdated == true) { */
  /* 	//! */
  /* 	//! First remvoe the values:     */
  /* 	//fprintf(stderr, "(info)\t nrows=%u=%u, ncols=%u, cnt_vertex=%u, at %s:%d\n", obj_cpy_ranksEachRow.nrows, matrix_input->nrows, matrix_input->ncols, obj_result->cnt_vertex, __FILE__, __LINE__); */
  /* 	setDefaultValueTo_empty__removeCellsWithDifferentMembershipId__s_kt_matrix_t(&obj_cpy_ranksEachRow, obj_result->vertex_clusterId, obj_result->cnt_vertex, map_vertexGroups_cols); */
  /*     } //! else we use the matrix 'as-is'. */
  /*   } //! else we for simplity 'dropt eh udpate */
  /*   // TODO: vlaidate cornreesness of [”elow] asusmption wrt. 'droppignt eh exprot' (oekseth, 06. des. 2016). */
  /*   //! Tehreafter export the result: */
  /*   export__singleCall__s_kt_matrix_t(&obj_cpy_ranksEachRow, config.stringOf_exportResult__heatMapOf_clusters, config.fileHandler); */
  /* } */
  
  //!
  //! De-allocate the locally allocated matrix:
  if(matrix_input->matrix != obj_1->matrix) { //! then we asusem the matrix has been allcoated:
    free__s_kt_matrix(&obj_cpy_ranksEachRow);
  }
  free__s_kt_list_1d_uint_t(&obj_mapOf_rootVertices);
  //! ---------------------------------------------
  return true; //! ie, as we asusme the oerpaiton was au success.
}

/**
   @brief (oekseth, 06. apr. 2017)
   @remarks difference between a 'threshold-based dsijoitn-forest' VS the DB-scan-algorithm conserns the use of thresohlds on the 'interesiting set of nodes' (ie, in addition to thresholds on cells/edges), ie, a difference which is singicant (eg, when comapred to the difference between DB-scan/disjoint VS k-means).
 **/
static s_kt_clusterAlg_fixed_resultObject_t compute__matrixInput_kt_clusterAlg_dbScan(s_kt_clusterAlg_dbScan_t *self, const s_kt_matrix_t *matrix, const s_kt_api_config_t config) {
  assert(self); assert(matrix); assert(matrix->nrows); assert(matrix->ncols);

  s_kt_clusterAlg_fixed_resultObject_t obj_result = initAndReturn__s_kt_clusterAlg_fixed_resultObject_t();
  //!
  //! Apply filtering and start/evlauate the cluster-evlauation-procedure:
  if(self->config_filter != e_kt_clusterAlg_dbScan_howToSetFilter_dataIsAlaredyMasked) { //! then we need to 'add' a maksing-step.
    const bool is_ok = compute__matrixInput__extensiveArgs__kt_clusterAlg_dbScan(matrix, &obj_result, self->edgeScore_min, self->edgeScore_max,
										 self->nodeCount_min, self->nodeCount_max,
										 /*nodeThresh__useDirectSCoreInteadOfCount=*/false,
										 self->useRelativeScore_betweenMinMax, self->isTo_adjustToRanks_beforeTrehsholds_forRows,
										 /*forRankAdjustment_adjsutTo_0_100=*/self->config_applyFiltersOnRanks_adjustedTo_0_100,
										 self->isTo_insertIntoCompressedMatrix_beforeComptautiosn,
										 NULL,
										 config, false);
    assert(is_ok);
  } else {
    const bool is_ok = compute__matrixInput__extensiveArgs__kt_clusterAlg_dbScan(matrix, &obj_result, /*threshold_min=*/T_FLOAT_MAX, /*threshold_max=*/T_FLOAT_MAX,
										 T_FLOAT_MAX, T_FLOAT_MAX,
										 /*nodeThresh__useDirectSCoreInteadOfCount=*/false,
										 self->useRelativeScore_betweenMinMax, self->isTo_adjustToRanks_beforeTrehsholds_forRows,
										 /*forRankAdjustment_adjsutTo_0_100=*/false,
										 self->isTo_insertIntoCompressedMatrix_beforeComptautiosn,
										 NULL,
										 config, false);
    assert(is_ok);
  }
  /* const s_kt_matrix_base_t *matrix_filtered = matrix; s_kt_matrix_base_t matrix_filtered_local = initAndReturn__empty__s_kt_matrix_base_t(); */

  /*   matrix_filtered = &matrix_filtered_local; */
  /*   //! */
  /*   //! Start the filtering: */
  /* } */

  /* if(matrix != matrix_filtered) { */
  /*   free__s_kt_matrix_base_t(&matrix_filtered_local); */
  /* } */
  //! 
  //! Construct the result-object:
  return obj_result;
}


/**




   assert(false); // ok:FIXME: updae our "perf_relative_cacheMisses.pl" .... support matrix-result-merging ... coentualize differnet result-sets ... update our approach to 'unify'  reuslts into one result-file (eg, by using grep to filter the reuslts) .... use latte rin a test-script-setting ... vlaiditng informative-degree wr.t latter.
   // --------------------o
   assert(false); // ok:FIXME[conceptual]: ...    Motivaiton ....    update our   "bmc_generic_simAndClust.tex" ... and apply a spell-check ... 
   assert(false); // ok:FIXME[conceptual]: ...  "bmc_generic_simAndClust.tex" write ... background 
   assert(false); // ok:FIXME[conceptual]: ...  "bmc_generic_simAndClust.tex" write ... DB-scan-use-case
   // --------------------
   assert(false); // ok:FIXME[conceptaul]: .... new "ktSim_setOf_synData_spec.h" ... describe a matimatic itnerprtiaotni of our syentitc-evlauation-scheme ... and how latter relates to both (a) our textual desripn (of a sound appraoch for simalrty-emtric comparison) and wrt. the differtn/uqniue features of/in each (or our/the evaluted) simlairty-metrics .... 
   assert(false); // ok:FIXME[code]: "__constructFeatureVectors__s_ktSim_setOf_synData_spec_t(..)", "constructMAtrixOf_featureScores__s_ktSim_setOf_synData_spec_t(...)" (and asisted data-strucutres) found in "tut_simEval_1_syntheticData_syntheticFunctions.c(..)" ... update latter to 'capture' our above datga-distriubiton-tasks ... and then 'move out' the latter into a new file named .....??... get compiling .... 
   assert(false); // ok:FIXME[code]: ... add supprot for a rank-based evlauation-data-case .... where .....??... 
   // ----- 
   assert(false); // ok:FIXME[dbScan::eval::external::newTut--simEval...]: ... support the "dataCase(..)" in "bmc_generic_simAndClust.tex"
   assert(false); // FIXME[dbScan::eval::conceptaul]:  ... 
   assert(false); // ok:FIXME[dbScan::eval::conceptaul]:  ... in order to evlauate the dataCase(..) we 'invert' ... complete latter desicpriton ... udapte oru aritlc e... validate oru appraoch
   assert(false); // ok:FIXME[dbScan::eval::code]: udpate our "ktSim_setOf_synData_spec.h", "ktSim_setOf_synData_spec.c", "tut_simEval_1_syntheticData_syntheticFunctions.c" and oru "bmc_generic_simAndClust.tex" wrt. [ªbove].
   assert(false); // ok:FIXME[dbScan::eval::code]: ... new-fucn ... merge-matrices in our "kt_matrix.h" ... writign a  new 'module' anemd "kt_matrix__stub__mergeMatrix.c"
   assert(false); // ok:FIXME[dbScan::eval::code]: ... cosntrct a matrix which 'unifies' existing matrices wrt. data-out-generiaotn.
   assert(false); // ok:FIXME[dbScan::eval::code]: ... 'add' teh simlairty-emtrics 'into our psot-evlauation' whcih are described/used in oru aritlc-escaffold ... and then update our 'full' evlauation wrt. latter.
   assert(false); // ok:FIXME[dbScan::eval::code]:  ... extend our appraoch ... 'merge' muliple/different data-dsitributions .... ie, to 'unify data-sets which are added/consturcted' wrti our 'distrubiotn-pritn-fucntion(...)' .. written a scafofdl-appraoch ,... though need to apply/use an emrpcial inveionat.
   assert(false); // ok:FIXME[dbScan::eval::conceptaul]: ... udpate sim-emtric-eval ... setBased-CCM .... hca-k-mean ... store clsuter-results ... (for each data-evlauation wrt. relationishipb etween simalirty-emtrics) ... compute/evlauate ... the relationship  between ....??....  <-- dorpped, ie, may instead be 'dyamically' perofmred (in the fugure)
   assert(false); // ok:FIXME[dbScan::eval::conceptaul]:  ... introspect upon how to 'use' the different reuslt-atmrices generated from our "__writeOutFacts_matrix__local(..)" ... The above results are only a subset of thoese generated for each of oru data-evlauation-cases. By calling/using our \url{tut_simEval_1_syntheticData_syntheticFunctions.c} the follwoing reuslt-files are generated: raw-scores (ie, the scores foudn int eh evaluauted matrix), ranks (ie, the ranks for the scores evaluated speerately for each row), scores-average-noramlization (where we compaute "score(i) = (score(i) - AVG(score))/(max(score)-AVG(score))", an appraoch simliar to the work of \cite{}), devaitions (where we compmtue for: mean, variance, skewness, kurtosis, quartile-1, quartile-2, quartile-3, valueExtreme-min, valueExtreme-max, quartile-2-variance), and thereafter copmtuer a simliarty-amtrix seperately for the simalrity-emtircs of: Euclid, Cityblock, Clark, Jensen-Shannon, Kendall:Dice, and MINE:Mic. In our Fig \ref{} we observe a subset of the latter simalirty-emtric figures, ie, where the latter describes/dentoes the simliaryrt between the complete set of feature-vertices in each of the ... classes. From the latter applicaoitn (of differnet simalrity-emtrics) we observe how .... 
   assert(false); // ok:FIXME[dbScan::eval::code]: udpate our "ktSim_setOf_synData_spec.h", "ktSim_setOf_synData_spec.c", "tut_simEval_1_syntheticData_syntheticFunctions.c" and oru "bmc_generic_simAndClust.tex" wrt. [ªbove].
   assert(false); // ok:FIXME[dbScan::eval::code]:  ... extend our appraoch ... for simlairty-emtircs ... for [simCat(1), simCat(2), all-metrics] ... 
   // ---------
   // ----- 


   assert(false); // FIXME[code::new::tut-CCM-update::concpetual]: ... CCM .... describe different sytnetic data-sets: category(1) ... first CCM-score-column always reflects a  self-comparison (and where a comaprsion betwene different scores will give indiciaotns of diffenret min-weithing between self-comparsion-cases) ... catoegyr(2): .... 
   assert(false); // FIXME[code::new::tut-CCM-update::code]: ... new new ... based on desicpriotn in our "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures:clustAlg:CCM:extremeCases" .... seperately for each CCM-metric generate graphs/matrices for: direct-scores, data-variation(scores) .... disucss the differences in STD between diffneret CCMs ... 
   assert(false); // FIXME[code::new::tut-CCM-update::visu+article]: ... ... motivattion is to idenitfy target/scope-cases wrt. fiing use-cases where a new-prioposed algorithm sigciantly outperofmrs other altirms. An example consers our new-propseod 'direct' DB-scan algorithm-permtaution: in a number of journals a difference in clsuter-algoritm-accurace betwee 0.4 VS 0.9 for ARI is sufficeint, eg, as seen in a new-proposed dB-scan algorithm \cite{hahsler2016clustering}. From our view-point that different cluster-algrootuims are tailored towards different data-topology-itnerptaiton-gold-cases we belive taht theere is a combiantorical/large number of possible algorithms for fast (and caccurate) classifciatons. Therefore  .... 
   // ----- 
   assert(false); // FIXME[dbScan::eval::new-tut::""] ... mask-fitler-based appraoch .... Evalaute for eight differnet sysntietc data-sets. Generate a matrix [filter-type=mask(input=[sim-metric | nroamlizaiton-strategy], threshold=X)][X=sorted-rank]= CCM-score, where we 'as our defualt appraoch' comptue the CCMs usign the ARI CCM. In the/oru evlauation we use different mask-sigicnace-thresholds (with a 'fixed space') .... update our "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures:clustAlg:CCM:dbScan"
   assert(false); // FIXME[code::new::tut-CCM-update::visu+article]: ... applicaitoni of CCM-based knolwedge/sigicance .... In the figure we evlauate the effects of different strategies for "DB-scan", ie, using a systnietc/simplifed appraoch/interprtaiton. The aobve CCM-score-resutls correspodns to effects/applicaitons of our disjtoint-forest appraoch/algorithm, ie, the algorithm breifly described in seciton \ref{}.  ... we observe that our appraoch ahvies/gets a high CCM-score .... when compared to thesytentic data-evalautions in our Fig. \ref{tut_simEval_2_syntheticData_diffBetweenSimMetrics.c} 
   % FIXME: update our DB-scan artilce wrt. [below] results.
   % FIXME: vlaidate [”elwo] assuption ... and if [”elow] does Not hold ... then 'cosntruct' a data-set whihc 'force this rpoerpty' to hold ... 
   .... The above resutls clearly idnciates that there is a number of data-topology-disitrubions where a/our 'direct-feature-score-use' provide results of higher acucracy than the estlibhed Eculdi simalrity-metric. Differnetly told, a $O(n*|F|)$ time-and-memory-complexity DB-scan-impemtatnion may for serveral data-topology-cases produce mroe accurate results than a $O(n*n*|F|)$ time-and-memory-complexity DB-scan impmentaiton.    
   // ----- 
   assert(false); // FIXME[dbScan::eval::verif]: ... invietage [ªbove] results ... getting 'torugh' ... fixing memroy-erorrs (ie, if any) ... 
   assert(false); // FIXME[dbScan::eval::conceptual]: ... the traits 'to searhc for' wrt. a visual data-comparison .... 
   assert(false); // FIXME[dbScan::eval::code]: ... udpate sim-emtric-eval ... 
   assert(false); // FIXME[dbScan::eval::code]: ...
   assert(false); // FIXME[dbScan::eval::conceptaul]:  ... 
   assert(false); // FIXME[dbScan::eval::visu]: ... vaidaulise [ªbov€] ... and udpat eour aritlce wr.t teh results ... a scuccess-critera in .... conserns the ability to correctly identify .... 
   assert(false); // FIXME[dbScan::eval::figure]: ... "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures" ... sepeatley for 4x subsets of data .... writign out the 'raw' scores ... 
   assert(false); // FIXME[dbScan::eval::figure]: ... "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures::scoreVSVariation" ... sepeatley for ['all'] x [score, score(normalized), rank, deviations
   assert(false); // FIXME[dbScan::eval::figure]: ... 
   assert(false); // FIXME[dbScan::eval::external::newTut--simEva::"tut_simEval_2_syntheticData_diffBetweenSimMetrics.c"]: write && integrate latter ... epxort the result to a matrix ... udapte our "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures"
   // ----- 
   assert(false); // FIXME[code::new::tut::sim-metrics] ... effects of simlairty-emtric-comtpatuions .... complete code-udpate wrt. lattr ... udpate ouw JS-vsiauzlieion.
   assert(false); // FIXME[dbScan::eval::figure]: ... "" ... sepeatley for [subset, 'all'] x [score, score(normalized), rank, deviations]
   // ----- 
   assert(false); // FIXME[dbScan::eval::external::"tut_simEval_2_syntheticData_diffBetweenSimMetrics.c"]: ... extract a subset from our "tut_simEval_1_syntheticData_syntheticFunctions.c" ... then use the 'procuedre' described in our "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures:simMetric:postEval"
   assert(false); // FIXME[dbScan::eval::visu]: ... vaidaulise [ªbov€] ... and udpat eour aritlce wr.t teh results ... 
   // ----- 
   assert(false); // FIXME[dbScan::eval::external::newTut--simEval...]: .... apply prodcued described in oru "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures:simMetric:postEval_ccm"
   assert(false); // FIXME[dbScan::eval::visu]: ... vaidaulise [ªbov€] ... and udpat eour aritlce wr.t teh results ... 
   // ----- 
   assert(false); // FIXME[dbScan::eval::new-tut::""] ... PCA-emulation ... for our sytentic data-sets ... use a dynamic-HCA-k-means-appraoch ... where HCA is sued to find 'k-count' in clusteirng .. where we investigate the minimum number of features which may be inlcueed for the clsuter-result to be the same. (ie, mazimum number of features which may be removed). In this evaluation we (for simplicty) apply/use the following proecudre: step(1) comptue HCA, idnietyf the best matrix-based CCM-score, and sepertely select vertices based on eitehr a ranked-max-score or a ranked-min-score (wr.t he HCA-tree-topology); step(2) re-apply the HCA, usign as inptu the matrix with the limited/constrianted number of 'maksed' features; step(3) ...??...  
   % FIXME: consider updating/mofyuing oru "../acm__clustMetricCmp__algCmp/bmc_mine.pdf" wrt. the latter ... ie, to descirbe an appraoch were HCA-reduciton-step in combiantion with acucrate simarlity-emtrics may sigcnalty improve lclsuter-repdiont-accuracy .... 
   .... as a future-work we plan evlauating the latter proceudre for/on "k-means" itertive-clsuter-algorithms, eg, wrt. the task/applicaiton to relate accuracy/correnctess of a HCA-based PCA-fitlienrg to differetn traits/features of data-set-topologies.
   assert(false); // FIXME[code::new::tut-CCM-update::visu+article]: ...
   // ----- 
   assert(false); // FIXME[dbScan::eval::new-tut::""]
   assert(false); // FIXME[code::new::tut-CCM-update::visu+article]: ...
   // --------------------o



   assert(false); // FIXME[conceptual]: 
   assert(false); // FIXME[dbScan::eval::external::newTut--simEval...]: ... integrate/follwo proproceudre descrbed in oru "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures:clustAlg:disjointForest:maskThresholds"
   assert(false); // FIXME[dbScan::eval::external]: support "fig:subSec:measure:dataEvaluation:inputData:extremeFeatures:clustAlg:disjointForest:kdTree_VS_brute_largeFeatureSpace"
   assert(false); // FIXME[dbScan::article]: udpate our fig ... 
   // --------------------
   // --------------------
   % -------

   // --------------------
   assert(false); // FIXME[conceptual+perf]: ... compare with ...
   assert(false); // FIXME[dbScan::article]: udpate our fig ... 
   // --------------------
   assert(false); // FIXME[conceptual+perf]: ... compare with ... 
"~/poset_src/data_analysis/hplysis-cluster-analysis-software/src/externalLibs__scikit_learn/scikit-learn-master/sklearn/cluster/_dbscan_inner.pyx" .... describe 'weak' impemtantion-strategies wrt. latter .... then update our "kt_clusterAlg_dbScan.c" ... to/with an imrpvoed verison of the latter .... and compare the 'imprved verison' with our "kt_forest_findDisjoint.c" .... evlauating perf-overhead wrt. latter ...  eg, wrt. "cset[np.npy_intp] ... seen.count(v) == 0" where the latter ".count(..)" may result in an .....??... exeuciton-time-overhead .... to invesitge latter we constructed a wors-tcase-impemtnation (of the latter) (sepeately for a pointer-based stack and a list-based stack, ie, in our \url{tut_measure_dbScan_auxFunctions.c}) ... we observe that perf-opvehread is .....??..... Simliarty we evlauate time-cost of (a) using as sparse data-set for non-sparse filtered input, and (b) non-sarpse/maitrx-data-set for a sparse data-input. In our Fig. \ref{} we .... 
   % FIXME: wrt. "strapping" ... if the data-set is symmetirc, is the "strapping" automatically performed? ... however for a DB-scan we observe tha thte data-set is not symmetirc, ie, given teh high-low vertex-importance data-traversal .... 
   assert(false); // FIXME[cache-grind]: ... perform a meory-access-evalaiton of latter .... and udpate our Fig. \ref{} ... 
   // --------------------
   assert(false); // FIXME[perf]: ... update our "tut_measure_dbScan_auxFunctions.c" ... include the interaiotn-approchac used by the R-library ... inv eistte perf-ovehread wrt. latter .... udpate our aritlce-fig .....??.... importance/novelty/interest conserns the exeuction-time-overhead of non-optmized impemntiatons ... In our Fig. \ref{} we .... 
   assert(false); // FIXME[cache-grind]: ... perform a meory-access-evalaiton of latter .... and udpate our Fig. \ref{} ... 
   assert(false); // FIXME["kt_clusterAlg_dbScan.c"]: ... conceptaully descirbe how latter may straight-froardly be addressed .... 
   // --------------------o
   assert(false); // FIXME[conceptual]: ... scope ... scaffold a speeraiton wrt. our DB-scan-article .... first focus on our 'orignal' idea of dsijtoint-forest-perf-improvmenets, whiel second seeks/tries to improve perfomance of .....??.... 
   assert(false); // FIXME[conceptual]: ...  construct figures dpedicing our nroamlziaiotn-strategy .... read through the 'big report' ... fetch figures ... pmerutte latter .... use these to examplfiy/illustrate reaosns for validity of oru proposed algorithm-modifciaotn (ie, of the disjoint-forest/DB-scan algorithm). 
   assert(false); // FIXME[dbScan::article]: udpate our fig ... 
   // --------------------o
   assert(false); // FIXME[dbScan::eval::external--newTut--disjointForest--clsutAlg]: ... ability/potential to use $O(n*|F|)$ value-filter-appraoch instead of $O(n*n*|F|)$ simliarity-filter-appraoch .... 
   assert(false); // FIXME[dbScan::eval::external]: ... Importance of simliarty-metrics in DB-scan/disjoint-forest. Consturct two charts which 'show' exueciton-time of simliarty-metrics VS data-traversal .... importance of simlairty-metrics in brute-force 
   // --------------------o
   assert(false); // FIXME[dbScan::eval::external]: ... new script ... 
   assert(false); // FIXME[dbScan::eval::external]: ... write our a graph for time-cost of different [ncols][nrows] relationship .... update our "tut_measure_dbScan_auxFunctions.c" with a new fucniton ... use resulst to demonstet/examplify that ooptmizaiotn-benefits are stornlgy depednet/reliant on expected netowrk-topology (eg, where a randomzied access is withour increased perofmance-cost if time-cost of value-based accesses has a sigcant time-cost-overhead) .... an applciaotin of the latter is/conserns .....??....  ... when comaprign the xeuction-time of linear VS non-random access we observe that our software amanges a 2x peromfrance-icnrease when rows are linearly traversed .... Re-produce: \url{./x_tut dbScan case:0 nrows:750000 ncols:512} VS \url{./x_tut dbScan case:1 nrows:750000 ncols:512}.  
   assert(false); // FIXME[dbScan::eval::external]: ... construct a large synetic feautre-data-set .... introspect upon the best + worst-outcomes wrt. such an evlauaiton .... call the phython-apckage .... investigate effects on exueciotn-time adn results wrt.: (a) different cofnigruations (eg, kd-tree VS brute-force), (b) feature-count (ie, the imapct of the latter), (c) topology-order of the different 'value-cases' ... validate taht the results confirms to our expectiaons .... generate an appraoch to validate/inspect the results (ie, a new tut-scirpt) .... udpate our python-impemtatnion to 'load' data-files ... then comptue the results and manully investigate the results .... then visalize the results ... then udpate our "fig:performance:topology-traverse-order" figure in our "bmc_article.tex" 
   // --------------------
   assert(false); // FIXME[dbScan::eval::external]: ... new script ... time-eval of sim-matrix .... modfying/using "basicCases__language" as a tempalte ...  using a subet of exisitng evlauation-approach (eg, two extreme cases) .... add new figure in our "bmc_article.tex" .... where latter focus of different sim-metric-comptuations ....  whe  comparing 'brute' for "sklean" with our "./x_tut dbScan case:3 nrows:7500 ncols:512" we observe a peromance-differnece of 8.8x .... a difference which corresponds/relfects the differnece observed in time-cost between the current Pyhon-ismlairyt-libraries VS our approach .... from the latter we infer/drive/observe that ... the time-cost of simliarty-matric-comtpaution is the 'alfa and omega' cruc in "DB-scan" comtpaution/evlaution (ie, simlair to other/altneritve clsuter-algoirithms such as "k-means" and "SOM") .... what is of interest is to observe the poissible optmziaiotn-degree which may be gained/attained by instead comptuing/evaluating the transposed version/algoirhtm .... 
   // --------------------o
   assert(false); // FIXME[dbScan::eval::external]: ... accurayc-eval-impact of our proposed "strapping" appraoch (for DB-scan data-sets) ... 
   // --------------------
   assert(false); // FIXME[dbScan::eval::external]: ...
   // --------------------
   assert(false); // FIXME[conceptual]: ... 
   assert(false); // FIXME[conceptual]: ... assumption that 'rows are indpendent while feautres are depednet' ... which is based on the observaiotn of ......??.... 
   assert(false); // FIXME[conceptual]: ... motivaiton for evaluating different DB-scan-otpmal travesal-orders/appraoches ... ie, as we in a 'normal' evaluation-apprach are limited/constrianted by the exeuciotn-time-perofmrance of the simlairty-metric comptautions .... from the asusmption that 'rows are indpendent while feautres are depednet, we ..... 
   // --------------------
   assert(false); // FIXME[dbScan::eval::external]: ...
   // --------------------
   assert(false); // FIXME[dbScan::eval]: ... time-diff ... iterationng through a char-list VS an integer-list ... and vlaidate the the latter has an insufifcnt/ingorable exueciotn-tiem-difference.
   assert(false); // FIXME[dbScan::eval]: ... tiem-diff between investigating roots VS leafs (eg, [10][1000]  VS [1000][10]) .... an aprpaoch which (seeks to) capture the time-cost-benefit of first eleuvaintg the vertices with highcet cnetlriaty (eg, wrt. our-degree-count) .... and then compare the cost-benefit (of the latter) to the tiem-cost of comptugint eh centrlaity-emasure (eg, out-degree) .... then udpating oru docuemtantion ... ""
   assert(false); // FIXME[dbScan::eval]: ... memory-time ... list-base-access time-idfference when comarpgin "for(..)" direct-access-aprpaoch VS a whiel-based approach .... (wehre we evlauate diffenret cod-elogics-ovhereads wrt. the while-loops, eg, to try cpaturing the cost of 'dummy'/muliple "contineu;" statmenets) ... where we inveistgate latter by compling through/using "g++ -O2" optmied-compile-mode ... from teh altter we observe hwo the/a BFS-whiel-appraoch results/cases a perofmrance-overhead of ....  .... and in thsi cotnest evlaute the perofmrance-overhead  of inseritng 'non-evlauated vertices' .... eg, for the case where 10,000 vertices are inserted while only 10 veritces are evlauated wrt. their simlairty ... udpate our "fig:performance:topology-traverse-order::dataStructure-listTraversalType"
   assert(false); // FIXME[dbScan::eval]: ... memory-time ... comapre 'direct' memory-access to "	std::vector<int> points; 	std::vector<double> dists;" (eg, wrt. the "points.push_back(i);" funciton-call) .... (where latter is among others used in [ªbove] "brute.cpp") ... and then use result-data from the altter to .... .... and simalirty evlauate for "std::copy(N2.begin(), N2.end(),  std::back_inserter(N));", ie, capturing the realtvie-time-cost-significance of the copy-operaiton ... "std::copy(vec.begin(), vec.end(), output.begin() + pos);" (where latter is used in teh #>weights[IntegerVector(N.begin(), N.end())]" weightr-comptaution-oepraiton in the "R_dbscan.cpp" impemtatnion (which then traverse the result 'summing' the valeus in the latter) .... "while (!N.empty()) {       int j = N.back();       N.pop_back(); }", where we comapre latter to .....??...
   assert(false); // FIXME[dbScan::eval]: ... time-cost of ... 2d-sparse VS ... poitner-based-list-traversal (where we for the latter used a list of 'chained pointers') ... udpate our "fig:performance:topology-traverse-order::dataStructure-listTraversalType"
   ... udpate our "fig:performance:topology-traverse-order::dataStructure-listTraversalType"
   assert(false); // FIXME[dbScan::eval]: ... memory-time ... use our B-tree-comparison-appraoches to evlauate time-cost of different data-accesses ... udpate our "fig:performance:topology-traverse-order::dataStructure-listTraversalType"
   // --------------------
   assert(false); // FIXME[dbScan::eval]: ... compute disjtoint-forest for different appraoches ... and vliddate that we manage to infer data-sets faster than a data-set with 8000 rows ("https://cs.joensuu.fi/sipu/datasets/t4.8k.txt" ... and consider comparigon other data-sets at "https://cs.joensuu.fi/sipu/datasets/")  ... then comparign our reuslts to "eec_time_dbScan.png" ("https://cran.r-project.org/web/packages/dbscan/vignettes/dbscan.pdf") .... and then impelmetn the CPUdb-scan-veriosn of "~/Skrivebord/dbscan_GPUOptimized_1-s2.0-S1877050913003438-main.pdf" and compare the latter impemtantion-aprpaoch to our db-scan-aprpaoch .... ie, usign latter as a baseline wrt. perofmranc-e-imrpvoements. (where the authros in the latter CPU-based impemtantion implment DB-scan using ... ) .... evaluate effects of differnet value-compression-strategies, and oothes evlauating/describing the  perofmance-cost-overhead wrt. different optmziaotn-strategies .... udpating our fig: "fig:performance:topology-traverse-order::dataStructure-compreesionStrategies"


   // -------------------------------------------
   assert(false); // FIXME[dbScan::eval]: 
   assert(false); // FIXME[dbScan::eval]: ... time-cost-overhead of "kd-tree" approaches (eg, trnsalting/bulind a simplfied verison of the "kd_tree.cpp" insert-logics and searhc-logics) ... try to compare 'our underatnding of a tree-baed appraoch' to the overehad when comptuing simalirty-metircs. In teh  latter we comapre brute=$O(n*n*|F|)$ VS kd-tree=$n*|F|$, ie, where a "kd-tree" appraoch proivides an optimzaiotn-factor of $n$. However for the topolical use-case where $cost(F) ~ 1$ and $(hubs(n)/n) ~ 0$ a 'brute' appraoch has only a tiemc-ost of $n$, ie, for which we have examplfed a topolcial case where our 'brute' impemtantion may perform/go coniderably faster (than other appraoches). However in oru latter example we have compared/evluated an xtreme topolcla case, ie, a case which we do not expect to be represntive. Toa ddress/elvuate teh altter ... 

   assert(false); // FIXME[dbScan::eval]: ... manage to comptue "db-scan" using differnet permtautiosn of the "sklarn-pyhton-librayr" ... and comapre teh exeuciton-tiem to the time iterating through the compelte data-set ... validiatng (our assumption) that the 'real/retrue time-compelxtiy (of Db-scan and simlair algorithsm) are O(E) rahter than "n*log(n)" ... ie, givne teh ned to evlauate all edges.
   assert(false); // FIXME[dbScan::eval]: ... time-cost of ... accessing 'full' VS 'subset-matrix' (and describe different use-cases where a stack-based appraoch is secpted to produce inferrior/incompelte results).
   assert(false); // FIXME[dbScan::eval]: ... compare wrt. the "Clusters__findNeighbors(...)" funciton .... eg, when comapred to 'our' pre-copmtued 'booelan table' of 'trehhold-expansion-vertices' ... seperatley ... write a benchmark-scirpt wrt. [”elow] (ie, to evluate hte perf-rocst of the authros appraoch), ie, where we consturct a sparse list of veritces/relationships 'where each dsitance is within epsiolon, and where each vertex has at leat minPoints number of intereting/related vertices' ... and udpate our "kt_forest_findDisjoint.c" wrt. supporitn latter 'boolean table' wrt. {epsilon, minPoints} .... the perofmrnace-increase wrt. latter is due to a higher spatila adn temproal lcoaitliy of meory-accsess .... 'this appraoch' we frist re-map vertices wrt. tehri centrlaity (eg, ordered wrt. ...??..., an appraoch which is bleived to increase perofmrance due to ....??..), and then insert hte latter in our dense strucutre (hwere the 'comrepssed data-strucutre' produce/gives increased perofmrance for data-topolgoeis hwere ....??...).
   assert(false); // FIXME[dbScan::evalnew-algorithm-permtuation::future-work]: 
   assert(false); // FIXME[dbScan::eval::implmeent-new-alg]: 



   assert(false); // FIXME[dbScan::eval]: ... manage to get 'direclty integrated' the "/home/klatremus/poset_src/externalLibs/cluster_dbScan/dbscan-dbscan_1.1-0/src/R_dbscan.cpp" into our appraoch (ie, 'driectly' into a tut-file) ... and then write a tut-example where we use a matrix as input ... and use the "R-software-apcke-impmetnationi"  appraoch ... comparing the latter ro our 'ideal' distjoint-froest-evlauation-appraoch
   assert(false); // FIXME[dbScan::eval]: ... find 'cases' where a "k closets-points-filter" may be better/,mroe-prciesly be found/idneifed using a 'cocnrete' sigicnace-threshold (rather than a rank-based thresohld) .... ie, to argue that "/home/klatremus/poset_src/externalLibs/cluster_dbScan/dbscan-dbscan_1.1-0/src/brute.cpp" provides insufifcent/in-effcient "db-scan" impmetnation.
   assert(false); // FIXME[dbScan::eval]: ... topology-applicaiotn-cases ... describe data-evlauaiton-cases where ... there is 'no neded' for an a-poriory evaluation/applciaont of simlairty-measures. A use-case-applciaiotn-example of/for the latter is seen in/for the usec-ase of ... 
   assert(false); // FIXME[dbScan::eval]: ... topology-applicaiotn-cases ... describe data-evlauaiton-cases where ... 
   assert(false); // FIXME[dbScan::eval]: ... topology-applicaiotn-cases ... describe data-evlauaiton-cases where ... 


   assert(false); // FIXME[dbScan::eval]: ... memory-time ...  overehad by 'comptuing at differnet time-poitns' teh sialmrity-emtrics (ie, instread of usign our approach of a 'first-step to comptue comptue teh simalrity-emtrics .... where we for the latter evalaute the tiem-cost of 'selcitng at random' the 'row-id' to comapre 'its' neihborus with .... and in this context argue for the wkeaness of current aprpaoches wrt. theri 'uanblity/non-supporte for handlign of 'maksed valeus/scores (eg, wrt. the 'fact' that the "R_dbscan.cpp" does Not invesitgte/evlauate the cocnrete score of the ....??... before evlauating/applying the simalirty-metric) .... a differnet aspect conserns the lack of 'sparse support' for data-evaluation wr.t latter .... 
   assert(false); // FIXME[dbScan::eval]: ... memory-time ... 
   assert(false); // FIXME[dbScan::eval]: ... 
   // --------------------
  assert(false); // FIXME[dbscan::coneptaul]: .... discuss/elvuate the 'set-based data-structure' used in our "kt_forest_findDisjoint.h" ... time-cost wrt. latter ... ad how the altte rmay be imrpoved/optmized.
  assert(false); // FIXME[dbscan::coneptaul]: .... time-cost of a 'funciotn-call to a function which fidn the min-set of DBSCAN-itneresting vertices using a radnom-row-data-access' VS 'a seperate pre-step evaluating for all cases .... and therafter 'extend' the meausrmeent-exmaple to cover/describe for "Clusters__findNeighbors(..)", ie, where we evlauate/describe a 'random-acces w/thrree indicrecc77/impclit funciton-calls: "find-neighbourhood([similarity(getSimilarity(inner-product(vertex, row_id)))] for (row_id in nrows))", ie, where latter reuslts in four implcit/indirect calls to compute the dsitnace (and select the shrotest paths) between two vertices .... from latter we observe how an investigation wrt. an all-gaisnta-all-simComptaution 'at random' captures the latter appraoch .... 
  assert(false); // FIXME[dbscan::coneptaul]: .... 
  assert(false); // FIXME[dbscan::coneptaul]: .... idneitfy/suggest differetn/speical use-cases where a tree-based seleciotn of vertices is pointless ... evlauitng different 'extreme topologies' 
  assert(false); // FIXME[dbscan::mesuarement::syntatic]: .... time-cost of difnfnere t'set-based-isneriotn-and-merge' operaitons .... 
  assert(false); // FIXME["DBSCAN: Density-Based Spatial Clustering of Applications with Noise"]: ... compare with the "sci-kit-learn" implmetnation ... evlauting spefical use-cases: VS simple-relation-traversal, VS simMetric=[none, euclid],  VS set-based iunseriotn-time.
  assert(false); // FIXME: write a 'main-loop' where we consturct a 'comrpessed set' of vertices (usign Db-scan/[”elow] 'requriements') and then call oru "kt_forest_findDisjoint.h" API .... and where we emprically investigate different 'cdntroid-roder-schemes' wrt. [non-sorted (cnt-relations), sorted, ...] .... ....??... 

   // --------------------
   assert(false); // FIXME[dbScan::tut]: ... a simple example where we 'get' our "kt_clusterAlg_dbScan" 'through', ie, demosntrating applcailbity of the latter ... 
   assert(false); // FIXME[dbScan::tut]: ... an example where we demosntrate/evlauate the effect of different trheshold-fitlers .... where the CCM-score is computed seperately for differen trheshold-cases ... and where reuslts are investigated through/by using the GNU-plot-lbirary-wrapper "../gnuplot_i/src/" ... where goal is to investigate how the lines realted for different data-set-cases .... where a the results (wrt. the latter) is of sicentific interest/improtance for .....??.... 
  assert(false); // FIXME[dbScan::tut]: ... 
**/



/**
   assert(false); // ok:FIXME[dbScan::code::newFunc]: ... "s_kt_clusterAlg_dbScan_t" ...  'cfongiraution-struct' .... write latte r... add docu ... add intlaizaiotns
   assert(false); // ok:FIXME[dbScan::code::newFunc]: ... matrix-based ... fucniton-desicpriotn and 'base-call-lgocis'
   // ---------------------
   assert(false); // FIXME[dbScan::code::newFunc]: ...  udpat ethe "kt_matrix__filter__simpleMask__kt_api(..)" ("kt_matrix") to use/support a "0_100" value-adjustment ... wrt the "useRelativeScore_betweenMinMax" param 
   assert(false); // FIXME[dbScan::code::newFunc]: ... 'fork out' the fitler-logics in our "kt_matrix__disjointClusterSeperation" into a seperate fucniton in our .... and then update our "kt_api.c" fucntion .... thereafter 'extend' latter with suppoirt for 'intersting-node-count' filter (ie, 'remvoing' ndoes which are Not insde the givne filter).
   assert(false); // FIXME[dbScan::code::newFunc]: ... matrix-based ... filtering ... support ... using our [ªbove] 'new-forked' .... 
   assert(false); // FIXME[dbScan::code::newFunc]: ... matrix-based ... filtering ... defualt 'strategy' to find the min-threshold to be used
   assert(false); // FIXME[dbScan::code::newFunc]: ... matrix-based ... 're-indexing' wrt. the data-insertion-routines .... using logics in our "kt_distance_cluster.c" wrt. .... 
   // ---------------------
   assert(false); // FIXME[dbScan::code::newFunc]: ... sparse-input ... concpetual ... 'figure out' how adding support for 'this' wrt. filtierng ... udpating our article-scaffodl wrt. the differences in fitler--logics between a 'sprase data-setevaluation' VS a sednese-evlauation-aprpaoch'.
   assert(false); // FIXME[dbScan::code::newFunc]: ... sparse-input ... 
   assert(false); // FIXME[dbScan::code::newFunc]: ... 
   assert(false); // FIXME[dbScan::code::newFunc]: ... 
   assert(false); // FIXME[dbScan::code::newFunc]: ... update our header-file
   assert(false); // FIXME[dbScan::compile]: ... get our "kt_clusterAlg_dbScan.c" compiling.
   assert(false); // FIXME[dbScan::code::integration]: ... disjoint-forest-update .... add a new enum to 'desice' if we are to udpat ethe 'recaiblity-set of vertices' wrt. (a) none, (b) core/assignind nodes, (c) non-core/non-assignind nodes, none (ie, assume the travesal-order is sufficent, an asusmption found/applied in the default "DB-SCAN" impemtantiosn of other software-libraries) ... then udpate our measurements and our article wrt. the latter.
   // ---------------------------------------------------
   assert(false); // FIXME[dbScan::code::integration]: ... "hpLysis_api"... add new ddb-scan-enum to latter ... and add suppoert for different funciotns which 'use latter' (eg, enum-ot-stirng).
   assert(false); // FIXME[dbScan::code::integration]: ... "hpLysis_api"... java-script and Perl ... search for all 'occurences' of k-means and HCA in our Perl and java-scirpt ... udpating latter. 
   assert(false); // FIXME[dbScan::code::integration]: ... "hpLysis_api"... 'actual support' ... add a call to our "kt_clusterAlg_dbScan.c"
   assert(false); // FIXME[dbScan::code::integration]: ... "hpLysis_api"... when exportiing k-means-results to the "s_kt_clusterResult_condensed_t" set ... use the 'diea' from "fuzzy-logics' ... eg, to construct a distance-matrix from each vertex to each of the centordi-vertices ... ie, by first updating oru exprot-rotuien adn data-rotuien ... and then update first our Perl-mdoeule ... our PÅerl-CGI moduel ... and our javasc-ritp-visauzliation-appraoch .... through as a rpe-step scaffold a stand-alone artilce which ampleis/higlights the improtance (of our latter appraoch), ie, how the 'latter support' may fcialite/provide support for clsuter-analsysis-tasks such as .....??...
   assert(false); // FIXME[dbScan::code::integration]: ... "hpLysis_api"...
   assert(false); // FIXME[dbScan::code::integration]: ... README.md .... update latter 
   assert(false); // FIXME[dbScan::code]: ... 
   assert(false); // FIXME[dbScan::code]: ... 
**/


//bool apply__s_hp_distance__config_clusterCloseness_t(s_hp_distance__config_clusterCloseness_t *self, s_kt_list_1d_uint_t *obj_mapOf_disjtointForestIDs, const s_kt_matrix_t *mat_input, s_kt_list_1d_uint_t *retList)  {
//static s_kt_list_1d_uint_t __findClosetVertices_toClusters(s_hp_distance__config_clusterCloseness_t *self, s_kt_list_1d_uint_t *obj_mapOf_disjtointForestIDs, const s_kt_matrix_t *mat_input) {
//}
