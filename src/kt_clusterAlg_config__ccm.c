#include "kt_clusterAlg_config__ccm.h"

//! @return an itniated object of the "s_kt_clusterAlg_config__ccm_t" object (oekseth, 06. feb. 2017).
s_kt_clusterAlg_config__ccm_t initAndReturn__s_kt_clusterAlg_config__ccm_t(const s_kt_matrix_base_t *matrix_unFiltered) {
  s_kt_clusterAlg_config__ccm_t self;
  //! --------------------------------------
  //!
  //! Intitae theresold-variables:
  self.ccmToApply__minVertexCount__each__fraction = 0.05; //! ie, minmum five per-cent of the vertices in each of the 'clusters to be merged'.
  self.k_clusterCount__min = 2;
  self.k_clusterCount__max = 40; //UINT_MAX;
  //! --------------------------------------
  //!
  //! Intiate clsuter-convergence-varialbes:
  //! Note: for details wrt. [”elow] see our "kt_matrix_cmpCluster.h" header-file.
  self.ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC;
  self.ccm_type = e_kt_matrix_cmpCluster_clusterDistance__config_metric__default;
  self.ccm_config = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
  self.matrix_unFiltered = matrix_unFiltered;
  self.isTo_pply__CCM = true;
  //! 
  //! @return
  return self;
}

//! A shallow copy of a configuraiotn-object and an input-matrix.
//! @reutnr a shallow itnaited object.
s_kt_clusterAlg_config__ccm_t copyObj__doNotUseCCM__s_kt_clusterAlg_config__ccm_t(const s_kt_clusterAlg_config__ccm_t *old, const s_kt_matrix_base_t *matrix_input) { //t_float **matrix, uint nrows) {
  s_kt_clusterAlg_config__ccm_t self = *old;
  if(matrix_input) {
    //if(matrix) {
    //t_float **matrix = matrix_input->matrix;
    self.matrix_unFiltered = matrix_input; //initAndReturn__notAllocate__s_kt_matrix_base_t(nrows, nrows, matrix);
      //initAndReturn__notAllocate__s_kt_matrix_base_t(matrix_input->nrows, matrix_input->ncols, matrix);
  }
  self.isTo_pply__CCM = true;
  //! 
  //! @return
  return self;  
}


//! Apply the cofnigured CCM-emtric and return the reuslt (eokseth, 06. feb. 2017).
t_float apply__CCM__s_kt_clusterAlg_config__ccm_t(const s_kt_clusterAlg_config__ccm_t *self, const uint *mapOf_vertexToCentroid1) { //, const s_kt_matrix_t *matrix) {   
  assert(self); 
  if(self->matrix_unFiltered == NULL) {
    fprintf(stderr, "!!\t The inptu-amtrix is Not speciifed, ie, please provide a symmetitrc iniput-matrix. For quesitons  pelase cotnact the senior delvoeper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false); // FIXME: remove.
    return T_FLOAT_MAX;
  }
  const s_kt_matrix_base_t *matrix = self->matrix_unFiltered;
  assert(self->matrix_unFiltered); assert(mapOf_vertexToCentroid1);
  //  s_kt_matrix_cmpCluster_clusterDistance_config_t config_local  = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
  uint k_clusterCount = 0;
  assert(mapOf_vertexToCentroid1);
  for(uint i = 0; i < matrix->nrows; i++) {
    if(mapOf_vertexToCentroid1[i] != UINT_MAX) {
      k_clusterCount = macro_max(k_clusterCount, mapOf_vertexToCentroid1[i]);
    }
  }
  if(k_clusterCount == 0) {return T_FLOAT_MAX;} //! ie, as all vertices are in the same clsuter.
  k_clusterCount++; //! ie, as we itnerested in the 'count' and Not the max-index.
  if(k_clusterCount == UINT_MAX) {
    fprintf(stderr, "!!(odd-usage)\t Seems like you have requested an evlauation where nrows(%u) !> cnt_clusters(%u). If this is what you intended, then please ingore this observaiton. Oderwhise we suggesting invesitgiating the latter. OBservation at [%s]:%s:%d\n", matrix->nrows, k_clusterCount, __FUNCTION__, __FILE__, __LINE__);
    assert(false); // TODO: consider removing this
    return T_FLOAT_MAX;
  }
  if(matrix->nrows < k_clusterCount) {
    fprintf(stderr, "!!(odd-usage)\t Seems like you have requested an evlauation where nrows(%u) !> cnt_clusters(%u). If this is what you intended, then please ingore this observaiton. Oderwhise we suggesting invesitgiating the latter. OBservation at [%s]:%s:%d\n", matrix->nrows, k_clusterCount, __FUNCTION__, __FILE__, __LINE__);
    assert(false); // TODO: consider removing this
  }
  // printf("k_clusterCount=%u, at %s:%d\n", k_clusterCount, __FILE__, __LINE__);
  
  s_kt_matrix_cmpCluster_clusterDistance_t obj_local = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
  //assert(obj_dataConfig.k_clusterCount != 0); assert(obj_dataConfig.k_clusterCount != UINT_MAX);
  if(matrix->nrows == matrix->ncols) {
    init__s_kt_matrix_cmpCluster_clusterDistance(&obj_local, mapOf_vertexToCentroid1, matrix->nrows, matrix->ncols, k_clusterCount, self->matrix_unFiltered->matrix, self->ccm_config);
    //!
    //! Compute: 
    const t_float score = compute__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local, /*enum_id=*/self->ccm_enum,  /*cmp_type=*/self->ccm_type);
    
    //!
    //! De-allcoate:
    free__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local);		  
    //!
    //! @return:
    return score;
  } else {
    fprintf(stderr, "!!\t The inptu-amtrix is Not symmetirc, ie, please provide a symmetitrc iniput-matrix. For quesitons  pelase cotnact the senior delvoeper at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false); // FIXME: remove.
    return T_FLOAT_MAX;
  }
}
    
//! idneties the CCM-score for a "clusterResult" and updates the scalar_ccmBestScore if the CCM-score 'of this call' is better than "prev_ccmScore" (oekseth, 06. mar. 2017).
bool apply__CCM__updateBestScore__s_kt_clusterAlg_config__ccm_t(const s_kt_clusterAlg_config__ccm_t *self, const s_kt_clusterAlg_fixed_resultObject_t *clusterResult, const t_float prev_ccmScore, t_float *scalar_ccmBestScore) {
  assert(self);
  assert(clusterResult);
  //!
  //! Apply CCM-based evlauation: 
  uint *__mapOf_vertexBelongsToCluster = clusterResult->vertex_clusterId;
  if(__mapOf_vertexBelongsToCluster == NULL) {*scalar_ccmBestScore = T_FLOAT_MAX; return true;} //! ie, as we then assuem there were no idneitfed clsuters, ieg, due to non-central ndoes in the MCL clsuter-algortiuhm (oekseth, 06. jul. 2017).
  assert(__mapOf_vertexBelongsToCluster);
  {
    const s_kt_matrix_base_t *matrix = self->matrix_unFiltered;
    assert(matrix);
    assert(matrix->matrix);
    //fprintf(stderr, "nrows=%u, cnt_vertex=%u, at %s:%d\n", matrix->nrows, clusterResult->cnt_vertex, __FILE__, __LINE__);
    assert(matrix->nrows <= clusterResult->cnt_vertex); //! as we otherwise will have a memory-seg-fulat-error wrt. our dat-aaccesses.
  }
  const t_float ccm__local = apply__CCM__s_kt_clusterAlg_config__ccm_t(self, __mapOf_vertexBelongsToCluster);
  //!
  //! Comapre the result to 'the curent best-case':
  bool isImproved = false;
  const t_float ccm_worst_case_value = get_emptyCCM_score__s_kt_clusterAlg_config__ccm_t(self);
  if(ccm__local != T_FLOAT_MAX) {
    if(ccm_worst_case_value == T_FLOAT_MAX) {
      if(prev_ccmScore > ccm__local) {
	*scalar_ccmBestScore = ccm__local;
	isImproved = true;
      }
      // printf("compare (%f VS old=%f), where isImproved='%s', at %s:%d\n", ccm__local, prev_ccmScore, (isImproved) ? "true" : "false", __FILE__, __LINE__);
    } else if(ccm_worst_case_value == T_FLOAT_MIN_ABS) {
	if(prev_ccmScore < ccm__local) {
	  *scalar_ccmBestScore = ccm__local;
	  isImproved = true;
	}
	//printf("compare (%f VS old=%f), where isImproved='%s', at %s:%d\n", ccm__local, prev_ccmScore, (isImproved) ? "true" : "false", __FILE__, __LINE__);
    } else {assert(false);} //! ie, as we then need adding suppoirt or this 'use-case'.
  }
  /* if(isImproved) { */
  /* }   */
  //! ----------
  //!
  //! @return
  return isImproved;
}
