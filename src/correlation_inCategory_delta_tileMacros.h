#ifndef correlation_inCategory_delta_tileMacros_h
#define correlation_inCategory_delta_tileMacros_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file correlation_inCategory_delta_tileMacros
   @brief 
   - provide macros to simplify integraiton correlation-metric using our high-efficent approach, an approach which combiens SSE-intristnicts and use of emory-tiling.
   - wrt. the memory-tiling we expect the callers to have 'divied' the 'rows to process' into chunks: for details please see the users/callers 'of the amcro-fucntions listed in this file'
   @author Ole Kristian Ekseth (oekseth)
 **/

#include "correlation_macros__distanceMeasures.h"
#include "correlation_macros__distanceMeasures_wrapper.h"
//! -------------------------------------------------------------------------------------------------

//! -------------------------------------------------------------------------------------------------



#endif //! EOF
