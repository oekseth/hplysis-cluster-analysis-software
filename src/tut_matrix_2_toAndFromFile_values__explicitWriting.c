#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_matrix.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.

/**
   @brief evlauates the reading of a file which has been 'amnally' cosnturcted: otherise simliar to our "tut_matrix_1_toAndFromFile_values.c".
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks general: use logics in our "kt_matrix.h" 
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  const uint nrows = 10; const uint ncols = 10; 
  //!
  //! Allocate:
  s_kt_matrix_t mat_1  = initAndReturn__s_kt_matrix(nrows, ncols); 
  assert(mat_1.nrows == nrows);
  assert(mat_1.ncols == ncols);
  //!
  //! Set values:
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      //! Sepcify numbers to simplfiy de-buggin:
      mat_1.matrix[row_id][col_id] = (col_id) ? col_id : row_id;
    }
  }
  //! Export result:
  const char *fileName_matrix = "sampleMatrix.tsv";
  {
    FILE *fileP = fopen(fileName_matrix, "wb");
    assert(fileP);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {	
	char sep = '\t';
	if((col_id +1) == ncols) {
	  sep = '\n';
	} 
	fprintf(fileP, "%f%c", mat_1.matrix[row_id][col_id], sep);	
      }
    }
    fclose(fileP);
  }
  /* bool is_ok = export__singleCall__s_kt_matrix_t(&mat_1, fileName_matrix, NULL); */
  /* assert(is_ok); */
  //!
  //! Read input-file:
  s_kt_matrix_t mat_fromFile = readFromAndReturn__file__advanced__s_kt_matrix_t(fileName_matrix, initAndReturn__s_kt_matrix_fileReadTuning_t());
    
  //! 
  //! Comapre results: we expec tboth matrices to be equal:
  assert(mat_1.nrows == mat_fromFile.nrows);
  //  printf("%u VS %u, w/mat_fromFile[0][0]=%f, %s:%d\n", mat_1.ncols , mat_fromFile.ncols, mat_fromFile.matrix[0][0], __FILE__, __LINE__);  
  assert(mat_1.ncols == mat_fromFile.ncols);  
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      const t_float score_2 = mat_fromFile.matrix[row_id][col_id];
      assert(score_2 != T_FLOAT_MAX); //! ie, as we expec thte score to be 'set'.
      assert(mat_1.matrix[row_id][col_id] == score_2);
    }
  }

  //!
  //! De-allocates:
  free__s_kt_matrix(&mat_1);
  free__s_kt_matrix(&mat_fromFile);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

