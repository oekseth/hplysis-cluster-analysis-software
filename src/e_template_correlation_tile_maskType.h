#ifndef e_template_correlation_tile_maskType_h
#define e_template_correlation_tile_maskType_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @enum e_template_correlation_tile_maskType
   @brief provide logics for computation of ranks, lgoics to be used/integrated in all-against-all-compairosn
   @author Ole Kristina Ekseth (oekseth, 06. setp. 2016)
 **/
typedef enum e_template_correlation_tile_maskType {
  e_template_correlation_tile_maskType_mask_none,
  e_template_correlation_tile_maskType_mask_explicit, //! ie, then use a speerte mask-table.
  //e_template_correlation_tile_maskType_mask_implicit_emptyValue_0, //! ie, then use an implict mask: speifiec through the value of: "0"
  e_template_correlation_tile_maskType_mask_implicit, //! ie, then use an implict mask: speifiec through the value of: "T_FLOAT_MAX"
  e_template_correlation_tile_maskType_mask_undef,
} e_template_correlation_tile_maskType_t;

#define __macro__e_template_correlation_tile_maskType_mask_none 0
#define __macro__e_template_correlation_tile_maskType_mask_explicit 1
#define __macro__e_template_correlation_tile_maskType_mask_implicit 2
//#define __macro__


#endif //! EOF
