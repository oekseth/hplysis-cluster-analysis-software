#include "matrix_cmpAlg_ROC.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */




void get_forRow_ROC_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const t_float *arrOf_data, const t_float *arrOf_minThresholds,  const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float *arrOfResult__spec, t_float *arrOfResult__sens) {
  //! What we expect:
  assert(arrOf_truth); assert(arrOf_truth_size); assert(arrOf_data); assert(arrOf_minThresholds); assert(arrOf_minThresholds_size); assert(arrOfResult__spec); assert(arrOfResult__sens);


  // FIXME: cosnider using 'this' as a strategy to compare two correlation-matrices ... eg, to ....??...

  // FIXME: explain this proceudre, ie, valdiate logical correctness  ... how may may 'make use of the ROC' procedure ... and the lbirary 'this funciton is to be included in' (oekseth, 06. mai. 2016).
  // FIXME: iniclude into our ...??... the ROC-code from "https://github.com/Bioconductor-mirror/ROC/blob/master/src/ROC.cc" ... and include the 're-written veriosn of the latter' in the "cluster.c" 

#define __macroTypeOf_maskTo_use 0
#include "matrix_deviation___stub__ROC.c"
}

void get_forRow_ROC_typeOf_mask_implicit_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const t_float *arrOf_data, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float *arrOfResult__spec, t_float *arrOfResult__sens) {
  //! What we expect:
  assert(arrOf_truth); assert(arrOf_truth_size); assert(arrOf_data); assert(arrOf_minThresholds); assert(arrOf_minThresholds_size); assert(arrOfResult__spec); assert(arrOfResult__sens);
#define __macroTypeOf_maskTo_use 1
#include "matrix_deviation___stub__ROC.c"
}
void get_forRow_ROC_typeOf_mask_explicit_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const t_float *arrOf_data, const char *mask, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float *arrOfResult__spec, t_float *arrOfResult__sens) {
  //! What we expect:
  assert(arrOf_truth); assert(arrOf_truth_size); assert(arrOf_data); assert(arrOf_minThresholds); assert(arrOf_minThresholds_size); assert(arrOfResult__spec); assert(arrOfResult__sens);
#define __macroTypeOf_maskTo_use 2
#include "matrix_deviation___stub__ROC.c"
}

//! Compute ROC
void get_matrix_ROC_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const uint nrows, t_float **data, const t_float *arrOf_minThresholds,  const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float **matrixOfResult__spec, t_float **matrixOfResult__sens) {
  assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id++) {
    const t_float *arrOf_data = data[row_id];
    t_float *arrOfResult__spec = matrixOfResult__spec[row_id];
    t_float *arrOfResult__sens = matrixOfResult__sens[row_id];
    //! Compute:
#define __macroTypeOf_maskTo_use 0
#include "matrix_deviation___stub__ROC.c"
  }
}
//! Compute ROC using implicti masks
void get_matrix_ROC_typeOf_mask_implicit_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const uint nrows, t_float **data, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float **matrixOfResult__spec, t_float **matrixOfResult__sens) {
  assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id++) {
    const t_float *arrOf_data = data[row_id];
    t_float *arrOfResult__spec = matrixOfResult__spec[row_id];
    t_float *arrOfResult__sens = matrixOfResult__sens[row_id];
    //! Compute:
#define __macroTypeOf_maskTo_use 1
#include "matrix_deviation___stub__ROC.c"
  }
}
//! Compute ROC using explicit masks
void get_matrix_ROC_typeOf_mask_explicit_float__matrix_cmpAlg_ROC(const bool *arrOf_truth, const uint nrows, t_float **data, const char *mask, const t_float *arrOf_minThresholds, const uint arrOf_truth_size, const uint arrOf_minThresholds_size, t_float **matrixOfResult__spec, t_float **matrixOfResult__sens) {
  assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id++) {
    const t_float *arrOf_data = data[row_id];
    t_float *arrOfResult__spec = matrixOfResult__spec[row_id];
    t_float *arrOfResult__sens = matrixOfResult__sens[row_id];
    //! Compute:
#define __macroTypeOf_maskTo_use 2
#include "matrix_deviation___stub__ROC.c"
  }
}

