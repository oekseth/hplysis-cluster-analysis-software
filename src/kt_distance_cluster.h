#ifndef kt_distance_cluster_h
#define kt_distance_cluster_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_distance_cluster
   @brief provide lgoics for comparison of distance-matrices, eg, wrt. 'overall sunm' of distance-matrices.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"

#include "graphAlgorithms_aux.h"
#include "correlation_api.h"
#include "matrix_transpose.h"
#include "distance_2rows_slow.h"
#include "kt_distance_slow.h"
#include "e_kt_clusterComparison.h"
#include "kt_matrix.h"

//! Use a slwo procedure to infer the weights assicted to the rows and columns.
t_float* __calculate_weights_slow(const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weights[], const uint transpose, const char dist, const t_float cutoff, const t_float exponent);

/**
   @brief copmute the correlation-score for a given matrix.
   @param <metric_id> is the type of correlation to apply
   @param <typeOf_correlationPreStep> is the type of rpe-step to be used wrt. correlations
   @param <nrows> is the number of rows
   @param <ncols> is the number of columns
   @param <data> is the data-set to be evaluated
   @param <mask> if used sepcifies the set of interesting valeus: otehrwise we assume all valeus are of interest
   @param <weight> if used then we weight volumsn differently: otehrwise all cells are weighted euqally
   @param <transpose> which if set to "1" impleis that we evlauate he matrix wrt. its 'transposed' (ie, inverted) form, ie, to comapre wrt. teh columsn and Not wrt. the rows.
   @param <cutoff> the curt-off which is used in computation of weights, ie, in the post-processing after the correlation-metric is applied
   @param <exponent> The exponent to be used to calculate the weights. 
   @param <isTo_use_fastFunction> which if set to false imples that we use the 'legacy' slow function.
   @param <CLS> which is the block-sise of the tiles to use
   @param <iterationIndex_2> which if set 'narrows' the ros (of columns) to be indvestigated for data2.
   @return a pointer to a newly allocated array containing the calculated weights for the rows (iftranspose==0) or columns (if transpose==1). 
   @remarks This function calculates the weights using the weighting scheme proposed by Michael Eisen: 
   - w[i] = 1.0 / sum_{j where d[i][j]<cutoff} (1 - d[i][j]/cutoff)^exponent
   - where the cutoff and the exponent are specified by the user.
**/
t_float* calculate_weights__extensiveParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weights[], const uint transpose, const t_float cutoff, const t_float exponent,  const uint CLS, const uint iterationIndex_2);
//t_float* calculate_weights__extensiveParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weights[], const uint transpose, const t_float cutoff, const t_float exponent, const uint CLS, const uint iterationIndex_2);



//t_float* calculate_weights(const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weights[], const uint transpose, const char dist, const t_float cutoff, const t_float exponent, const bool isTo_use_fastFunction = true, const uint CLS = UINT_MAX, const uint iterationIndex_2 = UINT_MAX);

//! A slow pertmuation of the cluster-distance funciton: included for benchmarkin.
t_float clusterdistance_slow(uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const char dist, const char method, uint transpose, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed);

/**
   @brief infer thej distance between two clusters/point-clouds/cluster-clouds
   @remarks The clusterdistance routine calculates the distance between two clusters containing genes or microarrays using the measured gene expression vectors. The distance between clusters, given the genes/microarrays in each cluster, can be defined in several ways. Several distance measures can be used. 
   @remarks in below we describe a few number of the parameters:
   # "n1": The number of elements in the first cluster.
   # "n2": The number of elements in the second cluster.
   # "index1": Identifies which genes/microarrays belong to the first cluster.
   # "index2": Identifies which genes/microarrays belong to the second cluster.
   # "method" Defines how the distance between two clusters is defined, given which genes belong to which cluster:
   - method=='a': the distance between the arithmetic means of the two clusters
   - method=='m': the distance between the medians of the two clusters
   - method=='s': the smallest pairwise distance between members of the two clusters
   - method=='x': the largest pairwise distance between members of the two clusters
   - method=='v': average of the pairwise distances between members of the clusters
   # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
   # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
   @return The routine returns the distance in t_float precision. If the parameter transpose is set to a nonzero value, the clusters are interpreted as clusters of microarrays, otherwise as clusters of gene.
**/
t_float clusterdistance__extensiveParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const e_kt_clusterComparison_t method, uint transpose, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed, const uint CLS, const bool isTo_useImplictMask);


/**
   @brief infer thej distance between two clusters/point-clouds/cluster-clouds
   @remarks The clusterdistance routine calculates the distance between two clusters containing genes or microarrays using the measured gene expression vectors. The distance between clusters, given the genes/microarrays in each cluster, can be defined in several ways. Several distance measures can be used. 
   @remarks in below we describe a few number of the parameters:
   # "n1": The number of elements in the first cluster.
   # "n2": The number of elements in the second cluster.
   # "index1": Identifies which genes/microarrays belong to the first cluster.
   # "index2": Identifies which genes/microarrays belong to the second cluster.
   # "method" Defines how the distance between two clusters is defined, given which genes belong to which cluster:
   - method=='a': the distance between the arithmetic means of the two clusters
   - method=='m': the distance between the medians of the two clusters
   - method=='s': the smallest pairwise distance between members of the two clusters
   - method=='x': the largest pairwise distance between members of the two clusters
   - method=='v': average of the pairwise distances between members of the clusters
   # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
   # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
   @return The routine returns the distance in t_float precision. If the parameter transpose is set to a nonzero value, the clusters are interpreted as clusters of microarrays, otherwise as clusters of gene.
**/
t_float clusterdistance__extensiveParams__enumMethod(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const e_kt_clusterComparison_t enum_id_method, uint transpose, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed, const uint CLS, const bool isTo_useImplictMask);
//! A simplifed permtuation of "clusterdistance__extensiveParams__enumMethod(..)" (oekseth, 06. des. 2016)
t_float clusterdistance__extensiveParams__enumMethod__simplified(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], const uint n1, const uint n2, uint index1[], uint index2[], const e_kt_clusterComparison_t enum_id_method);




/**
   @brief comptue the scalar simlairty between two different matrices (oekseth, 06. jan. 2017)
   @param <clusterCmp_method_id> descirbes the type of cluster-comparison to be used/applied
   @param <obj_metric> is the type fo meitc-comparison to apply
   @param <data1> is the first matrix to use: we expect data1->ncols == data2->ncols
   @param <data2> is the second matrix to use.
   @param <scalar_result> the scalar describing the simliarty between the two matrices
   @return true if the oepraiton was a success.
 **/
bool scalar__getSimliarty__betwenMatrices__kt_distance_cluster(const e_kt_clusterComparison_t clusterCmp_method_id, const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *data1, const s_kt_matrix_t *data2, t_float *scalar_result);

#endif //! EOF
