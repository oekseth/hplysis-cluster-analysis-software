assert(self);
assert(superset);
  assert(self->nrows == 0);     assert(self->ncols == 0);
  assert(superset->nrows > 0);  assert(superset->ncols > 0);
//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //! Initaite:
  init__s_kt_matrix(self, /*nrows=*/superset->nrows, /*ncols=*/superset->ncols, /*isTo_allocateWeightColumns=*/false);  //! where the 'weights' are Not copied as we expect to use the 'ranks' in our data-set-copy'.
  assert(self->nrows == superset->nrows);     assert(self->ncols == superset->ncols);

//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

#if(__localConfig__copyWeights == 1)
if(superset->weight != NULL) { //! then copy the weights:
  for(uint local_id = 0; local_id < superset->ncols; local_id++) {      
    const uint global_vertex = local_id;  
    t_float score = 0;
    //! Udpate the 'new' object with the 'knowledge' from the superset-object.
    get_weight__s_kt_matrix(superset, global_vertex, &score);
    set_weight__s_kt_matrix(self, local_id, score);
  }    
 }
#endif

//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //!
  //! Udpate the name-mappigns:
  if(isTo_updateNames) {
    if(superset->nameOf_rows != NULL) {
      bool addFor_column = false;
      for(uint local_id = 0; local_id < self->nrows; local_id++) {      
	const uint global_vertex = local_id;
	char *stringTo_add = NULL;
	//printf("----\n (get-string), at %s:%d\n", __FILE__, __LINE__);
	get_string__s_kt_matrix(superset, global_vertex, addFor_column, &stringTo_add);
	assert(global_vertex < superset->nrows);
	assert(stringTo_add == superset->nameOf_rows[global_vertex]);
	if(stringTo_add && strlen(stringTo_add)) {
	  set_string__s_kt_matrix(self, local_id, stringTo_add, addFor_column);
	}
      }
    }
    //! ---------
    if(superset->nameOf_columns != NULL) {
      bool addFor_column = true;
      for(uint local_id = 0; local_id < self->ncols; local_id++) {      
	const uint global_vertex = local_id;
	char *stringTo_add = NULL;
	get_string__s_kt_matrix(superset, global_vertex, addFor_column, &stringTo_add);
	assert(global_vertex < superset->ncols);
	assert(stringTo_add == superset->nameOf_columns[global_vertex]);
	if(stringTo_add && strlen(stringTo_add)) {
	  set_string__s_kt_matrix(self, local_id, stringTo_add, addFor_column);
	}
      }
    }
  }

//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //! -----------------------
  //!
  //! Comptue the rank and thereafter insert
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
#if(__localConfig__useRanks == 1)
    //! Get the ranks:
    t_float *list_rank =get_rank__correlation_rank(self->ncols, superset->matrix[row_id]); 
    assert(list_rank);
    //! Insert the ranks:
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      self->matrix[row_id][col_id] = list_rank[col_id];
    }
    //! De-allcoat ethe locally reserved memory:
    free_1d_list_float(&list_rank); list_rank = NULL; //! ie, de-allcoate.
#else //! Then we apply a 'simple copy-operation':
    memcpy(self->matrix[row_id], superset->matrix[row_id], sizeof(t_float)*self->ncols);
#endif
  }

#undef __localConfig__copyWeights
#undef __localConfig__useRanks
