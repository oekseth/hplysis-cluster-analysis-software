#ifndef distance_2rows_slow_h
#define distance_2rows_slow_h
#include "graphAlgorithms_aux.h"
#include "correlation_api.h"
#include "matrix_transpose.h"
//#include "cluster.h" //! ie, to ensure that we do nto voersimpfly wr.t peromfrnac-eocmparison-aprpaoches
  /* /\** */
  /*    @brief compute the weighted euclidian distance between two rows or columns in a matrix. */
  /*    @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing. */
  /* **\/ */
  /* t_float euclid(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose); */
  /* /\** */
  /*    @brief compute the weighted "City Block" distance between two rows or columns in a matrix.  */
  /*    @remarks City Block distance is defined as the absolute value of X1-X2 plus the absolute value of Y1-Y2 plus..., which is equivalent to taking an "up and over" path. */
  /*    @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing. */
  /* **\/ */
  /* t_float cityblock(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose); */
  /**
     @brief compute the weighted Pearson distance between two rows or columns in a matrix. 
     @remarks We define the Pearson distance as one minus the Pearson correlation. This definition yields a semi-metric: 
     # d(a,b) >= 0, and d(a,b) = 0 iff a = b. but the triangular inequality d(a,b) + d(b,c) >= d(a,c) does not hold (e.g., choose b = a + c).
     @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
  **/
  t_float correlation(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose);
  /**
     @brief compute the weighted Pearson distance between two rows or columns, using the absolute value of the correlation.
     @return the Pearson distance.
     @remarks This definition (ie, in above) yields a semi-metric: d(a,b) >= 0, and d(a,b) = 0 iff a = b. but the triangular inequality d(a,b) + d(b,c) >= d(a,c) does not hold (e.g., choose b = a + c).
     @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
  **/
  t_float acorrelation(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose);
  /**
     @brief compute the weighted Pearson distance between two rows or columns, using the uncentered version of the Pearson correlation. 
     @remarks In the uncentered Pearson correlation, a zero mean is used for both vectors even if the actual mean is nonzero. This definition yields a semi-metric: d(a,b) >= 0, and d(a,b) = 0 iff a = b. but the triangular inequality d(a,b) + d(b,c) >= d(a,c) does not hold (e.g., choose b = a + c).
     @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
  **/
  t_float ucorrelation(const uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose);
  /**
     @brief compute the weighted Pearson distance between two rows or columns, using the absolute value of the uncentered version of the Pearson correlation. 
     @remarks The uacorrelation routine calculates In the uncentered Pearson correlation, a zero mean is used for both vectors even ifthe actual mean is nonzero. This definition yields a semi-metric: d(a,b) >= 0, and d(a,b) = 0 iff a = b. but the triangular inequality d(a,b) + d(b,c) >= d(a,c) does not hold (e.g., choose b = a + c).
     @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
  **/
  t_float uacorrelation(const uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose) ;

  /**
     @brief return a reference to the corresponding function.
     @param <dist> identifes the function-reference to retreive/return
     @return a fucntion-reference.
  **/
  static t_float(*setmetric_slow(char dist)) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) { 
    printf("k='%c' at %s:%d\n", dist, __FILE__, __LINE__);
    switch(dist)
      {
      case 'e': return &euclid;
      case 'b': return &cityblock;
      case 'c': return &correlation;
      case 'a': return &acorrelation;
      case 'u': return &ucorrelation;
      case 'x': return &uacorrelation;
      /* case 's': return &spearman; */
      /* case 'k': return &c_kendall; */
      default: return &euclid;
      }
    return NULL; // Never get here
  }

#endif //! EOF
