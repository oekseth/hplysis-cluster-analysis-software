{
  assert(objCaller_postProcessResult);

s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t  *local_objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t*)objCaller_postProcessResult;
  t_float *result = local_objCaller_postProcessResult->listOf_result_afterCorrelation; //! ie, the list to update.
  assert(result);
  //assert((SM % 4) == 0); //! otehrwise udpate [below].
  // assert(false); // FIXME: sub-divide [below] into 'sperate squares'.	    
	    
  const uint chunkSize_index2_iterSize = macro__get_maxIntriLength_float(chunkSize_index2);

  const t_float cutoff =  local_objCaller_postProcessResult->cutoff;
  const t_float exponent =  local_objCaller_postProcessResult->exponent;
  assert(exponent != 0); //! ie, as the call will otehrsie be errnous.
  const t_float cutoff_inv = 1/cutoff;

  // const VECTOR_FLOAT_TYPE vec_fixed_1 = VECTOR_FLOAT_SET1(1);
  // const VECTOR_FLOAT_TYPE vec_fixed_2 = VECTOR_FLOAT_SET1(2);
  // const VECTOR_FLOAT_TYPE vec_fixed_cutoff_inv = VECTOR_FLOAT_SET1(cutoff_inv);
  // const VECTOR_FLOAT_TYPE vec_fixed_exponent = VECTOR_FLOAT_SET1(exponent);
  // const VECTOR_FLOAT_TYPE vec_fixed_nan = VECTOR_FLOAT_DIV(VECTOR_FLOAT_SET1(0), VECTOR_FLOAT_SET1(1)); //! ie, to 'cosisntently tinrodcue' NAN-valeus
  // 
  // FIXME: move [”elow] cofnigruaiton thethe caller ... and then update our 'non-improvec code'.
  const bool isTo_updateAlsoFor_symmetric_elements = false;

  for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1++) {
    const uint global_index1 = index1 + local_index1; 

    //assert(false); // FIXME: is [”elow] index-range ocrrect?
    
    uint local_index2 = 0;
    //if(chunkSize_index2_iterSize > 0) {
    //VECTOR_FLOAT_TYPE vec_result_inner = VECTOR_FLOAT_SET1(0);
    //   for(; local_index2 < chunkSize_index2_iterSize; local_index2 += VECTOR_FLOAT_ITER_SIZE) {
    // 	const uint global_index2 = index2 + local_index2;
		  
    // 	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&listOf_inlineResults[local_index1][local_index2]);
    // 	VECTOR_FLOAT_TYPE vec_log_inp = VECTOR_FLOAT_SUB(vec_fixed_1, VECTOR_FLOAT_MUL(vec_input, vec_fixed_cutoff_inv));
    // 	VECTOR_FLOAT_TYPE vec_log_result = VECTOR_FLOAT_LOG(vec_log_inp);
    // 	VECTOR_FLOAT_TYPE vec_exp = VECTOR_FLOAT_EXP(VECTOR_FLOAT_MUL(vec_fixed_exponent, vec_log_result));
    // 	// FXIEM: when this 'code-chunk- works then 'move' [below] if-caluse outside the foor-loops
    // 	if(isTo_updateAlsoFor_symmetric_elements) { vec_exp = VECTOR_FLOAT_MUL(vec_exp, vec_fixed_2); }

    // 	//! REmvoe the NAN values:
    // 	assert(false); // FIXME: validate correctness of [”elow] ... and thereafter move [”elow] to our "def_intri.h" ... <-- consider also to update our artilce ... and other/similar 'appraoches'.
    // 	vec_exp = VECTOR_FLOAT_MAX(vec_exp, vec_fixed_nan);
    // 	//! Store the result:
    // 	VECTOR_FLOAT_STORE(&result[global_index2], VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOAD(&result[global_index2]), vec_exp));
    // 	//! Updat ethe temproary result-set:
    // 	vec_result_inner = VECTOR_FLOAT_ADD(vec_result_inner, vec_exp);
    //   }
    //   //! Update the result
    //   result[global_index1] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result_inner);
    // }
    for(; local_index2 < chunkSize_index2; local_index2++) {
      const uint global_index1 = index1 + local_index1; const uint global_index2 = index2 + local_index2;
      const t_float distance = listOf_inlineResults[local_index1][local_index2];
      macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(distance, cutoff, exponent, global_index1, global_index2, result, isTo_updateAlsoFor_symmetric_elements);      
    }
  }
}
