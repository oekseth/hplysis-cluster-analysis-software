static void __MiFa__wrapperTut__prefixName(syntetic)(__MiFa__wrapperTut__funcArgs) {
  //printf("at ... , %s:%d\n", __FILE__, __LINE__);
  assert(resultPrefix); assert(data_id_category_global);
#include "__apply__tut_dynamicK__syntetic_config.c"
  { 
#include "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.    
    MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment); //! ie, apply logics  (oekseth, 06. arp. 2017):    
  }
}
static void __MiFa__wrapperTut__prefixName(syntetic_noise)(__MiFa__wrapperTut__funcArgs) {
  assert(resultPrefix); assert(data_id_category_global);
#include "__apply__tut_dynamicK__syntetic_config.c"
  { 
#include "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.    
    MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment); //! ie, apply logics  (oekseth, 06. arp. 2017):    
  }
}
static void __MiFa__wrapperTut__prefixName(syntetic_differentDims)(__MiFa__wrapperTut__funcArgs) {
  assert(resultPrefix); assert(data_id_category_global);
#include "__apply__tut_dynamicK__syntetic_config.c"
  { 
#include "tut__aux__dataFiles__differentDimensions_wrapper.c"
    MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment); //! ie, apply logics  (oekseth, 06. arp. 2017):    
  }
}
static void __MiFa__wrapperTut__prefixName(realLife_1)(__MiFa__wrapperTut__funcArgs) {
  assert(resultPrefix); assert(data_id_category_global);
#include "__apply__tut_dynamicK__syntetic_config.c"
  { 
#include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
    MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment); //! ie, apply logics  (oekseth, 06. arp. 2017):    
  }
}
static void __MiFa__wrapperTut__prefixName(realLife_1_large)(__MiFa__wrapperTut__funcArgs) {
  assert(resultPrefix); assert(data_id_category_global);
#include "__apply__tut_dynamicK__syntetic_config.c"
  { 
#include "tut__aux__dataFiles__realMine_large.c" //! ie, the file which hold the cofniguraitosn to be used.
    MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment); //! ie, apply logics  (oekseth, 06. arp. 2017):    
  }
}
static void __MiFa__wrapperTut__prefixName(vincentarelbundock)(__MiFa__wrapperTut__funcArgs) {
  assert(resultPrefix); assert(data_id_category_global);
#include "__apply__tut_dynamicK__syntetic_config.c"
  { 
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
    MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment); //! ie, apply logics  (oekseth, 06. arp. 2017):    
  }
}
static void __MiFa__wrapperTut__prefixName(FCPS)(__MiFa__wrapperTut__funcArgs) {
  assert(resultPrefix); assert(data_id_category_global);
#include "__apply__tut_dynamicK__syntetic_config.c"
  { 
#include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
    MiF__cal__tut_ccm_13_categorizeData(nameOf_experiment); //! ie, apply logics  (oekseth, 06. arp. 2017):    
  }
}

//! ------------------------------------------------------------
//! 
//! Reset file-macros:
#undef __MiF__wrapperTut__fileSets__internalFunc
#undef __MiFa__wrapperTut__prefixName
#undef __MiFa__wrapperTut__funcArgs
