#include "measure_kt_clusterAlg_hca.h"


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure.h"
#include "maskAllocate.h"
#include "kt_clusterAlg_hca.h"
#include "measure_base.h"
#include "kt_api.h"
#include "kt_distance.h"

static void __measure_allAlgorithms(const uint nrows, const uint ncols, const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) {

  if(true == describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id))  {return;} // TODO: cosndier dropping 'this ciertiera'.
  if(isTo_use_MINE__e_kt_correlationFunction(metric_id) == true) {return;} // TODO: cosndier dropping 'this ciertiera'.


  const char default_value_char = 1;
  t_float **distMatrix = allocate_2d_list_float(nrows, ncols, default_value_char);
  t_float *weights = allocate_1d_list_float(nrows, default_value_char);
  t_float *transposed_weights = allocate_1d_list_float(ncols, default_value_char);
  uint **mask1 = allocate_2d_list_uint(nrows, ncols, default_value_char);
  
  const uint arrOf_hcaAlg_size = 4;
  const uint arrOf_hcaAlg[arrOf_hcaAlg_size] = {'s', 'm', 'a', 'c'};
  for(uint alg_id = 0; alg_id < arrOf_hcaAlg_size; alg_id++) {
    const char *stringOf_measureText_base = NULL;
    const char clusterMethod_char = arrOf_hcaAlg[alg_id];
    if(clusterMethod_char == 's') {
      stringOf_measureText_base = "Hierarhical clusters for hca-alg=single";
    } else if(clusterMethod_char == 'm') {
      stringOf_measureText_base = "Hierarhical clusters for hca-alg=max";
    } else if(clusterMethod_char == 'a') {
      stringOf_measureText_base = "Hierarhical clusters for hca-alg=average";
    } else if(clusterMethod_char == 'c') {
      stringOf_measureText_base = "Hierarhical clusters for hca-alg=centroid";
    } else {
      assert(false); //! ie, as we then need adding support 'for this'.
    }
    
	    
    for(uint transpose = 0; transpose < 2; transpose++) {
      for(char config_weight = 0; config_weight < 2; config_weight++) {
	for(char config_mask = 0; config_mask < 3; config_mask++) {
	  for(uint isTo_use_fastFunction = 0; isTo_use_fastFunction < 2; isTo_use_fastFunction++) {	
	    //const bool transpose = config_transpose;
	    uint **local_mask1 = NULL; 	
	    bool masks_isAllocated = false;
	    e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_true;
	    if(config_mask == 0) {
	      masksAre_used = e_cmp_masksAre_used_false;
	    } else if(config_mask == 2) {
	      if(transpose == false) {
		local_mask1 = mask1; 
	      } else {
		masks_isAllocated = true;
		local_mask1 = allocate_2d_list_uint(ncols, nrows, default_value_char);
	      }
	    }

	    	    
	    //! -------------------------------------------
	    //! Generate the measurement-test:
	    char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
	    char stringOf_measureText[6000]; memset(stringOf_measureText, '\0', 6000);
	    sprintf(stringOf_measureText, "%s: \"%s -- %s\"; transpose='%s',  use-weight='%s', mask-type='%s', use-fast-function='%s', %s", 
		    stringOf_measureText_base, 
		    
		    get_stringOf_enum__e_kt_correlationFunction_t(metric_id),
		    get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(typeOf_correlationPreStep),
		    (transpose) ? "true" : "false",
		    (config_weight) ? "true" : "false",
		    (config_mask == 2) ? "maskExplicit" : (config_mask == 1) ? "maskImplicit" : "mask-none",
		    //(config_mask == 1) ? "maskExplicit" :  "mask-none",
		    (isTo_use_fastFunction) ? "true" : "false",
		    stringOf_data_toAdd
		    );
	
	    t_float *local_weight = weights;
	    if(transpose) {local_weight = transposed_weights;}
	    t_float **local_distMatrix = distMatrix;
	    //bool distMatrix_isAllocated = false;
	    if(transpose) {
	      //distMatrix_isAllocated = true;
	      local_distMatrix = allocate_2d_list_float(ncols, nrows, default_value_char);
	    }
	    //! 
	    //! Start the clock:
	    start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	    
	    //! The call:
	    Node_t *arrOf_result = treecluster__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncols, /*local-data=*/NULL, local_mask1, local_weight, transpose, clusterMethod_char, local_distMatrix, /*isTo_use_continousSTripsOf_memory=*/true, /*isTo_useImplictMask=*/(!local_mask1 && (masksAre_used == e_cmp_masksAre_used_true)), isTo_use_fastFunction);
	    
	    
	    //! --------------
	    // printf("update time-measurement, at %s:%d\n", __FILE__, __LINE__);
	    __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(stringOf_measureText, nrows, ncols, T_FLOAT_MAX); //mapOf_timeCmp_forEachBucket[chunk_index]); 	  	    
	    assert(arrOf_result); free_1d_list_Node(&arrOf_result); arrOf_result = NULL;
	    if(masks_isAllocated) {
	      free_2d_list_uint(&local_mask1, ncols);
	    }
	    if(transpose) {
	      free_2d_list_float(&local_distMatrix, ncols);
	    }
	  }
	}
      }
    }
  }
  //! ---------------------------
  //!
  //! //! De-allocate:
  free_2d_list_float(&distMatrix, nrows);
  free_1d_list_float(&weights);
  free_1d_list_float(&transposed_weights);
  free_2d_list_uint(&mask1, nrows);
}


static void __measure_allAlgorithms__differentRowSiszes(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep) {

  for(uint i = 1; i < 20; i++) {
    const uint base_size = 256;
    const uint nrows = base_size*i;
    const uint ncols = base_size*i;
    const loint max_data_size = kilo * kilo * 100; //! ie, 100*1000*1000 elements to try using less than 1,000,000,000 GB of memory 
    if((nrows*ncols) > max_data_size) {
      fprintf(stderr, "The current row-size (%u)(%u) is outside the current max-member-threshold=%llu, ie, consider udpating the latter threadols (eg, if your a are running a large system than a latop with in-sufficient memoery, an observat ion at [%s]:%s:%d\n", nrows, ncols, max_data_size, __FUNCTION__, __FILE__, __LINE__);
      continue;
    }
    //! The call:
    __measure_allAlgorithms(nrows, ncols, metric_id, typeOf_correlationPreStep);
  }
}

static void __measure_allAlgorithms__allMatrix_multipleMatrixSizes() {
  for(uint metric_index = 0; metric_index < e_kt_correlationFunction_undef; metric_index++) {
    //const e_kt_correlationFunction metric_id = e_kt_correlationFunction_groupOf_squared_Pearson;
    e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_index;
    //e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
    if(false == describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id))  {
      for(uint data_mod = 0; data_mod < 3; data_mod++) {
	const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = (e_kt_categoryOf_correaltionPreStep_t)data_mod;
	if( (typeOf_correlationPreStep != e_kt_categoryOf_correaltionPreStep_none) && (isTo_use_kendall__e_kt_correlationFunction(metric_id) ) ) {
	  continue; //! ie, as we then assume 'this case' is not of itnerest to evlauate.
	}

	//! The call:
	__measure_allAlgorithms__differentRowSiszes(metric_id, typeOf_correlationPreStep);
      }
    }
  }
}


//! Then eprform a simpel test to evlauate the 'passing of itnernal assers' wrt. the k-means-clsuter-comparison.
static void simple_test() {
  //!
  //! Intaite the data:
  const uint nrows = 10; const uint ncols = 10;
  s_kt_correlationMetric_t obj_metric;
  const e_kt_correlationFunction_t metric_id  = e_kt_correlationFunction_groupOf_minkowski_euclid;  const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_binary;    
  init__s_kt_correlationMetric_t(&obj_metric, metric_id, typeOf_correlationPreStep);
  s_kt_matrix obj_1;
  const bool isto_allocateweightcolumns = false;
  init__s_kt_matrix(&obj_1, nrows, ncols, isto_allocateweightcolumns);
  const bool isTo_transposeMatrix = false;
  { //! Intiate the matrix:
    assert(obj_1.matrix);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	obj_1.matrix[row_id][col_id] = col_id;
      }
    }
  }
  //! -------------------------------------------------
  //! -------------------------------------------------
  //! ---------   Perform the cluster-analsysis:       ----------------------------------------
  { printf("# Compute HCA for \%s\", at [%s]:%s:%d\n", "single", __FUNCTION__, __FILE__, __LINE__);
    s_kt_clusterAlg_hca_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&obj_result, nrows);
    const e_kt_clusterAlg_hca_type_t cluster_method = e_kt_clusterAlg_hca_type_single; //! which 'does not make use of Knitting-Tools new data-scheme for HCA-speed-up'
    //! The call:    
    kt_matrix__hca(obj_metric, &obj_1, &obj_result, cluster_method, init__simple__s_kt_api_config_t(isTo_transposeMatrix)); 
    //! ------------------------------------
    //!
    //! De-allocate:
    free__s_kt_clusterAlg_hca_resultObject_t(&obj_result); 
  } //! -------------------- 
  { printf("# Compute HCA for \%s\", at [%s]:%s:%d\n", "max", __FUNCTION__, __FILE__, __LINE__);
    s_kt_clusterAlg_hca_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&obj_result, nrows);
    const e_kt_clusterAlg_hca_type_t cluster_method = e_kt_clusterAlg_hca_type_max; //! which 'does not make use of Knitting-Tools new data-scheme for HCA-speed-up'
    //! The call:    
    kt_matrix__hca(obj_metric, &obj_1, &obj_result, cluster_method, init__simple__s_kt_api_config_t(isTo_transposeMatrix)); 
    //! ------------------------------------
    //!
    //! De-allocate:
    free__s_kt_clusterAlg_hca_resultObject_t(&obj_result); 
  } //! -------------------- 
  { printf("# Compute HCA for \%s\", at [%s]:%s:%d\n", "average", __FUNCTION__, __FILE__, __LINE__);
    s_kt_clusterAlg_hca_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&obj_result, nrows);
    const e_kt_clusterAlg_hca_type_t cluster_method = e_kt_clusterAlg_hca_type_average; //! which 'does not make use of Knitting-Tools new data-scheme for HCA-speed-up'
    //! The call:    
    kt_matrix__hca(obj_metric, &obj_1, &obj_result, cluster_method, init__simple__s_kt_api_config_t(isTo_transposeMatrix)); 
    //! ------------------------------------
    //!
    //! De-allocate:
    free__s_kt_clusterAlg_hca_resultObject_t(&obj_result); 
  } //! -------------------- 
  { printf("# Compute HCA for \%s\", at [%s]:%s:%d\n", "centroid", __FUNCTION__, __FILE__, __LINE__);
    s_kt_clusterAlg_hca_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&obj_result, nrows);
    const e_kt_clusterAlg_hca_type_t cluster_method = e_kt_clusterAlg_hca_type_centroid; //! which 'does not make use of Knitting-Tools new data-scheme for HCA-speed-up'
    //! The call:    
    kt_matrix__hca(obj_metric, &obj_1, &obj_result, cluster_method, init__simple__s_kt_api_config_t(isTo_transposeMatrix)); 
    //! ------------------------------------
    //!
    //! De-allocate:
    free__s_kt_clusterAlg_hca_resultObject_t(&obj_result); 
  } //! -------------------- 

  //! ------------------------------------
  //!
  //! De-allocate:
  free__s_kt_matrix(&obj_1);
}

//! The main-function to test our "measure_codeStyle_functionRef_api"
void main__kt_clusterAlg_hca(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r

  if(isTo_processAll || (0 == strncmp("simple_test", array[2], strlen("simple_test"))) ) {
    simple_test();
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("all-hcaAlgorithms", array[2], strlen("all-hcaAlgorithms"))) ) {

    const e_kt_correlationFunction_t metric_id  = e_kt_correlationFunction_groupOf_minkowski_euclid;  const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_binary;  
    const uint nrows = 1024; const uint ncols = 1024;
    __measure_allAlgorithms(nrows, ncols, metric_id, typeOf_correlationPreStep);
    cnt_Tests_evalauted++;
  }


  if(isTo_processAll || (0 == strncmp("all-hcaAlgorithms--differentRowSizes", array[2], strlen("all-hcaAlgorithms--differentRowSizes"))) ) {

    const e_kt_correlationFunction_t metric_id  = e_kt_correlationFunction_groupOf_minkowski_euclid;  const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_binary;  
    //const uint nrows = 1024; const uint ncols = 1024;
    __measure_allAlgorithms__differentRowSiszes(metric_id, typeOf_correlationPreStep);
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("all-hcaAlgorithms-andAllMetrics", array[2], strlen("all-hcaAlgorithms-andAllMetrics"))) ) {
    __measure_allAlgorithms__allMatrix_multipleMatrixSizes();
    cnt_Tests_evalauted++;
  }



  /* if(isTo_processAll || (0 == strncmp("", array[2], strlen(""))) ) { */

  /*   cnt_Tests_evalauted++; */
  /* } */
  




  //! ------------------------------------------------------------------------------------------------------------------
  //! Warn  if no parameters matched:
  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}
