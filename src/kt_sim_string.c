#include "kt_sim_string.h"
/* #include<stdio.h> */
/* #include<string.h> */
 
/* void longest_common_subsequence_algorithm(); */
/* void print_sequence(int a, int b); */

 
/* int main() */
/* { */
/*   printf("\nEnter the First String:\t"); */
/*   scanf("%s", first_sequence); */
/*   printf("\nEnter the Second String:\t"); */
/*   scanf("%s", second_sequence); */
/*   printf("\nLongest Common Subsequence:\t"); */
/*   longest_common_subsequence_algorithm(); */
/*   print_sequence(size_1, size_2); */
/*   printf("\n"); */
/*   return 0; */
/* } */

//! Comptue the "LCS(..)".
//! @return the longest common sub-seuqneucne (LCS). 
static t_float compute__kt_sim_string__LCS(const char *first_sequence, const char *second_sequence) {
  assert(first_sequence); assert(second_sequence);
  const uint size_1 = strlen(first_sequence);
  const uint size_2 = strlen(second_sequence);
  assert(size_1 > 0);
  assert(size_2 > 0);

  
  //  assert(false); // FIXME: use a different allcoation than [”elow] ...   storing only for the previous columnand previus row.
  //int a, b, c, d;
  const uint empty_0 = 0;
  uint **temp = allocate_2d_list_uint((size_1+1), (size_2+1), empty_0);
  /* char first_sequence[30], second_sequence[30], longest_sequence[30][30]; */


  //! Intiate the first row and first column:
  for(uint a = 0; a <= size_1; a++) {       temp[a][0] = 0;     }
  for(uint a = 0; a <= size_2; a++) {       temp[0][a] = 0;     }                                
  //! 
  for(uint a = 1; a <= size_1; a++)  {
    for(uint b = 1; b <= size_2; b++) {
      if(first_sequence[a - 1] == second_sequence[b - 1]) { //! then an overlap:
	temp[a][b] = temp[a - 1][b - 1] + 1; //! ie, update the count.
	//	longest_sequence[a][b] = 'c';
      } else if(temp[a - 1][b] >= temp[a][b - 1]) { //! then 'go' from the previous-row.
	temp[a][b] = temp[a - 1][b]; //! ie, same-count.
	//	longest_sequence[a][b] = 'u';
      } else {
	temp[a][b] = temp[a][b - 1]; //! ie, same-count.
	//	longest_sequence[a][b] = 'l';
      }
    }      
  }
  //!
  //! @return the longest common sub-seuqneucne
  const t_float max_size = (t_float)temp[size_1][size_2];
  /* assert(false); // FIXME: vlaidte correcntess of [above]. */

  /* assert(false); // FIXME[concept]: ... Pairwise-similarity-metric ... sum-of-scores ... where we compare (obj1.head == obj2.head) ... where score-distance is set to metric(obj1.tail, obj2.tail) .... ie, a 'simple' permtuation of sparse-simalirty-metirc-ocmtautions. */
  /* assert(false); // FIXME[concept]: ... Pairwise-similarity-metric ...  */
  /* assert(false); // FIXME[concept]: ...  */

  assert(temp);
  free_2d_list_uint(&temp, (size_1+1)); temp = NULL;
  return max_size;
}

/* void print_sequence__LCS(int a, int b) { */
/*   if(a == 0 || b == 0)     { */
/*       return; */
/*     } */
/*   if(longest_sequence[a][b] == 'c')     { */
/*       print_sequence(a - 1, b - 1); */
/*       printf("%c", first_sequence[a - 1]); */
/*     } */
/*   else if(longest_sequence[a][b] == 'u')     { */
/*       print_sequence(a - 1, b); */
/*     } */
/*   else     { */
/*       print_sequence(a, b - 1); */
/*     } */
/* } */


static t_float compute__kt_sim_string__LevenshteinDistance(const char *first_sequence, const char *second_sequence) {
  assert(first_sequence); assert(second_sequence);
  const uint size_1 = strlen(first_sequence);
  const uint size_2 = strlen(second_sequence);
  const uint empty_0 = 0;
  uint **dist = allocate_2d_list_uint((size_1 + 1), (size_2 + 1), empty_0);
  assert(dist);
  // int d[sl + 1][tl + 1];
  for (uint i = 0; i <= size_1; i++) {
    dist[i][0] = i;
  }
  for (uint i = 0; i <= size_2; i++) {
    dist[0][i] = i;
  }
  for (uint i = 1; i <= size_1; i ++) {
    for (uint j = 1; j <= size_2; j ++) {      
      if (first_sequence[i - 1] == second_sequence[j - 1]) {
	dist[i][j] = dist[i - 1][j - 1];
      }
      else {
	dist[i][j] = macro_min(dist[i - 1][j] + 1,  /* deletion */
		      macro_min(dist[i][j - 1] + 1,  /* insertion */
			  dist[i - 1][j - 1] + 1));    /* substitution */
      }
    }
  }
    const t_float max_size =  (t_float)dist[size_1][size_2];
    free_2d_list_uint(&dist, (size_1+1));
    return max_size;
  //  return d[sl][tl];
}
//! Comptue the "Levenshtein-Distance(..)".
//! @return the Levenshtein-Distance. 
static t_float __erronous__compute__kt_sim_string__LevenshteinDistance(const char *first_sequence, const char *second_sequence) {
  assert(first_sequence); assert(second_sequence);
  const uint size_1 = strlen(first_sequence);
  const uint size_2 = strlen(second_sequence);
  assert(size_1 > 0);
  assert(size_2 > 0);

  assert(false); // FIXME: seems like [”elow] routine rpdocue/gneerate enrorus results .... ie, try intrsopecting upon [”elow] .... 

  const uint empty_0 = 0;
  // FIXME: add support for [below] .... then generate/cosntruct a test-case wehre we evaluate for [”elow] ... 
#define __MiC__localScoreFunction 0
  //! Allocate vecotrs to hodl the path to choose: 
  uint *v0 = allocate_1d_list_uint((size_2 + 1), empty_0);
  uint *v1 = allocate_1d_list_uint((size_2 + 1), empty_0);
#if(__MiC__localScoreFunction == 1)
  t_float *v0_0 = allocate_1d_list_float((size_2 + 1), empty_0);
  t_float *v1_0 = allocate_1d_list_float((size_2 + 1), empty_0);
#endif
/* function LevenshteinDistance(char s[1..m], char t[1..n]): */
/* // create two work vectors of integer distances */
/*     declare int v0[n + 1] */
/*     declare int v1[n + 1] */

    // initialize v0 (the previous row of distances)
    // this row is A[0][i]: edit distance for an empty s
    // the distance is just the number of characters to delete from t
  for(uint m = 0; m < size_2; m++) {     
    v0[m] = m;       
#if(__MiC__localScoreFunction == 1)
    v0_s[m] = m;       
#endif
  }
  for(uint i = 0; i < (size_1-1); i++) {      
    // calculate v1 (current row distances) from the previous row v0
    
    // first element of v1 is A[i+1][0]
    //   edit distance is delete (i+1) chars from s to match empty t
    v1[0] = i + 1;
#if(__MiC__localScoreFunction == 1)
    v1_s[0] = 0;
#endif
    for(uint m = 0; m < (size_2-1); m++) {     
    // use formula to fill in the rest of the row      
      uint substitutionCost = 0;
      if(first_sequence[i] != second_sequence[m]) {
	substitutionCost = 1;
      }
      //      printf("[%u]--[%u]: subst=%u, at %s:%d\n", i, m, substitutionCost, __FILE__, __LINE__);
      const uint score_right = v0[m + 1] + 1;
      uint best_score = score_right;
#if(__MiC__localScoreFunction == 1)
      t_float best_score_sum = v0_s[m+1] + local_score;
#endif
      const uint score_left = v1[m] + 1;
      if(score_left < score_right) {
	best_score = score_left;
#if(__MiC__localScoreFunction == 1)
	best_score_sum = v1[m] + local_score;
#endif
	printf("\t [%u]--[%u]: subst=%u, best_score=%u, at %s:%d\n", i, m, substitutionCost, best_score, __FILE__, __LINE__);
      }
      const uint score_match = v0[m] + substitutionCost;
      if(score_match < best_score) {
	printf("\t [%u]--[%u]: subst=%u, best_score=[%u VS %u], at %s:%d\n", i, m, substitutionCost, best_score, score_match, __FILE__, __LINE__);
	best_score = score_match;
#if(__MiC__localScoreFunction == 1)
	best_score_sum = v0[m] + local_score;
#endif
      }
      v1[m + 1] = best_score;
      printf("[%u]--[%u]: subst=%u, best_score=%u, at %s:%d\n", i, m, substitutionCost, best_score, __FILE__, __LINE__);
#if(__MiC__localScoreFunction == 1)
      v1_s[m+1] = best_score_sum;
#endif
      //      v1[m + 1] = macro_min(macro_min(v1[m] + 1, v0[m + 1] + 1), v0[m] + substitutionCost);
    }
    
    // copy v1 (current row) to v0 (previous row) for next iteration    
    uint *v0_tmp = v0; v0 = v1; v1 = v0_tmp; //! ie, swap(..).
    assert(v0 != v1);
    assert(v0 != v0_tmp);
#if(__MiC__localScoreFunction == 1)
    t_float *v0_s_tmp = v0_s; v0_s = v1_s; v1_s = v0_s_tmp; //! ie, swap(..).
#endif
  }
  // after the last swap, the results of v1 are now in v0
  //!
  //! @return the longest common sub-seuqneucne
#if(__MiC__localScoreFunction == 1)
  const t_float max_size = (t_float)v0_s[size_2];
  assert(v0_s != v1_s);
  free_1d_list_float(&v0_s);
  free_1d_list_float(&v1_s);
#else
  const t_float max_size = (t_float)v0[size_2];
#endif
  printf("score(max)=%f, size(strings)=[%u, %u], given str=[\"%s\" VS \"%s\"], at %s:%d\n", max_size, size_1, size_2, first_sequence, second_sequence, __FILE__, __LINE__);
  assert(v0 != v1);
  free_1d_list_uint(&v0);
  free_1d_list_uint(&v1);
  return max_size;
}

//! Comptue the "Damerau-Levenshtein-Distance(..)".
//! @return the Damerau-Levenshtein-Distance. 
t_float compute__kt_sim_string__DL_DamerauLevenshteinDistance(const char *first_sequence, const char *second_sequence) {
  assert(first_sequence); assert(second_sequence);
  const uint size_1 = strlen(first_sequence);
  const uint size_2 = strlen(second_sequence);
  assert(size_1 > 0);
  assert(size_2 > 0);
  const uint empty_0 = 0;
  // FIXME: add support for [below] .... then generate/cosntruct a test-case wehre we evaluate for [”elow] ... 
#define __MiC__localScoreFunction 0
  //! Allocate vecotrs to hodl the path to choose: 
  uint *da = allocate_1d_list_uint((size_2 + 1), empty_0);
  //  uint *v1 = allocate_1d_list_uint((size_2 + 1), empty_0);
#if(__MiC__localScoreFunction == 1)
  /* t_float *v0_0 = allocate_1d_list_float((size_2 + 1), empty_0); */
  /* t_float *v1_0 = allocate_1d_list_float((size_2 + 1), empty_0); */
#endif
  //  unsigned int lev_dam_dist(std::string s1,  std::string s2)

  /* size_t size1 = s1.size(); */
  /* size_t size2 = s2.size(); */
  //  const uint empty_0 = 0;
  uint **dist = allocate_2d_list_uint((size_1 + 1), (size_2 + 1), empty_0);
  assert(dist);
  for (uint i = 0; i <= size_1; i++) {
    dist[i][0] = i;
  }
  for (uint i = 0; i <= size_2; i++) {
    dist[0][i] = i;
  }

    for (uint i = 1; i <= size_1; i ++) {
      for (uint j = 1; j <= size_2; j ++) {      
	uint cost = (first_sequence[i - 1] == second_sequence[j - 1]) ? 0 : 1 ;
	if ( (i > 1) && (j > 1) && (first_sequence[i] == second_sequence[j - 1]) && (first_sequence[i - 1] == second_sequence[j])) {
	  uint score_min = dist[i - 1][j];
	  uint score_alt = dist[i][j - 1] + 1; 
	  if(score_min > score_alt) { //! htne udpat ehte min-score.
	    score_min = score_alt;
	  }
	  score_alt = dist[i][j] + cost;
	  if(score_min > score_alt) { //! htne udpat ehte min-score.
	    score_min = score_alt;
	  }
	  score_alt = dist[i - 2][j - 2];
	  if(score_min > score_alt) { //! htne udpat ehte min-score.
	    score_min = score_alt;
	  }	  
	  //! 
	  //! Update: 
	  dist[i][j] = score_alt;
	} else {
	  uint score_min = dist[i][j -1] + 1;
	  uint score_alt = dist[i - 1][j] + 1;
	  if(score_min > score_alt) { //! htne udpat ehte min-score.
	    score_min = score_alt;
	  } 
	  score_alt = dist[i - 1][j - 1] + cost;
	  if(score_min > score_alt) { //! htne udpat ehte min-score.
	    score_min = score_alt;
	  } 
	  //! 
	  //! Update: 
	  dist[i][j] = score_alt;
	}
      }
    }
    const t_float max_size =  (t_float)dist[size_1][size_2];
    free_2d_list_uint(&dist, (size_1+1));
    return max_size;
}





/* static int max(int x, int y) { */
/*   return x > y ? x : y; */
/* } */

/* static int min(int x, int y) { */
/*   return x < y ? x : y; */
/* } */

static t_float jaro_winkler_distance(const char *s, const char *a, const bool computeFor_winkler) {

  /* assert(false); // FIXME[below]: ... re-write the code ...  */
  /* assert(false); // FIXME[below]: ... scaffodl hwot eh metric may be implemtned/permtuated into our pairwise-similarity-metric */

#define SCALING_FACTOR 0.1
  //  uint i, j, l;
  const uint sl = strlen(s);
  const uint al = strlen(a);
  uint sflags[sl], aflags[al];
  uint range = macro_max(0, macro_max(sl, al) / 2 - 1);


  if (!sl || !al)
    return 0.0;

  for (uint i = 0; i < al; i++)
    aflags[i] = 0;

  for (uint i = 0; i < sl; i++)
    sflags[i] = 0;

  /* calculate matching characters */
  uint m = 0;
  for (uint i = 0; i < al; i++) {
    uint l = 0;
    for (uint j = macro_max(i - range, 0), l = macro_min(i + range + 1, sl); j < l; j++) {
      if( (a[i] == s[j]) && !sflags[j] ){
	sflags[j] = 1;
	aflags[i] = 1;
	m++;
	break;
      }
    }
  }

  if (!m)
    return 0.0;

  /* calculate character transpositions */
  uint l = 0;   uint t = 0;
  for (uint i = 0; i < al; i++) {
    if (aflags[i] == 1) {
      uint j = 0;
      for (j = l; j < sl; j++) {
	if (sflags[j] == 1) {
	  l = j + 1;
	  break;
	}
      }
      if (a[i] != s[j])
	t++;
    }
  }
  t /= 2;

  /* Jaro distance */
  t_float dw = (((t_float)m / sl) + ((t_float)m / al) + ((t_float)(m - t) / m)) / 3.0;
  if(computeFor_winkler == false) {
    return dw; //! ie, no need for idneifying the 'common-prefix.
  }
  /* calculate common string prefix up to 4 chars */
  //  uint l = 0;
  for (uint i = 0; i < macro_min(macro_min(sl, al), 4); i++)
    if (s[i] == a[i])
      l++;

  /* Jaro-Winkler distance */
  dw = dw + (l * SCALING_FACTOR * (1 - dw));

  return dw;
}


//! @return the string-based simalrity-metric.
t_float compute__kt_sim_string(const s_kt_sim_string_t *self, const e_kt_sim_string_type_t enum_id, const char *s1, const char *s2) {
  assert(s1); assert(strlen(s1) > 1);
  assert(s2); assert(strlen(s2) > 1);
  t_float ret_val = T_FLOAT_MAX;
  if(enum_id == e_kt_sim_string_type_lcs) {
    ret_val = compute__kt_sim_string__LCS(s1, s2);
  } else if(enum_id == e_kt_sim_string_type_LevenshteinDistance) {
    ret_val = compute__kt_sim_string__LevenshteinDistance(s1, s2);
  } else if(enum_id == e_kt_sim_string_type_DL_DamerauLevenshteinDistance) {
    ret_val = compute__kt_sim_string__DL_DamerauLevenshteinDistance(s1, s2);
  } else if(enum_id == e_kt_sim_string_type_jaro) {
    ret_val = jaro_winkler_distance(s1, s2, /*computeFor_winkler=*/false);
  } else if(enum_id == e_kt_sim_string_type_jaro_winkler) {
    ret_val = jaro_winkler_distance(s1, s2, /*computeFor_winkler=*/true);
    //  } else if(enum_id == ) {
  } else {
    fprintf(stderr, "!!\t Not supported for metric=\"%s\", at [%s]:%s:%d\n", getString__e_kt_sim_string_type_t(enum_id), __FUNCTION__, __FILE__, __LINE__);
    return T_FLOAT_MAX;
  }
  if(self && self->isTo_normalize_result) {
    if(ret_val != 0) {
      assert(ret_val != T_FLOAT_MAX);
      const uint size_1 = strlen(s1);
      const uint size_2 = strlen(s2);
      assert(size_1 > 0);
      assert(size_2 > 0);
      //! Normalize:
      ret_val = 1 - (ret_val /macro_max(size_1, size_2)); 
    } else {ret_val = 1;}
  }
  return ret_val;
}


s_kt_list_2d_kvPair_t setOf__compute__kt_sim_string(s_kt_sim_string_t *self, const e_kt_sim_string_type_t enum_id, const s_kt_list_2d_kvPair_filterConfig_t *config_filter, s_kt_list_1d_string_t *strObj_1, s_kt_list_1d_string_t *strObj_2) {
  assert(self); assert(strObj_1); assert(strObj_2);
  assert(strObj_1->nrows);
  assert(strObj_2->nrows);
  s_kt_list_2d_kvPair_t obj_result = init__s_kt_list_2d_kvPair_t(strObj_1->nrows, 10);
  //setToEmpty__s_kt_list_2d_kvPair_t();
  //  const t_float *weight = NULL;  // if(strObj_weight && strObj_weight->list_size) {weight = obj_weight->list;}
  //! ---------------------
  //!
  //! Get the metic:
  //!
  //! Iterate:
  const char *prev_str_1 = NULL;
  for(uint head_id = 0; head_id < strObj_1->nrows; head_id++) {
    const char *str_1 = strObj_1->nameOf_rows[head_id];
    if(str_1 && strlen(str_1)) {
      //! Note: in [below] we itnroduce/add seperate logic to handle isseus where muliple rows are the same, ie, a feature observed for some data-sets, ie, a local optmzioatn-strategy wrt. exueicont-time.
      if(prev_str_1 && 
	 (
	  (strlen(str_1) == strlen(prev_str_1)) && 
	  (0 == strcmp(str_1, prev_str_1) ) ) ) { //! then we copy the 'old' results:      
	assert(head_id > 0);
	if(self->isTo_in2dCompute_includeInResult_idsWithSameString) {
	  if((head_id-1) < obj_result.list_size) { //! then the may be data to add:
	    // printf("(merge)\t \"%s\" and \"%s\", at %s:%d\n", str_1, prev_str_1, __FILE__, __LINE__);
	    s_kt_list_1d_kvPair_t *obj_row_prev = &(obj_result.list[head_id-1]);
	    assert(head_id < obj_result.list_size);
	    s_kt_list_1d_kvPair_t *obj_row = &(obj_result.list[head_id]);
	    assert(obj_row);
	    assert(obj_row_prev);
	    bool isAdded = false;
	    const uint current_pos = obj_row_prev->list_size;
	    for(uint i = 0; i < current_pos; i++) {
	      if(false == MF__isEmpty__s_ktType_kvPair(obj_row_prev->list[i])) { //! ie, to reflect [above]. 
		// TODO: cosnider wriitng a new fucntion to avoid [”elow] fucniton-calls from causing a performanc-eoverhead.
		assert(false == MF__isEmpty__s_ktType_kvPair(obj_row_prev->list[i])); //! ie, to reflect [above].

		assert(false); // FIXME: remove

		push__s_kt_list_1d_kvPair_t(obj_row, obj_row_prev->list[i]);
		isAdded = true;
	      }
	    }
	    
	    if(isAdded) {

	      assert(false); // FIXME: remove

	      obj_result.current_pos = head_id + 1;	
	    }
	  }
	} //! else we do Not add.
      } else {
	prev_str_1 = str_1;
	//!
	//! Add data:
	//!
	//! Iterate:
	bool head_isAdded = false; t_float sim_score_prev = T_FLOAT_MAX;
	uint local__config_cntMax_afterEach = config_filter->config_cntMax_afterEach; //! where latter is adjsuted in [”elow] based on local observiaotns.
	for(uint tail_id = 0; tail_id < strObj_2->nrows; tail_id++) {
	  const char *str_2 = strObj_2->nameOf_rows[tail_id];
	  if(str_2 && strlen(str_2)) {
	    //!
	    //! Comptue simalirty:
	    //! ---------------------
	    //!
	    //! Intalise:  
	    //! ---------------------
	    //! 
	    //! Apply logics:
	    t_float sim_score = T_FLOAT_MAX;
	    bool isTo_ignore = false;
	    if(tail_id > 0) { //! then investigate fi the string-count is to be ingored.
	      const char *str_2_prev = strObj_2->nameOf_rows[tail_id-1];
	      if(str_2_prev && strlen(str_2_prev)) {
		if(strlen(str_2_prev) == strlen(str_2)) {
		  if(0 == strcmp(str_2, str_2_prev)) {
		    isTo_ignore = true;
		    sim_score = sim_score_prev;
		    if(self->isTo_in2dCompute_includeInResult_idsWithSameString == true) {
		      local__config_cntMax_afterEach++; //! ie, as we for cases where 'muliple ros are the same' are interested in including these without missing/forgetting 'the others'.
		    }
		  }
		}
	      }
	    }
	    if( (sim_score != T_FLOAT_MAX) && (self->isTo_in2dCompute_includeInResult_idsWithSameString == false) ) {
	      continue; //! ie, as this string is then arelady evaluated. 
	    }
	    if( (self->isTo_in2dCompute_includeInResult_idsWithSameString == false) 
		&& (strlen(str_1) == strlen(str_2))
		&& (0 == strcmp(str_1, str_2)) ) {continue;} //! ie, as a self-comparison is then udnerstodo to have limtied/none values 'what-so-ever'. 
	    //if(isTo_ignore == true) {continue;} //! ie, as this string is then arelady evaluated. 
	    const uint size_1 = strlen(str_1); const uint size_2 = strlen(str_2);
	    if( 
	       (self->in2dCompute_threshold_relative_minMax_stringLengthDiff != 0.0)
	       && (self->in2dCompute_threshold_relative_minMax_stringLengthDiff != T_FLOAT_MAX)
	       && ( macro_max(size_1, size_2)/macro_min(size_1, size_2) ) > self->in2dCompute_threshold_relative_minMax_stringLengthDiff)
	      { 
		continue; //! ie, as we then assume the string may safely be ingored/omitted from comaprsion.
	      }
	    if( sim_score == T_FLOAT_MAX) {
	    //	    t_float 
	      if( (strlen(str_1) > 3) && (strlen(str_2) > 3) ) {
		sim_score_prev = sim_score = compute__kt_sim_string(self, enum_id, str_1, str_2);	      
	      }
	    }
	    //	  printf("(cmp)\t \"%s\" VS  \"%s\", at %s:%d\n", str_1, str_2, __FILE__, __LINE__);
	    //!
	    //! Insert into local list: 
	    //	  const uint current_pos = get_currrentPos__s_kt_set_2dsparse_t(&result_sparse);
	    /* uint arr_id_size = 0; */
	    /* const uint *arr_id = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&result_sparse, /\*row_id=*\/0, &arr_id_size); */
	    /* uint arr_scores_size = 0; */
	    /* const t_float *arr_scores = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&result_sparse, /\*row_id=*\/0, &arr_scores_size); */
	    /* assert(arr_id_size == arr_scores_size); */
	    //printf("scores for[\"%s\", \"%s\"]: ", str_1, str_2);
	    //assert(arr_id_size <= 1); //! ie, as we assume only the 'tail' is inserted.
	    if(sim_score != T_FLOAT_MAX) {
	      //	  for(uint i = 0; i < arr_scores_size; i++) {
	      //	    const uint v_id = arr_id[i];
	      if(!config_filter || (sim_score < config_filter->config_ballSize_max) ) {
		//printf("%u, ", v_id);
		// printf("(cmp)\t \"%s\" VS  \"%s\": %f, at %s:%d\n", str_1, str_2, v_score, __FILE__, __LINE__);
		//  printf("\t score=%u, at %s:%d\n", 		
		const s_ktType_kvPair_t obj_this = MF__initVal__s_ktType_kvPair(tail_id, sim_score);
		assert(false == MF__isEmpty__s_ktType_kvPair(obj_this));
		//! Add:
		push__s_kt_list_2d_kvPair_t(&obj_result, head_id, obj_this);
		head_isAdded = true;
	      }
	    }
	    //	  printf(", at %s:%d\n", __FILE__, __LINE__);
	    //! De-allcoate lcoal-result-set:
	    //	  free_s_kt_set_2dsparse_t(&result_sparse);
	  }
	}
	if(config_filter && head_isAdded) { //! then filter the vertices based on the critera in question. 
	  //      if(config_filter && obj_row) { //! then filter the vertices based on the critera in question. 
	  s_kt_list_1d_kvPair_t *obj_row = &(obj_result.list[head_id]);
	  assert(obj_row);
	  uint current_pos = obj_row->list_size;
	  bool is_notSet = true;
	  const uint current_pos_prev = current_pos;
	  for(uint i = 0; (i < current_pos_prev) && is_notSet; i++) {
#define index_last (current_pos_prev - i) - 1
	    if(MF__isEmpty__s_ktType_kvPair(obj_row->list[index_last]) && is_notSet) {assert(index_last < obj_row->list_size); current_pos = index_last; }
	    else {is_notSet = false;} //! ie, then 'stop' the update-sequence
#undef index_last
	  }
	  for(uint i = 0; i < current_pos; i++) {
	    //	    printf("[%u], given current_pos=%u, current_pos_prev=%u,  at %s:%d\n", i, current_pos, current_pos_prev, __FILE__, __LINE__);
	    assert(false == MF__isEmpty__s_ktType_kvPair(obj_row->list[i])); //! ie, to reflect [above].
	  }
	  obj_row->current_pos = current_pos; //! ie, update.
	  if( (current_pos > config_filter->config_cntMin_afterEach) || (config_filter->config_cntMin_afterEach == UINT_MAX) ) {
	    //! 
	    if(current_pos > local__config_cntMax_afterEach) { //! then we sort the data-set by the score and then remove the 'm' 'worst' elements:
	      sort__s_kt_list_1d_kvPair_t(obj_row); //! ie, sort by the 'scores' rather than the 'keys'.
	      //!
	      //! Remove vertices 'abvove the threshold':
	      for(uint i = /*nn_numberOfNodes=*/local__config_cntMax_afterEach; i < /*cnt_added=*/current_pos; i++) {
		obj_row->list[i] = MF__init__s_ktType_kvPair();
		assert(MF__isEmpty__s_ktType_kvPair(obj_row->list[i])); //! ie, to reflect [above].
	      }
	      assert(local__config_cntMax_afterEach <= current_pos);
	      current_pos = local__config_cntMax_afterEach;
	    }
	  }
	  obj_row->current_pos = current_pos; //! ie, update.
#ifndef NDEBUG
	  //! Validate that all of the vertieces 'are inside the range':
	  for(uint i = 0; i < current_pos; i++) {
	    assert(false == MF__isEmpty__s_ktType_kvPair(obj_row->list[i])); //! ie, to reflect [above].
	    const uint tail_id = obj_row->list[i].head; //! ie, the tail-idnex in the (tail, score) pair.
	    assert(tail_id != UINT_MAX);
	    assert(tail_id < strObj_2->nrows);
	  }
#endif	  
	  //! 
	  //! Resize the lsit, ie, to reduce memory-consumption:
	  //if((current_pos *2) < obj_row->list_size) 
	  { //! then we 'comrpess' the meomry-size-usage:
	    assert(current_pos > 0);
	    if(current_pos > 0) {
	      setTo_size__s_kt_list_1d_kvPair_t(obj_row, current_pos);
	    } else {
	      free__s_kt_list_1d_kvPair_t(obj_row);
	    }
	  }
	}
	if(head_isAdded) {
	  obj_result.current_pos = head_id + 1;
	}
      }
    }
  }

  //!
  //! De-allcoate result:

  
  //!
  //! @return
  return obj_result;
}
