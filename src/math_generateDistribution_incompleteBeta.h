#ifndef math_generateDistribution_incompleteBeta_h
#define math_generateDistribution_incompleteBeta_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file math_generateDistribution_incompleteBeta
   @brief provide logics for comptaution of the "incomplete-beta" distribution
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"

//! @return the result of copmtauting student-t-stribution
t_float studenttdistribution(const uint ncols, t_float score);

//! @return the result of computing the inverse student-t-stribution
t_float invstudenttdistribution(const uint ncols, t_float score);

#endif //! EOF
