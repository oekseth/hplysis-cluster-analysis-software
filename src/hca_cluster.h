#ifndef hca_cluster_h
#define hca_cluster_h

/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */
#include "s_dense_closestPair.h"

  /**
     @file hca_cluster
     @brief an example-appløication for how our Hierarchical Cluster-Algorithm Memory (HCA-mem) algorithm provides a boost to perofrmance.
     @author Ole Kristian Ekseth (oekseth).
  **/



  /**
     @brief an hca_node object represents a signle node in a tree created by hierarchical clustering. 
     @remarks The tree can be represented by an array of n hca_node structs, where n is the number of elements minus one. The integers left and right in each hca_node struct refer to the two elements or subnodes that are joined in this node. The original elements are numbered 0..nelements-1, and the nodes -1..-(nelements-1). For each node, distance contains the distance between the two subnodes that were joined.
 **/
typedef struct hca_node {
  uint left; uint right; t_float distance;
  //hca_node() : left(UINT_MAX), right(UINT_MAX), distance(0) {};
} hca_node_t;



//! The __cplusplus macro was included in this document by oekseth to make it possible to link to KnittingTools C++-library.
#ifdef __cplusplus
extern "C"{
#endif 
 




  /**
     @brief translates a tree of vertices into clusters based on the hierarchical clutering-results.
     @param <nelements> The number of elements that were clustered.
     @param <tree> The clustering solution. Each node in the array describes one linking event, with tree[i].left and tree[i].right representig the elements that were joined. The original elements are numbered 0..nelements-1, nodes are numbered -1..-(nelements-1). Size set to "nelements - 1"
     @param <nclusters> The number of clusters to be formed.
     @param <clusterid> The number of the cluster to which each element was assigned. Space for this array should be allocated before calling the cuttree routine. If a memory error occured, all elements in clusterid are set to -1. Size set to "nelements"
     @remarks The cuttree routine takes the output of a hierarchical clustering routine, and divides the elements in the tree structure into clusters based on the hierarchical clustering result. The number of clusters is specified by the user.
  **/
  void hca_cutTree(const uint nelements, hca_node_t* tree, const uint nclusters, uint clusterid[]);

  /**
     @brief Compute Pairwise Maximum- (complete-) Linking (PML) cluster
     @param <nrows> the number of rows in the distmatrix input-data-set
     @param <ncols> the number of columns in the distmatrix input-data-set
     @param <distmatrix> the inptu-data-set.
     @return a cluster-memership-index for each vertex: call our "hca_cutTree(..)" funciton to infer the cluster-ids.
  **/
  hca_node_t* hca_computeCluster_pml(const uint nrows, const uint ncols, float** distmatrix);

  /**
     @brief Comptue Pairwise Average Linking (PAL) on the given distance matrix.
     @param <nrows> the number of rows in the distmatrix input-data-set
     @param <ncols> the number of columns in the distmatrix input-data-set
     @param <distmatrix> the inptu-data-set.
     @return a cluster-memership-index for each vertex: call our "hca_cutTree(..)" funciton to infer the cluster-ids.
  **/
    hca_node_t* hca_computeCluster_pal(const uint nrows, const uint ncols, float** distmatrix);
#ifdef __cplusplus
}
#endif

#endif //! EOF
