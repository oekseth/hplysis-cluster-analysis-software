#ifndef aux_findPartitionFor_tiles_h
#define aux_findPartitionFor_tiles_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file aux_findPartitionFor_tiles
   @brief provide lgocis for partitioning a block-sized partioin into tiles
   @author Ole Kristian Ekseth (oekseth, 06. nov. 2016).
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"

/**
   @struct s_aux_findPartitionFor_tiles
   @brief store the result of a block-partiion of tiles wrt. matrix-operations (oesketh, 06. nov. 2016).
 **/
typedef struct s_aux_findPartitionFor_tiles {
  uint nrows;
  uint cntBlocks; 
  uint sizeEachBlock_beforeLast; //! ie, the 'fized' SM-size.
  uint sizeEachBlock_last;
  //uint nrows_1;   uint nrows_2; //! ie, as we are itnerested in hadnlign different matrix-szies for the two evaluated input-matrices.
  /* uint ncols; */
  /* //! --------- */

  /* uint cntBlocks_r1;   uint cntBlocks_r2; //! ie, for the two 'row' cases: */
  /* uint cntBlocks_c; //! ie, for the column-case. */
} s_aux_findPartitionFor_tiles_t;

/**
   @brief intiates the lgocial container for the tiled block (oekseth, 06. nov. 2016)
   @param <nrows> is the numberof elements (eg, rows or columns) to be 'used' in the 'tiled portion'.
   @param <SM> is the block-size of each tile: the last 'tiled block' is epxected to have a block-size less-than-or-equal to SM (ie, a lgocisla 'part' of this function).
   @return the initated s_aux_findPartitionFor_tiles_t object.
 **/
s_aux_findPartitionFor_tiles_t initAndGet__s_aux_findPartitionFor_tiles_t(const uint nrows, const uint SM);


//! @return the current block-size of the element:
static inline uint __attribute__((gnu_inline, always_inline, artificial))
//static inline uint __attribute__((__gnu_inline__, __always_inline__, __artificial__))
get_blockSize__s_aux_findPartitionFor_tiles_t(const s_aux_findPartitionFor_tiles_t *self, const uint block_id) {
  assert_possibleOverhead(self);
  assert_possibleOverhead(self->cntBlocks >= 1);
  assert_possibleOverhead(block_id < self->cntBlocks);
  if(block_id != (self->cntBlocks - 1)) {return self->sizeEachBlock_beforeLast;}
  else {return self->sizeEachBlock_last;}
}
//! @return the number of blocks to evlauate:
static uint get_numberOfCellsToEvaluate__s_aux_findPartitionFor_tiles_t(const s_aux_findPartitionFor_tiles_t *self) {
  assert_possibleOverhead(self);
  uint cnt_rows_evaluated = 0;
  if(self->cntBlocks > 1) { cnt_rows_evaluated += (self->sizeEachBlock_beforeLast * (self->cntBlocks - 1)); }
  cnt_rows_evaluated += self->sizeEachBlock_last;
  //! 
  //! @return
  return cnt_rows_evaluated;
}

#ifndef NDEBUG
//! A fucntion used to vlaidate correctness of this structure.
void debug_applyInternalCorrectnessTests();
#endif

#endif //! EOF
