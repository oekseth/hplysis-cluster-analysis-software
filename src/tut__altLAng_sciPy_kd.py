print(__doc__)

import numpy as np

from sklearn.cluster import DBSCAN
from sklearn import cluster
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import NearestNeighbors
from sklearn.neighbors import KDTree

import time

#nrows = 1000*1000
nrows = 1000*10
#nrows = 750*10
#nrows = 750*40
ncols = 5
#ncols = 20
#ncols = 512
#ncols = 30000
#100 000 000 = 10^8 = (10^4)^2
# centers = [[1, 1, 1, 10], [-1, -1, -1, 20], [1, -1, -1, 30], [1, -1, 1, 40], [-1, -1, 1, -90]]
# #centers = [[1, 1, 1], [-1, -1, -1], [1, -1, 1]]
# #centers = [[1, 1], [-1, -1], [1, -1]]
# X, labels_true = make_blobs(n_samples=nrows, centers=centers, cluster_std=0.4,
#                             random_state=0)
X = np.random.rand(nrows, ncols)
#print("len(X)=%d, len(X[0])=%d" % len(X), len(X[0]))
#X = StandardScaler().fit_transform(X)
t0 = time.time()
kdt = KDTree(X, leaf_size=12, metric='euclidean')
#nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(X)
#!
#! Complete:
#X = np.random.rand(nrows, ncols)
t1 = time.time()
print('Time(construct): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
#!
#!
cnt_calls = 0
for i in xrange(nrows):
    dist, ind = kdt.query([X[i]], k=10)                
    cnt_calls = cnt_calls+1

t1 = time.time()
print('Time(query): %.2fs' % (t1 - t0)).lstrip('0')
print('Count(query-vertices): %d' % cnt_calls)
#!
#! DB-SCAN:
t0 = time.time()
#dbscan = cluster.DBSCAN(algorithm='kd_tree')
#dbscan = cluster.DBSCAN(eps=.2, metric='euclid', algorithm='kd_tree')
#dbscan = cluster.DBSCAN(eps=.2, metric='cosine', algorithm='kd_tree')
#dbscan = cluster.DBSCAN(eps=.2, metric='kulsinski', algorithm='kd_tree')
dbscan = cluster.DBSCAN(eps=.2, metric='kulsinski', algorithm='brute')
#dbscan = cluster.DBSCAN(eps=.2, metric='canberra', algorithm='kd_tree')
#dbscan = cluster.DBSCAN(metric='canberra', algorithm='brute')
#dbscan = cluster.DBSCAN(eps=.2, algorithm='kd_tree')
dbscan.fit(X)
t1 = time.time()
print('Time(dbSCAN): %.2fs' % (t1 - t0)).lstrip('0')
#    dbscan_brute_1 = cluster.DBSCAN(eps=.2, metric='euclidean', algorithm='brute')

#    dbscan_canberra_brute_1 = cluster.DBSCAN(eps=.2, metric='canberra', algorithm='brute')
