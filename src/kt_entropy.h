#ifndef kt_entropy_h 
#define kt_entropy_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


//									listOf_values_forIndex_midPoint[(int)val];})
// #define log_index_fast(val) ({assert((int)val < listOf_values_forIndex_size); assert(listOf_values_forIndex_midPoint); listOf_values_forIndex_midPoint[(int)val];})

//#define log_index(val) ({assert((uint)val < listOf_values_forIndex_size); listOf_values_forIndex[(uint)val];})
//#define log_index_fast(val) (log(val))

#include "def_intri.h"
#include "kt_matrix.h"
#include "correlation_macros__distanceMeasures.h"

/**
   @struct s_kt_entropy
   @brief provide a wrapper for entropy-comptuations (oekseth, 06. jun. 2017).
 **/
typedef struct s_kt_entropy {
  uint listOf_values_forIndex_size;   uint listOf_values_size;
  t_float *listOf_values_forIndex; t_float *listOf_values_forIndex_midPoint; 
  t_float *listOf_values;
  t_float *listOf_values_shallowCopy_aboveZero;
  VECTOR_FLOAT_TYPE vec_fast_log_defaultValue;
  //! -----------
  t_float adjustment_log_inv;
  //! -----------
  bool config__useNaiveLogComputationAppraoch;
  t_float val_adjust; //! ie, the "a" value/parameter in the Enotpry-paakcage.
  t_float value_power; //! eg, used for "e_kt_entropy_genericType_Simpson_generic"
} s_kt_entropy_t;
//! Define a fast acccestor to the object
#define MF__s_kt_entropy__log_index_fast(self, val) ({ assert((int)val < self->listOf_values_forIndex_size); self->listOf_values_forIndex_midPoint[(int)val];})
//#define MF__s_kt_entropy__log_index_fast(self, val) ({ assert((int)val < self->listOf_values_forIndex_size); self->listOf_values_forIndex_midPoint[(int)val];})
//! Apply post-scaling: 
#define __MF__s_kt_entropy__postAdjustment() ({if(isTo_applyPostScaling) {sum_H = sum_H * self->adjustment_log_inv; }}) /*! eg, sum_H / log(10);*/ 
// FIXME[SSE]: add support for SSE in [”elow] ...
#define __MF__s_kt_entropy__sum(arr, arr_size) ({t_float sum = 0; for(uint i = 0; i < arr_size; i++) {/*printf("arr[%u]=%u, at %s:%d\n", i, (uint)arr[i], __FILE__, __LINE__); assert(arr[i] != T_FLOAT_MAX);*/ sum = (t_float)macro_pluss(sum, (t_float)arr[i]);} /*printf("sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);*/ sum;}) //! ie, reutnr the sum of the vlaues.
#define __MF__s_kt_entropy__sum__uint(arr, arr_size) ({uint sum = 0; for(uint i = 0; i < arr_size; i++) {sum = macro_pluss(sum, arr[i]);} sum;}) //! ie, reutnr the sum of the vlaues.
//! Extend "__MF__s_kt_entropy__sum(...)" with logics to idnetify the number of cases which only a signle '1' conunt.
#define __MF__s_kt_entropy__sum__andSingleTons__uint(arr, arr_size) ({uint sum = 0; for(uint i = 0; i < arr_size; i++) {sum = macro_pluss(sum, arr[i]); if(arr[i] == 1) {sum_count1++;}} sum;}) //! ie, reutnr the sum of the vlaues.
#define __MF__s_kt_entropy__sum__countAbove0__uint(arr, arr_size) ({uint sum = 0; for(uint i = 0; i < arr_size; i++) {sum = macro_pluss(sum, (uint)arr[i]); if(arr[i] > 0) {sum_aboveZero++;}} sum;}) //! ie, reutnr the sum of the vlaues.
//__MF__s_kt_entropy__sum() ({t_float sum = 0; 
//! Apply the emprical entropy-meausre.
#define __MF__s_kt_entropy__metric_empirical() ({ \
    t_float log_score_sum = mathLib_float_log(sum); \
    t_float score_sum_inv = 1.0/(t_float)sum; /*! ie, to use mutlipclaiton instead of idvion, thereby optmziehng the comtpautions. */ \
    assert(sum != 0); \
    assert(isinf(score_sum_inv) == false); \
    for(uint i = 0; i < arr_size; i++) {				\
      const t_float score_raw = (t_float)__MiF__getScore();		\
      assert(isOf_interest(score_raw)); \
      /*const t_float log_score_1 = MF__s_kt_entropy__log_index_fast(self, score_raw); */ \
      const t_float log_score_1 = __MiF__getScore_log();		\
      const t_float log_score = log_score_1-log_score_sum; /*! ie, log(value/sum) */ \
      /*printf("\t\t sum_H = %f - (%f * %f * %f), at %s:%d\n", sum_H, score_sum_inv, score_raw, log_score, __FILE__, __LINE__); */ \
      sum_H -= score_sum_inv*(t_float)score_raw*log_score;			\
    } \
    /*! Post-scaling: b*/ \
    if(false) {printf("sum_H=%f, score_sum_inv=%f, log_score_sum=%f, arr_size=%u, isTo_applyPostScaling=%d, at %s:%d\n", sum_H, score_sum_inv, log_score_sum, arr_size, isTo_applyPostScaling, __FILE__, __LINE__);} \
    __MF__s_kt_entropy__postAdjustment(); })

/**
   @enum e_kt_entropy_logType
   @brief idenitfes the type of lgoarithm to be sued (oekseht, 06. jun. 2017).
 **/
typedef enum e_kt_entropy_logType {
  e_kt_entropy_logType_log10,
  e_kt_entropy_logType_log2
  //e_kt_entropy_logType_
} e_kt_entropy_logType_t;

/**
   @enum e_kt_entropy_Dirichlet
   @brief desicbe differnet coefficeints to be used wrt. diivsion. 
 **/
typedef enum e_kt_entropy_Dirichlet {
  e_kt_entropy_Dirichlet_none, //! a=0.
  e_kt_entropy_Dirichlet_Jeffreys, //! a=0.5
  e_kt_entropy_Dirichlet_Laplace, //! a=1
  e_kt_entropy_Dirichlet_SG, //! $a = 1/length(arr)$
  // e_kt_entropy_Dirichlet_minMax, //! $a = sqrt(sum(arr))/length(arr)$
  /* e_kt_entropy_Dirichlet_, //! a= */
  /* e_kt_entropy_Dirichlet_, */
} e_kt_entropy_Dirichlet_t;


/**
   @enum e_kt_entropy_genericType
   @brief dienfes entropy-metircs supproted by the hpLysis-entropy library (oesketh, 06. jul. 2017).
 **/
typedef enum e_kt_entropy_genericType {
  e_kt_entropy_genericType_ML_Shannon,
  e_kt_entropy_genericType_MM_MillerMadow,
  e_kt_entropy_genericType_CS_ChaoShen,
  //! Note: "Dirichlet prior Bayesian estimators of entropy, mutual information and related quantities" [entropy-R-package].
  e_kt_entropy_genericType_Dirichlet_Jeffreys,
  e_kt_entropy_genericType_Dirichlet_Laplace,
  e_kt_entropy_genericType_Dirichlet_SG,
  e_kt_entropy_genericType_Dirichlet_minimax,
  e_kt_entropy_genericType_shrink,
  // ------------ entropy-meausres Not foudn in the R-Entropy-package:
  // FIXME: udpate our aritlce wr.t [”elow] .... emphasisign the importance of such support .... eg, wrt. a niav eimpemtatnion ....
  // FIXMe[concpetual]: how may we comapre different entopry-meausres .... eg, wrt. evaluation of result-accuracy?
  e_kt_entropy_genericType_Simpson, 
  e_kt_entropy_genericType_Simpson_generic, 
  e_kt_entropy_genericType_Gini, 
  e_kt_entropy_genericType_Hulbert_permutation, 
  e_kt_entropy_genericType_HCDT, 
  e_kt_entropy_genericType_Hill, 
  e_kt_entropy_genericType_Renyi, 
  e_kt_entropy_genericType_PT_1, 
  e_kt_entropy_genericType_PT_2, 
  e_kt_entropy_genericType_AD, 
  e_kt_entropy_genericType_HHI, 
  //  e_kt_entropy_genericType_, 
  // ---------------------------------------------------------
  /* e_kt_entropy_genericType_shrink_ */
  /* e_kt_entropy_genericType_shrink_ */
  /* e_kt_entropy_genericType_, */
  /* e_kt_entropy_genericType_, */
  e_kt_entropy_genericType_undef
} e_kt_entropy_genericType_t;

#define __isCase(enum_case) enum_case == enum_entropy
//! @return the string-represetnation of the enum-id in queisont.
const char *get_stringOfEnum__e_kt_entropy_genericType_t(const e_kt_entropy_genericType_t enum_entropy);
//! @return the string-represetnation of the enum-id in queisont.
e_kt_entropy_genericType_t get_enumForString__e_kt_entropy_genericType_t(const char *str_cmp);


static s_kt_entropy_t init__s_kt_entropy(const uint listOf_values_forIndex_size, const uint listOf_values_size, const bool mineAccuracy_computeLog_useAbs, const bool optmize_use_logValues_fromLocalTable, const e_kt_entropy_logType_t log_type, const e_kt_entropy_logType_t log_type_postScaling, const e_kt_entropy_Dirichlet_t type_adjustEach, const uint arr_size) {
  //! Note: the "listOf_values_size" is used ...   for which we comptue ... "log(m*n) = log(m) + log(n)" and "log(m*/n) = log(m) - log(n)" ... store the results in a new list "c_log[i] = log(c[i])" and "c_log_inv[i] = 1/c_log[i];" and "cumhist_log[i][s] = log(cumhist[i][s]);" <-- tids-kostnad vil kunne redusess fra 'random access' til 'linear access', dvs. ....??...
  s_kt_entropy_t self; self.listOf_values_forIndex_size = listOf_values_forIndex_size;
  self.listOf_values_size = listOf_values_size;
  // FIXME: set "self.config__useNaiveLogComputationAppraoch" by defulat to false ... ipment supprot for this ... update artilce wrt. 'such a strategy' ... scaffold aritlce-captions .... then write tut-perofmrance-tests ... 
  // FIXME: ensure taht [”elow] "self.config__useNaiveLogComputationAppraoch" is used ... eg, in combiantino with "SG" ... 
  self.config__useNaiveLogComputationAppraoch = true; 
  // FIXME: explian the 'poiint/usage of  [ªbove] ... and wirte a simplfied constrcutro ... to idnfy 'ranges' ... tehn writign different tu-exases elvauting 'this'.
  
  self.listOf_values_forIndex = NULL; 
  self.listOf_values_forIndex_midPoint = NULL; 
  self.listOf_values = NULL;
  self.listOf_values_shallowCopy_aboveZero = NULL;
  self.val_adjust = 0;
  self.value_power = 0.5; //! ie, the defualt vlaue if used.
  if(type_adjustEach == e_kt_entropy_Dirichlet_Jeffreys) {self.val_adjust = 0.5;}
  if(type_adjustEach == e_kt_entropy_Dirichlet_Laplace) {self.val_adjust = 1;}
  else {self.val_adjust = 1.0/(t_float)arr_size;}
  //! -------------------------------------------------------------
  //! -------------------------------------------------------------
  //! -------------------------------------------------------------
  if(log_type == e_kt_entropy_logType_log10) {
#define __MiF__log(val) ({log(val);})
    assert(listOf_values_size > 0);
#include "kt_entropy___stub__init.c"
  } else if(log_type == e_kt_entropy_logType_log2) {
    const t_float log_2_score = log(2);
#define __MiF__log(val) ({t_float l_tmp = log(val); if(l_tmp != 0) {l_tmp = l_tmp/log_2_score;} l_tmp;})
  assert(listOf_values_size > 0);
#include "kt_entropy___stub__init.c"
  } else {
    assert(false);
  }
  if(log_type_postScaling == e_kt_entropy_logType_log10) {
    self.adjustment_log_inv = 1/(t_float)log(10);
  } else if(log_type_postScaling == e_kt_entropy_logType_log2) {
    self.adjustment_log_inv = 1/(t_float)log(2);
  } else {
    assert(false);
    self.adjustment_log_inv = 1;
  }

  //! ----------------------------------------------
  //! 
  //! @return
  return self;
}

/**
   @brief provide a simplfied intiation-interface to our "s_kt_entropy_t" (oekseth, 06. jul. 2017).
   @param <max_theoreticCountSize> is the max-possbile count-sum whihc may be encountered in the inptu-data.
   @param <vector_length> is the average/expected vector-length for each vertex.
   @param <enum_entropy> is the entopry-type be intiated for, eg, wrt. the "a" parameter used in Dirichlet-entropy-emtics.
   @return a new-cosntructed object
 **/
static s_kt_entropy_t initSimple__s_kt_entropy(const uint max_theoreticCountSize, const uint vector_length, const e_kt_entropy_genericType_t enum_entropy) {
  e_kt_entropy_Dirichlet_t type_adjustEach = e_kt_entropy_Dirichlet_none;
  if(enum_entropy == e_kt_entropy_genericType_Dirichlet_Jeffreys) {type_adjustEach = e_kt_entropy_Dirichlet_Jeffreys;}
  else if(enum_entropy == e_kt_entropy_genericType_Dirichlet_Laplace) {type_adjustEach = e_kt_entropy_Dirichlet_Laplace;}
  else if(enum_entropy == e_kt_entropy_genericType_Dirichlet_SG) {type_adjustEach = e_kt_entropy_Dirichlet_SG;}
  //else if(enum_entropy == e_kt_entropy_genericType_Dirichlet_minimax) {type_adjustEach = e_kt_entropy_Dirichlet_minMax;}
  assert(max_theoreticCountSize > 0);
  const uint size = max_theoreticCountSize+1;
  if(size == 0) {printf("(kt-entropy)\t max_theoreticCountSize=%u=%f, size=%u=%d, max(uint)=%u, max(uint|float)=%u\n", max_theoreticCountSize, (t_float)max_theoreticCountSize, size, (int)size, UINT_MAX, (uint)T_FLOAT_MAX);}
  assert(max_theoreticCountSize != UINT_MAX);
  assert(max_theoreticCountSize < UINT_MAX);
  assert(size > 0);
  return init__s_kt_entropy(/*listOf_values_forIndex_size=*/size,
			    /*listOf_values_size=*/size,  // FIXME: is this correct?
			    /*mineAccuracy_computeLog_useAbs=*/true,
			    /*optmize_use_logValues_fromLocalTable=*/true, 
			    /*log_type=*/e_kt_entropy_logType_log10,
			    /*log_type=*/e_kt_entropy_logType_log10,
			    type_adjustEach,
			    vector_length);
}

//! De-allcote the "s_kt_entropy_t" object (oekseth, 06. jun. 2017).
static void free__s_kt_entropy(s_kt_entropy_t *self) {
  assert(self);
  if(self->listOf_values) {free_1d_list_float(&(self->listOf_values)); self->listOf_values = NULL;}
  //  if(self->listOf_values) {free_1d_list_float(&(self->listOf_values)); self->listOf_values = NULL}
  if(self->listOf_values_forIndex) {free_1d_list_float(&(self->listOf_values_forIndex)); self->listOf_values_forIndex = NULL;}
  //if(self->listOf_values_forIndex_midPoint) {free_1d_list_float(&(self->listOf_values_forIndex_midPoint)); self->listOf_values_forIndex_midPoint = NULL;}  
}


/**
   @enum e_kt_entropy_frequencyType
   @brief a gneeric appraoch to specify the entopry-frequency-calcuylaiotn-type/appraoch.
 **/
typedef enum e_kt_entropy_frequencyType {
  e_kt_entropy_frequencyType_raw, 
  e_kt_entropy_frequencyType_empirical, 
  e_kt_entropy_frequencyType_Dirichlet, 
} e_kt_entropy_frequencyType_t;

//! Comptue frequcny for the Dirichlet.
#define __MF__freqs_Dirichlet__atIndex(arr, index, count_inv) ({macro_pluss(arr[i], macro_mul(val_adjust, count_inv));})
// FIXME[SSE]: add support for SSE in [”elow] ... ie, after we have dientifed/describded the emprical time-cost-case ... ie, wrt. threshodls for applciaton of SSE .... then update our entropy-HPOC-artilce ... using latter to describe/emphasice ....??.... ... eg, wrt. size-cases hwere SSE-complexities may boost applciaotn ... usign latter to argue for when such compelxties are wroth itnrodcuinsg ... 
#define __MF__freqs_Dirichlet_sum(arr, arr_size, sum, val_adjust) ({ \
      sum += (t_float)arr_size*(t_float)val_adjust; /*! ie, compute: "ya = y+a".*/ \
  const t_float empty_0 = 0;				       \
  const t_float count_inv = 1.0/(t_float)sum; /*! ie, an optmziaotn-strategy.*/	\
  t_float sum_local = 0; for(uint i = 0; i < (uint)arr_size; i++) {	\
    sum_local = macro_pluss(sum_local, __MF__freqs_Dirichlet__atIndex(arr, i, count_inv)); \
  } sum_local;})
#define __MF__freqs_Dirichlet(arr, arr_size, sum, val_adjust) ({ \
      sum += (t_float)arr_size*val_adjust; /*! ie, compute: "ya = y+a".*/ \
  const t_float empty_0 = 0;				       \
  t_float *vec_freq = allocate_1d_list_float(arr_size, empty_0); \
  const t_float count_inv = 1.0/(t_float)sum; /*! ie, an optmziaotn-strategy.*/	\
  for(uint i = 0; i < arr_size; i++) {					\
    vec_freq[i] = __MF__freqs_Dirichlet__atIndex(arr, i, count_inv);		\
  } vec_freq;})
#define __MF__freqs_relative(arr, arr_size, sum, val_adjust) ({ \
      sum += (t_float)arr_size*(t_float)val_adjust; /*! ie, compute: "ya = y+a".*/ \
  const t_float empty_0 = 0;				       \
  t_float *vec_freq = allocate_1d_list_float(arr_size, empty_0); \
  const t_float count_inv = 1.0/(t_float)sum; /*! ie, an optmziaotn-strategy.*/	\
  for(uint i = 0; i < arr_size; i++) {					\
    vec_freq[i] = (t_float)arr[i] * count_inv;				\
  } vec_freq;})

/**
   @remarks Computationa detials: 
     # Kullback-Leibler: 
     ..... in below we approximate the exueicont-tiem fro alrge feautre-vectors .... 
     Complexity(artimetics::each): O(sum=2a  + frequency=2d (or: frequency=2m through optmziaiton) + LR=l+d (or: 4a) + sum=m) ~ 3d+l (or: 8a);
 **/
static void kt_entropy__simFunc__kullBachLeibler_KL(const s_kt_entropy_t *self, const uint *arr1, const uint *arr2, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal, const e_kt_entropy_frequencyType_t typeOf_freqCompute) {
  //! Note: "KL.plugin = function(freqs1, freqs2, unit=c("log", "log2", "log10") )" ... and "KL.Dirichlet(..)"
  // FIXME[conceptual]: use 'this' relationship beteene entropy-divergence-copmtatuison adn teh 'æundlerying' simalrity-etrmcis such as KL and Chi to expalin/describe the relationship/overlap between simarlity-emtrics and emausres for entropy .... 
  // FIXME[conceptual]: ... relatre comptautiosn/optioans of entropy to netowrk-simalrities .... where the aritmetic compelxity of entropy-cotpmatusion .....??... 
  // FIXME[conceptual]: ... the reaosn for the estlibehd (and if-effeicnt aprpaoch) for entopry-comtpatuiosn may be the need/wish to simplify exeucitons (mvoiing the aritmetic compelxity from the user and voer to the software-compiler) .... eg, wrt. the splitting of an equation into sub-steps as seen in the Entropy library \cite{} .... The latter aprpaoch is reminsint/ismilar to oterh impemantion-aprpaoches which we have studied/elvuated, eg, wrt. .....??... 

  //!
  //! Apply loigcs:
#define __MiF__log(score) ({MF__s_kt_entropy__log_index_fast(self, score);})
#include "kt_entropy__simFunc__kullBachLeibler_KL.c"
}
//! Comptue KL where a non-topmzeid flaoting-potin comptaution is sued/applied.
static void kt_entropy__simFunc__kullBachLeibler_KL__float(const s_kt_entropy_t *self, const t_float *arr1, const t_float *arr2, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal, const e_kt_entropy_frequencyType_t typeOf_freqCompute) {
  //!
  //! Apply loigcs:
#define __MiF__log(score) ({mathLib_float_log(score);})
#include "kt_entropy__simFunc__kullBachLeibler_KL.c"
}

//! Compute: chi2 = sum( (freqs1-freqs2)^2/freqs2 )
#define __MF__sim__chi2() ({ \
  for(uint i = 0; i < arr_size; i++) {				\
  const t_float score_1 = __MiF__getScore_1(i);			\
  const t_float score_2 = __MiF__getScore_1(i);			\
  const t_float num   = (t_float)(arr1[i] - arr2[i]);		\
  if( (arr2[i] != 0) && (num != 0)) { \
    const t_float deNum = (t_float)arr2[i];			\
    sum_H += num*num/deNum; /*! ie, normlized Euclid.*/		\
  }}})

/**
   @brief Comptue Chi-squared entropy.
   @remarks Computationa detials: 
     # Chi-squared: 
     ..... in below we approximate the exueicont-tiem fro alrge feautre-vectors .... 
     Complexity(artimetics::each): O(sum=2a  + numerator=a+m + sum=d) = d;
 **/
static void kt_entropy__simFunc__ChiSquared_Chi2(const s_kt_entropy_t *self, const uint *arr1, const uint *arr2, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal, const e_kt_entropy_frequencyType_t typeOf_freqCompute) {
#include "kt_entropy__simFunc__ChiSquared_Chi2.c"
}
//! Comptue Chi-squared entropy.
static void kt_entropy__simFunc__ChiSquared_Chi2__float(const s_kt_entropy_t *self, const t_float *arr1, const t_float *arr2, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal, const e_kt_entropy_frequencyType_t typeOf_freqCompute) {
#include "kt_entropy__simFunc__ChiSquared_Chi2.c"
}

/* static void kt_entropy__matrix__MutualInformation_MI(const s_kt_entropy_t *self, const uint **matrix, const uint nrows, const uint ncols, const bool isTo_applyPostScaling, t_float *scalar_retVal) { */
/*   //! Compute: freqs2d = as.matrix(freqs2d/sum(freqs2d)) # just to make sure ... */

/*   //! Compute: a count-vector for: rows, ie, "freqs.x = rowSums(freqs2d) # marginal frequencies"; */
/*   //! Compute: a count-vector for: cols, ie, "freqs.y = colSums(freqs2d)"; */
/*   assert(false); // FXIME[conceptual]: figure out hwo to comptue teh null-model .... cosnider isntead usign the MINE-equaiton wr.t the mtual-infroatmion-aprpaoch/strategy ....  */
/*   //! Compute:  freqs.null = freqs.x %o% freqs.y # independence null model */
/*   //! Compute:   MI = KL.plugin(freqs2d, freqs.null, unit=unit) */
/*   //! Compute:   return(MI) */
  
/* } */

/* static void kt_entropy__matrix__ChiSquared_Chi2(const s_kt_entropy_t *self, const uint **matrix, const uint nrows, const uint ncols, const bool isTo_applyPostScaling, t_float *scalar_retVal) { */
/*   //! Compute:freqs2d = as.matrix(freqs2d/sum(freqs2d)) # just to make sure ... */
/*   //! Compute: freqs.x = rowSums(freqs2d) # marginal frequencies */
/*   //! Compute: freqs.y = colSums(freqs2d) */
/*   assert(false); // FXIME[conceptual]: figure out hwo to comptue teh null-model .... cosnider isntead usign the MINE-equaiton wr.t the mtual-infroatmion-aprpaoch/strategy ....  */
/*   //! Compute:  freqs.null = freqs.x %o% freqs.y # independence null model */
/*   //! Compute: chi2 = chi2.plugin(freqs2d, freqs.null, unit=unit) */
/* } */



/**
   @brief compute Shannon's entropy estimator
   @param <> 
   @param <arr> is the vector of counts, a coutn-vecotr which may incldue 'zeros'.
   @param <> 
 **/
static void kt_entropy__vector_entropy(const s_kt_entropy_t *self, const uint *arr, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal) {
  //! Note: correxpnds to "entropy.plugin(..)" in the enoptry-pacakge ... and/or "entropy.empirical(...)"
  /*
   freqs = freqs/sum(freqs) # just to make sure ...
   H = -sum( ifelse(freqs > 0, freqs*log(freqs), 0) )
*/
  uint sum = (uint)__MF__s_kt_entropy__sum(arr, arr_size);
  t_float sum_H = 0;
  if(sum != 0) {
    //! Apply the emprical entropy-meausre:
    // FIXME: validte correnctess of [below] .... 
#define  __MiF__getScore() ({(t_float)arr[i]*(t_float)score_sum_inv;})
#define  __MiF__getScore_log() ({MF__s_kt_entropy__log_index_fast(self, arr[i]) - log_score_sum;})
    __MF__s_kt_entropy__metric_empirical();
#undef __MiF__getScore
#undef __MiF__getScore_log
  }
  *scalar_retVal = sum_H;
}


/**
   @brief compute Shannon's entropy estimator
   @param <self> is the object ohlding the configiroants wrt. entorpy-comptautions.
   @param <arr> is the vector of counts, a coutn-vecotr which may incldue 'zeros'.
   @param <arr_size> is the nubmer of elements in the "arr" list.
   @param <isTo_applyPostScaling> which if set to true impleis that we adjust the entropy-score in a psot-step defined by/ion the "self" object.
   @param <scalar_retVal> hold the result of the entropy-cotpmatusion. 
 **/
static void kt_entropy__vector_entropy__slowFloats(const s_kt_entropy_t *self, const t_float *arr, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal) {
  *scalar_retVal = T_FLOAT_MAX;
  //! Note: correxpnds to "entropy.plugin(..)" in the enoptry-pacakge.
  t_float sum = 0;
  for(uint i = 0; i < arr_size; i++) {
    assert(isOf_interest(arr[i]));
    //    printf("[%u]=%f, at %s:%d\n", i, arr[i], __FILE__, __LINE__);
    sum += mathLib_float_abs(arr[i]);
  }
  //  printf("\t sum(before)=%f, given list-size=%u, at %s:%d\n", sum, arr_size, __FILE__, __LINE__);
  if(sum != 0) {
    t_float sum_H = 0;
    //! Apply the emprical entropy-meausre:
    // FIXME: validte correnctess of [below] .... 
#define  __MiF__getScore() ({arr[i]*score_sum_inv;})
#define  __MiF__getScore_log() ({mathLib_float_log_abs(arr[i]) - log_score_sum;})
    __MF__s_kt_entropy__metric_empirical();
#undef __MiF__getScore
#undef __MiF__getScore_log
    *scalar_retVal = sum_H;
    //    printf("\t entropy(Result)=%f, at %s:%d\n", *scalar_retVal, __FILE__, __LINE__);
  }
}

// #define __MF__s_kt_entropy__
//#define __MF__s_kt_entropy__freqs_Dirichlet() ({



/**
   @brief compute entropy::Dirichlet
   @param <> 
   @param <arr> is the vector of counts, a coutn-vecotr which may incldue 'zeros'.
   @param <> 
 **/
static void kt_entropy__vector_Dirichlet(const s_kt_entropy_t *self, const uint *arr, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal) {
  //! Note: correxpnds to "entropy.Dirichlet(..)" in the enoptry-pacakge.
  t_float sum = (t_float)__MF__s_kt_entropy__sum(arr, arr_size);
  t_float sum_H = 0;

  //!
  //!
  //!
  //!
  if(sum > 0) {
    if(self->config__useNaiveLogComputationAppraoch == false) {
      assert(false); // FIXME: add perf-evlauation ... dientify preference of such an appraoch .... 
      /**
	 // FIXME: udpat eour aritlce wr.t [below] .... 
	 a=1; freq_i = (count_i+a)*count_inv ... ... $sum = \sum freq_i$ ... H = \sum freq_i*log(freq_i)/sum = \sum freq_i*log(freq_i)*(1/sum) = \sum ((count_i+a)*(1/count)*(1/sum) ) * (log(count_i+a) - log(1/count) ) = \sum(count_i+a)*sum(inv)*(table[count] - log(sum(inv))) ... 
      **/
      const t_float val_adjust = self->val_adjust;
      const t_float vec_freq_sum = (t_float)__MF__freqs_Dirichlet_sum(arr, arr_size, sum, (self->val_adjust));
      if(vec_freq_sum > 0) {
	// FIXME: vlaidte correctness of [below] ... 
	const t_float vec_freq_sum_inv = 1.0/vec_freq_sum;
	const t_float vec_freq_sum_log = mathLib_float_log_abs(vec_freq_sum);
	const t_float sum_inv = 1.0/(t_float)sum;
	const t_float sum_inv_tot = sum_inv * vec_freq_sum_inv;
	//! Apply the emprical entropy-meausre:
	// FIXME: validte correnctess of [below] .... 
	const t_float val_adjust = self->val_adjust;
#define  __MiF__getScore() ({((t_float)arr[i]+val_adjust)*sum_inv_tot;})
#define  __MiF__getScore_log() ({MF__s_kt_entropy__log_index_fast(self, arr[i]) - vec_freq_sum_log;})
	__MF__s_kt_entropy__metric_empirical();
#undef __MiF__getScore
#undef __MiF__getScore_log
      }
      // FIXME[concpetual]: try to cosntuct a an equaiton which may be used to m'compile-in' an effectie entropy-emtric ... and then use latter aprpaoch to aruge for the gnericity of oru optmzied Entopry-based copmtatuion-aprpaoch ... 
      // FIXME[concpetual]: ... 
      // FIXME[concpetual]: ...
      //! FIXME[compute]: ... score = arr[i] + self->val_adjust; 
      //! FIXME[compute]:
      /*
	ya = y+a          # counts plus pseudocounts
	na = sum(ya)      # total number of counts plus pseudocounts
	pa = ya/na        # empirical frequencies adjusted with pseudocounts
      */
/* #define  __MiF__getScore() ({arr[i]*score_sum_inv;}) */
/* #define  __MiF__getScore_log() ({MF__s_kt_entropy__log_index_fast(self, arr[i]) - log_score_sum;}) */
/*   __MF__s_kt_entropy__metric_empirical(); */
/* #undef __MiF__getScore */
/* #undef __MiF__getScore_log */
    } else {
      const t_float val_adjust = self->val_adjust;
      t_float *vec_freq = __MF__freqs_Dirichlet(arr, arr_size, sum, (self->val_adjust));
      //! Apply the emprical entropy-meausre:
      kt_entropy__vector_entropy__slowFloats(self, vec_freq, arr_size, isTo_applyPostScaling, scalar_retVal);
      //!
      //! De-allocate:
      free_1d_list_float(&vec_freq); vec_freq = NULL;
    }
  }
}



/* static t_float __lambda_shrink() { */

/*   //! Compute: score^2, ie, : varu = u*(1-u)/(n-1) */
/*   //! Compute: , ie, : msp = sum( (u-target)^2 ) */
/*   //! Compute: , ie, : return lambda = sum( varu ) / msp */
/*   //! Compute: , ie, :  */
/*   //! Compute: , ie, :  */
/* } */

static t_float *__freqs_shrink(const s_kt_entropy_t *self, const uint *arr, const uint arr_size, const t_float lambda_input) {
  //! freqs.shrink = function (y, lambda.freqs, verbose = TRUE) 
  const t_float empty_0 = 0;
  t_float *vec_freq = allocate_1d_list_float(arr_size, empty_0);
  //! ------------------------------------------------------
  //! 
  //! Compute: , ie, : lambda.freqs = get.lambda.shrink(n, u, target, verbose) 
  t_float sum = 0;
  //! Compute: n=sum(..)
  for(uint i = 0; i < arr_size; i++) {
    sum += (t_float)arr[i];
  }
  //! 
  //! Compute: , ie, : msp = sum( (u-target)^2 )
  t_float msp = 0;
  //! Compute: , ie, : size_inv = target = 1/length(y) # uniform target (note length() works also for matrices)
  const t_float size_inv = 1/(t_float)arr_size;
  //const t_float sum_inv = 1/(t_float)sum;
  for(uint i = 0; i < arr_size; i++) {
    //! Compute: , ie, : n = sum(y)
    //! Compute: , ie, : vec_freq = u = y/n 
    vec_freq[i] = macro_mul(arr[i], size_inv);
    const t_float diff = macro_minus(vec_freq[i], size_inv);
    msp = macro_pluss(msp, macro_mul(diff, diff));
  }  
  t_float lambda = 0; 
  if( (lambda_input != 0) && (lambda_input != T_FLOAT_MAX) 
      // && (msp != 0) 
      ) {
    //! 
    if(msp != 0) {
      const t_float sum_nMinus1 = 1/(t_float)(sum-1);
      const t_float msp_inv = 1/msp;
      const t_float score_inv = msp_inv*sum_nMinus1;
      for(uint i = 0; i < arr_size; i++) {
	//! Compute: score^2, ie, : varu = u*(1-u)/(n-1)
	//! Compute: , ie, : lambda = sum( varu ) / msp
	lambda = macro_pluss(lambda, macro_mul(macro_mul(vec_freq[i] , macro_minus(1,vec_freq[i])), score_inv)); //! ie, lambda += freq[i]*(1-freq[i])/score
      }
    }
  } else {lambda = lambda_input;}
  if(lambda != 0) {
    if( (lambda > 0) && (lambda < 1)) {
      //! ------------------------------------------------------
      //! 
      //! Compute:
      const t_float lamb_1 =  lambda*size_inv;
      const t_float lamb_minus =  (1 - lambda);
      for(uint i = 0; i < arr_size; i++) {
	//! Compute: , ie, : return u.shrink = lambda.freqs * target + (1 - lambda.freqs) * u
	vec_freq[i] = macro_pluss(lamb_1 , macro_mul(lamb_minus , vec_freq[i]));
      }
    } //! else we iether have over-shrnkage or under-shrinkage.
  }
  //! Compute: , ie, : 
  return vec_freq;
}
/**
   @brief compute entropy::Dirichlet
   @param <> 
   @param <arr> is the vector of counts, a coutn-vecotr which may incldue 'zeros'.
   @param <> 
 **/
static void kt_entropy__vector_Shrink(const s_kt_entropy_t *self, const uint *arr, const uint arr_size, const bool isTo_applyPostScaling, const t_float lambda_default, t_float *scalar_retVal) {
  //! Compute: , ie, : f = freqs.shrink(y, lambda.freqs=lambda.freqs, verbose=verbose)
  //! Compute: , ie, : h = entropy.plugin(f, unit=unit)
  t_float *vec_freq = __freqs_shrink(self, arr, arr_size, lambda_default);
  kt_entropy__vector_entropy(self, arr, arr_size, isTo_applyPostScaling, scalar_retVal);
  //!
  //! De-allocate:
  free_1d_list_float(&vec_freq); vec_freq = NULL;
}

static void kt_entropy__vector_Shrink__pair__chi2(const s_kt_entropy_t *self, const uint *arr1, const uint *arr2, const uint arr_size, const bool isTo_applyPostScaling, const t_float lambda_default, t_float *scalar_retVal) {
  //! Compute: , ie, : f1 = freqs.shrink(y1, lambda.freqs=lambda.freqs1, verbose=verbose)
  //! Compute: , ie, : f2 = freqs.shrink(y2, lambda.freqs=lambda.freqs2, verbose=verbose)
  // FIXME: write a wrapper-funciton for [”elow] ... to aovdi muliple/duplciate entopry-calls ... and integrate with our simalrity-emtric-comtpatuion ... 
  t_float *vec_freq1 = __freqs_shrink(self, arr1, arr_size, lambda_default);
  t_float *vec_freq2 = __freqs_shrink(self, arr2, arr_size, lambda_default);
  //! Compute: , ie, : chi2 = chi2.plugin(f1, f2, unit=unit)
  kt_entropy__simFunc__ChiSquared_Chi2__float(self, vec_freq1, vec_freq2, arr_size, isTo_applyPostScaling, scalar_retVal, e_kt_entropy_frequencyType_raw);
  //!
  //! De-allocate:
  free_1d_list_float(&vec_freq1); vec_freq1 = NULL;
  free_1d_list_float(&vec_freq2); vec_freq2 = NULL;
  //! Compute: , ie, : 
}
static void kt_entropy__vector_Shrink__pair__KL(const s_kt_entropy_t *self, const uint *arr1, const uint *arr2, const uint arr_size, const bool isTo_applyPostScaling, const t_float lambda_default, t_float *scalar_retVal) {
  //! Compute: , ie, : f1 = freqs.shrink(y1, lambda.freqs=lambda.freqs1, verbose=verbose)
  //! Compute: , ie, : f2 = freqs.shrink(y2, lambda.freqs=lambda.freqs2, verbose=verbose)
  // FIXME: write a wrapper-funciton for [”elow] ... to aovdi muliple/duplciate entopry-calls ... and integrate with our simalrity-emtric-comtpatuion ... 
  t_float *vec_freq1 = __freqs_shrink(self, arr1, arr_size, lambda_default);
  t_float *vec_freq2 = __freqs_shrink(self, arr2, arr_size, lambda_default);
  //! Compute: , ie, : KL = KL.plugin(f1, f2, unit=unit)
  kt_entropy__simFunc__kullBachLeibler_KL__float(self, vec_freq1, vec_freq2, arr_size, isTo_applyPostScaling, scalar_retVal, e_kt_entropy_frequencyType_raw);
  //!
  //! De-allocate:
  free_1d_list_float(&vec_freq1); vec_freq1 = NULL;
  free_1d_list_float(&vec_freq2); vec_freq2 = NULL;
  //! Compute: , ie, : 
}

static void kt_entropy__freqMatrix_Shrink__pair__KL(const s_kt_entropy_t *self, const s_kt_matrix_base_t *mat1_freq, const s_kt_matrix_base_t *mat2_freq, const bool isTo_applyPostScaling, const t_float lambda_default, s_kt_matrix_base_t *mat_result) {
//static void kt_entropy__matrix_Shrink__pair__KL(const s_kt_entropy_t *self, const s_kt_matrix_base_t *mat1, const s_kt_matrix_base_t *mat2, const bool isTo_applyPostScaling, const t_float lambda_default, s_kt_matrix_base_t *mat_result) {
  if(mat2_freq->ncols != mat1_freq->nrows) {
    fprintf(stderr, "!!(Critical::input-error)\t Excpected both inptu-amtrixes to have the same column-size, which is Not the case, ie, given mat1.ncols=%u VS mat2.ncols=%u, at %s:%d\n", mat1_freq->ncols, mat2_freq->ncols, __FILE__, __LINE__);
    return;
  }
  assert(mat_result); assert(mat_result->matrix == NULL); //! ie, as we assuemt hey latter has Not been allcoted.
  *mat_result = initAndReturn__s_kt_matrix_base_t(mat1_freq->nrows, mat2_freq->nrows);
  // FIXME: fork this funcitoni out .... genralise ... thenw rtie a wrapper-fucntion in our "hp_entropy.h" ... 
  
  //! 
  //! Compute frequencies:
  // FIXME[code]: generliae this funciton ... to get a matix of freuqncy-counts ... then add supprot foru our different simalirty-emtics ... 
  for(uint row_id_1 = 0; row_id_1 < mat1_freq->nrows; row_id_1++) {
    for(uint row_id_2 = 0; row_id_2 < mat2_freq->nrows; row_id_2++) {
      const t_float *arr1 = mat1_freq->matrix[row_id_1];
      const t_float *arr2 = mat2_freq->matrix[row_id_2];
      const uint arr_size = mat2_freq->ncols;
      t_float retVal_local = 0;
      t_float *scalar_retVal = &retVal_local;      
      //!
      //! Apply loigcs:
#define __MiF__log(score) ({mathLib_float_log(score);})
      const e_kt_entropy_frequencyType_t typeOf_freqCompute = e_kt_entropy_frequencyType_raw;
#include "kt_entropy__simFunc__kullBachLeibler_KL.c"
      //! Score results:
      mat_result->matrix[row_id_1][row_id_2] = *scalar_retVal;
    }
  }
}

/* /\** */
/*    @brief compute Miller-Madow (1955) entropy estimator */
/* /\*    @param <>  *\/ */
/*    @param <arr> is the vector of counts, a coutn-vecotr which may incldue 'zeros'. */
/*    @param <>  */
/*  **\/ */
/* static void kt_entropy__MillerMadow(const s_kt_entropy_t *self, const uint *arr, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal) { */

/* } */
// FIXME[SSE]: ... add ... after evlauation of time-cost for differnet nan-handlingers rt. the 'div' operator ... 
#define __MiF__entropy__case_num2_div1(arr_size) ({ \
  for(uint i = 0; i < arr_size; i++) { \
  if(arr[i] > 0) { \
  const t_float score_num = __MiF__getScore_1(); \
  const t_float score_num_log = __MiF__getScore_2();		\
  const t_float score_div = __MiF__getScore_div(); \
  assert(score_num != 0); \
  const t_float score_mul = macro_mul(score_num , score_num_log); \
  sum_H -= ((score_div != 0) && (score_mul != 0) ) ? macro_div(score_mul, score_div) : 0; \
  }}})

  /*! Compute: list_xmtZero = (list[i] > 0).push = yx;  sum = sum(list_xmtZero) = n; */
    /*! Compute: sum_count1 = (list_xmtZero[i] == 1) = (list[i] == 1) = f1;*/
#define MF__entropy__ChaoShen(arr, arr_size) ({ \
      t_float sum_H = 0; uint sum_count1 = 0; uint sum = (uint)__MF__s_kt_entropy__sum__andSingleTons__uint(arr, arr_size); \
  if(sum != 0) { \
    if(sum_count1 == sum) {sum_count1 = sum - 1;} \
    const t_float coverage = 1 - ((t_float)sum_count1 / (t_float)sum); /* ie, compute "C".*/ \
    /*! Compute: list_freq = list_freq[i]/sum; */ \
    const t_float logScore_sum = mathLib_float_log(sum); \
    /*! Compute: H = -sum(list_coverageFreq[i]*log(list_coverageFreq[i]) / list_sampleBin[i]); */ \
    __MiF__entropy__case_num2_div1(arr_size); \
    /*printf("sum_H=%f given: compute-entropy for arr_size=%u, sum=%u, logScore_sum=%f, at %s:%d\n", sum_H, arr_size, sum, logScore_sum, __FILE__, __LINE__); */  \
  } \
  /*! Post-scaling:*/ \
  __MF__s_kt_entropy__postAdjustment(); sum_H;})

/* static void kt_entropy__ChaoShen(const s_kt_entropy_t *self, const uint *arr, const uint arr_size, const bool isTo_applyPostScaling, t_float *scalar_retVal) { */
/*   // FIXME: competle impemtantiosn of "/home/klatremus/Dokumenter/master_project/hg-bioprosjekt/poset_structure_0.0.0.1/src/externalLibs/entropy_package/entropy/R/entropy.ChaoShen.R" (and siamrli algortihms/software-appraoches) ...  */
/*   //  assert(false); // FIXME: write an equation capturing ChaoShen meausre ...  */
/* } */


#define __MiF__getConfigValue__Dirichlet__adjustValue(arr, arr_size) ({ \
  t_float val_adjust = 0.5; \
  if(__isCase(e_kt_entropy_genericType_Dirichlet_Jeffreys)) {val_adjust = 0.5; \
  } else if(__isCase(e_kt_entropy_genericType_Dirichlet_Laplace)) {val_adjust = 1.0; \
  } else if(__isCase(e_kt_entropy_genericType_Dirichlet_SG)) {val_adjust = 1/(t_float)arr_size; \
  } else if(__isCase(e_kt_entropy_genericType_Dirichlet_minimax)) { \
  t_float sum = 0; for(uint i = 0; i < arr_size; i++) {sum += arr[i];} \
  val_adjust = (sum != 0) ? sum/(t_float)arr_size : 0; \
  } else {assert(false);} val_adjust;})


static void kt_entropy__generic__vector(s_kt_entropy_t *self, const s_kt_list_1d_uint_t *obj_list, e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, const t_float lambda_default, t_float *retVal) {
    //!
    //!
#define __defParams__ self, obj_list->list, obj_list->list_size, isTo_applyPostScaling, retVal
#define __MiF__assertA(a_score) ({if(self->val_adjust != self->val_adjust) {fprintf(stderr, "!!\t A-score='%f' was expected to have score='%f', ie, which is Not the case: please udpate your itn-setting, at [%s]:%s:%d\n", a_score, self->val_adjust, __FUNCTION__, __FILE__, __LINE__);}})
    //!
    //!
    if(__isCase(e_kt_entropy_genericType_ML_Shannon)) { 
      //! 
      const uint *arr = obj_list->list; const uint arr_size = obj_list->list_size;
      //! 
      // kt_entropy__vector_entropy(__defParams__);
  //! Note: correxpnds to "entropy.plugin(..)" in the enoptry-pacakge ... and/or "entropy.empirical(...)"
  /*
   freqs = freqs/sum(freqs) # just to make sure ...
   H = -sum( ifelse(freqs > 0, freqs*log(freqs), 0) )
  */
      uint sum = (uint)__MF__s_kt_entropy__sum(arr, arr_size);
      t_float sum_H = 0;
      //! Apply the emprical entropy-meausre:
      // FIXME: validte correnctess of [below] .... 
#define  __MiF__getScore() ({(t_float)arr[i]*score_sum_inv;})
#define  __MiF__getScore_log() ({MF__s_kt_entropy__log_index_fast(self, arr[i]) - log_score_sum;})
      if(sum != 0) {
	__MF__s_kt_entropy__metric_empirical();
      }
#undef __MiF__getScore
#undef __MiF__getScore_log
      // printf("#! Result[shannon]: score=%f given sum=%u, arr_size=%u, at %s:%d\n", sum_H, sum, arr_size, __FILE__, __LINE__);      
      *retVal = sum_H;
    } else if(__isCase(e_kt_entropy_genericType_MM_MillerMadow)) {
      //! @brief compute Miller-Madow (1955) entropy estimator
      //! 
      const uint *arr = obj_list->list; const uint arr_size = obj_list->list_size;
      //! 
      // FIXME: competle impemtantiosn of "/home/klatremus/Dokumenter/master_project/hg-bioprosjekt/poset_structure_0.0.0.1/src/externalLibs/entropy_package/entropy/R/entropy.MillerMadow.R" (and siamrli algortihms/software-appraoches) ... 
      //uint sum_count1 = 0; 
      uint sum_aboveZero = 0; uint sum = (uint)__MF__s_kt_entropy__sum__countAbove0__uint(arr, arr_size); 
      if(sum != 0) {
	//! Note: [below] correspodns to:   H = entropy.empirical(y, unit="log"): H = -sum( ifelse(freqs > 0, freqs*log(freqs), 0) )
	t_float sum_H = ((t_float)sum_aboveZero - 1)/(2*(t_float)sum);
	//! Apply the emprical entropy-meausre:
#define  __MiF__getScore() ({arr[i];})
#define  __MiF__getScore_log() ({MF__s_kt_entropy__log_index_fast(self, arr[i]);})
	if(false) { // FIXME: re-include below when we have resolved seg-fault-issue:
	  __MF__s_kt_entropy__metric_empirical();
	} else {
	  t_float log_score_sum = mathLib_float_log(sum);	  
	  t_float score_sum_inv = 1.0/(t_float)sum; /*! ie, to use mutlipclaiton instead of idvion, thereby optmziehng the comtpautions. */ 
	  assert(sum != 0); 
	  assert(isinf(score_sum_inv) == false); 
	  for(uint i = 0; i < arr_size; i++) {				
	    if((int)__MiF__getScore() >= self->listOf_values_forIndex_size) {
	      fprintf(stderr, "!!\t(kt-entropy)\t score:(%f=%d) >= limit(%d), max(float)=%f, max(uint)=%u\n", (t_float)__MiF__getScore(), (int)__MiF__getScore(), self->listOf_values_forIndex_size, T_FLOAT_MAX, UINT_MAX);
	    }
	    assert((int)__MiF__getScore() < self->listOf_values_forIndex_size);
	    const t_float score_raw = (t_float)__MiF__getScore();		
	    assert(isOf_interest(score_raw)); 
	    /*const t_float log_score_1 = MF__s_kt_entropy__log_index_fast(self, score_raw); */ 
	    const t_float log_score_1 = __MiF__getScore_log();		
	    const t_float log_score = log_score_1-log_score_sum; /*! ie, log(value/sum) */ 
	    /*printf("tt sum_H = %f - (%f * %f * %f), at %s:%dn", sum_H, score_sum_inv, score_raw, log_score, __FILE__, __LINE__); */ 
	    sum_H -= score_sum_inv*(t_float)score_raw*log_score;			
	  } 
	  /*! Post-scaling: b*/ 
	  if(false) {printf("sum_H=%f, score_sum_inv=%f, log_score_sum=%f, arr_size=%u, isTo_applyPostScaling=%d, at %s:%dn", sum_H, score_sum_inv, log_score_sum, arr_size, isTo_applyPostScaling, __FILE__, __LINE__);} 
	  __MF__s_kt_entropy__postAdjustment();
	}
#undef __MiF__getScore
#undef __MiF__getScore_log
	//!
	//!
	//! @return
	*retVal = sum_H;
      }
      //*scalar_retVal = sum_H;
      //! 
      //kt_entropy__MillerMadow(__defParams__);
    } else if(__isCase(e_kt_entropy_genericType_CS_ChaoShen)) {
      /*
      @brief compute Chao-Shen (2003) entropy estimator
   @param <> 
   @param <arr> is the vector of counts, a coutn-vecotr which may incldue 'zeros'.
   @param <> 
   @remarks Computationa detials: 
     # Note: in [”elow] we use meuarements from \rul{https://software.intel.com/sites/landingpage/IntrinsicsGuide/#expand=10,13,109&text=_ps&techs=SSE,SSE2,SSE3,SSSE3} ... 
     # In below we inlcude desciptions for the "Ivy Bridge" computer-hardware-achcitetures: 
     add (a): latency=3;
     substract (s): latency=3; 
     multiplication (m): latency=5; 
     division (d): latency= 11-14; 
     exponent (e): latency= ....??...
     logaritm (l): latency= ....??...

     # Chao-Chen (2003) entorpy-estimator:
     ..... in below we approximate the exueicont-tiem fro alrge feautre-vectors .... 
     Complexity(artimetics::each): O(sum=s  + coverage=m + numerator=m+l + deNumerator(prestep)=a+e + deNumerator(during)=d) ~ d+e+l ~ l; 
 **/
      //! kt_entropy__ChaoShen(__defParams__);
      const uint *arr = obj_list->list; const uint arr_size = obj_list->list_size;
      //!
      //!
#define  __MiF__getScore_1() ({assert(arr[i] != 0); assert(sum != 0); t_float l_sum = (t_float)arr[i]/(t_float)sum; l_sum;}) 
#define  __MiF__getScore_2() ({MF__s_kt_entropy__log_index_fast(self, arr[i]) - logScore_sum;})
      //! Compute:   list_coverageFreq = list_freq[i] * coverage = pa;
      //! Compute:  list_sampleBin[i] = (1 - (1 - list_coverageFreq[i])^sum) = la;
      // FIXME: is it correct ot use the 'sum' oeprator in [”elow] ... ie, instead of '2' .... ie, as the nubmer may grwo large ... 
#define  __MiF__getScore_div() ({t_float l_score_div = score_num * coverage; t_float sum_beforePow = (1 - (1 - l_score_div)); t_float l_div = mathLib_float_pow(sum_beforePow, sum); l_div;})
      // FIXME[tut+perf]: perofmr an evlauation wrt. the time-comepxltiy of [”elow] ... 
      t_float sum_H = MF__entropy__ChaoShen(arr, arr_size);
      //  assert(false); // FIXME: vlaidte [”elow] ... ie, as both scores will hav ethe same sum .... where abmitity ocsnerns teh use of the "sum(..)" funciton-call in the R-entropy-blirary
      //!
      //! @return
      *retVal = sum_H;
      //! -------- 
#undef __MiF__getScore_1
#undef __MiF__getScore_2
#undef __MiF__getScore_div
    } else if(__isCase(e_kt_entropy_genericType_Dirichlet_Jeffreys)) {__MiF__assertA(0.5); kt_entropy__vector_Dirichlet(__defParams__);
    } else if(__isCase(e_kt_entropy_genericType_Dirichlet_Laplace)) {__MiF__assertA(1.0); kt_entropy__vector_Dirichlet(__defParams__);
    } else if(__isCase(e_kt_entropy_genericType_Dirichlet_SG)) {
      const t_float a_score = 1/(t_float)obj_list->list_size; 
      __MiF__assertA(a_score); 
      kt_entropy__vector_Dirichlet(__defParams__);
    } else if(__isCase(e_kt_entropy_genericType_Dirichlet_minimax)) {
      t_float sum = 0; for(uint i = 0; i < obj_list->list_size; i++) {
	sum += (t_float)obj_list->list[i];
      }
      const t_float a_score = (sum != 0) ? sum/(t_float)obj_list->list_size : 0; 
      __MiF__assertA(a_score); 
      kt_entropy__vector_Dirichlet(__defParams__);
    } else if(__isCase(e_kt_entropy_genericType_shrink)) {kt_entropy__vector_Shrink(self, obj_list->list, obj_list->list_size, isTo_applyPostScaling, lambda_default, retVal);
    } else { //! then we uwe non-topmzeid strategies:
      // ---------------------------
	/* const uint *arr = obj_list->list;       const uint arr_size = obj_list->list_size; */
	/* const t_float deNum_inv = 1/((t_float)(arr_size * (arr_size-1))); */
	const uint *arr = obj_list->list; const uint arr_size = obj_list->list_size;
	//! Note: correxpnds to "entropy.Dirichlet(..)" in the enoptry-pacakge.
	uint sum = (uint)__MF__s_kt_entropy__sum(arr, arr_size);
	t_float sum_H = 0;
	t_float H = 0;
	const t_float val_adjust = self->val_adjust;
	t_float *vec_freq = __MF__freqs_relative(arr, arr_size, sum, (self->val_adjust));
	// ---------------------------
	//! 
	//! 
	if(__isCase(e_kt_entropy_genericType_Simpson)) {
	  //const uint *arr = obj_list->list;       const uint arr_size = obj_list->list_size;
	  const t_float deNum_inv = 1/((t_float)(arr_size * (arr_size-1)));
	  //t_float H = 0;
	  for(uint i = 0; i < arr_size; i++) {
	    //	if(arr[i] > 0) 
	    {
	      const t_float theta = (t_float)vec_freq[i]; // macro_mul((t_float)arr[i], (t_float)arr[i]-1), deNum_inv);
	      //const t_float theta = (t_float)arr[i]; // macro_mul((t_float)arr[i], (t_float)arr[i]-1), deNum_inv);
	      //const t_float theta = macro_mul(macro_mul((t_float)arr[i], (t_float)arr[i]-1), deNum_inv);
	      H = macro_pluss(H, theta);
	    }
	  }
	  *retVal = H;
	} else if(__isCase(e_kt_entropy_genericType_Simpson_generic)) {
	  const t_float deNum_inv = 1/((t_float)(arr_size * (arr_size-1)));
	/* const uint *arr = obj_list->list;       const uint arr_size = obj_list->list_size; */
	/* const t_float deNum_inv = 1/((t_float)(arr_size * (arr_size-1))); */
	/* t_float H = 0; */
	for(uint i = 0; i < arr_size; i++) {
	  if(arr[i] > 0) {
	    const t_float value_power = self->value_power;
	    t_float theta = macro_mul(macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]-1), deNum_inv);
	    //	    t_float theta = macro_mul(macro_mul((t_float)arr[i], (t_float)arr[i]-1), deNum_inv);
	    theta = macro_mul(theta, mathLib_float_pow(theta, value_power));
	    H = macro_pluss(H, theta);
	    //H += theta;
	  }
	}
	*retVal = H;
      } else if(__isCase(e_kt_entropy_genericType_Gini)) {
	for(uint i = 0; i < arr_size; i++) {
	  //if(arr[i] > 0) 
	  const t_float theta = macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  // printf("%f => %f, at %s:%d\n", vec_freq[i], theta, __FILE__, __LINE__);
	  H = macro_pluss(H, theta);
	}
	*retVal = H;      
      } else if(__isCase(e_kt_entropy_genericType_Hulbert_permutation)) {
	for(uint i = 0; i < arr_size; i++) {
	  //if(arr[i] > 0) 
	  const t_float value_power = self->value_power;
	  t_float theta = 1- vec_freq[i];// macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  theta = mathLib_float_pow(theta, value_power);
	  //	  theta = macro_mul(theta, mathLib_float_pow(theta, value_power));
	  theta = 1 - theta;
	  // printf("%f => %f, at %s:%d\n", vec_freq[i], theta, __FILE__, __LINE__);
	  H = macro_pluss(H, theta);
	}
	*retVal = H;      
      } else if(__isCase(e_kt_entropy_genericType_HCDT)) {
	for(uint i = 0; i < arr_size; i++) {
	  //if(arr[i] > 0) 
	  const t_float value_power = self->value_power;
	  t_float theta = vec_freq[i];// macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  theta = mathLib_float_pow(theta, value_power);
	  theta = theta - 1; //! ie, 'ivnert'
	  //t_float theta = macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  // printf("%f => %f, at %s:%d\n", vec_freq[i], theta, __FILE__, __LINE__);
	  //theta = 1 - theta;
	  H = macro_pluss(H, theta);
	}
	//! Adjsut and update: 
	if(H != 0) {H = H / ((t_float)arr_size - 1.0); }
	*retVal = H;      
	} else if(__isCase(e_kt_entropy_genericType_Hill)) { //! 
	for(uint i = 0; i < arr_size; i++) {
	  //if(arr[i] > 0) 
	  const t_float value_power = self->value_power;
	  t_float theta = vec_freq[i];// macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  theta = mathLib_float_pow(theta, value_power);
	  //t_float theta = macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  // printf("%f => %f, at %s:%d\n", vec_freq[i], theta, __FILE__, __LINE__);
	  //theta = 1 - theta;
	  H = macro_pluss(H, theta);
	}
	//! Adjsut and update: 
	if(H != 0) {H = H / (arr_size - 1) ; }
	*retVal = H;      
	} else if(__isCase(e_kt_entropy_genericType_Renyi)) { //! which use 'log' after hill
	for(uint i = 0; i < arr_size; i++) {
	  //if(arr[i] > 0) 
	  const t_float value_power = self->value_power;
	  t_float theta = vec_freq[i];// macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  theta = mathLib_float_pow(theta, value_power);
	  //t_float theta = macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  // printf("%f => %f, at %s:%d\n", vec_freq[i], theta, __FILE__, __LINE__);
	  //theta = 1 - theta;
	  H = macro_pluss(H, theta);
	}
	//! Adjsut and update: 
	if(H != 0) {H = mathLib_float_log(H) / (arr_size - 1) ; }
	*retVal = H;      
      } else if(__isCase(e_kt_entropy_genericType_PT_1)) {
	for(uint i = 0; i < arr_size; i++) {
	  //if(arr[i] > 0) 
	  const t_float value_power = self->value_power + 1;
	  t_float theta = vec_freq[i];// macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  theta = mathLib_float_pow(theta, value_power);
	  //t_float theta = macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	  // printf("%f => %f, at %s:%d\n", vec_freq[i], theta, __FILE__, __LINE__);
	  //theta = 1 - theta;
	  H = macro_pluss(H, theta);
	}
	//! Adjsut and update: 
	if(H != 0) {
	  const t_float value_power = 1.0/(t_float)self->value_power;
	  H = mathLib_float_pow(H, value_power);
	  //  H = mathLib_float_log(H) / (arr_size - 1) ; 
	}
	*retVal = H;      
      } else if(__isCase(e_kt_entropy_genericType_PT_2)) {
	for(uint i = 0; i < arr_size; i++) {
	  if(arr[i] > 0) {
	    const t_float value_power = self->value_power + 1;
	    t_float theta = vec_freq[i];// macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	    theta = mathLib_float_pow(theta, value_power);
	    theta = theta / (t_float)arr[i];
	    //t_float theta = macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	    // printf("%f => %f, at %s:%d\n", vec_freq[i], theta, __FILE__, __LINE__);
	    //theta = 1 - theta;
	    H = macro_pluss(H, theta);
	  }
	}
	//! Adjsut and update: 
	if(H != 0) {
	  const t_float value_power = 1.0/(t_float)self->value_power;
	  H = mathLib_float_pow(H, value_power);
	  //  H = mathLib_float_log(H) / (arr_size - 1) ; 
	}
	*retVal = H;      
      } else if(__isCase(e_kt_entropy_genericType_AD)) {
	for(uint i = 0; i < arr_size; i++) {
	  //	  if(arr[i] > 0) 
	  {
	    const t_float value_power = self->value_power; // + 1;
	    t_float theta = vec_freq[i];// macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	    theta = mathLib_float_pow(theta, value_power);
	    theta = theta - 1; /// (t_float)arr[i];
	    //t_float theta = macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); //-1), deNum_inv);
	    // printf("%f => %f, at %s:%d\n", vec_freq[i], theta, __FILE__, __LINE__);
	    //theta = 1 - theta;
	    H = macro_pluss(H, theta);
	  }
	}
	//! Adjsut and update: 
	if(H != 0) {
	  const t_float value_power = 1.0 - (t_float)self->value_power;
	  t_float denum = mathLib_float_pow(2.0, value_power);
	  if(denum != 1) {
	    H = H / (denum - 1); //mathLib_float_pow(H, value_power);
	  }
	  //  H = mathLib_float_log(H) / (arr_size - 1) ; 
	}
	*retVal = H;      
      } else if(__isCase(e_kt_entropy_genericType_HHI)) {
	  for(uint i = 0; i < arr_size; i++) {
	    //	if(arr[i] > 0) 
	    {
	      const t_float theta = macro_mul((t_float)vec_freq[i], (t_float)vec_freq[i]); // macro_mul((t_float)arr[i], (t_float)arr[i]-1), deNum_inv);
	      //const t_float theta = (t_float)arr[i]; // macro_mul((t_float)arr[i], (t_float)arr[i]-1), deNum_inv);
	      //const t_float theta = macro_mul(macro_mul((t_float)arr[i], (t_float)arr[i]-1), deNum_inv);
	      H = macro_pluss(H, theta);
	    }
	  }
	  *retVal = H;	  
      }
      //!
      //! De-allocate:
      free_1d_list_float(&vec_freq); vec_freq = NULL;
      //} else if(__isCase()) {
      //} else if(__isCase()) {
      //} else if(__isCase()) {
    }
#undef __defParams__

    //  }

// FIXME: implment for ... the other not-yet-supproted euqiaonts in our related-work-seicont wrt. entopry-emausres and aprpxomaite-entropy-meausres .... 

}

/**
   @brief idneitfes the frequency for differen metrics (oekseth, 06. jun. 2017).
 **/
static void kt_entropy__generic__vector__frequency(s_kt_entropy_t *self, const s_kt_list_1d_uint_t *obj_list, e_kt_entropy_genericType_t enum_entropy, const bool isTo_applyPostScaling, const t_float lambda_default, s_kt_list_1d_float_t *ret_list) {
  //! then we fetch the frequency.
  assert(ret_list); assert(ret_list->list_size == 0);

    // FIXME[SSE]: write tut-perf-test to dineitfy threshold when SSE is to be used ... then update [”elow]

#define __MiF__printError() ({fprintf(stderr, "!!\t Funciton Not supported for enum=%u, at [%s]:%s:%d\n", enum_entropy, __FUNCTION__, __FILE__, __LINE__);})
    if(__isCase(e_kt_entropy_genericType_ML_Shannon)) { //kt_entropy__vector_entropy
      if(ret_list->list_size != obj_list->list_size) {
	free__s_kt_list_1d_float_t(ret_list);
	*ret_list = init__s_kt_list_1d_float_t(obj_list->list_size);
      }
      //! Corresponds-to: "freqs.empirical"      
      t_float sum = 0;
      for(uint i = 0; i < obj_list->list_size; i++) {
	sum += (t_float)obj_list->list[i];
      }      
      if(sum != 0) {
	const t_float sum_inv = 1.0/(t_float)sum;
	t_float H = 0;
	// FIXME[SSE]: .... [”elow] .... 
	// FIXME[tut+erpf]: evalaute the otpmziaotn-degree by 'moving' the "1/div" otuside the fort-loop ... udpatiign our cpation-figure-text .... 
	for(uint i = 0; i < obj_list->list_size; i++) {
	  ret_list->list[i] = obj_list->list[i]*sum_inv;
	  //H += obj_list->list[i]*sum_inv;
	}
	//*retVal = H;
      }
    } else if(__isCase(e_kt_entropy_genericType_MM_MillerMadow)) {__MiF__printError();
    } else if(__isCase(e_kt_entropy_genericType_CS_ChaoShen)) {__MiF__printError();
      //    } else if(enum_entropy <= e_kt_entropy_genericType_Dirichlet_Jeffreys) {
    } else if(enum_entropy <= e_kt_entropy_genericType_Dirichlet_minimax) {
      const t_float val_adjust = __MiF__getConfigValue__Dirichlet__adjustValue(obj_list->list, obj_list->list_size);
      t_float sum = (t_float)__MF__s_kt_entropy__sum(obj_list->list, obj_list->list_size);
      t_float *vec_freq = __MF__freqs_Dirichlet(obj_list->list, obj_list->list_size, sum, val_adjust);      
      //! Then include the [ªbove] into the object:
      if(vec_freq) {
      if(ret_list->list_size != obj_list->list_size) {
	free__s_kt_list_1d_float_t(ret_list);
	  ret_list->list = vec_freq; ret_list->list_size = obj_list->list_size;
	} else {
	  for(uint i = 0; i < obj_list->list_size; i++) {
	    ret_list->list[i] = vec_freq[i];
	  }
	  free_1d_list_float(&vec_freq); vec_freq = NULL;
	}
      }
      //! Corresponds-to: "freqs.Dirichlet"
      assert(false); // FIXME: .... support

      assert(false); // FIXME: write  ause ca-se where the frequcnies are used as inptu to ... KL .... ie, a use-case wehre lgoarithm-opmziaotn is .... 
      assert(false); // FIXME: write  ause ca-se where the frequcnies are used as inptu to ... simalrity-emtrics .... ie, to avoid the overhads in calls .... 
      assert(false); // FIXME[cocnepotaul]: .... argue for the genericity of our aprpaopch ... in cotnrast to estlibhed strategies .... how e provide a geneirc interface ... whiel entorpy-lbiraires siuch as \cite{} aruges that they manage to unify differnet reseharc-domaisn .... they have not provided/integated/fcliated the addresisng of time-costs assited to simalrity-emtics ..... --todo[tut+perf]: wrirte an R-script which evaluat ethe time-cost of comptuign Euclid and DL usign an opmtized-R impemtantion .... then use the latter tiem-difference to aruge for the optmzlaity of our aprpaoch .... ...  

    } else if(__isCase(e_kt_entropy_genericType_shrink)) {
      //! Corresponds-to: ""
      t_float *vec_freq = __freqs_shrink(self, obj_list->list, obj_list->list_size, lambda_default);
      //! Then include the [ªbove] into the object:
      if(vec_freq) {
      if(ret_list->list_size != obj_list->list_size) {
	free__s_kt_list_1d_float_t(ret_list);
	  ret_list->list = vec_freq; ret_list->list_size = obj_list->list_size;
	} else {
	  for(uint i = 0; i < obj_list->list_size; i++) {
	    ret_list->list[i] = vec_freq[i];
	  }
	  free_1d_list_float(&vec_freq); vec_freq = NULL;
	}
      }
      //ret_list->list = vec_freq1; ret_list->list_size = obj_list->list_size;
      /* // FIXME: validte correctness of [below] ... ie, to return a scalar (frather than a lsit) .... comapre with the R library ...  */
      /* t_float H = 0; */
      /* for(uint i = 0; i < obj_list->list_size; i++) { */
      /* 	H += vec_freq1[i]; */
      /* } */
      /* *retVal = H; */
      //!
      //! De-allocate:
      // free_1d_list_float(&vec_freq1); vec_freq1 = NULL;
      //    } else if(__isCase()) {
    }
}

static void kt_entropy__generic__matrix(s_kt_entropy_t *self, const s_kt_matrix_base_t *mat_input_1, const s_kt_matrix_base_t *mat_input_2, e_kt_entropy_genericType_t enum_entropy, const bool get_freq, s_kt_matrix_base_t *mat_result) {

  // FIXME: write matrix-funcitons for metrics ... eg, wrt. "kt_entropy__matrix_Shrink__pair__KL(...)" ... 

/**
ok:// FIXME: include ... "entropy.shrink.R"
// FIXME: .... new "hp_entropy.h" .... which provide/prsent a simpfleid interface to oru "kt_entropy.h" ... 


---------------------------------------
// FIXME[perf]: ... log-based ... Shannon ... KL ... shrink ... 
// FIXME[perf]: ... non-log-based ... 
// FIXME[perf]: ... 
// FIXME[perf]: ... logic to parse a string of scores seeprated by a sepeator ... then validate taht our aprpaoch amnages to load 100,000 entiees cosndierably faster than 10 seconds (where latter is the R-time to laod a vector) .... then udpate our aritlce wr.t such a support ....
// FIXME: .... new "hp_entropy.h" .... support ... matrix-copmtaution + sim-evla-aomcptuations .... eg, to first construct a simarlity-emtrics ... from entorpy-eval, and then call fucntiosn (eg, chi2) ... 

// FIXME[conceputal: ... desribe sifferetn operaotrs/strategies wrt. 'summing' of scores ... 
// FIXME[conceputal: ... 
// FIXME[conceputal: ... 

---------------------------------------
// FIXME[perf]: ... tiem-diff-overhead ... wrt. [above] ... ie, the perofmrance-effects of comptuign entropy and simarlity/dirvergence in perate comtpatuion-steps ... 
// FIXME[perf]: ... 
// FIXMe[tut+fig]: udpat eour R-meausrements ... use the "sum(..)" oerpator ... ie, as latter reudc ehte R-comptaution-spee by approx. 100x .... 
---------------------------------------
// FIXME: include ... "entropy.R"
// FIXME: .... include ... an API-itnerface to our "mine.c" ... eg, wrt. "mine_compute_score_notAllocate_logValues(..)" ... and in thi context ensreu that we 'remeber' to pre-allcoat ethe defualt MINE-log-data-struct ... 
// FIXME: .... new "hp_entropy.h" .... support ... 
// FIXME: .... new "hp_entropy_cmd.h" .... ie, a tmerinal/command/bash based API-interface. 
 **/

  // FIXME[conceptaul]: add support ... for idnieyign min-max, avg ... wrt. the "a" score-comtpatuions .... wrt. log-optmizaliaiotn.

  //! Nte: .... 
  //! Nte: .... 
  //! Nte: .... 
  assert(false); // FIXME[pre]: solve/address all of the [ªbove] FIXMEs ... 
  assert(false); // FIXME: write permtuation of entorpy-emtrics ... where we ... support SSE ... using our mien-impeltmatnion as a tempalte/basis ... 
  assert(false); // FIXME: write permtuation of entorpy-emtrics ... where we ... inline functions ... and use the aprallsiaotn-support in/fomr our mine.c impemtantion .... 
  assert(false); // FIXME: write permtuation of entorpy-emtrics ... where we ... 
  assert(false); // FIXME: complete ... 
  assert(false); // FIXME: add itnerface to MINE .... eg, wr.t itnalizaitonsros ... or export user to have 'set' these.
  assert(false); // FIXME: support generic simalrity-emtrics .... ie, after 'fetching' for frequencies ... 

  assert(false); // FIXME[concpetual]: relate 'this' to the comptautoin of semantic-siamrlityies ... ie, to ......??....
  // -------------------
  assert(false); // FIXME[code]: ... supprot entorpy-cotmpatuioni of a matrix where a max-count is known (eg, for gene-expresions-evalautiosn on large data-sets) .... where we instead of first parign the compelte matrix insteade comptues entropy when a 'line-end' is met/encountered .... 
  assert(false); // FIXME[tut+eval]: ... evaluate/comapre aboe appraoch to a nviae strategy .... aserting/statig that our appraoch manages a .....??... perofmrance-boost (when comarped to anive appraoches) ..... 
  // -------------------
  assert(false); // FIXME[code]: ... this fucntion amnages to cocndiealby reduce overhead ... eg, wrt the ENtropy-aopgacek strategy for comptugin the 'srhink(..) entriopy for eahc vector-pari .... ie, instead of first copmuting the latter seperately for each .... where time-overheaqd (Wr.t the latter) is observed to be .......??... 
  assert(false); // FIXME[tut+eval]: ... evaluate/comapre aboe appraoch to a nviae strategy .... aserting/statig that our appraoch manages a .....??... perofmrance-boost (when comarped to anive appraoches) ..... 
  assert(false); // FIXME[tut+eval]: ...
  // -------------------
  assert(false); // FIXME: ... 
  assert(false); // FIXME[tut+eval]: ... evaluate/comapre aboe appraoch to a nviae strategy .... aserting/statig that our appraoch manages a .....??... perofmrance-boost (when comarped to anive appraoches) ..... 
  assert(false); // FIXME[tut+eval]: ...
  // -------------------
  assert(false); // FIXME: ... data-parsing ... to enable seharing for best-match between a sampel-data-set versus a large collection of entites .... provide support for ... paring a signel row ... then compare the row to an inptu-amtrix ... where results is used to ....??.... --TODO[concpetaul]: idneityf/descirbe a use-ase wrt . such ... are mote this into the future-work-seicotn ... 
  // -------------------
  assert(false); // FIXME[code]: ... add parallell wrappers ... to our matrix-comtpatuion-wrapper .... copy-paste from our MIEN-impemtatnion .... 
  // -------------------
  assert(false); // FIXME[code]: ... 
  // -------------------
  assert(false); // FIXME[code]: ... get a Perl-wrapper-function to work for our "hp_entropy" and our "hp_api_entropy" .... where a use-case is to comptue entropy for Shannons entorpy-fucntion ... 
  // -------------------
  assert(false); // FIXME: ... 
  // -------------------
  assert(false); // FIXME: ... "kt_semanticSim.h" ... wirte tuts (for the latteR)
  assert(false); // FIXME: ... "kt_semanticSim.h" ... cmd-interface 
  assert(false); // FIXME: ... "kt_semanticSim.h" ... "README.md"
  // -------------------
  assert(false); // FIXME: ... 
  // -------------------
}
static void kt_entropy__generic__matrix__standAlone(const s_kt_matrix_base_t *mat_input_1, const s_kt_matrix_base_t *mat_input_2, e_kt_entropy_genericType_t enum_entropy, const bool get_freq, s_kt_matrix_base_t *mat_result) {
  assert(false); // FIXME: include directly ... "kt_entropy__freqMatrix_Shrink__pair__KL(...)"
  assert(false); // FIXME: include directly ... 
  assert(false); // FIXME: include directly ... 

  assert(false); // FIXME: complete ... 

}




#endif //! EOF
