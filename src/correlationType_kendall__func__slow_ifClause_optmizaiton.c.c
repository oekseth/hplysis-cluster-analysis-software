
if(mask1) {assert(mask1[index1]);} //! ie, as we then expect thsi to be set.
if(mask2) {assert(mask2[index2]);} //! ie, as we then expect thsi to be set.

for(uint i = 0; i < n; i++) {
  const t_float x1 = data1[index1][i]; const t_float y1 = data2[index2][i];

  if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
    for(uint j = 0; j < i; j++) {
      if( (!mask1 || mask1[index1][j]) && (!mask2 || mask2[index2][j]) ) {
	//if(mask1[index1][j] && mask2[index2][j]) {

	// FIXME: describe how this apporahc may be re-written into memory-effeicnt code. <-- try to use ofe intermeidate/extra/temorary-allcoated result-table, where we use enums to set the values of '<', '>', '==', and '!='. Thereafter we we iterate through the two matrices to update/infer "con", "dis", "exx" and "exy" (seperately for each row. <-- is the latter correct for 'transpose', ie, if we update for ...??...
	// FIXME: update [ªbove] and for "transpose == 1" ... ie, before implemnting an optmized version (of thsi funciotn).
	const t_float x2 = data1[index1][j]; const t_float y2 = data2[index2][j];
	if(isOf_interest(x1) && isOf_interest(x2) && isOf_interest(y1) && isOf_interest(y2) ) {

	  // FIXME: comapre the time-difference between [below] approaches ... ie, the signvace-time-cost of icnreased instruction-memory-cache.
#if(__localConfig_ifCaluseAlt_1 == 1)
	  //	      if(true) 
	  { 
	    //! Note: in [”elow] we idenify the number of pairs which need a 'shift'/'swap' to 'fit'/'update' .... ie, for each 'inner-pair.x' we count the number of values outer-pair.y which are either less than, larger than or equal to inner-pair-y. If we both know the the 'sorted range' of outer-pair.y, eg, outer-pair.y.sorted_index (and where the sorted_index has been post-processed to discard duplicates/overlaps), then a comparison of (x[i].value <? x[i+1].value) and ||y[i].sorted_index, y[i+1].sorted_index|| would provide/yeild  ....??...  <-- this would only be correct for the ouer-y-values which has an x-value 'bound by the latter criteria' .... ie, we need to idnetify 'range-counts' seperately for .... 
	    if(x1 < x2) { //! which in a sorted list implies to count the matches of y[x[i|sorted].index] VS y[x[i|sorted].index] <-- ie, 
	      if(y1 < y2) {con++;}
	      else if(y1 == y2) {exy++;}
	      else /*if y1 > y2*/ {dis++;} //! ie, a swap.
	    } else if(x1 == x2) {
	      if(y1 != y2) {exx++;}
	    } else { //! ie, x1 > x2
	      if(y1 < y2) {dis++;} //! ie, a swap.
	      else if(y1 == y2) {exy++;}
	      else /*if y1 > y2*/ {con++;} 
	    }
	  }
#else
	  {
	    if(x1 < x2 && y1 < y2) con++;   /* OR */  
	    if(x1 < x2 && y1 > y2) dis++;
	    //! ---
	    if(x1 > x2 && y1 < y2) dis++;   /* OR */  
	    if(x1 > x2 && y1 > y2) con++; 
	    //! ---- 
	    if(x1 == x2 && y1 != y2) exx++; /* OR */  
	    if(x1 != x2 && y1 == y2) exy++;
	  }
#endif
	  flag = 1;
	}
      }
    }
  }
 }
