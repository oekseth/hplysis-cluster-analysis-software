#include "correlationType_kendall_partialPreCompute_kendall.h" 

//! Set the the "s_correlationType_kendall_partialPreCompute_kendall" data-structure to empty.
void setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self) {
  assert(self);
  self->matrixSorted_column = NULL; self->matrixSorted_column_index = NULL;
  self->arr1_tmp = NULL; self->arr2_tmp = NULL; 
  self->nrows = 0; self->ncols = 0;
}

//! De-allocat the "s_correlationType_kendall_partialPreCompute_kendall" data-structure to empty.
void free__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self) {
  assert(self);
  if(self->matrixSorted_column) {free_2d_list_float(&self->matrixSorted_column, self->nrows);}
  if(self->matrixSorted_column_index) {free_2d_list_uint(&self->matrixSorted_column_index, self->nrows);}
  if(self->arr1_tmp) {free_1d_list_float(&self->arr1_tmp);}
  if(self->arr2_tmp) {free_1d_list_float(&self->arr2_tmp);}
  
  //! Tehn explcitly reset- the vlaues:
  setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(self);
}


//! Initiate the "s_correlationType_kendall_partialPreCompute_kendall" data-structure.
//! @remarks In the 'itnation' we comptue the ranks for the set of rows, ie, may take some seconds for large matrices.
void init__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self, const uint nrows, const uint ncols, t_float **data, char **mask, const bool needTo_useMask_evaluation) {
  assert(self);
  setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(self); //! ie, a 'safe-guard'.
  self->nrows = nrows; self->ncols = ncols;

  //! First allocate:
  self->matrixSorted_column = allocate_2d_list_float(nrows, ncols, /*default-value=*/0); 
  self->matrixSorted_column_index = allocate_2d_list_uint(nrows, ncols, /*default-value=*/0); 
  float *matrix_tmp_1_vector = allocate_1d_list_float(ncols, /*default-value=*/0);
  float *matrix_tmp_1_vector_tmp_array = allocate_1d_list_float(ncols, /*default-value=*/0);
  //! Then we compute the ransk for the input-matrix, ie, as the sorted valeus (in the 'input-data') will enver change: when combined witht he masks in "cdata" the results is correct ranks.
  


  for(uint index1 = 0; index1 < nrows; index1++) {
    //! Step(1): sort: --------------------------------------------------------------------------------------
    assert(data[index1]);
    //! Note: our "Kendall-Knight" impelmtnaiton is cosntructed to handle the case wehre makss are diffenret for two columsn, ie, which expalins why we may use 'this approach' for columsn which have different 'valeus of interestin' (ie, where the mask-selecitons are different).
    ktCorrelation_compute_rank_forVector(index1, /*size=*/ncols, /*data=*/data, /*mask=*/mask, /*temp-array=*/matrix_tmp_1_vector_tmp_array, /*the back-infex-reference=*/self->matrixSorted_column_index[index1], /*result-array=*/matrix_tmp_1_vector, e_distance_rank_typeOf_computation_ideal, needTo_useMask_evaluation, self->matrixSorted_column[index1]);
  }


  //! Allocate temporary lists used in [”elow]:
  free_1d_list_float(&matrix_tmp_1_vector);
  free_1d_list_float(&matrix_tmp_1_vector_tmp_array);
  const t_float empty_0 = 0;
  self->arr1_tmp = allocate_1d_list_float(ncols, /*default-value=*/empty_0);
  self->arr2_tmp = allocate_1d_list_float(ncols, /*default-value=*/empty_0);
  
}

/**
   @brief comptue the correlations by applying the pre-computed ranks to a differnet data-vector.
   @param <self> is the object which hold data for index1
   @param <index1> is the identity of the internal rank to be sued.
   @param <vector_2> is the data-row to compare with
   @param <mask2_vector> is the mask used to identify the set of interesting results.
   @param <metric_id> is the metric to be used speicically for the type of kendall-correlation to be comptued.
   @param <needTo_useMask_evaluation> which is used as a cofnigruation-parmeter wrt. rank-comptautions (in Kendall Tau's corrleation-metric).
   @return the inferred correlation-metric
 **/
t_float compute_results_applyObjectToExternalRow__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self, const uint index1, t_float *vector_2, char *mask2_vector, const e_kt_correlationFunction_t metric_id, const bool needTo_useMask_evaluation) {
  //! What we expect:
  assert(self);
  assert(self->nrows > 0);
  assert(index1 < self->nrows);
  assert(vector_2);

  //! Comptue the correlation-measure:
  return kendall_improvment_kingsAlgorithm(self->ncols, /*vector1_sorted=*/self->matrixSorted_column[index1], vector_2, mask2_vector, self->arr1_tmp, self->arr2_tmp,  /*list_column_index=*/self->matrixSorted_column_index[index1],  metric_id, /*needTo_useMask_evaluation=*/true); //, needTo_useMask_evaluation);
}
