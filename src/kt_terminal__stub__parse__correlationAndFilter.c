

//    printf("\n\n ..................... at %s:%d\n", __FILE__, __LINE__);

{ //! Comptue the cluster-analysis and write out (ie. if requested by the user) the computed correlation-matrices.
  //! Note: comptue the correlations and filter-applications, and write out (ie. if requested by the user) the computed correlation-matrices.
  if(self->isToApply_correlation_beforeFilter) {     
    const bool local_isToMerge = (self->whenToMerge_differentMatrices == e_kt_terminal_whenToMerge_beforeFilter);
    const bool writeOut_corrMetrics = self->isToApply_correlation_beforeFilter_includeInResult;
    s_kt_correlationMetric_t metric_local = self->metric_beforeFilter;
    const char *label_1 = "matrix_beforeFilter";    
    //const char *label_2 = "matrix_beforeFilter";

    //  printf("at %s:%d\n", __FILE__, __LINE__);
#include "kt_terminal__func__applyCorrMetrics.c" //! ie, update the correlation-metircs:
  }

  /* printf("\n\n ..................... self->isTo_applyDataFilter=%d, at %s:%d\n", self->isTo_applyDataFilter, __FILE__, __LINE__); */
  /*     printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__); */

  // TODO[JC]: may you reflect/evaluate if we 'if one of the [below] optiosn are set' ... should limit/contraint our 'a''plicaiton Not usign the devalut strateiges'? (oekseht, 06. des. 2016).
  if(self->isTo_applyDataFilter || self->isTo_applyDataAdjust) {

    // TODO: as an optization-step consider to add/write logics to adjust the matrix-diemsions based on [above] ... and then use these 'index-remappings' to (a) update 'our' matrix-object and (b) the string-name-mappings (ie, if any).  <-- add a new mapping-table for old->new and new->old .... for rows--cols .... <-- update our "kt_matrix" object 'with such a mapping-scheme', and then 'call this object' fro 'every time' a result-data-sets is 'generated'.

    if(needToMerge_results_areMerged == true) {
      {
	s_kt_matrix_filter_t filter_obj;      
	t_float **matrix = self->inputMatrix_result.matrix;
	const uint nrows = self->inputMatrix_result.nrows;
	const uint ncols = self->inputMatrix_result.ncols;
	assert(nrows > 0); 	assert(ncols > 0);
	assert(matrix);
	//! Intaite the local object:
	allocate__s_kt_matrix_filter_t(&filter_obj, /*isTo_init=*/true, matrix, nrows, ncols);
	//!
	//! Apply the filters:
	//    printf("\n\n ..................... at %s:%d\n", __FILE__, __LINE__);
	apply_s_kt_matrix_filter_t(&filter_obj,
				   //&(self->filter_middleOf_correlationApplication), 
				   self->filter_middleOf_correlationApplication.orderOf_filter_eitherOr, 
				   self->filter_middleOf_correlationApplication.orderOf_filter_cellMask,
				   self->filter_middleOf_correlationApplication.orderOf_adjustment
				   //, self->isTo_applyDataFilter, self->isTo_applyDataAdjust
				   );
	//! De-allocate:
	free__s_kt_matrix_filter_t(&filter_obj);
      }
    } else {
      { //! Then we analsye seperately for: matrix-1:
	s_kt_matrix_filter_t filter_obj;      
	t_float **matrix = self->inputMatrix_1.matrix;
	const uint nrows = self->inputMatrix_1.nrows;
	const uint ncols = self->inputMatrix_1.ncols;
	assert(matrix);
	//! Intaite the local object:
	allocate__s_kt_matrix_filter_t(&filter_obj, /*isTo_init=*/true, matrix, nrows, ncols);
	//!
	//! Apply the filters:
	apply_s_kt_matrix_filter_t(&filter_obj,
				   //&(self->filter_middleOf_correlationApplication), 
				   self->filter_middleOf_correlationApplication.orderOf_filter_eitherOr, 
				   self->filter_middleOf_correlationApplication.orderOf_filter_cellMask,
				   self->filter_middleOf_correlationApplication.orderOf_adjustment
				   //, self->isTo_applyDataFilter, self->isTo_applyDataAdjust
				   );
	//! De-allocate:
	free__s_kt_matrix_filter_t(&filter_obj);
      }
      { //! Then we analsye seperately for: matrix-2:
	s_kt_matrix_filter_t filter_obj;      
	t_float **matrix = self->inputMatrix_2.matrix;
	const uint nrows = self->inputMatrix_2.nrows;
	const uint ncols = self->inputMatrix_2.ncols;
	assert(matrix);
	//! Intaite the local object:
	allocate__s_kt_matrix_filter_t(&filter_obj, /*isTo_init=*/true, matrix, nrows, ncols);
	//!
	//! Apply the filters:
	apply_s_kt_matrix_filter_t(&filter_obj,
				   //&(self->filter_middleOf_correlationApplication), 
				   self->filter_middleOf_correlationApplication.orderOf_filter_eitherOr, 
				   self->filter_middleOf_correlationApplication.orderOf_filter_cellMask,
				   self->filter_middleOf_correlationApplication.orderOf_adjustment
				   //, self->isTo_applyDataFilter, self->isTo_applyDataAdjust
				   );
	//! De-allocate:
	free__s_kt_matrix_filter_t(&filter_obj);
      }
    }
  }

  //    printf("at %s:%d\n", __FILE__, __LINE__);    


  //printf("\n\n ..................... at %s:%d\n", __FILE__, __LINE__);

  if(self->isToApply_correlation_afterFilter) {
    const bool local_isToMerge = (self->whenToMerge_differentMatrices == e_kt_terminal_whenToMerge_afterFilter);
    const bool writeOut_corrMetrics = self->isToApply_correlation_afterFilter_includeInResult;
    s_kt_correlationMetric_t metric_local = self->metric_afterFilter;
    const char *label_1 = "matrix_afterFilter";    
    //const char *label_2 = "matrix_afterFilter";
#include "kt_terminal__func__applyCorrMetrics.c" //! ie, update the correlation-metircs:
  }

  //  printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);

  /*   } */
  
  
  /*   if(!needToMerge_results_areMerged && (self->whenToMerge_differentMatrices == e_kt_terminal_whenToMerge_beforeClustering) ) { */
  /*     const bool local_isToMerge = true; */
  /*     const bool writeOut_corrMetrics = false; */
  /*       s_kt_correlationMetric_t metric_local =  */
  /*       const char *label_1 = "matrix_afterFilter";       const char *label_2 = "matrix_afterFilter"; */
  /* #include "kt_terminal__func__applyCorrMetrics.c" //! ie, update the correlation-metircs: */
  /*     } */


  //    printf("at %s:%d\n", __FILE__, __LINE__);

  if(needToMerge_results_areMerged == false) {
    if(self->inputMatrix_result.matrix == NULL) {
      //! TODO: cosnider add a 'default hadnlign' wrt. this case <-- if so, then wehat arre the distance-metirc to be sued wrt. the 'merging'?
      fprintf(stderr, "!!\t We expected the two matrices to be merged before the clsuter-anlasysis, a case which does not hold: we will now abort, ie, please update your connfiguraitonis: for quesitons please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. nov. 2016).
      
      // printf("\n\n# nameOf_columns=%p, at %s:%d\n", self->inputMatrix_2.nameOf_columns, __FILE__, __LINE__);

      if(true) {
	//! Then we expec the second input-matrix to have data:
	assert(self->inputMatrix_2.ncols != 0);
	assert(self->inputMatrix_2.nrows != 0);
	const bool local_isToMerge = true;
	const bool writeOut_corrMetrics = false;
	s_kt_correlationMetric_t metric_local = self->metric_insideClustering;
	fprintf(stderr, "!!(note)\t To overcome above issue we now use the correlation-properties specified for 'metric-inside-clustering': for questions please contac the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	const char *label_1 = "matrix_beforeCorr_forced";    
	//const char *label_2 = "matrix_beforeFilter";
	
	//	printf("at %s:%d\n", __FILE__, __LINE__);
#include "kt_terminal__func__applyCorrMetrics.c" //! ie, update the correlation-metircs:

      } else {
#ifndef __is_called_fromTestFunction
      return 0;
#endif
      }
    } else if(self->inputMatrix_result.nrows != self->inputMatrix_result.ncols) {
      fprintf(stderr, "!!\t We expected the input-matrix to be symmetric, though in cotnrast to our expectiaotns the matrix has the demensions=(%u, %u), a case which does not hold: we will now abort, ie, please update your connfiguraitonis: for quesitons please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", self->nrows, self->ncols, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. nov. 2016).
    } else {
      assert(self->inputMatrix_result.matrix == NULL); //! ie, what we expect.
      self->inputMatrix_result.matrix = self->inputMatrix_1.matrix; self->inputMatrix_1.matrix = NULL;
      self->inputMatrix_result.weight = self->inputMatrix_1.weight; self->inputMatrix_1.weight = NULL;
      //! Note: we expect the result is an djcency-matrix:
      self->inputMatrix_result.nrows = self->inputMatrix_1.nrows; 	self->inputMatrix_result.ncols = self->inputMatrix_1.nrows; 
      //! Update the name-mappigns, ie, based on the assumption tha thte result is an acjency-matrix:
      self->inputMatrix_result.nameOf_rows = self->inputMatrix_1.nameOf_rows;
      self->inputMatrix_result.nameOf_columns = self->inputMatrix_1.nameOf_rows;
      self->inputMatrix_1.nameOf_rows = NULL;
    }
  }
}  
if(needToMerge_results_areMerged == false) {assert(false);} //! ie, as we shoudl have handled this case athtis exueicotn-point'.
assert(self->inputMatrix_result.matrix != NULL);
