#ifndef kt_distance_aux_h
#define kt_distance_aux_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_distance_aux
   @brief provide auxialitere-routiens for our simlairty/distance metrics (oekseth, 06. mar. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
**/


#include "e_kt_correlationFunction.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_swig_retValObj.h"

/**
   @brief idetnify the enum-id based on the number-pair for a (group, enum-member) correlation-metric (oekseth, 06. sept. 2016).
   @param <stringOf_enum> is the 'stringeid' representation of the enum
   @param <printAn_errorIf_notFound> is set to false if a'it may be acceptible' to Not find an asiscated enum.
   @return the enum describign the pair
 **/
e_kt_correlationFunction_t get_enumBasedOn_configuration__kt_metric_aux(const char *stringOf_enum, const bool printAn_errorIf_notFound);
/**
   @brief idetnify the enum-id based on the number-pair for a (group, enum-member) correlation-metric (oekseth, 06. sept. 2016).
   @param <charOf_group> is the number identifying the enum-group
   @param <charOf_groupMember> is the number identifying the metric in charOf_group
   @return the enum describign the pair
 **/
e_kt_correlationFunction_t get_enumBasedOn_configuration_numberPair_nonRank(const char charOf_group, const char charOf_groupMember);
/**
   @brief idetnify the enum-id based on the number-pair for a (group, enum-member) correlation-metric (oekseth, 06. sept. 2016).
   @param <stringOf_enum> is the 'stringeid' representation of the enum
   @return the enum describign the pair
 **/
e_kt_correlationFunction_t get_enumBasedOn_configuration(const char *stringOf_enum);
/**
   @brief idetnify the enum-id based on the number-pair for a (group, enum-member) correlation-metric (oekseth, 06. sept. 2016).
   @param <stringOf_enum> is the 'stringeid' representation of the enum
   @param <scalar_result> the enum describign the pair
 **/
void get_enumBasedOn_configuration_scalar(const char *stringOf_enum, uint *scalar_result);

//! Write otu a stringifed verison fo the correlation-metrics in Knitting-Tools (oekseth, 06. nov. 2016).
void printAll_correlationMatric_enums__kt_distance();

//! Idenitfy a human-redable representiaont of a gvien enum-id (oekseth, 06. otk. 2016).
//! @return the string-representitonat of the enum in quesiton.
const char *get_stringOf_enum__e_kt_correlationFunction_t(const e_kt_correlationFunction_t enum_id); 

//! Idenitfy a 'shortened' human-redable representiaont of a gvien enum-id (oekseth, 06. apr. 2017).
//! @return the string-representitonat of the enum in quesiton.
static const char *get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(const e_kt_correlationFunction_t enum_id) {
  const char *stringOf_enum = get_stringOf_enum__e_kt_correlationFunction_t(enum_id);
  const char *search_prefix = "e_kt_correlationFunction_groupOf_";
  const char *sub_string = strstr(stringOf_enum, search_prefix);
  if(sub_string && strlen(sub_string)) {
    stringOf_enum += strlen(search_prefix);
  }
  return stringOf_enum;
}
//! Idenitfy a human-redable representiaont of a gvien enum-id (oekseth, 06. otk. 2016).
//! @return the string-representitonat of the enum in quesiton.
static void get_stringOf_enum__e_kt_correlationFunction_t_scalar(const e_kt_correlationFunction_t enum_id, 
								 s_kt_swig_retValObj_t *scalar_result
								 //const char **scalar_result
								 ) {
  assert(scalar_result);
  scalar_result->name = get_stringOf_enum__e_kt_correlationFunction_t(enum_id);
}

//! Idenitfy a human-redable representiaont of a gvien enum-id (oekseth, 06. otk. 2016).
//! @return the string-representitonat of the enum in quesiton.
const char *get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(const e_kt_categoryOf_correaltionPreStep_t enum_id); 
//! Idenitfy a human-redable representiaont of a gvien enum-id (oekseth, 06. otk. 2016).
//! @return the string-representitonat of the enum in quesiton.
static void get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_scalar(const e_kt_categoryOf_correaltionPreStep_t enum_id, 
									   s_kt_swig_retValObj_t *scalar_result
									   //const char **scalar_result
									   ) {
  assert(scalar_result);
  scalar_result->name = get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(enum_id);
}


//! Idenitfy a human-redable representiaont of a gvien enum-id (oekseth, 06. otk. 2016).
//! @return the string-representitonat of the enum in quesiton.
const char *get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_xmtPrefix(const e_kt_categoryOf_correaltionPreStep_t enum_id); 
//! Idenitfy a human-redable representiaont of a gvien enum-id (oekseth, 06. otk. 2016).
//! @return the string-representitonat of the enum in quesiton.
static void get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_scalar_xmtPrefix(const e_kt_categoryOf_correaltionPreStep_t enum_id, 
									   s_kt_swig_retValObj_t *scalar_result
									   //const char **scalar_result
									   ) {
  assert(scalar_result);
  scalar_result->name = get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_xmtPrefix(enum_id);
}


//! @return the enum-attribute descriinb the pre-step described by teh string.
e_kt_categoryOf_correaltionPreStep_t get_typeOf_correlationPreStep_fromString(const char *stringOf_enum);
//! Update the scalar_result with the type of correlaitoni-pre-step.
static void get_typeOf_correlationPreStep_fromString_scalar(const char *stringOf_enum, uint *scalar_result) {
  assert(stringOf_enum); assert(stringOf_enum);
  *scalar_result = (uint)get_typeOf_correlationPreStep_fromString(stringOf_enum);
}


#endif //! EOF
