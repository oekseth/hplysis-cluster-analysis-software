{
  

  for (uint j = 0; j < ncolumns; j++) {
    uint count = 0;
#if (__typeOf_maskTo_use == 0)
    for(uint k = 0; k < n1; k++) {
      const uint i = (!map_index) ? k : map_index[k];
    }
#elif (__typeOf_maskTo_use == 1)
    for(uint k = 0; k < n1; k++) {
      const uint i = (!map_index) ? k : map_index[k];
      assert(i != UINT_MAX);
      assert_possibleOverhead(i < nrows);
      assert_possibleOverhead(j < ncolumns);
      if(isOf_interest(data[i][j])) {
	assert_possibleOverhead(count < temp_size); //! ie, the array-size which we expect.
	temp[count] = data[i][j];
	count++;
      }
    }
#else     
    for(uint k = 0; k < n1; k++) {
      const uint i = (!map_index) ? k : map_index[k];
      assert(i != UINT_MAX);
      if (mask[i][j]) {
	assert_possibleOverhead(i < nrows);
	assert_possibleOverhead(j < ncolumns);
	assert_possibleOverhead(count < temp_size); //! ie, the array-size which we expect.
	temp[count] = data[i][j];
	count++;
      }
    }
#endif
    if (count>0) {
      assert_possibleOverhead(count <= temp_size);
      arr_cdata[j] = get_median(temp, temp_sorted, count);
      arr_cmask[j] = 1;
    } else {
      arr_cdata[j] = 0.;
      arr_cmask[j] = 0;
    }
  }
  //! De-allcoate locally reserved memory:
  free_1d_list_float(&temp); 	free_1d_list_float(&temp_sorted);
}


#undef __typeOf_maskTo_use
