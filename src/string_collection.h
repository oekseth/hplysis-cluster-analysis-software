#ifndef string_collection_h
#define string_collection_h
/**
   @file 
   @brief Implements of a string_collection of labels, e.g. as basis for an hash list.
   @ingroup util
   @author Ole Kristian Ekseth (oekseth)
 **/
#include "types.h"
#include "libs.h"
#include "log_builder.h"

/**
   @class string_collection
   @brief Implements of a string_collection of labels, e.g. as basis for an hash list.
   @ingroup util
   @author Ole Kristian Ekseth (oekseth)
   @todo Consider adding possibility of building a hash-mapped string_collection for this label-array.
 **/
class string_collection {
 private:
  char *labels;
  uint size_labels;
  uint cnt_used_labels;
  //! Enlarges the internal list.
  void enlarge(uint elements_to_add) {    
  }
 public:
  //! @return the label given the index:
  char *get_label(uint index) {
    assert(index < cnt_used_labels);
    if((index < cnt_used_labels) && labels && size_labels) return labels[index];
    else return NULL;
  }

  //! @return the number of labels in this string_collection.
  uint get_size() {return cnt_used_labels;}

  //! Insert all the elements, given the string input array
  void insert_all(char **strings, uint cnt_strings) {
#ifndef NDEBUG
    //! Performs a manally copy in order to validate that no data are overwritten during the insertion process:
    char **temp = new char*[size_labels];
    log_builder::test_memory_condition_and_if_not_abort(temp!=NULL, __LINE__, __FILE__, __FUNCTION__);
    for(uint i = 0; i < size_labels; i++) temp[i] = strings[i];
    const uint cnt_used_labels_before = cnt_used_labels;
#endif
    //! Extend the pointer to the labels if it is to small:
    if((cnt_strings+cnt_used_labels+1) < size_labels) {
      if(!strings) {
	assert(!cnt_used_labels);
	assert(!size_labels);
	const uint new_size = 6 + 2*cnt_strings;
	strings = new char*[new_size];
	log_builder::test_memory_condition_and_if_not_abort(strings!=NULL, __LINE__, __FILE__, __FUNCTION__);
	for(uint i = 0; i < new_size; i++) strings[i] = NULL;
	size_labels = new_size;
      } else {
	const uint new_size = cnt_strings + 2*cnt_strings;
	new_strings = new char*[new_size];
	log_builder::test_memory_condition_and_if_not_abort(new_strings!=NULL, __LINE__, __FILE__, __FUNCTION__);
	//! Copies the pointer from the old- to the new pointer:
	for(uint i = 0; i < cnt_used_labels; i++) new_strings[i] = strings[i];	
	delete [] strings:
	strings = new_strings; new_strings = NULL;
	size_labels = new_size;
      }
    }
    //! Inserts the actual data:
    for(uint i = 0; i < cnt_strings; i++) {
      assert(strings[i]);
      assert(labels[cnt_used_labels+i] == NULL);
      const uint string_length = strlen(strings[i]);
      assert(string_length);
      labels[cnt_used_labels+i] = new char[string_length+1];
      labels[cnt_used_labels+i][string_length] = '\0';
      strncpy(labels[cnt_used_labels+i], strings[i], string_length);
      assert(0==strcmp(labels[cnt_used_labels+i], strings[i]));
      assert(labels[cnt_used_labels+1][string_length] == '\0');
    }
    cnt_used_labels += cnt_strings;
#ifndef NDEBUG
    //! Compares the updated list with our expectations:
    uint local_index = 0;
    for(uint i = 0; i < size_labels; i++) {
      if(i < cnt_used_labels_before) {
	assert(0==strcmp(labels[i], temp[i]));
      } else {
	assert(0==strcmp(labels[i], strings[local_index]));
	local_index++;
      }
    }
#endif
  }
/**
    @brief a getter/setter for alternate id(s) of this instance.
    @return the complete list of type string_collection_t.
  **/
  static string_collection *insert(string_collection *& obj, char **strings, uint cnt_strings) {
    if(!obj) obj = string_collection::constructor();
    obj->insert_all(strings, cnt_strings);     
    return obj;
  }

  //! @return true if data si found for the given label.
  bool contains(char *string) {return has_data(string);}
  /**

     @param <string> element to be removed from this string_collection, if present
     @param <found_at_index> The location of the index it was found; default set to UINT_MAX;
     @return true if data si found for the given label.
  **/
  bool has_data(char *string, uint &found_at_index) {
    //! FIXME: Consider using a hash here, and transfordming the below procesdure into a NDEBUG test method.    
    assert(string);
    found_at_index = UINT_MAX; // Default value.
    //! Tests if the value is found:
    bool found = false;
    for(uint i = 0; i < cnt_used_labels_before && !found; i++) {
      if(0 == strcmp(labels[i], string)) {found = true, found_at_index = i;}
    }
    return found;
  }

  //! @return trhe if data is set.
  bool has_data() {
    return (cnt_used_labels > 0);
  }
  /**
     @return true if it does not contain any data.
     @remarks Even though the function return true, memory may even though be reserved for this object, thereby if the intention is to deallocate, then call the static 'close' method.
  **/
  bool is_empty(){return (!has_data());}

  /**
     @brief removes an element from this string_collection if it is present
     @param <string> element to be removed from this string_collection, if present
     @return true if this string_collection contained the given element.
   **/
  bool remove(char *string) {
    uint found_at_index = UINT_MAX;
    //! Get the index for the string to remove:
    bool found = has_data(string, found_at_index);
    if(found) {
      assert(found_at_index < size_labels);
      //! De-allocates the memory reservation:
      delete [] labels[found_at_index]; labels[found_at_index] =NULL; 
      //! Update the last position in the internal storage-pointer, given its at the end.
      if((found_at_index+1)==cnt_used_labels) {
	cnt_used_labels--;
      }
    }
  }

  /**
     @brief tells whether this string_collection is equal to the given one
     @param <obj> The string_collection comparing it with.
     @return true if they are equal.
     @remarks Usage: set_obj->equals(another_set)
  **/
  bool equals(string_collection *obj) {
    // FIXME: This implementation is by default inefficient: If important, consider alternative approaches, e.g sorting/hashing during insertion.
    assert(obj); // Explicit what we assert.
    if(obj) {
      if(cnt_used_labels == obj->get_size()) return true;
      else {
	//! Compare element- by element:
	//! Note: If the internal list A contains less duplicate than the other (list B), then it would work fine as in-equalities would result in A not fiding all its elements in B, but the more difficult thing arises when A contains more duplicates than in B, which is solved by tracking all the found indexes in B, and then use a reciprocal call on those, given that all the values for string_collection A was satisfied (i.e. found).
	//! The internal list of reciprocal values.
	bool *lst_recip = new bool[cnt_used_labels];
	log_builder::test_memory_condition_and_if_not_abort(lst_recip!=NULL, __LINE__, __FILE__, __FUNCTION__);
	for(uint i = 0; i <cnt_used_labels;i++) {lst_recip[i]=false;}
	//! The comparison: Ends at the first 'not-found' occurence.
	bool found = true;	
	for(uint i = 0; i < cnt_used_labels && found; i++) {
	  uitn found_at_index = UINT_MAX;
	  if(0 != obj->has_data(labels[i], found_at_index)) {
	    found = false;
	  } else {lst_recip[found_at_index] = true;}
	}
	if(found) {
	  //! Confirms that all are set:
	  for(uint i = 0; i < cnt_used_labels && found; i++) {
	    if(!lst_recip[i]) {
	      //! If a duplicate in the 'obj' list, the 'not found' could be due to the fact that only the first match was used, therefore tests it:
	      uint temp_ind = UINT_MAX;
	      if(!has_data(obj->get_label(i, temp_ind))) {
		found = false;
	      }
	    }
	  }
	}
	//! Deallocates the reserved internal list:
	delete [] lst_recip; lst_recip = NULL;

	//! Our result is returned:
	return found;
      }
    }
  }
  //! @brief Clears the label-list, deallocating the memory of it.
  void clear() {
    if(labels) {
      assert(cnt_used_labels_before <= size_labels);
      for(uint i = 0; i < cnt_used_labels_before && !found; i++) {
	delete [] labels[i]; labels[i] = NULL;
      }
      delete [] labels; labels = NULL; cnt_used_labels_before = 0; size_labels = 0;
    }
  }

  //! Deallocates the memory for the object given.
  static void close(string_collection &*obj) {
    if(obj) {obj->clear(), delete obj; obj = NULL;}
  }
  //! Allocates- and initiates a string_collection type object.
  static string_collection *constructor() {
    return new string_collection();
  }

  //! The default constructor.
 string_collection() : labels(NULL), size_labels(0), cnt_used_labels(0)
    {}
}

typedef class string_collection string_collection_t;
#endif
