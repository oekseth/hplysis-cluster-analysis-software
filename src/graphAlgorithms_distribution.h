#ifndef graphAlgorithms_distribution_h
#define graphAlgorithms_distribution_h

/**
   @file graphAlgorithms_distribution
   @brief provide logics to compute distributions from datsets of values
   @author Ole Kristian Ekseth (oekseth)
 **/
#include "kt_distance_cluster.h"
#include "type_float_n.h"

/**
   @class graphAlgorithms_distribution
   @brief provide logics to compute distributions from datsets of values
   @author Ole Kristian Ekseth (oekseth)
 **/
class graphAlgorithms_distribution {
 public:
/**
   @brief distribution of values: compute the number of vertices/entries which are 'inside' each 'block of values'.
   @param <arrOf> is the input-list of values.
   @param <arrOf_result_scoreMinMax> is the minimum and maximum values for each bucket.
   @param <arrOf_result_counts> is the count/number of vertices which are 'inside' the range.
   @param <arrOf_result_sums> is the sum of scores assicated to each of the vertices: average valeus of each bucket is defined by "arrOf_result_sums[i]/arrOf_result_counts[i]"
   @param <numberOf_buckets> if set, then we infer the valueSpace from the min-max spread of values.
   @param <valueSpace_min> the lowest valeus which is included in the calcuclation/computation of the distribution
   @param <valueSpace_maxMinsOne> the firsrt value (in the numeric range) which is not included in the calcuclation/computation of the distribution
   @param <valueSpace_range> the fixed value-min-max deviation for each bucket: if set, then we expecs the numberOf_buckets to be set to UINT_MAX
**/
  static void narrowDown_usingFixedSpaceOf_value_uint(const list_generic<type_uint> &arrOf, list_generic<type_uint2x> &arrOf_result_scoreMinMax, list_generic<type_uint> &arrOf_result_counts, list_generic<type_uint> &arrOf_result_sums, const uint numberOf_buckets, const uint valueSpace_min = 0, const uint valueSpace_maxMinsOne = UINT_MAX,  uint valueSpace_range = UINT_MAX);
  /**
     @brief distribution of values: compute the number of vertices/entries which are 'inside' each 'block of values'.
     @param <arrOf> is the input-list of values.
     @param <arrOf_result_scoreMinMax> is the minimum and maximum values for each bucket.
     @param <arrOf_result_counts> is the count/number of vertices which are 'inside' the range.
     @param <arrOf_result_sums> is the sum of scores assicated to each of the vertices: average valeus of each bucket is defined by "arrOf_result_sums[i]/arrOf_result_counts[i]"
     @param <numberOf_buckets> if set, then we infer the valueSpace from the min-max spread of values.
     @param <valueSpace_min> the lowest valeus which is included in the calcuclation/computation of the distribution
     @param <valueSpace_maxMinsOne> the firsrt value (in the numeric range) which is not included in the calcuclation/computation of the distribution
     @param <valueSpace_range> the fixed value-min-max deviation for each bucket: if set, then we expecs the numberOf_buckets to be set to UINT_MAX
  **/
  static void narrowDown_usingFixedSpaceOf_value_float(const list_generic<type_float> &arrOf, list_generic<type_float_2x> &arrOf_result_scoreMinMax, list_generic<type_uint> &arrOf_result_counts,  list_generic<type_float> &arrOf_result_sums, const uint numberOf_buckets, const float valueSpace_min = FLT_MIN, const float valueSpace_maxMinsOne = FLT_MAX, float valueSpace_range = FLT_MAX);


  /**
     @brief distribution of counts: in contrast to "narrowDown_usingFixedSpaceOf_value_uint(..)" and "narrowDown_usingFixedSpaceOf_value_float(..)" this funciton use a 'fixed' window of counts, ie, not a 'fixed window of value-spread' (as used in the latteR).
     @param <arrOf> a sorted list of values
     @param <arrOf_result_scoreMinMax> is the minimum and maximum values for each bucket.
     @param <arrOf_result_counts> is the count/number of vertices which are 'inside' the range.
     @param <arrOf_result_sums> is the sum of scores assicated to each of the vertices: average valeus of each bucket is defined by "arrOf_result_sums[i]/arrOf_result_counts[i]"
     @param <numberOf_buckets> if set, then we infer the valueSpace from the min-max spread of values.
     @param <valueSpace_min> the lowest valeus which is included in the calcuclation/computation of the distribution
     @param <valueSpace_maxMinsOne> the firsrt value (in the numeric range) which is not included in the calcuclation/computation of the distribution
  **/
  static void narrowDown_usingFixedSpaceOf_count_uint(const list_generic<type_uint> &arrOf, list_generic<type_uint2x> &arrOf_result_scoreMinMax, list_generic<type_uint> &arrOf_result_counts,  list_generic<type_uint> &arrOf_result_sums, const uint numberOf_buckets, const uint valueSpace_min = 0, const uint valueSpace_maxMinsOne = UINT_MAX);
  /**
     @brief distribution of counts: in contrast to "narrowDown_usingFixedSpaceOf_value_uint(..)" and "narrowDown_usingFixedSpaceOf_value_float(..)" this funciton use a 'fixed' window of counts, ie, not a 'fixed window of value-spread' (as used in the latteR).
     @param <arrOf> a sorted list of values
     @param <arrOf_result_scoreMinMax> is the minimum and maximum values for each bucket.
     @param <arrOf_result_counts> is the count/number of vertices which are 'inside' the range.
     @param <arrOf_result_sums> is the sum of scores assicated to each of the vertices: average valeus of each bucket is defined by "arrOf_result_sums[i]/arrOf_result_counts[i]"
     @param <numberOf_buckets> if set, then we infer the valueSpace from the min-max spread of values.
     @param <valueSpace_min> the lowest valeus which is included in the calcuclation/computation of the distribution
     @param <valueSpace_maxMinsOne> the firsrt value (in the numeric range) which is not included in the calcuclation/computation of the distribution
  **/
  static void narrowDown_usingFixedSpaceOf_count_float(const list_generic<type_float> &arrOf, list_generic<type_float_2x> &arrOf_result_scoreMinMax, list_generic<type_uint> &arrOf_result_counts,  list_generic<type_float> &arrOf_result_sums, const uint numberOf_buckets, const float valueSpace_min = FLT_MIN, const float valueSpace_maxMinsOne = FLT_MAX);

  //! Identify the spread of values in a givne array.
  static uint *compute_c(const uint *P_map, const uint p, const int n) {
    assert(P_map);   assert(n > 0); assert(p > 0); 
    assert(p < (1000*1000*10)); //! ie, as we otehrwise should use a try-catch-block.
    uint *c = new uint[p];
    //! Intialize:
    memset(c, 0, p*sizeof(uint));
    
    // FIXME: describe a use-case for this funciton.
    
    for (uint i=0; i<n; i++) {
      assert(P_map[i] != UINT_MAX); assert(P_map[i] < p);
      c[P_map[i]]++; //! ie, the count of values in the P_map array.
    }
    for (uint i=1; i<p; i++) {
      c[i] += c[i-1]; //! ie, the count of values which are either smaller or with the same value as c[i].
    }  
    return c;
  }


  /**
     @brief the system test for class rule_set.
     @param <print_info> if set a limited status information is printed to STDOUT.
     @return true if test completed.
  **/
  static bool assert_class(const bool print_info);
};
#endif
