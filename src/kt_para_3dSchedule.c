#include "kt_para_3dSchedule.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


//! De-allocate the s_kt_para_3dSchedule_t object.
void free__s_kt_para_3dSchedule(s_kt_para_3dSchedule_t *self) {
  assert(self);
  if(self->mapOf_blockId_startEachBlock_rows) {
    free_2d_list_uint(&(self->mapOf_blockId_startEachBlock_rows), self->cnt_cores); self->mapOf_blockId_startEachBlock_rows = NULL;
    self->mapOf_blockId_startEachBlock_rows = 0;
  }
  if(self->mapOf_blockId_startEachBlock_rows_size) {
    free_2d_list_uint(&(self->mapOf_blockId_startEachBlock_rows_size), self->cnt_cores); self->mapOf_blockId_startEachBlock_rows_size = NULL;
    self->mapOf_blockId_startEachBlock_rows = 0;
  }
  //! ----
  if(self->mapOf_blockId_startEachBlock_cols) {
    free_2d_list_uint(&(self->mapOf_blockId_startEachBlock_cols), self->cnt_cores); self->mapOf_blockId_startEachBlock_cols = NULL;
    self->mapOf_blockId_startEachBlock_cols = 0;
  }
  if(self->mapOf_blockId_startEachBlock_cols_size) {
    free_2d_list_uint(&(self->mapOf_blockId_startEachBlock_cols_size), self->cnt_cores); self->mapOf_blockId_startEachBlock_cols_size = NULL;
    self->mapOf_blockId_startEachBlock_cols = 0;
  }
  //! ----
}


/**
   @brief identify the block-size for the thread_id in question.
   @param <self> is the intialized object which hold the thread-id-allocations.
   @param <thread_id> is the core to get the allocatiosn for.
   @param <block_id> is the current block-id to get the row-chunk for
   @param <scalarStart_row_id> is the row-start-postion of the iteraiotn
   @param <scalarStart_col_id> is the column-start-postion of the iteration
   //@return the number of elemnets to iterate over, ie, (scalarStart_row_id + ret-val, scalarStart_col_id + ret-val).
   @remarks a slow permtaution of the ''id-fetching': may have signficant implicatiosn for small block-sizes.
 **/
void slow2__idneitfy_indexRange__s_kt_para_3dSchedule_t(const s_kt_para_3dSchedule_t *self, const uint thread_id, const uint block_id, uint *scalarStart_row_id, uint *scalarStart_col_id, uint *scalarEnd_row_id, uint *scalarEnd_col_id) {
#include "kt_para_3dSchedule__func__findIndexRange.c" //! ie, apply the lgoics
}
