

#ifndef __config__funcTemplateAllAgainstAll__storeIntermeidateResults //! (eosekth, 06. otk. 2016).
#error "!!\t A boolen parameter si not set, ie, pelase investigae your calls: for quesiotns please contact the devleoepr at [oekseth@gmail.com]."
#endif
#ifndef __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes
#error "!!\t A boolen parameter si not set, ie, pelase investigae your calls: for quesiotns please contact the devleoepr at [oekseth@gmail.com]."
#endif
#ifndef __config__funcTemplateAllAgainstAll__inlinePostProcesType //! which refers to the macros such as "macro__e_allAgainstAll_SIMD_inlinePostProcess_none_storeIntermediateResults_forMPI" in ... ie, wrt. "e_allAgainstAll_SIMD_inlinePostProcess_t" (oekseht, 06. otk. 2016).
#error "!!\t A boolen parameter si not set, ie, pelase investigae your calls: for quesiotns please contact the devleoepr at [oekseth@gmail.com]."
#endif


//! Copmute the optmial distance-matrix:
//! Note: the idea (in the comptuation of the distance in a transposed matrix) is to 'move' "i" and "index1" vertical and "index1"  along the vertical axis. The implicaiton is that 'the sum of columns' is paritally updated for each row: instead of seperately coputing "[i][index1] x [i][index2]" for each pair of ("index1", "index2"), we for each "i" update ("index1", "index2"). The implication is that we accessess different column-index for "data1" and "data2": otherwise the 'space' [data2[i][index1] .... data2[i][index2]] would never have been fixed, ie, we would not have computed an 'all-against-all-sum'. From the latter we infer/observe that "index1" may be 'seen' as 'axis0' in the interprreation/comptuation, while "index" is interpreted as 'axix2' (in the comptuation/'loop'). 
//! Note(2): to understand why "j2" is used to uupdate/identify "resultMatrix[index1][index2]" and "data1[i][index1]" note that if "k2" had been used, then we would have computed the 'non-transposed' matrix, ie, the results would have been wrong: in order to update different resultMatrix[index1][index2] we need to 'shift' (the columns) between each new-identifed value, hence the need to use the inner "j2" counter. Wrt. the "rmul1[j2]" we observe that the counters need to be different (between "rmul1" and "rmul2"), ie, as we otehrwise would compute ....
//! Note(3): in this approxh we order the for-loops based on comparison of results from "e_typeOf_optimization_distance_none_xmtOptimizeTransposed" and "e_typeOf_optimization_distance_none_xmtOptimizeTransposed_subOptimal"
//void correlationFunc_optimal_compute_allAgainstAll_SIMD(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const s_allAgainstAll_config_t config) 
{  //! Apply the logics in the all-agaisnta-all-tiled-comparison-approach:
//, const uint CLS, const bool isTo_use_continousSTripsOf_memory, uint iterationIndex_2, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation, void *s_inlinePostProcess, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const s_kt_correlationConfig self, s_kt_computeTile_subResults_t *complexResult) { //const e_cmp_masksAre_used_t  masksAre_used) {

  // FIXME: consider to move this function into seerval/new 'sub-functions'.

  if(config.transposed == 1) {
    fprintf(stderr, "!! We expected a transpeod==0 matrix to be provided as input, ie, please validate correctness of your call: to efficently comptued fro transpsoed matrix we would reccomend first 'transposing the transposed matrix'. For questions, please do not hesistate cotnacting the developer at [oekseth@gmail.com]. Will now abort the exeuction (of this funciton). Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  if(config.isTo_use_continousSTripsOf_memory == false) {
    fprintf(stderr, "!!\t Your memory-allcoation rotuien were not as effective as we had expected, ie, please update your allcoation-rotuines: nwo drop the comptuation, ie, your call is pointless. For questions, please cotnact the authro at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  if(!nrows || !ncols || !data1 || !data2) {
    fprintf(stderr, "!!\t Incosnstency in the caller: we expect rows, columns and data-mtatirces to be set. For questions please contact the develoeprs [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  if(!mask1 || !mask2) {
    if(mask1 || mask2) {
      fprintf(stderr, "!!\t Incosnstency in the caller: if the mask talbes are explictly used (ie, FLT_MAX is not used to speciffy mask-values) then we expect both tables/lists to be used/set (which is in contrast to your call, where only one of them were used). For questions please cotnact the develoeprs [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    }
  }
  const e_cmp_masksAre_used_t masksAre_used = config.masksAre_used;
  assert(data1); assert(data2);
  const bool needTo_useMask_evaluation = (masksAre_used != e_cmp_masksAre_used_undef) ? (masksAre_used == e_cmp_masksAre_used_true) :  matrixMakesUSeOf_mask(nrows, ncols, data1, data2, mask1, mask2, config.transposed, config.iterationIndex_2);


  //  printf("needTo_useMask_evaluation=%u, given (masksAre_used=%u, and e_cmp_masksAre_used_true=%u), at %s:%d\n", needTo_useMask_evaluation, masksAre_used, e_cmp_masksAre_used_true, __FILE__, __LINE__);
  // assert(false); // FIXME: remove.

#if(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_implicit)
  //! ---
#elif(TEMPLATE_iterationConfiguration_typeOf_mask == __macro__e_template_correlation_tile_maskType_mask_explicit)
  assert_possibleOverhead(mask1);
  assert_possibleOverhead(mask2);
#else //! then we valdiate that the correct 'sub-function' is caleld wrt. the mask-case
  assert(masksAre_used != e_cmp_masksAre_used_true); 
  assert(needTo_useMask_evaluation == false); 
#endif


  

  // FIXME[JC]: as a future wok try to describe "https://en.wikipedia.org/wiki/Strassen_algorithm" wrt. matrix-operations ... eg, wrt. "http://www.cquestions.com/2011/09/strassens-matrix-multiplication-program.html" ... "http://math.ewha.ac.kr/~jylee/SciComp/sc-crandall/strassen.c" ...  "https://github.com/sangeeths/stanford-algos-1/blob/master/strassen-recursive-matrix-multiplication.c" <-- first ask Jan-Christain to evaluate/'suggest' the perofmrance-benefit.

  //! Indefy the 'boundaries' of our operation:
  //  const char *__restrict__ rmask1 = NULL; const char *__restrict__ rmask2 = NULL; 
  //t_float *__restrict__ rres;  
  //const t_float *__restrict__ rmul1; const t_float *__restrict__  rmul2; 
  assert(config.CLS > 0); 
  //printf("CLS=%u, at %s:%d\n", CLS, __FILE__, __LINE__);
  const uint SM = (uint)(config.CLS / (uint)sizeof (t_float)); // const t_float *__restrict__ rweight = weight;
  assert(nrows > 0); assert(ncols > 0); assert(SM > 0);

  const t_float tweight = (weight && (needTo_useMask_evaluation == false) ) ? get_sum_SIMD_vetor1d__float(nrows, weight) : 1;
  const uint iterationIndex_2 = (config.iterationIndex_2 == UINT_MAX) ? nrows : config.iterationIndex_2;
  //" 
  //! Iterate:
  // FIXME: try to improve [”elow] parallisation
  uint index1 = 0;

  t_float **listOf_inlineResults = NULL;
  //printf("config.sparseDataResult=%p, at %s:%d\n", config.sparseDataResult, __FILE__, __LINE__);
  if( (config.typeOf_postProcessing_afterCorrelation != e_allAgainstAll_SIMD_inlinePostProcess_undef) || (config.sparseDataResult != NULL) ) {
    assert(resultMatrix == NULL); //! ie, as we then expect that the resutl-matrix is not of interest.
    //! Allocate:
    const t_float empty_0 = 0;
    listOf_inlineResults = allocate_2d_list_float(SM, SM, empty_0);
    const uint size_SM_total = SM * SM;
    // fprintf(stderr, "allocates |list|=%u, and access poitner=listOf_inlineResults=%p, at %s:%d\n", SM, listOf_inlineResults, __FILE__, __LINE__);
    assert(SM > 0);     assert(SM != UINT_MAX);
    for(uint i = 0; i < SM; i++) { 
      //fprintf(stderr, "[%u/%u], at %s:%d\n", i, SM, __FILE__, __LINE__);
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(listOf_inlineResults[i]); 
      /* for(uint k = 0; k < SM; k++) { assert(k < SM);  */
      /* 	fprintf(stderr, "\t[%u/%u][%u/%u], at %s:%d\n", i, SM, k, SM, __FILE__, __LINE__); */
      /* 	assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(listOf_inlineResults[i][k]);} */
    }
    //for(uint i = 0; i < size_SM_total; i++) {assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(listOf_inlineResults[0][i]);}
    //! start: init: ------------------------------------------------------------------------------------------
#if( (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 0) && (TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 0) )
    KT__memset_2d_list(listOf_inlineResults, SM, SM, T_FLOAT_MIN);
    //    const uint size = SM * SM; for(uint i = 0; i < size; i++) {listOf_inlineResults[0][i] = T_FLOAT_MIN;} //! ie, then intiate the values to 'nearly 0', though with difference tha tthe latter is used as a 'mark' to state that 'a value has Not been set';
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
    KT__memset_2d_list(listOf_inlineResults, SM, SM, T_FLOAT_MAX);
    //const uint size = SM * SM; for(uint i = 0; i < size; i++) {listOf_inlineResults[0][i] = T_FLOAT_MAX;}
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1)		
    KT__memset_2d_list(listOf_inlineResults, SM, SM, T_FLOAT_MIN_ABS);
    //    const uint size = SM * SM; for(uint i = 0; i < size; i++) {listOf_inlineResults[0][i] = T_FLOAT_MIN_ABS;}
#else //! tehnw e are itnerested in the max-value
#warning "Investigate why this case was 'entered'"
#endif //! otehrwise we assuemt that eh compelx object is udpated, a 'compelx object' which we anyhow choose to udpate (ie, aslos tof rthe 'only-compare-tow-rows' case).
    //! finish: init: ------------------------------------------------------------------------------------------
  } else {
    assert(config.typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_undef);
    //    if(config.sparseDataResult == NULL) {
      assert(resultMatrix != NULL); //! ie, as the call would otheriwse be pointless.
      assert(listOf_inlineResults == NULL); //! ie, a 'fall-back-valdiation' wrt. any fugrue code-changes.
      //}
  }
  assert(SM > 0); //! ie, what we expect.


  enum e_template_correlation_tile_typeOf_distanceUpdate typeOf_distanceFormula = (e_template_correlation_tile_typeOf_distanceUpdate_t)TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate ;
  //  enum e_template_correlation_tile_typeOf_distanceUpdate typeOf_distanceFormula = e_template_correlation_tile_typeOf_distanceUpdate_scalar;
  const bool isTo_updateCountVariable = TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices;

  //! Idneitfy the min-max varialbes:
#include "funcRef_aux__set_minMax_variables.c" //! which 'set' "isTo_findMaxValue" and "isTo_findMinValue"



  const s_allAgainstAll_config_t config_tile = config; //.obj_rowConfiguration;

  //! Idneityf the local object and 
  //! Provide erorr-harndling rotuine.
#if(__config__funcTemplateAllAgainstAll__storeIntermeidateResults == 1)
  s_kt_computeTile_subResults_t *obj_resultsFromTiling = obj_config.obj_resultsFromTiling;
  assert(isEmpty__s_kt_computeTile_subResults(obj_resultsFromTiling)); //! ie, as we expec t'this' to have been itnaited to empty.
  if(obj_config.obj_resultsFromTiling == NULL) { //! then a user mgith have caleld the wrong function.
    fprintf(stderr, "!!\t We expected the \"obj_resultsFromTiling\" to have been set: your inptu-aprams did not correcposn to the expected configuraiotns, i,e plase udpate your calls: for quesitons please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  //!
  //! Allcoate memory:
  init__s_kt_computeTile_subResults(obj_resultsFromTiling, nrows, nrows, typeOf_distanceFormula, isTo_updateCountVariable, isTo_findMaxValue, isTo_findMinValue);
#else //! then we expect the user is 'only' interested int he comptued results, ie, not teh 'temorary set of vlaues':
  s_kt_computeTile_subResults_t obj_resultsFromTiling_local; 
  s_kt_computeTile_subResults_t *obj_resultsFromTiling = &obj_resultsFromTiling_local; 
  if(obj_config.obj_resultsFromTiling != NULL) { //! then a user mgith have caleld the wrong function.
    fprintf(stderr, "!!\t We expected the \"obj_resultsFromTiling\" to Not have been set: your inptu-aprams did not correcposn to the expected configuraiotns, i,e plase udpate your calls: for quesitons please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  //!
  //! Allcoate memory:
  init__s_kt_computeTile_subResults(obj_resultsFromTiling, SM, SM, typeOf_distanceFormula, isTo_updateCountVariable, isTo_findMaxValue, isTo_findMinValue);
#endif
  // assert(false); // FIXME: write a Perl-tiantiobn-script to cosntruict an if-stences to dientify teh correct "typeOf_distanceFormula" to use in [”elow] ... an


  //setTo_empty__s_kt_computeTile_subResults(&obj_resultsFromTiling);
  

  
  // FIXME: add macro-support for a parallel OpenMP scheduling ... cosntruct a 'sceudlign-table' for each row-tile (to avoid 'lab' wrt. the symemtry of the matrices) .. and allocate the "obj_resultsFromTiling" seperately for each SSE-thread ... and thereafter merge 'the differnet parallel sets' (when the parallsiation 'compåletes'.

  //#pragma omp parallel for schedule(guided)
  //#pragma omp parallel for schedule(guided,4)
  //#pragma omp parallel for schedule(dynamic,4)
  //for(uint cnt_index1 = 0; cnt_index1 < numberOf_chunks_rows; cnt_index1++) {

  /* assert(numberOf_chunks_rows >= 1); */
  /* assert(numberOf_chunks_cols >= 1); */


  

  // printf("(SM(|%u|)-tiling)\tbucketsFirst(#(%u))=last(|%u|), bucketsSecond(#(%u))=last(|%u|), nrows=%u, ncols=%u, at %s:%d\n", SM, numberOf_chunks_rows, chunkSize_row_last, numberOf_chunks_rows_2, chunkSize_row_last_2, nrows, ncols, __FILE__, __LINE__);

  uint cnt_cells_evaluated = 0;

  // printf("\n\n## nrows=%u, iterationIndex_2=%u, at %s:%d\n", nrows, iterationIndex_2, __FILE__, __LINE__);

  s_aux_findPartitionFor_tiles_t tile_r1 = initAndGet__s_aux_findPartitionFor_tiles_t(nrows, SM);
  s_aux_findPartitionFor_tiles_t tile_r2 = initAndGet__s_aux_findPartitionFor_tiles_t(iterationIndex_2, SM);
  s_aux_findPartitionFor_tiles_t tile_col = initAndGet__s_aux_findPartitionFor_tiles_t(ncols, SM);
  //! ---------------------
  //!
  //printf("config.sparseDataResult=%p, at %s:%d\n", config_allAgainstAll->sparseDataResult, __FILE__, __LINE__);
  const bool globalProp__dataSetIsSymmeric =  (data1 == data2) && (iterationIndex_2 == nrows)
    //! Note: the [”elow] proeprty is 'added' iot. correctly 'handle' the case where we use a rank-filter on node-subsets (oekseth, 06. aug. 2017).
    && (MF__isToUse_rankThresholds__s_kt_list_2d_kvPair_filterConfig_t((config_allAgainstAll->sparseDataResult)) == false);
  //!
  //! Include a pre-step to simplify OpenMP-applicaiton: 
  uint cnt_blocks_local = (tile_r1.cntBlocks * tile_r2.cntBlocks);
  s_kt_list_1d_pair_t map_index     = init__s_kt_list_1d_pair_t(cnt_blocks_local);
  s_kt_list_1d_pair_t map_index_cnt = init__s_kt_list_1d_pair_t(cnt_blocks_local);
  loint map_index_cntAdded = 0;
  //! 
  //! Starts iteration through the tiles:
  for(uint cnt_index1 = 0; cnt_index1 < tile_r1.cntBlocks; cnt_index1++, index1 += SM) {
    const uint chunkSize_index1 = get_blockSize__s_aux_findPartitionFor_tiles_t(&tile_r1, cnt_index1);
    //! Note: complexity in [”elow] is itnrodcued in order to 'only' iterate throug half of the items in a [][] matrix:
    uint numberOf_chunks_rows_local = 0; uint chunkSize_row_last_local = 0;  
    s_aux_findPartitionFor_tiles_t tile_r2_symmetric = tile_r2;
    if(globalProp__dataSetIsSymmeric) {
      // FIXME[JC]: are you able to 'make' [below] more 'symmeitrc' ... eg, to 'change the outer tile-size' wrt. ...??... (oekseth, 06. des. 2016).
      tile_r2_symmetric = initAndGet__s_aux_findPartitionFor_tiles_t((index1+chunkSize_index1), SM);
    } /* else { //! else the matrix is assumed not to be symmetric. */
    assert_possibleOverhead( (index1 + chunkSize_index1) <= nrows);
    for(uint cnt_index2 = 0, index2 = 0; cnt_index2 < tile_r2_symmetric.cntBlocks; cnt_index2++, index2 += SM) {
      assert((uint)map_index_cntAdded < map_index.list_size);
      //!
      //! Add:
      map_index_cnt.list[map_index_cntAdded].head = cnt_index1;       map_index_cnt.list[map_index_cntAdded].tail = cnt_index2;
      map_index.list[map_index_cntAdded].head = index1;       map_index.list[map_index_cntAdded].tail = index2;
      //! Increment:
      map_index_cntAdded++;
    }
  }
  {
    /* bool isTo_usePara = false; */
    /* if(obj_config.config_allAgainstAll->para_isTo_usePara_ifAboveThreshold && (map_index_cntAdded > obj_config.config_allAgainstAll->para_cntThreshold_SM_squares) ) {       */
    /*   isTo_usePara = true; */
    /* } */
    /* if(isTo_usePara)  */
#if(optmize_parallel_2dLoop_useFast == 1)
    //#if(usePara__inPairWiseSim_computation == 1)
      {
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#pragma omp parallel for schedule(static) //schedule(dynamic,3) //! where "3" is based on observaitons
#endif
	for(loint block_id = 0; block_id < map_index_cntAdded; block_id++) {
	  //! Get the proerpteis: 
	  const uint cnt_index1 = map_index_cnt.list[block_id].head;       const uint cnt_index2 = map_index_cnt.list[block_id].tail;
	  const uint index1     = map_index.list[block_id].head;           const uint index2     = map_index.list[block_id].tail;
	  const uint chunkSize_index1 = get_blockSize__s_aux_findPartitionFor_tiles_t(&tile_r1, cnt_index1);
	  //!
	  //! Include logics:
#include "func_template__allAgainstAll_eachBlock.c"
	}
      } 
#else 
      {
	for(loint block_id = 0; block_id < map_index_cntAdded; block_id++) {
	  //! Get the proerpteis:
	  const uint cnt_index1 = map_index_cnt.list[block_id].head;       const uint cnt_index2 = map_index_cnt.list[block_id].tail;
	  const uint index1     = map_index.list[block_id].head;           const uint index2     = map_index.list[block_id].tail;
	  const uint chunkSize_index1 = get_blockSize__s_aux_findPartitionFor_tiles_t(&tile_r1, cnt_index1);
	  //!
	  //! Include logics:
	  // printf("block_id[%u/%u], at %s:%d\n", (uint)block_id, (uint)map_index_cntAdded, __FILE__, __LINE__);
#include "func_template__allAgainstAll_eachBlock.c"
	}
      }
#endif
  }

  // printf("\t\t |blocks|=[%u, %u], at [%s]:%s:%d\n", tile_r1.cntBlocks, tile_r2.cntBlocks, __FUNCTION__, __FILE__, __LINE__);
  
  //! De-allocate locally reserved memory:
  if(listOf_inlineResults) {
    free_2d_list_float(&listOf_inlineResults, SM);
  }
  //#if(__config__funcTemplateAllAgainstAll__storeIntermeidateResults == 1)
  free__s_kt_computeTile_subResults(obj_resultsFromTiling);
  //#endif //! else we assume the user is itnerested in 'keeping the data for their own use'.
  //} 

#if(configureDebug_useExntensiveTestsIn__tiling == 1)
  { //! Validate that all celsl have been valuated (oekseth, 06. nov. 2016):
    assert(iterationIndex_2 != UINT_MAX);
    const uint cnt_expected = nrows * iterationIndex_2;
    const uint cnt_expected_sym = (nrows * iterationIndex_2) * 0.5;      
    // printf("## debug_cnt-evaluated=%u, cnt-expected=%u=(%u * %u), cnt-expected-sym=%u, at %s:%d\n", cnt_cells_evaluated, cnt_expected, nrows, iterationIndex_2, cnt_expected_sym, __FILE__, __LINE__);

    if(data1 == data2) {
      assert_possibleOverhead(cnt_cells_evaluated >= cnt_expected_sym);
    } else {
      assert_possibleOverhead(cnt_cells_evaluated == cnt_expected);
    }
  }
#endif
  //! Dea-llocte:
  free__s_kt_list_1d_pair_t(&map_index);
  free__s_kt_list_1d_pair_t(&map_index_cnt);
}



//! ********************''
#undef __config__funcTemplateAllAgainstAll__storeIntermeidateResults
#undef __config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes
#undef __config__funcTemplateAllAgainstAll__inlinePostProcesType
