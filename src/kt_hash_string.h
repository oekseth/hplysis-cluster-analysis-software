#ifndef kt_hash_string_h
#define kt_hash_string_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_hash_string
   @brief a simplicitc hash-funciotn-interface for strings (oekseth, 06. aug. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks used to simplify API and access in/through different programming-languates
**/

#include "kt_list_1d.h"
#include "kt_list_1d_string.h"
#include "kt_list_2d.h"

/**
   @struct s_kt_hash_string
   @brief provide an interface ofr accessing unique strings (oesketh, 06. aug. 2017).
   @reamrks wrt. our logica we (in this strucutre-class) integrate/add support for 'finding' the list to insert in/at (where the 2d-uint-list is used to 'handle' collecitons wrt overlapping hash-keys, ie, to insert using 'push(..)' and then apply/use a llienar-search when finding the string-representaion of the key, eg,w rt. uniquness-testng before 'pusing' the key to the local-insert-strucutre, wher ethe 'inner-key' reflects the users key-reference))
 **/
typedef struct s_kt_hash_string {
  s_kt_list_1d_string_t obj_strings; 
  uint obj_strings_cntInserted; //! hwich is icnremented for eeach unique string.
  s_kt_list_2d_uint_t map_collisions; 
} s_kt_hash_string_t;

//! @return an itnalised verison of the hash-object
static s_kt_hash_string_t init__s_kt_hash_string_t() {
  s_kt_hash_string_t self;
  self.obj_strings = setToEmpty_andReturn__s_kt_list_1d_string_t();
  self.obj_strings_cntInserted = 0;
  self.map_collisions = setToEmpty__s_kt_list_2d_uint_t();
  //! 
  //! @return
  return self;
}

/**
   @brief cosntruct a unqiue set/colleciton of strings (oekseth, 06. agu. 2017).
   @param <obj_words> is the collection of words to idneity unqiueness of, ie, to insert into the returned object.
   @param <word_seperator> seperates the words in each sentence: if '\0' is sued then we use the 'compelte stinrgs'.
   @return an itnalised verison of the hash-object
**/
s_kt_hash_string_t init__fromHash__s_kt_hash_string_t(s_kt_list_1d_string_t *obj_words, char word_seperator);

//! De-allcoate the "s_kt_hash_string_t" object.
static void free__s_kt_hash_string_t(s_kt_hash_string_t *self) {
  assert(self);
  free__s_kt_list_1d_string(&(self->obj_strings));
  free__s_kt_list_2d_uint_t(&(self->map_collisions));
  self->obj_strings_cntInserted = 0;
}


//! @return the index at where the string was inserted.
uint add_string__kt_hash_string(s_kt_hash_string_t *self, const char *str);
//! @return the key of the string: if Not fodun hten UINT_MAX is returned.
uint get_key__kt_hash_string(const s_kt_hash_string_t *self, const char *str);

//! Dump the key-name mappoigns to the file-pointer in question. 
void dump__s_kt_hash_string_t(const s_kt_hash_string_t *self, FILE *fPointer);
//! Dump the key-name mappoigns to the file-pointer in question. 
void dump__sortByBucket__s_kt_hash_string_t(const s_kt_hash_string_t *self, FILE *fPointer);

/**
   @struct s_kt_hash_string_2dSparse
   @brief provide a sparse mapping between strings 
 **/
typedef struct s_kt_hash_string_2dSparse {
  s_kt_hash_string_t obj_dense; //! ie, the keys used in look-ups, ie, [key] = [].
  s_kt_hash_string_t obj_sparse; //! ie, the keys used inside the data-stuct, ie, [] = [keys].
  s_kt_list_2d_uint_t obj_2d; //! ie, the 2d-sparse-data-strucutre we construct. 
  bool isTo_useDifferentMappingTables_forKeysIndexes; //! which fi set to false impleis taht we do Not use the "obj_sparse" struct.
} s_kt_hash_string_2dSparse_t;

//! Itniate the s_kt_hash_string_2dSparse_t objec tot empty.
static s_kt_hash_string_2dSparse_t setTo_empty__s_kt_hash_string_2dSparse_t() {
  s_kt_hash_string_2dSparse_t self;
  self.obj_dense  = init__s_kt_hash_string_t();
  self.obj_sparse = init__s_kt_hash_string_t();
  self.obj_2d = setToEmpty__s_kt_list_2d_uint_t();
  self.isTo_useDifferentMappingTables_forKeysIndexes = false;
  //!
  //! @return 
  return self;
}

//! De-allcoat ethe "s_kt_hash_string_2dSparse_t" object (oekseth, 06. aug. 2017).
static void free__s_kt_hash_string_2dSparse_t(s_kt_hash_string_2dSparse_t *self) {
  assert(self);
  free__s_kt_hash_string_t(&(self->obj_dense));
  free__s_kt_hash_string_t(&(self->obj_sparse));
  free__s_kt_list_2d_uint_t(&(self->obj_2d));
}
/**
   @brief cosntruct a unqiue set/colleciton of strings (oekseth, 06. agu. 2017).
   @param <obj_words> is the collection of words to idneity unqiueness of, ie, to insert into the returned object.
   @param <word_seperator_keyValues> seperates the words in each sentence: if '\0' is sued then we use the 'compelte stinrgs'.
   @param <word_seperator_valueValues> seperates the words in each 'value-block': if '\0' is sued then we use the 'compelte stinrgs'.
   @param <isTo_useDifferentMappingTables_forKeysIndexes> which if set to false impleis taht we do Not use the "obj_sparse" struct.
   @return an itnalised verison of the hash-object
   @remakrs provide lgocis for use-case susch as:
   -- use-case(1): steps: (1) read file into a string-array; (2) split words/strings using the user-porivded feild-delimoror (eg, '\t'); (3) insert into our "s_kt_list_2d_uint_t" [key1][key2] pairs; return botht he new-cosntructed hash-object and the 2d-sparse-list.
**/
s_kt_hash_string_2dSparse_t init__fromHash__s_kt_hash_string_2dSparse_t(s_kt_list_1d_string_t *obj_words, char word_seperator_keyValues, char word_seperator_valueValues, const bool isTo_useDifferentMappingTables_forKeysIndexes);

//! Constuct an "s_kt_list_1d_string_t" object for an "[head]=sparse(keys)" list. 
//! @reutrn the new-cosntructed object.
static s_kt_list_1d_string_t constructObj__listOf_stringsForHead__s_kt_hash_string_2dSparse_t(const s_kt_hash_string_2dSparse_t *self, const uint head_id) {
  assert(head_id != UINT_MAX);
  s_kt_list_1d_string_t obj_ret = setToEmpty_andReturn__s_kt_list_1d_string_t();
  if(head_id >= self->obj_2d.current_pos) {return obj_ret;} //! ie, as no relationshisp are set ofr the vertex.
  //!
  //! Construct the string-list:
  const s_kt_list_1d_uint_t *arrOf_children = &(self->obj_2d.list[head_id]);
  if(arrOf_children->current_pos > 0) { //! then add children:
    assert(arrOf_children->list);
    for(uint i = 0; i < arrOf_children->current_pos; i++) {
      const uint tail_id = arrOf_children->list[i];
      assert(tail_id != UINT_MAX);
      //!
      //! Get the string:
      const char *str_tail = NULL;
      if(self->isTo_useDifferentMappingTables_forKeysIndexes == true) {
	str_tail = self->obj_sparse.obj_strings.nameOf_rows[tail_id];
      } else { //! then we use the same 'mappign-table' as for the ehad-id.
	str_tail = self->obj_dense.obj_strings.nameOf_rows[tail_id];
      }
      assert(str_tail); assert(strlen(str_tail));
      //!
      //! Add the string:
      set_stringConst__s_kt_list_1d_string(&obj_ret, /*index=*/i, str_tail);
    }
  }

  //!
  //! @return
  return obj_ret;
}

/* //! Exports the object to a tsv-file */
/* //! @return true upon scucess. */
/* bool export_tsv__s_kt_hash_string_2dSparse_t(const s_kt_hash_string_2dSparse_t *self, const char *result_file); */

#endif //! EOF
