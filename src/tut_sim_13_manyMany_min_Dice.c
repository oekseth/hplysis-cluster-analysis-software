 #ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_distance.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"

/**
   @brief demonstrates how a similartiy-metirc may be used to describe the simlairty between two matrices.
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks in this example we use teh 'second matrix' as the 'base-comparison-strategy', and where the row-size of the matrices differs.
   @remarks many-to-many: min-index: extract the min-index assicated to each comptued simliarty-matrix-row; a simple permtaution of "tut_sim_5_oneToMany_Gower.c" where we comptue for "row(...) x row(...)" wrt. the two input-matrices.
   @remarks general: use logics in our "hp_distance" to comptue the simalrit between two vectors.
   @remarks an example-applicaiton is seen in (one of) our implementaiotns of the "kmeans++" algorithm ("____init_centers_pySciKitLearn__kmeanspp__s_kt_matrix_base_t(..)"). 
   @remarks simliartiy: comptue the sim-matrix="Dice" between two matrices using our "hp_distance.h" API 
   @remarks "hp_distance": we call the "apply__hp_distance(..)" function;
   @remarks generic: we use our "initSampleOfValues__rand__s_kt_matrix_base_t(..)" (defined in our "kt_matrix_base.h") to simplify the writign of 'statnized' code-examples: simplifeis the 'constuciton' of two uqniue vectors to compare.
   @remarks generalizaiton(example): to simplify the understanidng and re-use of our appraoch we have strucutred our "tut_sim_*" examples to use the same 'example-seutp', ie, where differneces is found wrt. the API-calls in to our "hp_distance", and wrt. different "s_kt_correlationMetric" calls.
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const uint nrows = 2; const uint ncols = 100; 
  const uint nrows_2 = 6; 
#endif
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
  //! Specify/define the simlairty-metric:
  s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_innerProduct_Dice, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
  //s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/ /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"

  //! Allocate two vectors, ie, matrices with demsinos [1, ncols]:
  //! Note: for simplicty we use [”elow] fucntion, defined in our "kt_matrix_base.h", ie, to set the vectors to default valeus, therby reducing the nubmer of code-lines in this use-case-example; in the latter randomenss is used, ie, for which we expect the vectors to be different.
  assert(nrows > 2);   assert(ncols > 0); //! ie, as we otherise have a poinbtelss' call
  s_kt_matrix_base vec_1 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
  s_kt_matrix_base vec_2 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows_2, ncols);
  s_kt_matrix_base vec_result = initAndReturn__empty__s_kt_matrix_base_t();//! ie, set to 'empty'.

  //!
  //! Apply logics:
  s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
  hp_config.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_min__index; //! ie, the 'itneresting' part of this use-case-example
  const bool is_ok = apply__hp_distance(obj_metric, &vec_1, &vec_2,  &vec_result, /*config=*/hp_config);
  assert(is_ok); //! ie, as we epxec the operaiton to have been 'a success'.
  //!
  //! 'Fetch' out the result:
  assert(vec_result.nrows == 1);   assert(vec_result.ncols == nrows);
  //for(uint row_id = 0; row_id < nrows; row_id++) {
  { const uint row_id = 0;
    for(uint col_id = 0; col_id < nrows; col_id++) {
      //!
      //! Write out the result:
      fprintf(stdout, "# Result: \t\t sim-score[%u][%u]=\"%f\", at [%s]:%s:%d\n", row_id, col_id, vec_result.matrix[row_id][col_id], __FUNCTION__, __FILE__, __LINE__);
      //fprintf(stdout, "# Result: \t\t sim-score[%u][%u]=\"%f\", at [%s]:%s:%d\n", vec_result.matrix[row_id][col_id], __FUNCTION__, __FILE__, __LINE__);
    }
  }

  //!
  //! De-allocate:
  free__s_kt_matrix_base_t(&vec_1);
  free__s_kt_matrix_base_t(&vec_2);
  free__s_kt_matrix_base_t(&vec_result);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
 

 
