#ifndef correlation_api_rowCompare_h
#define correlation_api_rowCompare_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file correlation_api_rowCompare
   @brief a wrapper to simplify inclsuion of different correlation-sub-libraries (oesketh, 06. sept. 2016).
**/


#include "mask_api.h"
#include "correlation_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"
#include "correlation_rank_rowPair.h" //! which defines functions used for our "s_correlation_rank_rowPair"
#include "s_allAgainstAll_config.h"
#include "correlationType_spearman.h"
#include "correlationType_kendall.h"
#include "correlationType_kendall_partialPreCompute_kendall.h" //! an object-structure to partially pre-comptue Kendsalls correlation-measure for an input-matrix.
#include "correlationType_delta.h" //! eg, for "spearman", "cityblock" and "Pearson's correlation-metrics"
#include "correlationType_proximity.h"

//! @brief a generalized version of our kt_spearman(..) funciton.
static t_float spearman(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose) {
  s_allAgainstAll_config_t config = get_init_struct_s_allAgainstAll_config();  //! ie, default intaiton.
  //! The call:
  return kt_spearman(n, data1, data2, mask1, mask2, weight, index1, index2, transpose, config);
}

/**
   @brief compute the Kendall distance between two rows or columns. The Kendall distance is defined as one minus Kendall's tau.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
 **/
static t_float kendall(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose) {
  s_allAgainstAll_config_t config; setTo_empty__s_allAgainstAll_config(&config); //! ie, default intaiton.
  //! The call:
  return kt_kendall(ncols, data1, data2, mask2, mask2, weight, index1, index2, transpose, e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine, config);
}


/**
   @brief compute the euclidian (or: geometric distance) between two rows or columns.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
 **/
static t_float euclid(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose) {
  s_allAgainstAll_config_t config; setTo_empty__s_allAgainstAll_config(&config); //! ie, default intaiton.
  //! The call:
#if(functionConfiguration__isToAssume__allDistanceMetricsUseMacroDefinedFunctions == 1) 
    return __kt_euclid_slow(ncols, data1, data2, mask2, mask2, weight, index1, index2, transpose);    
#else
    return kt_euclid(ncols, data1, data2, mask2, mask2, weight, index1, index2, transpose, config);
#endif
}

/**
   @brief compute the Cityblock (or: Manhatten distance) between two rows or columns.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
 **/
static t_float cityblock(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose) {
  s_allAgainstAll_config_t config; setTo_empty__s_allAgainstAll_config(&config); //! ie, default intaiton.
  //! The call:
#if(functionConfiguration__isToAssume__allDistanceMetricsUseMacroDefinedFunctions == 1) 
  return __kt_cityblock_slow(ncols, data1, data2, mask2, mask2, weight, index1, index2, transpose);
#else
  return kt_cityblock(ncols, data1, data2, mask2, mask2, weight, index1, index2, transpose, config);
#endif
}




#endif //! EOF
