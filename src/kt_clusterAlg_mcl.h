#ifndef kt_clusterAlg_mcl_h
#define kt_clusterAlg_mcl_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_clusterAlg_mcl
   @brief The function-api to our high-performance clustering-methods for the MCL cluster-algoruhtm (oekseth, 06. setp. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
   -- correctness: our algorithm is based on the soruce-code-impemtatnions seen in the MCL software-lbiriary, ie, if latter is wrong then our code is worng. 
**/


#include "kt_clusterAlg_dbScan.h"

/**
   @struct s_kt_clusterAlg_mcl
   @brief configures the MCL cluster-algorithm-impmentaiotn (oesketh, 06. jul. 2017).
 **/
typedef struct s_kt_clusterAlg_mcl {
  t_float conf_pow; //! which represent the "-I" paramter in MCL.
  t_float conf_minError; //! ie, the cahos-paramter.
  t_float conf_cellThreshold; 
  t_float conf_postEval_pruning_node; //! ie,   
  t_float conf_postEval_pruning_cell; //! ie,   
  s_kt_correlationMetric_t obj_metric; //! which is the metric used during the "matrix_Square(..)" oepraiton in each while-loop-call.
  s_hp_distanceCluster_t objMetric__postMerge;
  bool nodeThresh__useDirectSCoreInteadOfCount; 
  uint convergence_minClusters; //! which if set to a $value > 1$ is used during travesal to investite for overfitting wrt. matrix-comptuations.
  //t_float conf_; //! ie,   
  loint convergence_minClusters__cntIteraitonsBetweenEach; //! ie, to reduce the time-cost of operations.
} s_kt_clusterAlg_mcl_t;

//! @return an initialized version of the "s_kt_clusterAlg_mcl_t" object (oekseth, 06. jul. 2017)
static s_kt_clusterAlg_mcl_t setToEmpty__s_kt_clusterAlg_mcl_t() {
  s_kt_clusterAlg_mcl_t self;
  //! ---------------------------------------------------------
  self.conf_minError = 0.001;
  self.conf_pow = 2;
  self.conf_cellThreshold = 0.00001;
  self.conf_postEval_pruning_node = 0.1;
  self.conf_postEval_pruning_cell = 0.1;
  self.obj_metric = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_innerProduct_innerProduct, e_kt_categoryOf_correaltionPreStep_none);
  self.objMetric__postMerge = init__s_hp_distanceCluster_t(); //! which is used during the ideitnifciaotn of distjoitn-forests in the post-step.
  self.nodeThresh__useDirectSCoreInteadOfCount = true; //! where the latter is a 'unfie apramter' wrt. the MCL-alogrithm, eg, in contrsat to DB-SCAN; 
  self.convergence_minClusters = 2; //! ie, a minum of two clsuters in the set.
  self.convergence_minClusters__cntIteraitonsBetweenEach = 1; //! ie, to reduce the time-cost of operations.
  //! ---------------------------------------------------------
  //! 
  //! @return the object: 
  return self;
}

/**
   @brief comptue the MCL algorithm (oesketh, 06. jul. 2017).
   @param <self> is the cofniguraiton-object
   @param <obj_1> is the input-matirx
   @param <obj_result> is the result-set
   @param <clusterConfig> describes how enties are to be unfied after the MCL-iteriaotn-stpe, eg, simalri to the DB-SCAN algorithm
   @return true if the inptus are valid.
 **/
bool compute__kt_clusterAlg_mcl(s_kt_clusterAlg_mcl_t *self, const s_kt_matrix_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, s_kt_api_config_t clusterConfig);

#endif
