
void PLS::std(Matrix<float> *M, Vector<float> *mean, Vector<float> *retvector) {
  float *ptM, *ptret, *ptmean;
  int i, j;

  retvector->ResetVector();
  ptret = retvector->GetData();
  ptmean = mean->GetData();

  for (j = 0; j < nrows; j++) {
    ptM = M->GetColumn(j);
    for (i = 0; i < ncols; i++) {
      //! Compute: result[i] += (M[i][j] - row_mean[i])^2
      ptret[j] += (ptM[i] - ptmean[j]) * (ptM[i] - ptmean[j]);
    }
  }

  for (i = 0; i < nrows; i++) {
    //! Compute: result[i] = sqrt(result[i] / (ncols - 1))
    ptret[i] = ptret[i] / (((float)ncols)- (float)1.0);
    ptret[i] = sqrt(ptret[i]);
    if (ptret[i] < EPSILON) {
      ptret[i] = 1;
    }
  }
}



void PLS::std(Vector<float> *M, Vector<float> *mean, Vector<float> *retvector) {
  float *ptM, *ptret, *ptmean;
  int i;

  retvector->ResetVector();
  ptret = retvector->GetData();
  ptmean = mean->GetData();

  ptM = M->GetData();
  for (i = 0; i < M->GetNElements; i++) {
    //! Compute: result_scalar += (row[i] - mean)^2
    ptret[0] += (ptM[i] - ptmean[0]) * (ptM[i] - ptmean[0]);
  }
	
  ptret[0] /= (((float)M->GetNElements)- (float)1.0);
  ptret[0] = sqrt(ptret[0]);

  if (ptret[0] < EPSILON) {
    ptret[0] = 1;
  }
}





void PLS::zscore(Matrix<float> *M, Vector<float> *mean, Vector<float> *std) {
  int i,j;
  float *ptM, *ptmean, *ptstd;

  ptmean = mean->GetData();
  ptstd = std->GetData();
  for(j = 0; j < nrows; j++){	
    ptM = M->GetColumn(j);
    for (i = 0; i < ncols; i++) {
      //! Compute: matrix[i][j] = (matrix[i][j] - mean[i]) * ptstd_inv[j];
      ptM[i] = (ptM[i] - ptmean[j]) / ptstd[j];
    }
  }
}
