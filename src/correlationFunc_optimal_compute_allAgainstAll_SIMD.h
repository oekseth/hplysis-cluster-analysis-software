#ifndef correlationFunc_optimal_compute_allAgainstAll_SIMD_h
#define correlationFunc_optimal_compute_allAgainstAll_SIMD_h


//! Copmute the optmial distance-matrix:
// void correlationFunc_optimal_compute_allAgainstAll_SIMD(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, const s_allAgainstAll_config_t config); 
//void correlationFunc_optimal_compute_allAgainstAll_SIMD(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const bool transpose, const uint CLS, const bool isTo_use_continousSTripsOf_memory, uint iterationIndex_2 = UINT_MAX, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef, void *s_inlinePostProcess = NULL, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation = e_typeOf_metric_correlation_pearson_undef, const s_kt_correlationConfig self, s_kt_computeTile_subResults_t *complexResult); 
//const e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_undef);

#endif //! EOF
