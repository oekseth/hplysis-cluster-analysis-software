#include "correlationType_kendall.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */




/* *********************************************************************  */
/* *********************************************************************  */

/* //! @return true if we asusem teh t_float-value is of interest. */
/* static t_float isOf_interest_func(const t_float value) { */
/*   return ( (value != T_FLOAT_MAX) && (value != T_FLOAT_MIN_ABS) ); */
/* } */




static t_float __kt_kendall_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose) {

  uint con = 0;
  uint dis = 0;
  uint exx = 0;
  uint exy = 0;
  uint flag = 0;
  /* flag will remain zero ifno nonzero combinations of mask1 and mask2 are
   * found.
   */
  t_float denomx;
  t_float denomy;
  t_float tau;


  // FIXME: before starting on otimizign this funciton ... validate that the sort-function has a high time-compelxtiy, ie, that 'there is a eprformance-reaosn to try to avoid the "log(n)" step in Knitghts algorithm.

  /** 
      Idea: identify disagreement between to lists:
      ----------------------------
      [*1*][i] <----> [*2*][i]
      ^                ^  
      |                |
      |                |
      [*1*][j] <----> [*2*][j]	
      ----------------------------	  
   **/

  // FIXME: comapre teh tiem-dfifenrece of 'ibncluding this code into a funciton' VS 'calling this fucntion for every time'.

  // FIXME: wrt. [below] ... write an atlernative funciton which uses _mm_load_ps(..) ... and then comapre the exeuciton-time .... and support the case where all valeus are of itnerest ... and compare the perofmrance-difference

  if(transpose == 0) {
    //! Note[memory-access-patterns]:

    // printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", n, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).

    if(mask1) {assert(mask1[index1]);} //! ie, as we then expect thsi to be set.
    if(mask2) {assert(mask2[index2]);} //! ie, as we then expect thsi to be set.

    for(uint i = 0; i < n; i++) {
      const t_float x1 = data1[index1][i]; const t_float y1 = data2[index2][i];

      if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
	for(uint j = 0; j < i; j++) {
	  if( (!mask1 || mask1[index1][j]) && (!mask2 || mask2[index2][j]) ) {
	    //if(mask1[index1][j] && mask2[index2][j]) {

	    // FIXME: describe how this apporahc may be re-written into memory-effeicnt code. <-- try to use ofe intermeidate/extra/temorary-allcoated result-table, where we use enums to set the values of '<', '>', '==', and '!='. Thereafter we we iterate through the two matrices to update/infer "con", "dis", "exx" and "exy" (seperately for each row. <-- is the latter correct for 'transpose', ie, if we update for ...??...
	    // FIXME: update [ªbove] and for "transpose == 1" ... ie, before implemnting an optmized version (of thsi funciotn).
	    const t_float x2 = data1[index1][j]; const t_float y2 = data2[index2][j];
	    if(isOf_interest(x1) && isOf_interest(x2) && isOf_interest(y1) && isOf_interest(y2) ) {

	      // FIXME: comapre the time-difference between [below] approaches ... ie, the signvace-time-cost of icnreased instruction-memory-cache.
	      if(true) { 
		//! Note: in [”elow] we idenify the number of pairs which need a 'shift'/'swap' to 'fit'/'update' .... ie, for each 'inner-pair.x' we count the number of values outer-pair.y which are either less than, larger than or equal to inner-pair-y. If we both know the the 'sorted range' of outer-pair.y, eg, outer-pair.y.sorted_index (and where the sorted_index has been post-processed to discard duplicates/overlaps), then a comparison of (x[i].value <? x[i+1].value) and ||y[i].sorted_index, y[i+1].sorted_index|| would provide/yeild  ....??...  <-- this would only be correct for the ouer-y-values which has an x-value 'bound by the latter criteria' .... ie, we need to idnetify 'range-counts' seperately for .... 
		if(x1 < x2) { //! which in a sorted list implies to count the matches of y[x[i|sorted].index] VS y[x[i|sorted].index] <-- ie, 
		  if(y1 < y2) {con++;}
		  else if(y1 == y2) {exy++;}
		  else /*if y1 > y2*/ {dis++;} //! ie, a swap.
		} else if(x1 == x2) {
		  if(y1 != y2) {exx++;}
		} else { //! ie, x1 > x2
		  if(y1 < y2) {dis++;} //! ie, a swap.
		  else if(y1 == y2) {exy++;}
		  else /*if y1 > y2*/ {con++;} 
		}
	      } else {
		if(x1 < x2 && y1 < y2) con++;   /* OR */  
		if(x1 < x2 && y1 > y2) dis++;
		//! ---
		if(x1 > x2 && y1 < y2) dis++;   /* OR */  
		if(x1 > x2 && y1 > y2) con++; 
		//! ---- 
		if(x1 == x2 && y1 != y2) exx++; /* OR */  
		if(x1 != x2 && y1 == y2) exy++;
	      }
	      flag = 1;
	    }
          }
        }
      }
    }
  } else {
    
    // FIXME: if we use sorting, then ....

    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) {
	for(uint j = 0; j < i; j++) {
	  if( (!mask1 || mask1[j][index1]) && (!mask2 || mask2[j][index2]) ) {
	    //! Note: to improve this funciotn (wrt. exueciton-time) we might compute the 'relationships' seperately for (x1, x2)  and (y1, y2). If we divide this 'loop' into three different for-loops, we in the loop(1.a) compute for result[i][j]
	    const t_float x1 = data1[i][index1];  const t_float x2 = data1[j][index1];
	    const t_float y1 = data2[i][index2];  const t_float y2 = data2[j][index2];  
            
	    if(isOf_interest(x1) && isOf_interest(x2) && isOf_interest(y1) && isOf_interest(y2) ) {

	      if(x1 < x2 && y1 < y2) con++;    //! which in a sorted lsit implies: (x1 < x2) && (
	      // FIXME: investigate the effect of using "else if" instead of "if" in [below].
	      if(x1 > x2 && y1 > y2) con++;    //! which in a sorted lsit implies: .... 
	      if(x1 < x2 && y1 > y2) dis++;    //! which in a sorted lsit implies: .... 
	      if(x1 > x2 && y1 < y2) dis++;    //! which in a sorted lsit implies: .... 
	      if(x1 == x2 && y1 != y2) exx++;  //! which in a sorted lsit implies: .... 
	      if(x1 != x2 && y1 == y2) exy++;  //! which in a sorted lsit implies: .... 
	      flag = 1;
	    }
          }
        }
      }
    }
  }
  //! Note: "The Kendall tau rank distance is a metric that counts the number of pairwise disagreements between two ranking lists. The larger the distance, the more dissimilar the two lists are. Kendall tau distance is also called bubble-sort distance since it is equivalent to the number of swaps that the bubble sort algorithm would make to place one list in the same order as the other list. The Kendall tau distance was created by Maurice Kendall" ["https://en.wikipedia.org/wiki/Kendall_tau_distance"].
  if(!flag) return 0.;
  denomx = con + dis + exx;
  denomy = con + dis + exy;
  if(denomx==0) return 1;
  if(denomy==0) return 1;
  tau = (con-dis)/sqrt(denomx*denomy);
  return 1.-tau;
}

//! A naive implemtnaiton which is a copy-pate of the "cluster.c" blirary
//! @remarks we do not inlcud the GNU-lizence of the cluster.c library as this funciton is Not to be used in the 'live softwre' accesialbe for the users (of thsi software),, ie, merely intended to be sued as a point-fo-reference wrt. performance' (oekseth, 06. otk. 2016).
t_float kendall__slow_naiveImplementaiton_improvedWrt_ifClause(int n, t_float** data1, t_float** data2, int** mask1, int** mask2,  const t_float weight[], int index1, int index2, int transpose, const bool isTo_useImprvoedIfClause) {
  uint con = 0;
  uint dis = 0;
  uint exx = 0;
  uint exy = 0;
  uint flag = 0;
  /* flag will remain zero ifno nonzero combinations of mask1 and mask2 are
   * found.
   */
  t_float denomx;
  t_float denomy;
  t_float tau;


  // FIXME: before starting on otimizign this funciton ... validate that the sort-function has a high time-compelxtiy, ie, that 'there is a eprformance-reaosn to try to avoid the "log(n)" step in Knitghts algorithm.

  /** 
      Idea: identify disagreement between to lists:
      ----------------------------
      [*1*][i] <----> [*2*][i]
      ^                ^  
      |                |
      |                |
      [*1*][j] <----> [*2*][j]	
      ----------------------------	  
   **/

  // FIXME: comapre teh tiem-dfifenrece of 'ibncluding this code into a funciton' VS 'calling this fucntion for every time'.

  // FIXME: wrt. [below] ... write an atlernative funciton which uses _mm_load_ps(..) ... and then comapre the exeuciton-time .... and support the case where all valeus are of itnerest ... and compare the perofmrance-difference

  if(transpose == 0) {
    if(isTo_useImprvoedIfClause) {
#define __localConfig_ifCaluseAlt_1 1
#include "correlationType_kendall__func__slow_ifClause_optmizaiton.c.c"
#undef __localConfig_ifCaluseAlt_1
    } else {
#define __localConfig_ifCaluseAlt_1 0
#include "correlationType_kendall__func__slow_ifClause_optmizaiton.c.c"
#undef __localConfig_ifCaluseAlt_1
    }
    //! Note[memory-access-patterns]:
    // printf("elements-to-evlauate=%u, at [%s]:%s:%d\n", n, __FUNCTION__, __FILE__, __LINE__); //! (eosekth, 06. otk. 2016).
  } else {
    
    // FIXME: if we use sorting, then ....

    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) {
	for(uint j = 0; j < i; j++) {
	  if( (!mask1 || mask1[j][index1]) && (!mask2 || mask2[j][index2]) ) {
	    //! Note: to improve this funciotn (wrt. exueciton-time) we might compute the 'relationships' seperately for (x1, x2)  and (y1, y2). If we divide this 'loop' into three different for-loops, we in the loop(1.a) compute for result[i][j]
	    const t_float x1 = data1[i][index1];  const t_float x2 = data1[j][index1];
	    const t_float y1 = data2[i][index2];  const t_float y2 = data2[j][index2];  
            
	    if(isOf_interest(x1) && isOf_interest(x2) && isOf_interest(y1) && isOf_interest(y2) ) {

	      if(x1 < x2 && y1 < y2) con++;    //! which in a sorted lsit implies: (x1 < x2) && (
	      // FIXME: investigate the effect of using "else if" instead of "if" in [below].
	      if(x1 > x2 && y1 > y2) con++;    //! which in a sorted lsit implies: .... 
	      if(x1 < x2 && y1 > y2) dis++;    //! which in a sorted lsit implies: .... 
	      if(x1 > x2 && y1 < y2) dis++;    //! which in a sorted lsit implies: .... 
	      if(x1 == x2 && y1 != y2) exx++;  //! which in a sorted lsit implies: .... 
	      if(x1 != x2 && y1 == y2) exy++;  //! which in a sorted lsit implies: .... 
	      flag = 1;
	    }
          }
        }
      }
    }
  }
  //! Note: "The Kendall tau rank distance is a metric that counts the number of pairwise disagreements between two ranking lists. The larger the distance, the more dissimilar the two lists are. Kendall tau distance is also called bubble-sort distance since it is equivalent to the number of swaps that the bubble sort algorithm would make to place one list in the same order as the other list. The Kendall tau distance was created by Maurice Kendall" ["https://en.wikipedia.org/wiki/Kendall_tau_distance"].
  if(!flag) return 0.;
  denomx = con + dis + exx;
  denomy = con + dis + exy;
  if(denomx==0) return 1;
  if(denomy==0) return 1;
  tau = (con-dis)/sqrt(denomx*denomy);
  return 1.-tau;


}

//! A naive implemtnaiton which is a copy-pate of the "cluster.c" blirary
//! @remarks we do not inlcud the GNU-lizence of the cluster.c library as this funciton is Not to be used in the 'live softwre' accesialbe for the users (of thsi software),, ie, merely intended to be sued as a point-fo-reference wrt. performance' (oekseth, 06. otk. 2016).
t_float kendall__slow_naiveImplementaiton(int n, t_float** data1, t_float** data2, int** mask1, int** mask2,  const t_float weight[], int index1, int index2, int transpose) {
  int con = 0;
  int dis = 0;
  int exx = 0;
  int exy = 0;
  int flag = 0;

  assert(mask1);
  assert(mask2);

  /* flag will remain zero if no nonzero combinations of mask1 and mask2 are
   * found.
   */
  t_float denomx;
  t_float denomy;
  t_float tau;
  int i, j;
  if (transpose==0)
  { for (i = 0; i < n; i++)
    { if (mask1[index1][i] && mask2[index2][i])
      { for (j = 0; j < i; j++)
        { if (mask1[index1][j] && mask2[index2][j])
          { t_float x1 = data1[index1][i];
            t_float x2 = data1[index1][j];
            t_float y1 = data2[index2][i];
            t_float y2 = data2[index2][j];
            if (x1 < x2 && y1 < y2) con++;
            if (x1 > x2 && y1 > y2) con++;
            if (x1 < x2 && y1 > y2) dis++;
            if (x1 > x2 && y1 < y2) dis++;
            if (x1 == x2 && y1 != y2) exx++;
            if (x1 != x2 && y1 == y2) exy++;
            flag = 1;
          }
        }
      }
    }
  }
  else
  { for (i = 0; i < n; i++)
    { if (mask1[i][index1] && mask2[i][index2])
      { for (j = 0; j < i; j++)
        { if (mask1[j][index1] && mask2[j][index2])
          { t_float x1 = data1[i][index1];
            t_float x2 = data1[j][index1];
            t_float y1 = data2[i][index2];
            t_float y2 = data2[j][index2];
            if (x1 < x2 && y1 < y2) con++;
            if (x1 > x2 && y1 > y2) con++;
            if (x1 < x2 && y1 > y2) dis++;
            if (x1 > x2 && y1 < y2) dis++;
            if (x1 == x2 && y1 != y2) exx++;
            if (x1 != x2 && y1 == y2) exy++;
            flag = 1;
          }
        }
      }
    }
  }
  if (!flag) return 0.;
  denomx = con + dis + exx;
  denomy = con + dis + exy;
  if (denomx==0) return 1;
  if (denomy==0) return 1;
  tau = (con-dis)/sqrt(denomx*denomy);
  return 1.-tau;
}



//! @return the number of combinations for assicated overlap:
static long long int get_numberOf_combinations_forOverlaps(const t_float* data, size_t arr_size) {  /* Assumes data is sorted.*/
  
// #if(configure_distanceMetricComputation_kendallFast_useSumInsteadOf_mul == 1)
//   float tieCount = 0; float cnt_overlap_pairs = 0;
// #else
  long long int tieCount = 0; long long int cnt_overlap_pairs = 0;
  //#endif

  
  for(size_t i = 1; i < arr_size; i++) {
    if(data[i] == data[i-1]) {
// #if(configure_distanceMetricComputation_kendallFast_useSumInsteadOf_mul == 1)
//       tieCount += 0.25; //! ie, as "(tieCount * (tieCount + 1)) / 2" ~~ tieCount += 1/4
// #else 
      tieCount++;
// #endif
    } else if(tieCount) { //! then the 'previous' items are the same:
      // FIXME: verify our assumption that [”elow] has an singicnat time-cost ... or alterntivly that compiler manages to handle this case 'through simple optizmiaotn-strategies'.
      //#if(configure_distanceMetricComputation_kendallFast_useSumInsteadOf_mul == 1)
#if(configure_generic_useMulInsteadOf_div == 1)
     cnt_overlap_pairs += (tieCount * (tieCount + 1)) * 0.5; //! ie, icnremnt wrt. the number of sorted pairs.
#else
     cnt_overlap_pairs += (tieCount * (tieCount + 1)) / 2; //! ie, icnremnt wrt. the number of sorted pairs.
#endif
      tieCount = 0; //! ie, as this element was not sorted.
    }
  }
  if(tieCount) { //! then the previous visisted/ordered list-items were the same.
#if(configure_generic_useMulInsteadOf_div == 1)
    cnt_overlap_pairs += (tieCount * (tieCount + 1)) * 0.5; //! ie, icnremnt wrt. the number of sorted pairs.
#else
    cnt_overlap_pairs += (tieCount * (tieCount + 1)) / 2; //! ie, icnremnt wrt. the number of sorted pairs.
#endif
  }
  return cnt_overlap_pairs;
}


//! Comptue Kendalls tau 
//! @remarks This function calculates the Kendall covariance (if cor == 0) or correlation (if cor != 0), but assumes arr1 has already been sorted and arr2 has already been reordered in lockstep. 
static t_float __knights_algorithm(const uint arr_size, t_float* arr1, t_float* arr2, const e_kt_correlationFunction_t metric_id) {
    // assert(false); // FIXME: remember to first sort the first array before usign 'it' as input to this funciton.
    // assert(false); // FIXME: remember to 're-oder' the second array based on the firt array.

    // assert(false); // FIXME: when calling this funciton ... remember to always use a 'temporal copy' of arr1 and arr2 .. ie, as we modify the latters 'in the operaiton'.

#if(configure_generic_useMulInsteadOf_div == 1)
    const long long int nPair = (long long int) arr_size * ((long long int) arr_size - 1) * 0.5; //! ie, the number of sorted pairs.
#else
    const long long int nPair = (long long int) arr_size * ((long long int) arr_size - 1) / 2; //! ie, the number of sorted pairs.
#endif
    int64_t numberOf_overlappingCombinations_arr1_to_arr2 = nPair;

    long long int numberOf_overlappingCombinations_arr1 = 0, tieCount = 0;
    //! Note: worst-case complexity of [below] is "2*n", ie, aprpxoiate the same as the "n*log(n)" merge-sort.

    // FIXME: describe a case where [below] may be omitted ... ie, where the overlappign combiantions may be removed without loss of accuracy.
    for(size_t i = 1; i < arr_size; i++) {
        if(arr1[i - 1] == arr1[i]) {
	  //! Then the values are euqal, ie, increment.
	  tieCount++;
        } else if(tieCount > 0) {
	  //! Then we evaluate/idneitfy the sort-distance for the set of equal values in arr1 wrt. arr2:
	  sort_array_insertionSort_slow(/*start-pos=*/arr2 + i - tieCount - 1, /*end-pos=*/tieCount + 1);
	  //insertionSort(/*start-pos=*/arr2 + i - tieCount - 1, /*end-pos=*/tieCount + 1;)
#if(configure_generic_useMulInsteadOf_div == 1)
	  numberOf_overlappingCombinations_arr1 += tieCount * (tieCount + 1) * 0.5; //! ie, icnremnt wrt. the number of sorted pairs.
#else
	  numberOf_overlappingCombinations_arr1 += tieCount * (tieCount + 1) / 2; //! ie, icnremnt wrt. the number of sorted pairs.
#endif
	  numberOf_overlappingCombinations_arr1_to_arr2 += get_numberOf_combinations_forOverlaps(arr2 + i - tieCount - 1, tieCount + 1); //! ie, icnremnt wrt. the number of sorted pairs for the saem index-chunk in arr2
	  //! Reset:
	  tieCount = 0;
        }
    }

    if(tieCount > 0) {
      const size_t i = arr_size;
      assert(i == arr_size); //! ie, what we expect.
      sort_array_insertionSort_slow(/*start-pos=*/arr2 + i - tieCount - 1, /*end-pos=*/tieCount + 1);
      //insertionSort(arr2 + i - tieCount - 1, tieCount + 1); //! where "tieCount" is sued to avoid including the 'last values which were similar'.
#if(configure_generic_useMulInsteadOf_div == 1)
      numberOf_overlappingCombinations_arr1 += tieCount * (tieCount + 1) * 0.5; //! ie, icnremnt wrt. the number of sorted pairs.
#else
      numberOf_overlappingCombinations_arr1 += tieCount * (tieCount + 1) / 2; //! ie, icnremnt wrt. the number of sorted pairs.
#endif
      numberOf_overlappingCombinations_arr1_to_arr2 += get_numberOf_combinations_forOverlaps(arr2 + i - tieCount - 1, tieCount + 1); //! ie, icnremnt wrt. the number of sorted pairs for the saem index-chunk in arr2
    } //! else there are no 'direct


    //! 
    //! Idnetify the swap-distance for arr2:
    const long long int swap_distance_arr1_to_arr2 = mergeSort(arr2, arr1, arr_size, NULL); //! ie, the squared absolute distance "sum(term1 - term2)^2"
    const long long int numberOf_overlappingCombinations_arr2 = get_numberOf_combinations_forOverlaps(arr2, arr_size); //! ie, "sum(term1^2)"
    
    const int64_t numberOf_swaps = //! ie,: sum(term1 - term2)^2
      numberOf_overlappingCombinations_arr1_to_arr2 
      - /*swaps-for-qual-values-seperately-in-arr1-and-arr2=*/(numberOf_overlappingCombinations_arr1 + numberOf_overlappingCombinations_arr2) 
      +  /*swap-distance=*/2 * swap_distance_arr1_to_arr2;

    
    const t_float denumerator1 = (t_float)nPair - (t_float)numberOf_overlappingCombinations_arr1; //! ie, 
    const t_float denumerator2 = (t_float)nPair - (t_float)numberOf_overlappingCombinations_arr2;
    const t_float covariance = 2*numberOf_swaps;

    if(numberOf_swaps == 0) {
      // TODO[aritlce]: verify correctness of [”elow] 'handling' (oekseth, 06. des. 2016) && (oekseth, 06. apr. 2017).
      return metricMacro__constants__defaultValue__postProcess__allMatches;
      //return 0;
    }

    //! -------
    if(metric_id == e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine) {
      //if(isTo_use_correlation) {
      const t_float denumerator = (sqrt(denumerator1) * sqrt(denumerator2));
      //! ---
      const t_float cor = (denumerator && numberOf_swaps) ? numberOf_swaps / denumerator : metricMacro__constants__defaultValue__postProcess__allMatches;
      // printf("tieCount=%u, arr_size=%u, nPair=%f, numberOf_overlappingCombinations_arr1=%f, numberOf_overlappingCombinations_arr2=%f, numberOf_swaps=%f, denumerator=%f, cor=%f, at %s:%d\n",  	     (uint)tieCount, (uint)arr_size, (t_float)nPair, (t_float)numberOf_overlappingCombinations_arr1, (t_float)numberOf_overlappingCombinations_arr2, (t_float)numberOf_swaps, denumerator, cor, __FILE__, __LINE__);
      return cor;
    } else  if(metric_id == e_kt_correlationFunction_groupOf_rank_kendall_coVariance) {
      /* Return covariance. */
      return covariance;
    } else if(metric_id == e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock) {
      const t_float denumerator = (denumerator1 + denumerator2 - covariance);
      //! ---
      const t_float cor = (denumerator && numberOf_swaps) ? numberOf_swaps / denumerator : metricMacro__constants__defaultValue__postProcess__allMatches;
      return cor;
    } else if(metric_id == e_kt_correlationFunction_groupOf_rank_kendall_Dice) {
      const t_float denumerator = (denumerator1 + denumerator2);
      //! ---
      const t_float cor = (denumerator && numberOf_swaps) ? 2*numberOf_swaps / denumerator : metricMacro__constants__defaultValue__postProcess__allMatches;
      return cor;
    } else if(metric_id == e_kt_correlationFunction_groupOf_rank_kendall_Goodman) {
      //! Note: [”elow] equaiton is based on the work found in ["https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-15-S2-S2" w/title "On the selection of appropriate distances for gene expression data clustering"] (oekseth, 06. apr. 2017).
      //! Note: difference betwene "Dice" and "Goodman-Kruskal" is wrt. the 'omssison' of the "2 *" adjustment.
      const t_float denumerator = (denumerator1 + denumerator2);
      //! ---
      const t_float cor = (denumerator && numberOf_swaps) ? numberOf_swaps / denumerator : metricMacro__constants__defaultValue__postProcess__allMatches;
      return cor;
    } else if(metric_id == e_kt_correlationFunction_groupOf_rank_kendall_coVariance_normalized) {
      assert(arr_size > 1);
      const t_float denumerator = ((t_float)arr_size * (t_float(arr_size)-1)); //! where simliarty wrt. "Dice" is see in the sue of the "2 *" adjustment. 
      //! ---
      const t_float cor = (denumerator && numberOf_swaps) ? 2*numberOf_swaps / denumerator : metricMacro__constants__defaultValue__postProcess__allMatches;
      return cor;
    } else {
      fprintf(stderr, "!!\t The requested distance-configuraiton is Not supported: Please update your API-calls and/or send a feautre re-quest to the develoepr at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false);

      //! A fall-back: return the co-veriance:
      return covariance;
    }
}

  // FIXME: update our "graphAlgorithms_timeMeasurements_syntetic.cxx" ... evalaute difference memory-access-cases wrt. itnristinictics ... complete the update of our insertion-sort wrt. itnrisitnicts ... and write new tests for "__copy_rank_hadnleEmptyCells(..)" and our "update_cells_above_firstFoorLoop(..)" and our "__adjustMatrix_byWeight(..)" and our "ktCorr_matrixBase__optimal_compute_allAgainstAll(..)" ... in our "graphAlgorithms_timeMeasurements_syntetic.cxx" and update assicated code in our "graphAlgorithms_distance.cxx" 



//! Copy the rank and ingore/avoid non-set values.
static t_float __copy_rank_hadnleEmptyCells(const uint n, const uint *vector1_ranks, const t_float *vector_2, const char *mask2_vector, t_float *arr1_tmp, t_float *arr2_tmp, const bool needTo_useMask_evaluation) {
  //! Update the index:
  if(needTo_useMask_evaluation == false) { //! then we assuem all values are of interest, ie, no T_FLOAT_MAX valeus nor any mask2_vector[][]==0 values
    for(uint i = 0; i < n; i++) {
      const uint index = vector1_ranks[i];
      arr2_tmp[index] = vector_2[i]; //! ie, update the postion.
    }
  } else {
    if(mask2_vector) {
      for(uint i = 0; i < n; i++) {
	if(isOf_interest(arr1_tmp[i])) {
	  // FIXME: instead of [”elow] use _mm_storeu_ps
	  const uint index = vector1_ranks[i];
	  if(mask2_vector[index]) {
	    assert(index < n);
	    arr2_tmp[index] = vector_2[i]; //! ie, update the postion.
	  } else { //! then we ecplixtly markt he value to be ingored.
	    arr2_tmp[index] = T_FLOAT_MAX;
	    arr1_tmp[index] = T_FLOAT_MAX;
	  }
	} //! otherwise we asusme teh values is not of interest.
      } 
    } else {
      for(uint i = 0; i < n; i++) {
	if(isOf_interest(arr1_tmp[i])) {
	  const uint index = vector1_ranks[i];
	  if(isOf_interest(arr2_tmp[index])) {
	    assert(index < n);
	    arr2_tmp[index] = vector_2[i]; //! ie, update the postion.
	  } else { //! then we ecplixtly markt he value to be ingored.
	    arr2_tmp[index] = T_FLOAT_MAX;
	    arr1_tmp[index] = T_FLOAT_MAX;
	  }
	} //! otherwise we asusme teh values is not of interest.
      } 
    }
    // FIXME[jc] ... tag="part-traverse-1d-cmp-ofInterest" .. see "ex_intrisintics.h" for 'candidate-optimziaiton'.

    //! Complete, ie, iterate/remove/'shift-away' non-set values.
    uint current_index = 0;
    for(uint i = 0; i <n; i++) {
      if(isOf_interest(arr1_tmp[i])) {
	arr1_tmp[i] = arr1_tmp[current_index]; 
	// FIXME: validate correctness of [”elow].
	// arr1_tmp[count] = T_FLOAT_MAX; 
	current_index++;
      }
      //! else/otehrwise the function/value is ingored. 
      //assert(i >= count);
    }
  }

  return 1;
}

//! Comptue Kendalls Tau through Knitghts improved algorithm -reusing memory:
t_float kendall_improvment_kingsAlgorithm(const uint n, t_float *vector_1, t_float *vector_2, const char *mask2_vector, t_float* arr1_tmp, t_float* arr2_tmp, const uint *vector1_ranks, const e_kt_correlationFunction_t metric_id, const bool needTo_useMask_evaluation) {
//t_float kendall_improvment_kingsAlgorithm(const uint n, t_float *vector_1, t_float *vector_2, const char *mask2_vector, t_float* arr1_tmp, t_float* arr2_tmp, const uint *vector1_ranks, const bool vector1_is_sorted, const enum e_kt_correlationFunction_t metric_id, const bool needTo_useMask_evaluation) {
  assert(vector_1); assert(vector_2); 
  //assert(arr1_tmp); assert(arr2_tmp);
  //! Copy:
  memcpy(arr1_tmp, vector_1, sizeof(t_float)*n);
  
  const bool isTo_reUse_memory = (arr1_tmp != NULL);
  if(arr1_tmp == NULL) {
    arr1_tmp = allocate_1d_list_float(n, /*default-value=*/0); //(t_float*)malloc(n*sizeof(t_float));
  }
  if(arr2_tmp == NULL) {
    arr2_tmp = allocate_1d_list_float(n, /*default-value=*/0); 
    //arr2_tmp = (t_float*)malloc(n*sizeof(t_float));
  }


  // printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    

  //const bool has_allcoatedMemory_locally = (vector1_ranks == NULL); 
  if(vector1_ranks == NULL) { //! then we assume teh input-list is not sorted
    // printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    
    // t_float *data_tmp[1];
    // data_tmp[0] = vector_1;
    //uint *index = (uint*)malloc(n*sizeof(uint));
    uint *index = allocate_1d_list_uint(n, /*default-value=*/0); 
    t_float *arr_result = allocate_1d_list_float(n, /*default-value=*/0); 
    //t_float *arr_result = (t_float*)malloc(n*sizeof(t_float));
    getrank_memoryRe_use(n, vector_1, arr_result, index, e_distance_rank_typeOf_computation_ideal, NULL);

    //! Copy the rank and ingore/avoid non-set values.
    __copy_rank_hadnleEmptyCells(n, index, vector_2, mask2_vector, arr1_tmp, arr2_tmp, needTo_useMask_evaluation);

    // //! Update the index:
    // for(uint i = 0; i < n; i++) {
    //   const uint index = index[i];
    //   assert(index < n);
    //   arr2_tmp[index] = vector_2[i]; //! ie, update the postion.
    // }
    //! The call:
    t_float distance = 1;
    if(n > 1) {
      distance = __knights_algorithm(n, arr1_tmp, arr2_tmp, metric_id);    
    } else if(n == 1) {
      // TODO[aritlce]: verify correctness of [”elow] 'handling' (oekseth, 06. des. 2016)
      distance = (arr1_tmp[0] == arr2_tmp[0]); //! ie, if the distances are equal 'then there is zero number of swaps, for which we infer that they are euqal' (oekseth, 06. des. 2016).
    }
    //! De-allocate
    free (index); free(arr_result);
    if(isTo_reUse_memory == false) {
      free(arr1_tmp); free(arr2_tmp);
    }
    //! @return
    //printf("distance=%f, n=%u, at [%s]:%s:%d\n", distance, n, __FUNCTION__, __FILE__, __LINE__);    
    return distance;
    //mergeSort(arr1_tmp, arr1_tmp, /*size=*/n);
  } else {
    // printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    
    //! Copy the rank and ingore/avoid non-set values.
    __copy_rank_hadnleEmptyCells(n, vector1_ranks, vector_2, mask2_vector, arr1_tmp, arr2_tmp, needTo_useMask_evaluation);

    //memcpy(arr2_tmp, vector_2, sizeof(t_float)*n);
    //! The call:
    t_float distance = 1;
    if(n > 1) {
      distance = __knights_algorithm(n, arr1_tmp, arr2_tmp, metric_id);    
    } else if(n == 1) {
      // TODO[aritlce]: verify correctness of [”elow] 'handling' (oekseth, 06. des. 2016)
      distance = (arr1_tmp[0] == arr2_tmp[0]); //! ie, if the distances are equal 'then there is zero number of swaps, for which we infer that they are euqal' (oekseth, 06. des. 2016).
    }
    //const t_float distance = __knights_algorithm(n, arr1_tmp, arr2_tmp, metric_id);    
    //! De-allocate
    if(isTo_reUse_memory == false) {
      free(arr1_tmp); free(arr2_tmp);
    }
    //! @return
    return distance;
  }
}



/**
   @brief compute the Kendall distance between two rows or columns. The Kendall distance is defined as one minus Kendall's tau.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
 **/
t_float kt_kendall(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_kt_correlationFunction_t metric_id, const s_allAgainstAll_config_t self) {
  if( (transpose == 0) && self.forNonTransposed_useFastImplementaiton) {

    const t_float defaultValue_float = 0;
    t_float *arr1_tmp = allocate_1d_list_float(ncols, defaultValue_float);
    t_float *arr2_tmp = allocate_1d_list_float(ncols, defaultValue_float);
    
    t_float *vector_1 = data1[index1];
    if(mask1) { //! then we use implcit makssk, ie, to simplify our call to the "kendall_improvment_kingsAlgorithm(..)" function:
      vector_1 = allocate_1d_list_float(ncols, defaultValue_float);
      assert(vector_1);
      memcpy(vector_1, data1[index1], sizeof(t_float)*ncols);
      const char *__restrict__ mask1_local = mask1[index1];
      assert(mask1_local);
      for(uint col_id = 0; col_id < ncols; col_id++) {
	if(mask1_local[col_id]) {
	  vector_1[col_id] = T_FLOAT_MAX; //! ie, then use implict mask.
	}	  
      }
    }

    // FIXME: identify a use-case where "metric_id" should be set to "false".


    //assert(index2 < nrows);
    
    // printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    

    assert(data2[index2]);
    //    printf("(call-improved)\t at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    
    const t_float return_value = kendall_improvment_kingsAlgorithm(ncols, vector_1, data2[index2], 
								   (mask2) ? mask2[index2] : NULL, 
								   arr1_tmp, arr2_tmp, /*vector1_ranks=*/NULL, metric_id, /*needTo_useMask_evaluation=*/s_allAgainstAll_config__needTo_useMask_evaluation(&self));
    
    if(mask1) {
      assert(vector_1 != data1[index1]);
      free_1d_list_float(&vector_1);
    }

    //! De-allcoate locally rserved ata:
    free_1d_list_float(&arr1_tmp); free_1d_list_float(&arr2_tmp);
    //! @return the result:
    return return_value;
  } else {
    //    printf("at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);    
    return __kt_kendall_slow(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose);
  }
}
