/**
   @remarks build approx. 10 different evlauation-cases, where latter is of speicfic interes/importance in hypotsis-evalaution.
 **/

  { //! ****************************************************************:: complete set of simlairty-metics as a pre-step:
    const bool isTo__evaluate__simMetric__MINE__Euclid = false;
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = false;
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s_all_%s", stringOf_resultDir_base, "hcaAndKMeans");
      //const char *stringOf_resultDir = "results/dataSyn_hcaAndKMeans";
#define __Mi__algSet 0
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = true;
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s_all_%s", stringOf_resultDir_base, "hcaAndKMeans__hcaPreStep");
      //const char *stringOf_resultDir = "results/dataSyn_hcaAndKMeans__hcaPreStep";
#define __Mi__algSet 0
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    //! -------------------K-MEANS---------------------------
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = false;
      //const char *stringOf_resultDir = "results/dataSyn_KMeans";
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s_all_%s", stringOf_resultDir_base, "kMeans");
#define __Mi__algSet 1
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = true;
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s_all_%s", stringOf_resultDir_base, "kMeans__hcaPreStep");
      //const char *stringOf_resultDir = "results/dataSyn_KMeans__hcaPreStep";
#define __Mi__algSet 1
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    //! ----------------HCA------------------------------
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = false;
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s_all_%s", stringOf_resultDir_base, "hca");
      //const char *stringOf_resultDir = "results/dataSyn_hca";
#define __Mi__algSet 2
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    //! ----------------------------------------------
  }

  { //! ****************************************************************
    const bool isTo__evaluate__simMetric__MINE__Euclid = true;
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = false;
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s%s", stringOf_resultDir_base, "hcaAndKMeans");
      //const char *stringOf_resultDir = "results/dataSyn_hcaAndKMeans";
#define __Mi__algSet 0
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = true;
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s%s", stringOf_resultDir_base, "hcaAndKMeans__hcaPreStep");
      //const char *stringOf_resultDir = "results/dataSyn_hcaAndKMeans__hcaPreStep";
#define __Mi__algSet 0
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    //! -------------------K-MEANS---------------------------
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = false;
      //const char *stringOf_resultDir = "results/dataSyn_KMeans";
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s%s", stringOf_resultDir_base, "kMeans");
#define __Mi__algSet 1
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = true;
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s%s", stringOf_resultDir_base, "kMeans__hcaPreStep");
      //const char *stringOf_resultDir = "results/dataSyn_KMeans__hcaPreStep";
#define __Mi__algSet 1
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    //! ----------------HCA------------------------------
    {
      const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
      const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
      const bool isTo__testHCA_komb__inKmeans = false;
      char stringOf_resultDir[2000] = {'\0'}; sprintf(stringOf_resultDir, "%s%s", stringOf_resultDir_base, "hca");
      //const char *stringOf_resultDir = "results/dataSyn_hca";
#define __Mi__algSet 2
#include "tut_hypothesis_whyResultDiffers_1.c"
#undef __Mi__algSet
    }
    //! ----------------------------------------------
  }
