
//! Add the name of this row:
char string_local[1000] = {'\0'}; sprintf(string_local, "matrix.%u::%s::%s", matrix_id, getStringOf__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp), typeOf_distanceInference);
assert(row_id_global < obj_metricDataScores.nrows);
assert(strlen(string_local));
//printf("row=[%u]\t inserts-string=\%s\", at %s:%d\n", row_id_global, string_local, __FILE__, __LINE__);
#ifndef NDEBUG
//! Validate that the string-id has Not bee set 'at an earlier exueciton-poibnt', ie, to avoid overlaps wrt. the result-generation:
assert(!obj_metricDataScores.nameOf_rows || !obj_metricDataScores.nameOf_rows[row_id_global] || !strlen(obj_metricDataScores.nameOf_rows[row_id_global]));
#endif
set_string__s_kt_matrix(&obj_metricDataScores, row_id_global, string_local, /*addFor_column=*/false);
//printf("ok\t inserts-string=\%s\", at %s:%d\n", string_local, __FILE__, __LINE__);

//fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
	    
uint local_col_id = 0;
//!
//! Iterate through the data:
for(uint data_1 = 0; data_1 < cnt_data_nrows; data_1++) {
  //!
  //! Iterate, comparing 'this' to the other/laterntive data-sets:
  for(uint data_2 = 0; data_2 < cnt_data_ncols; data_2++) {
    const uint cnt_clusters_1 = cnt_clusters;
    const uint cnt_clusters_2 = cnt_clusters;
    uint *mapOf_vertexToCentroid1 = arrOf_2d_data__set1[data_1];
    uint *mapOf_vertexToCentroid2 = arrOf_2d_data__set2[data_2];
    //! ---- 
    s_kt_matrix_cmpCluster_t obj_cmp; setTo_empty__s_kt_matrix_cmpCluster_t(&obj_cmp);
    const uint max_cntClusters = macro_max(cnt_clusters_1, cnt_clusters_2);
    assert(matrixOf_coOccurence); //! ie, as we expect 'this to be set'.    
    // printf("\t(copmute)\t %s\t\t [%u][%u]\t, at %s:%d\n", string_local, data_1, data_2, __FILE__, __LINE__);
    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, cnt_vertices, /*isTo_includeWeight=*/false, max_cntClusters, matrixOf_coOccurence, matrixOf_coOccurence, config_intraCluster);
    if(!( (obj_cmp.clusterDistances_cluster1.nrows != 0)  && (obj_cmp.clusterDistances_cluster2.nrows != 0) )) {
      assert(false); //! ie, as we then have an in-cosnsitency.
    }
		  
    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    //! Comptue for the 'specific' cluster-comparison-score:
    s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
    computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, metric_clusterCmp, &metric_result); 
    const t_float scalar_score_altTest = metric_result.score_1;
    assert(local_col_id < obj_metricDataScores.ncols);
    assert(row_id_global < obj_metricDataScores.nrows);
    obj_metricDataScores.matrix[row_id_global][local_col_id] = scalar_score_altTest;
    local_col_id++; //! ie, increment
    //!
    //! Updat ehte matrix with the score:
    //!
    //! De-allocate:
    free__s_kt_matrix_cmpCluster(&obj_cmp);
  }
 }	    	    
