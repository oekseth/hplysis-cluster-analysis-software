#ifndef kt_aux_matrix_binning_h
#define kt_aux_matrix_binning_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_aux_matrix_binning.h
   @brief provides access to 'binning' funcitons, eg, for use when construcitng histograms (oekseth, 06. feb. 2018)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2018).
   @remarks among others used in algorithms and applications of image anlaysis. 
**/


//#include "type_2d_float_nonCmp_uint.h"
#include "kt_matrix_base.h"
#include "kt_list_2d.h"

/**
   @brief constructs a histogram from a matrix
   @param <self> is the matrix with data
   @param <cnt_bins> is the number of 'buckets' to be used wrt. the histogram-cosntruciton
   @param <isTo_forMinMax_useGlobal> which if set to false impleis taht the 'buckets' are ajduted locally for each row.
   @param <enum_id_scoring> the type of scores to be stored in the histogram.
   @return a vector holding the histogram
 **/
s_kt_list_1d_float_t applyTo_matrix__kt_aux_matrix_binning(const s_kt_matrix_base_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring); 

/**
   @brief constructs a histogram from a matrix
   @param <self> is the list with data
   @param <cnt_bins> is the number of 'buckets' to be used wrt. the histogram-cosntruciton
   @param <isTo_forMinMax_useGlobal> which if set to false impleis taht the 'buckets' are ajduted locally for each row.
   @param <enum_id_scoring> the type of scores to be stored in the histogram.
   @return a vector holding the histogram
 **/
s_kt_list_1d_float_t applyTo_list__kt_aux_matrix_binning(const s_kt_list_1d_float_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring); 


/**
   @brief constructs a a histogram from a 2d-list of sparse relationships
   @param <self> is the matrix with data
   @param <cnt_bins> is the number of 'buckets' to be used wrt. the histogram-cosntruciton
   @param <isTo_forMinMax_useGlobal> which if set to false impleis taht the 'buckets' are ajduted locally for each row.
   @param <enum_id_scoring> the type of scores to be stored in the histogram.
   @return a vector holding the histogram
 **/
static s_kt_list_1d_float_t applyTo_sparse__kt_aux_matrix_binning(const s_kt_list_2d_kvPair_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring) {
  return translate_histogram__s_kt_list_2d_kvPair_t(self, cnt_bins, isTo_forMinMax_useGlobal, enum_id_scoring);
}



#endif //! EOF
