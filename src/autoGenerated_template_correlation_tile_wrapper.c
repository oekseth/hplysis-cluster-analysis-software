/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */




/**
   @brief a wrapper-template to build tile-functions for different configurations
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_notMask> name of funciton
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit> name of function
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit> name of function
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_notMask_weight> name of function: use weight
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit_weight> name of function: use weight
   @tparam<TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit_weight> name of function: use weight
   ----------------------------------------------------------------------------------------
   @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask> name of funciton
   @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit> name of function
   @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit> name of function
   @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask_weight> name of function: use weight
   @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit_weight> name of function: use weight
   @tparam<TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit_weight> name of function: use weight
   ----------------------------------------------------------------------------------------
   @tparam<TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask> name of funciton
   @tparam<TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit> name of function
   @tparam<TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit> name of function
   @tparam<TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask_weight> name of function: use weight
   @tparam<TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit_weight> name of function: use weight
   @tparam<TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit_weight> name of function: use weight
   ----------------------------------------------------------------------------------------
   @tparam<TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask> name of funciton
   @tparam<TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit> name of function
   @tparam<TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit> name of function
   @tparam<TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask_weight> name of function: use weight
   @tparam<TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit_weight> name of function: use weight
   @tparam<TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit_weight> name of function: use weight
   ----------------------------------------------------------------------------------------
   @tparam<TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC> which is used to specify the function responsible for post-processg (eg, if teh resutls are compiued in 'aprtial' using MPI): difers from the otter fucntiosn wrt. its 'non-assciated' to 'mask and 'weight', ie, is therefore defined only once (for a given 'fucntion-id');
   ----------------------------------------------------------------------------------------
   @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight> weight-macro-function
   @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight> SSE-macro-fucntion for copmtuing weights
   @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument> which if set impleis that we use the input-weight as argumetn when calling the distnace-macro-matrix.
   @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax> which if set to "1" impleis that we do seelec tthe max-value instead of cocnatnating valeus.
   @tparam<TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin> which if set to "1" impleis that we do seelec tthe min-value instead of cocnatnating valeus.
   @tparam<TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate> which refers to teh "e_template_correlation_tile_typeOf_distanceUpdate" enum and is used to simplify the comptaution of partial sets wrt. SSE-computaiton: relates to "s_template_correlation_tile_temporaryResult_SSE", "s_template_correlation_tile_temporaryResult" and "s_kt_computeTile_subResults_t".
   @tparam<TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices> which if set to "1" is used to incldue a count of number of paris itneresting of reach 'co-veraicne-correlation-cell' (in the result-matrix).
   @tparam<> ... 
   @tparam<> ... 

   @remarks last generated at Fri Oct 13 17:49:55 2017 from "build_codeFor_tiling.pl"
 **/
	    
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC_notMask
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
#ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
	    
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
#ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
	    
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
#ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
	    
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
#ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
	    
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
#ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
	    
#ifndef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
#ifndef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
	    
//! -------------------------------------------------- Config-params:
//! ---
#ifndef TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif
//! ---
#ifndef TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#error "Error: tempalte-parameter not set, ie, please udpate your calls"
#endif

#define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_notMask
#define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask
#define TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask
#define TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask
#define TEMPLATE_iterationConfiguration_typeOf_mask __macro__e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
//#define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
//! --
#define TEMPLATE_distanceConfiguration_useWeights 0
#define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
//#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#define TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction
//! Apply [above] specificaiotns:
#include "template_correlation_tile.cxx"

#define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight
#define TEMPLATE_iterationConfiguration_typeOf_mask __macro__e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
//#define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
//! --
#define TEMPLATE_distanceConfiguration_useWeights 1
#define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
//#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#define TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction
//! Apply [above] specificaiotns:
#include "template_correlation_tile.cxx"

#define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit
#define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit
#define TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit
#define TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit
#define TEMPLATE_iterationConfiguration_typeOf_mask __macro__e_template_correlation_tile_maskType_mask_explicit //! ie, Not use mask
//#define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
//! --
#define TEMPLATE_distanceConfiguration_useWeights 0
#define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
//#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#define TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction
//! Apply [above] specificaiotns:
#include "template_correlation_tile.cxx"

#define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight
#define TEMPLATE_iterationConfiguration_typeOf_mask __macro__e_template_correlation_tile_maskType_mask_explicit //! ie, Not use mask
//#define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
//! --
#define TEMPLATE_distanceConfiguration_useWeights 1
#define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
//#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#define TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction
//! Apply [above] specificaiotns:
#include "template_correlation_tile.cxx"

#define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit
#define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit
#define TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit
#define TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit
#define TEMPLATE_iterationConfiguration_typeOf_mask __macro__e_template_correlation_tile_maskType_mask_implicit //! ie, Not use mask
//#define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
//! --
#define TEMPLATE_distanceConfiguration_useWeights 0
#define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
//#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#define TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction
//! Apply [above] specificaiotns:
#include "template_correlation_tile.cxx"

#define TEMPLATE_NAMEOF_MAIN_FUNC TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__compareTwoRows TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__oneToMany TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight
#define TEMPLATE_NAMEOF_MAIN_FUNC__manyToMany TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight
#define TEMPLATE_iterationConfiguration_typeOf_mask __macro__e_template_correlation_tile_maskType_mask_implicit //! ie, Not use mask
//#define TEMPLATE_iterationConfiguration_typeOf_mask e_template_correlation_tile_maskType_mask_none //! ie, Not use mask
//! --
#define TEMPLATE_distanceConfiguration_useWeights 1
#define TEMPLATE_distanceConfiguration_macroFor_metric TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_SSE TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#define TEMPLATE_distanceConfiguration_macroFor_metric_includePower TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#define TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
//#define TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#define TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#define TEMPLATE_local_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#define TEMPLATE_local_distanceConfiguration_typeOf_postProcessing_distanceFunction TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction
//! Apply [above] specificaiotns:
#include "template_correlation_tile.cxx"

//! -------------------------------------------------------------------
//!            Reset template-parameters:
//! -------------------------------------------------------------------

#undef TEMPLATE_NAMEOF_MAIN_FUNC_notMask
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask
#undef TEMPLATE_NAMEOF_MAIN_FUNC_notMask__weight
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask__weight
#undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit
#undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit__weight
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit__weight
#undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit
#undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit__weight
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit__weight

//! -------**: end: local-row-functions:
#undef TEMPLATE_NAMEOF_MAIN_FUNC_notMask
#undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit
#undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit
#undef TEMPLATE_NAMEOF_MAIN_FUNC_notMask_weight
#undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_explicit_weight
#undef TEMPLATE_NAMEOF_MAIN_FUNC_mask_implicit_weight
//!   ----------------------------------------------------------------------------------------
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_notMask_weight
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_explicit_weight
#undef TEMPLATE_global_2Rows_NAMEOF_MAIN_FUNC_mask_implicit_weight
//!   ----------------------------------------------------------------------------------------
#undef TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask
#undef TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit
#undef TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit
#undef TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_notMask__weight
#undef TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight
#undef TEMPLATE_global_oneToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight
//!   ----------------------------------------------------------------------------------------
#undef TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask
#undef TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit
#undef TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit
#undef TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_notMask__weight
#undef TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_explicit__weight
#undef TEMPLATE_global_manyToMany_NAMEOF_MAIN_FUNC_mask_implicit__weight
//!   ----------------------------------------------------------------------------------------
#undef TEMPLATE_semiGlobal_correlationMPI_postProcess_NAMEOF_FUNC
//!   ----------------------------------------------------------------------------------------
#undef TEMPLATE_distanceConfiguration_macroFor_metric_nonWeight
#undef TEMPLATE_distanceConfiguration_macroFor_metric_inputParam_usePowerInArgument
#undef TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMax
#undef TEMPLATE_distanceConfiguration_macroFor_metric_concatenateResults_useMin
#undef TEMPLATE_distanceConfiguration_macroFor_metric_SSE_nonWeight
#undef TEMPLATE_global_distanceConfiguration_typeOf_distanceUpdate
#undef TEMPLATE_global_distanceConfiguration_isTo_updateCountOf_vertices
#undef TEMPLATE_global_distanceConfiguration_isTo_applyWeightMuliplicaitonAfter_computationOfScores
#undef TEMPLATE_global_distanceConfiguration_typeOf_postProcessing_distanceFunction
