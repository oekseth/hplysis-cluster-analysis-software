// printf("at %s:%d\n", __FILE__, __LINE__);
      s_aux_findPartitionFor_tiles_t tile_r2_symmetric = tile_r2;
      if(globalProp__dataSetIsSymmeric) {
	// FIXME[JC]: are you able to 'make' [below] more 'symmeitrc' ... eg, to 'change the outer tile-size' wrt. ...??... (oekseth, 06. des. 2016).
	tile_r2_symmetric = initAndGet__s_aux_findPartitionFor_tiles_t((index1+chunkSize_index1), SM);
      } /* else { //! else the matrix is assumed not to be symmetric. */
      //! 
      //! re-Initaite the count-object:
#if(__config__funcTemplateAllAgainstAll__storeIntermeidateResults == 1)
      clearOldData(obj_resultsFromTiling, typeOf_distanceFormula, isTo_updateCountVariable, isTo_findMaxValue, isTo_findMinValue);
#endif //! else we assume the user is itnerested in 'keeping the data for their own use'.
      const uint chunkSize_index2 = get_blockSize__s_aux_findPartitionFor_tiles_t(&tile_r2_symmetric, cnt_index2);
      assert_possibleOverhead( (index2 + chunkSize_index2) <= iterationIndex_2);

      uint i = 0;

      cnt_cells_evaluated += (chunkSize_index2*chunkSize_index1);

      // printf("cnt-cells-evaluated=%u given += %u = (|block1|=%u * |block2|=%u), at %s:%d\n", cnt_cells_evaluated, chunkSize_index2*chunkSize_index1, chunkSize_index1, chunkSize_index2, __FILE__, __LINE__);
      //!
      //! Comptue for teh 'inner tile':
      s_kt_computeTile_subResults_t *complexResult = obj_resultsFromTiling; //! which is used in oru "func_template__tile.c"
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
      assert(complexResult);
#endif
      const s_allAgainstAll_config_t self = *config_allAgainstAll;

      // const uint numberOf_chunks_cols

#define __localConfig__templateTile__rowSize_isKnown 1
      uint debug_cnt_resultCells_updated = 0;
      const uint numberOf_chunks_cols = tile_col.cntBlocks;
      const uint chunkSize_col_last = tile_col.sizeEachBlock_last;
      //assert(SM == tile_col.sizeEachBlock_beforeLast); //! ie, what we expect. <-- if 'this is wrong' then inveistgate if 'it is due to ncols < SM' (oekseth, 06. nov. 2016).
      if(listOf_inlineResults) {
	//fprintf(stderr, "has |list|=%u, and access poitner=listOf_inlineResults=%p, at %s:%d\n", SM, listOf_inlineResults, __FILE__, __LINE__);
	assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(listOf_inlineResults[0]);
      }
//printf("at %s:%d\n", __FILE__, __LINE__);
#include "func_template__tile.c" //! ie, compute for teh tile.
#undef __localConfig__templateTile__rowSize_isKnown
      
//printf("at %s:%d\n", __FILE__, __LINE__);
#if(__config__funcTemplateAllAgainstAll__storeIntermeidateResults == 0)
//printf("at %s:%d\n", __FILE__, __LINE__);
      //! **************************************************************************'
 if(listOf_inlineResults == NULL) {assert(resultMatrix);}
      const uint globalStartPos_head = index1; const uint globalStartPos_tail = index2;
      //      const uint globalStartPos_head = cnt_index1; const uint globalStartPos_tail = cnt_index2;
      //printf("globalStartPos=(%u, %u), given block-cnt=[%u][%u], at %s:%d\n", globalStartPos_head, globalStartPos_tail, cnt_index1, cnt_index2, __FILE__, __LINE__);
      const uint nrows_to_evaluate = chunkSize_index1;
      const uint nrows_outer_to_evaluate = chunkSize_index2;      
      {
	const uint cnt_expected = (nrows_to_evaluate * nrows_outer_to_evaluate);
	//printf("## debug_cnt-evaluated=%u, cnt-expected=%u, at %s:%d\n", debug_cnt_resultCells_updated, cnt_expected, __FILE__, __LINE__);
	assert_possibleOverhead(debug_cnt_resultCells_updated == cnt_expected);
      }
      //! Apply the lgoics, ie update:
      //! Note: in [”elow] we expect the local_resultMatrix to have been defined, a result-macris which is used to 'ensure' an voerlap wrt. "listOf_inlineResults" and "resultMatrix"



      // printf("\t\t iterate through nrows_to_evaluate=%u/%u, nrows_outer_to_evaluate=%u/%u, where ncols=%u, at [%s]:%s:%d\n", nrows_to_evaluate, nrows, nrows_outer_to_evaluate, nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
      //debug_cnt_resultCells_updated = 0; //! ie, reset.  
#define __localConfig_calledFrom_allAgainstAll 1
#include "func_template__postProcess__completeCorrelationComputation_squaredDistanceMatrix.c"
	/* { */
	/*   const uint cnt_expected = (nrows_to_evaluate * nrows_outer_to_evaluate); */
	/*   printf("## debug_cnt-evaluated=%u, cnt-expected=%u, at %s:%d\n", debug_cnt_resultCells_updated, cnt_expected, __FILE__, __LINE__); */
	/*   assert_possibleOverhead(debug_cnt_resultCells_updated == cnt_expected); */
	/* } */
#undef __localConfig_calledFrom_allAgainstAll
      //printf("\t\t at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

#if(__config__legacyFunction_useExplictPostProcessing_subsetOf_dataTypes == 1) //! then we use an arppaoch whcih (a) is only partially supported and (b) which is expected to result in a slwodown <-- inlcuded in order to test the time-effect of such a 'post-step' (oekseth, 06. okt. 2016)
      if(typeOf_metric_correlation != e_typeOf_metric_correlation_pearson_undef) {
	  
	// FIXME[perofrmance]: add a perofmrance-step evaluating this 'case'.

	//printf("\t\t at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

	// FIXME[assert]: given lgoical compelxity of [”elow] write a corretness-test when we are satsified with perofrmance ... ie, combine correctness-tests with (A) re-structing of code and (b) 'mving to the "cluster.c" file.'.
	assert(!resultMatrix || needTo_useMask_evaluation); //! ie, as this call is otherwise assicated with an overehad, ie, would be 'test to perofrm after we have copmtued the results'
	//! Comptue teh sum of chunks for the case where all valeus are of interst.
	__computeAllAgainstAll_get_sumsFor_chunks(ncols, data1, data2, mask1, mask2, weight, resultMatrix, chunkSize_index1, chunkSize_index2, index1, index2, listOf_inlineResults, tweight, typeOf_metric_correlation, needTo_useMask_evaluation);
      }
//printf("at %s:%d\n", __FILE__, __LINE__);
#endif

//printf("at %s:%d\n", __FILE__, __LINE__);

      s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)s_inlinePostProcess;
#if(__config__funcTemplateAllAgainstAll__inlinePostProcesType == macro__e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights) 
//printf("at %s:%d\n", __FILE__, __LINE__);
#include "correlation__codeStub__allAgainstAllPostProcess__logSum.c"

#elif(__config__funcTemplateAllAgainstAll__inlinePostProcesType == macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min) 
  #include "correlation__codeStub__allAgainstAllPostProcess__minVal.c"
//printf("at %s:%d\n", __FILE__, __LINE__);

#elif(__config__funcTemplateAllAgainstAll__inlinePostProcesType == macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max) 
//printf("at %s:%d\n", __FILE__, __LINE__);

  #include "correlation__codeStub__allAgainstAllPostProcess__maxVal.c"
#elif(__config__funcTemplateAllAgainstAll__inlinePostProcesType == macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum) 
//printf("at %s:%d\n", __FILE__, __LINE__);

#ifndef NDEBUG
if(listOf_inlineResults) { //!  then we are intested in validtaitng that all 'cells' are of interest, ie, to avoid the perofmrance-peantliy assited to 'if-breanc-testing of the latter (oekseth, 06. aug. 2017).
  for(uint local_index1 = 0; local_index1 < chunkSize_index1; local_index1++) {
    assert(listOf_inlineResults[local_index1]);
    for(uint local_index2 = 0; local_index2 < chunkSize_index2; local_index2++) {
      const t_float score = listOf_inlineResults[local_index1][local_index2];
      assert(isOf_interest(score)); 
      assert(isnan(score) == false);
      assert(isinf(score) == false);
    }
  }
 }
#endif
assert(config_allAgainstAll);
//printf("at %s:%d\n", __FILE__; __LINE__);
if(config_allAgainstAll->sparseDataResult == NULL) { //! then we update the sparse-data-set with the result (oekseth, 06. aug. 2017).
  // } else {
//! The call:
  #include "correlation__codeStub__allAgainstAllPostProcess__sum.c"
 }
#elif(__config__funcTemplateAllAgainstAll__inlinePostProcesType == macro__e_allAgainstAll_SIMD_inlinePostProcess_nonOptimizedEvaluateAllCases) 
  #include "correlation__codeStub__allAgainstAllPostProcess__evalauteAllCases.c"
#elif( (__config__funcTemplateAllAgainstAll__inlinePostProcesType == macro__e_allAgainstAll_SIMD_inlinePostProcess_undef)  || (__config__funcTemplateAllAgainstAll__inlinePostProcesType == macro__e_allAgainstAll_SIMD_inlinePostProcess_none_storeIntermediateResults_forMPI) )
      ; //! then we 'inetntioanlly' do ntohing, ie, as we then assume that the restuls are Not to be post-processed.
      // #elif(__config__funcTemplateAllAgainstAll__inlinePostProcesType == macro__e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_) 
#else
      #error "Did not expect this case, ie, please update your calls"
#endif //! else we assume the user is itnerested in 'keeping the data for their own use'.
//printf("at %s:%d\n", __FILE__, __LINE__);
if(config_allAgainstAll->sparseDataResult != NULL) { //! then we update the sparse-data-set with the result (oekseth, 06. aug. 2017).
  const bool isLast_columnForBlock = ( (cnt_index2 +1 ) == tile_r2_symmetric.cntBlocks );
  if(false) {printf("\t\t(info)\t\t block[%u][%u] where isLastBlock='%s', at %s:%d\n", cnt_index1, cnt_index2, (isLast_columnForBlock) ? "true" : "false", __FILE__, __LINE__);}
  apply__s_kt_list_2d_kvPair_filterConfig_t(config_allAgainstAll->sparseDataResult, listOf_inlineResults, globalStartPos_head, globalStartPos_tail, chunkSize_index1, chunkSize_index2, isLast_columnForBlock, globalProp__dataSetIsSymmeric);
 }


      //! **************************************************************************'
#endif //! endif(__config__funcTemplateAllAgainstAll__storeIntermeidateResults == 0)
