/* void DBSCAN::run_cluster()  { */
/*   ClusterId cid = 1; */
/*   // foreach pid */
/*   for (PointId pid = 0; pid < _ps.size(); pid++) */
/*     { */
/*       // not already visited */
/*       if (!_visited[pid]){   */
	
/* 	_visited[pid] = true; */
	
/* 	// get the neighbors */
/* 	Neighbors ne = findNeighbors(pid, _eps); */
	
/* 	  // not enough support -> mark as noice */
/* 	if (ne.size() < _minPts) {  _noise[pid] = true; }  */
/* 	else { */
/* 	  // std::cout << "Point i=" << pid << " can be expanded " << std::endl;// = true; */
	      
/* 	    // Add p to current cluster */
	    
/* 	    Cluster c;              // a new cluster */
/* 	    c.push_back(pid);   	// assign pid to cluster */
/* 	    _pointId_to_clusterId[pid]=cid; */

/* 	      // go to neighbors */
/* 	      for (unsigned int i = 0; i < ne.size(); i++) { */
/* 		  PointId nPid = ne[i]; */
/* 		  // not already visited */
/* 		  if (!_visited[nPid]) { */
/* 		      _visited[nPid] = true; */

/* 		      // go to neighbors */
/* 		      Neighbors ne1 = findNeighbors(nPid, _eps); */
/* 		      // enough support */
/* 		      if (ne1.size() >= _minPts) */
/* 			{ */
/* 			  std::cout << "\t Expanding to pid=" << nPid << std::endl;	 */
/* 			  // join */
/* 			  BOOST_FOREACH(Neighbors::value_type n1, ne1) */
/* 			    { */
/* 			      // join neighbord */
/* 			      ne.push_back(n1);  */
/* 			      std::cerr << "\tPushback pid=" << n1 << std::endl; */
/* 			    } */
/* 			  std::cout << std::endl; */
/* 			} */
/* 		    } */

/* 		  // not already assigned to a cluster */
/* 		  if (!_pointId_to_clusterId[nPid]) */
/* 		    { */
/* 		      std::cout << "\tadding pid=" << nPid << std::endl; */
/* 		      c.push_back(nPid); */
/* 		      _pointId_to_clusterId[nPid]=cid; */
/* 		    } */
/* 		} */

/* 	      _clusters.push_back(c); */
/* 	      cid++; */
/* 	    } */
/* 	} // if (!visited */
/*       } // for */
/*   } */


/* void Clusters__static__randomInit	(Points & ps, unsigned int dims,  */
/* 		 unsigned int num_points) */
/* { */
/*   for (unsigned int j = 0; j < num_points; j++) */
/*     { */
/*       Point p(dims); */
/*       for (unsigned int i = 0; i < dims; i++)	 */
/* 	{ */
/* 	  p(i) = (-1.0 + rand() * (2.0) / RAND_MAX); */
/* 	  //			std::cout << p(i) << ' '; */
/* 	} */
/*       ps.push_back(p); */
/*       //		std::cout << std::endl; */
/*     } */
/* } */


/* // assign each point to a new cluster */
/* void uniformPartition(); */
/* double getDistance(const VEC_T v1, const VEC_T v2) */
/* { */
/*   return 1.0 - (inner_prod(v1, v2) / (norm_2(v1) * norm_2(v2))); */
/* }; */
/* double distance(typename Distance_Policy::vector_type x, typename Distance_Policy::vector_type y) */
/* { */
/*   return getDistance(x, y); */
/* }; */


// compute similarity
//template <typename Distance_type>
/* void clusters__computeSimilarity(Distance_type & d) */
/* { */
/*   unsigned int size = _ps.size(); */
/*   _sim.resize(size, size, false); */
/*   for (unsigned int i=0; i < size; i++) */
/*     { */
/*       for (unsigned int j=i+1; j < size; j++) */
/* 	{ */
/* 	  _sim(j, i) = _sim(i, j) = d.similarity(_ps[i], _ps[j]); */
/* 	  //				std::cout << "(" << i << ", " << j << ")=" << _sim(i, j) << " "; */
/* 	} */
/*       std::cout << std::endl; */
/*     } */
/* }; */


// assign each point to a new cluster
/* void Clusters__uniformPartition() */
/* { */
/*   PointId pid = 0; */
/*   ClusterId cid = 0; */
/*   BOOST_FOREACH(Point p, _ps) */
/*     { */
/*       // create a new cluster for this current point */
/*       Cluster c; */
/*       c.push_back(pid++); */
/*       _clusters.push_back(c); */
/*       _pointId_to_clusterId.push_back(cid++);			 */
/*     } */
/* } */

	
//
// findNeighbors(PointId pid, double threshold)
//
// this can be implemented with reduced complexity by using R+trees
//
// findNeighbors(PointId pid, double threshold) 	
/* Neighbors Clusters__findNeighbors(PointId pid, double threshold) { */
/*   Neighbors ne;   */
/*   for (unsigned int j=0; j < _sim.size1(); j++) { */
/*     if 	((pid != j ) && (_sim(pid, j)) > threshold) { */
/*       ne.push_back(j); */
/*       //std::cout << "sim(" << pid  << "," << j << ")" << _sim(pid, j) << ">" << threshold << std::endl; */
/*     } */
/*   } */
/*   return ne; */
/* }; */


/* // single point output */
/* std::ostream& operator<<(std::ostream& o, const Point& p) */
/* { */
/*   o << "{ "; */
/*   BOOST_FOREACH(Point::value_type x, p) */
/*     { */
/*       o << " " << x; */
/*     } */
/*   o << " }, "; */
  
/*   return o; */
/* } */

/* // single cluster output */
/* std::ostream& operator<<(std::ostream& o, const Cluster& c) */
/* { */
/*   o << "[ "; */
/*   BOOST_FOREACH(PointId pid, c) */
/*     { */
/*       o << " " << pid; */
/*     } */
/*   o << " ]"; */
  
/*   return o; */
/* } */

/* // clusters output */
/* std::ostream& operator<<(std::ostream& o, const Clusters& cs) */
/* { */
/*   ClusterId cid = 1; */
/*   BOOST_FOREACH(Cluster c, cs._clusters) */
/*     { */
/*       o << "c(" << cid++ << ")="; */
      
/*       BOOST_FOREACH(PointId pid, c) */
/* 	{ */
/* 	  o << cs._ps[pid]; */
/* 	} */
/*       o << std::endl; */
/*     } */
/*   return o; */
/* } */

/* int main() { */
/* 	Clustering::Points ps; */

/* 	// random init points dataset (dims, points) */
/* 	Clusters__static__randomInit(ps, 10, 100);    */

/* 	// init: sim threshold, minPts */
/* 	Clustering::DBSCAN clusters(ps, /\*eps=*\/0.8, /\*minPts=*\/2);  */

/* 	// uniform distribution dataset */
/* 	//	clusters.uniformPartition();           */

/* 	// build similarity  matrix */
/* 	Distance<Cosine<Clustering::Point> > d; */
/* 	clusters.clusters__computeSimilarity(d);      */

/* 	// run clustering */
/* 	clusters.run_cluster(); */

/* 	std::cout << clusters; */

/* 	int test; */
/* 	std::cin >> test; */
/* } */
