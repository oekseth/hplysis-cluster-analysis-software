/* #ifndef metricMacro_absoluteDifference_h */
/* #define metricMacro_absoluteDifference_h */


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_absoluteDifference
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "absoluteDifference".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/


// FIXME: write perofmrance-teets and correnctess-tests for tehse cases/metrics



//! Compute the correlations-core for: Soerensen
#define correlation_macros__distanceMeasures__absoluteDifferenc__Sorensen(term1, term2, obj) ({ \
  const t_float diff_numerator = mathLib_float_abs(term1 - term2); \
  const t_float sum_denumerator = (term1 + term2);		   \
  /*! Update the input_result-object: */ \
  obj.numerator += diff_numerator; obj.denumerator += sum_denumerator; \
    })
//#define correlation_macros__distanceMeasures__absoluteDifferenc__Sorensen__postProcess(obj, ncolsAndWeight, ncolsAndWeight_inverse) ({ metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide(obj, 
//    t_float input_result = 0; if( (obj.numerator != 0) && (obj.denumerator != 0) ) {input_result = obj.numerator / obj.denumerator;} input_result;})

#define correlation_macros__distanceMeasures__absoluteDifferenc__Sorensen__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE diff_numerator = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE sum_denumerator = VECTOR_FLOAT_ADD(term1, term2); \
    /*! Update the input_result-object: */					\
    obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, diff_numerator); obj.numerator = VECTOR_FLOAT_ADD(obj.denumerator, sum_denumerator); \
})

//! @return the correlations-core for: Gower
#define correlation_macros__distanceMeasures__absoluteDifferenc__Gower(term1, term2) ({const t_float ret_val = correlation_macros__distanceMeasures__chebychev(term1, term2); ret_val;})
#define correlation_macros__distanceMeasures__absoluteDifferenc__Gower__postProcess(value, ncolsAndWeight, ncolsAndWeight_inverse) ({const t_float input_result = (value != 0) ? value * ncolsAndWeight_inverse : metricMacro__constants__defaultValue__postProcess__noMatch; input_result; }) //! ie, adjsut by the overall signicance of the terms invovled.
#define correlation_macros__distanceMeasures__absoluteDifferenc__Gower__SSE(term1, term2) ({const VECTOR_FLOAT_TYPE vec_diff = correlation_macros__distanceMeasures__chebychev__SSE(term1, term2); vec_diff;})

//! @brief compute the correlations-core for: Soergel
#define correlation_macros__distanceMeasures__absoluteDifferenc__Soergel(term1, term2, obj) ({ \
  const t_float diff_numerator = mathLib_float_abs(term1 - term2); \
  const t_float sum_denumerator = macro_max(term1, term2);		\
/*! Update the input_result-object: */				       \
  obj.numerator += diff_numerator; obj.denumerator += sum_denumerator; \
})
#define correlation_macros__distanceMeasures__absoluteDifferenc__Soergel__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE diff_numerator = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE sum_denumerator = VECTOR_FLOAT_MIN(term1, term2); \
      /*! Update the input_result-object: */					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, diff_numerator); obj.numerator = VECTOR_FLOAT_ADD(obj.denumerator, sum_denumerator); \
    })

//! @brief compute the correlations-core for:Kulczynski
#define correlation_macros__distanceMeasures__absoluteDifferenc__Kulczynski(term1, term2, obj) ({ \
	  const t_float diff_numerator = mathLib_float_abs(term1 - term2); \
	  const t_float sum_denumerator = macro_min(term1, term2);	\
	  /*! Update the input_result-object: */				\
	  obj.numerator += diff_numerator; obj.denumerator += sum_denumerator; \
	})
#define correlation_macros__distanceMeasures__absoluteDifferenc__Kulczynski__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE diff_numerator = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE sum_denumerator = VECTOR_FLOAT_MIN(term1, term2); \
      /*! Update the input_result-object: */					\
    obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, diff_numerator); obj.numerator = VECTOR_FLOAT_ADD(obj.denumerator, sum_denumerator); \
    })

//! @return the correlations-core for: Canberra
#define correlation_macros__distanceMeasures__absoluteDifferenc__Canberra(term1, term2) ({ \
    const t_float diff = mathLib_float_abs(term1 - term2);	\
    const t_float denumerator = (term1 + term2);		   \
    const t_float input_result_local = (denumerator != 0) ? diff / denumerator : 0;	\
    input_result_local;}) //! ie, return the input_result
#define correlation_macros__distanceMeasures__absoluteDifferenc__Canberra__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1, term2); \
      VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_DIV(diff,  denumerator); \
      /*Handle dividye-by-zero-cases: */ \
      input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);	\
      input_result;})

//! @return the correlations-core for: Lorentzian
// FIXME[performacne] consider using a look-up-table for "ln" ...
#define correlation_macros__distanceMeasures__absoluteDifferenc__Lorentzian(term1, term2) ({ \
  const t_float diff = 1 + mathLib_float_abs(term1 - term2); \
  const t_float input_result = mathLib_float_log(diff); \
  input_result;}) //! ie, return.
#define correlation_macros__distanceMeasures__absoluteDifferenc__Lorentzian__SSE(term1, term2) ({ \
  VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
  vec_diff = VECTOR_FLOAT_ADD(VECTOR_FLOAT_SET1(1), vec_diff); \
  VECTOR_FLOAT_TYPE vec_input_result = VECTOR_FLOAT_LOG(vec_diff); \
  vec_input_result;}) //! ie, return


//! -------------------------------------------------------------------------
//! -------------------------------------------------------------------------
//! -------------------------------------------------------------------------
//!
//! *********** new correlaiton-emtrics defined by (oekseth, 06. setp. 2016) ************

//! @return the correlations-core for: oekseth-mean-deviation
#define correlation_macros__distanceMeasures__oekseth_forMasks_mean(term1, term2, obj) ({\
  obj.numerator += term1; obj.denumerator += term2; })
#define correlation_macros__distanceMeasures__oekseth_forMasks_mean__postProcess(obj) ({ \
      metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(obj.numerator, obj.denumerator);})
#define correlation_macros__distanceMeasures__oekseth_forMasks_mean__SSE(term1, term2, obj) ({\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, term1);			\
      obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, term2);		\
    })

//! @return the correlations-core for: oekseth-stanard-devaition between compelx data-sets.
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow(term1, term2, obj, power) ({ \
      obj.numerator += term1;						\
      obj.denumerator += term2;						\
      obj.denumeratorLimitedTo_row1 += mathLib_float_pow_abs(term1, power); \
      obj.denumeratorLimitedTo_row2 += mathLib_float_pow_abs(term2, power); \
    })
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow__SSE(term1, term2, obj, power) ({ \
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, term1);		\
      obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, term2);	\
      obj.denumeratorLimitedTo_row1 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row1, VECTOR_FLOAT_POWER_abs(term1, power)); \
      obj.denumeratorLimitedTo_row2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row2, VECTOR_FLOAT_POWER_abs(term2, power)); \
    })
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow__postProcess(obj, power) ({ \
  const t_float ncolsAndWeight_inverse = (obj.cnt_interesting_cells != 0) ? 1/(t_float)obj.cnt_interesting_cells : 0; \
  const t_float numerator = mathLib_float_pow_abs(obj.numerator, power) + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row1); \
  const t_float denumerator = mathLib_float_pow_abs(obj.denumerator, power) + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row2); \
  metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(numerator, denumerator); }) //! ie, return.


//! @return the correlations-core for: Oekseth_ForMasks_standardDeviation_Pow
// FIXEM: consider to use a pre-allcoated talbe of pow-values for [”elow]
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow_zeroOne(term1, term2, obj, power) ({ \
      obj.numerator += term1;						\
      obj.denumerator += term2;						\
      obj.denumeratorLimitedTo_row1 += mathLib_float_pow_abs(term1, power); \
      obj.denumeratorLimitedTo_row2 += mathLib_float_pow_abs(term2, power); \
    })
//! @return the correlations-core for: Oekseth_ForMasks_standardDeviation_Pow
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow_zeroOne__SSE(term1, term2, obj, power) ({ \
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, term1);		\
      obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, term2);	\
      obj.denumeratorLimitedTo_row1 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row1, VECTOR_FLOAT_fastpow_0_1(term1, power)); \
      obj.denumeratorLimitedTo_row2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row2, VECTOR_FLOAT_fastpow_0_1(term2, power)); \
    }) //! ie, return the input_result:

//      printf("(before)terms=(%f, %f), result=(%f, %f) row1=(%f, %f) (in \"metricMacro_absoluteDifference.h\"), at %s:%d\n", term1, term2, obj.numerator, obj.denumerator, obj.denumeratorLimitedTo_row1, obj.denumeratorLimitedTo_row2, __FILE__, __LINE__); 
//      printf("(after)terms=(%f, %f), result=(%f, %f) row1=(%f, %f) (in \"metricMacro_absoluteDifference.h\"), at %s:%d\n", term1, term2, obj.numerator, obj.denumerator, obj.denumeratorLimitedTo_row1, obj.denumeratorLimitedTo_row2, __FILE__, __LINE__); 
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2(term1, term2, obj) ({ \
      obj.numerator += term1;						\
      obj.denumerator += term2;						\
      obj.denumeratorLimitedTo_row1 += macro_mul(term1, term1); \
      obj.denumeratorLimitedTo_row2 += macro_mul(term2, term2); \
    }) //! ie, return the input_result:
//! @return the correlations-core for: Oekseth_ForMasks_standardDeviation_Pow
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2__SSE(term1, term2, obj) ({ \
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, term1);		\
      obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, term2);	\
      obj.denumeratorLimitedTo_row1 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row1, VECTOR_FLOAT_MUL(term1, term1)); \
      obj.denumeratorLimitedTo_row2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row2, VECTOR_FLOAT_MUL(term2, term2)); \
    }) //! ie, return the input_result:
//  printf("ncolsAndWeight_inverse=%f, numerator=%f, denumerator=%f (in \"metricMacro_absoluteDifference.h\"), at %s:%d\n", ncolsAndWeight_inverse, numerator, denumerator, __FILE__, __LINE__); 
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow2__postProcess(obj_local) ({ \
  const t_float ncolsAndWeight_inverse = (obj_local.cnt_interesting_cells != 0) ? 1/(t_float)obj_local.cnt_interesting_cells : 0; \
  const t_float numerator   = macro_mul(obj_local.numerator,   obj_local.numerator)   + (ncolsAndWeight_inverse*obj_local.denumeratorLimitedTo_row1); \
  const t_float denumerator = macro_mul(obj_local.denumerator, obj_local.denumerator) + (ncolsAndWeight_inverse*obj_local.denumeratorLimitedTo_row2); \
  metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(numerator, denumerator); }) //! ie, return.
//! -----------
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3(term1, term2, obj) ({ \
      obj.numerator = macro_pluss(obj.numerator, term1);		\
      obj.denumerator = macro_pluss(obj.denumerator, term2);		\
      obj.denumeratorLimitedTo_row1 += macro_mul(macro_mul(term1, term1), term1); \
      obj.denumeratorLimitedTo_row2 += macro_mul(macro_mul(term2, term2), term2); \
    }) //! ie, return the input_result:

#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3__postProcess(obj) ({ \
  const t_float ncolsAndWeight_inverse = (obj.cnt_interesting_cells != 0) ? 1/(t_float)obj.cnt_interesting_cells : 0; \
  const t_float numerator   = macro_mul(obj.numerator,   obj.numerator)   + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row1); \
  const t_float denumerator = macro_mul(obj.denumerator, obj.denumerator) + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row2); \
  metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(numerator, denumerator); }) //! ie, return.
//! -----------
/* #define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__postProcess(obj, ncolsAndWeight, ncolsAndWeight_inverse) ({ \ */
/*       const t_float numerator   = macro_mul(macro_mul(obj.numerator,   obj.numerator), obj.numerator)     + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row1); \ */
/*       const t_float denumerator = macro_mul(macro_mul(obj.denumerator, obj.denumerator), obj.denumerator) + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row2); \ */
/*       metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(numerator, denumerator); }) //! ie, return. */
//! -----------
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4(term1, term2, obj) ({ \
  obj.numerator = macro_pluss(obj.numerator, term1);			\
  obj.denumerator = macro_pluss(obj.denumerator, term2);		\
  obj.denumeratorLimitedTo_row1 += macro_mul(macro_mul(macro_mul(term1, term1), term1), term1); \
  obj.denumeratorLimitedTo_row2 += macro_mul(macro_mul(macro_mul(term2, term2), term2), term2); \
    }) //! ie, return the input_result:
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__old(term1, term2, obj) ({ \
  assert_possibleOverhead(isinf(term1)  == false); \
  assert_possibleOverhead(isinf(term2)  == false); \
  assert_possibleOverhead(isnan(term1)  == false); \
  assert_possibleOverhead(isnan(term2)  == false); \
  obj.numerator = macro_pluss(obj.numerator, term1);			\
  obj.denumerator = macro_pluss(obj.denumerator, term2);		\
  obj.denumeratorLimitedTo_row1 += macro_mul(macro_mul(macro_mul(term1, term1), term1), term1); \
  obj.denumeratorLimitedTo_row2 += macro_mul(macro_mul(macro_mul(term2, term2), term2), term2); \
    }) //! ie, return the input_result:


#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__postProcess(obj) ({ \
  const t_float ncolsAndWeight_inverse = (obj.cnt_interesting_cells != 0) ? 1/(t_float)obj.cnt_interesting_cells : 0; \
    /*! Then conitneu with our logics (oesketh, 06. mar. 2017): */ \
    const t_float numerator_p2 = ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row1;		\
    t_float numerator_p1 = macro_mul(macro_mul(macro_mul(obj.numerator,   obj.numerator), obj.numerator), obj.numerator); \
    t_float ret_result = T_FLOAT_MAX; /*! which is 'introduced' iot. handle the 'posislbity' of a (T_FLOAT_MAX-m) * (T_FLOAT_MAX-k) muliplciaton-case (oekseth, 06. amr. 2017)*/ \
    if(isinf(numerator_p1) == false) { \
    /*! Compute: */ \
    const t_float numerator   = numerator_p1       + numerator_p2; \
    const t_float denumerator = macro_mul(macro_mul(macro_mul(obj.denumerator, obj.denumerator), obj.denumerator), obj.denumerator) + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row2); \
    /*! Compute: */ \
    ret_result = metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(numerator, denumerator); \
    } ret_result; }) //! ie, return.

#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__postProcess_old(obj) ({ \
    /*! What we expect: != NAN (oekseth, 06. mar. 2017) */ \
    /*! Compute: (oekseth, 06. mar. 2017) */ \
  const t_float ncolsAndWeight_inverse = (obj.cnt_interesting_cells != 0) ? 1/(t_float)obj.cnt_interesting_cells : 0; \
    /*! What we expect: != NAN (oekseth, 06. mar. 2017) */ \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(ncolsAndWeight_inverse)        == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(obj.denumeratorLimitedTo_row1) == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(obj.denumeratorLimitedTo_row2) == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(obj.numerator)                 == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(obj.denumerator)               == false); \
    /*! What we expect: != INFINITY (oekseth, 06. mar. 2017) */ \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(ncolsAndWeight_inverse)        == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(obj.denumeratorLimitedTo_row1) == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(obj.denumeratorLimitedTo_row2) == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(obj.numerator)                 == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(obj.denumerator)               == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(obj.numerator   != T_FLOAT_MAX); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(obj.denumerator != T_FLOAT_MAX); \
    /*! Then conitneu with our logics (oesketh, 06. mar. 2017): */ \
    const t_float numerator_p2 = ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row1;		\
    t_float numerator_p1 = macro_mul(macro_mul(macro_mul(obj.numerator,   obj.numerator), obj.numerator), obj.numerator); \
    t_float ret_result = T_FLOAT_MAX; /*! which is 'introduced' iot. handle the 'posislbity' of a (T_FLOAT_MAX-m) * (T_FLOAT_MAX-k) muliplciaton-case (oekseth, 06. amr. 2017)*/ \
    if(isinf(numerator_p1) == false) { \
    /*! What we expect (oekseth, 06. mar. 2017) */			\
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(numerator_p1)   == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(numerator_p2)   == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(numerator_p1)   == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(numerator_p2)   == false); \
    /*! Compute: */ \
    const t_float numerator   = numerator_p1       + numerator_p2; \
    const t_float denumerator = macro_mul(macro_mul(macro_mul(obj.denumerator, obj.denumerator), obj.denumerator), obj.denumerator) + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row2); \
    /*! What we expect (oekseth, 06. mar. 2017) */ \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(numerator)   == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(denumerator) == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(numerator)   == false); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(denumerator) == false); \
    /*! Compute: */ \
    ret_result = metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(numerator, denumerator); \
    assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(ret_result) == false); \
    } ret_result; }) //! ie, return.
//! -----------
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5(term1, term2, obj) ({ \
      obj.numerator = macro_pluss(obj.numerator, term1);		\
      obj.denumerator = macro_pluss(obj.denumerator, term2);		\
      obj.denumeratorLimitedTo_row1 += macro_mul(macro_mul(macro_mul(macro_mul(term1, term1), term1), term1), term1); \
      obj.denumeratorLimitedTo_row2 += macro_mul(macro_mul(macro_mul(macro_mul(term2, term2), term2), term2), term2); \
    }) //! ie, return the input_result:
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5__postProcess(obj) ({ \
  const t_float ncolsAndWeight_inverse = (obj.cnt_interesting_cells != 0) ? 1/(t_float)obj.cnt_interesting_cells : 0; \
      const t_float numerator   = macro_mul(macro_mul(macro_mul(macro_mul(obj.numerator,   obj.numerator), obj.numerator), obj.numerator), obj.numerator)       + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row1); \
      const t_float denumerator = macro_mul(macro_mul(macro_mul(macro_mul(obj.denumerator, obj.denumerator), obj.denumerator), obj.denumerator), obj.numerator) + (ncolsAndWeight_inverse*obj.denumeratorLimitedTo_row2); \
      t_float ret_result = T_FLOAT_MAX; /*! which is 'introduced' iot. handle the 'posislbity' of a (T_FLOAT_MAX-m) * (T_FLOAT_MAX-k) muliplciaton-case (oekseth, 06. amr. 2017)*/ \
      if(isinf(numerator) == false) {					\
	ret_result = metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide(numerator, denumerator);} ret_result; }) //! ie, return.
//! -----------------------------
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow3__SSE(term1, term2, obj) ({ \
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, term1);		\
      obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, term2);		\
      obj.denumeratorLimitedTo_row1 += VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(term1, term1), term1); \
      obj.denumeratorLimitedTo_row2 += VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(term2, term2), term2); \
    }) //! ie, return the input_result:
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow4__SSE(term1, term2, obj) ({ \
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, term1);		\
      obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, term2);		\
      obj.denumeratorLimitedTo_row1 += VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(term1, term1), term1), term1); \
      obj.denumeratorLimitedTo_row2 += VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(term2, term2), term2), term2); \
    }) //! ie, return the input_result:
//! -----------
#define correlation_macros__distanceMeasures__oekseth_forMasks_standardDeviation_pow5__SSE(term1, term2, obj) ({ \
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, term1);		\
      obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, term2);		\
      obj.denumeratorLimitedTo_row1 += VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(term1, term1), term1), term1), term1); \
      obj.denumeratorLimitedTo_row2 += VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(term2, term2), term2), term2), term2); \
    }) //! ie, return the input_result:


//#endif //! EOF
