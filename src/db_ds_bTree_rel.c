#include "db_ds_bTree_rel.h"
//! -------------------------------------------
//! -------------------------------------------
#define __Mi__localType__wrapper s_db_rel_t
#define __MiF__search__direct __M__searchPos__s_db_rel
//#define __MiF__cmp__equal __M__belowOrAbove__s_db_rel_t
#define __MiF__cmp__equal __M__isEqual__s_db_rel_t //__M__belowOrAbove__s_db_rel_t
#define __MF__verticesTo__includeIntoStack MF__resultsToUSe__afterSearch__s_db_searchNode_rel
#define __MF__updateResultStack(rel)({if(result_set) { uint searchId_head = UINT_MAX; uint searchId_tail = UINT_MAX; __MF__verticesTo__includeIntoStack(rel); if(searchId_head != UINT_MAX) {push__s_kt_set_1dsparse_t(result_set, searchId_head);} if( (searchId_tail != UINT_MAX) && (searchId_tail != searchId_head) ) { push__s_kt_set_1dsparse_t(result_set, searchId_tail); } }})
//! Include differnet 'value-combinations':
//! ---------------------------------------------
//! ---------------------------------------------
#define __Mi__localType s_db_node_typeOfKey_rel_case4_t
#define __M__constant __M__constant__case_4
// ---
#define __MiF__search__recursive __recursiveIns__s_db_node_typeOfKey_rel_case4_t
#define __MiF__search__slowEvaluateAll search__slowEvaluateAll__rel_case4_t
#define __MiF__free     free_mem__s_db_rel_case4_t
//#define __MiF__free_mem free_mem__s_db_node_typeOfKey_rel_case4_t
#define __MiF__search search__s_db_rel_case4_t
#define __MiF__insert inserts_db_rel_case4_t
#include "db_ds_bTree_keyValue__case32.c"
//! ---------------------------------------------
#define __Mi__localType s_db_node_typeOfKey_rel_case16_t
#define __M__constant __M__constant__case_16
// ---
#define __MiF__search__recursive __recursiveIns__s_db_node_typeOfKey_rel_case16_t
#define __MiF__search__slowEvaluateAll search__slowEvaluateAll__rel_case16_t
#define __MiF__free     free_mem__s_db_rel_case16_t
//#define __MiF__free_mem free_mem__s_db_node_typeOfKey_rel_case16_t
#define __MiF__search search__s_db_rel_case16_t
#define __MiF__insert inserts_db_rel_case16_t
#include "db_ds_bTree_keyValue__case32.c"
//! ---------------------------------------------
#define __Mi__localType s_db_node_typeOfKey_rel_case32_t
#define __M__constant __M__constant__case_32
// ---
#define __MiF__search__recursive __recursiveIns__s_db_node_typeOfKey_rel_case32_t
#define __MiF__search__slowEvaluateAll search__slowEvaluateAll__rel_case32_t
#define __MiF__free     free_mem__s_db_rel_case32_t
//#define __MiF__free_mem free_mem__s_db_node_typeOfKey_rel_case32_t
#define __MiF__search search__s_db_rel_case32_t
#define __MiF__insert inserts_db_rel_case32_t
#include "db_ds_bTree_keyValue__case32.c"
/* #include "db_ds_bTree_keyValue__case16.c" */
/* #include "db_ds_bTree_keyValue__case32.c" */
//! ---------------------------------------
/* #undef __Mi__localType__wrapper */
/* #undef __MiF__search__direct */
/* #undef __MiF__cmp__equal */
/* /\* #include "db_ds_bTree_rel__case4.c" *\/ */
/* /\* #include "db_ds_bTree_rel__case16.c" *\/ */
/* /\* #include "db_ds_bTree_rel__case32.c" *\/ */
/* //! ------------------------------------- */
#undef __Mi__localType__wrapper
#undef __MiF__search__direct
#undef __MiF__cmp__equal
#undef __MF__verticesTo__includeIntoStack
#undef __MF__updateResultStack

//! *************************************************************
//! *************************************************************
//! *************************************************************
//! @return an intlized 'verison' of our "s_db_ds_bTree_rel_t" struct.
s_db_ds_bTree_rel_t init__s_db_ds_bTree_rel_t(const e_db_ds_bTree_cntChildren_t typeOf_tree) {
  s_db_ds_bTree_rel_t self;
  self.root_case4 = NULL;
  self.root_case16 = NULL;
  self.root_case32 = NULL;
  self.typeOf_tree = typeOf_tree;
  //! @return 
  return self;
}
//! De-allcoates a given s_db_ds_bTree_rel_t tree.
void free__s_db_ds_bTree_rel_t(s_db_ds_bTree_rel_t *self) {
  if(self->typeOf_tree == e_db_ds_bTree_cntChildren_4) {
    if(self->root_case4) {
      free_mem__s_db_rel_case4_t(self->root_case4);
      free((self->root_case4));
    }
  } else if(self->typeOf_tree == e_db_ds_bTree_cntChildren_16) {
    if(self->root_case16) {
      free_mem__s_db_rel_case16_t(self->root_case16);
      free((self->root_case16));
    }
  } else if(self->typeOf_tree == e_db_ds_bTree_cntChildren_32) {
    if(self->root_case32) {
      free_mem__s_db_rel_case32_t(self->root_case32);      
      free((self->root_case32));
    }
  } else {
    assert(false); //! ie, then add support 'for this'.
  }
}
/**
   @brief insert a given "key" object into our struct (oekseth, 06. mar. 2017)
   @param <self> is the object to insert in
   @param <key> is the key to insert
   @return true upon success.
 **/
bool insert__s_db_ds_bTree_rel_t(s_db_ds_bTree_rel_t *self, const s_db_rel_t key) {
  s_kt_set_1dsparse_t *result_set = NULL;
  if(self->typeOf_tree == e_db_ds_bTree_cntChildren_4) {
    //if(self->root_case4) 
      {
	inserts_db_rel_case4_t(&(self->root_case4), key, result_set);
      return 1; //! ie, as the key 'was not found'.
    }
  } else if(self->typeOf_tree == e_db_ds_bTree_cntChildren_16) {
    //if(self->root_case16) 
      {
	inserts_db_rel_case16_t(&(self->root_case16), key, result_set);
      return 1; //! ie, as the key 'was not found'.
    }
  } else if(self->typeOf_tree == e_db_ds_bTree_cntChildren_32) {
    //if(self->root_case32)  
      {
	inserts_db_rel_case32_t(&(self->root_case32), key, result_set);
      return 1; //! ie, as the key 'was not found'.
    }
  } else {
    assert(false); //! ie, then add support 'for this'.
  }
  return 0; //! ie, as the key 'was not found'.
}
/**
   @brief idneitfy a "scalar_result" which 'corresponds' to "key" (oekseht, 06. mar. 2017)
   @param <self> is the object to search in
   @param <key> is the key to search for
   @param <scalar_result> is the idneitifed object
   @param <isTo_inpsectAll> which is to be set to tru if arbitrary or undefined 'searhc-kesy' are used.
   @param <result_set> which if Not set to null is 'used' to insert the idneitfed result-relationships.
  @return the number fo elments found.
 **/
uint find__s_db_ds_bTree_rel_t(const s_db_ds_bTree_rel_t *self, const s_db_rel_t key, s_db_rel_t *scalar_result, const bool isTo_inpsectAll__, s_kt_set_1dsparse_t *result_set)  {  
  bool isTo_inpsectAll = isTo_inpsectAll__;
  if(__M__hasArbitraryClauses__s_db_rel_t(key)) {isTo_inpsectAll = true;} //! ie, then 'override' the latter.

  if(self->typeOf_tree == e_db_ds_bTree_cntChildren_4) {
    if(self->root_case4) {
      if(isTo_inpsectAll) {
	return search__slowEvaluateAll__rel_case4_t(self->root_case4, key, scalar_result, result_set);	
      } else {
	return search__s_db_rel_case4_t(self->root_case4, key, scalar_result, result_set);
      }
    }
  } else if(self->typeOf_tree == e_db_ds_bTree_cntChildren_16) {
    if(self->root_case16) {
      if(isTo_inpsectAll) {
	return search__slowEvaluateAll__rel_case16_t(self->root_case16, key, scalar_result, result_set);	
      } else {
	return search__s_db_rel_case16_t(self->root_case16, key, scalar_result, result_set);
      }
    }
  } else if(self->typeOf_tree == e_db_ds_bTree_cntChildren_32) {
    if(self->root_case32) {
      if(isTo_inpsectAll) {
	return search__slowEvaluateAll__rel_case32_t(self->root_case32, key, scalar_result, result_set);	
      } else {
	return search__s_db_rel_case32_t(self->root_case32, key, scalar_result, result_set);
      }
    }
  } else {
    assert(false); //! ie, then add support 'for this'.
  }
  return 0; //! ie, as the key 'was not found'.
}

