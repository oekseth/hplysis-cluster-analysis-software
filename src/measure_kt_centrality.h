#ifndef measure_kt_centrality_h
#define measure_kt_centrality_h


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file measure_distribution_studentT
   @brief a test-framework for perofmrance-evlauation of 'baisc' probability-distributions
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/


//! the main-fucntionf ro this.
void test_measure_kt_centrality();


#endif //! EOF
