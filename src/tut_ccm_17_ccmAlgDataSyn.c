#include "hpLysis_api.h"
#include "alg_dbScan_brute.h"
#include "hp_ccm.h"
#include "hp_exportMatrix.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.


struct tut_ccm_17_ccmAlgDataSyn_algs {
  const char *str; int alg_id; uint niter; bool exploreAllSimMetrics;
};


//! @remarks this funciton explreos 20 different permutations of the disjoitn-aritcila-data
static void __tut_ccm_17__apply(const char *resultPrefix, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum, const bool onlyComputeFor_fixedInputMatrix) {
//static void __tut_ccm_17__apply(s_kt_matrix_setOf_t *mat_collection, s_kt_list_1d_string_t *arrOf_stringNames, const char *resultPrefix) {
#if false
  const uint map_algList_size = 6;
  struct tut_ccm_17_ccmAlgDataSyn_algs map_algList[map_algList_size] = {
    //    e_alg_dbScan_brute_linkedListSet,
    {"disjoint-hpCluster-direct", e_hpLysis_clusterAlg_disjoint, /*niter=*/100, false},
    {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
    {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
    {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/1000, false},
    {"miniBatch-100", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/100, false},
    {"miniBatch-1000", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/1000, false},
  };
#else
  const uint map_algList_size = 29;
  struct tut_ccm_17_ccmAlgDataSyn_algs map_algList[map_algList_size] = {
    //    e_alg_dbScan_brute_linkedListSet,
    {"disjoint-hpCluster-direct", e_hpLysis_clusterAlg_disjoint, /*niter=*/100, false},
    {"disjoint-DBSCAN-direct", e_hpLysis_clusterAlg_disjoint_DBSCAN, /*niter=*/100, false},
    {"disjoint-hpCluster-direct-mclPost", e_hpLysis_clusterAlg_disjoint_mclPostStrategy, /*niter=*/100, false},
    {"disjoint-DBSCAN-dynamic", e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, /*niter=*/100, false},
    {"disjoint-hpCluster-dynamic", e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds, /*niter=*/100, false},
    {"disjoint-hpCluster-dynamic-bestSim", e_hpLysis_clusterAlg_disjoint_ccm_hpCluster_dynamicThresholds, /*niter=*/100, true},
    {"disjoint-KD-hpCluster-direct", e_hpLysis_clusterAlg_disjoint_kdTree, /*niter=*/100, false},
    {"disjoint-KD-hpCluster-dynamic", e_hpLysis_clusterAlg_disjoint_kdTree_ccm_dynamicThreshold_NN, /*niter=*/100, false},
    {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
    {"k-means-10", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/10, false},
    {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
    {"k-means-100", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/100, false},
    {"k-means-500", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/500, false},
    {"k-means-500", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/500, false},        
    {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/1000, false},
    {"k-means-1000", e_hpLysis_clusterAlg_kCluster__AVG, /*niter=*/1000, false},
    {"k-rank-100", e_hpLysis_clusterAlg_kCluster__rank, /*niter=*/100, false},
    {"k-rank-100", e_hpLysis_clusterAlg_kCluster__rank, /*niter=*/100, false},
    {"k-medoid-100", e_hpLysis_clusterAlg_kCluster__medoid, /*niter=*/100, false},
    {"k-medoid-100", e_hpLysis_clusterAlg_kCluster__medoid, /*niter=*/100, false},
    {"miniBatch-10", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/10, false},
    {"miniBatch-10", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/10, false},
    {"miniBatch-100", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/100, false},
    {"miniBatch-1000", e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, /*niter=*/1000, false},
    {"HCA-single", e_hpLysis_clusterAlg_HCA_single, /*niter=*/100, false},
    {"HCA-centroid", e_hpLysis_clusterAlg_HCA_centroid, /*niter=*/100, false},
    {"HCA-max",  e_hpLysis_clusterAlg_HCA_max,/*niter=*/100, false},
    {"HCA-average", e_hpLysis_clusterAlg_HCA_average, /*niter=*/100, false},
    {"HCA-Kruskal",  e_hpLysis_clusterAlg_kruskal_hca, /*niter=*/100, false},
    // {"HCA-Kruskal-dynamic", e_hpLysis_clusterAlg_kruskal_fixed_and_CCM, /*niter=*/100, false},
    //!    {"disjoint-hpCluster-direct",  /*niter=*/100, false},
    //!    {, false},   
  };
#endif

  const uint arrOf_sim_ccmCorr_postProcess_size = 14;
  s_kt_correlationMetric_t arrOf_sim_ccmCorr_postProcess[arrOf_sim_ccmCorr_postProcess_size] = {
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_cityblock, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Clark, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_none), //! ie, Pearson
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute, e_kt_categoryOf_correaltionPreStep_none), //! ie, Perason w/eslibhed permtaution
      //!      Absolute difference::Canberra 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_absoluteDifference_Canberra, e_kt_categoryOf_correaltionPreStep_none),
      //!Fidelity::Hellinger:
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_fidelity_Hellinger, e_kt_categoryOf_correaltionPreStep_none),
      //!Downward mutaiblity::symmetric-max
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_downwardMutability_symmetricMax, e_kt_categoryOf_correaltionPreStep_none),
      //!Inner product
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_innerProduct_innerProduct, e_kt_categoryOf_correaltionPreStep_none),
      //! Squared distance for Euclid
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Euclid, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_shannon_JensenShannon, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_squared_Pearson, e_kt_categoryOf_correaltionPreStep_rank), //! ie, spearman.
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none),
      initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_MINE_mic, e_kt_categoryOf_correaltionPreStep_none),
      //! --- 
      //!      initAndReturn__s_kt_correlationMetric_t(, e_kt_categoryOf_correaltionPreStep_none),
    };
  //! Define the export-objecgt
  s_hp_exportMatrix_t obj_exportMatrix = init__s_hp_exportMatrix_t(arrOf_sim_ccmCorr_postProcess, arrOf_sim_ccmCorr_postProcess_size);  

  
  const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
  //  const uint cnt_cases = 2;
  //   const uint cnt_cases = 10;
  //  const uint cnt_clusterCases = 4;
  // const uint nrows_increase = 800; uint cnt_blocks = 2; 5; //20;
  //   const uint nrows_increase = 400; uint cnt_blocks = 2; //5; //20;
  //   assert(cnt_clusterCases <= cnt_blocks); //! ie, what we expect in below.
   //   const uint nrows_increase = 100; uint cnt_blocks = 10; //20;
  
   //   const uint nrows_increase = 400; uint cnt_blocks = 5; //20;
  // if(mat_collection && mat_collection->list_size) {cnt_blocks = mat_collection->list_size;}
  //  const uint nrows_increase = 1000; const uint cnt_blocks = 100;
  //  const uint nrows_increase = 1000; const uint cnt_blocks = 10;  
  //  const uint nrows_increase = 4000;
  //  const uint nrows_increase = 2000; //! 820s, 176x
  const uint nrows_increase = 100;
  //  const uint nrows_increase = 1000;
  //const uint nrows_increase = 500;
  const uint cnt_blocks = 10;  
  //!

  //! Allocate result-matrix:
  s_kt_matrix_t mat_resultCCM = initAndReturn__s_kt_matrix(cnt_blocks * cnt_cases * cnt_clusterCases, map_algList_size);
  s_kt_matrix_t mat_resultTime = initAndReturn__s_kt_matrix(cnt_blocks * cnt_cases * cnt_clusterCases, map_algList_size);

//! Iterate through cases wrt. matrix-cluster-seperation:
  uint dim_index = 0;
  for(uint case_id = 0; case_id < cnt_cases; case_id++) {
    printf("case_id=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
    //!
    //! Iterate through cases wrt. relative cluster-ocunt-inlfucne
    for(uint clusterCase = 0; clusterCase < cnt_clusterCases; clusterCase++) {
      //!
      //! Identify the configuraitons:
      const char *fileTag = "";	
      //! Construct a givne matrix: 
      for(uint block_id = 0; block_id < cnt_blocks; block_id++) {
	uint nrows = nrows_increase + (block_id * nrows_increase);
	assert(nrows != UINT_MAX);

	//! 
	//! 
	uint cnt_clusters = 2;
	if(clusterCase == 0) {
	} else if(clusterCase == 1) {
	  cnt_clusters = 2; 
	} else if(clusterCase == 2) {
	  cnt_clusters = nrows/4; 
	  assert(cnt_clusters > 0);
	  assert(cnt_clusters <= nrows);
	} else if(clusterCase == 3) {
	  cnt_clusters = nrows / 2; 
	  assert(cnt_clusters > 0);
	  assert(cnt_clusters <= nrows);
	} else {
	  assert(false); //! ie, as this option is Not supported
	}
	s_hp_clusterShapes_t obj_shape = setToEmptyAndReturn__s_hp_clusterShapes_t();
	/* s_kt_matrix_t mat_shallow = setToEmptyAndReturn__s_kt_matrix_t(); */
	/* if(mat_collection && mat_collection->list_size) { */
	/*   mat_shallow = mat_collection->list[block_id]; */
	/*   assert(arrOf_stringNames); */
	/*   assert(block_id < arrOf_stringNames->nrows); */
	/*   fileTag = arrOf_stringNames->nameOf_rows[block_id]; */
	/*   nrows = mat_shallow.nrows; */
	/* } */
	if(onlyComputeFor_fixedInputMatrix == false) 	{
	  
	  //! 
	  //! Wrap result-object into an s_kt_matrix_base object
	  s_kt_matrix_base vec_1 = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_shape.matrix.nrows, obj_shape.matrix.ncols, obj_shape.matrix.matrix);
	  //! ----------------------------------------------------------------------
	  //!
	  //!
	  //! Build a vertex-clsuter-membership-set:
	  if(case_id == 0) {
	    obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "linear_1_100_minMax";
	  } else if(case_id == 1)  {
	    obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/80, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "linear_1_80_minMax";
	  } else if(case_id == 2)  {
	    obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/40, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "linear_1_40_minMax";
	  } else if(case_id == 3)  {
	    obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/20, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "linear_1_20_minMax";
	  } else {
//! then we use a 'random case-seliont', where muliple 'calls' are sued to explroe differetn 'random calls'.
	    obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "random";
	  }
	}
	if(onlyComputeFor_fixedInputMatrix == false) {
	  //!
	  bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
	  assert(is_ok);    
	  { //! Write out the input-matrix: 
	    //! Find min--max (eg, to simplify generaiton of Latex heat-map tables):
	    t_float minMax[2] = {T_FLOAT_MAX, T_FLOAT_MIN_ABS};
	    for(uint i = 0; i < obj_shape.matrix.nrows; i++) {
	      for(uint j = 0; j < obj_shape.matrix.ncols; j++) {
		minMax[0] = macro_min(minMax[0], obj_shape.matrix.matrix[i][j]);
		minMax[1] = macro_max(minMax[1], obj_shape.matrix.matrix[i][j]);
	      }
	    }
	    //! Write out:
	    allocOnStack__char__sprintf(2000, str, "%s_tag_%s__cnt-clusters_%u__nrows_%u__range_%.1f_%.1f.tsv", resultPrefix, fileTag, cnt_clusters, nrows, minMax[0], minMax[1]);	  
	    const bool is_ok = export__singleCall__s_kt_matrix_t(&(obj_shape.matrix), str, NULL);
	    assert(is_ok);
	  }
	}
	{ //! Iterate through the algorithms:
	  { //! then update the column-header: 
	    allocOnStack__char__sprintf(2000, str, "tag:\"%s\";cnt-clusters=%u;nrows=%u", fileTag, cnt_clusters, nrows);
	    set_stringConst__s_kt_matrix(&mat_resultCCM, /*index=*/dim_index, str, /*addFor_column=*/false);
	    set_stringConst__s_kt_matrix(&mat_resultTime, /*index=*/dim_index, str, /*addFor_column=*/false);
	  }
	  //!
	  //! ITerate through the clusters: 
	  t_float time_alg_first = T_FLOAT_MAX;
	  for(uint alg_id = 0; alg_id < map_algList_size; alg_id++) {
	    //! Construct the default prefix: 
	    if(dim_index == 0) { //! then update the column-header: 
	      {
		allocOnStack__char__sprintf(2000, str, "%s", map_algList[alg_id].str);
		set_stringConst__s_kt_matrix(&mat_resultCCM, /*index=*/alg_id, str, /*addFor_column=*/true);
		set_stringConst__s_kt_matrix(&mat_resultTime, /*index=*/alg_id, str, /*addFor_column=*/true);
	      }
	      
	    }
	    s_kt_list_1d_uint_t clusters = setToEmpty__s_kt_list_1d_uint_t();	    
	    t_float time_result = 0; start_time_measurement();
	    { //! Compute clusters:
	      const e_hpLysis_clusterAlg_t alg_enum = (e_hpLysis_clusterAlg_t)map_algList[alg_id].alg_id;
	      fprintf(stderr, "\t Compute for algorithm:\"%s\", at %s:%d\n", map_algList[alg_id].str, __FILE__, __LINE__);
	      assert(alg_enum != e_hpLysis_clusterAlg_undef); //! was this would indicate an error.
	      //! Note: the resutls differes as different default cofniguraiton-settings are used. For details of the latter, see the "obj_hp.config.kdConfig" variable
	      s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	      // time_result = (t_float)end_time_measurement(/*msg=*/map_algList[alg_id].str, FLT_MAX);	      
	      //! 
	      //! Apply logics:
	      if(onlyComputeFor_fixedInputMatrix) {
		if(false) {
		  obj_hp.config.corrMetric_prior_use = false;  //! ie, use data-as-is.
		  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; //isAn__adjcencyMatrix;
		} else {
		  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false; //isAn__adjcencyMatrix;
		  obj_hp.config.corrMetric_prior.metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;		  
		  obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = false;
		}
		uint ncols = nrows;
		if(true) { //! then we explroe other alternatives:
		  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false; //isAn__adjcencyMatrix;
		  obj_hp.config.corrMetric_prior_use = true;  
		  ncols = 3;
		  //		  ncols = 10;
		  //		  ncols = 24;
		}
		//!
		s_kt_matrix_base_t mat_sample = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
		s_kt_matrix_t mat_shal = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_sample);
		//! 
		printf("\t\t Start: compute-cluster, at %s:%d\n", __FILE__, __LINE__);		
		time_result = 0; start_time_measurement();
		const bool is_ok = cluster__hpLysis_api(&obj_hp, alg_enum, &(mat_shal), /*nclusters=*/UINT_MAX, /*npass=*/map_algList[alg_id].niter);
		//! Stop measurements: 
		const t_float time_result_local = (t_float)end_time_measurement(/*msg=*/map_algList[alg_id].str, FLT_MAX);	      
		time_result = time_result_local; //! ie, updat ethe time-emasurement.
		printf("\t\t OK:    compute-cluster, at %s:%d\n", __FILE__, __LINE__);
		assert(is_ok);
		free__s_kt_matrix_base_t(&mat_sample);	       
		if(alg_id != 0) {
		  printf("\t\t Diff: %f\n", time_result / time_alg_first);
		} else {
		  time_alg_first = time_result;
		}
		printf("--------------------\n");
		mat_resultTime.matrix[dim_index][alg_id] = time_result;
	      } else {
		//!
		obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; //isAn__adjcencyMatrix;
		obj_hp.config.config__CCM__dynamicKMeans.ccm_enum = ccm_enum;
		obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
		// obj_hp.config.kdConfig.enum_id = self.enum_kdTree; ////! ie, the defualt enum-kd-type
		if(false) {
		  //! Note: below invovles additional default steps wrt. simliarty-metrics, hence the results may differ.
		  const e_kt_correlationFunction_t sim_pre = e_kt_correlationFunction_groupOf_minkowski_euclid;      
		  obj_hp.config.corrMetric_prior.metric_id = sim_pre; 
		  obj_hp.config.corrMetric_prior_use = true;
		}
		//! 
		printf("\t\t compute-cluster, at %s:%d\n", __FILE__, __LINE__);
		const bool is_ok = cluster__hpLysis_api(&obj_hp, alg_enum, &(obj_shape.matrix), /*nclusters=*/UINT_MAX, /*npass=*/map_algList[alg_id].niter);
		assert(is_ok);
	      }
	      clusters = getClusters__hpLysis_api(&obj_hp);
	      //!
	      //! 
	      if(!onlyComputeFor_fixedInputMatrix && map_algList[alg_id].exploreAllSimMetrics) { //! then a dynamic search is appleid for the different CCMs:
		const uint list_metrics_size = 4;
		const s_kt_correlationMetric_t list_metrics[list_metrics_size] = {
		  initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_cityblock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
		  initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_none),
		  initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_squared_Pearson, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank),
		  initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank)
		};
		uint _list_metrics_size = list_metrics_size;
		if(onlyComputeFor_fixedInputMatrix) {_list_metrics_size = 0;} //! ie, as we omit the time-consuming step of vluaitng simliarty-metrics.
		//! FIXME: cosnider to explroe for both min--max.
		t_float ccm_min = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, ccm_enum, NULL, NULL);
		for(uint i = 0; i < _list_metrics_size; i++) {
		  start_time_measurement();
		  s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
		  if(onlyComputeFor_fixedInputMatrix) {
		    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; // isAn__adjcencyMatrix=true; 
		  } else {
		    // FIXME: consider setting bleow param to 'false'.
		    obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; // isAn__adjcencyMatrix=true; 
		    obj_hp.config.config__CCM__dynamicKMeans.isTo_pply__CCM = true;
		    obj_hp.config.corrMetric_prior = list_metrics[i];
		  }
		  const bool is_ok = cluster__hpLysis_api(&obj_hp, alg_enum, &(obj_shape.matrix), /*nclusters=*/UINT_MAX, /*npass=*/map_algList[alg_id].niter);
		  assert(is_ok);
		  const t_float time_result_local = (t_float)end_time_measurement(/*msg=*/map_algList[alg_id].str, FLT_MAX);	      
		  if(onlyComputeFor_fixedInputMatrix) {
		    time_result = time_result_local; //! ie, updat ethe time-emasurement.
		  } else {
		    t_float ccm_local = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, ccm_enum, NULL, NULL);
		    if(ccm_min > ccm_local) { //! then select the current CCM:
		      ccm_min = ccm_local;
		      time_result = time_result_local; //! ie, updat ethe time-emasurement.
		      free__s_kt_list_1d_uint_t(&clusters);
		      clusters = getClusters__hpLysis_api(&obj_hp);
		    }
		  }
		  free__s_hpLysis_api_t(&obj_hp);
		}
	      }
	      //! Get the CCM-score:
	      // t_float ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, ccm_enum, NULL, NULL);
	      //! Get the clusters:
	      //! De-allocte:
	      free__s_hpLysis_api_t(&obj_hp);
	    }
	    {
	      assert(dim_index  < mat_resultTime.nrows);
	      assert(alg_id  < mat_resultTime.ncols);
	      mat_resultTime.matrix[dim_index][alg_id] = time_result;
	    }
	    //! Update timings:
	    if(onlyComputeFor_fixedInputMatrix == false)
	    { //! Compute the CCM:
	      t_float ccm_score = T_FLOAT_MAX;
	      s_kt_matrix_base_t mat_shallow_data = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&obj_shape.matrix);
	      if(clusters.list_size < mat_shallow_data.nrows) { //! then intialize the cluster-memeberships:
		s_kt_list_1d_uint_t clusters_new = init__s_kt_list_1d_uint_t(mat_shallow_data.nrows);
		{ //! Set each 'not-part-of-any-cluster' to their own clusters:
		  //! Note: the interptaiton is correct for some use-cases, though not all, hence explaisn why 'the asusmption' is not globally enforced.
		  uint max_clusterId = 0;
		  for(uint row_id = 0; row_id < clusters.list_size; row_id++) {
		    if(clusters.list[row_id] != UINT_MAX) {
		      max_clusterId = macro_max(max_clusterId, clusters.list[row_id]);
		    }
		  }
		  max_clusterId++;
		  for(uint row_id = clusters.list_size; row_id < mat_shallow_data.nrows; row_id++) {
		    clusters_new.list[row_id] = max_clusterId++;
		  }
		}
		//! Update the internal object:
		free__s_kt_list_1d_uint_t(&clusters);
		clusters = clusters_new;
	      }
	      const bool is_ok = ccm__singleMatrix__hp_ccm(ccm_enum, &mat_shallow_data, clusters.list, &ccm_score);
	      assert(is_ok);
	      if(is_ok) {
		assert(dim_index  < mat_resultCCM.nrows);
		assert(alg_id  < mat_resultCCM.ncols);
		mat_resultCCM.matrix[dim_index][alg_id] = ccm_score;
	      }
	    }
	    //! De-allocate: 
	    free__s_kt_list_1d_uint_t(&clusters);
	  }
	  //! and at this poin the algorithms are evaluated.
	  //! Increment the deimsnion-counter:
	  dim_index++;	    
	}
	// const uint cnt_clusters = case_id*config_clusterBase;
	//! *************************************************************************************** 
	//! 
	//! De-allocate: 
	free__s_hp_clusterShapes_t(&obj_shape);
      }
    }
  }
  //! -------------------------------------------
  //! -------------------------------------------
  { //! Write out 'raw' matrix:
    {
      allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_features_time.tsv", resultPrefix);
      export__singleCall__s_kt_matrix_t(&mat_resultTime, resultPrefix_local, NULL);
    }
    {
      allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_features_time_transp.tsv", resultPrefix);
      export__singleCall_firstTranspose__s_kt_matrix_t(&mat_resultTime, resultPrefix_local, NULL);
    }    
  }
  {//! Write out proepteis wrt. latter:
    //! Note: [alg]x[cluster-memberships], and (raw,  rank)[alg]x[CCM-scores]
    //! Buidl a file-path: 
    allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_topology_alg_ccm", resultPrefix);
    //! The data-consturciton-call:
    writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, mat_resultCCM, resultPrefix_local, NULL, NULL, /*isTo_writeOutTransposed=*/true);
    //!
    //! De-allocate
    free__s_kt_matrix(&mat_resultCCM);
  }
  {//! Write out proepteis wrt. latter:
    //! Note: [alg]x[cluster-memberships], and (raw,  rank)[alg]x[CCM-scores]
    //! Buidl a file-path: 
    allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_topology_alg_time", resultPrefix);
    //! The data-consturciton-call:
    writeOut__s_hp_exportMatrix_t(&obj_exportMatrix, mat_resultTime, resultPrefix_local, NULL, NULL, /*isTo_writeOutTransposed=*/true);
    //!
    //! De-allocate
    free__s_kt_matrix(&mat_resultTime);
  }    
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief examplifies how to use different DBSCAN permutations.
   @author Ole Kristian Ekseth  (oekseth,  06. jan. 2017).
   @remarks 
   - extends "tut_ccm_17_ccmAldDataSyn.c" wrt. evaluation of mulitple algorithms, and the construction of result data.
   - compile: g++ -O2 -g tut_ccm_17_ccmAlgDataSyn.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_17.exe; ./tut_17.exe
**/
int main() 
#else
  static void tut_ccm_17_ccmAlgDataSyn()
#endif
{
#ifndef __M__calledInsideFunction ////! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  ////! *************************************************************************
  ////! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  ////! *************************************************************************
#endif 
  const bool onlyComputeFor_fixedInputMatrix = true; //! which is used tfor the case where we are interested in compating htexection-time of the algorithms (and Not the time computing pariwise simlairty metirc).

  const char *str_tut = "tut_ccm_17";
  const uint map_ccms_size = 4;
  const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t map_ccms[map_ccms_size] = {
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_SSE,
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette,
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC,
    //e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin,
    e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns,
  };
  uint _map_ccms_size = map_ccms_size;
  if(onlyComputeFor_fixedInputMatrix) {_map_ccms_size = 1;}
  for(uint ccm_id = 0; ccm_id < _map_ccms_size; ccm_id++) {
    const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = map_ccms[ccm_id];
    const char *str = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t(ccm_enum);
    allocOnStack__char__sprintf(2000, str_filePrefix, "%s_%s", str_tut, str);
    ////! Apply:
    __tut_ccm_17__apply(str_filePrefix, ccm_enum, onlyComputeFor_fixedInputMatrix);
  }

  ////!
  ////! @return
#ifndef __M__calledInsideFunction
  ////! *************************************************************************
  ////! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  ////! *************************************************************************  
  return true;
#endif
}

  
  
