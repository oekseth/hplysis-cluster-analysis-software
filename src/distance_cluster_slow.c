
  if(n1 < 1 || n2 < 1) return -1.0;


  t_float **distanceMatrix_intermediate = NULL; char **mask_dummy = NULL;  //! Then we a transposed matrix out of the input-tranpose
  if((transpose == 1) && isTo_invertMatrix_transposed) {
    //! Allocate memory, ie, using the 'inverse settings' as [above]:
    maskAllocate__makedatamask(/*nrows=*/ncolumns, /*ncols=*/nrows, &distanceMatrix_intermediate, &mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);	  
    //! TODO: consider to remove [below] asserts ... adn then add the parameters to [below] funciotn:
    assert(mask == NULL);
    //! Compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    matrix__transpose__compute_transposedMatrix(/*nrows=*/nrows, /*ncols=*/ncolumns, /*data1=*/data, mask, distanceMatrix_intermediate, mask_dummy, /*isTo_useSSE=*/true);
    //! Update, ie, 'invert' the variables:
    transpose = 0; const uint tmp = nrows; 
    nrows = ncolumns; ncolumns = tmp;    
    t_float **tmp_matrix = data; data = distanceMatrix_intermediate; distanceMatrix_intermediate = tmp_matrix;
    char **tmp_mask = mask; mask = mask_dummy; mask_dummy = tmp_mask;
  }
  

  /* Set the metric function as indicated by dist */
  t_float (*metric) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) = setmetric_slow(dist);

  t_float return_distance = T_FLOAT_MAX;


const t_float empty_value_float = 0; const uint empty_value_uint = 0; const char empty_value_char = 0;

  /* Check the indices */
  if(transpose==0) { 
    //! Note[memory-access-patterns]: 2;
    for(uint i = 0; i < n1; i++) {
      const uint index = index1[i];
      assert(index >= 0); // FIXME: remove this wehen we have change dthe index-lists to INT_MAX
      if( (index < 0) || (index >= nrows) || (index == INT_MAX) ) return_distance = -1.0;
    }
    for(uint i = 0; i < n2; i++) {
      const uint index = index2[i];
      assert(index >= 0); // FIXME: remove this wehen we have change dthe index-lists to INT_MAX
      if( (index < 0) || (index >= nrows) || (index == INT_MAX) ) return_distance = -1.0;
    }
  } else {
    //! Note[memory-access-patterns]: 2;
    for(uint i = 0; i < n1; i++) {
      const uint index = index1[i];
      assert(index >= 0); // FIXME: remove this wehen we have change dthe index-lists to INT_MAX
      if(index >= ncolumns) {return_distance = -1.0;}
      //if( (index < 0) || (index >= ncolumns) || (index == INT_MAX) ) return_distance = -1.0;
    }
    for(uint i = 0; i < n2; i++) {
      const uint index = index2[i];
      assert(index >= 0); // FIXME: remove this wehen we have change dthe index-lists to INT_MAX
      if(index >= ncolumns) {return_distance = -1.0;}
      //if( (index < 0) || (index >= ncolumns) || (index == INT_MAX) ) return_distance = -1.0;
    }
  }

  if(return_distance != T_FLOAT_MAX) {
    if(distanceMatrix_intermediate != NULL) {
      free_2d_list_float(&distanceMatrix_intermediate, ncolumns); distanceMatrix_intermediate = NULL;
    }
    if(mask_dummy != NULL) {free_2d_list_char(&mask_dummy, ncolumns); mask_dummy = NULL;}
    //maskAllocate__freedatamask(/*nelements=*/ncolumns, distanceMatrix_intermediate, mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);    
    return return_distance; //! ie, as the value has then been set.
  }

  switch (method)
    { case 'a': //! the distance between the arithmetic means of the two clusters
    { /* Find the center */
      //! Note: use a 'two-step' distance-computation: step(a) idnetify the sum of distances assicated to each vertex/clsuter, and then in step(b) copmute the distance between 'the average/mean sum of dsitances' between the point-clouds in each of the two input-clsuters.
      if(transpose==0) {
	//! Note[memory-access-patterns]: worst-case implies "n1" + "n2" + "ncolumns" cache-misses;
	// FIXME: for [”elow] write a 'geneirc code-test' to evlaaute/compare the time-cost of 'ordering' the memory-allcoations ... eg, to first allocate for "[0]" and thereafter for "[1]"
	//! Allcoate:
        uint* count[2] = { 
	  allocate_1d_list_uint(/*size=*/ncolumns, empty_value_uint),
	  allocate_1d_list_uint(/*size=*/ncolumns, empty_value_uint)};
        char* cmask[2] = { 
	  allocate_1d_list_char(ncolumns, empty_value_char),
	  allocate_1d_list_char(ncolumns, empty_value_char)
	};
        t_float* cdata[2] = { 
	  allocate_1d_list_float(/*size=*/ncolumns, empty_value_float),
	  allocate_1d_list_float(/*size=*/ncolumns, empty_value_float)};

	//! Idneitfy the counts/sinficance for the first clsuter:
        for(uint i = 0; i < n1; i++) {
	  const uint k = index1[i];
	  assert(k >= 0);
	  assert(k != INT_MAX);
	  assert(k != UINT_MAX);
          for(uint j = 0; j < ncolumns; j++) {
	    //! Note: for "n1" we update the first column, while for "n2" we udpate the seperate column, ie, to 'reflect' the two different clusters which are compared.
	    if(isOf_interest(data[k][j])) {
	      if(!mask || mask[k][j] != 0) { //! then increment the distance, ie, as the cell is of itnerest: 
		cdata[0][j] += data[k][j];
		count[0][j] += 1; //! ie, increment.
	      }
	    }
	  }
        }
	//! Idneitfy the counts/sinficance for the second cluster:
	//! Note[memory-access-patterns]: at worst-case each 'outer index' has a random/unpredictable location/value, for which "n2" cache-misses is expected. 
        for(uint i = 0; i < n2; i++) {
	  const uint k = index2[i];
	  assert(k >= 0);
	  assert(k != INT_MAX);
	  assert(k != UINT_MAX);
          for(uint j = 0; j < ncolumns; j++) {
	    if(isOf_interest(data[k][j])) {
	      if(!mask || mask[k][j] != 0) { //!  then increment the distance, ie, as the cell is of itnerest
		cdata[1][j] += data[k][j];
		count[1][j] += 1;
	      }
            }
	  }
        }
	//! Idnetify the average distance seperately for vertices in each 'point-cloud'/'cluster-set':
	//! Note[memory-access-patterns]: eitehr one or two cache-misses
        for(uint i = 0; i < 2; i++) {
          for(uint j = 0; j < ncolumns; j++) {
	    if(count[i][j] > 0) {
	      cdata[i][j] = cdata[i][j] / count[i][j]; //! ie, the average/'mean' distance between all of the points assicated to a given vertex/cluster.
              cmask[i][j] = 1; //! ie, as the cell is then of interest.
            } else {cmask[i][j] = 0;}
          }
	}
	//! Compute the distance between teh two 'point-clouds'
	// FIXME: complete [below]
	//! Note: the 'average distance' assicated to each of the 'clusters/vertices in each point-cloud' (as computed [ªbove]) is descriptive/correct based on the observation of ....??...
	//! Note[memory-access-patterns]: in worst-case "ncolumns" cache-misses
        const t_float distance = metric (ncolumns,cdata,cdata,cmask,cmask,weight,0,1,0);
	//! De-allocate the temporary containers:
        for(uint i = 0; i < 2; i++) {
	  free_1d_list_float(&cdata[i]);
	  free_1d_list_char(&cmask[i]);
	  free_1d_list_uint(&count[i]);
          //delete [] cmask[i];           delete [] count[i];
        }
	//! @return
        return_distance = distance;
      } else { //! then "transposed==1":
	//! Note[memory-access-patterns]: worst-case implies ("n1" * "nrows") + ("n2" * "nrows") + (2 * "nrows") cache-misses;
        uint** count = NULL;
        t_float** cdata = NULL;
        char** cmask = NULL;

	/* //! Note[memory-access-patterns]: */
	/* if(isTo_use_continousSTripsOf_memory) { */
	  const uint size_total = nrows * 2;
	  //! Allcoate for the 'inner':
	  const uint ncols_data = 2;
	  count = allocate_2d_list_uint(nrows, ncols_data, empty_value_uint);
	  //count[0] = allocate_1d_list_uint(/*size=*/size_total, /*empty-value=*/0);
	  cmask = allocate_2d_list_char(nrows, ncols_data, empty_value_char);
	  //cmask[0] = list_generic<char>::allocate_list(/*size=*/size_total, /*empty-value=*/0);
	  cdata = allocate_2d_list_float(nrows, ncols_data, empty_value_float);
	/*   //cdata[0] = allocate_1d_list_float(/\*size=*\/size_total, /\*empty-value=*\/0); */
	/*   uint offset = 2; */
	/*   /\* for(uint i = 1; i < nrows; i++) { *\/ */
	/*   /\*   count[i] = count[0] + offset; *\/ */
	/*   /\*   cdata[i] = cdata[0] + offset; *\/ */
	/*   /\*   cmask[i] = cmask[0] + offset; *\/ */
	/*   /\*   offset += 2; //! ie, as an 'inverse' to [above] if-stentence-clause. *\/ */
	/*   /\* } *\/ */
	/* } else { //! then we use a memory-allcoation-approach regarded as causing a signfincat cdelay in exeuction-time. */
	/*   assert(nrows < 100000); //! else a try-catch-block may be of itnerest. */
	/*   count = new uint*[nrows]; */
	/*   cdata = new t_float*[nrows]; */
	/*   cmask = new char*[nrows]; */
	  
	/*   for(uint i = 0; i < nrows; i++) { */
	/*     count[i] = new uint[2]; */
	/*     cdata[i] = new t_float[2]; */
	/*     cmask[i] = new char[2]; */
	/*   } */
	/* } */

	//! --------------------------------------------------
	//! Idneitfy the counts/sinficance for the first clsuter:
	//! Note[memory-access-patterns]: worst-case implies "n1" * "nrows" cache-misses
        for(uint i = 0; i < n1; i++) {
	  const uint k = index1[i];
	  assert(k >= 0);
	  assert(k != INT_MAX);
	  assert(k != UINT_MAX);
          for(uint j = 0; j < nrows; j++) { //! then increment the distance, ie, as the cell is of itnerest
	    if(isOf_interest(data[j][k])) {
	      if(!mask || mask[j][k] != 0) {
		cdata[j][0] += data[j][k];
		count[j][0] += 1;
	      }
	    }
          }
        }
	//! Idneitfy the counts/sinficance for the second cluster:
	//! Note[memory-access-patterns]: worst-case implies "n2" * "nrows" cache-misses
        for(uint i = 0; i < n2; i++) {
	  const uint k = index2[i];
	  assert(k >= 0);
	  assert(k != INT_MAX);
	  assert(k != UINT_MAX);
          for(uint j = 0; j < nrows; j++) {
	    if(isOf_interest(data[j][k])) {
	      if(!mask || mask[j][k] != 0) {
		cdata[j][1] += data[j][k];
		count[j][1] += 1;
	      }
            }
          }
        }
	//! Idnetify the average distance seperately for vertices in each 'point-cloud'/'cluster-set':
	//! Note[memory-access-patterns]: "nrows" cache-misses
        for(uint i = 0; i < nrows; i++) {
          for(uint j = 0; j < 2; j++) {
            if(count[i][j] > 0) {
	      cdata[i][j] = cdata[i][j] / count[i][j]; //! ie, the average/'mean' distance between all of the points assicated to a given vertex/cluster.
              cmask[i][j] = 1;
            } else {cmask[i][j] = 0;}
	  }
	}
	//! Compute distance: "nrows" cache-misses	
        const t_float distance = metric (nrows,cdata,cdata,cmask,cmask,weight,0,1,1);
	
	//! De-allocate the temporary containers:
	//	if(isTo_use_continousSTripsOf_memory) {
	free_2d_list_uint(&count, nrows);
	free_2d_list_float(&cdata, nrows);
	free_2d_list_char(&cmask, nrows);
	  /* delete [] count[0]; */
	  /* delete [] cdata[0]; */
	  /* delete [] cmask[0]; */
	/* } else  { */
	/*   for(uint i = 0; i < nrows; i++) { */
	/*     free_1d_list_uint(&count[i]); */
	/*     free_1d_list_float(&cdata[i]); */
	/*     free_1d_list_char(&cmask[i]); */
	/*     /\* delete [] count[i]; *\/ */
	/*     /\* delete [] cdata[i]; *\/ */
	/*     /\* delete [] cmask[i]; *\/ */
	/*   } */
	/* } */
	  /* free_generic_type_1d(count); */
	  /* free_generic_type_1d(cdata); */
	  /* free_generic_type_1d(cmask); */
	/* delete [] count; */
	/* delete [] cdata; */
	/* delete [] cmask; */
	//! @return
        return_distance = distance;
      }
    }
    case 'm': // the distance between the medians of the two clusters
    { 
      if(transpose == 0) {
        //t_float* temp = allocate_1d_list_float(/*size=*/nrows, /*empty-value=*/0), 
        t_float* cdata[2] = { 
	  allocate_1d_list_float(/*size=*/ncolumns, /*empty-value=*/0), 
	  allocate_1d_list_float(/*size=*/ncolumns, /*empty-value=*/0)};
        char* cmask[2] = { 
	  allocate_1d_list_char(/*size=*/ncolumns, /*empty-value=*/0), 
	  allocate_1d_list_char(/*size=*/ncolumns, /*empty-value=*/0)};
	

	//! --------------------------------------------------
	//! Copmute the assicated meidans for the two data-sets/cluster/point-cloud:
	//! Note[memory-access-patterns]: worst-case implies ("n1" * "nrows") + ("nrows"*2) cache-misses, which may be improved into ("n1" * "nrows")  through an 'ideal' memory-access-scheme
	computeSumOf_squaredMatrix_updateTail_wrtMedians(n1, index1, nrows, ncolumns, data, mask, cdata[0], cmask[0], /*isTo_useImplictMask=*/false);
	computeSumOf_squaredMatrix_updateTail_wrtMedians(n2, index2, nrows, ncolumns, data, mask, cdata[1], cmask[1], /*isTo_useImplictMask=*/false);

	//! Compute distance:
	//! Note[memory-access-patterns]: worst-case implies "ncolumns" cache-misses
        const t_float distance = metric (ncolumns,cdata,cdata,cmask,cmask,weight,0,1,0);

	//! De-allocate the temporary containers:
	//arrOf_counts.free_memory();
	free_generic_type_2d(cdata[0]);
	free_generic_type_2d(cdata[1]);
	free_generic_type_2d(cmask[0]);
	free_generic_type_2d(cmask[1]);
        /* for(uint i = 0; i < 2; i++) { */
	/*   delete [] cdata[i]; */
	/*   delete [] cmask[i]; */
	/* } */
	//! @return_distance = the result
        return_distance = distance;
      } else {
	// list_generic<type_float> arrOf_counts;
        t_float** cdata = NULL;
        char** cmask = NULL;

	//if(isTo_use_continousSTripsOf_memory) {
	  const uint size_total = nrows * 2;
	  //! Allcoate for the 'inner':
	  //const uint size_total = nrows * 2;
	  //! Allcoate for the 'inner':
	  const uint ncols_data = 2;
	  //count = allocate_2d_list_uint(nrows, ncols_data, empty_value_uint);
	  //count[0] = allocate_1d_list_uint(/*size=*/size_total, /*empty-value=*/0);
	  cmask = allocate_2d_list_char(nrows, ncols_data, empty_value_char);
	  //cmask[0] = list_generic<char>::allocate_list(/*size=*/size_total, /*empty-value=*/0);
	  cdata = allocate_2d_list_float(nrows, ncols_data, empty_value_float);
	  /* cmask = new char*[nrows]; */
	  /* cmask[0] = new char[size_total]; memset(cmask[2], 0, size_total*sizeof(char)); */
	  /* cdata = new t_float*[nrows]; */
	  /* cdata[0] = allocate_1d_list_float(/\*size=*\/size_total, /\*empty-value=*\/0); */
	  /* uint offset = 2; */
	  /* //! Note[memory-access-patterns]: */
	  /* for(uint i = 1; i < nrows; i++) { */
	  /*   //count[i] = count[0] + offset; */
	  /*   cdata[i] = cdata[0] + offset; */
	  /*   cmask[i] = cmask[0] + offset; */
	  /*   offset += 2; //! ie, as an 'inverse' to [above] if-stentence-clause. */
	  /* } */
	/* } else { //! then we use a memory-allcoation-approach regarded as causing a signfincat cdelay in exeuction-time. */
	/*   assert(nrows < 100000); //! else a try-catch-block may be of itnerest. */
	/*   cdata = new t_float*[nrows]; */
	/*   cmask = new char*[nrows]; */
	/*   //! Note[memory-access-patterns]: */
	/*   for(uint i = 0; i < nrows; i++) { */
	/*     cdata[i] = new t_float[2]; */
	/*     cmask[i] = new char[2]; */
	/*   } */
	/* } */

        t_float* temp = allocate_1d_list_float(ncolumns, 0);
        for (uint j = 0; j < nrows; j++) {
	  uint count = 0;
          for (uint k = 0; k < n1; k++) {
	    const uint i = index1[k];
            if(!mask || mask[j][i]) {
	      temp[count] = data[j][i];
              count++;
            }
          }
          if (count>0) {
	    cdata[j][0] = get_median_alt2 (count,temp);
            cmask[j][0] = 1;
          } else {
	    cdata[j][0] = 0.;
            cmask[j][0] = 0;
          }
        }
        for (uint j = 0; j < nrows; j++) {
	  uint count = 0;
          for (uint k = 0; k < n2; k++) {
	    const uint i = index2[k];
            if (!mask || mask[j][i]) {
	      temp[count] = data[j][i];
              count++;
            }
          }
          if (count>0) {
	    cdata[j][1] = get_median_alt2 (count,temp);
            cmask[j][1] = 1;
          } else {
	    cdata[j][1] = 0.;
            cmask[j][1] = 0;
          }
        }
        free(temp);	

	//! Compute distance: worst-case is "nrows" cache-misses for non-ideal memory-shcem while constant for 'ideal' memory-scheme
        const t_float distance = metric (nrows,cdata,cdata,cmask,cmask,weight,0,1,1);

	//! De-allocate the temporary containers:
	//free_2d_list_uint(&count);
	free_2d_list_float(&cdata, nrows);
	free_2d_list_char(&cmask, nrows);
	/* if(isTo_use_continousSTripsOf_memory) { */
	/*   delete [] cdata[0]; */
	/*   delete [] cmask[0]; */
	/* } else  { */
	/*   for(uint i = 0; i < nrows; i++) { */
	/*     delete [] cdata[i]; */
	/*     delete [] cmask[i]; */
	/*   } */
	/* } */
	/* delete [] cdata; */
	/* delete [] cmask; */

	//! @return_distance = the result
        return_distance = distance;
      }
    }
    case 's': // the smallest pairwise distance between members of the two clusters
    { 
      const uint n = (transpose==0) ? ncolumns : nrows;
      t_float mindistance = T_FLOAT_MAX;
      //! Identify the minimum distance between each pair in each opf the point-clouds:
      //! Note[memory-access-patterns]: ("n1" * "n2" * ("ncolumns" | "nrows")) memory-cachce-misses for worst-case though ("n1" * "n2") cache-misses for an 'ideal' memory-scheme
      for(uint i1 = 0; i1 < n1; i1++) {
        for(uint i2 = 0; i2 < n2; i2++) {
          const uint j1 = index1[i1];
          const uint j2 = index2[i2];
	  assert(j1 >= 0);
	  assert(j2 >= 0);
	  assert(j1 != INT_MAX);
	  assert(j2 != INT_MAX);
          const t_float distance = metric (n,data,data,mask,mask,weight,j1,j2,transpose);
          if(distance < mindistance) mindistance = distance;
        }
      }
      //! @return_distance = the result
      return_distance = mindistance;
    }
    case 'x': //! the largest pairwise distance between members of the two clusters
    { 
      const uint n = (transpose==0) ? ncolumns : nrows;
      t_float maxdistance = 0;
      //! Note[memory-access-patterns]: (same as for above "s").
      for(uint i1 = 0; i1 < n1; i1++) {
        for(uint i2 = 0; i2 < n2; i2++) {
          const uint j1 = index1[i1];
          const uint j2 = index2[i2];
	  assert(j1 >= 0);
	  assert(j2 >= 0);
	  assert(j1 != INT_MAX);
	  assert(j2 != INT_MAX);
          const t_float distance = metric (n,data,data,mask,mask,weight,j1,j2,transpose);
          if(distance > maxdistance) maxdistance = distance;
        }
      }
      //! @return_distance = the result
      return_distance = maxdistance;
    }
    case 'v': //! average of the pairwise distances between members of the clusters
    { 
      const uint n = (transpose==0) ? ncolumns : nrows;
      t_float distance = 0;
      //! Note[memory-access-patterns]: (same as for above "s").
      for(uint i1 = 0; i1 < n1; i1++) {
        for(uint i2 = 0; i2 < n2; i2++) {
	  const uint j1 = index1[i1];
          const uint j2 = index2[i2];
	  assert(j1 >= 0);
	  assert(j2 >= 0);
	  assert(j1 != INT_MAX);
	  assert(j2 != INT_MAX);
          distance += metric (n,data,data,mask,mask,weight,j1,j2,transpose);
        }
      }
      distance /= (n1*n2);
	//! @return_distance = the result
      return_distance = distance;
    }
  }

  //! Ivnestigate if we are to 'invert' the results:
  if((transpose == 1) && isTo_invertMatrix_transposed) {
    assert(distanceMatrix_intermediate);
    //! 'Back-translate' the distance-matrix, ie, compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    assert(data != distanceMatrix_intermediate); //! Note: at this exeuction-point we assume that "data" is the transpsoied matrix, ie, given our precivous/earlier call to [below] function
    matrix__transpose__compute_transposedMatrix(/*nrows=*/nrows, /*ncols=*/ncolumns, /*data1=*/data, mask, distanceMatrix_intermediate, mask_dummy, /*isTo_useSSE=*/true);    

    t_float **tmp_matrix = data; data = distanceMatrix_intermediate; distanceMatrix_intermediate = tmp_matrix;
    char **tmp_mask = mask; mask = mask_dummy; mask_dummy = tmp_mask;    
    if(distanceMatrix_intermediate != NULL) {
      free_2d_list_float(&distanceMatrix_intermediate, nrows); distanceMatrix_intermediate = NULL;
    }
    if(mask_dummy != NULL) {free_2d_list_char(&mask_dummy, nrows); mask_dummy = NULL;}
    //maskAllocate__freedatamask(/*nelements=*/ncolumns, distanceMatrix_intermediate, mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);    
  }


  if(return_distance != T_FLOAT_MAX) {return return_distance;}
  else {
    //! @return the result
    assert(false); //! ie, then ivnestgiate this case.
  /* Never get here */
    return -2.0;
  } 
