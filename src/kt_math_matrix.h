#ifndef kt_math_matrix_h
#define kt_math_matrix_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_math_matrix
   @brief provide functions for math-operations on matrices
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"



//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate" where we comptue the 'min' valeu for each 'run', ie, an overehad wrt. summations.
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM);
//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE" with tiling
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM);
//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow" with tiling
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__tiling(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM);
//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow" with SSE
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result) ;
//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput" with a "__restrict__" guineklien-keyword to the compiler
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result) ;
//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow_transposedAsInput(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result);
//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow(t_float **matrix, const uint size_adjusted, t_float **result);
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! Comptue inner distance-metric for euclid
void kt_func_metricInner_fast__fitsIntoSSE__euclid__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM);
//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM);
//! ------------------------------------------------------------------------
void kt_func_metricInner_fast__fitsIntoSSE__euclid__int16(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM);
//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__int16(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM);
//! ------------------------------------------------------------------------
/* //! Comptue inner distance-metric for euclid */
/* void kt_func_metricInner_fast__fitsIntoSSE__euclid__char(char **matrix, const uint nrows, const uint ncols, char **result, const uint SM); */
//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__char(char **matrix, char **matrix_2, const uint nrows, const uint ncols, char **result, const uint SM);
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------


/**
   @enum e_kt_math_matrix__typeOfComputationInverse 
   @brief idnetifies the type of method to be used in idneitifcation of a matrix' inverse representation (oekseth, 06. okt. 2016)
 **/
typedef enum e_kt_math_matrix__typeOfComputationInverse {
  e_kt_math_matrix__typeOfComputationInverse_coFactor,
  //e_kt_math_matrix__typeOfComputationInverse_coFactor_useMemCpy,
  /* e_kt_math_matrix__typeOfComputationInverse_coFactor_nonRecursive__SSE, */
  e_kt_math_matrix__typeOfComputationInverse_LU,
  e_kt_math_matrix__typeOfComputationInverse_LU__SSE
} e_kt_math_matrix__typeOfComputationInverse_t;

/**
   @struct s_kt_math_matrix_config_inverse
   @brief an object which is used for perofmrnace3-evlauation of differnet coding-stratgeis wrt. matrix-inversion (oekseth, 06. okt. 2016).
 **/
typedef struct s_kt_math_matrix_config_inverse {
  e_kt_math_matrix__typeOfComputationInverse_t typeOf_optimization;
  bool in_coFactor_use_fast;
  bool in_coFactor_isToUse_fastMemAlloc;
  bool in_coFactor_isToUse_preComputedPows;
  bool isToUse_fast_genericImplmentation__forLoops;
  bool isToUse_fast_genericImplmentation__ifClauseOrder;
  bool isToUse_valueRangeCompression; //! which if set impleis that we try minmizng the exeuciotn-time-cost by using a 'value-comrepssed' dat-set during computation.
} s_kt_math_matrix_config_inverse_t;

//! Set the "s_kt_math_matrix_config_inverse" object to empty, ie, using defualt values.
//! @return the enw-cosntructed object.
static s_kt_math_matrix_config_inverse_t getEmptyObject__s_kt_math_matrix_config_inverse() {
  s_kt_math_matrix_config_inverse_t self;
  self.typeOf_optimization = e_kt_math_matrix__typeOfComputationInverse_coFactor;
  self.in_coFactor_use_fast = true;
  self.in_coFactor_isToUse_fastMemAlloc = true;
  self.in_coFactor_isToUse_preComputedPows = true;
  self.isToUse_fast_genericImplmentation__forLoops = true;
  self.isToUse_fast_genericImplmentation__ifClauseOrder = true;
  self.isToUse_valueRangeCompression = true;

  //! @return the enw-cosntructed object.
  return self; 
}
/**
   @brief comtue the inverse matrix (oekseth, 06. okt. 2016).
   @param <matrix> is the matrix to inverse
   @param <size> is the matrix-size of an nxn matrix.
   @param <self> is the configuration of the inverse-matrix-opertiaont: for default value-configuraiton use the call to "getEmptyObject__s_kt_math_matrix_config_inverse()".
   @return the inversed matrix.
 **/
t_float **get_inverseMatrix(t_float **matrix, const uint size, const s_kt_math_matrix_config_inverse_t self);


#endif //! EOF
