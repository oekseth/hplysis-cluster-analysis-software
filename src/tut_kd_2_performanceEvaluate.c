#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kd_tree.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hpLysis_api.h"//! to define "hpLysis__globalInit__kt_api()"




/**
   @brief demosntate differnet cofnigruaitons to kd-tree-comtpatuiosn.
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017 and 06. dec. 2020).
   @remarks 
   -- compile: g++ -I../src/  -O2  -mssse3   -L ../src/  tut_kd_2_performanceEvaluate.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC -o x_kd
   -- compile: g++ -I../src/  -O2  -mssse3   -L ../src/    -l lib_x_hpLysis  -Wl,-rpath=. -fPIC -o x_tut
**/
int main() 
#else
  void tut_kd_2_performanceEvaluate()
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //!
  //! Load an arbitrary data-set:
  // const uint nrows = 1000; const uint ncols = 20;
  //const uint nrows = 1000; const uint ncols = 20;
  const uint nrows = 1000*10; const uint ncols = 5;
  //const uint nrows = 1000*1000; const uint ncols = 20;
  s_kt_matrix_t mat_input = initAndReturn__s_kt_matrix(nrows, ncols);
  //!
  //! Set wild-card values:
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      const t_float score = rand();
      if(score != 0) {
	mat_input.matrix[row_id][col_id] = 1.0/score;
      }
    }
  }

  s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();
  // if(false)
  { //! Compute DB-SCAN using the default/naive ddb-scan prodcuedre: 
    // ----------------- 
    { //! kd-tree
      //! 
      //! Build the kd-tree:
      start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
      s_kd_tree_t obj_kd = build_tree__s_kd_tree(&mat_input);
      end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
      //! ---------------------------
      // start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
      s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
      //assert(cnt_search <= nrows);
      //! Compute the clusters:
      s_kt_list_1d_uint_t map_clusterId = dbScan__s_kd_searchConfig_t(&obj_search, /*config_minCnt=*/1);
      const float time_total = end_time_measurement("dbSCAN:KD:radius", 0); //! ie, stop the timer and write out the timiing-difference.
      t_float score_global = 0; uint cnt_withMembers = 0;
      for(uint i = 0; i < map_clusterId.list_size; i++) {
	if(map_clusterId.list[i] != UINT_MAX) {score_global += map_clusterId.list[i]; cnt_withMembers++;}
      }
      fprintf(stderr, "#! time:%.2f\t sum(total)=%f, count(vertices-in-clusters)=%d, at %s:%d\n", time_total, score_global,cnt_withMembers,  __FILE__, __LINE__);
      //! De-allocate:
      free__s_kt_list_1d_uint_t(&map_clusterId);
      free__s_kd_searchConfig_t(&obj_search);
      free__s_kd_tree(&obj_kd);
    }
    // ----------------- 
    { //! brute:fast
      //! 
      //! Build the kd-tree:
      start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
      s_kd_tree_t obj_kd = build_tree__s_kd_tree(&mat_input);
      end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
      //! ---------------------------
      start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
      s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
      //assert(cnt_search <= nrows);
      s_kt_list_1d_uint_t map_clusterId = dbScan__s_kd_searchConfig_t(&obj_search, /*config_minCnt=*/1);
      end_time_measurement("dbSCAN:brute:fast", 0); //! ie, stop the timer and write out the timiing-difference.
      t_float score_global = 0; uint cnt_withMembers = 0;
      for(uint i = 0; i < map_clusterId.list_size; i++) {
	if(map_clusterId.list[i] != UINT_MAX) {score_global += map_clusterId.list[i]; cnt_withMembers++;}
      }
      fprintf(stderr, "#! sum(total)=%f, count(vertices-in-clusters)=%d, at %s:%d\n", score_global,cnt_withMembers,  __FILE__, __LINE__);
      //! De-allocate:
      free__s_kt_list_1d_uint_t(&map_clusterId);
      free__s_kd_searchConfig_t(&obj_search);
      free__s_kd_tree(&obj_kd);
    }
    // ----------------- 
    { //! brute:fast:xmtSort
      //! 
      //! Build the kd-tree:
      start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
      s_kd_tree_t obj_kd = build_tree__s_kd_tree(&mat_input);
      end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
      //! ---------------------------
      start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
      s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/UINT_MAX, UINT_MAX, /*ballsize=*/10, /*isTo_sort=*/false, &obj_metric);
      //assert(cnt_search <= nrows);
      s_kt_list_1d_uint_t map_clusterId = dbScan__s_kd_searchConfig_t(&obj_search, /*config_minCnt=*/1);
      end_time_measurement("dbSCAN:brute:fast:xmtSort", 0); //! ie, stop the timer and write out the timiing-difference.
      t_float score_global = 0; uint cnt_withMembers = 0;
      for(uint i = 0; i < map_clusterId.list_size; i++) {
	if(map_clusterId.list[i] != UINT_MAX) {score_global += map_clusterId.list[i]; cnt_withMembers++;}
      }
      fprintf(stderr, "#! sum(total)=%f, count(vertices-in-clusters)=%d, at %s:%d\n", score_global,cnt_withMembers,  __FILE__, __LINE__);
      //! De-allocate:
      free__s_kt_list_1d_uint_t(&map_clusterId);
      free__s_kd_searchConfig_t(&obj_search);
      free__s_kd_tree(&obj_kd);
    }
    // ----------------- 
    { //! brute:slow:xmtSort
      //! 
      //! Build the kd-tree:
      start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
      s_kd_tree_t obj_kd = build_tree__s_kd_tree(&mat_input);
      end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
      //! ---------------------------
      start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
      s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_slow, &obj_kd, /*nn_numberOfNodes=*/UINT_MAX, UINT_MAX, /*ballsize=*/10, /*isTo_sort=*/false, &obj_metric);
      //assert(cnt_search <= nrows);
      s_kt_list_1d_uint_t map_clusterId = dbScan__s_kd_searchConfig_t(&obj_search, /*config_minCnt=*/1);
      end_time_measurement("dbSCAN:brute:slow:xmtSort", 0); //! ie, stop the timer and write out the timiing-difference.
      t_float score_global = 0; uint cnt_withMembers = 0;
      for(uint i = 0; i < map_clusterId.list_size; i++) {
	if(map_clusterId.list[i] != UINT_MAX) {score_global += map_clusterId.list[i]; cnt_withMembers++;}
      }
      fprintf(stderr, "#! sum(total)=%f, count(vertices-in-clusters)=%d, at %s:%d\n", score_global,cnt_withMembers,  __FILE__, __LINE__);
      //! De-allocate:
      free__s_kt_list_1d_uint_t(&map_clusterId);
      free__s_kd_searchConfig_t(&obj_search);
      free__s_kd_tree(&obj_kd);
    }
  }
  if(false)
  { //! A brute-force-naive-appraoch: 
  //! 
  //! Build the kd-tree:
    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
    s_kd_tree_t obj_kd = build_tree__s_kd_tree(&mat_input);
    end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
    // ----------------- 
    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
    const uint cnt_search = nrows;
    s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_slow, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
    assert(cnt_search <= nrows);
    t_float score_global = 0;
    for(uint row_id = 0; row_id < cnt_search; row_id++) {
      s_kd_result result = init__s_kd_result_t(); 
      searchNode__s_kd_tree_t(&obj_search, NULL, row_id, &result);
      //! Get average-score:
      t_float score_local = 0; uint score_cnt = 0;
      for(uint i = 0; i < result.result.list_size; i++) {
	t_float score = result.result.list[i].tail;
	if(isOf_interest(score)) { score_local += score; score_cnt++;}
      }
      if(score_cnt > 0) {
	const t_float score_cnt_inv = 1.0/(t_float)score_cnt;
	score_global += (score_cnt_inv * score_local);
	//printf("|[%u]| = %u, at %s:%d\n", row_id, score_cnt, __FILE__, __LINE__);
      }
      //! De-allocte:
      free__s_kd_result_t(&result);
    }
    fprintf(stderr, "#! sum(total)=%f, at %s:%d\n", score_global, __FILE__, __LINE__);
    end_time_measurement("search:brute:slow", 0); //! ie, stop the timer and write out the timiing-difference.
    free__s_kd_searchConfig_t(&obj_search);
    free__s_kd_tree(&obj_kd);
  }
  if(false)
  { //! A brute-force-pre-compute-appraoch: 
    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
    s_kd_tree_t obj_kd = build_tree__s_kd_tree(&mat_input);
    end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
    // ----------------- 
    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
    const uint cnt_search = nrows;
    s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_brute_fast, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
    assert(cnt_search <= nrows);
    t_float score_global = 0;
    for(uint row_id = 0; row_id < cnt_search; row_id++) {
      s_kd_result result = init__s_kd_result_t(); 
      searchNode__s_kd_tree_t(&obj_search, NULL, row_id, &result);
      //! Get average-score:
      t_float score_local = 0; uint score_cnt = 0;
      for(uint i = 0; i < result.result.list_size; i++) {
	t_float score = result.result.list[i].tail;
	if(isOf_interest(score)) { score_local += score; score_cnt++;}
      }
      if(score_cnt > 0) {
	const t_float score_cnt_inv = 1.0/(t_float)score_cnt;
	score_global += (score_cnt_inv * score_local);
	// printf("|[%u]| = %u, at %s:%d\n", row_id, score_cnt, __FILE__, __LINE__);
      }
      //! De-allocte:
      free__s_kd_result_t(&result);
    }
    fprintf(stderr, "#! sum(total)=%f, at %s:%d\n", score_global, __FILE__, __LINE__);
    end_time_measurement("search:brute:fast", 0); //! ie, stop the timer and write out the timiing-difference.
    free__s_kd_searchConfig_t(&obj_search);    
    free__s_kd_tree(&obj_kd);
  }
  { //! A KD-TREE-appraoch: 
    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
    s_kd_tree_t obj_kd = build_tree__s_kd_tree(&mat_input);
    end_time_measurement("time-kd-tree", 0); //! ie, stop the timer and write out the timiing-difference.
    // ----------------- 
    start_time_measurement(); //! start the time-measurements, a function defined in our "measure_base.h".
    const uint cnt_search = nrows;
    s_kd_searchConfig_t obj_search = init__s_kd_searchConfig_t(e_kd_tree_searchStrategy_r_nearest, &obj_kd, /*nn_numberOfNodes=*/10, UINT_MAX, /*ballsize=*/T_FLOAT_MAX, /*isTo_sort=*/false, &obj_metric);
    assert(cnt_search <= nrows);
    t_float score_global = 0;
    for(uint row_id = 0; row_id < cnt_search; row_id++) {
      s_kd_result result = init__s_kd_result_t(); 
      searchNode__s_kd_tree_t(&obj_search, NULL, row_id, &result);
      //! Get average-score:
      t_float score_local = 0; uint score_cnt = 0;
      for(uint i = 0; i < result.result.list_size; i++) {
	t_float score = result.result.list[i].tail;
	if(isOf_interest(score)) { score_local += score; score_cnt++;}
      }
      if(score_cnt > 0) {
	const t_float score_cnt_inv = 1.0/(t_float)score_cnt;
	score_global += (score_cnt_inv * score_local);
      }
      //! De-allocte:
      free__s_kd_result_t(&result);
    }
    fprintf(stderr, "#! sum(total)=%f, at %s:%d\n", score_global, __FILE__, __LINE__);
    end_time_measurement("search:KD:radius", 0); //! ie, stop the timer and write out the timiing-difference.
    free__s_kd_searchConfig_t(&obj_search);
    free__s_kd_tree(&obj_kd);
  }
  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_input);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
