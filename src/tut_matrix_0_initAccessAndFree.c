#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_matrix.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.

/**
   @brief describe basic lgoics wrt. our "kt_matrix.h" API.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks general: use logics in our "kt_matrix.h" 
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  const uint nrows = 2; const uint ncols = 3; 
  //!
  //! Allocate:
  s_kt_matrix_t mat_1  = initAndReturn__s_kt_matrix(nrows, ncols); 
  assert(mat_1.nrows == nrows);
  assert(mat_1.ncols == ncols);
  //!
  //! Set values:
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      mat_1.matrix[row_id][col_id] = (row_id + 1)*0.3*(col_id + 1);
    }
  }
  //!
  //! Transpose the [ªbove] amtrix:
  s_kt_matrix_t mat_trans = setToEmptyAndReturn__s_kt_matrix_t();
  bool is_ok = init__copy_transposed__s_kt_matrix(&mat_trans, &mat_1, /*isTo_updateNames=*/false);
  assert(is_ok);
    
  //! 
  //! Comapre results: we expec tboth matrices to be equal:
  assert(mat_1.nrows == mat_trans.ncols);
  assert(mat_1.ncols == mat_trans.nrows);   
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      //! Write out:
      printf("[%u][%u] = (%f VS %f), at %s:%d\n", row_id, col_id, mat_1.matrix[row_id][col_id], mat_trans.matrix[col_id][row_id], __FILE__, __LINE__);
    }
  }

  //!
  //! De-allocates:
  free__s_kt_matrix(&mat_1);
  free__s_kt_matrix(&mat_trans);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

