#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
#include "matrixdata.h" //! which is sued for the Vilje-matrix-data-loading in our "tut_inputFile_6.c" (eosekth, 60. feb. 2017).
/**
   @brief examplify how a matrix may be loaded 'directly' into the 'our extenvie evlauation-pipeline'.(oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks extends our other "tut_inputFile_*" files wrt. 
   -- (a) home-grown parser: use a parser deidcated for the Vilje cupser-computer file-format;
   -- (b) 'direct data': instead of using 'a file from disk' or 'a mathemcial distrubiton-fucntion supported by Kntiting-Tools' we speicfy the data directly: the latter is simliar to "tut_inputFile_1_small__gold.c", with difference that we 'increse the concpetual compelxity' by using our home-grown "matrixdata.h" binary-dat-aparser.
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
  obj_config.config__isToPrintOut__iterativeStatusMessage = true;
  obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
  obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
  obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  obj_config.config_arg_npass = 10000; 
  //!
  //! Result data:
  obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  const char *config__fileName__binaryMatrix = "data/vilje-cost-rack1-half.dat"; //! a matrix which may be given at request to "jancrhis@ntnu.no" at NTNUs super-comptuer-faciltity:
  //! ----------------------------------- 
  //! 
  //! Describe classes of meitrcs and clsuter-algs to be used: for details see our "hp_clusterFileCollection.h" and our "hp_clusterFileCollection_simMetricSet.h":
  const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
  const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_all_notPostProcess; 
  const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! -----------------------------------   
  //! ----------------------------------- 
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  obj_config.isToStore__inputMatrix__inFormat__csv = globalConfig__isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;

  //! --------------------------------------------
  //!
  //! File-specific cofnigurations: 
  s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = false;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
  //fileRead_config.isTo_transposeMatrix = true;
  fileRead_config.isTo_transposeMatrix = true;
  //!
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  if(true) {
    fileRead_config__syn.imaginaryFileProp__nrows = 10;
    fileRead_config__syn.imaginaryFileProp__ncols = 10;
  } else { //! then we are interested in a more performacne-demanind approach: 
    fileRead_config__syn.imaginaryFileProp__nrows = 400;
    fileRead_config__syn.imaginaryFileProp__ncols = 400;
  }
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  s_kt_matrix_fileReadTuning_t fileRead_config__syn_latency = fileRead_config__syn;
  //!
  //! Build and specfiy a tempraory matrix which is cocncated with the 'specific' data-distributions:
  /* s_kt_matrix_fileReadTuning_t fileRead_config__syn__concat__type__overhead = fileRead_config__syn; */
  /* s_kt_matrix_fileReadTuning_t fileRead_config__syn__concat__type__latency = fileRead_config__syn; */
  //!
  //! Read the input-file:
  mdata_t dat;   mdata_initf ( &dat, config__fileName__binaryMatrix );
  if(!dat.n) {
    fprintf(stderr, "!!\t Seems like the cosntructed matrix was empty when laoding from \"%s\", ie, please invesetivgate. OBservaiton at [%s]:%s:%d\n", config__fileName__binaryMatrix, __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up.
  }
  //! Itniate:
  s_kt_matrix_t matrix_concatToAll__type__overhead = initAndReturn__s_kt_matrix(dat.n, dat.n);
  s_kt_matrix_t matrix_concatToAll__type__latency = initAndReturn__s_kt_matrix(dat.n, dat.n);
  //!
  //! Load the Vilje super-comptuer matrix (into our two data-strucutres), where logics are foudn in  the "matrixdata.h" written by "jancrhis@ntnu.no":
  for(uint i=0; i<(uint)dat.n; i++ ) {
    for(uint j=0; j<(uint)dat.n; j++ ) {
      matrix_concatToAll__type__overhead.matrix[i][j] = TS(dat,i,j);
      matrix_concatToAll__type__latency.matrix[i][j]  = TL(dat,i,j);
    }
  }
  //! De-allcoate the 'parser':
  mdata_finalize ( &dat );


  //! 
  //! Update the configuraiton-object:
  assert(matrix_concatToAll__type__latency.ncols > 0);
  assert(matrix_concatToAll__type__overhead.ncols > 0);
  fileRead_config__syn.mat_concat = &matrix_concatToAll__type__overhead;
  fileRead_config__syn_latency.mat_concat = &matrix_concatToAll__type__latency;

  const uint mapOf_realLife_size = 2;
  const s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size] = { //! where the lattter struct is defined in our "hp_clusterFileCollection.h"
    //! Note: in [”elow] we inaite the "s_hp_clusterFileCollection_t" struct defined in our "hp_clusterFileCollection.h":
    {/*tag=*/"vilje_superComputer__overhead", /*file_name=*/NULL, /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/true, /*k_clusterCount=*/10, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg},
    {/*tag=*/"vilje_superComputer__latency", /*file_name=*/NULL, /*fileRead_config=*/fileRead_config__syn_latency, /*fileIsAdjecency=*/true, /*k_clusterCount=*/10, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,metric__beforeClust, metric__insideClust, clusterAlg}
  };

  //! 
  //! Apply Logics:
  const bool is_ok = traverse__s_hp_clusterFileCollection_traverseSpec(&obj_config, mapOf_realLife, mapOf_realLife_size);
  assert(is_ok);

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  free__s_kt_matrix(&matrix_concatToAll__type__overhead);
  free__s_kt_matrix(&matrix_concatToAll__type__latency);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
