#include "hca_cluster.h"
#include "s_dense_closestPair.h"
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */


#ifndef min
#define min(x, y)	((x) < (y) ? (x) : (y))
#endif
#ifndef max
#define	max(x, y)	((x) > (y) ? (x) : (y))
#endif




//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
static hca_node_t *allocate_1d_list_hca_node(const uint size) {
#ifdef __cplusplus
  hca_node_t *tmp = new hca_node_t[size];
#else 
  hca_node_t *tmp = (hca_node_t*)malloc(sizeof(hca_node_t)*size);
#endif
  //memset(tmp, default_value, size*sizeof(hca_node));
  return tmp;
}
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
static void free_1d_list_hca_node(hca_node_t **list) {  
  assert_local(list);
  hca_node_t *tmp = *list;
#ifdef __cplusplus
  delete [] tmp;
#else 
  free(tmp);
#endif
}







// //! @return true if we asusem teh float-value is of interest.
// static float isOf_interest(const float value) {
//   return ( (value != FLT_MAX) && (value != FLT_MIN) );
// }

/**
   @brief translates a tree of vertices into clusters based on the hierarchical clutering-results.
   @param <nelements> The number of elements that were clustered.
   @param <tree> The clustering solution. Each node in the array describes one linking event, with tree[i].left and tree[i].right representig the elements that were joined. The original elements are numbered 0..nelements-1, nodes are numbered -1..-(nelements-1). Size set to "nelements - 1"
   @param <nclusters> The number of clusters to be formed.
   @param <clusterid> The number of the cluster to which each element was assigned. Space for this array should be allocated before calling the cuttree routine. If a memory error occured, all elements in clusterid are set to -1. Size set to "nelements"
   @remarks The cuttree routine takes the output of a hierarchical clustering routine, and divides the elements in the tree structure into clusters based on the hierarchical clustering result. The number of clusters is specified by the user.
**/
void hca_cutTree(const uint nelements, hca_node_t* tree, const uint nclusters, uint clusterid[]) {


  // FIXME: does the tree cotnain references to vertices or clsuters? ... <-- write a code-correctness-test wrt. this function ... ie, in conjuction with tests/applciaitons of cluster-bliraries. in the 'assert-fucniton-call'.
  
  uint icluster = 0;
  const uint n = nelements-nclusters; /* number of nodes to join */
  for(uint i = nelements-2; i >= n; i--) {
    const uint k1 = tree[i].left;
    if(k1 != UINT_MAX) {
      clusterid[k1] = icluster;
      icluster++;
    }
    const uint k2 = tree[i].right;
    if(k2 != UINT_MAX) {
      clusterid[k2] = icluster;
      icluster++;
    }
  }

  uint *nodeid =   allocate_1d_list_uint(/*size=*/n, /*empty-value=*/UINT_MAX);

  for(uint i = n-1; i >= 0; i--) {
    assert_local(nodeid[i] >= 0);
    uint j = nodeid[i];
    if(nodeid[i] == UINT_MAX) {
      j = icluster;
      nodeid[i] = j;
      icluster++;
    } else {;}
    assert_local(tree[i].left >= 0);
    const uint k1 = tree[i].left;

    if(k1 == UINT_MAX) {
      // FIXME: seems like [below] is 'coisnsitent' ... ie, consider using 'negative' "int" numbers in the "hca_node" structure.
      const int index_tmp_1 = -k1 - 1; //! eg, as seen wrt. "nodeid[-k-1] = j;" in "cluster.c" wrt. the "hca_cutTree(..)" function.
      assert_local(index_tmp_1 <= 0);
      nodeid[index_tmp_1] = j;
    } else clusterid[k1] = j;
    const uint k2 = tree[i].right;
    if(k2 == UINT_MAX) {
      // FIXME: seems like [below] is 'coisnsitent' ... ie, consider using 'negative' "int" numbers in the "hca_node" structure.
      const int index_tmp_2 = -k2 -1;
      assert_local(index_tmp_2 >= 0);
      nodeid[index_tmp_2] = j; 
    } else clusterid[k2] = j;
  }
  
  free(nodeid);
  return;
}



/* ******************************************************************** */
/* ******************************************************************** */


//! A local ANSI C style funciton to find the min-staince
static t_float __local__find_minPair(const uint nrows, const uint ncols, t_float** distmatrix, uint* ip, uint* jp) {
  t_float min_value = distmatrix[0][0]; 
  *ip = 1;
  *jp = 0;

  // FIXME: add a debug-param to investigate/test if the distnaces lawyas are symemtic ... adn tehn udpat eour "readme(..)", "graphAlgorithms_distance.cxx" and 'this file'.

  for(uint i = 0; i < nrows; i++) {
    for(uint j = 0; j < ncols; j++) {
      const t_float temp = distmatrix[i][j];
      if(temp <= min_value) {      
	min_value = temp;
        *ip = i;
        *jp = j;
      }
    }
  }
  return min_value;
}

/**
   @brief Compute Pairwise Maximum- (complete-) Linking (PML) cluster
   @param <nrows> the number of rows in the distmatrix input-data-set
   @param <ncols> the number of columns in the distmatrix input-data-set
   @param <distmatrix> the inptu-data-set.
   @return a cluster-memership-index for each vertex: call our "hca_cutTree(..)" funciton to infer the cluster-ids.
**/
hca_node_t* hca_computeCluster_pml(const uint nrows, const uint ncols, float** distmatrix) {
  if(nrows != ncols) {
    fprintf(stderr, "!!\t expected the input-matrix to describe the distnace between two vertices, ie, not a set of features: from nrows(%u) != ncols(%u) we assume that the latter is nto the case, ie, please cosntuct a distance/correlation-matrix (before calling thsi funciton), eg, using one of the functiosn which we provide. Fro questions pelase cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
    return NULL;
  }
  assert_local(nrows == ncols); // FIXME:r emvoe this and then update [below]
  const uint nelements = nrows;
  const uint nnodes = nelements - 1;

  //return NULL;




#if (config_typeOf_function_shortestPaths_useOptimal == 1)


  // FIXME: consider to make use of a 'nive' approach if "nrows < 10" ... ie, to avoid the assicated ovehread wrt. our approxh.

  //uint cnt_bucketsEachRows = ncols / /*block-size=*/(1024*3);
  //uint cnt_bucketsEachRows = ncols / /*block-size=*/(1024*2);
  uint cnt_bucketsEachRows = ncols / /*block-size=*/1024;
  //uint cnt_bucketsEachRows = ncols / /*block-size=*/512;
  //uint cnt_bucketsEachRows = ncols / /*block-size=*/256;
  //if(ncols > (1024*6)) 
  {
    //cnt_bucketsEachRows = 100;
    //cnt_bucketsEachRows = 500;
    //cnt_bucketsEachRows = 2; //60;
    //cnt_bucketsEachRows = 10;
    //cnt_bucketsEachRows = 20;
    cnt_bucketsEachRows = 10;
    //cnt_bucketsEachRows = 20;
    //cnt_bucketsEachRows = 30;
    //cnt_bucketsEachRows = ncols / 64; ///*block-size=*/(1024*6);
    //cnt_bucketsEachRows = ncols / /*block-size=*/(1024*3);
  }
  // FIXME: include [”elow]
  /* if(cnt_bucketsEachRows < 4) { cnt_bucketsEachRows = ncols / /\*block-size=*\/1024; } */
  /* if(cnt_bucketsEachRows < 4) { cnt_bucketsEachRows = ncols / /\*block-size=*\/512; } */
  /* if(cnt_bucketsEachRows < 4) { cnt_bucketsEachRows = ncols / /\*block-size=*\/64; } */
  /* if(cnt_bucketsEachRows < 4) { cnt_bucketsEachRows = 4;} */
  assert_local(cnt_bucketsEachRows > 0);

  const uint defValue_uint = 0;
  s_dense_closestPair_t s_obj_closestPair; s_dense_closestPair_init(&s_obj_closestPair, nrows, ncols, cnt_bucketsEachRows, distmatrix, /*config_useIntrisintitcs=*/false, /*isTo_use_fast_minTileOptimization=*/true);
  const t_float default_value_float = 0;
#if(config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos == 0)
  t_float *arrOf_tmp_tranposeOld_1 = allocate_1d_list_float(/*size=*/ncols, default_value_float);
  t_float *arrOf_tmp_tranposeOld_2 = allocate_1d_list_float(/*size=*/ncols, default_value_float);
#else
  t_float *arrOf_tmp_tranposeOld_1 = NULL; t_float *arrOf_tmp_tranposeOld_2 = NULL;
#endif

#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
  //! Provide logics for updating our "s_obj_closestPair" obly once, ie, if the buckets 'are the same':
  uint *mapOf_columnPos_toUse_is = allocate_1d_list_uint(/*size=*/nrows, defValue_uint);
  uint *mapOf_columnPos_toUse_js = allocate_1d_list_uint(/*size=*/nrows, defValue_uint);
#else
  uint *mapOf_columnPos_toUse_is = NULL;   uint *mapOf_columnPos_toUse_js = NULL;
#endif

#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
  const uint nrows_local = nrows*2;
  uint *arrOf_condensed_rowId_toUse_is = allocate_1d_list_uint(/*size=*/nrows_local, defValue_uint);
  assert(arrOf_condensed_rowId_toUse_is);
  uint *arrOf_condensed_rowId_toUse_js = allocate_1d_list_uint(/*size=*/nrows_local, defValue_uint);
  assert(arrOf_condensed_rowId_toUse_js);
  //const uint *Arrof_Condensed_Rowid_Touse_startPos = arrOf_condensed_rowId_toUse_js;
#else
  uint *arrOf_condensed_rowId_toUse_is = NULL; uint *arrOf_condensed_rowId_toUse_js = NULL;
#endif
  uint *mapOf_updateTimePoint = allocate_1d_list_uint(/*size=*/nrows_local, defValue_uint); //! which is used to enure that a row-id is evlauated only once.
#endif

  uint* clusterid = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/0);
  hca_node_t* result = allocate_1d_list_hca_node(/*size=*/nelements);
  /* Setup a list specifying to which cluster a gene belongs */
  for(uint j = 0; j < nelements; j++) clusterid[j] = j;
  for(uint n = nelements; n > 1; n--) {
    uint is = 1;     uint js = 0;

    // printf("-----------------------------, at %s:%d\n", __FILE__, __LINE__);
    //! Update the log-function, starting 'at a new time-point':
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_dataUpdateCalls_getMinPos);
#endif
    //! Idenitfy the shortest distance between two vertices:
#if 1 == 1 
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    //printf("at %s:%d\n", __FILE__, __LINE__);   
    const uint inode = nelements-n;
    //result[inode].distance = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &is, &js, /*nrows_threshold=local_matrixSize=*/nrows, /*ncols_threshold=*/ncols); 
    result[inode].distance = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &is, &js, /*nrows_threshold=local_matrixSize=*/n, /*ncols_threshold=*/n); 
    assert_local(is < nrows); assert_local(js < ncols);
    // is = 0; js = 0; // FIXME: remove

    const uint local_matrixSize = n; //! ie, to simplify our writing.
    // FIXME: include [”elow]:
#ifndef NDEBUG
#if (config_debugSlowdown_useExtensive_correctnessTesting == 1)
    { //! Then we validate correctenss of our "s_obj_closestPair" algorithm-implementaiton:
      uint is_tmp = 1; uint js_tmp = 0;
      // FIXME: .... consider to both update [”elow] and our procedure wr.t the 'zero' case ... eg, to ingore 'zero-values' from evlauation.
      const t_float distance_cmp = __local__find_minPair(n, n, distmatrix, &is_tmp, &js_tmp);
      if(false){printf("(cmp)\t compare rows=(fast, slow)=(%u, %u) and columns(%u, %u) for distnace=(%f, %f), and |matrix|=%u, at %s:%d\n", is, is_tmp, js, js_tmp, result[inode].distance, distance_cmp, n, __FILE__, __LINE__);}
      if(distance_cmp != result[inode].distance) {
	//! then write out the current score-table, ie, to simplify debugging:
	if(true) {writeOut_MinScores_humanized(stdout, &s_obj_closestPair);}
      }
      assert_local(distance_cmp == result[inode].distance);
      assert_local(is_tmp == is); assert_local(js_tmp == js);
    }
#endif
#endif
#else //! then we use a slwoer version:
    result[nelements-n].distance = __local__find_minPair(nrows, ncols, distmatrix, &is, &js);
#endif    
#else //! then we evlauate/test the time-cost of the "__local__find_minPair(..)" function
    //! Note[aritcle]: by 'usign this block' we observe that the "__local__find_minPair(..)" time-cost is isngificnat, ie, when compared to the 'find-min' llogics.
    result[nelements-n].distance = 1; 
#endif 
    //! Update the log-function, starting 'at a new time-point':
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_dataUpdateCalls_getMinPos, /*cnt=*/1);
#endif

    const uint columnUpdate_pos_start = min(is, js) + 1; //! ie, .....??.... 
    const uint columnUpdate_pos_end   = n;



#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific);
#if (config_useLog_main_signiOfSameBucket == 1) || (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
    //printf("**-----, at %s:%d\n", __FILE__, __LINE__);
    const uint bucket_id_js = get_columnBucketId(&s_obj_closestPair, js); //! ie, to ge thte signicance of valeus in the same bucket.
    const uint bucket_id_is = get_columnBucketId(&s_obj_closestPair, is); //! ie, to ge thte signicance of valeus in the same bucket.
    //printf("**-----, at %s:%d\n", __FILE__, __LINE__);
    //const uint isIn_sameBucket_case1 = (bucket_id_is == bucket_id_js);
#endif 
#endif


    uint arrOf_condensed_rowId_toUse_is_currPos = 0;  uint arrOf_condensed_rowId_toUse_js_currPos = 0;
    uint *mapOf_columnPos_toUse_is_local = mapOf_columnPos_toUse_is; 
    uint *arrOf_condensed_rowId_toUse_is_local = arrOf_condensed_rowId_toUse_is; 
    uint *arrOf_condensed_rowId_toUse_is_local_currPos_scalar = &arrOf_condensed_rowId_toUse_is_currPos; 
    uint cnt_alreadyInserted_forColumnAtSelfPos_is = 0; uint cnt_alreadyInserted_forColumnAtSelfPos_js = 0;
    if( (is == js)
#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
      || (bucket_id_is == bucket_id_js) 
#endif
	) {
      //printf(" set (is, js) indexes to be in the same tile, at %s:%d\n", __FILE__, __LINE__);
      mapOf_columnPos_toUse_is_local = mapOf_columnPos_toUse_js; //! ie, to simplify the udpate-procedure.
      arrOf_condensed_rowId_toUse_is_local = arrOf_condensed_rowId_toUse_js; //! ie, to simplify the udpate-procedure.
      arrOf_condensed_rowId_toUse_is_local_currPos_scalar = &arrOf_condensed_rowId_toUse_js_currPos; //! ie, to simplify the udpate-procedure.
      assert(arrOf_condensed_rowId_toUse_is_local_currPos_scalar == &arrOf_condensed_rowId_toUse_js_currPos);
#ifndef NDEBUG
      //! Then validate that the udpate-seuqnece works as expected:
      // TOFOD[aritcle]: cosndier to update a to-be-written- article wrt. the compelxtiies of using 'references as coutners', eg, as exmaplfied in [”eloł]:
      (*arrOf_condensed_rowId_toUse_is_local_currPos_scalar)++; 
      assert(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar == 1);
      assert(arrOf_condensed_rowId_toUse_js_currPos == *arrOf_condensed_rowId_toUse_is_local_currPos_scalar);
      (*arrOf_condensed_rowId_toUse_is_local_currPos_scalar)--;
      assert(arrOf_condensed_rowId_toUse_js_currPos == *arrOf_condensed_rowId_toUse_is_local_currPos_scalar);
#endif
    }

    // printf("--------- test: %f, at %s:%d\n", distmatrix[1][0], __FILE__, __LINE__);

    { // FIXME: instead of [”elow] use _mm_storeu_ps
      /* Fix the distances */

#if (config_typeOf_function_shortestPaths_useOptimal == 1)
#if(config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos == 0)	
      //! Update the log-function, starting 'at a new time-point':
      update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_copyToHcaMem);
      //! Copy the old values:
      //! Note: this 'procedure' must be called before the udpate of both the rows and columns, ie, as we otehrwise may risk 'causing' cinsostnecies', eig, wrt, the 'previous' 'summary of valeus'.
      //for(uint j = columnUpdate_pos_start; j < columnUpdate_pos_end; j++) {
      // FIXME: what is the time-cost wr.t [”elow] 'extensive' copy?
      // FIXME: try to 'dientify' the [min,max] index-ranges which are being investgiated in our call to "s_dense_closestPair_updateAt_column(..)" ... and then udpate [”elow].
      for(uint j = 0; j < n-1; j++) {
	if(false) {printf("oldValue[%u][%u]=%f, at %s:%d\n", j, js, distmatrix[j][js], __FILE__, __LINE__);}
	arrOf_tmp_tranposeOld_1[j] = distmatrix[j][js];  //! which is sued in our s_dense_closestPair_updateAt_column(..)"
      }
      if(is != js) { //! then copy the old values:
	// FIXME: try to 'dientify' the [min,max] index-ranges which are being investgiated in our call to "s_dense_closestPair_updateAt_column(..)" ... and then udpate [”elow].
	//for(uint j = is+1; j < n-1; j++) { arrOf_tmp_tranposeOld_2[j] = distmatrix[j][is]; } //! which is sued in our s_dense_closestPair_updateAt_column(..)"
	for(uint j = 0; j < n-1; j++) { 
	  if(false) {printf("oldValue[%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__);}
	  arrOf_tmp_tranposeOld_2[j] = distmatrix[j][is];
	} //! which is sued in our s_dense_closestPair_updateAt_column(..)"
      }
      //! Update the log-function, starting 'at a new time-point':
      update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_copyToHcaMem, /*cnt=*/1);
#endif
      //! Update the log-function, starting 'at a new time-point':
      update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_max);
#endif

      // FIXME: include [”elow]
      if(false) {printf("(update-matrix) for values [%u][%u....%u], at %s:%d\n", js, 0, js, __FILE__, __LINE__);}
      // FIXME: include
      for(uint j = 0; j < js; j++) {
      	/* if(distmatrix[js][j] < distmatrix[js][j]) { */
	/*   distmatrix[js][j] = distmatrix[js][j];	   */
	/* } */
	distmatrix[js][j] = max(distmatrix[is][j], distmatrix[js][j]);
	// printf("[row=%u][%u]=%f, at %s:%d\n", js, j, distmatrix[js][j], __FILE__, __LINE__);
      }


      //printf("--------- test: %f, at %s:%d\n", distmatrix[1][0], __FILE__, __LINE__);
      //! Note: [below] is only called for "js < is"
      // FIXME: update [”elow] wr.t 'case(1)' in our SP-optmizaiton.
#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
      for(uint j = js+1; j < is; j++) {
      	if(distmatrix[j][js] < distmatrix[is][j]) {
	  distmatrix[j][js] = distmatrix[is][j];	  
	  const t_float currentValue = distmatrix[j][js];
	  //#if (config_typeOf_function_shortestPaths_useOptimal == 1)
	  mapOf_columnPos_toUse_js[j] = js;
	  if(js == is) {mapOf_updateTimePoint[j] = n;} //! ie, to enure that a row-id is evlauated only once.
	  //#endif
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
	  const uint row_id = j; 
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal == 1)
	  // FIXME: validate correctness of [”elow] ... and add seperate tables for "arrOf_condensed_rowId_toUse_js" and "arrOf_condensed_rowId_toUse_js_currPos" if the tables are differnet
	  s_dense_closestPair_t *self = &s_obj_closestPair; //! ie, to satsifiy the specific grammar of the 'ANSI C macro' sytnax;
	  if(__macro__minValueIsImproved(self, row_id, bucket_id_js, currentValue)) {
	    const uint column_pos = js;
	    cnt_alreadyInserted_forColumnAtSelfPos_js += (uint)update_minValues_for_columnBucket_setOptimal_atColumnPos(self, row_id, bucket_id_js, column_pos, currentValue);	    
	  } else if(__macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id_js, js)) {
	    arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest.
	  } //! else we assuem the column is not of interest.
#else
	  arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest.
#endif //! end("config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal")
#endif
	} else {
	  mapOf_columnPos_toUse_js[j] = UINT_MAX; //! ie, as the valeu then did not imrpvoe.
	}
      }
#else
      for(uint j = js+1; j < is; j++) {
	distmatrix[j][js] = max(distmatrix[is][j], distmatrix[j][js]);
      }
#endif
      //distmatrix[j][js] = max(distmatrix[is][j], distmatrix[j][js]);
      // printf("[row=%u][%u]=%f, at %s:%d\n", j, js, distmatrix[j][js], __FILE__, __LINE__);
      

      //printf("--------- test: %f, at %s:%d\n", distmatrix[1][0], __FILE__, __LINE__);
      //! Note: in [below] if "js > is", then the 'updated rows' may change <-- could we 'move this' before the row-update <-- consider using a new amcro-arpam 'to thest time correctess-impact'. <-- seems like the  "js !> is" is an example where the 'sorted proepty' may/will not hold.
      // FIXME: include



#if(config_reWrite_mainHcaLoop_notUpdatePointless_memoryCells == 1) 
      if(is != js) //! otehrwise the valeus will anyghow be overwritten
#endif      
	{
	  // FIXME: update [”elow] wr.t 'case(1)' in our SP-optmizaiton.
#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	  for(uint j = is+1; j < n; j++) {
	    if(distmatrix[j][js] < distmatrix[j][is]) {
	      distmatrix[j][js] = distmatrix[j][is];	  
	      const t_float currentValue = distmatrix[j][js];
	      mapOf_columnPos_toUse_js[j] = js;
	      if(js == is) {mapOf_updateTimePoint[j] = n;} //! ie, to enure that a row-id is evlauated only once.
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
	      const uint row_id = j; 
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal == 1)
	      // FIXME: validate correctness of [”elow] ... and add seperate tables for "arrOf_condensed_rowId_toUse_js" and "arrOf_condensed_rowId_toUse_js_currPos" if the tables are differnet
	      s_dense_closestPair_t *self = &s_obj_closestPair; //! ie, to satsifiy the specific grammar of the 'ANSI C macro' sytnax;
	      if(__macro__minValueIsImproved(self, row_id, bucket_id_js, currentValue)) {
		const uint column_pos = js;
		cnt_alreadyInserted_forColumnAtSelfPos_js += (uint)update_minValues_for_columnBucket_setOptimal_atColumnPos(self, row_id, bucket_id_js, column_pos, currentValue);	      
	      } else if(__macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id_js, js)) {
		arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest.
	      } //! else we assuem the column is not of interest.
	      /* if(__macro_isTo_updateColumn_forBucketTile(self, row_id, bucket_id_js, js)) { */
	      /* 	arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest. */
	      /* } //! else we assuem the column is not of interest. */
#else
	      arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest.
#endif
#endif
	    } else {
	      mapOf_columnPos_toUse_js[j] = UINT_MAX; //! ie, as the valeu then did not imrpvoe.
	    }
	  }
#else
	  for(uint j = is+1; j < n; j++) {
	    distmatrix[j][js] = max(distmatrix[j][is], distmatrix[j][js]);
	    // printf("[row=%u][%u]=%f, and where local_matrixSize=%u, at %s:%d\n", j, js, distmatrix[j][js], local_matrixSize, __FILE__, __LINE__);
	  }
#endif
	}
#if(config_reWrite_mainHcaLoop_notUpdatePointless_memoryCells == 1) 
      else { //! then we anyhow need to update for the last value
	// TODO: consider dropping this, ie, as the 'last valeu' is not expected to be used.
	//! Note: [”elow] operation is expected to have an ins-igficnat time-cost.
	const uint j = n-1;
	distmatrix[j][js] = max(distmatrix[j][is], distmatrix[j][js]);
      }	
#endif          
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
      //! Update the log-function, starting 'at a new time-point':
      update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_max, /*cnt=*/1);
#endif    

      //printf("--------- test: %f, at %s:%d\n", distmatrix[1][0], __FILE__, __LINE__);

      //! Copy:
      // FIXME: wrt. [”elow] cosnider to include a 'copy' funciton in our "s_dense_closestPair(..)" <-- may reduce the time-cost by a factor of "|rows|*|rows|" <-- experiemtn witht eh latter .... make use of 'itnernal cegbug-aprams to hyptoese the exueciton-tiem-effect ... write correctness-tests ... using a macro-apram ... and then updte our article.
      if(false) {printf("(update-matrix) for values [%u][%u....%u], at %s:%d\n", is, 0, is, __FILE__, __LINE__);}
      // FIXME: include [”elow]


      const uint endPos_rowUpdate_is = is;
#if(config_reWrite_mainHcaLoop_useMemCpy == 1)      
      if(is > 0) {
	if(is != (n-1)) {
	  const uint j = 0;
	  memcpy(&distmatrix[is][j], &distmatrix[n-1][j], sizeof(t_float)*endPos_rowUpdate_is);
	}
      } else 
#endif
	{
	  for(uint j = 0; j < endPos_rowUpdate_is; j++) { 
	    distmatrix[is][j] = distmatrix[n-1][j]; 
	    // printf("[row_id=%u][%u]=%f, at %s:%d\n", is, j, distmatrix[is][j], __FILE__, __LINE__);
	  }
	}
    

    
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
      //! Update the log-function, starting 'at a new time-point':
      //! Note: we first start at thsi exeuction-point given the in-signicnace cost of [ªbove] 'hroizntal' memory-copying:
      update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_cpy);
      update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_cpy_partialBuckets);
#endif

      // FIXME: update [”elow] wr.t 'case(2)' in our SP-optmizaiton.
      const uint endPos_coluUpdate_is = n-1;
      const uint n_minusOne = n-1;
      // FIXME: valdiate that [”elow] if-caluse is reflected in ª[bveo] wrt. the "distmatrix" udpates
      //if(n_minusOne != is) 
      
      // FIXME: in [”elow] only iterate through each 'column-bukcet' wrt. "distmatrix[n_minusOne][j]" ... and compare each bucket to the to-be-added row-buckets .. eg, wrt. "[1...100][1...100]"

      if((is+1) < endPos_coluUpdate_is) { //! Copy the 'last row' to the column-in-question.
	uint j = 0;
	//! IDentify the 'non-bucket-chunk' which we need to inpsect, ie, wr.t using the 'alreayd known bucket-min-values to pmtize the iteraiton':
	/* printf("... at %s:%d\n", __FILE__, __LINE__); */
	/* printf("**-----, at %s:%d\n", __FILE__, __LINE__); */
	const uint is_plussOne = is+1;
	assert(endPos_coluUpdate_is <= ncols);
	assert(is_plussOne < ncols);
	const uint bucket_id_prev = get_columnBucketId(&s_obj_closestPair, is+1);
	/* printf("**-----, at %s:%d\n", __FILE__, __LINE__); */
	/* printf("... bucket_id_prev=%u, cnt_bucketsEachRows=%u, at %s:%d\n", bucket_id_prev, s_obj_closestPair.cnt_bucketsEachRows, __FILE__, __LINE__); */
	//const uint columnPos_startOf_bucket = get_columnBucket_startPosOf_bucket(&s_obj_closestPair, bucket_id_prev); //! ie, the start-postion of the bukcet.
	const uint columnPos_startOf_bucket_next = get_columnBucket_endPlussOnePosOf_bucket(&s_obj_closestPair, bucket_id_prev); //! ie, the start-postion of the bukcet.
	//const uint columnPos_startOf_bucket = get_columnBucket_startPosOf_bucket(&s_obj_closestPair, bucket_id_is); //! ie, the start-postion of the bukcet.
	bool debug_isUpdated_cpy_partialBuckets = false;

	for(j = is+1; j < endPos_coluUpdate_is; j++) {
	  //printf("columnPos_startOf_bucket=%u, j=%u at bucket=%u, at %s:%d\n", columnPos_startOf_bucket_next, j, get_columnBucketId(&s_obj_closestPair, j), __FILE__, __LINE__);
	  if(columnPos_startOf_bucket_next == j) {
	    //! then 'finalize' the log-measuremtn:
	    //printf("end-measurement, at %s:%d\n", __FILE__, __LINE__);
	    update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_cpy_partialBuckets, /*cnt=*/1);
	    debug_isUpdated_cpy_partialBuckets = true;
	  }
	  //distmatrix[j][is] = 1;
	
	  // printf("odl-value=%f, at %s:%d\n", distmatrix[j][is], __FILE__, __LINE__);

	  distmatrix[j][is] = distmatrix[n_minusOne][j];
	  const t_float currentValue = distmatrix[j][is];

	  // printf("(update)\t [%u][%u]=%f, js=[%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], js, distmatrix[j][js], __FILE__, __LINE__);

#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	  // FIXME: validate correctness of [”elow] if-calsue:
	  if((is != js) || (distmatrix[j][is] <= distmatrix[j][js]) ) {
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
	    //printf("\t (update)\t [%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__);
	  
	    //! Note: our "mapOf_updateTimePoint" is used to handle the case wrt. mulipel updates: we use a 'boolean exailiy-table' where the [j] = 'time-point in euxeicont' (eg, "n") ... ie, to 'seprate' [”elow] from earlier udpates ... without the need for a 'compelte re-iteration'.

	    // assert(false); // FIXME: thereafter update [below] .... seems like [below] does not proeprtly 'handle' the case where 'the udpated valie was the best chosie, though no longer is'.

	    //! Investigate 
	    //if(true) {
	    if( (is != js) || (mapOf_updateTimePoint[j] != n) ) { //! then we assuem that "j" has nto been already marked as 'of interest'.
	      //printf("(update---)\t [%u][%u]=%f, and index-optmal-at-bucket=%u, at %s:%d\n", j, is, distmatrix[j][is], (uint)s_obj_closestPair.matrixOf_locallyTiled_minPairs_columnPos[is][bucket_id_is], __FILE__, __LINE__);
	      // FIXME: ... below may results in non-order wrt. row-ids ... try to use a differnet aprpaoch than [”elow]
	      const uint row_id = j; 	    
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal == 1)
	      // FIXME: validate correctness of [”elow] ... and add seperate tables for "arrOf_condensed_rowId_toUse_js" and "arrOf_condensed_rowId_toUse_js_currPos" if the tables are differnet
	      s_dense_closestPair_t *self = &s_obj_closestPair; //! ie, to satsifiy the specific grammar of the 'ANSI C macro' sytnax;
	      if(__macro__minValueIsImproved(self, row_id, bucket_id_is, currentValue)) {
		const uint column_pos = is;
		cnt_alreadyInserted_forColumnAtSelfPos_is += (uint)update_minValues_for_columnBucket_setOptimal_atColumnPos(self, row_id, bucket_id_is, column_pos, currentValue);	      
	      } else if(__macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id_is, js)) {
		arrOf_condensed_rowId_toUse_is_local[(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar)++] = row_id; //! ie, as we then assuem teh row is of itnerest. */
		//arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest.
	      } //! else we assuem the column is not of interest.
	      /* if(__macro_isTo_updateColumn_forBucketTile(self, row_id, bucket_id_is, is)) { */
	      
	      /*   //printf("(update****)\t [%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__); */
	      /*   arrOf_condensed_rowId_toUse_is_local[(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar)++] = row_id; //! ie, as we then assuem teh row is of itnerest. */
	      /* } //! else we assuem the column is not of interest. */
#else
	      //printf("(update****)\t [%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__);
	      arrOf_condensed_rowId_toUse_is_local[(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar)++] = row_id; //! ie, as we then assuem teh row is of itnerest.
#endif
	    } //! else we asusme the row is already added.
#endif
	    mapOf_columnPos_toUse_is_local[j] = is; 
	  } else {
	    //! Then we assume that the valeu is unchanged, ie, eitehr "[j] = js" or "[j] = UINT_MAX"
	  }
#endif	//distmatrix[is][j] = distmatrix[n-1][j]; // FIXME: remove and incldue [ªbove]
	//distmatrix[is][j] = distmatrix[n_minusOne][j]; // FIXME: remove and incldue [ªbove]
	// printf("[row_id=%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__);

	//mapOf_columnPos_toUse_js[j] = is; // FIXME: remove
	}
	//assert(columnPos_startOf_bucket_next >= j);
	if(columnPos_startOf_bucket_next == j) {
	  //! then 'finalize' the log-measuremtn:
	  //printf("end-measurement, at %s:%d\n", __FILE__, __LINE__);
	  update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_cpy_partialBuckets, /*cnt=*/1);
	  debug_isUpdated_cpy_partialBuckets = true;
	}
	//assert(debug_isUpdated_cpy_partialBuckets == true);
	// printf("end-measurement, columnPos_startOf_bucket=%u, j=%u, at %s:%d\n", columnPos_startOf_bucket_next, j, __FILE__, __LINE__);
      } //! End-of: "copy the 'last row' to the column-in-question".
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
      //! Update the log-function, starting 'at a new time-point':
      update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_cpy, /*cnt=*/1);
#endif
    }

    //printf("--------- test: %f, at %s:%d\n", distmatrix[1][0], __FILE__, __LINE__);

    //! Update the log-function, starting 'at a new time-point':
#if(config_useLog_main_signiOfSameBucket == 1)
    update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific, /*cnt=*/(bucket_id_is == bucket_id_js)); 
#else
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific, /*cnt=*/1); 
#endif
#endif

#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_dataUpdateCalls); 
    //! Then update the closest-distance matrix:
    if(local_matrixSize > 0) { //! then we udpate 'for the next iteration':	
      // printf("... at %s:%d\n", __FILE__, __LINE__);
      
      //if(js != (local_matrixSize-1)) 



      const uint columnUpdate_pos_start = min(is, js) + 1; //! ie, givent eh for-loop-updates of "j = js+1" and "j = is+1" for "distmatrix[j][js]".
      const uint columnUpdate_pos_end   = n-1;

#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
      if(bucket_id_is == bucket_id_js) {
	//! Then our "arrOf_condensed_rowId_toUse_js" dientifes the 'best-fitting' column-positions:
	if( (is != (local_matrixSize-1)) || (js != (local_matrixSize-1)) ) {
	  assert_local( (is+1) == columnUpdate_pos_start); //! ie, as othwsie would be indicative of an in-cosnsitent code-update.
	  // printf("--------------------- at %s:%d\n", __FILE__, __LINE__);
	  assert(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar == arrOf_condensed_rowId_toUse_js_currPos);
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	  s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/columnUpdate_pos_start, columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos, (cnt_alreadyInserted_forColumnAtSelfPos_is + cnt_alreadyInserted_forColumnAtSelfPos_js));
#else	  
	  s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/columnUpdate_pos_start, columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos);
#endif
	} //! otherwise this 'case' whill be implcialy handled when we call our "s_dense_closestPair_getClosestPair(..)"
      } else
#endif
	//! Provide logics for updating our "s_obj_closestPair" obly once, ie, if the buckets 'are the same':
	{
	  //! Note[time]: in [”elow] block if [abov€] data-set never changes the time-cost of below is 1.2x (when compare dot the overall exueciotn-time).
	  //! Update wrt. the 'vertically updated column':
	  // const uint row_endPos = min(local_matrixSize-1, nnodes-inode); //! ie, .....??....
	  if(is != js) {
	    //assert_local(columnUpdate_pos_start < columnUpdate_pos_end);
	    assert(mapOf_columnPos_toUse_is_local == mapOf_columnPos_toUse_is); //! ie, as we then assume taht 'udapting one implies udpating "is" and not "js'.
	    assert(mapOf_columnPos_toUse_is_local != mapOf_columnPos_toUse_js); //! ie, as we then assume taht 'udapting one implies udpating "is" and not "js'.

	  
	    /* 	  //! The operation: */
	    if(js != (local_matrixSize-1)) {
	      // printf("js=%u, ncols=%u, ... at %s:%d\n", js, ncols, __FILE__, __LINE__);
	      assert_local(js != n);
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	      s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/js, /*row-start-pos=*/columnUpdate_pos_start, /*row_endPos+1=*/columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos, cnt_alreadyInserted_forColumnAtSelfPos_js);
#else
	      s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/js, /*row-start-pos=*/columnUpdate_pos_start, /*row_endPos+1=*/columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos);
#endif
	    } //! otherwise this 'case' whill be implcialy handled when we call our "s_dense_closestPair_getClosestPair(..)"	  
	    if(is != (local_matrixSize-1)) {
	      //! Update the log-function, starting 'at a new time-point':
	      update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections); 
	      // printf("... at %s:%d\n", __FILE__, __LINE__);
	      const uint row_start_pos = is+1; const uint row_endPosPlussOne = n-1;
	      if(row_start_pos < row_endPosPlussOne) {
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
		s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/row_start_pos, /*row_endPos+1=*/row_endPosPlussOne, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_2, mapOf_columnPos_toUse_is_local, arrOf_condensed_rowId_toUse_is, arrOf_condensed_rowId_toUse_is_currPos, cnt_alreadyInserted_forColumnAtSelfPos_is);
#else
		s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/row_start_pos, /*row_endPos+1=*/row_endPosPlussOne, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_2, mapOf_columnPos_toUse_is_local, arrOf_condensed_rowId_toUse_is, arrOf_condensed_rowId_toUse_is_currPos);
#endif
	      }
	      //! Update the log-function, starting 'at a new time-point':
	      update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections, /*counut=*/(row_endPosPlussOne-row_start_pos)); 
	    } //! otherwise this 'case' whill be implcialy handled when we call our "s_dense_closestPair_getClosestPair(..)"
	  } else {
	    // printf("... at %s:%d\n", __FILE__, __LINE__);
	    if(is != (local_matrixSize-1)) {
	      assert_local( (is+1) == columnUpdate_pos_start); //! ie, as othwsie would be indicative of an in-cosnsitent code-update.
	      assert(mapOf_columnPos_toUse_is_local == mapOf_columnPos_toUse_is); //! ie, as we then assume taht 'udapting one implies udpating the second'.
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	  s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/columnUpdate_pos_start, columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos, (cnt_alreadyInserted_forColumnAtSelfPos_is + cnt_alreadyInserted_forColumnAtSelfPos_js));
#else	  
	      s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/columnUpdate_pos_start, columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_is_local, *arrOf_condensed_rowId_toUse_is_local_currPos_scalar);
#endif
	    } //! otherwise this 'case' whill be implcialy handled when we call our "s_dense_closestPair_getClosestPair(..)"
	  }
	} //! else the value will otherwise be ingored 'due to the incremental reduction in the column-size'.
	


      //if(false) // FIXME: remove
      {
      	//! Udpate wrt. the two 'hroizontally updated rows':
      	//! Note: we update for index-ranges: js=[0, js] and is=[0 ... is]:
      	if(is != js) {
      	  if(js != 0) {
	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/js, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/js, /*row-end-pos-plussOne=*/(local_matrixSize-1));
      	  }
      	  if(is != 0) {
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections); 
	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/is, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections, /*counut=*/is); 
      	  }
      	} else { //! then we update for only the 'copied region':
	  if(is != 0) {
	    // FIXME: icnldue:
	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/is, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	  }
      	}
      } //! else the value will otherwise be ingored 'due to the incremental reduction in the column-size'.
      //! Evaluate if the object is conssitenten:
      // assert_local(classWideTests_runTime(&s_obj_closestPair, local_matrixSize-1, local_matrixSize-1, __FILE__, __LINE__, /*test_distancematrix=*/true)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.

      //! Update the log-function, starting 'at a new time-point':
      update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_dataUpdateCalls, /*cnt=*/1); 
    }
#endif    

    /* Update clusterids */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];
    //assert_local(n >= (nelements - 1)); // FIXME: if [below] does not hold, then consider to update/elvaute [below].
    clusterid[js] = n-nelements-1;
    clusterid[is] = clusterid[n-1];
  }
  //! De-allcoate the locally reserved memory:
  free_1d_list_uint(&clusterid);
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
  s_dense_closestPair_free(&s_obj_closestPair);
  if(arrOf_tmp_tranposeOld_1) {free_1d_list_float(&arrOf_tmp_tranposeOld_1);}
  if(arrOf_tmp_tranposeOld_2) {free_1d_list_float(&arrOf_tmp_tranposeOld_2);}
  if(mapOf_columnPos_toUse_js != NULL) {
    free_1d_list_uint(&mapOf_columnPos_toUse_js);
  }
  if(mapOf_columnPos_toUse_is != NULL) {
    free_1d_list_uint(&mapOf_columnPos_toUse_is);
  }
  if(arrOf_condensed_rowId_toUse_is != NULL) {
    free_1d_list_uint(&arrOf_condensed_rowId_toUse_is);
  }
  if(arrOf_condensed_rowId_toUse_js != NULL) {
    free_1d_list_uint(&arrOf_condensed_rowId_toUse_js);
  }
  if(mapOf_updateTimePoint != NULL) {
    free_1d_list_uint(&mapOf_updateTimePoint);
  }
#endif
  return result;
}


/* ******************************************************************* */

/**
   @brief Comptue Pairwise Average Linking (PAL) on the given distance matrix.
   @param <nrows> the number of rows in the distmatrix input-data-set
   @param <ncols> the number of columns in the distmatrix input-data-set
   @param <distmatrix> the inptu-data-set.
   @return a cluster-memership-index for each vertex: call our "hca_cutTree(..)" funciton to infer the cluster-ids.
**/
hca_node_t* hca_computeCluster_pal(const uint nrows, const uint ncols, float** distmatrix) {
  if(nrows != ncols) {
    fprintf(stderr, "!!\t expected the input-matrix to describe the distnace between two vertices, ie, not a set of features: from nrows(%u) != ncols(%u) we assume that the latter is nto the case, ie, please cosntuct a distance/correlation-matrix (before calling thsi funciton), eg, using one of the functiosn which we provide. Fro questions pelase cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
    return NULL;
  }
  assert_local(nrows == ncols); // FIXME:r emvoe this and then update [below]

  const uint nelements = nrows;
  assert_local(nelements > 0);
  if(nelements == 0) {return NULL;}
  /* Setup a list specifying to which cluster a gene belongs, and keep track
   * of the number of elements in each cluster (needed to calculate the
   * average). */
  uint* clusterid = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/0);
  uint* number = allocate_1d_list_uint(/*size=*/nelements, /*empty-value=*/0);
  hca_node_t* result = allocate_1d_list_hca_node(/*size=*/nelements);
  for(uint j = 0; j < nelements; j++) {
    number[j] = 1; clusterid[j] = j;
  }
  
  uint min_distance = UINT_MAX;
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
  uint cnt_bucketsEachRows = 10;
  s_dense_closestPair_t s_obj_closestPair; s_dense_closestPair_init(&s_obj_closestPair, nrows, ncols, cnt_bucketsEachRows, distmatrix, /*config_useIntrisintitcs=*/false, /*isTo_use_fast_minTileOptimization=*/true);
#endif

  for(uint n = nelements; n > 1; n--) {
    uint is = 1;     uint js = 0;
    //! Idenitfy the shortest distance between two vertices:
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    result[nelements-n].distance = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &is, &js, /*nrows_threshold=local_matrixSize=*/n, /*ncols_threshold=*/n); 
    const uint local_matrixSize = n; //! ie, to simplify our writing.
#else
    result[nelements-n].distance = __local__find_minPair(n, n, distmatrix, &is, &js); //! ie, identify the pair which has the lowest weight.
#endif
    /* Save result */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];

    /* Fix the distances */
    const uint sum = number[is] + number[js]; //! ie, the combined number/signfiance of the pair.
    //! Identify the relative importance of the pair-distance, ie, assicated to the 'minimal' column wrt. the rows:
    //! Note: in [below] the 'iteration-threshold is set to "js": ... 

    for(uint j = 0; j < js; j++) { //! ie, [js][0...js] , which for all js>0 will result in updates.
      distmatrix[js][j] = distmatrix[is][j]*number[is] 
                        + distmatrix[js][j]*number[js];
      distmatrix[js][j] /= sum;
    }


    //! Note: in [below] the 'iteration-threshold is set to "is":
    for(uint j = js+1; j < is; j++) { //! ie, [js...is][js] , which implies that condtion (js < is) will not be evalauted/applied/updated.
      distmatrix[j][js] = distmatrix[is][j]*number[is]
                        + distmatrix[j][js]*number[js];
      distmatrix[j][js] /= sum;
    }
    //! Note: in [below] the 'iteration-threshold is set to "n":
    for(uint j = is+1; j < n; j++) { //! ie, [is...n][js] , which implies that 
      distmatrix[j][js] = distmatrix[j][is]*number[is]
                        + distmatrix[j][js]*number[js];
      distmatrix[j][js] /= sum;
    }

    //! Copy:
    for(uint j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
    for(uint j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];

    /* Update number of elements in the clusters */
    number[js] = sum; //! ie, the signfiance in 'this' iteration.
    number[is] = number[n-1]; //! ie, the sigifance/importance/number in the previous iteration.

    /* Update clusterids */
    assert_local(n >= (nelements - 1)); // IFMX:E if [below] does not hold, then consider to update/elvaute [below].
    clusterid[js] = n - nelements - 1; //! ie, an incremntal increase.
    clusterid[is] = clusterid[n-1]; //! ie, inverse of "clusterid[js]".

#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    {
      //! Udpate wrt. the two 'hroizontally updated rows':
      //! Note: we update for index-ranges: js=[0, js] and is=[0 ... is]:
      if(is != js) {
	if(js != 0) {
	  s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/js, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/js, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	}
	if(is != 0) {
	  //! Update the log-function, starting 'at a new time-point':
	  //update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections); 
	  s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/is, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	  //! Update the log-function, starting 'at a new time-point':
	  //update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections, /*counut=*/is); 
	}
      } else { //! then we update for only the 'copied region':
	if(is != 0) {
	  // FIXME: icnldue:
	  s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/is, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	}
      }
    } //! else the value will otherwise be ingored 'due to the incremental reduction in the column-size'.
#endif
  }
  //! De-allcoate the locally reserved memory:
  free_1d_list_uint(&clusterid); free_1d_list_uint(&number);
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
  s_dense_closestPair_free(&s_obj_closestPair);
#endif

  return result;
}


#ifdef cha_cluster__config__distanceFunctionIsDefined //! then we assume the distance-funciton is defined (oekseth, 06. okt. 2016)
/**
   @brief Comptue Pairwise Average Linking (PAL) on the given distance matrix.
   @param <nrows> the number of rows in the distmatrix input-data-set
   @param <ncols> the number of columns in the distmatrix input-data-set
   @param <distmatrix> the inptu-data-set.
   @return a cluster-memership-index for each vertex: call our "hca_cutTree(..)" funciton to infer the cluster-ids.
**/
hca_node_t* hca_computeCluster_pcl(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncols, float** data, uint** mask, float weight[], float** distmatrix, const bool isTo_use_continousSTripsOf_memory, const bool needTo_useMask_evaluation) {

  //assert(transpose == 0); //! ie, what we expect

  const uint transpose = 0;
  const uint nelements = (transpose==0) ? nrows : ncols;

  const uint ndata = transpose ? nrows : ncols;
  const uint nnodes = nelements - 1;
  const uint ncolumns = ncols;
  //const uint ncols = ncolumns;

  const char default_value_char = 0;
  char **mask_internal = NULL;
  if(mask) { //! then allcoate and copy the mask:
    mask_internal = allocate_2d_list_char(nrows, ncolumns, default_value_char);
    for(uint i = 0; i < nrows; i++) { for(uint out = 0; out < ncolumns; out++) { mask_internal[i][out] = mask[i][out]; } } 
  }

  /* Set the metric function as indicated by dist */
  //float (*metric) (uint, float**, float**, char**, char**, const float[], uint, uint, uint) = graphAlgorithms_distance::setmetric(dist);
  void (*metric_oneToMany) (config_nonRank_oneToMany) = setmetric__correlationComparison__oneToMany_maskType__uint(metric_id, weight, mask, mask, needTo_useMask_evaluation);
    const e_cmp_masksAre_used_t  masksAre_used = (mask || needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
  t_float (*metric_each) (config_nonRank_each_t) = setmetric__correlationComparison__each(metric_id, weight, mask_internal, mask_internal, needTo_useMask_evaluation);
  s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask_internal, mask_internal, weight, transpose, masksAre_used, nrows, ncols);

  // assert(false); // FIXME: describe how may use 'partiality' wrt. index-ranks ... ie, update our cofnig-object.
  // assert(false); // FIXME: update [above] using our "isTo_useSpearman_rankPreComputation" ... instead 'accessing' a wrapper-function 'of this'.

  const uint default_value_uint = 0;   const t_float default_value_float = 0;

  uint* distid = allocate_1d_list_uint(/*size=*/nelements, default_value_uint);
  //const Node empty_node = /*empty-value=*/Node();
  hca_node_t* result = allocate_1d_list_hca_node(/*size=*/(nnodes+2)); //, empty_node);
  
  float *arrOf_tmp_tranposeOld_1 = allocate_1d_list_float(/*size=*/ncolumns, default_value_float);
  float *arrOf_tmp_tranposeOld_2 = allocate_1d_list_float(/*size=*/ncolumns, default_value_float);

  float** newdata = allocate_2d_list_float(nrows, ncolumns, default_value_float);
  char** newmask_tmp = NULL;
  
  //maskAllocate__makedatamask(nelements, ndata, &newdata, &newmask_tmp, isTo_use_continousSTripsOf_memory);
  //! Allcoate masks wrt. unit-elements
  uint** newmask_uint = allocate_2d_list_uint(nrows, ncolumns, default_value_uint); 
  // newmask_uint[0] = new uint[ndata*nelements];
  // memset(newmask_uint[0], 0, sizeof(uint)*ndata * nelements);
  // for(uint i = 0, offset=0; i < nelements; i++) {newmask_uint[i] = newmask_uint[0] + offset; offset += ndata;}

  for(uint i = 0; i < nelements; i++) {distid[i] = i;}   /* To remember which row/column in the distance matrix contains what */


  /* Storage for node data */

  assert(transpose == 0); //! ei, wehat we expect whenc omptuign for the optimal case.

  /* if(isTo_use_continousSTripsOf_memory) { */
  /*   const uint cnt_total = nelements * ndata; */
  /*   memcpy(newdata[0], data[0], cnt_total*sizeof(float)); */
  /*   if(mask) { */
  /*     memcpy(newmask_uint[0], mask[0], cnt_total*sizeof(uint)); */
  /*   } */
  /* } else */ {
    for(uint i = 0; i < nelements; i++) {
      memcpy(newdata[i], data[i], ndata*sizeof(float));
      if(mask) {memcpy(newmask_uint[i], mask[i], ndata*sizeof(uint));}
    }
  }
  data = newdata;   mask = newmask_uint;
  assert(mask); //! ie, what we expect.

  const uint ndata_innerOptimized = (ndata > VECTOR_FLOAT_ITER_SIZE) ? ndata - VECTOR_FLOAT_ITER_SIZE : 0;

#if (config_typeOf_function_shortestPaths_useOptimal == 1)
  uint cnt_bucketsEachRows = 10;
  s_dense_closestPair_t s_obj_closestPair; s_dense_closestPair_init(&s_obj_closestPair, nrows, ncols, cnt_bucketsEachRows, distmatrix, /*config_useIntrisintitcs=*/false, /*isTo_use_fast_minTileOptimization=*/true);
#endif

  //! Allcoate a tempraory lost to hold the memory-pointers:
  t_float **mem_ref_data = MF__getMemRef_data(data, nrows);
  uint **mem_ref_mask = (mask) ?  MF__getMemRef_mask(mask, nrows) : NULL;


  for (uint inode = 0; inode < nnodes; inode++) { /* Find the pair with the shortest distance */
    uint is = 1;     uint js = 0;
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    assert(nelements >= inode);
    const uint local_matrixSize = nelements - inode; //! ie, to simplify our writing.
    assert(local_matrixSize < (nnodes+2)); //! ie, inside the allcoated memoyr-sapce.
    result[local_matrixSize].distance = s_dense_closestPair_getClosestPair(&s_obj_closestPair, &is, &js, /*nrows_threshold=local_matrixSize=*/local_matrixSize, /*ncols_threshold=*/local_matrixSize); 
#else
    result[inode].distance = __local__find_minPair(nelements - inode, ncols, distmatrix, &is, &js); //! ie, identify the pair which has the lowest weight.
#endif
    //result[inode].distance = find_closest_pair(nelements-inode, distmatrix, &is, &js);
    result[inode].left = distid[js];
    result[inode].right = distid[is];

    /* Make node js the new node */
    for(uint i = 0; i < ndata; i++) { 
      data[js][i] = data[js][i]*mask[js][i] + data[is][i]*mask[is][i];
      mask[js][i] += mask[is][i];
      if (mask[js][i]) data[js][i] /= mask[js][i];
    }

    /* if(isTo_use_continousSTripsOf_memory) { */
    /*   memcpy(data[is], data[nnodes-inode], sizeof(float)*ncolumns); //! ie, as an alternative to ' */
    /*   if(mask) { */
    /* 	memcpy(mask[is], mask[nnodes-inode], sizeof(uint)*ncolumns); //! ie, as an alternative to ' */
    /* 	memcpy(mask_internal[is], mask_internal[nnodes-inode], sizeof(char)*ncolumns); //! ie, as an alternative to ' */
    /*   } */
    /* } else */ {
      // FIXME: validate correctness of [below] wrt. the 'continous' meory-allcioation
      // free_1d_list_float(&data[is]); 
      // if(mask) {free_1d_list_uint(&mask[is]);}
      data[is] = data[nnodes-inode];
      if(mask) {
	mask[is] = mask[nnodes-inode];
      }
      if(mask_internal) {
	mask_internal[is] = mask_internal[nnodes-inode]; }
      // FIXME: replace all corruecnes of "delete []"
    }

    // free(data[is]);
    // free(mask[is]);
    // data[is] = data[nnodes-inode];
    // mask[is] = mask[nnodes-inode];
  
    /* Fix the distances */
    distid[is] = distid[nnodes-inode];
    for (uint i = 0; i < is; i++)
      distmatrix[is][i] = distmatrix[nnodes-inode][i];
    for (uint i = is + 1; i < nnodes-inode; i++)
      distmatrix[i][is] = distmatrix[nnodes-inode][i];

    distid[js] = -inode-1;

    //! Comptue for: for (i = 0; i < js; i++) 
    kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, data, /*index1=*/js,  &config_metric, 
			  NULL, //(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute,
			  /*arrOf_result=*/distmatrix[js],
			  metric_oneToMany);

    //assert(false); // fIXME: include [”elow]:
    // for (i = 0; i < js; i++) 
    //   distmatrix[js][i] = metric(ndata,data,data,mask,mask,weight,js,i,0);
    //assert(false); // fIXME: include [”elow]:
    for (uint i = js + 1; i < nnodes-inode; i++) {
      distmatrix[i][js] =   kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ndata, data, data, /*index1=*/js, /*index2=*/i, config_metric, metric_each,
					     NULL); //(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);
    }


#if (config_typeOf_function_shortestPaths_useOptimal == 1)

    {
      //! Udpate wrt. the two 'hroizontally updated rows':
      //! Note: we update for index-ranges: js=[0, js] and is=[0 ... is]:
      if(is != js) {
	if(js != 0) {
	  s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/js, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/js, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	}
	if(is != 0) {
	  //! Update the log-function, starting 'at a new time-point':
	  //update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections); 
	  s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/is, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	  //! Update the log-function, starting 'at a new time-point':
	  //update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections, /*counut=*/is); 
	}
      } else { //! then we update for only the 'copied region':
	if(is != 0) {
	  // FIXME: icnldue:
	  s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/is, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	}
      }
    } //! else the value will otherwise be ingored 'due to the incremental reduction in the column-size'.
#endif


    //   distmatrix[i][js] = metric(ndata,data,data,mask,mask,weight,js,i,0);
  }


  /* Free temporarily allocated space */
  // FIXME: validate [”elow] ... ie, consider to uset he isTo_use_continousSTripsOf_memory parameter ... and then 'in the caller' store a reference to the 'base-membory-pointer'.
  //s_dense_closestPair_free(&s_obj_closestPair);
  //maskAllocate__freedatamask(nelements, newdata, newmask_tmp, isTo_use_continousSTripsOf_memory);
  MF__free__memRef(mem_ref_data, mem_ref_mask);
  /* assert(newmask_uint); free_2d_list_uint(&newmask_uint, nrows); */
  /* if(mask_internal) {  free_2d_list_char(&mask_internal, nrows); } */
  free_1d_list_uint(&distid);
  free_1d_list_float(&arrOf_tmp_tranposeOld_1);
  free_1d_list_float(&arrOf_tmp_tranposeOld_2);
 
  return result;





#if (config_typeOf_function_shortestPaths_useOptimal == 1)
  s_dense_closestPair_free(&s_obj_closestPair);
#endif

}
#endif //! endif("cha_cluster__config__distanceFunctionIsDefined")
