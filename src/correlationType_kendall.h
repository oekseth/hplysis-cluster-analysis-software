#ifndef correlationType_kendall_h
#define correlationType_kendall_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file correlationType_kendall
   @brief provide lgois for copmtuation of the Kendall correlation-metric
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "mask_api.h"
#include "correlation_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"
#include "s_allAgainstAll_config.h"
#include "correlation_rank_rowPair.h" //! which defines functions used for our "s_correlation_rank_rowPair"
#include "correlation_inCategory_delta.h"


//! A naive implemtnaiton which is a copy-pate of the "cluster.c" blirary
//! @remarks we do not inlcud the GNU-lizence of the cluster.c library as this funciton is Not to be used in the 'live softwre' accesialbe for the users (of thsi software),, ie, merely intended to be sued as a point-fo-reference wrt. performance' (oekseth, 06. otk. 2016).
t_float kendall__slow_naiveImplementaiton(int n, t_float** data1, t_float** data2, int** mask1, int** mask2,  const t_float weight[], int index1, int index2, int transpose);
t_float kendall__slow_naiveImplementaiton_improvedWrt_ifClause(int n, t_float** data1, t_float** data2, int** mask1, int** mask2,  const t_float weight[], int index1, int index2, int transpose, const bool isTo_useImprvoedIfClause);

//! Comptue Kendalls Tau through Knitghts improved algorithm -reusing memory:
t_float kendall_improvment_kingsAlgorithm(const uint n, t_float *vector_1, t_float *vector_2, const char *mask2_vector, t_float* arr1_tmp, t_float* arr2_tmp, const uint *vector1_ranks, const e_kt_correlationFunction_t metric_id, const bool needTo_useMask_evaluation);
//t_float kendall_improvment_kingsAlgorithm(const uint n, t_float *vector_1, t_float *vector_2, const char *mask2_vector, t_float* arr1_tmp, t_float* arr2_tmp, const uint *vector1_ranks, const bool vector1_is_sorted, const bool isTo_use_correlation, const bool needTo_useMask_evaluation);

/**
   @brief compute the Kendall distance between two rows or columns. The Kendall distance is defined as one minus Kendall's tau.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)"), with difference of "6*n" extra/unneccesary cache-misses
 **/
t_float kt_kendall(const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_kt_correlationFunction_t metric_id, const s_allAgainstAll_config_t self);
//t_float kt_kendall(const uint ncols, t_float *__restrict__ row_1, t_float *__restrict__ row_2, char *mask1, char *mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_kt_correlationFunction_t metric_id, const s_allAgainstAll_config_t self);



#endif //! EOF
