/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


#include "correlation_macros__distanceMeasures.h"

#include "measure_base.h"

//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
    sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}



int main() {

  assert(false); // FIXME: add seomthing.


  assert(false); // FIXME: try to compare our approach with the "ALGLIB" appraoch for standarized amtrix-operiaotns ... eg, wrt. "ae_v_dotproduct(..)"  in "ap.cpp" : <-- seems like the authros does not make use of memory-tilign .. ie, their appraoch si to 'include mroe operaitons in each for-loop ,... a 'task' which we would expect the compiler 'to manage, ie, fix'. <-- if oru results indicate a performance-increase wr.t our appraoch then we 'have somethign itneresting', ie, as we have then amanged to opmtize apselized linear-algebra lbirary (ie, givne oru 'specialied' focus towards clsuter-analsysis-software).
  //! ----------
        /* ae_int_t n4 = n/4; */
        /* ae_int_t nleft = n%4; */
        /* for(i=0; i<n4; i++, v0+=4, v1+=4) */
        /*     result += v0[0]*v1[0]+v0[1]*v1[1]+v0[2]*v1[2]+v0[3]*v1[3]; */
        /* for(i=0; i<nleft; i++, v0++, v1++) */
  //! ----------
}
