#include "aux_findPartitionFor_tiles.h"


/**
   @brief intiates the lgocial container for the tiled block (oekseth, 06. nov. 2016)
   @param <nrows> is the numberof elements (eg, rows or columns) to be 'used' in the 'tiled portion'.
   @param <SM> is the block-size of each tile: the last 'tiled block' is epxected to have a block-size less-than-or-equal to SM (ie, a lgocisla 'part' of this function).
   @return the initated s_aux_findPartitionFor_tiles_t object.
 **/
s_aux_findPartitionFor_tiles_t initAndGet__s_aux_findPartitionFor_tiles_t(const uint nrows, const uint SM) {
  assert(nrows != 0); assert(nrows != UINT_MAX); assert(SM != 0); assert(SM != UINT_MAX);
  s_aux_findPartitionFor_tiles_t self;
  self.nrows = nrows;
  
  //!
  //! Seperately handle teh different caseS:
  if(nrows <= SM) {
    self.cntBlocks = 1; 
    self.sizeEachBlock_beforeLast = 0;
    self.sizeEachBlock_last = nrows;
  } else {
    const uint numberOf_chunks_minusOne = nrows / SM;
    assert_possibleOverhead(numberOf_chunks_minusOne >= 1);
    const uint size_of_chunksTotal = (SM*numberOf_chunks_minusOne);
    const uint chunkSize_row_last = nrows - size_of_chunksTotal;
    //! -----------
    if(chunkSize_row_last != 0) {
      self.cntBlocks = numberOf_chunks_minusOne + 1;
      self.sizeEachBlock_beforeLast = SM; self.sizeEachBlock_last = chunkSize_row_last;
    } else { //! otherwise we assume that 'all SM-blocks fits into nrows':
      self.cntBlocks = numberOf_chunks_minusOne;
      self.sizeEachBlock_beforeLast = SM; self.sizeEachBlock_last = SM;
    }
    //! -----------
#ifndef NDEBUG //! then 'add' an internal ciorrectness-evaluation:
    { //! Validate that the totcal number of blocks are correct, ie, usign our 'itnenral funciton':
      uint cnt_rows_evaluated = 0;
      for(uint block_id = 0; block_id < self.cntBlocks; block_id++) {
	cnt_rows_evaluated += get_blockSize__s_aux_findPartitionFor_tiles_t(&self, block_id);
      }
      assert(cnt_rows_evaluated == nrows);
    }
    { //! Validate wrt. a 'simple summation':
      const uint cnt_rows_evaluated = get_numberOfCellsToEvaluate__s_aux_findPartitionFor_tiles_t(&self);
      assert(cnt_rows_evaluated == nrows);      
    }
#endif
  }
  //printf("(object:SM=%u)\t nrows=%u, cntBlocks=%u, block-size=(%u, %u), at %s:%d\n", SM, nrows, self.cntBlocks, self.sizeEachBlock_beforeLast, self.sizeEachBlock_last, __FILE__, __LINE__);
  //! -------------------------------------------
  //!
  //! Complete:
  return self;
}

#ifndef NDEBUG
//! A fucntion used to vlaidate correctness of this structure.
void debug_applyInternalCorrectnessTests() {
  
  //! Note: in below we eplroe different partiion-strategies through a 'permtuation' of different cases: we leave the correntes-sevlauatsion to the each of our [”elow] 'inti-routiens':
  s_aux_findPartitionFor_tiles_t self;
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/3, /*SM=*/16);
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/9, /*SM=*/16);
  //! ---
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/15, /*SM=*/16);
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/16, /*SM=*/16); //! ie, a 'complete block-size-case'.
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/17, /*SM=*/16);
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/18, /*SM=*/16);
  //! ---
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/31, /*SM=*/16);
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/32, /*SM=*/16); //! ie, a 'complete block-size-case'.
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/33, /*SM=*/16);
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/34, /*SM=*/16);
  //! ---
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/61, /*SM=*/16);
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/62, /*SM=*/16);
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/64, /*SM=*/16); //! ie, a 'complete block-size-case'.
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/65, /*SM=*/16);
  self = initAndGet__s_aux_findPartitionFor_tiles_t(/*nrows=*/66, /*SM=*/16);
}
#endif
