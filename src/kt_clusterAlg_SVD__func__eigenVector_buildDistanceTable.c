


//  if( (typeOf_not_useEitherOr_sumOf_tailDistances == true) ) {
{
  //    if(powerIncrease_ofDistanceSum == UINT_MAX) { //! then we use 'the toal sum of distances assicated to the tail-vertex'.
  for(uint row_id = 0; row_id < nrows; row_id++) {
    //const t_float *row = matrix[row_id];
    const t_float *__restrict__ row = matrix[row_id];
#if(__config__compute_eigenVector__isTo_useSSE == 0)
    //! ******************************************************************************
    t_float sum = 0;
    for(uint col_id = 0; col_id < ncols; col_id++) {
    //! ******************************************************************************-------: performance-cases wrt. 'static' functions:
#if(__macro__typeOf_performanceTuning == __macro__e_kt_centrality_config__testImpactOfNaiveImplemementation_none)
#if(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_scalar)
      sum += row[col_id];
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_log) 
      sum += (sum != 0) ? mathLib_float_log(row[col_id]) : 0;
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) 
      sum += mathLib_float_pow(row[col_id], self.powerIncrease_ofDistanceSum);
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) 
      sum += mathLib_float_pow(self.powerIncrease_ofDistanceSum, row[col_id]);
#else //! then we need to add a 'handler' for thsi case.
#error "!!\t Add supprot for this case"
#endif      
#else //! then we test wrt. callign 'extenrally inlcuded' functiosn:
      sum += __macro_funcTag__accessScores_distanceTable(matrix, row_id, col_id, row, self);
      //      sum += __kt_centralityMacroDefineFunc_buildDistanceTable__internalFuncBuildDistanceTable(__macro_funcTag__accessScores_distanceTable)(matrix,, row_id, col_id, row, self);
#endif
    //! ******************************************************************************-------: end:performance-cases wrt. 'static' functions:
    }
    //! ******************************************************************************
#else //! thebn we use an SSE-version:
    uint col_id = 0; 

    //! --------------------------------------
    //! Initaite:
#if(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_scalar)
    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOAD(&row[col_id]);
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_log) 
    VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_LOG(VECTOR_FLOAT_LOAD(&row[col_id])); 
    vec_local = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(vec_local); 
    VECTOR_FLOAT_TYPE vec_result = vec_local;
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) 
    VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_POWER(VECTOR_FLOAT_LOAD(&row[col_id]), self.powerIncrease_ofDistanceSum);       
    VECTOR_FLOAT_TYPE vec_result = vec_local;
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) 
    VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_POWER_arg1Scalar_arg2Vec(self.powerIncrease_ofDistanceSum, VECTOR_FLOAT_LOAD(&row[col_id]));       
    VECTOR_FLOAT_TYPE vec_result = vec_local;
      //sum += mathLib_float_pow(powerIncrease_ofDistanceSum, row[col_id]);
#else //! then we need to add a 'handler' for thsi case.
#error "!!\t Add supprot for this case"
#endif
    //! -------------
    //! Iterate:
    col_id += VECTOR_FLOAT_ITER_SIZE;
    for(; col_id < ncols; col_id += VECTOR_FLOAT_ITER_SIZE) {
#if(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_scalar)
      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOAD(&row[col_id]));
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_log) 
      VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_LOG(VECTOR_FLOAT_LOAD(&row[col_id])); 
      vec_local = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(vec_local); 
      vec_result = VECTOR_FLOAT_ADD(vec_result, vec_local);
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) 
      VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_POWER(VECTOR_FLOAT_LOAD(&row[col_id]), self.powerIncrease_ofDistanceSum);       
      vec_result = VECTOR_FLOAT_ADD(vec_result, vec_local);
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) 
      VECTOR_FLOAT_TYPE vec_local = VECTOR_FLOAT_POWER_arg1Scalar_arg2Vec(self.powerIncrease_ofDistanceSum, VECTOR_FLOAT_LOAD(&row[col_id]));       
      vec_result = VECTOR_FLOAT_ADD(vec_result, vec_local);
      //sum += mathLib_float_pow(powerIncrease_ofDistanceSum, row[col_id]);
#else //! then we need to add a 'handler' for thsi case.
#error "!!\t Add supprot for this case"
#endif
    }
    const t_float sum = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
    //! ****************************************************************************** End-of-SSE
#endif
    //! 
    //! Each-row: handle the post-processing-cases for the given row:
#if(__config__typeOf_eigenVectorType_adjustment_postProcessTable == __macro__e_kt_eigenVectorCentrality_postProcess_result)
    mapOf_vertexSums[row_id] = sum;
#elif(__config__typeOf_eigenVectorType_adjustment_postProcessTable == __macro__e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result)
    mapOf_vertexSums[row_id] = (sum != 0) ? 1/sum : 0;
#elif(__config__typeOf_eigenVectorType_adjustment_postProcessTable == __macro__e_kt_eigenVectorCentrality_postProcess_1_dividedBy_result_log)
    mapOf_vertexSums[row_id] = (sum != 0) ? 1/mathLib_float_pow(sum, self.powerIncrease_ofDistanceSum) : 0;
#elif(__config__typeOf_eigenVectorType_adjustment_postProcessTable == __macro__e_kt_eigenVectorCentrality_postProcess_log)
    mapOf_vertexSums[row_id] = (sum != 0) ? mathLib_float_log(sum) : 0;
#elif(__config__typeOf_eigenVectorType_adjustment_postProcessTable == __macro__e_kt_eigenVectorCentrality_postProcess_logPlussOne)
    mapOf_vertexSums[row_id] = (sum != -1) ? mathLib_float_log(sum + 1) : 0;
#else  //! then we need to add a 'handler' for thsi case.
#error "!!\t Add supprot for this case"
#endif
  }
}
/* } else { //! then we use 'power': */
/*   for(uint row_id = 0; row_id < nrows; row_id++) { */
/*     const t_float *__restrict__ row = matrix[row_id]; */
/* #if(__config__compute_eigenVector__isTo_useSSE == 0) */
/*     t_float sum = 0; */
/*     for(uint col_id = 0; col_id < ncols; col_id++) { */
/*       sum += mathLib_float_pow(row[col_id], powerIncrease_ofDistanceSum); */
/*     } */
/*     mapOf_vertexSums[row_id] = sum; */
/* #else */
/*     uint col_id = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOAD(&row[col_id]); col_id += VECTOR_FLOAT_ITER_SIZE; */
/*     for(; col_id < ncols; col_id += VECTOR_FLOAT_ITER_SIZE) { */
/*       vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_POWER(VECTOR_FLOAT_LOAD(&row[col_id]), powerIncrease_ofDistanceSum)); */
/*     } */
/*     mapOf_vertexSums[row_id] = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); */
/* #endif */
/*   } */
/*  } */
/* } */
