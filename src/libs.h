#ifndef libs_h
#define libs_h
/**
   @file
   @brief Specifies commonly used libraries.
   @ingroup common
   @author Ole Kristian Ekseth (oekseth)
   @date 29.12.2011 by oekseth (initial)
**/ 
#ifdef __cplusplus
extern "C"
#endif
/* #ifdef __cplusplus */
/* extern "C" { */
/* #endif */

/* #ifndef configure_globalProject_NOT_INCLUDE_configuration //! eg, to be used when integraitn/combinging "ANSI C" libraryes (oekseth, 06. setp. 2016) */
/* #include "../configure.h" */
/* #endif */

// #include "configure_cCluster.h"

#ifndef SWIG
#include <stdarg.h>
#ifdef __cplusplus
//#include <cstring> //! commented out on 06.11.2020 as a compilation-error occured 
#include <cstdlib>
#include <cstdio>
#include <cctype>
#endif
#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <math.h>

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#endif //! #ifndef SWIG

#ifndef optimize_parallel_singleThreaded //! which may be used to simplify compiaoitnsl on different operaitng systems, and for cases where applsim is not needed (oekseth, 06. feb. 2018)
#define optimize_parallel_singleThreaded 1
 #endif


//#if !defined(__APPLE__) && !defined(__sun)
// #if(0 == 1)
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
//#include <syscall.h>
#include <mpi.h>
#endif
#ifdef USE_MPI
#endif

#ifndef SWIG
#include <unistd.h>
#include <sys/types.h>
#endif // #ifndef SWIG
#ifdef __cplusplus
//#include <cmph.h>
#include <queue> // a FIFO to retrieve the file locations
#include <stack>
#include <map> // the c++ hash map to use
//#include "tbb.h"
#include <list>
// Degubber
//#include "libfence.a"
using namespace std;
//#include "error_handling.h"
#endif
#if defined(__APPLE__) || defined(__sun) 
//#if !defined(__APPLE__) && !defined(__sun) 
#define isnanf(value) (value != value) //! ie, a dummy wrapper, eg, to addres sue-case observed at ["https://stackoverflow.com/questions/2249110/how-do-i-make-a-portable-isnan-isinf-function"], though first reported by "Ole Vegard Solberg (SINTEF)" (oekseth, 06. feb. 2018).
#define isnan(value) (value != value) //! ie, a dummy wrapper, eg, to addres sue-case observed at ["https://stackoverflow.com/questions/2249110/how-do-i-make-a-portable-isnan-isinf-function"], though first reported by "Ole Vegard Solberg (SINTEF)" (oekseth, 06. feb. 2018).
#define isinf(value) ( (value > T_FLOAT_MAX) || (value < T_FLOAT_MIN_ABS) ) 
#endif
/* #ifdef __cplusplus */
/* } */
/* #endif */


#endif //! EOF
