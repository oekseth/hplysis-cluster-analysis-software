#ifndef log_clusterC_h
#define log_clusterC_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "CLUSTERC-mem-data-anlysis"
 *
 * "CLUSTERC-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "CLUSTERC-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with ClusterC-mem-data-anlysis. 
 */


//#ifndef configIsCalledFromExternal_software
#include "def_intri.h"
#include "types.h"
#include "configure_cCluster.h"
#ifndef SWIG
#include <time.h>
#include <signal.h>
#include <sys/times.h>
#include <time.h>
#include <stdio.h>
#include <sys/times.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#endif // #ifndef SWIG
//#endif //! else we assuem the caller uses a 'linker' to include 'these' (oekseth, 06. july 2016).

/**
   @file log_clusterC.h
   @brief a structure and lgoics to capture the details of our CLUSTERC-mem implementation.
   @author Ole Kristian Ekseth (oekseth)
 **/
/**
   @enum e_log_clusterC_typeOf
   @brief a set of ids which is used in our log_clusterC to describe variaous performance-cases.
   @author Ole Kristian Ekseth (oekseth)
   @remarks 
   -- "e_log_clusterC_typeOf_numberOf_updates_fromFunc_updateColumn_details_minBucketUpdate": indicates that for "sinus" data-distributions the tiem-cost of 'imrpviing buckets' is in-signficant.
   -- "e_log_clusterC_typeOf_all_clusterCSpecific_cpy_partialBuckets" wrt. our "clusterC_cluster.c" for n=16,000: data-types["sin":0, "rand":0, ""linear-differentCoeff-a":0].
 **/
typedef enum e_log_clusterC_typeOf {
  //e_log_clusterC_typeOf_all, //! ie, the compelte exeuciton-time of our CLUSTERC-mem algorithm,
  e_log_clusterC_som_all,
  e_log_clusterC_som_init,
  e_log_clusterC_somWorker_all,
  e_log_clusterC_somWorker_init,
  e_log_clusterC_somWorker_step1_all,
  e_log_clusterC_somWorker_step1_STD,
  e_log_clusterC_somWorker_step1_initMask,
  e_log_clusterC_somWorker_step1_initRandom,
  e_log_clusterC_somWorker_step2_all,
  e_log_clusterC_somWorker_step2_distanceMetric,
  e_log_clusterC_somWorker_step2__update_closest_pairs,
  e_log_clusterC_somAssign_all,
  e_log_clusterC_somAssign_distanceMatrix,
  e_log_clusterC_somAssign_iteration,
  //! ---------------------
  e_log_clusterC_kMeans_all,
  e_log_clusterC_kMeans_init,
  e_log_clusterC_kMean_iteration_all,
  e_log_clusterC_kMean_iteration_randomAssign,
  e_log_clusterC_kMean_iteration_findCenter,
  e_log_clusterC_kMean_iteration_findAllDistances,

  //e_log_clusterC_somCluster_,
  //! Note: if new eums are added, then remember to update our "writeOut_object(..)" in our "log_clusterC.c"
  //! ---- 'generic' log-options.
  e_log_clusterC_typeOf_caseEnumeration_1,
  e_log_clusterC_typeOf_caseEnumeration_2,
  e_log_clusterC_typeOf_caseEnumeration_3,
  e_log_clusterC_typeOf_caseEnumeration_4,
  e_log_clusterC_typeOf_caseEnumeration_5,
  e_log_clusterC_typeOf_caseEnumeration_6,
  e_log_clusterC_typeOf_caseEnumeration_7,
  e_log_clusterC_typeOf_caseEnumeration_8,
  e_log_clusterC_typeOf_caseEnumeration_9,
  e_log_clusterC_typeOf_caseEnumeration_10,
  e_log_clusterC_typeOf_caseEnumeration_11,
  e_log_clusterC_typeOf_caseEnumeration_12,
  //! ---------------------------
  e_log_clusterC_typeOf_undef  
} e_log_clusterC_typeOf_t;

//! The __cplusplus macro macro was included in this document by oekseth to make it possible to link to KnittingTools C++-library.
#ifdef __cplusplus
extern "C"{
#endif 


/**
   @struct s_log_clusterC
   @brief a structure and lgoics to capture the details of our CLUSTERC-mem implementation.
   @author Ole Kristian Ekseth (oekseth)
   @remarks among other an object of this struct is used to 'remember' the signficant of different 'operation-types', eg, wrt. the cost of 'value-udpate due to shrinking matrix-size in/from our "s_dense_closestPair_getClosestPair(..)"'.
**/
typedef struct s_log_clusterC {
  struct tms _mapOf_times_internal_tms[e_log_clusterC_typeOf_undef]; //! ie, the number of measurements.
  clock_t _mapOf_times_internal_clock[e_log_clusterC_typeOf_undef]; //! ie, the number of measurements.
  //struct tms _mapOf_times_internal[e_log_clusterC_typeOf_undef]; //! ie, the number of measurements.
  t_float mapOf_times[e_log_clusterC_typeOf_undef];
  uint mapOf_counter[e_log_clusterC_typeOf_undef]; //! ie, counters assicated to a given "e_log_clusterC_typeOf" attribute.
  char *stringOf_description[e_log_clusterC_typeOf_undef];
} s_log_clusterC_t;


  //! Intiaite the object.
  void s_log_clusterC_init(s_log_clusterC_t *obj);
  
  /**
     @brief Assicated a descirption-tag to the log-enum in question.
     @param <self> is the object to update
     @param <enum_id> is attribute to update in obj.
     @param <stringOf_description> is the descritpion to assicated to the log-enum.
  **/
  void s_log_clusterC_setDescription(s_log_clusterC_t *self, const e_log_clusterC_typeOf_t enum_id, char *stringOf_description);
/**
   @brief Start the measurement with regard to time.
   @param <obj> is the object to update
   @param <enum_id> is attribute to update in obj.
**/
  void start_time_measurement_clusterC(s_log_clusterC_t *obj, const e_log_clusterC_typeOf_t enum_id);
/**
   @brief Ends the measurement with regard to time.
   @param <obj> is the object to update
   @param <enum_id> is attribute to update in obj.
**/
  void end_time_measurement_clusterC(s_log_clusterC_t *obj, const e_log_clusterC_typeOf_t enum_id);
  /**
     @brief update the "s_log_clusterC_t" object with counter (oekseth, 06. july 2016).
     @param <self> the object to hold the s_log_clusterC_t object.
     @param <enum_id> the log-type to update
     @param <cnt> if not set to "0" nor "UINT_MAX" then wwe use this valeu to update the assciated s_log_clusterC_t structure     
  **/
  void log_updateCounter_clusterC(s_log_clusterC_t *self, const e_log_clusterC_typeOf_t enum_id, const uint cnt);

  //! Specify the type of cluster-layouts which are to be 'written out'.
  typedef enum e_logCluster_typeOf_writeOut {
    e_logCluster_typeOf_writeOut_SOM,
    e_logCluster_typeOf_writeOut_kMeans,
    e_logCluster_typeOf_writeOut_cmpCases_6x,
    e_logCluster_typeOf_writeOut_cmpCases_12x,
    e_logCluster_typeOf_writeOut_undef
  } e_logCluster_typeOf_writeOut_t;

  /**
     @brief write out the resutls of the log_clusterC object.
     @param <obj> is the object to write out the contents for
     @param <stream> is the stream to write the result to, eg, STDOUT
     @param <typeOf_logMessage> idnetiies the type of lgo-message which is to ge generated.
     @param <stringOf_description> which if set is used as an 'identifier' wrt. the log-message
   **/
  void writeOut_object_clusterC(const s_log_clusterC_t *obj, FILE *stream, const e_logCluster_typeOf_writeOut_t typeOf_logMessage, const char *stringOf_description /* = NULL */);

#ifdef __cplusplus
}
#endif
#endif //! EOF

