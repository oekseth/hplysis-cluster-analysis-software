/**
   @bbiref cmptue for "self->cluster_cluster" and "self->cluster_cluster__count" matrices:
   @remarks wrt. perofmrance-otpmziaotn the following obserivaonts are of interest/improtance:
   -- sparseness: verrtex-clsuter-embershsip are sparse, ehnce a sgiciant eprofmrance-ovrhead may be incurred/applied for simlarity-metirc-based appraoches .
 **/

      uint max_clusterCount = 0; for(uint row_id = 0; row_id < self->nrows; row_id++) {max_clusterCount = macro_max(max_clusterCount, map_vertexToCluster[row_id]);} max_clusterCount++;
      assert(self->cnt_cluster == max_clusterCount);
      //!
      //! Iterate: for each cluster-combination:
//! Note: in [”elow] we compute for: $d(v_i \in C_k, v_m \in C_x)$, ie, for which we are in need/interest of all pariiwse simarlity-metrics
      for(uint cluster_id_in = 0; cluster_id_in < self->cnt_cluster; cluster_id_in++) {
	uint mappings_cluster1_size = 0; uint *mappings_cluster1 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_in, &mappings_cluster1_size);
	if(mappings_cluster1_size > 0) {
	  assert(mappings_cluster1); assert(matrix);
/* 	  if(config.isTo_use_KD_treeInSimMetricComputations) { */
/* 	    //! Then idneitfy the siamrlity through iteration/travesal of KD-tree and rank-seleciton. */
/* 	    //! */
/* 	    //! Idneitfy the min-set of vertices  */
/* 	    for(uint i = 0; i < mappings_cluster1_size; i++) { */
/* 	      const uint ind1 = mappings_cluster1[i]; assert(ind1 != UINT_MAX); */
/* 	      assert(false); // FIXME: add for this ... and vlaidte/investiagte correncess. */
/* 	      assert(false); // FIXME: write new data-strucutres .... and inlcude [below]. */
/* #if(0 == 1) */
/* 	      for(uint k = 0; k < cnt_tails; k++) { */
/* 		//! */
/* 		//! */
/* 		const uint ind2 = ; */
/* 		assert(false); // FIXME: do soemthing */
/* 	      } */
/* #endif */
/* 	    } */
/* 	    //!  */
/* 	    //! Compute score and apply/use a post-interaiton-merge-strategy: */
/* 	    for(uint cluster_id_out = 0; cluster_id_out < cnt_clusters; cluster_id_out++) { */
/* 	      assert(false); // FIXME: write new data-strucutres .... and inlcude [below]. */
/* #if(0 == 1) */
/* 		//! */
/* 		//! Compute the score: */
/* 		__MiFd //! ie, comptue the score. */
/* 		  //	      const t_float score = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(tmpArr, tmpArr__size); */
/* 		  const uint clus_in  = cluster_id_in;    const uint clus_out = cluster_id_out; */
/* 		self->cluster_cluster[clus_in][clus_out] = score; */
/* 		// TODO: vlaidate this assumption ... ie, that we are Not to adjust the count 'relative' the the STD-value (oekseht, 06. feb. 2017). */
/* 		self->cluster_cluster__count[clus_in][clus_out] = 1; */
/* 		//! De-allocate: */
/* 		assert(tmpArr); free_1d_list_float(&tmpArr); */
/* #endif */
/* 	    } */
/* 	  } else */ {
	    for(uint cluster_id_out = 0; cluster_id_out < self->cnt_cluster; cluster_id_out++) {
	      // if(cluster_id_out == cluster_id_in) {continue;} //! ie, as we then asusme this case is eeprately evlauated/consiered 
	      uint mappings_cluster2_size = 0;
	      uint *mappings_cluster2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_out, &mappings_cluster2_size); //! ie, the vertices in the clsuter(i) set.
	      // TODO[article]: cosnider a 'different appraoch' wrt. this .... eg, considering a 'permtaution' of our "clusterdistance__extensiveParams__enumMethod__simplified(..)" call.
	      if(mappings_cluster1_size && mappings_cluster2_size) {
		assert(matrix); 
		//! Allocate space:
		const long long int tmpArr__size = mappings_cluster2_size * mappings_cluster1_size;
		const t_float empty_0 = 0;
		t_float *tmpArr = allocate_1d_list_float(tmpArr__size, empty_0);
		assert(tmpArr__size > 0); assert(tmpArr);
		long long int current_pos = 0;
		//! Iterate
		for(uint i = 0; i < mappings_cluster1_size; i++) {
		  const uint ind1 = mappings_cluster1[i]; assert(ind1 != UINT_MAX);
		  for(uint k = 0; k < mappings_cluster2_size; k++) {
		    const uint ind2 = mappings_cluster2[k]; assert(ind2 != UINT_MAX);
		    t_float score = matrix[ind1][ind2];
		    if(config.metric_complexClusterCmp_vertexCmp_isTo_use == true) { //! then we apply a 'complex comparison-strategy':
		      if(config.isTo_use_KD_treeInSimMetricComputations) { 
			//! Then idneitfy the siamrlity through iteration/travesal of KD-tree and rank-seleciton. 
			//! Note: strategy: to first udpate a glaol list and thereafter comptue/infer simalirty. 
			//! Note: we update a data-strucutre "[cluster-id][cluster-id]=tmpArr=[scores]", and thereafter apply/use a post-rpcoessing for 'this'.
			//! Note: store for "[cluster-id][row-id]=tmpArr=[scores]".
			assert(false); // FIXME: validate [”elow]:
			assert(false); // FIXME: add code
		      } else {
			assert(false); // FIXME: validate [”elow]:
			score = kt_compare__each__maskImplicit(config.metric_complexClusterCmp_obj.metric_id, config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, self->nrows, self->nrows, matrix, matrix, /*row_id=*/ind1, /*col_id=*/ind2);
		      }
		      // printf("score[%u][%u]=%f, at %s:%d\n", row_id, col_id, score, __FILE__, __LINE__);
		    }
		    //! Update:
		    tmpArr[current_pos++] = score;
		  }
		}
		//!
		//! Compute the score:
		__MiFd //! ie, comptue the score.
		  //	      const t_float score = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(tmpArr, tmpArr__size);
		  const uint clus_in  = cluster_id_in;    const uint clus_out = cluster_id_out;
		self->cluster_cluster[clus_in][clus_out] = score;
		// TODO: vlaidate this assumption ... ie, that we are Not to adjust the count 'relative' the the STD-value (oekseht, 06. feb. 2017).
		self->cluster_cluster__count[clus_in][clus_out] = 1;
		//! De-allocate:
		assert(tmpArr); free_1d_list_float(&tmpArr);
	      }
	    }
	  }
	}
      }
//if(false) 
{ // FIXME: vlaidte dorrectnes sof this ... ie, that [abov]€ 'covers' the [below] lgoci ... for which there si No need to call/use [”elow] (oesketh, 06. jun. 2017).

  if(config.isTo_use_KD_treeInSimMetricComputations) {
    
    assert(false); // FIXME: try/consider to mege/unify [”elow] with [ªbove] ..... ie, seems like [below] is rendundant .... ie, as we in [”elow] comptue for $d(v_i \in C_k, v_m \in C_k)$ .... ie, where the 'stroage' of these scores should be sufficent .... ie, a 2d-list .... 

  } 

  //!
  //! Iterate: from each vertex sepeartely to each of the clusters:
  //! Note: we compute for: [v_k][\v_i \in C_x] .... ie, to store for all vertices [row-id][cluster-id]=vertex-score
      for(uint row_id = 0; row_id < self->nrows; row_id++) {	
	for(uint cluster_id_in = 0; cluster_id_in < self->cnt_cluster; cluster_id_in++) {
	  uint mappings_cluster2_size = 0;
	  uint *mappings_cluster2 = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->mappings_cluster_toVertex), cluster_id_in, &mappings_cluster2_size);
	  // TODO[article]: cosnider a 'different appraoch' wrt. this .... eg, considering a 'permtaution' of our "clusterdistance__extensiveParams__enumMethod__simplified(..)" call.
	  if(mappings_cluster2_size) {
	    assert(matrix); 
	    //! Allocate space:
	    const long long int tmpArr__size = mappings_cluster2_size;
	    const t_float empty_0 = 0;
	    t_float *tmpArr = allocate_1d_list_float(tmpArr__size, empty_0);
	    assert(tmpArr__size > 0); assert(tmpArr);
	    long long int current_pos = 0;
	    //! Iterate
	    const uint ind1 = row_id;
	    for(uint k = 0; k < mappings_cluster2_size; k++) {
	      const uint ind2 = mappings_cluster2[k]; assert(ind2 != UINT_MAX);
	      t_float score = matrix[ind1][ind2];
	      if(config.metric_complexClusterCmp_vertexCmp_isTo_use == true) { //! then we apply a 'complex comparison-strategy':
		assert(false); // FIXME: vlaidte correnctess of [”elow] wrt. teh "self->nrows, self->nrows" param-call (ie, consider instead using 'cols') .... nd udpate siamlirt calls.
		score = kt_compare__each__maskImplicit(config.metric_complexClusterCmp_obj.metric_id, config.metric_complexClusterCmp_obj.typeOf_correlationPreStep, self->nrows, self->nrows, matrix, matrix, /*row_id=*/ind1, /*col_id=*/ind2);
		// printf("score[%u][%u]=%f, at %s:%d\n", row_id, col_id, score, __FILE__, __LINE__);
	      }
	      //! Update:
	      tmpArr[current_pos++] = score;
	    }

	    //!
	    //! Compute the score:
	      __MiFd //! ie, comptue the score.
	    //const t_float score = STD__get_forRow_sampleDeviation__matrix_deviation_implicitMask(tmpArr, tmpArr__size);
	    const uint clus_in  = map_vertexToCluster[row_id]; 	    const uint clus_out = cluster_id_in;
	    if( (clus_in >= self->cnt_cluster)) {continue;} //! ie, as we then asusme that no well-defiend clsuters were foudn for these two vertices.
	    if( (clus_out >= self->cnt_cluster)) {continue;} //! ie, as we then asusme that no well-defiend clsuters were foudn for these two vertices.
	    assert_possibleOverhead(self->vertexTo_clusters);
	    self->vertexTo_clusters[row_id][clus_out] = score;
	    //self->cluster_cluster[clus_in][clus_out] = score;
	    // TODO: vlaidate this assumption ... ie, that we are Not to adjust the count 'relative' the the STD-value (oekseht, 06. feb. 2017).
	    //self->cluster_cluster__count[clus_in][clus_out] = 1;
	    //! De-allocate:
	    assert(tmpArr); free_1d_list_float(&tmpArr);
	  }
	}
      }
 }
#undef __MiFd
