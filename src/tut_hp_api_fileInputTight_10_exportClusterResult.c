#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis

/**
   @brief examplify how our "hp_api_fileInputTight.h" may be combined with our "kt_clusterResult_condensed.h" to consturct a consnsed set of cluster-attriubtes, where latter may be used in our "hpLysisVisu.js" wrt. data-visauzliaitons. 
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks focus on data-export of cluster-results, using a small 'easy-verifiable' data-set as input:
   -- input: in this example we use a 'hard-coed' data-input-file
   -- logic: apply all of the 'defualt' clsuter-algorithms supported by hpLysis: uste the 'extensive-input-param "apply__clustAlg__extraArgs__hp_api_fileInputTight(..)"  fucntion when comptuing clusters.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 
  //!
  /* //! Define an input-file to use in analysis: */
  /* const char *file_name = "data/local_downloaded/iris.data.hpLysis.tsv"; //! ie, the often-used IRIS data-set ("https://en.wikipedia.org/wiki/Iris_flower_data_set").  */

  /* const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_HCA_max; */
  /* const uint ncluster = 3; const char *stringOf_ncluster = "3"; */
  /* //! Load the TSV-file: */
  /* //! Note: advanced options in data-loading conserns noise-injection, both wrt. 'noise-functions' and 'merging of different data-sets'. */

  //!
  //! Construct a sample-data-set, and then generate results using the dfifernet clusteirng-algorihtm in hpLysis:
#if(1 == 1)
  const uint ncluster = 2; //! ie, use a 'dynamic approach'.
  const uint nrows = 9; const uint ncols = 9; 
  //! The matrix:
  t_float matrix[nrows][ncols] = {
	{0.000000, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.072906},
	{0.321928, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928},
	{0.321928, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928},
	{0.321928, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928},
	{0.321928, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.321928},
	{0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.321928},
	{0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.321928},
	{0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.321928},
	{0.072906, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.000000}
    };
#else
  const uint ncluster = UINT_MAX; //! ie, use a 'dynamic approach'.
  const uint nrows = 5; const uint ncols = 5; 
  //! The matrix:
  t_float matrix[nrows][ncols] = {
    {1, 1, 2, 2, 3},
    {1, 1, 5, 5, 3},
    {9, 9, 2, 2, 7},
    {9, 9, 2, 2, 7},
    {5, 5, 2, 7, 2},
  };
#endif
  s_kt_matrix_t mat_input = initAndReturn__s_kt_matrix(nrows, ncols); 
  //! Set the cells, ie, demosntrates a simple access to oru "s_kt_matrix_t" object:
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      mat_input.matrix[row_id][col_id] = matrix[row_id][col_id];
    }
  }
  //! ------------------------------------------------------------------------------
  //! 
  //! Use-case(1): Apply clustering:
  //! Note: for a comprehensive lsit of the clsuter-algorithm "metric" used in below, see our "hpLysis_api.h".
  //! ------
  //! Compute using a "Hierarchical Cluster Algorithm (HCA)":
  //! Note: in below call we implictly use a CCM-based appraoch to 'collect' k-means-clusters for HCA:
#if (1 == 1)
    {
      //const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_kCluster__SOM;
      //      const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_kCluster__AVG;
      const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_HCA_max;
#else
    for(uint clust_id = 0; clust_id < e_hpLysis_clusterAlg_undef; clust_id++) {
    const e_hpLysis_clusterAlg_t enum_id = (e_hpLysis_clusterAlg_t)clust_id; 
#endif
 //! ie, comptue using the compelte set of the 'base' clsuter-alogirhtm in the hpLysis software
    //  {
    //const e_hpLysis_clusterAlg_t enum_id = e_hpLysis_clusterAlg_HCA_max;
    s_kt_matrix_t mat_cluster_hca = setToEmptyAndReturn__s_kt_matrix_t(); //! ie, intiate.
    //!
    //! Intalise the clsuter-attribute-result-object:
    s_kt_clusterResult_condensed_t result_clusterMemberships; initSetToEmpty__s_kt_clusterResult_condensed_t(&result_clusterMemberships); //! ie, intalise latter.
    //!
    //! Speify the simalirty-metircs to be used: in this example we use Kendall-::Dice in the pre-step while Euclid in the post-step.
    s_kt_correlationMetric_t obj_metric_pre = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_rank_kendall_Dice, e_kt_categoryOf_correaltionPreStep_none);
    s_kt_correlationMetric_t obj_metric_inside = initAndReturn__s_kt_correlationMetric_t(e_kt_correlationFunction_groupOf_minkowski_euclid, e_kt_categoryOf_correaltionPreStep_none);
    //!
    //! Apply cluster-logics: 
    bool is_ok = apply__clustAlg__extraArgs__hp_api_fileInputTight(enum_id, obj_metric_pre, obj_metric_inside, ncluster, ncluster, 0.2, /*max-iter=*/100, &mat_input, /*result=*/&mat_cluster_hca, &result_clusterMemberships, /*isTo_inMatrixResult_storeSimMetricResult=*/true, /*isTo_useSimMetricInPreStep=*/true);
    //bool is_ok = apply__clustAlg__hp_api_fileInputTight(/*metric=*/enum_id, /*nclusters=*/ncluster, &mat_input, /*result=*/&mat_cluster_hca);
    assert(is_ok); //! ie, what is expected.
    // printf("nrows=%u, ncols=%u, at%s:%d\n", mat_cluster_hca.nrows, mat_cluster_hca.ncols, __FILE__, __LINE__)
    //!
    //! Export cluster-result:
    //! Note: first we need to speify the string-vairbles: to simplify maitnance and speficiaotn we use a macor-apram for this task:
    allocOnStack__char__sprintf(2000, resultFile_tsv, "sampleResult_alg_%u.tsv", enum_id);  //! ie, allcoate the "str_filePath".
    allocOnStack__char__sprintf(2000, resultFile_js, "sampleResult_alg_%u.js", enum_id);  //! ie, allcoate the "str_filePath".
    //! Export: TSV-tab
    is_ok = export__toFormat__tsv__s_kt_clusterResult_condensed_t(&result_clusterMemberships, resultFile_tsv, "\t", "\n");
    assert(is_ok);
    //! Export: java-script
    is_ok = export__toFormat__javaScript__s_kt_clusterResult_condensed_t(&result_clusterMemberships, resultFile_js);
    assert(is_ok);
    //! Write out the constructed siimlairty-matrix:
    allocOnStack__char__sprintf(2000, resultFile_tsv_simMatrix, "sampleResult_matrix_alg_%u.tsv", enum_id);  //! ie, allcoate the "str_filePath".
    is_ok = export__singleCall__s_kt_matrix_t(&mat_cluster_hca, /*result-file=*/resultFile_tsv_simMatrix, NULL); assert(is_ok); //! ie, what is expected.
    /* free__s_kt_matrix(&mat_computedSim); */
    /* free__s_kt_matrix(&mat_cluster_kMeans); */
    free__s_kt_matrix(&mat_cluster_hca);
    free__s_kt_clusterResult_condensed_t(&result_clusterMemberships);
  }
  //! 
  //! De-allocate:
  free__s_kt_matrix(&mat_input);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
