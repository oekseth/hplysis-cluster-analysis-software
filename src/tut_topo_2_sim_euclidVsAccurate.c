//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kd_tree.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hpLysis_api.h"//! to define "hpLysis__globalInit__kt_api()"
#include "hp_clusterFileCollection.h"
#include "matrix_deviation.h"
//
#include "hp_frameWork_CCM_boundaries.h" //! sed to structually generate different cluster-partions.
// ***********************************************************
#include "kt_storage_resultOfClustering.h"
#include "kt_distributionTypesC.h" //! which simplfies access to distrubion-types.
// ***********************************************************



typedef struct s_to2 {
  FILE *file_out;
  bool header_isAdded;
  bool export_raw_data;
} s_to2_t;

static s_to2_t init_s_to2_t(const char *file_name) {
  //!
  s_to2_t self;
  //!
  self.file_out = fopen(file_name, "wb");
  self.header_isAdded = false;
  self.export_raw_data = false;
  //!
  printf("(info)\t Log-file generated at %s [from [%s]:%s:%d\n]", file_name, __FUNCTION__, __FILE__, __LINE__);
  //!
  assert(self.file_out);
  /* //! */
  /* { //! Add header: */
  /*   const char *label_data = "#! row-name:"; */
  /*   const char *label_data_dim_rows = "Rows"; */
  /*   const char *label_data_dim_cols = "Columns"; */
  /*   const char *str_case_label = "Algorithm"; */
  /*   const char *config_minCnt = "DBSCAN-configuration"; */
  /*   const char *time_total = "Time"; */
  /*   // const char * = "";      */
  /*   fprintf(stdout, "%s\t%s\t%s\t%s\t%s\t%s\n", label_data, label_data_dim_rows, label_data_dim_cols, str_case_label, config_minCnt, time_total); */
  /*   fprintf(self.file_out, "%s\t%s\t%s\t%s\t%s\t%s\n", label_data, label_data_dim_rows, label_data_dim_cols, str_case_label, config_minCnt, time_total); */
  /* } */
  // FIXME: close thsi object
  //! 
  return self;
}

static void free_s_to2_t(s_to2_t *self) {
  fclose((self->file_out));
  self->file_out = NULL;
}

/* static void _add_to_result(s_to2_t *self, const char *label_data, const char *str_case_label, const float time_total, const uint config_minCnt, s_kt_matrix_t *mat_input) { */
/*   assert(self->file_out); */
/*   fprintf(self->file_out, "%s\t%u\t%u\t%s\t%u\t%.2f\n", label_data, mat_input->nrows, mat_input->ncols, str_case_label, config_minCnt, time_total); */
/* } */


static void to2_analyze_matrix(s_to2_t *self, s_kt_matrix_t *mat_input, const char *stringOf_tagSample) {
  if(self->export_raw_data) {
    {
      //! Then export:
      char file_name[1000]; memset(file_name, '\0', 1000); sprintf(file_name, "r-topo2-%s.tsv", stringOf_tagSample);
      printf("\t Export file:\"%s\", at [%s]:%s:%d\n", file_name, __FUNCTION__, __FILE__, __LINE__);
      export__singleCall__s_kt_matrix_t(mat_input, file_name, NULL);
    }
  }
  
  // FIXME: add something

}

static void to2_analyze_setOf_matrix(s_to2_t *self, const s_kt_correlationMetric_t obj_metric, s_kt_matrix_t *mat_list, const uint mat_input_size, const s_kt_list_1d_string_t *arrOf_stringNames, const char *stringOf_resultFile) { // e_kt_correlationFunction_t sim_pre, const e_hpLysis_clusterAlg clusterAlg, const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t enum_ccm, const char *stringOf_resultFile) {
  assert(mat_list);
  assert(mat_input_size);
  //assert(clusterAlg != e_hpLysis_clusterAlg_undef);
  //!
  //!
  //!
  for(uint data_id = 0; data_id < mat_input_size; data_id++) {	
    s_kt_matrix_t mat_input = mat_list[data_id];
    char stringOf_tagSample_local[1000]; memset(stringOf_tagSample_local,  '\0', 1000); sprintf(stringOf_tagSample_local, "data_%u_", data_id);
    const char *stringOf_tagSample = stringOf_tagSample_local; 
    if(arrOf_stringNames) {
      const char *str = getAndReturn_string__s_kt_list_1d_string(arrOf_stringNames, data_id);
      if(str && strlen(str)) {stringOf_tagSample = str;} /*! ie, then use the user-providced data-description to 'set' the string.*/
    }    
    assert(stringOf_tagSample); assert(strlen(stringOf_tagSample));
    //!
    //! Apply:
    to2_analyze_matrix(self, &mat_input, stringOf_tagSample); //, obj_metric);    
  }
  
}


  
//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).
static s_kt_matrix_t to2_readFile(s_hp_clusterFileCollection data_obj, const uint data_id, const uint sizeOf__nrows, const uint sizeOf__ncols, const bool config__isTo__useDummyDatasetForValidation) {
  //const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
  const char *stringOf_tagSample = data_obj.file_name;
  //const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  data_obj.tag;}
  //printf("(data=%u)\t#\t[algPos=%u]\t alg_id=%u, rand_id=%u\t\t %s \t at %s:%d\n", data_id, cnt_alg_counts, alg_id, rand_id, stringOf_tagSample, __FILE__, __LINE__);
  assert(stringOf_tagSample);
  
  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(config__isTo__useDummyDatasetForValidation == false) {
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) {
	fprintf(stderr, "!!\t Unable to read file=\"%s\" w/tag=\"%s\", at %s:%d\n", data_obj.file_name, data_obj.tag, __FILE__, __LINE__);
	assert(false); }
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else { //! then we inveeigate using a dummy data-set:
    obj_matrixInput = initAndReturn__s_kt_matrix(sizeOf__nrows, sizeOf__ncols);
    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
      for(uint k = 0; k < obj_matrixInput.ncols; k++) {
	obj_matrixInput.matrix[i][k] = (t_float)(i*k);
      }
    }
  }
  return obj_matrixInput;
}


static void to2_apply(s_to2_t *self, const s_kt_correlationMetric_t obj_metric, const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *resultPrefix) {
  assert(mapOf_realLife); assert(mapOf_realLife_size > 0);
  //!
  //! Transform data-set to a differnet foramt: 
  s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0);  
  s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t();
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
    s_kt_matrix_t obj_matrixInput = to2_readFile(mapOf_realLife[data_id], data_id, 10, 10, /*config__isTo__useDummyDatasetForValidation*/false); //, self->sizeOf__nrows, ->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
    //!
    //! 
    assert(obj_matrixInput.nrows > 0);
    const char *tag = mapOf_realLife[data_id].tag;
    assert(tag); assert(strlen(tag));
    //! Add: string:
    set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag);
    //! Add: matrix:
    assert(mat_collection.list[data_id].nrows == 0); //! ie, to avoid the need for de-allocation.
    mat_collection.list[data_id] = obj_matrixInput; //! ie, copy the cotnent.
  }
  //!
  //! Apply logics: 
  to2_analyze_setOf_matrix(self, obj_metric, mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix);
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(&mat_collection);
  free__s_kt_list_1d_string(&arrOf_stringNames);
}


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief provides a template for explroing the time-cost-effects of DBSCAN-strateiges, and its cofnigurations   
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017 and 06. dec. 2020).
   @remarks 
   -- compile: g++ -I../src/  -O2  -mssse3   -L ../src/  tut_topo_2_sim_euclidVsAccurate.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC -o x_to2
   @remarks logics:
   -- print matrix: supports exporting raw data.
   @fixme add support for:
   % FIXME: how to compare with Eucldean simliarty?
   -- 
   -- 
   -- 
   -- 
   -- 
**/
int main()
  #endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif

  //!
  const char *file_name = "r-to2.tsv";
  //! Init result-file:
  s_to2_t self = init_s_to2_t(file_name);
  self.export_raw_data = true;
  
  s_kt_correlationMetric_t obj_metric = setTo_default__andReturn__s_kt_correlationMetric_t();

  { //! Load for different real-life data-sets:
#include "tut_kd_3_data_simMetric__configFileRead.c"  //! which is used to cofnigure fiel-reading process, adding anumorus diffenre toptiosn.
    const bool isTo_transposeMatrix = false;
    const char *resultPrefix = "tut-to-2";  
    {
      const uint config__kMeans__defValue__k__min = 2;   const uint config__kMeans__defValue__k__max = 4;
      const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min;   
#include "tut__aux__dataFiles__mathFunctions__noise.c" //! ie, the file which hold the cofniguraitosn to be used.
      //!
      //!
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      to2_apply(&self, obj_metric, mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset); //, /*rowName_identifiesClusterId=*/false, isTo_transposeMatrix);   
    }
  }
  
  free_s_to2_t(&self);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}
  

