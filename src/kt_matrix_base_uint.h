#ifndef kt_matrix_base_uint_h
#define kt_matrix_base_uint_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_matrix_base_uint
   @brief a wrapper to provide simplfied access to an itnernal matrix (oekseth, 06. feb. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for a programming-API pelase see our "kt_api.h" header-file
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/


//#include "type_2d_float_nonCmp_uint.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"

/**
   @struct s_kt_matrix_base_uint
   @brief a abbreviated verison of our "s_kt_matrix_t" object (oekseth, 06. feb. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
 **/
typedef struct s_kt_matrix_base_uint {
  uint nrows; 
  uint ncols; 
  uint **matrix;
  bool matrix__isAllocated; // = false;
} s_kt_matrix_base_uint_t;

//! @return an empty matrix:
s_kt_matrix_base_uint_t initAndReturn__empty__s_kt_matrix_base_uint_t();
//! Intiates a new amtrix
s_kt_matrix_base_uint_t initAndReturn__s_kt_matrix_base_uint_t(const uint nrows, const uint ncols);
//! Intiates a new amtrix
s_kt_matrix_base_uint_t initAndReturn__notAllocate__s_kt_matrix_base_uint_t(const uint nrows, const uint ncols, uint **matrix);
//! Intiates a new amtrix from a "super" matrix using the vertices defined in "arrOf_verticesSub"
s_kt_matrix_base_uint_t initAndReturn__fromSubset__s_kt_matrix_base_uint_t(const s_kt_matrix_base_uint_t *super, const uint *arrOf_verticesSub, const uint arrOf_verticesSub_size);
//! De-allcoat ehte locallyallcoated object,
void free__s_kt_matrix_base_uint_t(s_kt_matrix_base_uint_t *self);
//! @return a sample-dataset where a 1/rand() is used 
//! @remarks applicaiton is to simplify the writign of test-cases, eg, to examplify "kmenas++" in our "tut_*" examples.
s_kt_matrix_base_uint_t initSampleOfValues__rand__s_kt_matrix_base_uint_t(const uint nrows, const uint ncols);

#endif //! EOF
