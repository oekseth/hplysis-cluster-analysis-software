#include "alg_dbScan_brute_linkedListSet.h"
#include "kt_matrix.h"
#include "kt_clusterAlg_fixed_resultObject.h"

#define UNCLASSIFIED UINT_MAX
#define NOISE UINT_MAX -2

#define CORE_POINT 1
#define NOT_CORE_POINT 0

#define SUCCESS 0
//#define FAILURE -3
// typedef unsigned int uint;


typedef struct dbScanPoint_s dbScanPoint_t;
struct dbScanPoint_s {
  //double x, y, z;
  uint cluster_id;
};



typedef struct traverseNode_s traverseNode_t;
struct traverseNode_s {
  uint index;
  traverseNode_t *next;
};

typedef struct epsilon_neighbours_s epsilon_neighbours_t;
struct epsilon_neighbours_s {
  uint num_members;
  traverseNode_t *head, *tail;
};


static traverseNode_t *create_traverseNode(uint index)
{
  traverseNode_t *n = (traverseNode_t *) calloc(1, sizeof(traverseNode_t));
  if (n == NULL)
    perror("Failed to allocate node.");
  else {
    n->index = index;
    n->next = NULL;
  }
  return n;
}

static void append_at_end(
		   uint index,
		   epsilon_neighbours_t *en)
{
  traverseNode_t *n = create_traverseNode(index);
  assert(n); //! ie, as we otherwise get a segfault.
  if (en->head == NULL) {
    en->head = n;
    en->tail = n;
  } else {
    en->tail->next = n;
    en->tail = n;
  }
  ++(en->num_members);
}

static epsilon_neighbours_t *get_epsilon_neighbours(
					     t_float **covarianceMatrix,
					     uint index,
					     dbScanPoint_t *points,
					     uint num_points,
					     double epsilon)
{
  assert(covarianceMatrix);
  epsilon_neighbours_t *en = (epsilon_neighbours_t *) calloc(1, sizeof(epsilon_neighbours_t));
  assert(en);
  for (int i = 0; i < num_points; ++i) {
    if (i == index)
      continue;
    // FIXME: consider if below is correct:
    // printf("index=%d,%d\n", index, i);
    if( (covarianceMatrix[index][i] < epsilon)) { // [index].z != T_FLOAT_MAX) && (points[i].z != T_FLOAT_MAX) ) {
      append_at_end(i, en);
    }
  }
  return en;
}

void print_epsilon_neighbours(
			      dbScanPoint_t *points,
			      epsilon_neighbours_t *en)
{
  if (en) {
    traverseNode_t *h = en->head;
    while (h) {
      printf("[%d]\n", h->index);
      /* printf("(%lfm, %lf, %lf)\n", */
      /* 	     points[h->index].x, */
      /* 	     points[h->index].y, */
      /* 	     points[h->index].z); */
      h = h->next;
    }
  }
}

static void destroy_epsilon_neighbours(epsilon_neighbours_t *en)
{
  if (en) {
    traverseNode_t *t, *h = en->head;
    while (h) {
      t = h->next;
      free(h);
      h = t;
    }
    free(en);
  }
}

static void spread(
	    t_float **covarianceMatrix,
	    uint index,
	    epsilon_neighbours_t *seeds,
	    uint cluster_id,
	    dbScanPoint_t *points,
	    uint num_points,
	    double epsilon,
	    uint minpts) {
  epsilon_neighbours_t *spread = get_epsilon_neighbours(covarianceMatrix, index, points, num_points, epsilon);
  //dist);
  assert(spread);
  if (spread->num_members >= minpts) {
    traverseNode_t *n = spread->head;
    dbScanPoint_t *d;
    while (n) {
      d = &points[n->index];
      if (d->cluster_id == NOISE ||
	  d->cluster_id == UNCLASSIFIED) {
	if (d->cluster_id == UNCLASSIFIED) {
	  append_at_end(n->index, seeds);
	}
	d->cluster_id = cluster_id;
      }
      n = n->next;
    }
  }  
  destroy_epsilon_neighbours(spread);
}


static int expand(
	   t_float **covarianceMatrix,
	   uint index,
	   uint cluster_id,
	   dbScanPoint_t *points,
	   uint num_points,
	   double epsilon,
	   uint minpts) 
{
  int return_value = NOT_CORE_POINT;
  epsilon_neighbours_t *seeds = get_epsilon_neighbours(covarianceMatrix, index, points, num_points, epsilon); 
  assert(seeds);
  if (seeds->num_members < minpts)
    points[index].cluster_id = NOISE;
  else {
    points[index].cluster_id = cluster_id;
    traverseNode_t *h = seeds->head;
    while (h) {
      points[h->index].cluster_id = cluster_id;
      h = h->next;
    }

    h = seeds->head;
    while (h) {
      spread(covarianceMatrix, h->index, seeds, cluster_id, points,
	     num_points, epsilon, minpts); //, dist);
      h = h->next;
    }

    return_value = CORE_POINT;
  }
  destroy_epsilon_neighbours(seeds);
  return return_value;
}


static void dbscan(
	    t_float **covarianceMatrix,
	    dbScanPoint_t *points,
	    uint num_points,
	    double epsilon,
	    uint minpts)
{
  uint i, cluster_id = 0;
  for (i = 0; i < num_points; ++i) {
    if (points[i].cluster_id == UNCLASSIFIED) {
      if (expand(covarianceMatrix, i, cluster_id, points,
		 num_points, epsilon, minpts) //		 dist) 
	  == CORE_POINT)
	++cluster_id;
    }
  }
}


static void print_points(
		  dbScanPoint_t *points,
		  uint num_points)
{
  uint i = 0;
  printf("Number of points: %u\n"
	 " vertex     cluster_id\n"
	 //	         " x     y     z     cluster_id\n"
	 "-----------------------------\n"
	 , num_points);
  while (i < num_points) {
    const uint cluster_id = (unsigned int)points[i].cluster_id;
    if(cluster_id == UNCLASSIFIED) {
      printf("[%d] 'unclassified'\n", i);
    } else if(cluster_id == NOISE) {
      printf("[%d] 'noise'\n", i);
    } else {
      printf("[%d] %u\n",  i, 
	     //    printf("%5.2lf %5.2lf %5.2lf: %d\n",  points[i].x,   points[i].y, points[i].z,
	     cluster_id);
    }
    ++i;
  }
}

/**
   @brief comptues DBSCAN.
   @param <obj_1> is the covariance simalrity matrix.
   @param <obj_result> holds the resutls of the clustering
   @param <epsilon> where scores less than eplsion is used.
   @param <minpts> the maximum number of poitns which needs to be related to the given vertex 'for it to be sued as a straverse starting point'.
   @return true upon success.
 **/
bool compute__alg_dbScan_brute_linkedListSet(const s_kt_matrix_base_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const t_float epsilon, const uint minpts) {
  if(!obj_1 || !obj_result || (obj_1->nrows == 0) ) {
    fprintf(stderr, "!!\t Input Not properly set: aborts computation. For questions, context Dr. Ekseth at [oekseth@gmail.com]. Observaiotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  if(obj_1->nrows != obj_1->ncols) {
    fprintf(stderr, "!!\t Input Not properly set: the matrix with dimensions=[%u, %u] does Not sem like an adjcency matrix; aborts computation. For questions, context Dr. Ekseth at [oekseth@gmail.com]. Observaiotn at [%s]:%s:%d\n", obj_1->nrows, obj_1->ncols, __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  const uint num_points = obj_1->nrows;
  dbScanPoint_t *points = (dbScanPoint_t *)calloc(num_points, sizeof(dbScanPoint_t));
  assert(points);
  for(uint i = 0; i < num_points; i++) {
    /* points[i].x = i; */
    /* points[i].y = i+1; */
    /* points[i].z = 1; */
    points[i].cluster_id = UNCLASSIFIED;
  }
  t_float **covarianceMatrix = obj_1->matrix;
  assert(covarianceMatrix);
  // points = p;
  // uint num_points = parse_input(stdin, &points, &epsilon, &minpts);
  if (num_points) {
    dbscan((t_float**)covarianceMatrix, points, num_points, epsilon, minpts); //, euclidean_dist);
    if(false) {
      printf("Epsilon: %lf\n", epsilon);
      printf("Minimum points: %u\n", minpts);
      print_points(points, num_points);
    }
  }
  //! 
  //! Find max-forest-count:
  uint max_cntForests = 0;
  for(uint row_id = 0; row_id < num_points; row_id++) {
    if((points[row_id].cluster_id != UNCLASSIFIED) && (points[row_id].cluster_id != NOISE) ) {
      max_cntForests = macro_max(max_cntForests, points[row_id].cluster_id);
    }
  }
  printf("max_cntForests=%d, at %s:%d\n",  max_cntForests, __FILE__, __LINE__);
  max_cntForests++;
  //!
  //! Update the result-object: 
  init__s_kt_clusterAlg_fixed_resultObject_t(obj_result, max_cntForests, num_points);
  for(uint row_id = 0; row_id < num_points; row_id++) {
    if((points[row_id].cluster_id != UNCLASSIFIED) && (points[row_id].cluster_id != NOISE) ) {
      obj_result->vertex_clusterId[row_id] = points[row_id].cluster_id; //! ie, udpat ethe membership
    }
  }
  //! De-allcoate:
  free(points);  
  // 	for(uint forest_id = 0; forest_id < max_cntForests; forest_id++) {
  //!
  //! @return true, ie, as we asusem the oepration was a success.
  return true;
}
#if false
int main(void) {
  // dbScanPoint_t *points;
  double epsilon = 0;
  uint minpts = 0;
  const uint num_points = 3;
  t_float covarianceMatrix[3][3];
  for(uint i = 0; i < num_points; i++) {
    for(uint k = 0; k < num_points; k++) {
      covarianceMatrix[i][k] = 0;
    }
  }
  covarianceMatrix[0][0] = 0;
  covarianceMatrix[0][1] = 2;
  covarianceMatrix[0][2] = 0;
  //! -- 
  covarianceMatrix[1][0] = 0;
  covarianceMatrix[1][1] = 0;
  covarianceMatrix[1][2] = 1;
  //! -- 
  covarianceMatrix[2][0] = 0;
  covarianceMatrix[2][1] = 1;
  covarianceMatrix[2][2] = 0;    
  dbScanPoint_t *points = (dbScanPoint_t *)calloc(num_points, sizeof(dbScanPoint_t));
  assert(points);
  for(uint i = 0; i < num_points; i++) {
    /* points[i].x = i; */
    /* points[i].y = i+1; */
    /* points[i].z = 1; */
    points[i].cluster_id = UNCLASSIFIED;
  }
  // points = p;
  // uint num_points = parse_input(stdin, &points, &epsilon, &minpts);
  if (num_points) {
    dbscan((t_float**)covarianceMatrix, points, num_points, epsilon,
	   minpts); //, euclidean_dist);
    printf("Epsilon: %lf\n", epsilon);
    printf("Minimum points: %u\n", minpts);
    print_points(points, num_points);
  }
  free(points);
  return 0;
}
#endif
