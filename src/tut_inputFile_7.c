#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.

/**
   @brief test effects of matrix-dimensions: build a set of evlauations where we grudally increase the data-size (oekseth, 06. feb. 2017).   
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks a permtuation of "tut_inputFile_3.c".
   @remarks evaluate the exec-time-effect wrt. different dimesnions of the input-amtrix .... eg, [300, 300], [3000, 300], [12000,300], [300, 3000], [300, 12000], [12000, 12000] ...
**/
int main() 
#endif
{

  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! Default configurations:
  s_hp_clusterFileCollection_traverseSpec_t obj_config = initAndReturn__s_hp_clusterFileCollection_traverseSpec_t();
  obj_config.config__isToPrintOut__iterativeStatusMessage = true;
  obj_config.config__performance__testImapcactOf__slowPerformance = false; //! which if used is included iot. test the impact of a naive/slwo distatnce-comptatuioons
  //! ----
  obj_config.pathTo_localFolder_storingEachSimMetric = "results/tut_2/";
  obj_config.nameOf_resultFile__clusterMemberships = "tut_2_result_simMetrics.js";
  obj_config.nameOf_resultFile__clusterMemberships__deviation = "tut_2_result_simMetrics__deviation.js"; //! which provide a 'sense' summary of the simarlity-matrix
  //const char *config__nameOfDefaultVariable = "uniform";
  //!
  //! Defauult clustering-spec (which is applied After the "config_dist_metricCorrType"):
  obj_config.config_arg_npass = 10000; 
  //!
  //! Result data:
  obj_config.config_exportFormat = e_hpLysis_export_formatOf_similarity_matrix__syntax_JS;

  const bool fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  const bool fractionOf_toAppendWith_sampleData_typeFor_columns = 0;

  const uint config__kMeans__defValue__k__min = 2;
  const uint config__kMeans__defValue__k__max = 4;
  //! Specify (if any) a matrix to be cocnated with the list of different data-values: a use-case is to evlauate a clsutering-algorithms ability to seprate/disntiguish between different data-dsitributiosn 
  // FIXME[article]: udpate our aritlce-test wrt. an valuation of [ªbove] ... ie, for diffnenret use-cases.
  const char *config__nameOfDefaultVariable = "uniform";
  //! ---------------------------------------------
  //!
  //! Cofnigruations specific for this example:
  const char *config__nameOfDefaultVariable__2 = "binomial_p005";
  const uint config__matrixDims__minSize = 256;
  const uint config__matrixDims__maxSizeApprox = 12000;
  const uint config__matrixDims__stepMult = 4;
  //! ---------------------------------------------
  const bool globalConfig__isToStore__inputMatrix__inFormat__csv = true; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
#endif //! ie, as we then assume 'this' is defined in the 'cinlsuion-plac'e of this tut-example.
  obj_config.isToStore__inputMatrix__inFormat__csv = globalConfig__isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.
  //obj_config.globalConfig__isToStore__inputMatrix__inFormat__csv = isToStore__inputMatrix__inFormat__csv; //! if "true": a format which is used to evaluate the clsuter-results of our evlauating for other/alternative algorithms and software-implementaitons.


  assert(config__kMeans__defValue__k__max >= config__kMeans__defValue__k__min);
  const uint __config__kMeans__defValue__k____cntIterations = 1 + config__kMeans__defValue__k__max - config__kMeans__defValue__k__min; 
  /* //! -------------------------------------------- */
  /* //! */
  /* //! File-specific cofnigurations:  */
  /* s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t(); */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = false; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows; */
  /* fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;   */
  /* fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file. */
  /* s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config; */
  /* //fileRead_config.isTo_transposeMatrix = true; */
  /* fileRead_config.isTo_transposeMatrix = true; */
  /* //! */
  s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
  //!
  //! We are interested in a more performacne-demanind approach: 
  const uint sizeOf__nrows = 500;
  const uint sizeOf__ncols = 500;
  fileRead_config__syn.imaginaryFileProp__nrows = sizeOf__nrows;
  fileRead_config__syn.imaginaryFileProp__ncols = sizeOf__ncols;
  //!
  fileRead_config__syn.isTo_transposeMatrix = false;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
  fileRead_config__syn.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
  fileRead_config__syn.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
  fileRead_config__syn.fileIsRealLife = false; //! ie, the 'important part' of this.
  //!
  //! Build and specfiy a tempraory matrix which is cocncated with the 'specific' data-distributions:
  s_kt_matrix_fileReadTuning_t fileRead_config__syn__concat = fileRead_config__syn;
  s_kt_matrix_t matrix_concatToAll; setTo_empty__s_kt_matrix_t(&matrix_concatToAll);
  if(config__nameOfDefaultVariable && strlen(config__nameOfDefaultVariable)) {
    matrix_concatToAll = readFromAndReturn__file__advanced__s_kt_matrix_t(/*file-descrptor=*/config__nameOfDefaultVariable, fileRead_config__syn__concat);
    //! 
    //! Update the configuraiton-object:
    assert(matrix_concatToAll.ncols > 0);
    fileRead_config__syn.mat_concat = &matrix_concatToAll;
  }

  const uint __cnt_cases__eachDim = (config__matrixDims__maxSizeApprox - config__matrixDims__minSize)/config__matrixDims__stepMult;

  //! --------------------------
  //!
  assert(__config__kMeans__defValue__k____cntIterations >= 1);
  const uint mapOf_realLife_size = __cnt_cases__eachDim * __cnt_cases__eachDim * __config__kMeans__defValue__k____cntIterations;  //! ie, the number of data-set-objects in [”elow]
  assert(fileRead_config__syn.fileIsRealLife == false);
  s_hp_clusterFileCollection_t mapOf_realLife[mapOf_realLife_size];
  //!
  //! Buidl the cofnigruation-objects:
  uint current_pos = 0;
  for(uint row_id = 1; row_id <= __cnt_cases__eachDim; row_id++) {
    const uint size_rows = row_id * config__matrixDims__minSize * config__matrixDims__stepMult;
    assert(size_rows < config__matrixDims__maxSizeApprox); // TODO: validate correctness of this.
    for(uint col_id = 1; col_id <= __cnt_cases__eachDim; col_id++) {
      //! Budil the sample-string:
      //! Note: though [”elow] will result in a memory-leaksge we assume 'this leakge' will be insigidncat, ie, where 'this leakge' is allowed to reduce the change of the user (ie, you) to be cofnused wr.t how to apply the lgocis we exmaplify (in this code-chunk-example).
      const char empty_0 = 0; const uint char_Def_Size = 1000;
      const uint size_cols = col_id * config__matrixDims__minSize * config__matrixDims__stepMult;
      assert(size_cols < config__matrixDims__maxSizeApprox); // TODO: validate correctness of this.
      char *stringOf_sampleData_type = allocate_1d_list_char(char_Def_Size, empty_0); sprintf(stringOf_sampleData_type, "%s_r%u_c%u", config__nameOfDefaultVariable__2, size_rows, size_cols);
      for(uint k_iter_count = 0; k_iter_count < __config__kMeans__defValue__k____cntIterations; k_iter_count++) {
	const uint k_clusterCount = config__kMeans__defValue__k__min + k_iter_count;
	assert(current_pos < mapOf_realLife_size);
	//!
	//! Set the dimsions wr.t the current object:
	fileRead_config__syn.imaginaryFileProp__nrows = size_rows;
	fileRead_config__syn.imaginaryFileProp__ncols = size_cols;
	//!
	//! Add the object:
	mapOf_realLife[current_pos].tag = stringOf_sampleData_type;
	mapOf_realLife[current_pos].file_name = config__nameOfDefaultVariable__2; //! ie, the mathemtical-sample-da-taset to use.
	mapOf_realLife[current_pos].fileRead_config = fileRead_config__syn;
	mapOf_realLife[current_pos].inputData__isAnAdjcencyMatrix = false;
	mapOf_realLife[current_pos].k_clusterCount = k_clusterCount;
	mapOf_realLife[current_pos].mapOf_vertexClusterId = NULL;
	mapOf_realLife[current_pos].mapOf_vertexClusterId_size = 0;
	mapOf_realLife[current_pos].alt_clusterSpec = e_hp_clusterFileCollection__goldClustersDefinedBy_undef;
	mapOf_realLife[current_pos].metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
	mapOf_realLife[current_pos].metric__insideClust = e_hp_clusterFileCollection_simMetricSet_MINE_Euclid;
	mapOf_realLife[current_pos].clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;
	//
	//mapOf_realLife[current_pos] = {/*tag=*/stringOf_sampleData_type, /*file_name=*/stringOf_sampleData_type, /*fileRead_config=*/fileRead_config__syn, /*fileIsAdjecency=*/false, /*k_clusterCount=*/k_clusterCount, /*mapOf_vertexClusterId=*/NULL, /*mapOf_vertexClusterId_size=*/0, e_hp_clusterFileCollection__goldClustersDefinedBy_undef,/*metric__beforeClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*metric__insideClust=*/e_hp_clusterFileCollection_simMetricSet_MINE_Euclid, /*clusterAlg=*/e_hp_clusterFileCollection__clusterAlg_kMeans};
	//!
	current_pos++;
      }
    }
  }

  //! ----------------------------------
  //! 
  //! Apply Logics:
  const bool is_ok = traverse__s_hp_clusterFileCollection_traverseSpec(&obj_config, mapOf_realLife, mapOf_realLife_size);
  assert(is_ok);

  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  free__s_kt_matrix(&matrix_concatToAll);

  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
