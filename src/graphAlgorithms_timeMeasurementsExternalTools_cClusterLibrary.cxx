#include "graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary.h"

#include <emmintrin.h>
#include <x86intrin.h> //! Note: for eahder-fiels see "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/"
#include "measure.h"
#if(optimize_parallel_singleThreaded == 0) //! where latter is expected to be defined in our "CMakeLists.txt" (oesketh, 06. feb. 2018).
#include <omp.h> // for parallisation using OpenMP
#endif
#include "graphAlgorithms_aux.h"
#include "stdint.h" //! provides support for "uint_fast_64_t"



// static const uint kilo = 1000;
// static clock_t clock_time_start; static clock_t clock_time_end;
// static struct tms tms_start;
// static  struct tms tms_end;
/**
   @brief Start the measurement with regard to time.
   @remarks 
   - Uses the global vairalbe in this file for this prupose.
   - Clears the cache before starting this measurement, causing a small slowdown in the outside performance, though not included in the estimates returned.
**/
// static  void start_time_measurement() {
// #ifndef NDEBUG
//   //! Clears the cache by allocating 100MB of memory- and the de-allocating it:
//   // TODO: Consier adding random access to it for further obfuscating of earlier memory accesses.
//   uint size = 1024*1024*100;
//   char *tmp = new char[size];
//   assert(tmp);
//   memset(tmp, 7, size);
//   delete [] tmp; tmp = NULL; size = 0;
// #endif
//   tms_start = tms();
//   clock_time_start = times(&tms_start);
//   // if(() == -1) // starting values
//   //   err_sys("times error");
// }
// /**
//    @brief Ends the measurement with regard to time.
//    @param <msg> If string is given, print the status information to stdout.
//    @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called.
//    @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called.
//    @return the clock tics on the system since the 'start_time_measurement()' method was called.
// **/
// static float end_time_measurement(const char *msg, double &user_time, double &system_time, const float naive_tickTime = FLT_MAX) {
//   clock_time_end = times(&tms_end); 
//   long clktck = 0; clktck =  sysconf(_SC_CLK_TCK);
//   const clock_t t_real = clock_time_end - clock_time_start;
//   const float time_in_seconds = t_real/(float)clktck;
//   const float diff_user = tms_end.tms_utime - tms_start.tms_utime;
//   if(diff_user) user_time   = (diff_user)/((float)clktck);
//   const float diff_system = tms_end.tms_stime - tms_start.tms_stime;
//   system_time = diff_system/((float)clktck);
//   if(msg) {
//     if( (naive_tickTime == FLT_MAX) || (naive_tickTime == 0) || (time_in_seconds == 0) ) {
//       if(naive_tickTime != 0) {
// 	printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg);
//       } //! else we assume the measurements are of insiginft valeu, ie, omit printing.
//     } else {
//       const float diff = naive_tickTime / time_in_seconds;
//       printf("%.4fx performance-difference: tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg);
//     }
//   }
//   //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg);
//   return time_in_seconds;
// }

// /**
//    @brief Ends the measurement with regard to time.
//    @return the clock tics on the system since the 'start_time_measurement()' method was called.
// **/
// static float end_time_measurement(const char *msg = NULL, const float naive_tickTime = FLT_MAX) {
//   double user_time = 0, system_time=0;
//   return end_time_measurement(msg, user_time, system_time, naive_tickTime);
// }


//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const uint isTo_use_continousSTripsOf_memory = UINT_MAX, const uint isTo_use_transpose = UINT_MAX, const uint isTo_sumDifferentArrays = UINT_MAX, const uint CLS = UINT_MAX, const float naive_tickTime = FLT_MAX ) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Complte the measurement:
  //time.end_time_measurement();
  //! Get knowledge of the memory-intiaition/allocation pattern:
  const char *stringOf_isTo_use_continousSTripsOf_memory = "";
  if(isTo_use_continousSTripsOf_memory == 1) {stringOf_isTo_use_continousSTripsOf_memory = ", memoryAllocatedIdeal=yes";}
  else if(isTo_use_continousSTripsOf_memory == 0) {stringOf_isTo_use_continousSTripsOf_memory = ", memoryAllocatedIdeal=no";}
  //! Get knowledge of the memory-access-pattern:
  const char *stringOf_isTo_use_transpose = "";
  if(isTo_use_transpose == 1) {stringOf_isTo_use_transpose = ", memoryAccessedTransposely=yes";}
  else if(isTo_use_transpose == 0) {stringOf_isTo_use_transpose = ", memoryAccessedTransposely=no";}
  //! Get knowledge of the memory-access-pattern:
  const char *stringOf_isTo_sumDifferentArrays = "";
  if(isTo_sumDifferentArrays == 1) {stringOf_isTo_sumDifferentArrays = ", compareTwoMatrices=yes";}
  else if(isTo_sumDifferentArrays == 0) {stringOf_isTo_sumDifferentArrays = ", compareTwoMatrices=no";}
  float tick_time = FLT_MAX;
  {
    //! Provides an identficator for the string:
    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
    sprintf(string, "(finding %u relations with %s%s%s%s, and CLS=%u)", size_of_array, stringOf_measureText, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    tick_time = end_time_measurement(string, naive_tickTime);
    //time.print_formatted_result(string); 
    delete [] string; string = NULL; 
  } 
  // if(measure_linear) {
  //   //! Provides an identficator for the string:
  //   char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
  //   sprintf(string, "(ideal comparison: finding %u relations with %s%s%s%s, and CLS=%u)", size_of_array, stringOf_measureText, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
  //   //! Prints the data:
  //   time.compare_with_linear_version(measure_linear, string);
  //   delete [] string; string = NULL; 
  // }
  //! @return the tick-time.
  return tick_time;
}

//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements_factor(const uint factor_rows, const uint factor_cols, const char *stringOf_measureText, const uint size_of_array, const uint isTo_use_continousSTripsOf_memory = UINT_MAX, const uint isTo_use_transpose = UINT_MAX, const uint isTo_sumDifferentArrays = UINT_MAX, const uint CLS = UINT_MAX, const float naive_tickTime = FLT_MAX ) {
  assert(stringOf_measureText);

  const uint string_length = 100 + strlen(stringOf_measureText);
  char *string = new char[string_length];
  sprintf(string, "%s w/factor=[%u, %u]", stringOf_measureText, factor_rows, factor_cols);
  const float tick_time = __assertClass_generateResultsOf_timeMeasurements(string, size_of_array, isTo_use_continousSTripsOf_memory, isTo_use_transpose, isTo_sumDifferentArrays, CLS, naive_tickTime);
  delete [] string;
  //! @return the tick-time.
  return tick_time;
}

static const uint arrOf_chunkSizes_size = 12; static const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {250, 500, 750, kilo, 2*kilo, 3*kilo,
											     10*kilo, 100*kilo, kilo*kilo, 
											     5*kilo*kilo, 10*kilo*kilo, 40*kilo*kilo };

static void __compare_cluster_naiveVSimproved_kmeans() {
  printf("\n-------------------------------------------------\n### Start new test-evaluation, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    float **distmatrix = NULL; char **mask = NULL;

    for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
      //! Note: the biggest optimization is seen for factor_rows=[1, 2, 3] w/approximated optimization=2000x .... for [”elow] 'transposed' the latter may be exaplined by the observation that there are relative: (a) fwer 'sperate chunks' to sort; (b) fwer cache-misses assicated to look-ups (when compared to the 'naive' implementaiton); (c) .... 
      // FIXME[article]: update our artilce wrt. [ªbove] observation.
      assert(factor_rows*2 < size_of_array); //! ie, to avoid 'size so f 1'
      const uint factor_cols = factor_rows;
      const uint nrows = size_of_array/factor_rows; const uint ncols = size_of_array * factor_cols;

      // FIXME: experemetn with different values for [below].
      const uint nclusters = 50; 
      const uint npass = size_of_array*size_of_array;

      //if(false) // FIXME: remove
      float relative_tickTime = 0;
      { //! use the naive appraoch (whcih we comapre with):
	//! Note: cpu=1777.8400s for list-size=1000,
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = false; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	assert(false); // FIXME: validate correctness of [”elow] allcaotion
	maskAllocate__makedatamask(/*nrows=*/nclusters, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	//! Compute:
	float error = 0;
	uint *arr_tclusterid = new uint[size_of_array]; uint *arr_clusterid = new uint[size_of_array];  uint *arr_counts = new uint[size_of_array]; uint *arr_mappings = new uint[size_of_array];
	const e_kt_correlationFunction metric_id  = e_kt_correlationFunction_groupOf_squared_Pearson;  const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_binary;
	assert(transpose == false);
	kmeans_or_kmedian__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, /*nrows=*/nrows, /*ncols=*/ncols, distmatrix, mask, NULL, npass,  result, mask_result, arr_clusterid, &error, NULL, arr_counts, arr_mappings, /*isTo_use_mean=*/false, /*isTo_preComputeDistanceMatrix_ifSpeedIncreases=*/false, NULL);
      
	//! De-allocate:
	delete [] arr_tclusterid; delete [] arr_counts; delete [] arr_mappings; delete [] arr_clusterid;
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/nclusters, result, mask_result, isTo_use_continousSTripsOf_memory);    
	//! Write out the result:
	relative_tickTime = __assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Naive all-against-all K-means", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS);
      }
      { //! Copare with the optimal appraoch, though without a 'first round of transpostion':
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 0; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/nclusters, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	float error = 0;
	uint *arr_tclusterid = new uint[size_of_array]; uint *arr_clusterid = new uint[size_of_array];  uint *arr_counts = new uint[size_of_array]; uint *arr_mappings = new uint[size_of_array];
	const e_kt_correlationFunction metric_id  = e_kt_correlationFunction_groupOf_squared_Pearson;  const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_binary;
	assert(transpose == false);
	kmeans_or_kmedian__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, /*nrows=*/nrows, /*ncols=*/ncols, distmatrix, mask, NULL,  npass,  result, mask_result, arr_clusterid, &error, NULL, arr_counts, arr_mappings, /*isTo_use_mean=*/false, /*isTo_preComputeDistanceMatrix_ifSpeedIncreases=*/false, NULL);
      
	//! De-allocate:
	delete [] arr_tclusterid; delete [] arr_counts; delete [] arr_mappings; delete [] arr_clusterid;
	maskAllocate__freedatamask(/*nelements=*/ncols, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/nclusters, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Optimal all-against-all k-means", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);
      }
    }
  }
}



static void  __compare_distance_naiveVSimproved_kendall() {
  printf("\n-------------------------------------------------\n### Start new test-evaluation, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    float **distmatrix = NULL; char **mask = NULL;
    for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
      printf("---------- \t \n");
      //! Note: ... 
      // FIXME[article]: update our artilce wrt. [ªbove] observation.
      assert(factor_rows*2 < size_of_array); //! ie, to avoid 'size so f 1'
      const uint factor_cols = factor_rows;
      const uint nrows = size_of_array/factor_rows; const uint ncols = size_of_array * factor_cols;      
      float relative_tickTime = 0;
      //if(false) // FIXME: remove
      { //! use the naive appraoch (whcih we comapre with):
	//! Note: cpu=1777.8400s for list-size=1000,
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = false; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	//! Compute:
	struct s_allAgainstAll_config config_allAgainstAll; setTo_empty__s_allAgainstAll_config(&config_allAgainstAll); config_allAgainstAll.CLS = CLS; 	config_allAgainstAll.isTo_use_continousSTripsOf_memory = isTo_use_continousSTripsOf_memory; config_allAgainstAll.typeOf_optimization = e_typeOf_optimization_distance_none;
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;
	ktCorr_compute_allAgainstAll_distanceMetric_kendall(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix, mask, mask, NULL, result, transpose, metric_id, &config_allAgainstAll);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	relative_tickTime = __assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Naive all-against-all Kendall", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS);
      }
      { //! Copare with the optimal appraoch, though without a 'first round of transpostion':
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 0; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	struct s_allAgainstAll_config config_allAgainstAll; setTo_empty__s_allAgainstAll_config(&config_allAgainstAll); config_allAgainstAll.CLS = CLS; 	config_allAgainstAll.isTo_use_continousSTripsOf_memory = isTo_use_continousSTripsOf_memory; config_allAgainstAll.typeOf_optimization = e_typeOf_optimization_distance_asFastAsPossible;
	const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;
	ktCorr_compute_allAgainstAll_distanceMetric_kendall(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix, mask, mask, NULL, result, transpose, metric_id, &config_allAgainstAll);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/nrows, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Optimal all-against-all Kendall", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);
      }
      { //! Include the 'transpostion': 
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/nrows, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	compute_allAgainstAll_distanceMetric_slow(/*dist=*/'k', /*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix,  NULL, result, mask, mask, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, isTo_use_continousSTripsOf_memory,
						  UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, NULL, e_cmp_masksAre_used_undef);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements("Optimal all-against-all Kendall", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);      
      }
    }
  }
}


static void  __compare_distance_naiveVSimproved_correlation() {
  printf("\n-------------------------------------------------\n### Start new test-evaluation, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    float **distmatrix = NULL; char **mask = NULL;
    for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
      printf("---------- \t \n");
      //! Note: ... 
      // FIXME[article]: update our artilce wrt. [ªbove] observation.
      assert(factor_rows*2 < size_of_array); //! ie, to avoid 'size so f 1'
      const uint factor_cols = factor_rows;
      const uint nrows = size_of_array/factor_rows; const uint ncols = size_of_array * factor_cols;      
      float relative_tickTime = 0;      
      //if(false) // FIXME: remove



      { //! use the naive appraoch (whcih we comapre with):
	//! Note: cpu=1777.8400s for list-size=1000,
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = false; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	//! Compute:


	struct s_allAgainstAll_config config_allAgainstAll; setTo_empty__s_allAgainstAll_config(&config_allAgainstAll); config_allAgainstAll.CLS = CLS; 	config_allAgainstAll.isTo_use_continousSTripsOf_memory = isTo_use_continousSTripsOf_memory; config_allAgainstAll.typeOf_optimization = e_typeOf_optimization_distance_none;
	ktCorr_compute_allAgainstAll_distanceMetric_correlation(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix, mask, mask, NULL, result, e_typeOf_metric_correlation_pearson_basic, transpose, &config_allAgainstAll);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	relative_tickTime = __assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Naive all-against-all Correlation", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS);
      }
      { //! Copare with the optimal appraoch, though without a 'first round of transpostion':
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 0; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	struct s_allAgainstAll_config config_allAgainstAll; setTo_empty__s_allAgainstAll_config(&config_allAgainstAll); config_allAgainstAll.CLS = CLS; 	config_allAgainstAll.isTo_use_continousSTripsOf_memory = isTo_use_continousSTripsOf_memory; config_allAgainstAll.typeOf_optimization = e_typeOf_optimization_distance_asFastAsPossible;
	ktCorr_compute_allAgainstAll_distanceMetric_correlation(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix, mask, mask, NULL, result, e_typeOf_metric_correlation_pearson_basic, transpose, &config_allAgainstAll); // CLS, isTo_use_continousSTripsOf_memory);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/nrows, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Optimal all-against-all Correlation", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);
      }
      { //! Include the 'transpostion': 
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	compute_allAgainstAll_distanceMetric_slow(/*dist=*/'c', /*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix,  NULL, result, mask, mask, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, isTo_use_continousSTripsOf_memory, 
						  UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, NULL, e_cmp_masksAre_used_undef);
	//UINT_MAX, NULL, e_allagainstall_simd_inlinepostprocess_undef, NULL, e_cmp_masksAre_used_undef);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Optimal all-against-all Correlation", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);      
      }
    }
  }
}


static void  __compare_distance_naiveVSimproved_euclid(const uint replicate_kmeans_clustering_pattern = true) {
  printf("\n-------------------------------------------------\n### Start new test-evaluation, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

  const uint arrOf_chunkSizes_size = 5; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {50*kilo,
											      100*kilo, 200*kilo, 
											      400*kilo, 800*kilo, 
											      //  5*kilo*kilo, 10*kilo*kilo, 40*kilo*kilo
  };

  // FIXME[aritcle]: udpate wrt. the observation .... than for size_of_array=800,000 perofrmance-increase is 1026.0000x for a signualr thread.

  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    float **distmatrix = NULL; char **mask = NULL;
        
    for(uint factor_rows_tmp = 80; factor_rows_tmp < 87; factor_rows_tmp++) {
      const uint factor_rows =  factor_rows_tmp * 10; //! ie, to inverse the data-sets.
      printf("---------- \t \n");
      //printf("---------- \t %u\t \n", factor_rows);
      //! Note: ... 
      // FIXME[article]: update our artilce wrt. [ªbove] observation.
      assert(factor_rows*2 < size_of_array); //! ie, to avoid 'size so f 1'
      const uint factor_cols = factor_rows;
      const uint nrows = size_of_array/factor_rows;
      const uint ncols = (replicate_kmeans_clustering_pattern) ? factor_rows : size_of_array * factor_cols;      
      float relative_tickTime = 0;      
      
      // FIXME: update [”elow] with an option of using a 'mask' property/input.
      // FIXME: update [”elow] with [”elowt] 'atlerantive' of using two matrices.
      // char **mask_data_2 = NULL;
      // if(isTo_sumDifferentArrays) {
      // 	maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &mul2, &mask_data_2, isTo_use_continousSTripsOf_memory);	  
      // }


      //if(false) // FIXME: remove
      { //! use the naive appraoch (whcih we comapre with):
	//! Note: cpu=1777.8400s for list-size=1000,
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = false; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	//! Compute:
	compute_allAgainstAll_distanceMetric_euclid_or_cityBlock(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix, mask, mask, NULL, result, /*isTo_use_cityBlock=*/false, transpose, e_typeOf_optimization_distance_none, CLS, isTo_use_continousSTripsOf_memory, UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, NULL, e_cmp_masksAre_used_undef);
      
	// assert(false);

	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	relative_tickTime = __assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols,"Naive all-against-all Euclid", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS);
      }
      if(false) // ie, where we de-activie in order to reduce the memory-consumpiotn of the result matrix
      { //! Copare with the optimal appraoch, though without a 'first round of transpostion':
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 0; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	compute_allAgainstAll_distanceMetric_euclid_or_cityBlock(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix, mask, mask, NULL, result,  /*isTo_use_cityBlock=*/false, transpose, e_typeOf_optimization_distance_asFastAsPossible, CLS, isTo_use_continousSTripsOf_memory, UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, NULL, e_cmp_masksAre_used_undef);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/nrows, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols,"Optimal all-against-all Euclid", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);
      }
      { //! Include the 'transpostion': 
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	compute_allAgainstAll_distanceMetric_slow(/*dist=*/'e', /*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix,  NULL, result, mask, mask, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, isTo_use_continousSTripsOf_memory, 
						  UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, NULL, e_cmp_masksAre_used_undef);
						  //UINT_MAX, NULL, e_allagainstall_simd_inlinepostprocess_undef, NULL, e_cmp_masksAre_used_undef);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Optimal all-against-all Euclid", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);      
      }
      { //! Investigate the effects of transposing a matrix in a spserte step, ie, as an laternative to use irregular memory-access-patterns when accessing arrays:	
	//! Note(1): examplifies the case where a 'transpostion' is used in sepeate distance-computations, ie, where 'we are not interested in comptuatin an all-against-all matrix'.
	//! Note(2): expected optimilaization of 'first inverting the matrix': (42.7s / 4.9s) = 8.7x
	// FIXME: update [aobv€] commetns/ntoes based onre sutls of [below]
	
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	for(uint index1 = 0; index1 < ncols; index1++) {
	  for(uint index2 = 0; index2 < ncols; index2++) {
	    // FIXME: include and update [below]:
	    //! Invert the matrix:
	    float **distmatrix_inv = NULL; char **mask_inv = NULL;
	    maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/nrows, &distmatrix_inv, &mask_inv, isTo_use_continousSTripsOf_memory);	  
	    for(uint i = 0; i < size_of_array; i++) {
	      for(uint k = 0; k < size_of_array; k++) {
		distmatrix_inv[i][k] = distmatrix[k][i];
	      }
	    }
	    //! Start the copmtuation using a 'naive' appraoch:
	    euclid(ncols, distmatrix_inv, distmatrix_inv, NULL, NULL, NULL, index1, index2, /*transpose=*/0);     
	    //! De-allocate:
	    maskAllocate__freedatamask(/*nelements=*/ncols, distmatrix_inv, mask_inv, isTo_use_continousSTripsOf_memory);    
	  }
	}
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Optimal all-against-all Euclid", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);      
      }
    }
  }
  // assert(false); // FIXME: remove
}


static void evaluate_memoryAccessOrder_2d_traversal_simple() {
  // FIXME: mmake use of this funciton.
  const uint kilo = 1000; 
  const uint arrOf_matrixSizes_size = 1;   const uint arrOf_matrixSizes[arrOf_matrixSizes_size] = {
    10000,
  };
  { //! An intial set of comparisons: goal is to identify the 'space' between ideal/optimal and 'non-ideal' memory-access-patterns:
    const uint size_of_array = 1000; 

    for(uint i = 0; i < arrOf_matrixSizes_size; i++) {
      const uint size_of_array = arrOf_matrixSizes[i];
      printf("# Array-size: %u\n", size_of_array);
      const uint numberOfTimes_dataIs_accessed = size_of_array;
      //! Evalaute cases both wrt. 'ideal' memory-allcoation and 'non-ideal' memory-allocation:
      for(uint isTo_use_continousSTripsOf_memory = 0; isTo_use_continousSTripsOf_memory < 2; isTo_use_continousSTripsOf_memory++) {
	//! Allocate memory:
	float **distmatrix = NULL; char **mask = NULL;
	//! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	  
	//! Fill the matrix with random numbers, and 'make every mask' of itnerest:
	for(uint y = 0; y < size_of_array; y++) {
	  assert(distmatrix);
	  for(uint x = 0; x < size_of_array; x++) {
	    // FIXME: update our code to use "bool" instead of "uint" for mask ... adn then compare/evaluate the time-difference ... ie, when we have dientifed other fucnitonw which 'benefit' from our approach.
	    //mask[y][x] = 1; 
	    if(false) {distmatrix[y][x] = (float)rand();}
	    else {distmatrix[y][x] = ((y+1)*size_of_array) + x + 1; } //! ie, give the latter a unique sorted number, ie, to avoid having to evlauate the imapct of our 'sort-funciton'.
	  }
	}
	{ 
	  //! Compare "graphAlgorithms_distance::find_closest_pair(..)":
	  //! Note: from this test/experiment we observe that the memory-allcoation-rotuines is not of importance
	  //! The operation:
	  uint i = UINT_MAX; uint j = UINT_MAX;
	  start_time_measurement();
	  //! Note: seems like [”elow] does not 'exbhitb' any large difference wrt. cache-miss-signficance, which could be explained by [below] being an 'iterative traversal', where the "size_of_array" possible cache-misses are isngifcnat compared to 'the other time-costs'.
	  find_closest_pair(size_of_array, distmatrix, &i, &j);	      
	  //! Then generate the results:
	  char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	  sprintf(string, "%s::memoryAccess-%s", macro_getVarName(find_closest_pair), (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
	  __assertClass_generateResultsOf_timeMeasurements(string, size_of_array);
	  delete [] string; string = NULL;
	}
	maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);
      }
    }
  }
}

static void evaluate_importanceOf_memoryAccessOrder_inMatrices() {
  // FIXME: mmake use of this funciton.
  const uint kilo = 1000; 
  const uint arrOf_matrixSizes_size = 1;   const uint arrOf_matrixSizes[arrOf_matrixSizes_size] = {
    10000,
  };
  // FIXME: remove [ªbove] and incldue [below].
  // const uint arrOf_matrixSizes_size = 7;   const uint arrOf_matrixSizes[arrOf_matrixSizes_size] = {
  //   50, 100, 500,
  //   1000, 5000, 10*kilo,
  //   15*kilo
  //   //, 50*kilo
  // };

  

  printf("......... at %s:%d\n", __FILE__, __LINE__);

  const bool measure_time = true;
  const bool isTo_testFixedOverhead = false; const bool isTo_test_randomAccess = false;  const bool isTo_compareWith_idea_accessPAttern = false;
  { //! An intial set of comparisons: goal is to identify the 'space' between ideal/optimal and 'non-ideal' memory-access-patterns:
    const uint size_of_array = 1000; 

    for(uint i = 0; i < arrOf_matrixSizes_size; i++) {
      const uint size_of_array = arrOf_matrixSizes[i];
      printf("# Array-size: %u\n", size_of_array);
      const uint numberOfTimes_dataIs_accessed = size_of_array;
      //! Note: in [below] we test/evaluate the overhead of 'fixed memory-bloccks', eg, wrt. "float " VS "double"
      {


	//! Evalaute cases both wrt. 'ideal' memory-allcoation and 'non-ideal' memory-allocation:
	for(uint isTo_use_continousSTripsOf_memory = 0; isTo_use_continousSTripsOf_memory < 2; isTo_use_continousSTripsOf_memory++) {
	  //! Allocate memory:
	  float **distmatrix = NULL; char **mask = NULL;
	  //! Note: in [below] call we allcoate memory diffntely depending upon the "isTo_use_continousSTripsOf_memory" value, ie, to evaluate/compare 'optimal' and 'non-optimal' memory-allocation to our "maskAllocate__makedatamask(..)"
	  maskAllocate__makedatamask(/*nrows=*/size_of_array, /*ncols=*/size_of_array, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	  
	  //! Fill the matrix with random numbers, and 'make every mask' of itnerest:
	  for(uint y = 0; y < size_of_array; y++) {
	    assert(distmatrix);
	    for(uint x = 0; x < size_of_array; x++) {
	      // FIXME: update our code to use "bool" instead of "uint" for mask ... adn then compare/evaluate the time-difference ... ie, when we have dientifed other fucnitonw which 'benefit' from our approach.
	      //mask[y][x] = 1; 
	      if(false) {distmatrix[y][x] = (float)rand();}
	      else {distmatrix[y][x] = ((y+1)*size_of_array) + x + 1; } //! ie, give the latter a unique sorted number, ie, to avoid having to evlauate the imapct of our 'sort-funciton'.
	    }
	  }


	  // if(true) 
	  {
	    //! Compare "graphAlgorithms_distance::spearman(..)" or "kendall(..)":
	    start_time_measurement();	    

	    //! The operation:
	    //const uint arrOf_indexCombinations_size = 1; const uint arrOf_indexCombinations[arrOf_indexCombinations_size] = {0}; 
	    // FIXME: remove [ªbove] and incldue [below].
	    // const uint arrOf_indexCombinations_size = 5; const uint arrOf_indexCombinations[arrOf_indexCombinations_size] = {0, size_of_array/3, size_of_array/2, size_of_array-1};
	    // for(uint index1_tmp = 0; index1_tmp < arrOf_indexCombinations_size; index1_tmp++) {
	    //   const uint index1 = arrOf_indexCombinations[index1_tmp]; 
	    float sum = 0;
	    //for(uint index1 = 0; index1 < 400; index1++) {
	    for(uint index1 = 0; index1 < 4; index1++) {
	    //	    for(uint index1 = 0; index1 < size_of_array; index1++) {
	      assert(index1 < size_of_array);
	      const uint index2 = (size_of_array - index1) - 1;
	      //{ const uint transpose = 1;
	      // FIXME: remove [ªbove] and incldue [below].
	      s_allAgainstAll_config_t config = get_init_struct_s_allAgainstAll_config(); //! ie, 'state' taht we are to ause a slow verison (oekseth, 06. sept. 2016).
	      for(uint transpose = 0; transpose < 2; transpose++) {
		//! Note: seems like [”elow] does .... 'exbhitb' large difference wrt. cache-miss-signficance, which could be explained by [below] being ...??...
		if(true) {
		  const e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine;
		  sum += kt_kendall(size_of_array, distmatrix, distmatrix, mask, mask, /*weight=*/NULL, index1, index2, transpose, metric_id, config);	      
		  //		  sum += graphAlgorithms_distance::euclid(size_of_array, distmatrix, distmatrix, mask, mask, /*weight=*/NULL, index1, index2, transpose);	      
		} else {
		  sum += kt_spearman(size_of_array, distmatrix, distmatrix, mask, mask, /*weight=*/NULL, index1, index2, transpose, config);	      
		}
	      }
	    }
	    //! Then generate the results:
	    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
	    //printf("sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
	    sprintf(string, "%s::memoryAccess-%s", macro_getVarName(spearman), (isTo_use_continousSTripsOf_memory) ? "ideal" : "old"); 
	    //	      sprintf(string, "%s::memoryAccess-%s::indexCase-%u::transporse-%s", macro_getVarName(spearman), (isTo_use_continousSTripsOf_memory) ? "ideal" : "old", index1_tmp, (transpose) ? "yes" : "no"); 
	    __assertClass_generateResultsOf_timeMeasurements(string, size_of_array);
	    delete [] string; string = NULL;
	  }

	  maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);
	}
      }
    }
  }
}

void graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary::main() {


  // FIXME: update our all-agaisnt-all-comptuations to only compute for "index2 <= index1" ... and thereafter add a post-update where we update for all "index2 > index2" ... ie, as we assume symmetry in the result ... with the implicaiton of another 2x performance-increase.

  // FIXME: from our [”elow] tests describe/assert if 'there is a point wrt. performance' in first transposing a matrix 'before clsuter-evaluation'.

  { //! Test the 'impact' of using Knights algortihm ("https://en.wikipedia.org/wiki/Kendall_rank_correlation_coefficient"):
    // FIXME: update our "kendall(..)" algorithm with "external_kendall_attachment.c" ... where the latter has a time-complexity of "n*log(n)" instead of "n*n".
    // --------------------------------------
    // (1.a) seperately sort "data1" and "data2": if the lists 'points to the same data-structure', then only sort for "data1": either performed in a seperate step (eg, if we compute all correlations in a 2d-matrix) or 'combined' (ie, if we are only interested in the correlations for one 'specific' dataset).
    // (1.b.alt1) if "data1" and "data2" poitns to different sets, then (i) merge the lists and (ii) conseuqtivly insert into a new list values assicated to "y";
    // (1.b.alt2) otherwise: insert into a new list values assicated to "y";
    // (2.a) to the new list apply a sort-algorithm where the number of 'the total number of swaps' in the merge-sort ("https://en.wikipedia.org/wiki/Kendall_rank_correlation_coefficient")
    // (2.b) first iterate through the list to seperately test the conidtions of '<', '>', '==', and '!=', and thereafter ifner the assicated count.      
    // --------------------------------------
    
    // 	Memory-cache-optimization: ....??... 
    // FIXME: add something
  }
  
  { //! Test the importanct/'impact' of memory-re-use.

    // FIXME: update our graphAlgorithms_distance::spearman(..)" and use the latter to test the difference.    
  }

  if(false)
  { //! Compare the different klcustering-algorithms
    __compare_cluster_naiveVSimproved_kmeans();
  }

  { //! Compare differences wrt. distance-matrix:
    { //! Compare 'naive' VS 'improved':
      // __compare_distance_naiveVSimproved_spearman();
      // FIXME: include [below]
      // 
      // 
      __compare_distance_naiveVSimproved_euclid();
      // __compare_distance_naiveVSimproved_correlation();
      // __compare_distance_naiveVSimproved_kendall();
    }
    { //! Evalaute the different optiosn/alternatives:
      // FIXME: add some others.
    }
  }





  // FIXME: add soemthing
}
