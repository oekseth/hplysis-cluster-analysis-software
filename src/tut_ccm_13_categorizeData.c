#include "hp_categorizeMatrices_syn.h" //! ie, the simplifed API for hpLysis

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include <time.h>    // time()

/**
   @brief examplfies how data-sets may be classified wrt. simliarty to differnt data-distributions, using diffneret CCMs to evlauate diffenret expalantion-factors for data-divergence
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).

**/
int main() 
#else
  void tut_ccm_13_categorizeData(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *stringOf_resultPath, const uint data_id_category)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
  const uint data_id_category = 0;
  //!
  //! Construct sample-data:
    //! 
    //! Local configurations:
#define __MiV__matrixDims 1 //! ie, to 'enalbe' lcoal speicficoant of [”elow] varialbes:
    const uint sizeOf__nrows = 10;   const uint sizeOf__ncols = 10;
    //  const uint sizeOf__nrows = 50;   const uint sizeOf__ncols = 50;
    const e_hp_clusterFileCollection_simMetricSet_t metric__beforeClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
    const e_hp_clusterFileCollection_simMetricSet_t metric__insideClust = e_hp_clusterFileCollection_simMetricSet_citBlock_pearsonSquared;
    const e_hp_clusterFileCollection__clusterAlg_t clusterAlg = e_hp_clusterFileCollection__clusterAlg_kMeans;      //! ------------------
    // const e_kt_correlationFunction_t metric__beforeClust = e_kt_correlationFunction_groupOf_minkowski_euclid;
    // const e_kt_correlationFunction_t metric__insideClust = e_kt_correlationFunction_groupOf_minkowski_euclid;
    // //const bool fileRead_config__transpose = false;
    // const e_hpLysis_clusterAlg_t clusterAlg = e_hpLysis_clusterAlg_kCluster__rank;
#include "tut__stub__config__inputData.c" //! which is used as 'cefualt cofnigruations' for [”elow]
    //! --------------------------------------------
    //!
    //! File-specific cofnigurations: 
    s_kt_matrix_fileReadTuning_t fileRead_config = initAndReturn__s_kt_matrix_fileReadTuning_t();
    //fileRead_config.isTo_transposeMatrix = true;
    fileRead_config.isTo_transposeMatrix = false;
    fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_rows = fractionOf_toAppendWith_sampleData_typeFor_rows;
    fileRead_config.fractionOf_toAppendWith_sampleData_typeFor_columns = fractionOf_toAppendWith_sampleData_typeFor_columns;  
    fileRead_config.isTo_exportInputFileAsIs__toFormat__js = "localData"; //! which is used to simplify web-based loading of the input-file.
    s_kt_matrix_fileReadTuning_t fileRead_config__transpose = fileRead_config;
    //fileRead_config.isTo_transposeMatrix = true;
    fileRead_config.isTo_transposeMatrix = true;
    //!
    //s_kt_matrix_fileReadTuning_t fileRead_config__syn = initAndReturn__s_kt_matrix_fileReadTuning_t();
#undef __MiV__matrixDims
    //! -----------------------------------------------------------------------------------------
    //!
    //!
#include "tut__aux__dataFiles__syn.c" //! ie, the file which hold the cofniguraitosn to be used.
    //! -----------------------------------------------------------------------------------------
    //!
    const char *stringOf_resultPath = "results/tut_ccm_13_categorizeData/";
    {
      allocOnStack__char__sprintf(2000, str_cmd, "mkdir -p %s", stringOf_resultPath); //! ie, creat eht direcotry (if directy does No0t relayd exists).
      MF__system(str_cmd);
    }    
#endif 
    s_hp_categorizeMatrices_syn__setOf_t obj_eval = init__s_hp_categorizeMatrices_syn__setOf(stringOf_resultPath);
    {//! Cosntruct a file holding the data-set which we evlauate/comapre each data-matrix to. 
      allocOnStack__char__sprintf(2000, fileName_toExportTo, "%s_functionScores.tsv", stringOf_resultPath); //! ie, creat eht direcotry (if directy does No0t relayd exists).
      const e_kt_categorizeMatrices_typeOfAdjust_synAndInput_t defaultAdjustment_ofSyn = e_kt_categorizeMatrices_typeOfAdjust_synAndInput_undef;
      static__exportSynteticDAtaMatrix(/*ncols=*/20, fileName_toExportTo, defaultAdjustment_ofSyn);
    }

    //! Configure clustierng: 
    //e_hpLysis_clusterAlg_t clustAlg = e_hpLysis_clusterAlg_HCA_single;
    //e_kt_matrix_cmpCluster_clusterDistance__cmpType_t config__ccmMatrixBased__toUse = e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
    for(uint sim_id = 0; sim_id < e_kt_correlationFunction_undef; sim_id++) {
      const e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)sim_id;
      if(describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id)) {continue;}
      printf("%s#\t (and %u data-sets)at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id), mapOf_realLife_size, __FILE__, __LINE__);
      //if( (metric_id != e_kt_correlationFunction_groupOf_minkowski_minkowski) && (metric_id != e_kt_correlationFunction_groupOf_minkowski_euclid)  && (metric_id != e_kt_correlationFunction_groupOf_minkowski_cityblock) ) {continue;} //! fixme:reomve this ... inclued to 'figure out why' a isnan || inf-case occurs (oesketh, 06. arp. 2017).
      if(isTo_use_directScore__e_kt_correlationFunction(metric_id)) {continue;}
      /* {//! Set string: */
      /* 	const char *str_local = get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id);  */
      /* 	/\* fprintf(stderr, "sim-metric=\"%s\", at %s:%d\n", str_local, __FILE__, __LINE__);  *\/ */
      /* 	/\* //allocOnStack__char__sprintf(2000, str_local, "row-hyptothesis_data2[%u]", data_id_out); //! ie, intate a new variable "str_local".  *\/ */
      /* 	/\* set_stringConst__s_kt_matrix(&mat_ncluster, /\\*index=*\\/sim_pos, str_local, /\\*addFor_column=*\\/false); *\/ */
      /* 	/\* set_stringConst__s_kt_matrix(&mat_ccm, /\\*index=*\\/sim_pos, str_local, /\\*addFor_column=*\\/false); *\/ */
      /* } */
      //! 
      //! Swet the data-strings:
      for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
	const s_hp_clusterFileCollection data_obj = mapOf_realLife[data_id];
	const char *stringOf_tagSample = mapOf_realLife[data_id].file_name;
	if(stringOf_tagSample == NULL) {stringOf_tagSample =  mapOf_realLife[data_id].tag;}
	//char str_local[3000]; sprintf(str_local, "%u_%s", data_id, stringOf_tagSample);
	//!
	//! Load data:
	s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
	s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
	if(data_obj.file_name != NULL) {		
	  obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
	  if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
	} else {
	  if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	    const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	    assert(is_ok);
	    if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
	  } else {
	    fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
	  }
	}
	//if( (stringOf_resultPath != NULL) && (mapOf_realLife[data_id].tag != NULL) ) 

	assert(obj_matrixInput.nrows > 0); //! ie, as we exepct data to have been loaded.
	//!
	//! Apply the logics:
	s_kt_matrix_t mat_result_simMetric = setToEmptyAndReturn__s_kt_matrix_t();
	{  //! Comptue the simlarity-metic.
	  s_kt_matrix_base_t mat_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&obj_matrixInput);
	  s_kt_matrix_base_t mat_result_simMetric_base = initAndReturn__empty__s_kt_matrix_base_t();
	  
	  const bool is_ok = apply__hp_distance(initAndReturn__s_kt_correlationMetric_t(metric_id, e_kt_categoryOf_correaltionPreStep_none), &mat_1, NULL, &mat_result_simMetric_base, init__s_hp_distance__config_t());
	  assert(is_ok);
	  mat_result_simMetric = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result_simMetric_base);
	}
	assert(mat_result_simMetric.nrows > 0);  assert(mat_result_simMetric.ncols > 0); 

	//!
	//! Costnruct the string to dineitfy the sim--data-entry
	const char *tag_base = mapOf_realLife[data_id].tag;
	const char *tag = strrchr(tag_base, '/');
	if(tag && strlen(tag)) {tag++;} //! ie, to icnrement past the "/"! symbol
	else {tag = tag_base;} //! ie, as we for the latter asusmes that no direcotyr-prefix is given.
	allocOnStack__char__sprintf(2000, stringOf_entry, "%u::%s %s", data_id, tag, get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id));  //! ie, allcoate the "str_filePath".
	//allocOnStack__char__sprintf(2000, stringOf_entry, "%s_%u_%s_%s.tsv", stringOf_resultPath, data_id, tag, get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(metric_id));  //! ie, allcoate the "str_filePath".
	
	//!
	//! Update our evlauation-data-structure:
	assert(obj_matrixInput.nrows > 0);  assert(obj_matrixInput.ncols > 0); 
	assert(mat_result_simMetric.nrows > 0);  assert(mat_result_simMetric.ncols > 0); 
	//! 
	// printf("post-evaluates, at %s:%d\n", __FILE__, __LINE__);
	add_dataSet__s_hp_categorizeMatrices_syn__setOf_t(&obj_eval, &obj_matrixInput, &mat_result_simMetric, stringOf_entry, init__s_hp_categorizeMatrices_syn_config_dataObj_t(/*metirc=*/initAndReturn__s_kt_correlationMetric_t(metric_id, e_kt_categoryOf_correaltionPreStep_none), data_id, /*data_id_category=*/data_id_category));
	// printf("ok:post-evaluates, at %s:%d\n", __FILE__, __LINE__);
	//!
	//! De-allocate:
	free__s_kt_matrix(&obj_matrixInput);
	free__s_kt_matrix(&mat_result_simMetric);
      }
    }
    //!
    //! De-allcoate and generate result-data-set:
    free__s_hp_categorizeMatrices_syn__setOf_t(&obj_eval);
    //!
    //! @return
#ifndef __M__calledInsideFunction
    //! *************************************************************************
    //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
    hpLysis__globalFree__kt_api();
    //! *************************************************************************  
    return true;
#endif
}
