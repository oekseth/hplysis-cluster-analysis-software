#include "measure_sort.h"
  /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"
#include "correlation_rank_rowPair.h"
/* #include "type_2d_float_nonCmp_uint.h" */
#include "configure_cCluster.h"
/* #include "list_uint.h" */
#include "def_intri.h"
#include "log_clusterC.h"
#include "mask_api.h"

#include "maskAllocate.h"
#include "math_generateDistribution.h"
#include "graphAlgorithms_aux.h"
//#include "graphAlgorithms_distance.h"
#include "kt_clusterAlg_fixed.h"


static t_float default_time_cmtValue = 0;
static const uint default_string_size = 1000; static const char default_value_char = '\0'; static const t_float default_value_float = 0;


//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements_factor(const uint factor_rows, const uint factor_cols, const char *stringOf_measureText, const uint size_of_array, const uint isTo_use_continousSTripsOf_memory = UINT_MAX, const uint isTo_use_transpose = UINT_MAX, const uint isTo_sumDifferentArrays = UINT_MAX, const uint CLS = UINT_MAX, const float naive_tickTime = FLT_MAX ) {
  assert(stringOf_measureText);

  const uint string_length = 100 + strlen(stringOf_measureText);
  char *string = allocate_1d_list_char(string_length, default_value_char);
  sprintf(string, "%s w/factor=[%u, %u] and transpose=%s and isTo_sumDifferentArrays=%s and CLS=%u", stringOf_measureText, factor_rows, factor_cols,
	  (isTo_use_transpose) ? "true" : "false",
	  (isTo_sumDifferentArrays) ? "true" : "false",
	  CLS);
  const float tick_time = __assertClass_generateResultsOf_timeMeasurements(string, size_of_array, naive_tickTime);
    //__assertClass_generateResultsOf_timeMeasurements(string, size_of_array, isTo_use_continousSTripsOf_memory, isTo_use_transpose, isTo_sumDifferentArrays, CLS, naive_tickTime);
  free_1d_list_char(&string);
  //! @return the tick-time.
  return tick_time;
}



//! Identify optmizal strategies for list-access, ie, compare different implementaiton-programming-complexities
//! Note: for 'simplicty in focus' we in this/belwo funciton only evlauate wrt. 1d-lists, ie, do not investgiate cases such as "tiling"
static void evaluate_artimetic_loops_1d() {
  // FIXME: for [”elow] update teh 'resutl-test' with 'what we observe/lkearn'.
#include "measure_sort__func__evaluate_artimetic_loops_1d.c"
}



/* //! Indetify the order of the old indices ifthe 'input' valeus/scroes are sorted. */
/* //! @remarks time-complexity estimated to eitehr constand or n*log(n), ie, depending on the list being sorted (or not). */
/* //! @remarks Memory-access-pattern of this funciton is:  (a) in merge-sorts "n*log(n)" procedure partial sequential, and (b) sequential (wrt. distance-updates). */
/* void sort_indexes(const list_generic<type_float> &arrOf_old, list_uint &arrOf_result_sortedIndex) { //int n, const float data[], int index[]) */
/*   assert(arrOf_old.get_current_pos()); */
/*   assert(arrOf_old.is_empty() == false); */


/*   // FIXME: udpate [”elow] .... (a) use parallisaiton and (b) try to use the 'default implemented appraoch' */
  
/*   list_generic<type_2d_float_nonCmp_uint> arrOf_new; */
/*   //  list_generic<type_uint_n<2> > tmp;  */
/*   //list_generic<type_uint2x > tmp; // FIXME: remove. */
/*   for(uint index = 0; index < arrOf_old.get_current_pos(); index++) { */
/*     const float score = arrOf_old[index].get_key(); */
/*     assert(score != FLT_MAX); */
/*     const uint insertedAt_index = arrOf_new.push(type_2d_float_nonCmp_uint(score, index)); */
/*     //tmp.push(type_uint2x(score, index)); */
/*     assert(arrOf_new[insertedAt_index].get_score() == score);     */
/*   } */
/*   assert(arrOf_new.get_current_pos() == arrOf_old.get_current_pos()); */


/*   // tmp.apply_merge_sort(); tmp.free_memory(); */

/*   //! Apply the merge-sort: */
/*   //printf("FIXME: include [”elow], at %s:%d\n", __FILE__, __LINE__); */

/*   // FIXME: compare [below] to "qsort(..)" (used in "cluster.c") and "quicksort(..)" (used in "mine.c") */
/*   sort_merge_sort<type_2d_float_nonCmp_uint>::sort_array(arrOf_new.get_unsafe_list(), arrOf_new.get_current_pos()); */
/*   //arrOf_new.apply_merge_sort(/\*isTo_remove_overlap=*\/false); */
/*   assert(arrOf_new.get_current_pos() == arrOf_old.get_current_pos()); */
  
/*   //printf(".......... at %s:%d\n", __FILE__, __LINE__); */

/*   //! Copy the set of indexes, ie, iterate through the sorted list: */
/*   for(uint index = 0; index < arrOf_new.get_current_pos(); index++) { */
/*     const uint new_index = arrOf_new[index].get_index(); */
/*     if( (new_index  != UINT_MAX) && (new_index < arrOf_old.get_current_pos()) ) { */
/*       assert(new_index != UINT_MAX); */
/*       assert(new_index < arrOf_old.get_current_pos()); */
/*       arrOf_result_sortedIndex.push(new_index); //! ie, insert the order of the new sorted indices assicated to the sorted set of "scores" */
/*     } else { */
/*       // FIXME: include [beloow] */
/*       // fprintf(stderr, "!!\t investigate this case, where [%u].index = %u is not set, given cnt-elements=%u,  at [%s]:%s:%d\n", index, new_index, arrOf_new.get_current_pos(), __FUNCTION__, __FILE__, __LINE__); */
/*     } */
/*   } */

/*   //! De-allocate the locally reserved memoory: */
/*   arrOf_new.free_memory(); */
/* } */


/* //! Infer the rank of elements in a non-sorted list of values, eg, a column in a matrix. */
/* //! @remarks time-complexity estimated to "n + n*log(n)", where the latter complexity is due to the sorting */
/* //! @remarks Memory-access-pattern of this funciton is:  (1) sequential ((a) in merge-sorts "n*log(n)" procedure partial sequential, and (b) sequential (wrt. distance-updates)); (2) a quential search without any (noteworthy) cache-misses. */
/* static void __internal__get_rank_wCppOverhead(const list_generic<type_float> &arrOf_input, list_uint &arrOf_result, const bool isTo_use_continousSTripsOf_memory = true, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse = e_distance_rank_typeOf_computation_ideal) { */
/* //void get_rank_wCppOverhead(const list_generic<type_float> &arrOf_input, list_uint &arrOf_result, const bool isTo_use_continousSTripsOf_memory, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse) {//int n, t_float data[]) */
/*   // FIXME: write an assert-class text-fucntion for this funciton. */
/*   // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.  ... testing the different combinations of the typeOf_performance_sortAppraoch_toUse parameter. */

/*   //! What we expect: */
/*   assert(arrOf_input.is_empty() == false); */
/*   assert(arrOf_result.is_empty()); */
/*   //! Safeguard by first de-allocating reserved memory (ie, ifany). */
/*   arrOf_result.free_memory(); */
/*   //! Note: for simplicty we asusme the list is sorted: */
/*   list_uint arrOf_result_sortedIndex; */
/*   sort_indexes(arrOf_input, arrOf_result_sortedIndex); */
/*   assert(arrOf_result_sortedIndex.get_current_pos()); */

/*   //! Iterate through the list, updating the index ifthe values differ: */
/*   type_float prev_object; assert(prev_object.is_empty()); */
/*   uint current_index = 0;  */
/*   uint first_index_same_value = UINT_MAX;  */
/*   // FIXME: compare [”elow] approach of [below] optimization-strategy, ie, to first udpate the 'global container' when all values are known: used to improve the memory-cache-locality. */
/*   list_uint arrOf_tmp_realWorld_index; //! a list which is used for vertices with the same score: expects an insigicant time-increase through this operation   */
/*   if(isTo_use_continousSTripsOf_memory == false) { */
/*     assert(false); // FIXME: 'rset' our appraoch usign the code int eh 'orignal' "cluster.c" file. */
/*   } */
/*   for(uint i = 0; i < arrOf_result_sortedIndex.get_current_pos(); i++) { */
/*     assert(arrOf_result_sortedIndex[i].is_empty() == false); */
/*     const uint realWorld_index = arrOf_result_sortedIndex[i].get_key(); */
/*     assert(realWorld_index != UINT_MAX); */
/*     assert(arrOf_input[realWorld_index].get_key() != UINT_MAX); */
/*     if(arrOf_input[realWorld_index].is_equal(prev_object) == false) { */
/*       //! Then we increment the count, and update the value: */
/*       if(arrOf_tmp_realWorld_index.get_current_pos()) { //! Then weupdate the rank-set: */
/* 	assert(first_index_same_value != UINT_MAX); */
/* 	//assert(current_index >= first_index_same_value); */
/* 	//! TODO: in [”elow] consider instead to update the index witht he same value with the 'min' rank, ie, instead of the 'min-rank'. */
/* 	const uint new_rank = 0.5*(current_index - first_index_same_value); */
/* 	for(uint k = 0; k < arrOf_tmp_realWorld_index.get_current_pos(); k++) { */
/* 	  const uint prev_realWorld_index = arrOf_tmp_realWorld_index[k].get_key(); */
/* 	  //printf("prev_realWorld_index=%u, at %s:%d\n", prev_realWorld_index, __FILE__, __LINE__); */
/* 	  assert(prev_realWorld_index != UINT_MAX); */
/* 	  assert(arrOf_result[prev_realWorld_index].get_key() == UINT_MAX); //! ie, as otherwise might result in overwriting of alrady existing values. */
/* 	  arrOf_result.insert(prev_realWorld_index, current_index);   */
/* 	} */
/*       } */


/*       //! Update the 'comparisn object': */
/*       prev_object = arrOf_input[realWorld_index]; */
/*       arrOf_tmp_realWorld_index.free_memory(); */
/*       arrOf_tmp_realWorld_index.push(realWorld_index); */
/*       //! Update the 'glboal position-variables': */
/*       first_index_same_value = i; */
/*       current_index++;       */
/*     } else { //! then we asusem the 'rank' is the same as the previous value. */
/*       assert(current_index > 0); */
/*       arrOf_tmp_realWorld_index.push(realWorld_index); */
/*     } */
/*   } //! and at this exueciton-poitn we expect each index to have been visited */
/*   { //! Finalize: */
/*     if(arrOf_tmp_realWorld_index.get_current_pos()) { //! Then weupdate the rank-set: */
/*       assert(first_index_same_value != UINT_MAX); */
/*       //assert(current_index > first_index_same_value); */
/*       //! TODO: in [”elow] consider instead to update the index witht he same value with the 'min' rank, ie, instead of the 'min-rank'. */
/*       const uint new_rank = 0.5*(current_index - first_index_same_value); */
/*       for(uint k = 0; k < arrOf_tmp_realWorld_index.get_current_pos(); k++) { */
/* 	const uint prev_realWorld_index = arrOf_tmp_realWorld_index[k].get_key(); */
/* 	assert(prev_realWorld_index != UINT_MAX); */
/* 	arrOf_result.insert(prev_realWorld_index, current_index);   */
/*       } */
/*     } */
/*   } */
/*   arrOf_result_sortedIndex.free_memory(); */
/*   arrOf_tmp_realWorld_index.free_memory(); */
/*   assert(arrOf_result.get_current_pos()); */
/* } */


static const uint arrOf_chunkSizes_size = 12; static const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {250, 500, 750, kilo, 2*kilo, 3*kilo,
											     10*kilo, 100*kilo, kilo*kilo, 
											     5*kilo*kilo, 10*kilo*kilo, 40*kilo*kilo };

static void  __compare_distance_naiveVSimproved_spearman() {
  printf("\n-------------------------------------------------\n### Start new test-evaluation, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    float **distmatrix = NULL; char **mask = NULL;
      
    
    //    for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
    for(uint factor_rows = 1; factor_rows < 10; factor_rows++) {
      //! Note: the biggest optimization is seen for factor_rows=[1, 2, 3] w/approximated optimization=2000x .... for [”elow] 'transposed' the latter may be exaplined by the observation that there are relative: (a) fwer 'sperate chunks' to sort; (b) fwer cache-misses assicated to look-ups (when compared to the 'naive' implementaiton); (c) .... 
      // FIXME[article]: update our artilce wrt. [ªbove] observation.
      assert(factor_rows*2 < size_of_array); //! ie, to avoid 'size so f 1'
      const uint factor_cols = factor_rows;
      const uint nrows = size_of_array/factor_rows; const uint ncols = size_of_array * factor_cols;
      printf("---------- \t \n");
      //if(false) // FIXME: remove
      float relative_tickTime = 0;
      { //! use the naive appraoch (whcih we comapre with):
	for(uint transpose = 0; transpose < 2; transpose++) {
	  const bool isTo_use_continousSTripsOf_memory = false; const uint CLS = 64; 
	  {
	    //! Note: cpu=1777.8400s for list-size=1000,
	    start_time_measurement();
	    maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	    assert(mask != NULL); 
	    //mask = allocate_2d_list_char(nrows, ncols, /*default-value=*/1); //! ie, ot test the cost of using the masks.
	    float **result = NULL; char **mask_result = NULL;
	    maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	    //! Compute:	
	    struct s_allAgainstAll_config config_allAgainstAll; setTo_empty__s_allAgainstAll_config(&config_allAgainstAll); 
	    config_allAgainstAll.CLS = CLS; 	config_allAgainstAll.isTo_use_continousSTripsOf_memory = isTo_use_continousSTripsOf_memory;
	    config_allAgainstAll.typeOf_optimization = e_typeOf_optimization_distance_none;
	    ktCorr_compute_allAgainstAll_distanceMetric_spearman(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix, mask, mask, NULL, result, /*masksAre_aligned=*/e_typeOf_measure_spearman_masksAreAligned_always, transpose, &config_allAgainstAll); 
	    
	    //! De-allocate:
	    maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	    maskAllocate__freedatamask(/*nelements=*/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);    
	    
	    //! Write out the result:
	    relative_tickTime = __assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Naive all-against-all Spearman", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS);
	  }
	  /* { //! Idnetify teh tiem assicated to the sorting. */
	  /*   start_time_measurement(); */
	  /*   maskAllocate__makedatamask(/\*nrows=*\/nrows, /\*ncols=*\/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	   */
	  /*   float **result = NULL; char **mask_result = NULL; */
	  /*   maskAllocate__makedatamask(/\*nrows=*\/ncols, /\*ncols=*\/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	   */
	  /*   //! Compute:	 */
	  /*   //! Note: to 'evlauate' the time assicated to the matrix-operaiton, please refer/see the computational time for comptuation of "euclid" and "correlation". */
	  /*   for(uint index1 = 0; index1 < ncols; index1++) { */
	  /*     for(uint index2 = 0; index2 < ncols; index2++) { */
	  /* 	uint m = 0; */
	  /* 	list_generic<type_float> arrOf_scores_1; list_generic<type_float> arrOf_scores_2; */
	      
	  /* 	for(uint i = 0; i < nrows; i++) { */
	  /* 	  arrOf_scores_1.insert(m, distmatrix[i][index1]); */
	  /* 	  arrOf_scores_2.insert(m, distmatrix[i][index2]); */
	  /* 	  m++; */
	  /* 	} */
	  /* 	if(m==0) { */
	  /* 	  arrOf_scores_1.free_memory(); arrOf_scores_2.free_memory(); */
	  /* 	} */
	  /* 	assert(arrOf_scores_1.get_current_pos()); */
	  /* 	assert(arrOf_scores_2.get_current_pos()); */
	  /* 	list_uint arrOf_ranks_1; __internal__get_rank_wCppOverhead(arrOf_scores_1, arrOf_ranks_1); */
	  /* 	arrOf_scores_1.free_memory(); */
	  /* 	list_uint arrOf_ranks_2; __internal__get_rank_wCppOverhead(arrOf_scores_2, arrOf_ranks_2); */
	  /* 	arrOf_scores_2.free_memory(); */
	  /*     } */
	  /*   } */
      
	  /*   //! De-allocate: */
	  /*   maskAllocate__freedatamask(/\*nelements=*\/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);     */
	  /*   maskAllocate__freedatamask(/\*nelements=*\/ncols, result, mask_result, isTo_use_continousSTripsOf_memory);     */

	  /*   //! Write out the result: */
	  /*   __assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Naive Spearman-only-sort", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /\*isTo_sumDifferentArrays=*\/true, CLS, relative_tickTime); */
	  /* } */
	}
      }
      //! ------------------------------------------------------------------------------
      
#ifndef NDEBUG
// #warning "TODO: include a permtuation of [”elow] measurement-call when our all-agianst-all-correlation-emtric works at wrt spearman, at [%s]:%s:%d\n"
#endif
      if(false) { //! Copare with the optimal appraoch, though without a 'first round of transpostion':
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 0; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	struct s_allAgainstAll_config config_allAgainstAll; setTo_empty__s_allAgainstAll_config(&config_allAgainstAll); 
	config_allAgainstAll.CLS = CLS; 	config_allAgainstAll.isTo_use_continousSTripsOf_memory = isTo_use_continousSTripsOf_memory;
	config_allAgainstAll.typeOf_optimization = e_typeOf_optimization_distance_asFastAsPossible;
	ktCorr_compute_allAgainstAll_distanceMetric_spearman(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix, mask, mask, NULL, result, /*masksAre_aligned=*/e_typeOf_measure_spearman_masksAreAligned_always, transpose, &config_allAgainstAll);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/nrows, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Optimal all-against-all Spearman non-transposed (direct)", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);
      }
#ifndef NDEBUG
// #warning "TODO: include a permtuation of [”elow] measurement-call when our all-agianst-all-correlation-emtric works at wrt spearman, at [%s]:%s:%d\n"
#endif
      if(false)
      { //! Copare with the optimal appraoch, though without a 'first round of transpostion':
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 0; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/nrows, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  

	printf("at %s:%d\n", __FILE__, __LINE__);
	//! Compute:
	//assert(false); // FIXME: udpate [”elwo] call using our new-added "kt_distance" fucntioanitliy. (oekseth, 06. sept. 2016).
	compute_allAgainstAll_distanceMetric_slow(/*dist=*/'s', /*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix,  NULL, result, mask, mask, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, isTo_use_continousSTripsOf_memory, 
						  UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, NULL, e_cmp_masksAre_used_undef);
						  //UINT_MAX, NULL, e_allagainstall_simd_inlinepostprocess_undef, NULL, e_cmp_masksAre_used_undef);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/nrows, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/nrows, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Semi-Optimal all-against-all Spearman non-transposed (implicit)", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);
      }
#ifndef NDEBUG
// #warning "TODO: include a permtuation of [”elow] measurement-call when our all-agianst-all-correlation-emtric works at wrt spearman, at [%s]:%s:%d\n"
#endif
      if(false)
      { //! Include the 'transpostion': 
	//! for x=y=|1000]|: tick=8.3500s
	start_time_measurement();
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 1; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/nrows, /*ncols=*/ncols, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
      
	//! Compute:
	compute_allAgainstAll_distanceMetric_slow(/*dist=*/'s', /*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix,  NULL, result, mask, mask, transpose, /*isTo_invertMatrix_transposed=*/true, CLS, isTo_use_continousSTripsOf_memory, 
						  UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, NULL, e_cmp_masksAre_used_undef);
						  //UINT_MAX, NULL, e_allagainstall_simd_inlinepostprocess_undef, NULL, e_cmp_masksAre_used_undef);
      
	printf("at %s:%d\n", __FILE__, __LINE__);

	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/size_of_array, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Semi-Optimal all-against-all Spearman", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);      
      }
      
#ifndef NDEBUG
// #warning "TODO: include a permtuation of [”elow] measurement-call when our all-agianst-all-correlation-emtric works at wrt spearman, at [%s]:%s:%d\n"
#endif
      if(false)
      { //! Idnetify teh tiem assicated to transpostion and the sorting.
	//! Note: to 'evlauate' the time assicated to the matrix-operaiton, please refer/see the computational time for comptuation of "euclid" and "correlation".
	start_time_measurement();
	//! Note: if [below] we set "transpose==0" as we assume that teh transposign-operation has an insigifnct tiem-csot, ie, to reduce the clustter in the output (Wrt. number of measurement-combiantoins).
	const bool isTo_use_continousSTripsOf_memory = true; const bool transpose = 0; const uint CLS = 64; 
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/nrows, &distmatrix, &mask, isTo_use_continousSTripsOf_memory);	  
	float **result = NULL; char **mask_result = NULL;
	maskAllocate__makedatamask(/*nrows=*/ncols, /*ncols=*/ncols, &result, &mask_result, isTo_use_continousSTripsOf_memory);	  
	float **matrix_tmp_1 = NULL; float **matrix_tmp_2 = NULL;
	const bool needTo_useMask_evaluation = true; // <-- FIXME: try diffenre  permtuations of this, ie, udapte our perormance-tests
	ktCorrelation_compute_rankFor_matrices(/*nrows=*/nrows, /*ncols=*/ncols, distmatrix, distmatrix,  mask, mask, &matrix_tmp_1, &matrix_tmp_2, transpose, needTo_useMask_evaluation, UINT_MAX);
      
	//! De-allocate:
	maskAllocate__freedatamask(/*nelements=*/size_of_array, distmatrix, mask, isTo_use_continousSTripsOf_memory);    
	maskAllocate__freedatamask(/*nelements=*/size_of_array, result, mask_result, isTo_use_continousSTripsOf_memory);    

	//! Write out the result:
	__assertClass_generateResultsOf_timeMeasurements_factor(factor_rows, factor_cols, "Sort lists for transposed dataset, XMT the 'transposing' operation", size_of_array, isTo_use_continousSTripsOf_memory, transpose, /*isTo_sumDifferentArrays=*/true, CLS, relative_tickTime);      
	delete [] matrix_tmp_1[0]; delete [] matrix_tmp_1;
      }
    }
  }
}



//! Compare the time assicated to different sort-functions
//! @remarks from the result it seems like the different implementaitons of "quicksort(..)" and "mergeSort(..)" for randomized inptu-datasets has approximate the same time-compelxity: the difference which eixsts seems to be due to internal noise-ovheread in the computer: though the 'merge-sort' has a slwodown of approx. 1/30 when compared to the quicsort(..) implementaiotn, the merge-sort perofrman best if called first (ie, as exmplified wrt. the use of 'merge-sort' as teh 'idea' function: when merge-sort is called 'both first and in the middle of the iteraiotn' merge-sort 'goes from being the best othaving a dealy of 1/30 when compared to quicksort(..)'). 
static void compare_sorting() {
  printf("\n-------------------------------------------------\n### Start new test-evaluation, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  // FIXME: consider to extend [”elow] with tests wrt. sort-funcitons of "backup_bst.h  bst.h  bubble_sort.h  bucket_sort.h  counting_sort.h  heap_sort.h  insertion_sort.h  merge_sort.h  quick_sort.h  radix_sort.h  red_black_tree.h" (where the altter is implemented/found in our "/home/klatremus/Dokumenter/algkons/sort/"). <-- discuss the need/feasiblity of 'such' with jan-crhistan ... ie, if theese/such results may be used 'to something useful'.
  const uint arrOf_chunkSizes_size = 7; const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {kilo, 10*kilo, 100*kilo, kilo*kilo, 5*kilo*kilo, 10*kilo*kilo, 40*kilo*kilo};
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint list_size = arrOf_chunkSizes[chunk_index];
    printf("----------\n# \t list-size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
    float *arr_in = new float[list_size]; 
    float *arr_result = new float[list_size];

    assert(e_typeOf_sortFunction_undef == 10); //! ie, otheriwse update [below].
    const char *stringOf_descriptions[e_typeOf_sortFunction_undef] = {
      "ideal-function", "quickSort_POSIX", 
      "quickSort_localImplementaiton_indexOverhead",

      "quickSort_templateImplementation_default",
      "quickSort_templateImplementation_default_memoryAcceesOptimized",
      "quickSort_templateImplementation_randomized",
      "mergeSort_internal",
      "mergeSort_list_generic", //! ie, teh merge-osrt routine used in our lsit-gneeric fucniton.
      "insertionSort_default",
      "insertionSort_bubbleSort",
    };

    //! Investigate/compare the different srot-approaches:
    for(uint enum_id = 0; enum_id < e_typeOf_sortFunction_undef; enum_id++) {
      bool isOf_interest = true;
      if( (list_size >= (kilo * kilo)) &&  ( (enum_id == e_typeOf_sortFunction_insertionSort_default) || (enum_id == e_typeOf_sortFunction_insertionSort_bubbleSort) ) ) {
	isOf_interest = false; //! ie, given the long/high weating-time wr.t the "n*n" time-compelxtiy.
      }
      for(uint i = 0; i < list_size; i++) {arr_in[i] = rand();} //! ie, intialise.      
      start_time_measurement();
      sort_array(arr_in, arr_result, list_size, (e_typeOf_sortFunction)enum_id);
      //! Then generate the results:
      char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
      sprintf(string, "sort-call w/list-size=%u and type=%s and where data-distribution=rand()", list_size, stringOf_descriptions[enum_id]); 

      __assertClass_generateResultsOf_timeMeasurements(string, list_size, default_time_cmtValue);
      delete [] string; string = NULL;	  
    }

    //! De-allcoate the lcoally reserved memory.
    delete [] arr_in; delete [] arr_result;
  }
}




//! The main assert function.
void measure_sort__main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r


  if(isTo_processAll || (0 == strncmp("SSE_memset", array[2], strlen("SSE_memset"))) ) {
    //! Identify optmizal strategies for list-access, ie, compare different implementaiton-programming-complexities
    //! Note: for 'simplicty in focus' we in this/belwo funciton only evlauate wrt. 1d-lists, ie, do not investgiate cases such as "tiling"
    evaluate_artimetic_loops_1d();
    cnt_Tests_evalauted++;
  } 
  if(isTo_processAll || (0 == strncmp("algorithms_sort", array[2], strlen("algorithms_sort"))) ) {
    //! Compare ovheread wrt. sorting:
    // FIXME: from the results update our "maskAllocate__sort_indexes(..)"    
    compare_sorting();
  } 
  if(isTo_processAll || (0 == strncmp("ranks-and-SpearmanSimple", array[2], strlen("ranks-and-SpearmanSimple"))) ) {
    __compare_distance_naiveVSimproved_spearman();
  } else if(isTo_processAll || (0 == strncmp("ranks", array[2], strlen("ranks"))) ) {

    assert(false); // FIXME: add something

    cnt_Tests_evalauted++;
  }
  
  
  
  assert(false); // FIXME: .... wrt. measuremetns ...  "correlation_rank" ... comapre wrt. diffferent mask-attributes .... eg, wrt. relative ovrhead ... and include calls to "correlation_rank_rowPair.c"


    assert(false); // FIXME: write a enw "measure_sort.cxx" for "correlation_sort.cxx" "correlation_rank.cxx" "correlation_rank_rowPair.cxx" ... where we use code-tests/tempaltes from our "graphAlgorithms_timeMeasurements_syntetic.cxx" and our "graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary.cxx"
    assert(false); // FIXME: move the distance-perfm-functions from our "graphAlgorithms_timeMeasurementsExternalTools_cClusterLibrary.cxx" to ....??...
    assert(false); // FIXME: udpate our distnac-emeausment-strategy wrt. "correlationType_delta.cxx" and 


  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}


