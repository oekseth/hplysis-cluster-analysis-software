 #include "hpLysis_api.h" //! ie, the simplifed API for hpLysis
#include <time.h>    // time()
#include "hp_ccm.h"
#include "kt_metric_aux.h"
#include "kt_list_1d_string.h"
/* #ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy */
/* #include "export_ppm.c" //! which is used for *.ppm file-export. */
/* #endif */

#include "kt_resultS_ccmTime.h" //! which provide lgocis for storing and exporting a combaiotn of time-matrix and clsuter-quality (CCM) matrix.
//#include "kt_resultS_ccmTime.h"
//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).
static s_kt_matrix_t __static__readDataFromObject(s_hp_clusterFileCollection data_obj, const uint data_id, const uint sizeOf__nrows, const uint sizeOf__ncols, const bool config__isTo__useDummyDatasetForValidation, const bool isTo_transposeMatrix) {
  //const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
  const char *stringOf_tagSample = data_obj.file_name;
  //const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  data_obj.tag;}
  //printf("(data=%u)\t#\t[algPos=%u]\t alg_id=%u, rand_id=%u\t\t %s \t at %s:%d\n", data_id, cnt_alg_counts, alg_id, rand_id, stringOf_tagSample, __FILE__, __LINE__);
  assert(stringOf_tagSample);
  
  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
  fileRead_config.isTo_transposeMatrix = isTo_transposeMatrix;
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(config__isTo__useDummyDatasetForValidation == false) {
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else { //! then we inveeigate using a dummy data-set:
    obj_matrixInput = initAndReturn__s_kt_matrix(sizeOf__nrows, sizeOf__ncols);
    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
      for(uint k = 0; k < obj_matrixInput.ncols; k++) {
	obj_matrixInput.matrix[i][k] = (t_float)(i*k);
      }
    }
  }
  return obj_matrixInput;
}


#define __MiFLocal__setScore(obj, row_id, col_id) ({ \
		  assert(row_id < obj.mat_result_ccm.nrows); \
		  assert(row_id < obj.mat_result_time.nrows); \
		  assert(col_id < obj.mat_result_ccm.ncols); \
		  assert(col_id < obj.mat_result_time.ncols); \
		  /*! Store results: */ \
		  obj.mat_result_ccm.matrix[row_id][col_id] = ccm_score; \
		  obj.mat_result_time.matrix[row_id][col_id] = time_result; })

#define __MiF__setString__row(obj, index, str) ({				\
	      assert(index < obj.mat_result_ccm.nrows);			\
	      assert(index < obj.mat_result_time.nrows);		\
      if(str && strlen(str)) {const char *tag = str; \
	      set_stringConst__s_kt_matrix(&(obj.mat_result_ccm), index, tag, /*addFor_column=*/false); \
	      set_stringConst__s_kt_matrix(&(obj.mat_result_time), index, tag, /*addFor_column=*/false); \
} else {char tag[1000]; memset(tag, '\0', 1000); sprintf(tag, "id=%u", block_id); \
	      set_stringConst__s_kt_matrix(&(obj.mat_result_ccm), index, tag, /*addFor_column=*/false); \
	      set_stringConst__s_kt_matrix(&(obj.mat_result_time), index, tag, /*addFor_column=*/false);} } )
#define __MiF__setString__col(obj, index, tag) ({ \
		  assert(index < obj.mat_result_ccm.ncols); \
		  assert(index < obj.mat_result_time.ncols); \
		  set_stringConst__s_kt_matrix(&(obj.mat_result_ccm), index, tag, /*addFor_column=*/true); \
		  set_stringConst__s_kt_matrix(&(obj.mat_result_time), index, tag, /*addFor_column=*/true); } )




static const uint list_tut_time_2__clustAlg_size = 6;
static e_hpLysis_clusterAlg list_tut_time_2__clustAlg[list_tut_time_2__clustAlg_size] = {
  e_hpLysis_clusterAlg_kCluster__AVG,
  //  e_hpLysis_clusterAlg_kCluster__medoid,  
  e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_kruskal_hca,
  e_hpLysis_clusterAlg_disjoint_kdTree,
  e_hpLysis_clusterAlg_disjoint,
};

static const uint list_tut_time_2__sim_size = 1;
static e_kt_correlationFunction_t list_tut_time_2__sim[list_tut_time_2__sim_size] = {
  e_kt_correlationFunction_groupOf_minkowski_euclid,
};
//!
//!
//! *************************************************************************************** 
//	    printf("at %s:%d\n", __FILE__, __LINE__);
//! Apply logics: compute for: cluster-algorithms: 
static void __tut_time_2__evalMatrix(s_kt_resultS_ccmTime_t &obj_ccm_clusterAlg, s_kt_matrix_base_t vec_2, const uint block_id, const uint cnt_blocks, const uint cnt_clusters, s_kt_list_1d_string_t *arrOf_stringNames) {
  // -------------------------------
  if(obj_ccm_clusterAlg.mat_result_ccm.nrows == 0) { //! then we intiate: 
    uint cnt_cols = (list_tut_time_2__sim_size * list_tut_time_2__clustAlg_size);
    obj_ccm_clusterAlg.mat_result_ccm  = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
    obj_ccm_clusterAlg.mat_result_time = initAndReturn__s_kt_matrix(/*rows=*/cnt_blocks, /*cols=*/cnt_cols);
    //! 
    //! Set strings:
    for(uint block_id = 0; block_id < cnt_blocks; block_id++) {		  
      const char *str_block = NULL;
      if(arrOf_stringNames) {
	assert(block_id < arrOf_stringNames->nrows);
	str_block = arrOf_stringNames->nameOf_rows[block_id];
      }
      __MiF__setString__row(obj_ccm_clusterAlg, block_id, str_block);
    }
    uint col_id_local = 0;
    for(uint sim_metric = 0; sim_metric < list_tut_time_2__sim_size; sim_metric++) {
      const s_kt_correlationMetric_t obj_sim = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_metric, e_kt_categoryOf_correaltionPreStep_none); 
      //getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
      //! Iterate throguh clsuter-algroithsm: 
      for(uint clust_alg = 0; clust_alg < list_tut_time_2__clustAlg_size; clust_alg++) {
      const e_hpLysis_clusterAlg_t enum_clust = list_tut_time_2__clustAlg[clust_alg];
	//const e_hpLysis_clusterAlg_t enum_clust = (e_hpLysis_clusterAlg_t)clust_alg;
	char tag_local[4000]; memset(tag_local, '\0', 4000); sprintf(tag_local, "%s_%s", get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(obj_sim.metric_id), get_stringOf__short__e_hpLysis_clusterAlg_t(enum_clust));
	__MiF__setString__col(obj_ccm_clusterAlg, /*index=*/col_id_local, tag_local);
	col_id_local++;
      }
    }
  }
  printf("at %s:%d\n", __FILE__, __LINE__);
  // -------------------------------
  //! 
  //! Iterate throguh sim-metrics: 
  uint col_id_local = 0;


  for(uint sim_metric = 0; sim_metric < list_tut_time_2__sim_size; sim_metric++) {
      const s_kt_correlationMetric_t obj_sim = initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_t)sim_metric, e_kt_categoryOf_correaltionPreStep_none); //getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
    //const s_kt_correlationMetric_t obj_sim = s_kt_correlationMetric_t initAndReturn__s_kt_correlationMetric_t((e_kt_correlationFunction_groupOf_minkowski_euclid)sim_metric, e_kt_categoryOf_correaltionPreStep_none); //getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
    //const s_kt_correlationMetric_t obj_sim = //getMetricObject__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t((e_kt_correlationMetric_initCategory_timeComplexitySet__small_t)sim_metric);
    assert(obj_sim.metric_id != e_kt_correlationFunction_undef);
    //! Iterate throguh clsuter-algroithsm: 
    for(uint clust_alg = 0; clust_alg < list_tut_time_2__clustAlg_size; clust_alg++) {
      e_hpLysis_clusterAlg_t enum_clust = list_tut_time_2__clustAlg[clust_alg];
      //      const e_hpLysis_clusterAlg_t enum_clust = (e_hpLysis_clusterAlg_t)clust_alg;
      if( (enum_clust != e_hpLysis_clusterAlg_disjoint_MCL) 
	  && (enum_clust != e_hpLysis_clusterAlg_kruskal_fixed_and_CCM) 
	  ) {
	//!
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	
	// (Symmetric) adjacency matrix                                                                                                                            
	if(arrOf_stringNames == NULL) { //! hten we asusemt hte inptu si sysnteitc.
	  obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = true; //true; // inputMatrix__isAnAdjecencyMatrix;	
	  //obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false; //true; // inputMatrix__isAnAdjecencyMatrix;	
	}
	obj_hp.config.corrMetric_prior_use = false; //! ie, use data-as-is.
	//obj_hp.config.corrMetric_prior_use = true; //! ie, use data-as-is.
	if(false) {
	  //! Specify that we are to use the k-means++-implmeantion defined/used in the "fast_Kmeans" software:
	  obj_hp.randomConfig.config_init.typeOf_randomNess = e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss; //! where the latter enum is define d in our "math_generateDistribution.h"
	}
	
	//! 
	//! Compute the cluster:
	printf("%s\t, at %s:%d\n", get_stringOf__short__e_hpLysis_clusterAlg_t(enum_clust), __FILE__, __LINE__);
	start_time_measurement(); //! ie, start timer.
	s_kt_matrix_t mat_inp = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&vec_2);
	if(enum_clust == e_hpLysis_clusterAlg_disjoint) {
	  obj_hp.config.kdConfig.enum_id = e_kd_tree_searchStrategy_brute_fast; //! ie, then se tthe searhc-strategy deifned in oru "kd_tree.h"
	  enum_clust = e_hpLysis_clusterAlg_disjoint_kdTree;
	}
	const bool is_ok = cluster__hpLysis_api(&obj_hp, enum_clust, 
						&mat_inp,
						//&(obj_shape_2.matrix), 
						cnt_clusters, /*npass=*/1000);
	//		  const bool is_ok = cluster__hpLysis_api(&obj_hp, enum_clust, &(obj_shape_2.matrix), cnt_clusters, /*npass=*/1000);
	assert(is_ok);
	//! 
	//! Evlauate accurayc of clsuter-prediction, where we for simplicyt use the defualt/eslibehd Silhouette CCM: 
	//s_kt_matrix_t mat_inpushallow = obj_shape_2.matrix; 
	// get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&(obj_shape_2.matrix)); 
	t_float ccm_score = T_FLOAT_MAX;
	if(obj_hp.config.corrMetric_prior_use == true) { //! then a simarlity-metirc has been computed.
	  ccm_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, &mat_inp);
	} else {
	  if( (obj_hp.obj_result_kMean.cnt_vertex > 0) && (obj_hp.obj_result_kMean.cnt_vertex != UINT_MAX) ) {
	    uint *map_clusterMembers = obj_hp.obj_result_kMean.vertex_clusterId;
	    s_kt_matrix_base_t vec_2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_inp);
	    const bool is_ok = ccm__singleMatrix__applyDefSimMetricBefore__hp_ccm(/*ccm_enum*/e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, &vec_2, map_clusterMembers, &ccm_score);
	    assert(is_ok);
	    }
	}
	const char *str_local = "cluster-algorithm"; const t_float time_result = (t_float)end_time_measurement(/*msg=*/str_local, FLT_MAX);  //! ie, stop timer.
	//!
	//! Store results:
	__MiFLocal__setScore(obj_ccm_clusterAlg, block_id, col_id_local);
	
	//!
	//! De-allcote:
	free__s_hpLysis_api_t(&obj_hp);
      }
      col_id_local++;
    }
  }
}

static void __tut_time_2__apply(s_kt_matrix_setOf_t *mat_collection, s_kt_list_1d_string_t *arrOf_stringNames, const char *resultPrefix) {
  
  //const uint cnt_cases = 1; const uint cnt_clusterCases = 1;
  const uint cnt_cases = 10; const uint cnt_clusterCases = 4;
  const uint nrows_increase = 100; uint cnt_blocks = 10; //20;
  //  const uint nrows_increase = 400; uint cnt_blocks = 5; //20;
  if(mat_collection && mat_collection->list_size) {cnt_blocks = mat_collection->list_size;}
  //      const uint nrows_increase = 1000; const uint cnt_blocks = 100;
  //!
  //! Iterate through cases wrt. matrix-cluster-seperation:
  for(uint case_id = 0; case_id < cnt_cases; case_id++) {
    printf("case_id=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
    //!
    //! Iterate through cases wrt. relative cluster-ocunt-inlfucne
    for(uint clusterCase = 0; clusterCase < cnt_clusterCases; clusterCase++) {
      //!
      //! Allcoate result matrices: 
      s_kt_resultS_ccmTime_t obj_ccm_clusterAlg = init__s_kt_resultS_ccmTime_t();
      //	s_kt_resultS_ccmTime_t obj_ = init__s_kt_resultS_ccmTime_t();
      
      const char *fileTag = "";	
      //! Construct a givne matrix: 
      for(uint block_id = 0; block_id < cnt_blocks; block_id++) {
	uint nrows = nrows_increase + (block_id * nrows_increase);
	assert(nrows != UINT_MAX);

	//! 
	//! 
	uint cnt_clusters = 2;
	if(clusterCase == 0) {
	} else if(clusterCase == 1) {
	  cnt_clusters = 2; 
	} else if(clusterCase == 2) {
	  cnt_clusters = nrows/4; 
	  assert(cnt_clusters > 0);
	  assert(cnt_clusters <= nrows);
	} else if(clusterCase == 3) {
	  cnt_clusters = nrows / 2; 
	  assert(cnt_clusters > 0);
	  assert(cnt_clusters <= nrows);
	} else {
	  assert(false); //! ie, as this option is Not supported
	}
	s_hp_clusterShapes_t obj_shape_2 = setToEmptyAndReturn__s_hp_clusterShapes_t();
	s_kt_matrix_t mat_shallow = setToEmptyAndReturn__s_kt_matrix_t();
	if(mat_collection && mat_collection->list_size) {
	  mat_shallow = mat_collection->list[block_id];
	  assert(arrOf_stringNames);
	  assert(block_id < arrOf_stringNames->nrows);
	  fileTag = arrOf_stringNames->nameOf_rows[block_id];
	  nrows = mat_shallow.nrows;
	} 
	{
	  
	  //! 
	  //! Wrap result-object into an s_kt_matrix_base object
	  // s_kt_matrix_base vec_1 = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_shape.matrix.nrows, obj_shape.matrix.ncols, obj_shape.matrix.matrix);
	  //! ----------------------------------------------------------------------
	  //!
	  //! Allocate result-matrix:
	  // s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
	  //!
	  //! Build a vertex-clsuter-membership-set:
	  if(case_id == 0) {
	    obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "linear_1_100_minMax";
	  } else if(case_id == 1)  {
	    obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/80, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "linear_1_80_minMax";
	  } else if(case_id == 2)  {
	    obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/40, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "linear_1_40_minMax";
	  } else if(case_id == 3)  {
	    obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/20, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "linear_1_20_minMax";
	  } else if(case_id < cnt_blocks)  { //! then we use a 'random case-seliont', where muliple 'calls' are sued to explroe differetn 'random calls'.
	    obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_random, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
	    fileTag = "random";
	  } else {
	    assert(false); //! ie, as this option is Not supported
	  }
	}
	
	//const uint cnt_clusters = case_id*config_clusterBase;
	bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
	assert(is_ok);    
	//!
	//! Fetch ferecne-facts: 
	uint *map_clusterMembers = obj_shape_2.map_clusterMembers;
	s_kt_matrix_base_t vec_2 = getShallow_matrix_base__s_hp_clusterShapes_t(&obj_shape_2);
	if(mat_collection && mat_collection->list_size) {
	  vec_2 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_shallow);
	}
	{ //! Apply logics: compute for: cluster-algorithms: 
	  __tut_time_2__evalMatrix(obj_ccm_clusterAlg, vec_2, block_id, cnt_blocks, cnt_clusters, arrOf_stringNames);
	}
	
	printf("at %s:%d\n", __FILE__, __LINE__);
	//! *************************************************************************************** 
	//! 
	//! De-allocate: 
	free__s_hp_clusterShapes_t(&obj_shape_2);
      }
      // -------------------------------------------
      //! 
      //! 
      { //! Write out result: 
	assert(fileTag);
	assert(strlen(fileTag)); 
	assert(resultPrefix); 
	assert(strlen(resultPrefix));
	assert(strlen(resultPrefix) < 1000);
	{
	  char file_name[2000];  memset(file_name, '\0', 2000); sprintf(file_name, "%s_%s_valueSplitCase_%u_clusterCountCase_%u_ccm_matrix_config.tsv", resultPrefix, fileTag, case_id, clusterCase); 
	  free_andExport__s_kt_resultS_ccmTime_t(&obj_ccm_clusterAlg, file_name);
	}
      }
      // -------------------------------------------
    }
  }
}




static void __eval__dataCollection__tut_time_2(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size, const char *resultPrefix, const bool rowName_identifiesClusterId, const bool isTo_transposeMatrix) {
  assert(mapOf_realLife); assert(mapOf_realLife_size > 0);
  //!
  //! Transform data-set to a differnet foramt: 
  s_kt_matrix_setOf_t mat_collection = initAndReturn__s_kt_matrix_setOf_t(mapOf_realLife_size, 0, 0);  
  s_kt_list_1d_string_t arrOf_stringNames = setToEmpty_andReturn__s_kt_list_1d_string_t();
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
    s_kt_matrix_t obj_matrixInput = __static__readDataFromObject(mapOf_realLife[data_id], data_id, 10, 10, /*config__isTo__useDummyDatasetForValidation*/false, isTo_transposeMatrix); //, self->sizeOf__nrows, ->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
    assert(obj_matrixInput.nrows > 0);
    const char *tag = mapOf_realLife[data_id].tag;
    assert(tag); assert(strlen(tag));
    const char *tag_suffix = strrchr(tag, '/');
    if(tag_suffix && strlen(tag_suffix)) {
      tag = tag_suffix + 1; //! ie, to avodi the fodler-speerator from clsuttienrg the results
    }
    //! Add: string:
    set_stringConst__s_kt_list_1d_string(&arrOf_stringNames, data_id, tag);
    //! Add: matrix:
    assert(mat_collection.list[data_id].nrows == 0); //! ie, to avoid the need for de-allocation.
    mat_collection.list[data_id] = obj_matrixInput; //! ie, copy the cotnent.
  }
  //!
  //! Apply logics: 
  { //! Comptue where gold is usedinfed through MINE+HCA:
    __tut_time_2__apply(&mat_collection, &arrOf_stringNames, resultPrefix);

  }
  /*   char resultPrefix_local[10000]; memset(resultPrefix_local, '\0', 10000); sprintf(resultPrefix_local, "%s_isToUseHCAMINE", resultPrefix); */
  /*   tut_kd_3__fromMatrix(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix_local, /\*config_rowNameIdentifiesCluster=*\/false); //! ie, use HCA+MINE. */
  /* } */
  /* if(rowName_identifiesClusterId) { //! Comptue where gold is usedinfed through overallpgin row-names: */
  /*   tut_kd_3__fromMatrix(mat_collection.list, mapOf_realLife_size, &arrOf_stringNames, resultPrefix, /\*config_rowNameIdentifiesCluster=*\/true); */
  /* }  */
  //!
  //! De-allocate:
  free__s_kt_matrix_setOf_t(&mat_collection);
  free__s_kt_list_1d_string(&arrOf_stringNames);
}


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief  evauatles a few clsuter-algortihsm on both syentic and real-lfie data-sets
   @author Ole Kristian Ekseth (oekseth, 06. okt. 2017).
   @remarks related-tut-examples:
   -- "tut_time_1_data_syntetic_wellDefinedClusters.c": 'his' is a subset of the latter.
   -- "tut_kd_3_data_simMetric.c". 
   -- "hp_evalHypothesis_algOnData.c": an API for data-anlaysis.
   -- "tut_kd_1_cluster_multiple_simMetrics.c": min-max-evaluation-accuracy wrt. CCMs and simalrityu-emtrics.
**/
int main(const int array_cnt, char **array) 
#else
  int tut_time_2_clustAlg_syntAndReal(const int array_cnt, char **array)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif 

  { //! Investigate for different 'base' CCM metrics: 
    const char *resultPrefix = "tut_time_2";
    //{ //! matrix-CCMs: 
    //! Note: capture the isolatoeted time-cost assotied to CCM-cotmpatuion, ie, for which cCM-comptaution is not invovled/integrated. 
    __tut_time_2__apply(NULL, NULL, resultPrefix);
  }
  //! 
  //! 
  //! 
  { //! Load for different real-life data-sets:
#include "tut_kd_3_data_simMetric__configFileRead.c"  //! which is used to cofnigure fiel-reading process, adding anumorus diffenre toptiosn.
    const bool isTo_transposeMatrix = true;
    const char *resultPrefix = "tut_evalKdSim3_eachEnsamble_transp";
    {
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
      //#include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
      //#include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection__tut_time_2(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset, /*rowName_identifiesClusterId=*/false, isTo_transposeMatrix);
    }
  }
  { //! Load for different real-life data-sets:
#include "tut_kd_3_data_simMetric__configFileRead.c"  //! which is used to cofnigure fiel-reading process, adding anumorus diffenre toptiosn.
    const bool isTo_transposeMatrix = false;
    const char *resultPrefix = "tut_evalKdSim3_eachEnsamble";
    {
#include "tut__aux__dataFiles__realKT.c" //! ie, the file which hold the cofniguraitosn to be used.
      //#include "tut__aux__dataFiles__FCPS.c" //! ie, the file which hold the cofniguraitosn to be used.
      //#include "tut__aux__dataFiles__realMine.c" //! ie, the file which hold the cofniguraitosn to be used.
      allocOnStack__char__sprintf(2000, resultPrefix__dataSubset, "%s_%s", resultPrefix, nameOf_experiment);
      __eval__dataCollection__tut_time_2(mapOf_realLife, mapOf_realLife_size, /*resultPrefix=*/resultPrefix__dataSubset, /*rowName_identifiesClusterId=*/false, isTo_transposeMatrix);
    }
  }

  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
#endif
  return true;
}

#undef __MiF__isTo_evaluate__local
#undef __MiFLocal__setScore
#undef __MiF__setString__row
#undef __MiF__setString__col

