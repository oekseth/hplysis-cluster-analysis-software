/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


/**
   @brief a wrapper-template to generate a terminal-bash-help-message
   @remarks last generated at Fri Dec  2 15:20:45 2016 from "build_codeFor_tiling.pl"
 **/

"Usage (input data):\n"
"----------------------------------\n"
"-input-file-1=<your-file-name> the matrix of data in the first matrix to evaluate wrt. correlations;\n"
"-input-file-2=<your-file-name> the matrix of data in the second matrix to evaluate wrt. correlations: optional: if not set we assume 'input-file-1' is to be used 'as-is';\n---\n"
"-input-file-1-mask=<your-file-name> optional: used to filter the interesting cells in the input-matrix: combined with 'non-set' valeus in the 'input-file-1' to describe the set of cells to be used in cluster-analysis; if used we expec thte matrix to hold 'binary' cells (ie, '0' or '1');\n"
"-input-file-2-mask=<your-file-name> optional: used to filter the interesting cells in the input-matrix: combined with 'non-set' valeus in the 'input-file-2' to describe the set of cells to be used in cluster-analysis; should Not be used if only '-inptu-file-1' is provided as argument; if used we expec thte matrix to hold 'binary' cells (ie, '0' or '1');\n--\n"
"-input-file-1-columnWeights=<your-file-name> optional: if set then we re-weight the  column-values based on the weights assicated to each column-value;\n"
"-input-file-2-columnWeights=<your-file-name> optional: if set then we re-weight the  column-values based on the weights assicated to each column-value;\n---\n"
"-result-file=<string> the result-file to export the result to: if not set then we write the result to the standard-out-channel (ie, 'stdout');\n"
"-result-format=<matrix|relation> which is used to decide the type of result-format;\n"
"\n\n"

"Usage (input dimension):\n"
"----------------------------------\n"
"-cnt-rows=<1, .....> which if used implies that we apply the HpLysis-software on a default randomized matrix with cnt_rows rows;\n"
"-cnt-cols=<1, .....> which if used implies that we apply the HpLysis-software on a default randomized matrix with cnt_features features;\n"
"-isTo_transposeMatrix=<0|1> which if set to '1' results in the matrix to be transposed. An example-use-case is where the rows represents individual guinea pigs and the columns different litter-sizes: in order to correlate the litter-sizes wrt. the guinea pigs (and not the individuals them self) we need to transpose (ie, 'invert') the matrix (ie, the input-data);\n"
"\n\n"

"Usage (result format):\n"
"----------------------------------\n"
"-input-use-stream-explicit-size=<0|1>  is an alternative to '-input-file-1'. Is to be used if only one input-matrix is to be evaluated; option to be set if the input-matrix is to be 'piped' into this tool, eg, as part of a software tool-chain;\n"
"-include-stringIdentifers-in-matrix=<0|1> which if set to '1' implies that each column and each row ill being with a string-identifier (ie, if string-identifiers are used in the input-data loaded by a user);\n"
"-exportInto-javaScript-syntax=<0|1> which if set to '1' implies that we make use of Java-Script syntax in our result-export-data;\n"
"-exportInto-JSON-syntax=<0|1> which if set to '1' implies that we make use of JSON syntax in our result-export-data;\n"
"-result-isTo-exportInputFile=<0|1> which if set implies that we include the input-data in the result-section, eg, iot. to compare the results with the '-sample-data-distribution=<...>' option;\n"
"\n\n"

"Correlation-configuration:\n"
"for cluster-analysis to be meaningfully performed we need options for three different correlation-metric-step: (a) before filtering, (b) after filtering, and (c) during cluster-analysis. This in order to ensure that both the filtering-step and cluster-analysis step reflects properties of the input-data.\n"
"----------------------------------\n"
"-help--printAll-correlationMetrics=<> optional: if set then we print out all the correlation-matrix, options which may be used to increase specify of your clustering-results, eg, for the 'before-filter-correlation-type-id' option;\n---\n"
"-before-filter-correlation-type-id=<correlation-enum-type> refers to the 'e_kt_correlationFunction' enum-names: if set describes the correlation-metric to be computed (based on each input-matrix) before the application of user-specified data-filters;\n"
"-before-filter-correlation-type-isTo-export= optional: if set then we export the result to the user-specified result-format;\n"
"-before-filter-correlation-preStep=<rank|binary|none> extends the '-before-filter-correlation-type-id=' correlation-metric with the type of data-filters: identifies how the similarities are to be weighted before application of the '-before-filter-correlation-type-id=' correlation-metric, eg, where 'rank' refers to the correlation-metric by 'Spearman';\n----\n"
"-after-filter-correlation-type-id=<correlation-enum-type> refers to the 'e_kt_correlationFunction' enum-names: if set describes the correlation-metric to be computed (based on each input-matrix) before the application of user-specified data-filters;\n"
"-after-filter-correlation-type-isTo-export= optional: if set then we export the result to the user-specified result-format;\n"
"-after-filter-correlation-preStep=<rank|binary|none> extends the '-after-filter-correlation-type-id=' correlation-metric with the type of data-filters: identifies how the similarities are to be weighted after application of the '-after-filter-correlation-type-id=' correlation-metric, eg, where 'rank' refers to the correlation-metric by 'Spearman';\n----\n"
"-in-clustering-correlation-type-id=<correlation-enum-type> refers to the 'e_kt_correlationFunction' enum-names: if set describes the correlation-metric to be computed (based on each input-matrix) before the application of user-specified data-filters;\n"
"-in-clustering-correlation-preStep=<rank|binary|none> extends the '-in-clustering-correlation-type-id=' correlation-metric with the type of data-filters: identifies how the similarities are to be weighted before application of the '-in-clustering-correlation-type-id=' correlation-metric, eg, where 'rank' refers to the correlation-metric by 'Spearman';\n----\n"
"-ifTwoMatrix-whenToMerge=<beforeFilter|afterFilter|inClustering> optional: specify at which execution-point two input-matrices to be merged: if you use two input-matrices then the execution-point of data-merging may significantly affect the result of your cluster-analysis. Note: the option is used 'internally' to figure out how to 'handle' (or: 'interpret') the case where it is unclear when are we to merge different data-sets (ie, if two input-matrices are given instead of only one).;\n"
"\n\n"

"Cluster-analysis:\n"
"----------------------------------\n"
"-hca_method=<single|maximum|average|centroid> describes the different hierarchical clustering-algorithms (HCAs), eg, for building dendograms; mandatory option if the HCA-dendogram cluster is to be used;\n----\n"
"-kmeans-number-of-clusters=<2...> the number of clusters to partition your data-set into; mandatory option if the k-means cluster is to be used;\n"
"-kmeans-number-of-iterations=<10...> optional: the maximum number of iteraitons to be used before the clustering-analsysis completes (ie, converges): an increased 'kmeans-npass' algorithm is expected to both increase the exeuction-time and the result-accruacies: for details please contact the authors of the hpLysiss software;\n"
"-kmean-useMedian=<0, .....> optional: which if set implies that the Median is to be used in the identification of the central vertices in each cluster, an option which is expected to result in a computation-slowdown: alternatively (ie, by default) the Mean is used (in the identified of central nodes in each cluster);\n"
"-kmean-alternativeAlgorithm-useMedoid=<0, .....> optional: if set implies that the 'https://en.wikipedia.org/wiki/Medoid' algorithm is usdted instead of the k-means algorithm;\n---\n"
"-som-apply=<0, .....> which if used states that the Self Organising Map (SOM) cluster-algorithm is to be applied; a mandatory argument if SOM is to be applied to your data-set;\n"
"-som-Tau=<0.01....> optional: is the accuracy-threshold to be used in identification of a 'converged' cluster.;\n"
"-som-number-of-iterations=<1, .....> optional: is the number of iterations to be used before the SOM-algorithm converges: conceptually similar to the '-kmeans-number-of-iterations' parameter;\n"
"-som_gridSize_x=<1, .....> optional: used in SOM-clustering to identify the number of grid-elements to partition a data-set in along the x-axis;\n"
"-som_gridSize_y=<1, .....> optional: used in SOM-clustering to identify the number of grid-elements to partition a data-set in along the y-axis;\n----\n"
"-pca-apply= which if used states that the Prinicipal Component Analysis (PCA) cluster-algorithm is to be applied; a mandatory argument if PCA is to be applied to your data-set;\n"
"\n\n"

"Adjustment of the input data:\n"
"----------------------------------\n"
"-sample-data-distribution=<random|uniform|binomial_p05|binomial_p010|binomial_p005|flat|linear-equal|linear-differentCoeff-b|linear-differentCoeff-a|sinus|lines-different-ax|lines-curved|lines-different-ax-and-axx|lines-ax-inverse-x|lines-circle|lines-sinsoid|lines-sinsoid-curved> optional: describes the type of sample-data-set to be evaluated (ie, if any): used to test accuracy of the HpLysis correlation-score; ignored if input-data is provided (by the user); for the latter options with a 'lines-' prefix '-noise-low' and '-noise-medium' may be used to indicate the noise-level (eg, to model in-accuracies in the evaluated data-set);\n"
"-sample-data-distribution-secondMatrix=<random|uniform|binomial_p05|binomial_p010|binomial_p005|flat|linear-equal|linear-differentCoeff-b|linear-differentCoeff-a|sinus|lines-different-ax|lines-curved|lines-different-ax-and-axx|lines-ax-inverse-x|lines-circle|lines-sinsoid|lines-sinsoid-curved> optional: if used then there are two interpretation-cases: case(a) if no input-data-sets are specified then 'this' is used to build the second data-comparison-matrix; case(b) if a data-matrix is specified as inptu then we 'merge' the '-sample-data-distribution-secondMatrix=<...>' with your input-data-matrix;\n"
"-percent-toAppendWithSampleDistributionFor-rows=<0, 100> which may be used to merge data-sets and '-sample-data-distribution=<..>': describes the percent of rows which are to be appended with the sample-distribution. To illustrate, if 'percent-toAppendWithSampleDistributionFor-rows=50', '-cnt-rows=10' and '-sample-data-distribution=uniform' then we include 10*50/100=5 rows with a uniform distribution: in brief, the combination of known distributions to 'unknown' data-sets simplifies the exploration and interpretation, eg, wrt. assertion of how correlation-significance. Wrt. the 'base-comparison-data' the '-percent-toAppendWithSampleDistributionFor-rows=<..>' may be used in combination with the options of '-input-use-stream-explicit-size=<>' and '-input-file=<>';\n"
"-percent-toAppendWithSampleDistributionFor-columns=<0, 100> similar to '-percent-toAppendWithSampleDistributionFor-rows=<..>' optional argument, with difference that we for this option introduce noise into the columns;\n"
"\n\n"

"Data-filtering:\n"
"----------------------------------\n"
"-filter-eitherOr-percentValuesAboveCountForScalarValue-row-min=<0.001 ... > min: remove rows where: the number of cells have values outside your X per-cent threshold;\n"
"-filter-eitherOr-percentValuesAboveCountForScalarValue-row-max=<0.001 ... > max: remove rows where: the number of cells have values outside your X per-cent threshold;\n"
"-filter-eitherOr-percentValuesAboveCountForScalarValue-columns-min=<0.001 ... > min: remove columns where: the number of cells have values outside your X per-cent threshold;\n"
"-filter-eitherOr-percentValuesAboveCountForScalarValue-columns-max=<0.001 ... > max: remove columns where: the number of cells have values outside your X per-cent threshold;\n"
"-filter-eitherOr-absoluteValueMinValue=<float-min, float-max> maximum difference between extreme of your specified min-max per-cent-values;\n"
"-filter-eitherOr-absoluteValueMinValue-row=<0.001 ... > row: the per-cent variation for values inside the 'maximum difference between the extreme' values;\n"
"-filter-eitherOr-absoluteValueMinValue-column=<0.001 ... > column: the per-cent variation for values inside the 'maximum difference between the extreme' values;\n"
"-filter-eitherOr-valueDiff-rows-min=<float-min, float-max> min: remove rows where: difference between cells is outside X;\n"
"-filter-eitherOr-valueDiff-rows-max=<float-min, float-max> max: remove rows where: difference between cells is outside X;\n"
"-filter-eitherOr-valueDiff-cols-min=<float-min, float-max> min: remove columns where: difference between cells is outside X;\n"
"-filter-eitherOr-valueDiff-cols-max=<float-min, float-max> max: remove columns where: difference between cells is outside X;\n"
"-filter-eitherOr-STD-rows-min=<float-min, float-max> min: remove rows where: standard deviation (STD) is outside X;\n"
"-filter-eitherOr-STD-rows-max=<float-min, float-max> max: remove rows where: standard deviation (STD) is outside X;\n"
"-filter-eitherOr-STD-cols-min=<float-min, float-max> min: remove columns where: standard deviation (STD) is outside X;\n"
"-filter-eitherOr-STD-cols-max=<float-min, float-max> max: remove columns where: standard deviation (STD) is outside X;\n"
"-filter-eitherOr-kurtosis-rows-min=<float-min, float-max> min: remove rows where: kurtosis is outside X;\n"
"-filter-eitherOr-kurtosis-rows-max=<float-min, float-max> max: remove rows where: kurtosis is outside X;\n"
"-filter-eitherOr-kurtosis-cols-min=<float-min, float-max> min: remove columns where: kurtosis is outside X;\n"
"-filter-eitherOr-kurtosis-cols-max=<float-min, float-max> max: remove columns where: kurtosis is outside X;\n"
"-filter-eitherOr-skewness-rows-min=<float-min, float-max> min: remove rows where: skewness is outside X;\n"
"-filter-eitherOr-skewness-rows-max=<float-min, float-max> max: remove rows where: skewness is outside X;\n"
"-filter-eitherOr-skewness-cols-min=<float-min, float-max> min: remove rows where: skewness is outside X;\n"
"-filter-eitherOr-skewness-cols-max=<float-min, float-max> max: remove rows where: skewness is outside X;\n"
"-filter-cellMask-valueUpperLower-min=<float-min, float-max> min: remove all cells in the matrix outside the value-threshold X;\n"
"-filter-cellMask-valueUpperLower-max=<float-min, float-max> max: remove all cells in the matrix outside the value-threshold X;\n"
"-filter-cellMask-meanAbsDiff-row-min=<float-min, float-max> min: remove cells where: the mean absolute difference is outside X;\n"
"-filter-cellMask-meanAbsDiff-row-max=<float-min, float-max> max: remove cells where: the mean absolute difference is outside X;\n"
"-filter-cellMask-meanAbsDiff-column-min=<float-min, float-max> min: remove cells where: the mean absolute difference is outside X;\n"
"-filter-cellMask-meanAbsDiff-column-max=<float-min, float-max> max: remove cells where: the mean absolute difference is outside X;\n"
"-filter-cellMask-medianAbsDiff-row-min=<float-min, float-max> min: remove cells where: the median absolute difference is outside X;\n"
"-filter-cellMask-medianAbsDiff-row-max=<float-min, float-max> max: remove cells where: the median absolute difference is outside X;\n"
"-filter-cellMask-medianAbsDiff-column-min=<float-min, float-max> min: remove cells where: the median absolute difference is outside X;\n"
"-filter-cellMask-medianAbsDiff-column-max=<float-min, float-max> max: remove cells where: the median absolute difference is outside X;\n"
"-adjustValues-log=<0|1> adjust each cell-value by the absoltue log(..) value;\n"
"-adjustValues-subract-from-row=<rank|binary|none> rows: subtract offset dynamic-X from each row;\n"
"-adjustValues-subract-from-columns=<rank|binary|none> rows: subtract offset dynamic-X from each column;\n"
"-adjustValues-sum-of-squares-rows=<0|1> remove the sum-of-squares from each row;\n"
"-adjustValues-sum-of-squares-columns=<0|1> remove the sum-of-squares from each column;\n"
