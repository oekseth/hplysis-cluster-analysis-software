#include "s_dataStruct_matrix_dense.h"
#include <math.h>
#include "math_generateDistribution.h"
//#include "cluster.h"

//! @return the string-representation of enum-type s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t
const char *static_getStrOf_enum_function(const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function) {
  if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_flat) {
    return "flat";
  } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_equal) {
    return "linear-equal";
  } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_b) {
    return "linear-differentCoeff-b";
  } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_a) {
    return "linear-differentCoeff-a";
  } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus) {
    return "sinus";
    /* } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear) { */
    // return "";
  } else {
    assert(false); //! ie, we then need to add support for this case.
    return NULL;
  }
}
//! @return the string-representation of enum-type s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t
const char *static_getStrOf_enum_NoiseFunction(const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function) {
  if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax) {return "lines-different-ax";}
  else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_curved) {return "lines-curved";}
  else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_and_axx) {return "lines-different-ax-and-axx";}
  else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_inverse_x) {return "lines-ax-inverse-x";}
  else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_circle) {return "lines-circle";}
  else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid) {return "lines-sinsoid";}
  else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid_curved) {return "lines-sinsoid-curved";}
  //else if(typeOf_function == ) {return "";}
  //else if(typeOf_function == ) {return "";}  
  else {
    assert(false); //! ie, we then need to add support for this case.
    return NULL;
  }
}
//! @return the string-representation of enum-type s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t
const char *static_getStrOf_enum_noise(const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_function) {
  if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_low)         {return "low";}
  else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_medium) {return "medium";}
  else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_large)  {return "large";}
  /* else if(typeOf_function == ) {return "";} */
  /* else if(typeOf_function == ) {return "";} */
  /* else if(typeOf_function == ) {return "";} */
  /* else if(typeOf_function == ) {return "";} */
  /* else if(typeOf_function == ) {return "";} */
  /* else if(typeOf_function == ) {return "";}   */
  else {
    assert(false); //! ie, we then need to add support for this case.
    return NULL;
  }
}

//! Intiate (ie, set to empty) the "s_dataStruct_matrix_dense_t" object wrt. the memory-refrences (and not the object-reference itself).
void init_s_dataStruct_matrix_dense_t_setToEmpty(s_dataStruct_matrix_dense_t *obj) {
  // printf("sets-to-empty, at %s:%d\n", __FILE__, __LINE__);
  obj->nameOf_rows = NULL; obj->nameOf_columns = NULL; obj->matrixOf_data = NULL;
  obj->cnt_columns = 0; obj->cnt_rows = 0;
  obj->cnt_columns_allocated = 0; obj->cnt_rows_allocated = 0;
  obj->_word_max_length = 0;
  obj->stringOf_defaultRowPrefix = "row";
  obj->__allocated_stringOf_defaultRowPrefix = NULL;
  //  printf("\n\nreset-stringOf_defaultRowPrefix, at %s:%d\n", __FILE__, __LINE__);
}

//! @return true if "obj" is set to empty, eg, through a call to our "init_s_dataStruct_matrix_dense_t_setToEmpty(obj)".
bool is_setTo_empty(const s_dataStruct_matrix_dense_t *obj) {
  assert(obj);
  if(obj->nameOf_columns != NULL) {return false;}
  if(obj->nameOf_rows != NULL) {return false;}
  if(obj->matrixOf_data != NULL) {return false;}
  if(obj->cnt_columns != 0) {return false;}
  if(obj->cnt_rows != 0) {return false;}
  
  // printf("\n\nobj-is-reset,. at %s:%d\n", __FILE__, __LINE__);

  //! At this exeuction-point we asusme the object is set to empty:
  return true;
}

/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
   @param <word_max_length> which if set iodentifies the max-word-length in the data: if word_max_length is greater than zero we allocate the "nameOf_columns" and "nameOf_rows" object-attributes.
 **/
void init_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj,  uint cnt_rows,  uint cnt_columns,  uint _word_max_length){
//void init_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns, const uint _word_max_length){
  assert(obj);
  //! First itnaitet he object:
  assert(obj->nameOf_rows == NULL);
  //  init_s_dataStruct_matrix_dense_t_setToEmpty(obj);
  uint word_max_length = _word_max_length;

  if(word_max_length < 1000) {
    word_max_length = 1000; //! ie, our default assumpiotn    
  }
  assert(word_max_length != UINT_MAX);

  //! Update the counts:
  obj->cnt_rows = cnt_rows; obj->cnt_columns = cnt_columns;
  //! Then allcoate the data, ie, if the lengts are set.
  const uint new_word_size = word_max_length + 1;     const char default_value_alloc = '\0';
  if(cnt_rows && word_max_length) {
    assert(obj->nameOf_rows == NULL);
    obj->nameOf_rows = allocate_2d_list_char(cnt_rows, new_word_size, default_value_alloc);
    obj->cnt_rows_allocated = cnt_rows;
    assert(obj->nameOf_rows != NULL);
  }
  if(cnt_columns && word_max_length) {
    // printf("allocates for %u columns, at %s:%d\n", cnt_columns, __FILE__, __LINE__);
    // printf("allocates for column-names-cnt=%u, w/max-word-size=%u, at %s:%d\n", cnt_columns, new_word_size, __FILE__, __LINE__);
    assert(obj->nameOf_columns == NULL);
    obj->nameOf_columns = allocate_2d_list_char(cnt_columns, new_word_size, default_value_alloc);
    obj->cnt_columns_allocated = cnt_columns;
  } else {
    // printf("not allocated for column-names, at %s:%d\n", __FILE__, __LINE__);
  }
  if(cnt_rows && cnt_columns) {
    const t_float default_value_float = 0;
    obj->matrixOf_data = allocate_2d_list_float(cnt_rows, cnt_columns, default_value_float);
    for(uint i = 0; i < cnt_rows; i++) {
      for(uint k = 0; k < cnt_columns; k++) {obj->matrixOf_data[i][k] = T_FLOAT_MAX;}} //! ie, set the vlaue epxlcitly to 'empty' (oekseth, 06. feb. 2017).
  }
  obj->_word_max_length = word_max_length;
  obj->stringOf_defaultRowPrefix = "row";
  obj->__allocated_stringOf_defaultRowPrefix = NULL;
  // printf("reset-stringOf_defaultRowPrefix, at %s:%d\n", __FILE__, __LINE__);
}


/**
   @brief transpose the given matrix.
   @param <self> is the matrix to transpose.
 **/
void s_dataStruct_matrix_dense_transpose(s_dataStruct_matrix_dense_t *self) {
  assert(self); assert(self->cnt_rows); assert(self->cnt_columns);

  //! Allocat ea new object:
  s_dataStruct_matrix_dense_t new_obj; init_s_dataStruct_matrix_dense_t_setToEmpty(&new_obj);
  init_s_dataStruct_matrix_dense_t(&new_obj, self->cnt_columns, self->cnt_rows, self->_word_max_length); //! Allocate using the 'ivnerse' approach:
  //! Copy:
  for(uint i = 0; i < new_obj.cnt_rows; i++) {
    //! Specify the scores:
    for(uint y = 0; y < new_obj.cnt_columns; y++) {
      new_obj.matrixOf_data[i][y] = self->matrixOf_data[y][i]; //! ie, the transpsoed.
    }
  }
  assert(self->cnt_rows == new_obj.cnt_columns); //! ie, the transpsoed proeprty
  assert(self->cnt_columns == new_obj.cnt_rows); //! ie, the transpsoed proeprty

  //! Copy the names, where the 'columns' are copyied to the rows, and rows are copied to the columns:
  for(uint row_id = 0; row_id < new_obj.cnt_rows; row_id++) {
    if(self->nameOf_columns[row_id]) {
      if(false) {printf("include-new-row-name=\"%s\", at %s:%d\n", self->nameOf_columns[row_id], __FILE__, __LINE__);}
      assert(self->nameOf_columns[row_id]);
      if(strlen(self->nameOf_columns[row_id])) {
	strcpy(new_obj.nameOf_rows[row_id], self->nameOf_columns[row_id]); 
      }
    }
  }
  for(uint column_id = 0; column_id < new_obj.cnt_columns; column_id++) {
    if(self->nameOf_rows[column_id]) {
      if(false) {printf("include-new-column-name=\"%s\", at %s:%d\n", self->nameOf_rows[column_id], __FILE__, __LINE__);}
      if(strlen(self->nameOf_rows[column_id])) {
	strcpy(new_obj.nameOf_columns[column_id], self->nameOf_rows[column_id]); 
      }
    }
  }
  /* //! Copy the names, where the 'columns' are copyied to the rows, and rows are copied to the columns: */
  /* memcpy((void*)new_obj.nameOf_rows[0], (void*)self->nameOf_columns[0], sizeof(char)*self->cnt_columns*self->_word_max_length); */
  /* memcpy((void*)new_obj.nameOf_columns[0], (void*)self->nameOf_rows[0], sizeof(char)*self->cnt_rows*self->_word_max_length); */
  
  new_obj.stringOf_defaultRowPrefix = self->stringOf_defaultRowPrefix;

  //! De-allocate the old object:
  free_s_dataStruct_matrix_dense_t(self);
  //! Copy our pointers to the new object:
  self->cnt_rows = new_obj.cnt_rows;
  self->cnt_columns = new_obj.cnt_columns;
  self->_word_max_length = new_obj._word_max_length;
  self->matrixOf_data = new_obj.matrixOf_data;
  self->nameOf_rows = new_obj.nameOf_rows;
  self->nameOf_columns = new_obj.nameOf_columns;
  self->stringOf_defaultRowPrefix = new_obj.stringOf_defaultRowPrefix;
  // printf("reset-stringOf_defaultRowPrefix, at %s:%d\n", __FILE__, __LINE__);
}

//! Set default row-names:
static void __set_default_rowNames(s_dataStruct_matrix_dense_t *obj, const uint row_startPos, const uint cnt_rows, const char *row_prefix) {  
  assert(obj->nameOf_rows);    
  // printf("row_prefix=\"%s\", at %s:%d\n", row_prefix, __FILE__, __LINE__);
  for(uint i = row_startPos; i < (uint)cnt_rows; i++) {
    //assert(strlen(self->nameOf_rows[i]));
    sprintf(obj->nameOf_rows[i], "%s-%u", row_prefix, i);
  }
}

//! Set default column-names:
static void __set_default_columnNames(s_dataStruct_matrix_dense_t *obj, const uint column_startPos, const uint cnt_columns, const char *column_prefix) {
  assert(obj->nameOf_columns);    
  assert(column_startPos <= cnt_columns);
  
  // printf("column_prefix=\"%s\" w/length=%u<%u, for range[%u, %u], at %s:%d\n", column_prefix, strlen(column_prefix), obj->_word_max_length, column_startPos, cnt_columns, __FILE__, __LINE__);
  for(uint i = column_startPos; i < (uint)cnt_columns; i++) {
    sprintf(obj->nameOf_columns[i], "%s-%u", column_prefix, i);
  }
}


/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object, and intiate the column-headers and row-headers to 'sample-texts'.
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
 **/
void init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns){
  assert(obj);
  //! First itnaitet he object:
  init_s_dataStruct_matrix_dense_t_setToEmpty(obj);

  //! Update the counts:
  obj->cnt_rows = cnt_rows; obj->cnt_columns = cnt_columns;

  uint word_max_length = 100; //! ie, an uppper trehshold.
  if(obj->stringOf_defaultRowPrefix) {
    word_max_length = 100 + strlen(obj->stringOf_defaultRowPrefix);
  }
  if(obj->_word_max_length == 0) {
    obj->_word_max_length = word_max_length;
  }

  //! Then allcoate the data, ie, if the lengts are set.
  assert(word_max_length != UINT_MAX);
  const uint new_word_size = word_max_length + 1;     const char default_value_alloc = '\0';
  if(cnt_rows && word_max_length) {
    assert(cnt_rows > 0);
    assert(cnt_rows != UINT_MAX);
    assert(new_word_size > 0);
    obj->nameOf_rows = allocate_2d_list_char(cnt_rows, new_word_size, default_value_alloc);
  }
  if(cnt_columns && word_max_length) {
    // printf("allocates for column-names-cnt=%u, at %s:%d\n", cnt_columns, __FILE__, __LINE__);
    obj->nameOf_columns = allocate_2d_list_char(cnt_columns, new_word_size, default_value_alloc);
  } else {
    // printf("not allocated for column-names, at %s:%d\n", __FILE__, __LINE__);
  }

  { //! Set default row-names:
    if(obj->nameOf_columns) {
      assert(obj->nameOf_columns);
      assert(obj->stringOf_defaultRowPrefix);
      //printf("stringOf_defaultRowPrefix=\"%s\", _word_max_length=%u, at %s:%d\n", obj->stringOf_defaultRowPrefix, obj->_word_max_length, __FILE__, __LINE__);
      assert(strlen(obj->stringOf_defaultRowPrefix) < obj->_word_max_length);
      __set_default_columnNames(obj, 0, cnt_columns, obj->stringOf_defaultRowPrefix);
    }
    /* for(uint i = 0; i < (uint)cnt_columns; i++) { */
    /*   sprintf(obj->nameOf_columns[i], "column-%u", i); */
    /* } */
    
    if(obj->nameOf_rows) {
      __set_default_rowNames(obj, 0, cnt_rows, obj->stringOf_defaultRowPrefix);
    }
  }


  if(cnt_rows && cnt_columns) {
    const t_float default_value_float = 0;
    obj->matrixOf_data = allocate_2d_list_float(cnt_rows, cnt_columns, default_value_float);
  }
}

//! De-allocat the "s_dataStruct_matrix_dense_t" object wrt. the memory-refrences (and not the object-reference itself).
void free_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj) {
  // printf("de-allocates, at %s:%d\n", __FILE__, __LINE__);
  assert(obj);
  if(obj->nameOf_rows) {free_2d_list_char(&(obj->nameOf_rows), obj->cnt_rows_allocated);}
  if(obj->nameOf_columns) {free_2d_list_char(&(obj->nameOf_columns), obj->cnt_columns_allocated);}
  if(obj->matrixOf_data) {free_2d_list_float(&(obj->matrixOf_data), obj->cnt_rows_allocated);}
  if(obj->__allocated_stringOf_defaultRowPrefix != NULL) {free_1d_list_char(&(obj->__allocated_stringOf_defaultRowPrefix));}
  obj->cnt_columns = 0; obj->cnt_rows = 0;
  obj->cnt_columns_allocated = 0; obj->cnt_rows_allocated = 0;
}

//! Copy a row to a spcified row-index.
void copyRow_intoPre_initiatedObject__s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj, const t_float *row, const uint row_index) {
  assert(obj);
  assert(obj->matrixOf_data);
  assert(obj->cnt_rows != 0);
  assert(obj->cnt_rows != UINT_MAX);
  assert(obj->cnt_rows >= row_index);
  assert(row);

  //! COpy the data:
  memcpy(obj->matrixOf_data[row_index], row, sizeof(t_float)*obj->cnt_rows);
}

//! Get an usnafe 'copy' of the result-data
t_float *get_unsafeRow__s_dataStruct_matrix_dense(const s_dataStruct_matrix_dense_t *obj, const uint row_index) {
  assert(obj);
  assert(obj->matrixOf_data);
  assert(obj->cnt_rows != 0);
  assert(obj->cnt_rows != UINT_MAX);
  assert(obj->cnt_rows >= row_index);
  
  return obj->matrixOf_data[row_index];
}
//! Get a 'copy' of the result-data
//const t_float *get_row__s_dataStruct_matrix_dense(const s_dataStruct_matrix_dense_t *obj, const uint row_index) {
 t_float *get_row__s_dataStruct_matrix_dense( s_dataStruct_matrix_dense_t *obj,  uint row_index) {
  assert(obj);
  assert(obj->matrixOf_data);
  assert(obj->cnt_rows != 0);
  assert(obj->cnt_rows != UINT_MAX);
  assert(obj->cnt_rows >= row_index);
  
  return obj->matrixOf_data[row_index];
}

//! @return the value assicated to the given cell.
const t_float get_cell__s_dataStruct_matrix_dense(const s_dataStruct_matrix_dense_t *obj, const uint x, const uint y) {
  assert(obj); 

  /* printf("at %s:%d\n", __FILE__, __LINE__); */
  /* printf("(x, y)=(%u, %u), at %s:%d\n", x, y, __FILE__, __LINE__); */
  /* printf("(x, y)=(%u, %u), obj=%p, at %s:%d\n", x, y, obj, __FILE__, __LINE__); */
  /* printf("(x, y)=(%u, %u), cnt_rows=%u, at %s:%d\n", x, y, obj->cnt_rows, __FILE__, __LINE__); */


  assert(obj->matrixOf_data);
  assert(obj->cnt_rows != 0);
  assert(obj->cnt_rows != UINT_MAX);
  assert(obj->cnt_rows >= x);
  assert(obj->cnt_columns != 0);
  assert(obj->cnt_columns != UINT_MAX);
  assert(obj->cnt_columns >= y);

  // printf("at %s:%d\n", __FILE__, __LINE__);

  // printf("cnt_rows=%u, cnt_columns=%u, get for (x, y)=(%u, %u), at [%s]:%s:%d\n", obj->cnt_rows, obj->cnt_columns, x, y, __FUNCTION__, __FILE__, __LINE__);

  //! @return the result:
  return obj->matrixOf_data[x][y];
}

/**
   @brief collect the results into a 1d-array, a 1d arrya expected to represent a 2d-martix.
   @param <self> is the object which hold the results.
   @param <arrOf_2d_result> is a 2d-array to store the resutls
   @param <arrOf_2d_result_size>  which is used as a 'cotnrol', ie, to valdiate that "arrOf_2d_result" has the expected size.
   @remarks function is (among others) used in our Matlab-wrapper of "mine_mexMatrix.c" to simplify/standize data-ccess of resutls.
 **/
void collect_scoresInto_1dArray__s_dataStruct_matrix_dense(const s_dataStruct_matrix_dense_t *self, t_float *arrOf_2d_result, const uint arrOf_2d_result_size) {
  assert(self); 

  assert(self->matrixOf_data);
  assert(arrOf_2d_result);

  const uint cnt_total = (self->cnt_rows * self->cnt_columns);
  assert(cnt_total == arrOf_2d_result_size);


  for(uint row_id = 0; row_id < self->cnt_rows; row_id++) {
    for(uint col_id = 0; col_id < self->cnt_columns; col_id++) {
      const t_float score = self->matrixOf_data[row_id][col_id];
      const uint nrows = self->cnt_rows;
      arrOf_2d_result[nrows*row_id + col_id] = score;
    }
  }
}

/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
   @param <word_max_length> which if set iodentifies the max-word-length in the data: if word_max_length is greater than zero we allocate the "nameOf_columns" and "nameOf_rows" object-attributes.
 **/
void init_listOf_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t **arrOf_1d, const uint list_size, const uint cnt_rows, const uint cnt_columns, const uint word_max_length) {
  assert(list_size > 0);
  s_dataStruct_matrix_dense_t obj_default_value; init_s_dataStruct_matrix_dense_t_setToEmpty(&obj_default_value); //! ie, to be used in ["elow]:
  //! Allcoat ethe object using a gneeirc approach:
  s_dataStruct_matrix_dense_t *tmp = alloc_generic_type_1d(/*type=*/s_dataStruct_matrix_dense_t, tmp, list_size, /*default_value=*/obj_default_value);
  //! Iterate through the objects:
  for(uint i = 0; i < list_size; i++) {
    init_s_dataStruct_matrix_dense_t(&tmp[i], cnt_rows, cnt_columns, word_max_length);
  }
  *arrOf_1d = tmp; //! ie, update the result-contianer.
  /* //! @return the new-allcoated list: */
  /* return tmp; */
}
//! De-allocates a list of "s_dataStruct_matrix_dense_t" objects.
void free_listOf_s_dataStruct_matrix_dense_t(const uint list_size, s_dataStruct_matrix_dense_t *arrOf_1d) {
  assert(list_size); assert(arrOf_1d);  
  for(uint i = 0; i < list_size; i++) {
    free_s_dataStruct_matrix_dense_t(&(arrOf_1d[i]));
  }
  //printf("--- de-allocates, at %s:%d\n", __FILE__, __LINE__);
  free_generic_type_1d(arrOf_1d);
  arrOf_1d = NULL;
}

//! Build a sample-data-set using random values.
void constructSample_data_random(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns) {
  assert(obj); 
  uint row_startPos = 0; uint column_startPos = 0;
  if(is_setTo_empty(obj) == true) {
    if(obj->__allocated_stringOf_defaultRowPrefix != NULL) {free_1d_list_char(&(obj->__allocated_stringOf_defaultRowPrefix));}    
    //! Allcoate the object:
    //printf("at %s:%d\n", __FILE__, __LINE__);
    assert(cnt_rows > 0);
    assert(cnt_columns > 0);
    init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /*word_max_length=*/0);
  } else { //! then we assume that we are to append elements to the alreayd inserted set of data:
    assert(obj->cnt_rows != UINT_MAX); 
    assert(obj->cnt_rows != 0);     assert(obj->cnt_columns != 0);
    assert(obj->cnt_rows >= cnt_rows); //! ie, as we then assume that we are to use 'funciton parameter-argument' "cnt_rows" as a 'start-point' wrt. construciton.
    assert(obj->cnt_columns >= cnt_columns); //! ie, as we assuem a s'quared matrix', ie, as we 'a tthe time of writing' does not see the need to extned the matrix with new columns (ie, ivne the large numbe rof possible compaitnsio/interptiaotn wr.t such an option) (oesketh, 06. aug. 2016).
    row_startPos = cnt_rows;
    __set_default_rowNames(obj, row_startPos, obj->cnt_rows, obj->stringOf_defaultRowPrefix); //! ie, update the set of row-headers.
    column_startPos = cnt_columns;    __set_default_columnNames(obj, column_startPos, obj->cnt_columns, obj->stringOf_defaultRowPrefix); //! ie, update the set of column-headers.
  }
  //! Iterate through the data:
  unsigned int initseed = 100;  srand(initseed);
  //printf("row_startPos=%u, cnt_rows=%u, at %s:%d\n", row_startPos, obj->cnt_rows, __FILE__, __LINE__);
  for(uint i = row_startPos; i < obj->cnt_rows; i++) {
    assert(obj->matrixOf_data[i]);
    //! Specify the scores:
    for(uint y = column_startPos; y < obj->cnt_columns; y++) {
      t_float score = (t_float)rand();
      if(score == T_FLOAT_MAX) {score = 0;} //! ie, as we then assuem that  '0' implies 'emptyness' <-- TDOO[JC]: may you JC valdiate correcteness/implcaitosn wrt. this asusmption-strategy? (oekseth, 06. des. 2016).
      obj->matrixOf_data[i][y] = (score != 0) ? score/(t_float)initseed : 0; //! ie, 'normalize' the sseds
    }
  }
}

//! Build a sample-data-set using uniform values, valeus which are gneerated using the algorithm implemneted in the "cluster.c" library.
void constructSample_data_uniform(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns) {
  assert(obj); 
  //assert(is_setTo_empty(obj));
  uint row_startPos = 0; uint column_startPos = 0;
  // printf("at %s:%d\n", __FILE__, __LINE__);
  if(is_setTo_empty(obj) == true) {
    if(obj->__allocated_stringOf_defaultRowPrefix != NULL) {free_1d_list_char(&(obj->__allocated_stringOf_defaultRowPrefix));}    
    //! Allcoate the object:
    // printf("(init-set) w/dimes=[%u, %u], at %s:%d\n", cnt_rows, cnt_columns, __FILE__, __LINE__);
    init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /*word_max_length=*/0);
  } else { //! then we assume that we are to append elements to the alreayd inserted set of data:
    assert(obj->cnt_rows != 0);     assert(obj->cnt_columns != 0);
    assert(obj->cnt_rows >= cnt_rows); //! ie, as we then assume that we are to use 'funciton parameter-argument' "cnt_rows" as a 'start-point' wrt. construciton.
    assert(obj->cnt_columns >= cnt_columns); //! ie, as we assuem a s'quared matrix', ie, as we 'a tthe time of writing' does not see the need to extned the matrix with new columns (ie, ivne the large numbe rof possible compaitnsio/interptiaotn wr.t such an option) (oesketh, 06. aug. 2016).
    row_startPos = cnt_rows;
    // printf("row_startPos=%u, cnt_rows=%u, stringOf_defaultRowPrefix=\"%s\", at %s:%d\n", row_startPos, obj->cnt_rows, obj->stringOf_defaultRowPrefix,  __FILE__, __LINE__);
    __set_default_rowNames(obj, row_startPos, obj->cnt_rows, obj->stringOf_defaultRowPrefix); //! ie, update the set of row-headers.
    column_startPos = cnt_columns;  __set_default_columnNames(obj, column_startPos, obj->cnt_columns, obj->stringOf_defaultRowPrefix); //! ie, update the set of column-headers.
    // __set_default_columnNames(obj, column_startPos, obj->cnt_columns, obj->stringOf_defaultRowPrefix); //! ie, update the set of row-headers.
  }
  /* //! Allcoate the object: */

  /* init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /\*word_max_length=*\/0); */
  //! Iterate through the data:

  // printf("row_startPos=%u, cnt_rows=%u, at %s:%d\n", row_startPos, obj->cnt_rows, __FILE__, __LINE__);
  //printf("row_startPos=%u, cnt_rows=%u, at %s:%d\n", row_startPos, obj->cnt_rows, __FILE__, __LINE__);
  for(uint i = row_startPos; i < obj->cnt_rows; i++) {
    //! Specify the scores:
    assert(obj->matrixOf_data[i]);
    for(uint y = column_startPos; y < obj->cnt_columns; y++) {
      const t_float score = (t_float)math_generateDistribution__uniform(); //! which is defined in "cluster.c"
      obj->matrixOf_data[i][y] = score;
    }
  }
}
/**
   @brief build a sample-data-set using bionomial values,
   // @param <config_cnt_trials> is the max-value to be used: is the number of trials to be used in comptuation/'drawing' of a number from the bionomial distribution.
   @param <config_probability> the likelyhood/probality of a single event: expected to be in range [0, 0.5].
   @remakrs the values are generated using the algorithm implemneted in the "cluster.c" library.
**/
void constructSample_data_bionmial(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const t_float config_probability) {
//void constructSample_data_bionmial(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const uint config_cnt_trials, const t_float config_probability) {
  if( (config_probability <= 0) || (config_probability > 0.5) ) {
    fprintf(stderr, "(criticla-error)\t you have requested the comptuation of the bionomial distribution for probability=%f, a value which does not confrom to our expectations, ie, please investgiate your call: for quesitons please cotnact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", (float)config_probability, __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  assert(obj); 
  //assert(is_setTo_empty(obj));
  uint row_startPos = 0; uint column_startPos = 0;
  //printf("at %s:%d\n", __FILE__, __LINE__);
  if(is_setTo_empty(obj) == true) {
    if(obj->__allocated_stringOf_defaultRowPrefix != NULL) {free_1d_list_char(&(obj->__allocated_stringOf_defaultRowPrefix));}    
    //! Allcoate the object:
    //printf("at %s:%d\n", __FILE__, __LINE__);
    init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /*word_max_length=*/0);
  } else { //! then we assume that we are to append elements to the alreayd inserted set of data:
    assert(obj->cnt_rows != 0);     assert(obj->cnt_columns != 0);
    assert(obj->cnt_rows >= cnt_rows); //! ie, as we then assume that we are to use 'funciton parameter-argument' "cnt_rows" as a 'start-point' wrt. construciton.
    assert(obj->cnt_columns >= cnt_columns); //! ie, as we assuem a s'quared matrix', ie, as we 'a tthe time of writing' does not see the need to extned the matrix with new columns (ie, ivne the large numbe rof possible compaitnsio/interptiaotn wr.t such an option) (oesketh, 06. aug. 2016).
    row_startPos = cnt_rows;
    __set_default_rowNames(obj, row_startPos, obj->cnt_rows, obj->stringOf_defaultRowPrefix); //! ie, update the set of row-headers.
    column_startPos = cnt_columns;  __set_default_columnNames(obj, column_startPos, obj->cnt_columns, obj->stringOf_defaultRowPrefix); //! ie, update the set of column-headers.
    // __set_default_columnNames(obj, column_startPos, obj->cnt_columns, obj->stringOf_defaultRowPrefix); //! ie, update the set of row-headers.
  }
  /* //! Allcoate the object: */
  /* init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /\*word_max_length=*\/0); */
  //! Iterate through the data:
  for(uint i = row_startPos; i < obj->cnt_rows; i++) {
    // printf("%u/%u, and cols=[%u...%u], at %s:%d\n", i, obj->cnt_rows, column_startPos, obj->cnt_columns, __FILE__, __LINE__);
    //printf("n=%u, p=%f, at %s:%d\n", config_cnt_trials, config_probability, __FILE__, __LINE__);
    //! Specify the scores:
    for(uint y = column_startPos; y < obj->cnt_columns; y++) {
      //printf("get.score, at %s:%d\n", __FILE__, __LINE__);
      //const t_float score = (t_float)math_generateDistribution__uniform();
      const int config_cnt_trials = (int)obj->cnt_columns; //! ie, our default assumption.
      //printf("n=%u, p=%f, at %s:%d\n", config_cnt_trials, config_probability, __FILE__, __LINE__);
      obj->matrixOf_data[i][y] = math_generateDistribution__binomial(config_cnt_trials, config_probability); //! which is defined in "cluster.c"
      {
	t_float score = obj->matrixOf_data[i][y];
	if(score == T_FLOAT_MAX) {score = 0;} //! ie, as we then assuem that  '0' implies 'emptyness' <-- TDOO[JC]: may you JC valdiate correcteness/implcaitosn wrt. this asusmption-strategy? (oekseth, 06. des. 2016).
	if(score != 0) {
	  obj->matrixOf_data[i][y] = 1.0/obj->matrixOf_data[i][y]; //! ei, to 'normalize', making it simpler to visually (eg, in the same line-plot) compare 'this' with "uniform(..)" and "random(..)" genrated numbers.
	}
      }
    }
  }
  //printf("at %s:%d\n", __FILE__, __LINE__);
}

//! Compute the function-scores (oekseth, 06. arp. 2017).
void static__constructSample_data_by_function__s_dataStruct_matrix_dense_t(t_float **matrix, const uint nrows, const uint row_startPos, const uint ncols, const uint column_startPos, const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function) {
  const t_float PI = 3.141592; //! ie, by memory (oesketh, 06. july 2016).

  /* //! Allcoate the object: */
  /* init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /\*word_max_length=*\/0); */
  uint i = row_startPos;
  //! Iterate through the data:
  for(; i < nrows; i++) {
    assert(matrix[i]);
    //! Specify the scores:
    for(uint y = column_startPos; y < ncols; y++) {
      if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_flat) {
	matrix[i][y] = i+1; //! ie, a constant fixed value which is 'fixed' for each line, ie, a 'flat line'.
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_equal) {
	matrix[i][y] = 2*y + 5; //! ie, a linear increase which is the same for each feature 'across' mulitiple/different lines
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_b) {
	matrix[i][y] = /*a=*/2*y + /*b=*/i; //! ie, similar to [above] wrt. coff. "b": as 'b' is fixed for each line-legend while "i" is the same amoong/in different lines, the set of lines will be parallel (to each other);
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_a) {
	matrix[i][y] = /*a=*/(i+0.5)*y + /*b=*/5; //! ie, similar to [above] wrt. coff. "a": the lines will have different slopes, ie, 'behave' differently.
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus) {
	matrix[i][y] = sin(i*10 * PI) + y;
      /* } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear) { */
      /* 	if(i == 0) { */
      /* 	  obj->matrixOf_data[i][y] = (t_float) y / (t_float) (obj->cnt_columns - 1); */
      /* 	} else { */
      /* 	  obj->matrixOf_data[i][y] = sin(i*10 * PI * obj->matrixOf_data[0][y]) + obj->matrixOf_data[0][y] + 0.1*y; */
      /* 	} */
      } else {
	assert(false); //! ie, we then need to add support for this case.
	const t_float score = (t_float)rand();
	matrix[i][y] = score;
      }
    }
  }
}

//! Build a sample-data-set using different functions 'to construct the data'.
void constructSample_data_by_function(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function) {
  assert(obj); 
  //assert(is_setTo_empty(obj)); 
  assert(typeOf_function != s_dataStruct_matrix_dense_typeOf_sampleFunction_undef);
  uint row_startPos = 0; uint column_startPos = 0;
  //printf("at %s:%d\n", __FILE__, __LINE__);
  if(is_setTo_empty(obj) == true) {
    if(obj->__allocated_stringOf_defaultRowPrefix != NULL) {free_1d_list_char(&(obj->__allocated_stringOf_defaultRowPrefix));}    
    //! Allcoate the object:
    //printf("at %s:%d\n", __FILE__, __LINE__);
    init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /*word_max_length=*/0);
  } else { //! then we assume that we are to append elements to the alreayd inserted set of data:
    assert(obj->cnt_rows != 0);     assert(obj->cnt_columns != 0);
    assert(obj->cnt_rows >= cnt_rows); //! ie, as we then assume that we are to use 'funciton parameter-argument' "cnt_rows" as a 'start-point' wrt. construciton.
    assert(obj->cnt_columns >= cnt_columns); //! ie, as we assuem a s'quared matrix', ie, as we 'a tthe time of writing' does not see the need to extned the matrix with new columns (ie, ivne the large numbe rof possible compaitnsio/interptiaotn wr.t such an option) (oesketh, 06. aug. 2016).
    row_startPos = cnt_rows;
    __set_default_rowNames(obj, row_startPos, obj->cnt_rows, obj->stringOf_defaultRowPrefix); //! ie, update the set of row-headers.
    column_startPos = cnt_columns;  __set_default_columnNames(obj, column_startPos, obj->cnt_columns, obj->stringOf_defaultRowPrefix); //! ie, update the set of column-headers.
    // __set_default_columnNames(obj, column_startPos, obj->cnt_columns, obj->stringOf_defaultRowPrefix); //! ie, update the set of row-headers.
  }
  //assert(cnt_rows > 1); 
  assert(cnt_columns > 1); //! ie, as otherwise this call is pointless.
  //! Apply the lgocis:
  static__constructSample_data_by_function__s_dataStruct_matrix_dense_t(obj->matrixOf_data, obj->cnt_rows, row_startPos, obj->cnt_columns, column_startPos, typeOf_function);
}

//! Build a sample-data-set using different functions 'to construct the data'.
void static__constructSample_data_by_function_noise(t_float **matrix, const uint nrows, const uint row_startPos, const uint ncols, const uint column_startPos, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_noise, const t_float y_adjustment_index) {
  const t_float PI = 3.141592; //! ie, by memory (oesketh, 06. july 2016).


  //! Note: ["elow] validtes are expected to conform to the "arrOf_noiseConfig" in our "config_mine.js"
  const t_float y_adjustmentMult = 0.1; //! which is defined in our "config_mine.js"
  assert(s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_undef == 3); //! ie, what we expect in ["elow].
  t_float swap_value = -0.05; t_float step_value = 0.005; 
  if(typeOf_noise == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_low) {
    //! then we do noting
  } else if(typeOf_noise == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_medium) {
    swap_value = -0.15; step_value = 0.005; 
  } else if(typeOf_noise == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_large) {
    swap_value = -0.25; step_value = 0.005; 
  } else {
    assert(false); // ie, as we then need to add support for this case.
  }

  /* //! Allcoate the object: */
  /* init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /\*word_max_length=*\/0); */
  uint i = row_startPos;
  //! Iterate through the data:
  for(; i < nrows; i++) {
    assert(matrix[i]);
    //! Specify the scores:
    for(uint x = column_startPos; x < ncols; x++) {
      //!
      //! Then 'copy-paste' functions from our "config_mine.js" Java-script file:
      //!
      //! First make use of the 'offset' defined in our "config_mine.js":
      const t_float y_mult = x*y_adjustmentMult + swap_value; swap_value = swap_value*(-1.0)+step_value;

      //!
      //! Idnetify the funciton r'eqqested' by the user:
      //! Note: though ["elow] 'inline-if-setences' is epxted to be an overhead wrt. exueciton-time, we assume that 'the tiem to cosntruct a satmpel-adat-aset' is insignfcant (when compared to the overlal exueciton-time of the Mine-algorithm-impelmetantion):
      const t_float y_local = ( (y_adjustment_index != T_FLOAT_MAX) && (y_adjustment_index != 0) ) ? y_adjustment_index : (t_float)i;
      if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax) { 
	if(y_local == 0.0) { matrix[i][x] = x;}
	else if(y_local == 1.0) { matrix[i][x] = -0.2 + y_mult*3;}
	else { matrix[i][x] = -0.5 + y_mult*(y_adjustment_index-1);}
	//! ---------------------------------------------------------------------------------------------------------
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_curved) { 
	if(y_local == 0) { matrix[i][x] = 0.2*sin(y_mult*(2.8+0.1));}
	else if(y_local == 1) { matrix[i][x] = sin(y_mult*(2.8-0.9));}
	else { matrix[i][x] = sin(y_mult*(2.8-1.2))*y_local;}
	//! ---------------------------------------------------------------------------------------------------------
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_and_axx) { 
	if(y_local == 0) { matrix[i][x] = 0.8 + y_mult*y_mult*.9;}
	else if(y_local == 1) { matrix[i][x] = sin(y_mult*(2.8-0.9));}
	else { matrix[i][x] = sin(y_mult*(2.8-1.2))*2;}
	//! ---------------------------------------------------------------------------------------------------------
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_inverse_x) { 
	if(y_local == 0) { matrix[i][x] = -6 + y_mult*y_mult*2.9;}
	else if(y_local == 1) { matrix[i][x] = -0.2 - y_mult*3;}
	else { matrix[i][x] = /*val3=*/-4 + y_mult*y_local;}
	//! ---------------------------------------------------------------------------------------------------------
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_circle) { 
	if(y_local == 0) { matrix[i][x] = /*val1=*/1.4*cos(y_mult*(2.8+0.1));}
	else if(y_local == 1) { matrix[i][x] = /*val2=*/1*sin(y_mult*(2.8-1.4));}
	else { matrix[i][x] = /*val3=*/(1.02*i)*sin(y_mult*(2.8-1.04*y_local));}
	//! ---------------------------------------------------------------------------------------------------------
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid) { 
	if(y_local == 0) { matrix[i][x] = /*val1=*/1.0*cos(y_mult*(2.8+3.2));}
	else if(y_local == 1) { matrix[i][x] = /*val2=*/1*cos(y_mult*(2.8+3.8));}
	else { matrix[i][x] = /*val3=*/(1.02*i)*cos(y_mult*(2.8+4.02*y_local));}
	//! ---------------------------------------------------------------------------------------------------------
      } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid_curved) { 
	if(y_local == 0) { matrix[i][x] = /*val1=*/1.0*cos(y_mult*(2.8+3.2));}
	else if(y_local == 1) { matrix[i][x] = /*val2=*/1*sin(y_mult*(2.8-1.4));}
	else { matrix[i][x] = /*val3=*/i*sin(y_mult*(2.8-1.09*y_local));}
      /* 	//! --------------------------------------------------------------------------------------------------------- */
      /* } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_) {  */
      /* 	if(i == 0) { matrix[i][x] = ;} */
      /* 	else if(i == 1) { matrix[i][x] = ;} */
      /* 	else { matrix[i][x] = ;} */

      /* 	obj->matrixOf_data[i][y] = i; //! ie, a constant fixed value which is 'fixed' for each line, ie, a 'flat line'. */
      /* } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_equal) { */
      /* 	obj->matrixOf_data[i][y] = 2*y + 5; //! ie, a linear increase which is the same for each feature 'across' mulitiple/different lines */
      /* } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_b) { */
      /* 	obj->matrixOf_data[i][y] = /\*a=*\/2*y + /\*b=*\/i; //! ie, similar to [above] wrt. coff. "b": as 'b' is fixed for each line-legend while "i" is the same amoong/in different lines, the set of lines will be parallel (to each other); */
      /* } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_a) { */
      /* 	obj->matrixOf_data[i][y] = /\*a=*\/(i+0.5)*y + /\*b=*\/5; //! ie, similar to [above] wrt. coff. "a": the lines will have different slopes, ie, 'behave' differently. */
      /* } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus) { */
      /* 	obj->matrixOf_data[i][y] = sin(i*10 * PI) + y; */
      /* /\* } else if(typeOf_function == s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear) { *\/ */
      /* /\* 	if(i == 0) { *\/ */
      /* /\* 	  obj->matrixOf_data[i][y] = (t_float) y / (t_float) (obj->cnt_columns - 1); *\/ */
      /* /\* 	} else { *\/ */
      /* /\* 	  obj->matrixOf_data[i][y] = sin(i*10 * PI * obj->matrixOf_data[0][y]) + obj->matrixOf_data[0][y] + 0.1*y; *\/ */
      /* /\* 	} *\/ */
      } else {
	assert(false); //! ie, we then need to add support for this case.
	const t_float score = (t_float)rand();
	matrix[i][x] = score;
      }
    }
  }  
  
  /* const t_float PI = 3.141592; //! ie, by memory (oesketh, 06. july 2016). */
  
  
  /* /\* //! Allcoate the object: *\/ */
  /* /\* init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /\\*word_max_length=*\\/0); *\/ */



  /* //! Iterate through the data:   */
  /* for(uint i = row_startPos; i < obj->cnt_rows; i++) { */
  /*   //! Specify the scores: */
  /*   for(uint x = column_startPos; x < obj->cnt_columns; x++) { */

  /*   } */
  /* } */
}

//! Build a sample-data-set using different functions 'to construct the data'.
void constructSample_data_by_function_noise(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_noise) {
  assert(obj); 
  //assert(is_setTo_empty(obj)); 
  assert(typeOf_function != s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_undef);
  //assert(cnt_rows > 1); 
  uint row_startPos = 0; uint column_startPos = 0;
  // printf("at %s:%d\n", __FILE__, __LINE__);
  if(is_setTo_empty(obj) == true) {
    if(obj->__allocated_stringOf_defaultRowPrefix != NULL) {free_1d_list_char(&(obj->__allocated_stringOf_defaultRowPrefix));}    
    //! Allcoate the object:
    init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(obj, cnt_rows, cnt_columns); //, /*word_max_length=*/0);
  } else { //! then we assume that we are to append elements to the alreayd inserted set of data:
    assert(obj->cnt_rows != 0);     assert(obj->cnt_columns != 0);
    assert(obj->cnt_rows >= cnt_rows); //! ie, as we then assume that we are to use 'funciton parameter-argument' "cnt_rows" as a 'start-point' wrt. construciton.
    assert(obj->cnt_columns >= cnt_columns); //! ie, as we assuem a s'quared matrix', ie, as we 'a tthe time of writing' does not see the need to extned the matrix with new columns (ie, ivne the large numbe rof possible compaitnsio/interptiaotn wr.t such an option) (oesketh, 06. aug. 2016).
    row_startPos = cnt_rows;
    __set_default_rowNames(obj, row_startPos, obj->cnt_rows, obj->stringOf_defaultRowPrefix); //! ie, update the set of row-headers.
    column_startPos = cnt_columns;  
    // printf("column-range=[%u, %u], at %s:%d\n", column_startPos, obj->cnt_columns, __FILE__, __LINE__);
    assert(obj->nameOf_columns);
    assert(obj->stringOf_defaultRowPrefix);
    assert(strlen(obj->stringOf_defaultRowPrefix) < obj->_word_max_length);
    __set_default_columnNames(obj, column_startPos, obj->cnt_columns, obj->stringOf_defaultRowPrefix); //! ie, update the set of column-headers.
  }
  assert(obj->cnt_columns > 1); //! ie, as otherwise this call is pointless.
  static__constructSample_data_by_function_noise(obj->matrixOf_data, obj->cnt_rows, row_startPos, obj->cnt_columns, column_startPos, typeOf_function, typeOf_noise, T_FLOAT_MAX);

}

/**
   @brief construct a sampel-date-set in value-ranges identified by stringOf_sampleData_type param.
   @param <data_obj> the object to hold the new-cosntructed data-set.
   @param <cnt_rows> the number of rows to be cosnturcted/inferred.
   @param <cnt_columns> the number of features/columns to be cosnturcted/inferred.
   @param <stringOf_sampleData_type> identified the type of funciton (eg, 'random' or 'uniform') for which we are to infer value-ranges.
   @return true on success.
 **/
bool constructSample_data_identifiedFromString(s_dataStruct_matrix_dense_t *data_obj, const uint _cnt_rows, uint cnt_columns, const char *stringOf_sampleData_type) {
  assert(data_obj);
  assert(_cnt_rows != UINT_MAX);
  //  printf("_cnt_rows=%u, cnt_columns=%u, at %s:%d\n", _cnt_rows, cnt_columns, __FILE__, __LINE__);

  if(stringOf_sampleData_type != NULL) {
    data_obj->stringOf_defaultRowPrefix = stringOf_sampleData_type; //! use the string-type as an 'idnetifier' wrt. the rows.
    if(strlen(stringOf_sampleData_type) > data_obj->_word_max_length)  {
      //! Then we need to allocate a sub-string, ie, to avoid emmory-corrutpion.
      char defulat_value = '\0';
      data_obj->_word_max_length = strlen(stringOf_sampleData_type) + 100;
      assert(data_obj->_word_max_length > 0);
      data_obj->__allocated_stringOf_defaultRowPrefix = allocate_1d_list_char(data_obj->_word_max_length, defulat_value);
      strncpy(data_obj->__allocated_stringOf_defaultRowPrefix, stringOf_sampleData_type, data_obj->_word_max_length);
      data_obj->stringOf_defaultRowPrefix = data_obj->__allocated_stringOf_defaultRowPrefix;
      //  printf("\n\nstringOf_defaultRowPrefix is udpated, at %s:%d\n", __FILE__, __LINE__);
    }

    // printf("stringOf_sampleData_type=\"%s\", at %s:%d\n", data_obj->stringOf_defaultRowPrefix, __FILE__, __LINE__);
  }

  s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_undef;
  const uint cnt_rows  = (_cnt_rows != 0) ? _cnt_rows : 2;
  bool use_uniform = false;  bool use_binomial = false; t_float config_binomial_probaility = T_FLOAT_MAX;
  bool isComputed = false;
  if(stringOf_sampleData_type && strlen(stringOf_sampleData_type)) { //! Then we try to dientify the data-type requested by the user:
    if(strstr(stringOf_sampleData_type, "lines-")) {
      enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax;
      //! then we evaluate wrt. the "s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise" enum:
      if(strstr(stringOf_sampleData_type, "lines-different-ax")) {
	typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax;
      } else if(strstr(stringOf_sampleData_type, "lines-curved")) {
	typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_curved;
      } else if(strstr(stringOf_sampleData_type, "lines-different-ax-and-axx")) {
	typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_and_axx;
      } else if(strstr(stringOf_sampleData_type, "lines-ax-inverse-x")) {
	typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_inverse_x;
      } else if(strstr(stringOf_sampleData_type, "lines-circle")) {
	typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_circle;
      } else if(strstr(stringOf_sampleData_type, "lines-sinsoid")) {
	typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid;
      } else if(strstr(stringOf_sampleData_type, "lines-sinsoid-curved")) {
	typeOf_data =s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid_curved ;
      } else {
	assert(false); // then we need to add support for this case.	
      }

      //!
      //! Then identify the noise-level:
      enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel typeOf_noiseLevel = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_low;
      if(strstr(stringOf_sampleData_type, "-medium")) {
	typeOf_noiseLevel = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_medium;
      } else if(strstr(stringOf_sampleData_type, "-large")) {
	typeOf_noiseLevel = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_large;	
      }

      //!
      //! Then comptue the sample-data:
      //! Build a sample-data-set using different functions 'to construct the data'.
      constructSample_data_by_function_noise(data_obj, cnt_rows, /*cnt_columns=*/cnt_columns, typeOf_data, typeOf_noiseLevel);
      //constructSample_data_by_function_noise(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, uint cnt_columns, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_noise)

      //! Make it epxlcit that we at this exeuction-point has 'cosntructed the data-set':
      isComputed = true; //! ie, to aovid duplicate efforts.

    } else if(strstr(stringOf_sampleData_type, "random")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_undef; //! ie, the 'default' alternative.
    } else if(strstr(stringOf_sampleData_type, "uniform")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_undef; //! ie, the 'default' alternative.
      use_uniform = true;
    } else if(strstr(stringOf_sampleData_type, "binomial_p05")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_undef; //! ie, the 'default' alternative.
      use_binomial = true; config_binomial_probaility =.5;
    } else if(strstr(stringOf_sampleData_type, "binomial_p010")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_undef; //! ie, the 'default' alternative.
      use_binomial = true; config_binomial_probaility = .1;
    } else if(strstr(stringOf_sampleData_type, "binomial_p005")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_undef; //! ie, the 'default' alternative.
      use_binomial = true; config_binomial_probaility = .05;
    } else if(strstr(stringOf_sampleData_type, "flat")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_flat;
    } else if(strstr(stringOf_sampleData_type, "linear-equal")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_equal;
    } else if(strstr(stringOf_sampleData_type, "linear-differentCoeff-b")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_b;
    } else if(strstr(stringOf_sampleData_type, "linear-differentCoeff-a")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_a;
    } else if(strstr(stringOf_sampleData_type, "sinus")) {
      typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus;
      /* } else if(strstr(stringOf_sampleData_type, "sinus-and-linear")) { */
      /* 	typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear; */
      /* } else if(strstr(stringOf_sampleData_type, "")) { */
      /* 	typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_; */
    } else {
      if(strchr(stringOf_sampleData_type, '/') == NULL) {
	fprintf(stderr, "!!\t Assumes your input-tag is Not a file-name: Was not able to correctly interpret your input-option \"%s\" wrt. syntetic-file-format, ie, please investigate. To smootth out the work, we will now continoue (in optmized mode) using a random-generated input-dataset. For questions please contact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_sampleData_type, __FUNCTION__, __FILE__, __LINE__);
      } //! else we asusemt eh latter is a file-name, ie do Not pritn a warnign (oekseth, 06. arp. 2017)
    }
  }


  if(isComputed == false) {
    if(typeOf_function != s_dataStruct_matrix_dense_typeOf_sampleFunction_undef) {
      //! Then construct the input-data using a pre-defined function:
      //printf("compute for typeOf_function=%d, at %s:%d\n", typeOf_function, __FILE__, __LINE__);
      constructSample_data_by_function(data_obj, cnt_rows, /*cnt_columns=*/cnt_columns, typeOf_function);
    } else { 
      //! Then cosntruct randomzied data usign our dense matrix-structurure:
      //! Build the two vectors, ie, construct data-sets:
      if(use_uniform) {
	//printf("----------- at %s:%d\n", __FILE__, __LINE__);
	constructSample_data_uniform(data_obj, cnt_rows, /*cnt_columns=*/cnt_columns);
	assert(data_obj->matrixOf_data);
      } else if(use_binomial) {
	//printf("at %s:%d\n", __FILE__, __LINE__);
	assert(config_binomial_probaility != T_FLOAT_MAX); //! ie, what we expect.
	constructSample_data_bionmial(data_obj, cnt_rows, /*cnt_columns=*/cnt_columns, config_binomial_probaility);
      } else {
	//printf("stringOf_sampleData_type=\"%s\", at %s:%d\n", stringOf_sampleData_type, __FILE__, __LINE__);
	assert(cnt_rows != UINT_MAX); 
	constructSample_data_random(data_obj, cnt_rows, /*cnt_columns=*/cnt_columns);
      }
    }
  }
  //! At this exeuction-poitn we assume the operation was a succes:
  return true;
}



/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_useJavaScript_syntax> which if set to true implies that we makes use of a 'java-script' wrappers from the values.
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const bool include_stringIdentifers_in_matrix, const char _isTo_writeOut_stringHeaders) {
  //! What we expect:
  assert(obj);   assert(stream);
  assert(obj->matrixOf_data); assert(obj->cnt_columns); assert(obj->cnt_rows);
  
  if(stringOf_header && strlen(stringOf_header)) {
    fprintf(stream, "\n#! %s\n", stringOf_header); //! where "#! " is used to avoid the string from wrongly being parsed as a data-row.
  }
  
  const char seperator_columns = '\t';

  if(_isTo_writeOut_stringHeaders == false) {nameOf_columns = NULL;}
  const bool isTo_writeOut_stringHeaders = true;

  //! Eitehr use the input-arguemnts or the 'implict' name-mappings found in "obj":
  char **nameOf_cols_local = (nameOf_columns != NULL) ? nameOf_columns : obj->nameOf_columns;
  char **nameOf_rows_local = (nameOf_rows != NULL) ? nameOf_rows : obj->nameOf_rows;
  //!
  //! Idnietyf columns to ignore:
  char empty_0 = 1; //! ie, amrk all columsn as of interest.
  char *mapOf_colsToUse = allocate_1d_list_char(obj->cnt_columns, empty_0);
  assert(mapOf_colsToUse);
  uint cntTotal__cols = 0;
  for(uint y = 0; y < obj->cnt_columns; y++) {
    long long int cnt_ofInterest = 0;
    for(uint i = 0; i < obj->cnt_rows; i++) {
      const t_float score = obj->matrixOf_data[i][y];  
      cnt_ofInterest += isOf_interest(score);
    }
    const bool isTo_use = (cnt_ofInterest > 0); //!, where where '0' implies 'false'.
    mapOf_colsToUse[y] = isTo_use;
    cntTotal__cols += isTo_use;
  }


  // printf("## isTo_writeOut_stringHeaders=%d, at %s:%d\n", isTo_writeOut_stringHeaders, __FILE__, __LINE__);

  if(isTo_writeOut_stringHeaders) {
  //if(nameOf_cols_local != NULL) {
    //! Write out the 'ehader', a 'header' expected by our "parse_main.c"
    fprintf(stream, "#! row-name:\t");
    //! Write out the column-rows:
    if(nameOf_cols_local != NULL) {
      uint y_currPos = 0;       bool newLineAdded = false;
      for(uint y = 0; y < obj->cnt_columns; y++) {
	if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
	const char *string_local = nameOf_cols_local[y];
	if(!string_local || !strlen(string_local)) {
	  string_local = "-"; //! ie, an empty-sign (oekseth, 06. des. 2016).
	}
	fprintf(stream, "%s", string_local); 
	//if((y +1) != obj->cnt_columns) {fputc(seperator_columns, stream);} else {fputc('\n', stream);}
	if((y_currPos +1) != cntTotal__cols) {fputc(seperator_columns, stream);} else {fputc('\n', stream); newLineAdded = true;}
	y_currPos++;
      }     
      if(newLineAdded == false) {fputc('\n', stream); newLineAdded = true;}
    } else {
      uint y_currPos = 0;       bool newLineAdded = false;
      for(uint y = 0; y < obj->cnt_columns; y++) {
	if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
	fprintf(stream, "col-%u", y);
	if((y_currPos +1) != cntTotal__cols) {fputc(seperator_columns, stream);} else {fputc('\n', stream); newLineAdded = true;}
	y_currPos++;
      }
      if(newLineAdded == false) {fputc('\n', stream); newLineAdded = true;}
    }
  }

  if(nameOf_rows_local && (include_stringIdentifers_in_matrix == false)) {
    if(isTo_writeOut_stringHeaders) {
      //! Then write out the row-identifers:
      fprintf(stream, "#! legend-id-rows:\t");
      //! Write out the column-rows:
      for(uint y = 0; y < obj->cnt_rows; y++) {
	fprintf(stream, "%s\t", nameOf_rows_local[y]); 
	if((y +1) != obj->cnt_rows) {fputc(seperator_columns, stream);} else {fputc('\n', stream);}
      }
    }
    //! Avoid teh string-variables from being pritned 'as-is' in the matrix:
    nameOf_rows_local = NULL;     nameOf_cols_local = NULL;
  }


  //! Write out the tab-formatted matrix:
  for(uint i = 0; i < obj->cnt_rows; i++) {
    uint cnt_rowIsOf_interst = 0;
    for(uint y = 0; y < obj->cnt_columns; y++) {
      if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
      if(obj->matrixOf_data[i][y] != T_FLOAT_MAX) {cnt_rowIsOf_interst++;}
    }
    if(cnt_rowIsOf_interst == 0) {continue;} //! ie, as the row is then not of interest.
    assert(obj->matrixOf_data[i]);
    if(nameOf_rows_local) { fprintf(stream, "%s\t", nameOf_rows_local[i]); }
    else if(include_stringIdentifers_in_matrix) {fprintf(stream, "row-%u\t", i); }
    //! Write out the scores:
    uint y_currPos = 0;
    for(uint y = 0; y < obj->cnt_columns; y++) {
      if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
      t_float score = obj->matrixOf_data[i][y];
      //if(score == T_FLOAT_MAX) {score = 0;} //! ie, as we then assuem that  '0' implies 'emptyness' <-- TDOO[JC]: may you JC valdiate correcteness/implcaitosn wrt. this asusmption-strategy? (oekseth, 06. des. 2016).
      if(score != T_FLOAT_MAX) {
	fprintf(stream, "%f", score);
      } else {
	fputc('-', stream);
      }
      if((y_currPos +1) != cntTotal__cols) {fputc(seperator_columns, stream);} else {fputc('\n', stream);}
      y_currPos++;
    }
  }
  if(mapOf_colsToUse) {free_1d_list_char(&mapOf_colsToUse); mapOf_colsToUse = NULL;}
}

//! Write out a java-script string to the "stream".
static void __write_outString_to_stream_javaScript(const char *stringOf_value, FILE *stream) {
  assert(stringOf_value);

  putc('\"', stream);
  //assert(strlen(stringOf_value));
  if(strlen(stringOf_value)) {
    const uint cnt_chars = (uint)strlen(stringOf_value); //nameOf_rows_local[y]);
    //! Handle special symbols, ie, encapsulate the latter:
    for(uint k = 0; k < cnt_chars; k++) {
      const char c = stringOf_value[k];
      if( (c == '"') || (c == '\'') || (c == '\\') ) { fputc('\\', stream);} //! ie, then add a "/" prefix to 'encapsulate' the symbol.
      //! Add the symbol:
      putc(c, stream);
    }
  }
  fputc('\"', stream);
}



/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <stringOf_measureId> 
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders) {
  //! What we expect:
  assert(obj);   assert(stream);
  assert(obj->matrixOf_data); assert(obj->cnt_columns); assert(obj->cnt_rows);
  
  /* if(stringOf_header && strlen(stringOf_header)) { */
  /*   fprintf(stream, "\n#! %s\n", stringOf_header); //! where "#! " is used to avoid the string from wrongly being parsed as a data-row. */
  /* } */
  
  const char seperator_columns = '\t';

  //! Eitehr use the input-arguemnts or the 'implict' name-mappings found in "obj":
  char **nameOf_cols_local = (nameOf_columns != NULL) ? nameOf_columns : obj->nameOf_columns;
  char **nameOf_rows_local = (nameOf_rows != NULL) ? nameOf_rows : obj->nameOf_rows;

  if( (nameOf_cols_local != NULL) && isTo_writeOut_stringHeaders) {
    //! then we 'wraps' tghe symbols into Java-script-syntax:
    fprintf(stream, "var setFromCGI_arrOf_columns = [");
    for(uint y = 0; y < obj->cnt_columns; y++) {      
      if(nameOf_cols_local[y] && nameOf_cols_local[y]) {
	//! Write out a java-script string to the "stream".
	__write_outString_to_stream_javaScript(nameOf_cols_local[y], stream);
      } else {
	fprintf(stream, "\"%u\"", y); 
      }
      //! Add seperator in the list:
      if((y +1) != obj->cnt_columns) {fputc(',', stream);fputc(' ', stream);} //else {fputc('\n', stream);}
    }
    //! Finalize:
    fputc(']', stream); fputc(';', stream); fputc('\n', stream);
  }


  if(nameOf_rows_local && (include_stringIdentifers_in_matrix == false) ) {
    if(isTo_writeOut_stringHeaders) {
      fprintf(stream, "var setFromCGI_arrOf_rows = [");
      for(uint y = 0; y < obj->cnt_rows; y++) {
	//assert(nameOf_cols_local[y]);
	const char *stringOf_rowHeader = nameOf_rows_local[y];
	if(stringOf_rowHeader == NULL) {stringOf_rowHeader = "(empty)";}
	//! Write out a java-script string to the "stream".
	__write_outString_to_stream_javaScript(stringOf_rowHeader, stream);
	//! Add seperator in the list:
	if((y +1) != obj->cnt_rows) {fputc(',', stream);fputc(' ', stream);} //else {fputc('\n', stream);}
      }
      //! Finalize:
      fputc(']', stream); fputc(';', stream); fputc('\n', stream);
    }
    //! Avoid teh string-variables from being pritned 'as-is' in the matrix:
    nameOf_rows_local = NULL;     nameOf_cols_local = NULL;
  }


  //! Write out the tab-formatted matrix:
  fputs("var setFromCGI_", stream); 
  assert(stringOf_header);   assert(strlen(stringOf_header));
  fputs(stringOf_header, stream); //! ie, the unique name of the matrix.
  
  //! 'Start' the matrix:
  fputc(' ', stream);   fputc('=', stream);   fputc(' ', stream); //! ie, " = "
  fputc('[', stream);  fputc('\n', stream); 
  
  //! Iterate through the rows
  for(uint i = 0; i < obj->cnt_rows; i++) {
    assert(obj->matrixOf_data[i]);
    //! Write out a java-script string to the "stream".
    if(nameOf_rows_local) {
      __write_outString_to_stream_javaScript(nameOf_rows_local[i], stream);
      //! Then we assume the values are to be 'found' inside a an array-block dientifed by teh 'row-hash-value':
      fputc(':', stream); 
    }
    //! Start this row:
    fputc('[', stream);

    //! Write out the scores:
    for(uint y = 0; y < obj->cnt_columns; y++) {
      t_float score = obj->matrixOf_data[i][y];
      if(score != T_FLOAT_MAX) {
	//if(score == T_FLOAT_MAX) {score = 0;} //! ie, as we then assuem that  '0' implies 'emptyness' <-- TDOO[JC]: may you JC valdiate correcteness/implcaitosn wrt. this asusmption-strategy? (oekseth, 06. des. 2016).
	fprintf(stream, "%f", score);
      } else {fputc('-', stream);}
      //! Add seperator in the list:
      if((y +1) != obj->cnt_columns) {fputc(',', stream);fputc(' ', stream);} //else {fputc('\n', stream);}
    }
      
    //! End this row:
    fputc(']', stream); 
    if((i+1) != obj->cnt_rows) {fputc(',', stream);} //! ie, a spseerator between teh rows.
    fputc('\n', stream);
  }
  //! End this matrix:
  fputc(']', stream); //  fputc(']', stream);  
  fputc(';', stream); fputc('\n', stream);
}
/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <isLast> which if set to true impleis that we 'finalize' the JSON-array.
   @param <isTo_explcitlyInclude_rowAndColumnHeaders> which if set impleis that we explcutly inlcude the row and column-hders: an example-usage is for the case where we cocnate the inptu-matrix with an 'adjsuted' data-sets, eg, where the 'previosuly fucniton-calls' describes a corelation-matrix-set with 50 per-cent threshodls.
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_JSON(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders, const bool isLast, const char *isTo_explcitlyInclude_rowAndColumnHeaders) {
  //! What we expect:
  assert(obj);   assert(stream);
  assert(obj->matrixOf_data); assert(obj->cnt_columns); assert(obj->cnt_rows);
  
  /* if(stringOf_header && strlen(stringOf_header)) { */
  /*   fprintf(stream, "\n#! %s\n", stringOf_header); //! where "#! " is used to avoid the string from wrongly being parsed as a data-row. */
  /* } */
  
  const char seperator_columns = '\t';

  //! Eitehr use the input-arguemnts or the 'implict' name-mappings found in "obj":
  char **nameOf_cols_local = (nameOf_columns != NULL) ? nameOf_columns : obj->nameOf_columns;
  char **nameOf_rows_local = (nameOf_rows != NULL) ? nameOf_rows : obj->nameOf_rows;


  if(isTo_writeOut_stringHeaders) {
    fputc('{', stream); fputc('\n', stream); //! ie, the 'start' of the JSON
  }
  if(
     ((nameOf_cols_local != NULL) && isTo_writeOut_stringHeaders) ||
     ((isTo_explcitlyInclude_rowAndColumnHeaders != NULL) && strlen(isTo_explcitlyInclude_rowAndColumnHeaders) )
     )  {
    //! then we 'wraps' tghe symbols into Java-script-syntax:
    fprintf(stream, "\"%s_arrOf_columns\" : [", (isTo_explcitlyInclude_rowAndColumnHeaders != NULL) ? isTo_explcitlyInclude_rowAndColumnHeaders : "setFromCGI");
    for(uint y = 0; y < obj->cnt_columns; y++) {
      if(nameOf_cols_local) {
	assert(nameOf_cols_local[y]);
	//! Write out a java-script string to the "stream".
	__write_outString_to_stream_javaScript(nameOf_cols_local[y], stream);
      } else { //! then we write out the result using an internal column-wrapper:
	char str_local[1000] = {'\0'}; sprintf(str_local, "col:%u", y);
	__write_outString_to_stream_javaScript(str_local, stream);	
      }
      //! Add seperator in the list:
      if((y +1) != obj->cnt_columns) {fputc(',', stream);fputc(' ', stream);} //else {fputc('\n', stream);}
    }
    //! Finalize:
    fputc(']', stream); fputc(',', stream); fputc('\n', stream);
  }
  if(
     (nameOf_rows_local && (include_stringIdentifers_in_matrix == false)  ) ||
     ((isTo_explcitlyInclude_rowAndColumnHeaders != NULL) && strlen(isTo_explcitlyInclude_rowAndColumnHeaders) )
     ) {
    if(isTo_writeOut_stringHeaders ||
     ((isTo_explcitlyInclude_rowAndColumnHeaders != NULL) && strlen(isTo_explcitlyInclude_rowAndColumnHeaders) )
       ) {
      fprintf(stream, "\"%s_arrOf_rows\" : [", (isTo_explcitlyInclude_rowAndColumnHeaders != NULL) ? isTo_explcitlyInclude_rowAndColumnHeaders : "setFromCGI");
      //fprintf(stream, "\"setFromCGI_arrOf_rows\" : [");
      for(uint y = 0; y < obj->cnt_rows; y++) {
	//assert(nameOf_cols_local[y]);
	if(nameOf_rows_local) {	  
	  const char *stringOf_rowHeader = nameOf_rows_local[y];
	  assert(stringOf_rowHeader);
	  if(stringOf_rowHeader == NULL) {stringOf_rowHeader = "(empty)";}
	  //! Write out a java-script string to the "stream".
	  __write_outString_to_stream_javaScript(stringOf_rowHeader, stream);
	} else { //! then we write out the result using an internal column-wrapper:
	  char str_local[1000] = {'\0'}; sprintf(str_local, "row:%u", y);
	  __write_outString_to_stream_javaScript(str_local, stream);	
	}
	//! Add seperator in the list:
	if((y +1) != obj->cnt_rows) {fputc(',', stream);fputc(' ', stream);} //else {fputc('\n', stream);}
      }
      //! Finalize:
      fputc(']', stream); fputc(',', stream); fputc('\n', stream);
    }
    //! Avoid teh string-variables from being pritned 'as-is' in the matrix:
    nameOf_rows_local = NULL;     nameOf_cols_local = NULL;
  }


  //! Write out the tab-formatted matrix:
  fputs("\"setFromCGI_", stream); 
  assert(stringOf_header);   assert(strlen(stringOf_header));
  fputs(stringOf_header, stream); //! ie, the unique name of the matrix.
  
  //! 'Start' the matrix:
  fputc('"', stream);  fputc(' ', stream);   fputc(':', stream);   fputc(' ', stream); //! ie, " = "
  fputc('[', stream);  fputc('\n', stream); 
  
  //! Iterate through the rows
  for(uint i = 0; i < obj->cnt_rows; i++) {
    assert(obj->matrixOf_data[i]);
    //! Write out a java-script string to the "stream".
    if(nameOf_rows_local) {
      __write_outString_to_stream_javaScript(nameOf_rows_local[i], stream);
      //! Then we assume the values are to be 'found' inside a an array-block dientifed by teh 'row-hash-value':
      fputc(':', stream); 
    }
    //! Start this row:
    fputc('[', stream);

    //! Write out the scores:
    for(uint y = 0; y < obj->cnt_columns; y++) {
      t_float score = obj->matrixOf_data[i][y];
      if(score != T_FLOAT_MAX) {
	//if(score == T_FLOAT_MAX) {score = 0;} //! ie, as we then assuem that  '0' implies 'emptyness' <-- TDOO[JC]: may you JC valdiate correcteness/implcaitosn wrt. this asusmption-strategy? (oekseth, 06. des. 2016).
	fprintf(stream, "%f", score);
      } else {fputc('-', stream);}
      //! Add seperator in the list:
      if((y +1) != obj->cnt_columns) {fputc(',', stream);fputc(' ', stream);} //else {fputc('\n', stream);}
    }
      
    //! End this row:
    fputc(']', stream); 
    if((i+1) != obj->cnt_rows) {fputc(',', stream);} //! ie, a spseerator between teh rows.
    fputc('\n', stream);
  }
  //! End this matrix:  
  fputc(']', stream); //  fputc(']', stream);  
  if(isLast == false) {
    fputc(',', stream); 
  } else {    
    fputc('\n', stream); fputc('}', stream);  //! ie, the 'end' of the JSON
  }
  fputc('\n', stream);
}

/**
   @brief write out the obj to the stream as a set of relations.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stringOf_predicate> which if set is used as predicate when writing out the data, eg, to simplify interpreation of the relationships.
   @param <stream> is the poitner to the restul-stream.
   @param <isTo_useJavaScript_syntax> which if set to true implies that we makes use of a java-script syntax.
   @param <isTo_writeOut_pairsSeperately> which is used if were are only interestinged in rpitning otu the scores
 **/
void export_dataSet_s_dataStruct_matrix_dense_relations(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, const char *stringOf_predicate, FILE *stream, const bool isTo_useJavaScript_syntax, const bool isTo_writeOut_pairsSeperately) {
  //! What we expect:
  assert(obj);   assert(stream);
  assert(obj->matrixOf_data); assert(obj->cnt_columns); assert(obj->cnt_rows);
  
  const char seperator_columns = '\t';

  if(isTo_useJavaScript_syntax == false) {
    if(stringOf_header && strlen(stringOf_header)) {
      fprintf(stream, "\n#! %s\n", stringOf_header); //! where "#! " is used to avoid the string from wrongly being parsed as a data-row.
    }
  }
  
  //! Eitehr use the input-arguemnts or the 'implict' name-mappings found in "obj":
  char **nameOf_cols_local = (nameOf_columns != NULL) ? nameOf_columns : obj->nameOf_columns;
  char **nameOf_rows_local = (nameOf_rows != NULL) ? nameOf_rows : obj->nameOf_rows;
  if(nameOf_rows_local != NULL) {assert(nameOf_cols_local != NULL);} //! e, either noen or both are expected to be set.

  const char *stringOf_predicate_local = (stringOf_predicate && strlen(stringOf_predicate)) ? stringOf_predicate : "related-to";


  if(isTo_useJavaScript_syntax) {
    //! Write out the tab-formatted matrix:
    fputs("var setFromCGI_", stream); 
    assert(stringOf_header);   assert(strlen(stringOf_header));
    fputs(stringOf_header, stream); //! ie, the unique name of the matrix.
    
    //! 'Start' the matrix:
    fputc(' ', stream);   fputc('=', stream);   fputc(' ', stream); //! ie, " = "
    fputc('[', stream); 
  }

  //!
  //! Idnietyf columns to ignore:
  char empty_0 = 1; //! ie, amrk all columsn as of interest.
  char *mapOf_colsToUse = allocate_1d_list_char(obj->cnt_columns, empty_0);
  assert(mapOf_colsToUse);
  uint cntTotal__cols = 0;
  for(uint y = 0; y < obj->cnt_columns; y++) {
    long long int cnt_ofInterest = 0;
    for(uint i = 0; i < obj->cnt_rows; i++) {
      const t_float score = obj->matrixOf_data[i][y];  
      cnt_ofInterest += isOf_interest(score);
    }
    const bool isTo_use = (cnt_ofInterest > 0); //!, where where '0' implies 'false'.
    mapOf_colsToUse[y] = isTo_use;
    cntTotal__cols += isTo_use;
  }
  //!
  //! Write out the tab-formatted matrix:
  for(uint i = 0; i < obj->cnt_rows; i++) {
    //!
    //! Handle cases where none of the row-valeus have a vlaue associated (oekseth, 06. june. 2017).
    if(obj->matrixOf_data[i] == NULL) {continue;}
    assert(obj->matrixOf_data[i]);
    long long int cnt_ofInterest = 0;
    for(uint y = 0; y < obj->cnt_columns; y++) {
      const t_float score = obj->matrixOf_data[i][y];
      cnt_ofInterest += isOf_interest(score);
    }
    if(cnt_ofInterest == 0) {continue;} //! ie, as we then assume the row is Not of interest
    //! 
    //! Add the row-header:
    if(isTo_useJavaScript_syntax) {
      fputc('[', stream);
    }
    //! 
    //! Write out the scores:
    uint currPos__col = 0;
    for(uint y = 0; y < obj->cnt_columns; y++) {
      t_float score = obj->matrixOf_data[i][y];
      if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
      if(score == T_FLOAT_MAX) {score = 0;} //! ie, as we then assuem that  '0' implies 'emptyness' <-- TDOO[JC]: may you JC valdiate correcteness/implcaitosn wrt. this asusmption-strategy? (oekseth, 06. des. 2016).
      if(nameOf_rows_local != NULL) {
	if(isTo_useJavaScript_syntax == false) {
	  fprintf(stream, 
		  "%s%c"
		  "%s%c"
		  "%s%c"
		  "%f", 
		  //! Then map the [ªbove] print-variables to the [below] data-variables:
		  nameOf_rows_local[i], seperator_columns, 
		  stringOf_predicate, seperator_columns, 
		  nameOf_cols_local[y], seperator_columns, 
		  score);
	} else if(isTo_writeOut_pairsSeperately) {
	  //! Write out a java-script string to the "stream".
	  fputc('[', stream);
	  __write_outString_to_stream_javaScript(nameOf_rows_local[i], stream); fputc(',', stream); fputc(' ', stream);
	  __write_outString_to_stream_javaScript(stringOf_predicate, stream); fputc(',', stream); fputc(' ', stream);
	  __write_outString_to_stream_javaScript(nameOf_cols_local[y], stream); fputc(',', stream); fputc(' ', stream);
	  fprintf(stream, "%f", score);
	  fputc(']', stream);
	} else {
	  fprintf(stream, "%f", score);
	}
      } else {
	fprintf(stream, "%f", score);
      }
      if(isTo_useJavaScript_syntax == false) {
	if((y +1) != cnt_ofInterest) {fputc(seperator_columns, stream);} else {fputc('\n', stream);}	
      } else {
	if((y +1) != cnt_ofInterest) { fputc(',', stream);
	} //else {}
	if(isTo_writeOut_pairsSeperately) {
	  fputc('\n', stream);
	}
      }
      currPos__col++;
    }
    if(isTo_useJavaScript_syntax) {
      //! End this row:
      fputc(']', stream);
    }
  }
  if(isTo_useJavaScript_syntax) {
    //! End this matrix:
    //fputc(']', stream);
    fputc(']', stream); fputc(';', stream); fputc('\n', stream);
  }
  if(mapOf_colsToUse) {free_1d_list_char(&mapOf_colsToUse); mapOf_colsToUse = NULL;}
}


