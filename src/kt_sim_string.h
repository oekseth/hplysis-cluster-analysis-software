#ifndef kt_sim_string_h
#define kt_sim_string_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_sim_string
   @brief provide an interface to comptue string-based simlairtie-metrics (oekseth, 06. aug. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks used to simplify API and access in/through different programming-languates
**/

#include "kt_list_1d.h"
#include "kt_list_1d_string.h"
#include "kt_list_2d.h"
#include "kt_hash_string.h"

/**
   @enum e_kt_sim_string_type
   @brief identifies the string-based base-simalrity-metic type (oekseth, 06. aug. 2017).
 **/
typedef enum e_kt_sim_string_type {
  e_kt_sim_string_type_lcs,
  e_kt_sim_string_type_LevenshteinDistance,
  e_kt_sim_string_type_DL_DamerauLevenshteinDistance,
  e_kt_sim_string_type_jaro,
  e_kt_sim_string_type_jaro_winkler,
  //  e_kt_sim_string_type_,
  e_kt_sim_string_type_undef
} e_kt_sim_string_type_t;

//! @return the string-represetnaiton of the enum in quesiotn.
static const char *getString__e_kt_sim_string_type_t(const e_kt_sim_string_type_t enum_id) {
  if(enum_id == e_kt_sim_string_type_lcs) {
    return "lcs";
  } else if(enum_id == e_kt_sim_string_type_LevenshteinDistance) {
    return "Levenshtein";
  } else if(enum_id == e_kt_sim_string_type_DL_DamerauLevenshteinDistance) {
    return "DL";
  } else if(enum_id == e_kt_sim_string_type_jaro) {
    return "jaro";
  } else if(enum_id == e_kt_sim_string_type_jaro_winkler) {
    return "jaroWinkler";
  } else if(enum_id == e_kt_sim_string_type_undef) {
    return "undef";
    //  } else if(enum_id == ) {
  } else {
    fprintf(stderr, "!!\t Not supported for metric='%u', at [%s]:%s:%d\n", enum_id, __FUNCTION__, __FILE__, __LINE__);
    return NULL;
  }
}

//! @return the enum-id of the givne string-id, ie, ,if the altter correpsodns to our exptiaotns (oekseth, 06. aug. 2017).
static e_kt_sim_string_type_t get_enumFromString__e_kt_sim_string_type_t(const char *str) {
  assert(str); assert(strlen(str));
  for(uint enum_id = 0; enum_id < e_kt_sim_string_type_undef; enum_id++) {
    const char *str_cmp = getString__e_kt_sim_string_type_t((e_kt_sim_string_type_t)enum_id);
    assert(str_cmp); assert(strlen(str_cmp));
    if(strlen(str_cmp) == strlen(str)) {
      if(0 == strncmp(str, str_cmp, strlen(str))) {return (e_kt_sim_string_type_t)enum_id;}
    }
  }
  //!
  //! Then the stirng is Not found:
  return e_kt_sim_string_type_undef;
}

/**
   @struct s_kt_sim_string
   @brief initilize the s_kt_sim_string struct (oekseth, 06. aug 2017).
 **/
typedef struct s_kt_sim_string {
  bool isTo_normalize_result;
  bool isTo_in2dCompute_includeInResult_idsWithSameString;
  t_float in2dCompute_threshold_relative_minMax_stringLengthDiff;
} s_kt_sim_string_t;

//! @return an tinlized veriosn of the s_kt_sim_string_t object.
static s_kt_sim_string_t init__s_kt_sim_string_t() {s_kt_sim_string_t self; 
  self.isTo_normalize_result = false; 
  self.isTo_in2dCompute_includeInResult_idsWithSameString = false; //! ie, to Not insert strings in the reuslt-set which 'are the same'.
  self.in2dCompute_threshold_relative_minMax_stringLengthDiff = 4.0; //! ie, where a difference in stirng-length greater than '4' (eg, length(string1)=2 VS length(string2)=10 is ignored): latter thresohld is assuemd to be correct if a DB-SCAN post-prociesisng of set-evlauatiosn are sued/applied.
  return self;}

//! @return the string-based simalrity-metric.
t_float compute__kt_sim_string(const s_kt_sim_string_t *self, const e_kt_sim_string_type_t enum_id, const char *s1, const char *s2);

/**
   @param <self> hold the set of configuraitons to use (oekseth, 06. mar. 2017).
   @param <obj_1> is the first 2d-list to cmpare.
   @param <obj_2> is the second 2d-list to compare   
   @param <config_filter> which if Not set tu "NULL" is used to filter the vertices whcih are to be 'remembered'.
   @return a 2d-list which hold the result:
 **/
s_kt_list_2d_kvPair_t setOf__compute__kt_sim_string(s_kt_sim_string_t *self, const e_kt_sim_string_type_t enum_id, const s_kt_list_2d_kvPair_filterConfig_t *config_filter, s_kt_list_1d_string_t *obj_1, s_kt_list_1d_string_t *obj_2);

//! comptue simalrity between strings in the "s_kt_hash_string_2dSparse_t" data-set.
//! @return a list of relationships between the 'tails' of 'head_id'
//! @remarks takes an "s_kt_hash_string_2dSparse_t" object as input .... constrct an all-agaisnt-all' comptuation for a given 'head', where configuraitons are specifed/set in the "s_kt_sim_string_t" object ... export results to formats of ... 
static s_kt_list_2d_kvPair_t setOf__compute__vertex__kt_sim_string(s_kt_sim_string_t *self, const e_kt_sim_string_type_t enum_id, const s_kt_list_2d_kvPair_filterConfig_t *config_filter, const s_kt_hash_string_2dSparse_t *obj_data, const uint head_id, s_kt_list_1d_string_t *objResult_indexToStrings) {  
  assert(self);
  assert(config_filter); assert(obj_data);
  assert(objResult_indexToStrings);
  *objResult_indexToStrings = constructObj__listOf_stringsForHead__s_kt_hash_string_2dSparse_t(obj_data, head_id);
  //  s_kt_list_1d_string_t obj_strings = constructObj__listOf_stringsForHead__s_kt_hash_string_2dSparse_t(obj_data, head_id);
  s_kt_list_2d_kvPair_t obj_ret = setToEmpty__s_kt_list_2d_kvPair_t();
  if(objResult_indexToStrings->nrows > 0) {
  //  if(obj_strings.nrows > 0) {
    obj_ret = setOf__compute__kt_sim_string(self, enum_id, config_filter, objResult_indexToStrings, objResult_indexToStrings); //&obj_strings);
  }
  //! De-allocate:
  //  *objResult_indexToStrings = obj_strings; //!
  //free__s_kt_list_1d_string(&obj_strings);
  //!
  //! @return 
  return obj_ret;
}

#endif //! EOF
