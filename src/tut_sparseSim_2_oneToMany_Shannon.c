#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_list_1d.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
#include "kt_sparse_sim.h" //! which is used for sparse comptautions of simlairty-metircs.

/**
   @brief compute simliarty between one vector and a 2d-list of vectors
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- a permtuation of our "tut_sparseSim_1_vec_Euclid" and "tut_sparseSim_5_para_oneToMany_Gower.c": in contrast to latter we do Not make use of parallisaiton.
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  const uint nrows_1 = 1; //! ie, comptue for two vectors.
  const uint nrows_2 = 200; //! ie, comptue for two vectors.
  //! Note: in [”elow] de 'deinfe' varialbes which will result in in-feeficent parfalle procuessing, though simplies de-dubing and evlauation of our reuslts.
  /* const uint advanced__parallelConfig__maxCntBlocks__eachThread  = 1; //! which we use 'in this example' to 'force' a parallel exueciton. */
  /* const uint advanced__parallelConfig__maxCntBlocks__minRowCount = nrows_2/50; //! which we use 'in this example' to 'force' a parallel exueciton. */
  const uint ncols = 30; 
  //!
  //! Specify the simlairty-metic: 
  s_kt_correlationMetric_t sim_metric = setTo_empty__andReturn__s_kt_correlationMetric_t();
  sim_metric.metric_id = e_kt_correlationFunction_groupOf_shannon_JensenShannon; //! ie, the metric to use.
  //! -----------------------------------------------------------------------------------
  //!
  //! Allocate:
  s_kt_set_2dsparse_t sparse_1 = initAndReturn__allocate__s_kt_set_2dsparse_t(nrows_1, /*isTo_allocateFor_scores=*/true);
  assert(sparse_1._nrows == nrows_1); //! ie, gvien our [ªbove] isneriotn-policy.
  s_kt_set_2dsparse_t sparse_2 = initAndReturn__allocate__s_kt_set_2dsparse_t(nrows_2, /*isTo_allocateFor_scores=*/true);
  assert(sparse_2._nrows == nrows_2); //! ie, gvien our [ªbove] isneriotn-policy.
  //!
  //! Set values:
  const t_float distance_default = 1;
  for(uint row_id = 0; row_id < nrows_1; row_id++) {
    assert(row_id < sparse_1._nrows);
    for(uint col_id = 0; col_id < ncols; col_id++) {
      //! Note: latter '2d-stack-call' si similar t o our "s_kt_list_1d_pairFloat_t" value, insert, where we for hte latter may use: "vec_1.list[curr_pos] = MF__initVal__s_ktType_pairFloat(row_id, col_id, distance_default);"
      push__idScorePair__s_kt_set_2dsparse_t(&sparse_1, row_id, col_id, distance_default);
    }
  }
  for(uint row_id = 0; row_id < nrows_2; row_id++) {
    assert(row_id < sparse_2._nrows);
    for(uint col_id = 0; col_id < ncols; col_id++) {
      //! Note: latter '2d-stack-call' si similar t o our "s_kt_list_1d_pairFloat_t" value, insert, where we for hte latter may use: "vec_1.list[curr_pos] = MF__initVal__s_ktType_pairFloat(row_id, col_id, distance_default);"
      push__idScorePair__s_kt_set_2dsparse_t(&sparse_2, row_id, col_id, distance_default);
    }
  }
  //!
  //! Convert into a 2d-list:
  //false, isTo_allocateFor_scores)  s_kt_set_2dsparse_t sparse_1 = convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(&vec_1);
  //!
  //! Compute simliarty:
  s_kt_sparse_sim obj_sim = init__s_kt_sparse_sim_t(sim_metric);
  //! Speciufy the 'aprallel cofniguraitons':
  obj_sim.config.isTo__useParallelExeuction = false; 
  /* obj_sim.config.advanced__parallelConfig__maxCntBlocks__eachThread = advanced__parallelConfig__maxCntBlocks__eachThread;   */
  /* obj_sim.config.advanced__parallelConfig__maxCntBlocks__minRowCount = advanced__parallelConfig__maxCntBlocks__minRowCount;   */
  //!
  //! Apply logics:
  s_kt_set_2dsparse_t sparse_result = computeStoreIn__2dList__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/&sparse_2, /*isTo_rememberScores=*/true, /*weight=*/NULL);
  //! De-allcoate the sparse cofnigruation-object:
  free__s_kt_sparse_sim_t(&obj_sim);
  //!
  //! WRite out the result: access the 2d-list and write out the results:
  for(uint row_id = 0; row_id < sparse_result._nrows; row_id++) {
    uint key_val_size = 0; uint list_val_size = 0;
    const uint *key_val = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&sparse_result, row_id, &key_val_size);
    const t_float *list_val = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&sparse_result, row_id, &list_val_size);
    /* //! What we expect: */
    /* assert(key_val_size == ncols); */
    /* assert(list_val_size == ncols); */
    assert(key_val);
    assert(list_val);
    //!
    //! Read through the 'sparse' data-set:
    for(uint col_id = 0; col_id < key_val_size; col_id++) {
      /* assert(key_val[col_id] == col_id); */
      /* assert(list_val[col_id] == distance_default); */
      printf("[%u][%u] = %f, at %s:%d\n", row_id, key_val[col_id], list_val[col_id], __FILE__, __LINE__);
    }
  }

  

  //!
  //! De-allocates:
  free_s_kt_set_2dsparse_t(&sparse_1);
  free_s_kt_set_2dsparse_t(&sparse_2);
  //free__s_kt_list_1d_pairFloat_t(&vec_1);
  free_s_kt_set_2dsparse_t(&sparse_result);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}


