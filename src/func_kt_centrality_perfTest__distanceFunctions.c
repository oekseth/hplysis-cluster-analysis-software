//! **********************************'
//! **********************************'
//! **********************************'
/* #ifndef __kt_centralityMacroDefineFunc_buildDistanceTable__internalFuncBuildDistanceTable */
/* #define __kt_centralityMacroDefineFunc_buildDistanceTable__internal_inner(fun,suffix) fun ## _ ## suffix */
/* #define __kt_centralityMacroDefineFunc_buildDistanceTable__internal(fun,suffix) __kt_centralityMacroDefineFunc_buildDistanceTable__func(fun, suffix) */
/* #define __kt_centralityMacroDefineFunc_buildDistanceTable__internalFuncBuildDistanceTable() __kt_centralityMacroDefineFunc_buildDistanceTable__internal("__internalFuncBuildDistanceTable", __macro_funcTag__accessScores_distanceTable) */
/* #endif //! ie, as we otehrwise assuemt aht these 'fucntion-macro-wrapeprs' are defined (oekseth, 06. otk. 2016). */

//! ************* fucntiosn for testing the 'impact' of calling external fucntiosn instead of 'templat-einluceed' functions <-- we expect htis difference to be 4x 'owrose' if this function had been deifned in a different 'linked' file.
#if(__macro__typeOf_performanceTuning == __macro__e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric)
static t_float  __macro_funcTag__accessScores_distanceTable(t_float **matrix, const uint row_id, const uint col_id, const t_float *__restrict__ row, const s_kt_centrality_config_t self) {
//static t_float  __kt_centralityMacroDefineFunc_buildDistanceTable__internalFuncBuildDistanceTable(__macro_funcTag__accessScores_distanceTable)(t_float **matrix, const uint row_id, const uint col_id, const t_float *__restrict__ *row, const s_kt_centrality_config self) {
  t_float sum = 0;
#if(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_scalar)
      sum += row[col_id];
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_log) 
      sum += (sum != 0) ? mathLib_float_log(row[col_id]) : 0;
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) 
      sum += mathLib_float_pow(row[col_id], self.powerIncrease_ofDistanceSum);
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) 
      sum += mathLib_float_pow(self.powerIncrease_ofDistanceSum, row[col_id]);
#else //! then we need to add a 'handler' for thsi case.
#error "!!\t Add supprot for this case"
#endif      
      return sum;
}
#elif(__macro__typeOf_performanceTuning == __macro__e_kt_centrality_config__testImpactOfNaiveImplemementation_callExternalFunction_whencomputingMetric_andAccessMatrisTransposed) 
static t_float  __macro_funcTag__accessScores_distanceTable(t_float **matrix, const uint row_id, const uint col_id, const t_float *__restrict__ row, const s_kt_centrality_config_t self) {
//static t_float  __kt_centralityMacroDefineFunc_buildDistanceTable__internalFuncBuildDistanceTable(__macro_funcTag__accessScores_distanceTable)(t_float **matrix, const uint row_id, const uint col_id, const t_float *__restrict__ *row, const s_kt_centrality_config self) {
  t_float sum = 0;
#if(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_scalar)
      sum += matrix[col_id][row_id];
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_log) 
      sum += (sum != 0) ? mathLib_float_log(matrix[col_id][row_id]) : 0;
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_valuePow) 
      sum += mathLib_float_pow(matrix[col_id][row_id], self.powerIncrease_ofDistanceSum);
#elif(__macro__typeOf_eigenVectorType_adjustment == __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_pow_inOrder_powValue) 
      sum += mathLib_float_pow(self.powerIncrease_ofDistanceSum, matrix[col_id][row_id]);
#else //! then we need to add a 'handler' for thsi case.
#error "!!\t Add supprot for this case"
#endif      
      return sum;
}
#endif //! ie, otehrwise we assume these fucntiosn are not of specfici interest

//! -------------------------
#undef __config__compute_eigenVector__isTo_useIncrementallyUpdatedVector
#undef __config__compute_eigenVector__isTo_useSSE
#undef  __macro__typeOf_eigenVectorType_adjustment
#undef __config__typeOf_eigenVectorType_adjustment_postProcessTable
#undef __macro__typeOf_eigenVectorType_use
#undef __macro__typeOf_performanceTuning
#undef __macro_funcTag__accessScores_distanceTable
