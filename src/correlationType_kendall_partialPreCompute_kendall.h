#ifndef correlationType_kendall_partialPreCompute_kendall_h
#define correlationType_kendall_partialPreCompute_kendall_h


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file correlationType_kendall_partialPreCompute_kendall
   @brief an object-structure to partially pre-comptue Kendsalls correlation-measure for an input-matrix.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/


#include "mask_api.h"
#include "correlation_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"
#include "correlation_rank_rowPair.h" //! which defines functions used for our "s_correlation_rank_rowPair"
#include "s_kt_correlationConfig.h"
#include "correlationType_kendall.h"

/**
   @struct s_correlationType_kendall_partialPreCompute_kendall
   @brief a data-structure to hold pre-comptued ranks and memory-containers to be used in all-against all copmtuations of Kendall's correlation-measure.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/
typedef struct s_correlationType_kendall_partialPreCompute_kendall {
  float **matrixSorted_column; uint **matrixSorted_column_index;
  float *arr1_tmp; float *arr2_tmp;
  uint nrows; uint ncols; //! which is used to simplify the fucniton-calls and to vlaidate correnctess of arugment-inptus, ie, to detect erors (which sometimes occure) in the caller.
} s_correlationType_kendall_partialPreCompute_kendall_t;


//! Set the the "s_correlationType_kendall_partialPreCompute_kendall" data-structure to empty.
void setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self);
//! De-allocat the "s_correlationType_kendall_partialPreCompute_kendall" data-structure to empty.
void free__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self);

//! Initiate the "s_correlationType_kendall_partialPreCompute_kendall" data-structure.
//! @remarks In the 'itnation' we comptue the ranks for the set of rows, ie, may take some seconds for large matrices.
void init__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self, const uint nrows, const uint ncols, t_float **data, char **mask, const bool needTo_useMask_evaluation);

/**
   @brief comptue the correlations by applying the pre-computed ranks to a differnet data-vector.
   @param <self> is the object which hold data for index1
   @param <index1> is the identity of the internal rank to be sued.
   @param <vector_2> is the data-row to compare with
   @param <mask2_vector> is the mask used to identify the set of interesting results.
   @param <metric_id> is the metric to be used speicically for the type of kendall-correlation to be comptued.
   @param <needTo_useMask_evaluation> which is used as a cofnigruation-parmeter wrt. rank-comptautions (in Kendall Tau's corrleation-metric).
   @return the inferred correlation-metric
 **/
t_float compute_results_applyObjectToExternalRow__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self, const uint index1, t_float *vector_2, char *mask2_vector, const e_kt_correlationFunction_t metric_id, const bool needTo_useMask_evaluation);

#endif //! EOF
