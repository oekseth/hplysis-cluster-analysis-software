#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_entropy.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.

/**
   @brief examplfies how to comptue Renyi's entropy-score seperately for each row in a matrix.
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2017).
   @remarks describe basic lgoics wrt. our "hp_entropy.h" API.
   @remarks compilation: g++ -I.  -g -O0  -mssse3   -L .  tut_entropy_2_matrix_Renyi.c  -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_Mentro; date; time ./tut_Mentro
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif   
  const uint nrows = 20;   
  //!
  //! Allocate:
  s_kt_list_1d_uint_t list_count = init__s_kt_list_1d_uint_t(nrows);
  //!
  //! Set values:
  uint max_theoreticCountSize = 0;
  for(uint row_id = 0; row_id < nrows; row_id++) {
    list_count.list[row_id] = row_id;
    max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count.list[row_id]);
  }  
  //!
  //! Comptue entropy:
  if(false) {
    t_float result = 0;
    compute_entropy__list__hp_entropy(&list_count, max_theoreticCountSize, e_kt_entropy_genericType_ML_Shannon, /*isTo_applyPostScaling=*/false, &result);
    printf("#! Result: score=%f, at %s:%d\n", result, __FILE__, __LINE__);
  } else { //! then we examplfiy how to comptue for all fot he differnet entropy-metrics: 
    for(uint entropy_index = 0; entropy_index < e_kt_entropy_genericType_undef; entropy_index++) {
      const e_kt_entropy_genericType_t enum_id = (e_kt_entropy_genericType_t)entropy_index;
      t_float result = 0;
      compute_entropy__list__hp_entropy(&list_count, max_theoreticCountSize, enum_id, /*isTo_applyPostScaling=*/false, &result);
      printf("#! Result: score[%s]=%f, at %s:%d\n", get_stringOfEnum__e_kt_entropy_genericType_t(enum_id), result, __FILE__, __LINE__);
    }
  }

  //!
  //! De-allocates:
  free__s_kt_list_1d_uint_t(&list_count);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

