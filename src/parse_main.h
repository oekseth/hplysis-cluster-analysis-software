#ifndef parse_main_h
#define parse_main_h

/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "Mine-data-anlysis"
 *
 * "Mine-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "Mine-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with "Mine-data-analysis". 
 */


#include "configure_cCluster.h" //! Whcih specifies the optmizaitons and macro-configurations
#include "def_intri.h"
#include "s_dataStruct_matrix_dense.h"

/**
   @brief test existence of file.
   @param <path_to_file> is the location of the file in question.
   @param <verify_file_is_regular> in order to investigate if the file has the protection which we expect.
   @param <sizeOf_file> is set to the size of the identified file: may be set though a false value is returned.
   @return true if the file exist.
**/
bool file_exists(const char *path_to_file, const bool verify_file_is_regular, uint *sizeOf_file);

//#ifndef NDEBUG //! then we include a set of funcitons, ie, to simplify correcness-testing.
//! Insert a value into a cell, a calue which is either a flaot-number or a string.
void __update_cell(s_dataStruct_matrix_dense_t *obj, const uint word_max_length, const uint row_id, const uint cnt_tabs, const char *word_buffer, const uint pos_in_column_0_prev, const char *input_file);
//! Get proeprties assiated to a givne file:
void __get_facts_forFile(FILE *file, const char column_seperator, uint *cnt_rows, uint *cnt_columns, uint *word_max_length);
//#endif

//! Load a stream into an "s_dataStruct_matrix_dense_t" object.
//! @return 1 if the operation was a success.
bool parse_matrix_fromStream_tab_usingInitatedObject(s_dataStruct_matrix_dense_t *obj, FILE *file, const uint word_max_length, const char *input_file);

//! Load a stream into an "s_dataStruct_matrix_dense_t" object.
//! @return 1 if the operation was a success.
bool parse_matrix_fromStream_tab(s_dataStruct_matrix_dense_t *obj, FILE *file, const uint fractionOf_toAppendWith_sampleData_typeFor_rows, const uint fractionOf_toAppendWith_sampleData_typeFor_columns);

//! Load the contents of a file into an "s_dataStruct_matrix_dense_t" object:
//! @return 1 if the operation was a success.
bool parse_matrix_fromFile_tab(s_dataStruct_matrix_dense_t *obj, const char *file_name, const uint fractionOf_toAppendWith_sampleData_typeFor_rows, const uint fractionOf_toAppendWith_sampleData_typeFor_columns) ;

/**
   @brief load a data-set into the data_objh, either using input-data or a value-range-distribution defined by our "stringOf_sampleData_type" param.
   @param <data_obj> the object to hold the new-cosntructed data-set: if set to a string-value, the other function-arpameters are ginored.
   @param <input_file> which if set ideitnfied the input-file to use
   @param <cnt_rows> the number of rows to be cosnturcted/inferred.
   @param <cnt_columns> the number of features/columns to be cosnturcted/inferred.
   @param <readData_fromStream> which if set implies that the data-'foudn' in "stream" is used to buidl the data-matrix: has prioerty over the stringOf_sampleData_type funciton-parameter.
   @param <stringOf_sampleData_type> identified the type of funciton (eg, 'random' or 'uniform') for which we are to infer value-ranges.
   @param <fractionOf_toAppendWith_sampleData_typeFor_rows> identifies the percentage of sampel-data-dsitribiton rows to append to a 'real-life' data-set: is used to combine either input_file, readData_fromStream or stringOf_sampleData_type_realLife with stringOf_sampleData_type
   @param <fractionOf_toAppendWith_sampleData_typeFor_columns> identifies the percentage of sampel-data-dsitribiton columns to append to a 'real-life' data-set: is used to combine either input_file, readData_fromStream or stringOf_sampleData_type_realLife with stringOf_sampleData_type
   @param <stringOf_sampleData_type_realLife> which if set is used to idnentify 'in-line' real-life data-set which are to be used.
   @param <isTo_transposeMatrix> which if set to true implies that we transpose the matrix.
   @return true on success.
 **/
bool build_dataset_fromInputOrSample(s_dataStruct_matrix_dense_t *data_obj, const char *input_file, uint cnt_rows, uint cnt_columns, const bool readData_fromStream, const char *stringOf_sampleData_type, uint fractionOf_toAppendWith_sampleData_typeFor_rows, uint fractionOf_toAppendWith_sampleData_typeFor_columns, const char *stringOf_sampleData_type_realLife, const bool isTo_transposeMatrix);
/* //! A function to validate correctness of the lgocis in this c-file. */
/* void parse_main_assert(); */

#endif
