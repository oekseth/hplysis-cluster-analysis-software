#include "correlation_sort.h"
#ifdef __cplusplus
#include "quick_sort.h"
#endif
/* #ifdef __cplusplus */
/* #include "sort_merge_sort.h" */
/* #endif */
/*
Find the median of X(1), ... , X(N), using as much of the quicksort
algorithm as is needed to isolate it.
N.B. On exit, the array X is partially ordered.
Based on Alan J. Miller's median.f90 routine.
*/

//! @return the median 
//! @remarks is an atlernatve fucntion to comptue median.
t_float get_median_alt2 (int n,   t_float x[]) {
  int i, j;
  int nr = n / 2;
  int nl = nr - 1;
  int even = 0;
  /* hi & lo are position limits encompassing the median. */
  int lo = 0;
  int hi = n-1;

  if (n==2*nr) even = 1;
  if (n<3)
  { if (n<1) return 0.;
    if (n == 1) return x[0];
    return 0.5*(x[0]+x[1]);
  }

  /* Find median of 1st, middle & last values. */
  do
  { int loop;
    int mid = (lo + hi)/2;
    t_float result = x[mid];
    t_float xlo = x[lo];
    t_float xhi = x[hi];
    if (xhi<xlo)
    { t_float temp = xlo;
      xlo = xhi;
      xhi = temp;
    }
    if (result>xhi) result = xhi;
    else if (result<xlo) result = xlo;
    /* The basic quicksort algorithm to move all values <= the sort key (XMED)
     * to the left-hand end, and all higher values to the other end.
     */
    i = lo;
    j = hi;
    do
    { while (x[i]<result) i++;
      while (x[j]>result) j--;
      loop = 0;
      if (i<j)
      { t_float temp = x[i];
        x[i] = x[j];
        x[j] = temp;
        i++;
        j--;
        if (i<=j) loop = 1;
      }
    } while (loop); /* Decide which half the median is in. */

    if (even)
    { if (j==nl && i==nr)
        /* Special case, n even, j = n/2 & i = j + 1, so the median is
         * between the two halves of the series.   Find max. of the first
         * half & min. of the second half, then average.
         */
        { int k;
          t_float xmax = x[0];
          t_float xmin = x[n-1];
          for (k = lo; k <= j; k++) xmax = max(xmax,x[k]);
          for (k = i; k <= hi; k++) xmin = min(xmin,x[k]);
          return 0.5*(xmin + xmax);
        }
      if (j<nl) lo = i;
      if (i>nr) hi = j;
      if (i==j)
      { if (i==nl) lo = nl;
        if (j==nr) hi = nr;
      }
    }
    else
    { if (j<nr) lo = i;
      if (i>nr) hi = j;
      /* Test whether median has been isolated. */
      if (i==j && i==nr) return result;
    }
  }
  while (lo<hi-1);

  if (even) return (0.5*(x[nl]+x[nr]));
  if (x[lo]>x[hi])
  { t_float temp = x[lo];
    x[lo] = x[hi];
    x[hi] = temp;
  }
  return x[nr];
}


//! @remarks time-complexity estimated to "2"
//! @remarks if arr_tmpToBeUsedIn_sorting is set to NULL, then we assuem the inptu-list is sorted.
t_float get_median(t_float *arrOf, t_float *arr_tmpToBeUsedIn_sorting, const uint arrOf_list_size) { //int n, t_float x[])
  assert(arrOf);
  if(arr_tmpToBeUsedIn_sorting != NULL) {
    // FIXME[jc]: when you have validted the itrnistinctc sort-function .. then cosnider to 'call the latter' in [below]
    sort_array(arrOf, /*result=*/arr_tmpToBeUsedIn_sorting, arrOf_list_size, e_typeOf_sortFunction_ideal);
    //! Tehn update the inptu-array wr.t teh sorted list:
    memcpy(arrOf, arr_tmpToBeUsedIn_sorting, sizeof(t_float)*arrOf_list_size);
  }
  //! First investigate ifthe case is simple enough to 'skip any complex logics':
  if(arrOf_list_size < 3) {
    if(arrOf_list_size == 0) return 0.;
    else if(arrOf_list_size == 1) {return arrOf[0];}
    else {return 0.5*(arrOf[0] + arrOf[1]);}
    assert(false); //! ie, as we expect the 'exeuction' to never reach this exeuciton-point.
  }

  
  const uint nr = arrOf_list_size / 2;
  assert(nr > 0); //! ei, givne [ªbove] fi-clause
  const bool even = (arrOf_list_size == 2*nr);
  if(even) {
    const uint mid_low   = arrOf[nr];
    const uint mid_upper = arrOf[nr+1];
    return 0.5 * (mid_low + mid_upper);
  } else {
    return arrOf[nr];
  }
}

//! Extend the quick-sort algorithm to also update wrt. the index of the vertex, ie, whick explain the use of both "a" and "idx"?
static void __quicksort(t_float *a, uint *idx, const uint l, const uint u) {
  if (l >= u)
    return;

  // printf("range=[%u, %u], at %s:%d\n", l+1, u, __FILE__, __LINE__);  

  uint m = l; t_float a_temp;
  for (uint i=l+1; i<=u; i++) {
    if (a[i] < a[l]) {
      ++m;
      
      const uint idx_temp = idx[m];
      idx[m] = idx[i];
      idx[i] = idx_temp;
      
      a_temp = a[m];
      a[m] = a[i];
      a[i] = a_temp;
    }
  }
  
  const uint idx_temp = idx[l];
  idx[l] = idx[m];
  idx[m] = idx_temp;
  
  a_temp = a[l];
  a[l] = a[m];
  a[m] = a_temp;
  
  if(m > 0) {
    __quicksort(a, idx, l, m-1);
  }
  __quicksort(a, idx, m+1, u);
}

//! Extend the quick-sort algorithm to also update wrt. the index of the vertex, ie, whick explain the use of both "a" and "idx"?
static void __quicksort_uint(uint *a, uint *idx, const uint l, const uint u) {
  if (l >= u)
    return;

  // printf("range=[%u, %u], at %s:%d\n", l+1, u, __FILE__, __LINE__);  

  uint m = l; uint a_temp;
  for (uint i=l+1; i<=u; i++) {
    if (a[i] < a[l]) {
      ++m;
      
      const uint idx_temp = idx[m];
      idx[m] = idx[i];
      idx[i] = idx_temp;
      
      a_temp = a[m];
      a[m] = a[i];
      a[i] = a_temp;
    }
  }
  
  const uint idx_temp = idx[l];
  idx[l] = idx[m];
  idx[m] = idx_temp;
  
  a_temp = a[l];
  a[l] = a[m];
  a[m] = a_temp;
  
  if(m > 0) {
    __quicksort_uint(a, idx, l, m-1);
  }
  __quicksort_uint(a, idx, m+1, u);
}

//! The sort funciton to be used in the POSIX "qsort(..)" function.
static int compare_float(const void* a, const void* b) {
  const t_float term1 = *(const t_float*)a;
  const t_float term2 = *(const t_float*)b;
  if (term1 < term2) return -1;
  if (term1 > term2) return +1;
  return 0;
}
//! The sort funciton to be used in the POSIX "qsort(..)" function.
static int compare_uint(const void* a, const void* b) {
  const uint term1 = *(const uint*)a;
  const uint term2 = *(const uint*)b;
  if (term1 < term2) return -1;
  if (term1 > term2) return +1;
  return 0;
}

//! Apply a  variaent of the quick-sort algorithm
void quicksort(const uint n, const t_float *arr_input, uint *arr_index, t_float *arr_result) {
  //! Copy:
  memcpy(arr_result, arr_input, n * sizeof(t_float));

  if(arr_index == NULL) { //! then we use a srot-strategy assumed /expected to be more effective
    qsort(arr_result, n, sizeof(t_float), compare_float);
  } else {
    
    //! Set default values:
    for(uint i=0; i<n; i++) {arr_index[i] = i;}
    __quicksort(arr_result, arr_index, 0, n-1);  
  }
}
//! Apply a  variaent of the quick-sort algorithm
void quicksort_uint(const uint n, const uint *arr_input, uint *arr_index, uint *arr_result) {
  //! Copy:
  memcpy(arr_result, arr_input, n * sizeof(uint));

  if(arr_index == NULL) { //! then we use a srot-strategy assumed /expected to be more effective
    qsort(arr_result, n, sizeof(uint), compare_uint);
  } else {
    
    //! Set default values:
    for(uint i=0; i<n; i++) {arr_index[i] = i;}
    __quicksort_uint(arr_result, arr_index, 0, n-1);  
  }
}


//! Sort the list
//! @return the number of 'swaps' in the list, ie, the sort-distance between the input array and the 'resulting' sorted array.
static long int insertionSort(t_float *arr, size_t arr_size, uint *arr_index /* = NULL */) {  
  if(arr_size < 2) {
    return 0; //! ie, as we then assume the lsit is sorted.
  }

  const size_t max_pos_iteration = arr_size - 1;
  long int swapCount = 0; //! which is used to ifner the distance.
  for(size_t i = arr_size - 2; i < arr_size; i--) {
    size_t j = i;
    t_float val = arr[i];
    
    // FIXME: in our benchamrk-test ... compare [”elwo] with a mroe intutive 'while' appraoch/search ... and 'if tehy procuece the same result' then sue a whiel-loop for [”elow].
    for(; (j < max_pos_iteration) && (arr[j + 1] < val); j++) {
      arr[j] = arr[j + 1];
      // FIXME: validate correctness of [below] art. the arr_index
      if(arr_index) {arr_index[j] = arr_index[j+1];} //! ie, then remember the index-position.
    }

    arr[j] = val;
    // FIXME: validate correctness of [below] art. the arr_index
    if(arr_index) {arr_index[j] = arr_index[i];} //! ie, then remember the index-position.

    swapCount += (j - i); //! ie, the number of swaps at this disatnce-position: 'corresponds' to Kendalls tau at "index <= i".
  }

  return swapCount;
}

/**
   sort_array -- Sorts the array by moving elements forward
   until a a key that is greater than 'this' is found.
   @Changed: 22.06.2011 by oekseth
*/
long int sort_array_insertionSort_slow(t_float *array, const uint size) {
  long int swapCount = 0; //! which is used to ifner the distance.
  for(uint j = 1; j < size; j++) {
    const t_float key = array[j];    
    int i = j -1; // Task 
    while(i >= 0 && array[i] > key) array[i+1] = array[i], i--; // Moves key forwrad
    swapCount += (long int)(j - i); //! ie, the number of swaps at this disatnce-position: 'corresponds' to Kendalls tau at "index <= i".
    array[i+1] = key; // Sets
  }
  return swapCount;
}


/**
   @brief a modfied isnertion-sort with optimized memory-access-schemes
   @remarks expeted to perform 2.2x faster that the "sort_array_insertionSort_slow(..)" function: combines ordered lsit-traversal with optimized meomrya-cceess throguht ehte "memmove(..)" funciton.
*/
long int sort_array_insertionSort(t_float *list, const uint list_size) {
  assert(list);

  assert(false); // FIXME: seems like 'this' proceudr eis erronrosu ... ie, try fixing it.

  long int swapCount = 0; //! which is used to ifner the distance.
  printf("sort-list w/size=%u, at %s:%d\n", list_size, __FILE__, __LINE__);
  if(true) { //! then we writ eout the result:
    printf("list(input) = [");
    for(uint col_id = 0; col_id < list_size; col_id++) {printf("%f, ", list[col_id]);}
    printf("], at %s:%d\n", __FILE__, __LINE__);
  }
  for(int j = 1; j < list_size; j++) {
    const t_float key = list[j];
    const uint end_pos = list_size - j -1; // Task 
    //    const uint end_pos = j -1; // Task 
    int i = (int)end_pos; //j -1; // Task 
    assert_possibleOverhead(i < list_size);
    // int debug_cnt = 0;
    while( (i >= 0) && (list[i] > key) ) {
      //list[i+1] = list[i]; 
      i--; 
      //debug_cnt++;
    }// Moves key forwrad	
    printf("\t\t[%u] key=%f, i=%d, at %s:%d\n", j, key, i, __FILE__, __LINE__);    
    if( (i != end_pos) && (i >= 0) ) { //! then valeus are updated:
      const int length = (j - i) - 1;
      //printf("length=%d, i=%d, j=%d, list_size=%u, at [%s]:%s:%d\n", length, i, j, list_size, __FUNCTION__, __FILE__, __LINE__);
      if(length > 0) {
	assert( (i + 1 + length) <= list_size);
	const t_float *arr_local = &list[i];
	assert(arr_local);
	t_float *arr_dest = &list[i + 1];
	assert(arr_dest);
	/* for(uint k = 0; k < (uint)length; k++) {arr_dest[k] = 1;} */
	/* //	    arr_local[k];} */
	memmove(/*dest=*/arr_dest, arr_local, sizeof(t_float)*length); //! ie, to make room for: list[i+1] = key; // Sets
	list[i+1] = key; // ie, to 'place the old value in the 'free space' created [above].
      }
    }
  }
#if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: consider using a differene num */
  //! Validate that [ªbov€] result is correct:
  if(list_size > 0) {
    if(true) { //! then we writ eout the result:
      printf("list(sorted) = [");
      for(uint col_id = 0; col_id < list_size; col_id++) {printf("%f, ", list[col_id]);}
      printf("], at %s:%d\n", __FILE__, __LINE__);
    }
    for(uint col_id = 1; col_id < list_size; col_id++) {
      if(isOf_interest(list[col_id]) && isOf_interest(list[col_id-1])) {
	//assert(list_input[col_id] == array_in[col_id]); //! ie, validate correctness of [above] soritng-routine.
	assert(list[col_id-1] <= list[col_id]); //! ie, validate correctness of [above] soritng-routine.
      }
    }
  }
#endif
  // for(uint j = 1; j < size; j++) {
  //   const t_float key = array[j];
  //   int i = j -1; // Task 
  //   while(i >= 0 && array[i] > key) array[i+1] = array[i], i--; // Moves key forwrad
  //   swapCount += (long int)(j - i); //! ie, the number of swaps at this disatnce-position: 'corresponds' to Kendalls tau at "index <= i".
  //   array[i+1] = key; // Sets
  // }
  return swapCount;
}




//! Merge the sorted sectiosn of one list arr_input, and then copy the updated sorted list into arr_result
static long int merge(t_float *arr_input, t_float *arr_result, const size_t pos_middle, const size_t combined_size, uint *arr_index /* = NULL */) {
  t_float *item_left = arr_input;
  t_float *item_right = arr_input + pos_middle;
  long int swaps  = 0;
  size_t current_pos = 0;
  size_t right_size = combined_size - pos_middle;   size_t left_size = pos_middle;

  while(left_size && right_size) {
    if(item_right[0] < item_left[0]) {
      arr_result[current_pos] = item_right[0];
      swaps += left_size;
      right_size--;
      item_right++;
    } else {
      arr_result[current_pos] = item_left[0];
      left_size--;
      item_left++;
    }
    current_pos++;
  }
  
  if(left_size) {
    memcpy(arr_result + current_pos, item_left, left_size * sizeof(t_float));
  } else if(right_size) {
    memcpy(arr_result + current_pos, item_right, right_size * sizeof(t_float));
  }
  
  return swaps;
}


//! Use merge-sort for in-place sorting of arr_input data.
//! @return the sort-distance, which is (for some cases) used tor computation of Kendalls Tau.
long int mergeSort(t_float* arr_input, t_float* arr_tmp, const size_t arr_size, uint *arr_index) {
    if(arr_size < 2) {
        return 0;
    }

    // FIXME: update our time-benchmark ... test the 'effects' of [below] ... ie, the 'threshold where insertionSort has a high degree of perofmrnace'.
    if(true) { //! Note: 'this' appraoch is 'best' if the valeus/eelements has a low degree of sorting.
      if(arr_size < 32) {
	// FIXME[aritcle]: figure out why use of [”elow] results in a performance-increase of 1.1x for reverse-ordered-lsits and 1.1x drecase for random-ordered-lists.
	//return sort_array_insertionSort_slow(arr_input, arr_size);
	// printf("arr_size=%u, at %s:%d\n", (uint)arr_size, __FILE__, __LINE__);
	const long int ret_val = sort_array_insertionSort_slow(arr_input, arr_size);
#if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: consider using a differene num */
	//! Validate that [ªbov€] result is correct:
	if(arr_size > 0) {
	  for(uint col_id = 1; col_id < arr_size; col_id++) {
	    if(isOf_interest(arr_input[col_id]) && isOf_interest(arr_input[col_id-1])) {
	      //assert(arr_input[col_id] == array_in[col_id]); //! ie, validate correctness of [above] soritng-routine.
	      assert(arr_input[col_id-1] <= arr_input[col_id]); //! ie, validate correctness of [above] soritng-routine.
	    }
	  }
	}
#endif
	return ret_val;
	//return insertionSort(arr_input, arr_size, arr_index);
      }
    } else {
      if(arr_size < 10) {
	return sort_array_insertionSort_slow(arr_input, arr_size);
      }
    }

    //! First merge sort the two halfs, and then merge the result.
    //! @remarks the coutns are used iot. get an estimate/computation of "Kendalls Tau" (which is not of interest is all sue-cases).
    const size_t half = arr_size / 2;
    long int swaps = 0;
    swaps += mergeSort(arr_input, arr_tmp, half, arr_index);
    swaps += mergeSort(&arr_input[half], &arr_tmp[half], arr_size - half, arr_index);
    swaps += merge(arr_input, arr_tmp, half, arr_size, arr_index);
    //! Update the result:
    memcpy(arr_input, arr_tmp, arr_size * sizeof(t_float));
    return swaps;
}


//! Merge the sorted sectiosn of one list arr_input, and then copy the updated sorted list into arr_result
static long int merge_memoryAcceesOptimized(t_float *arr_input, t_float *arr_result, const size_t pos_middle, const size_t combined_size, uint *arr_index /* = NULL */) {
  t_float *item_left = arr_input;
  t_float *item_right = arr_input + pos_middle;
  long int swaps  = 0;
  size_t current_pos = 0;
  size_t right_size = combined_size - pos_middle;   size_t left_size = pos_middle;

  // //! Note: we asusme tht  item_right[0] and item_left[0] never changes as the arr_input is 'given' a constant 'space', and where arr_input is sepcted to be different from arr_result:
  // assert(arr_input != arr_result); 
  // const t_float item_right_value = item_right[0]; const t_float item_left_value = item_left[0];
  // //! Reduce the number of non-consequative memory-writes:
  // uint i = 0, j = 0;
  // for(uint k = left_size; k < r; k++) {
  //   if(list[0][i] < list[1][j]) {
  //     array[k] = list[0][i++]; 
  //   } else {
  //     array[k] = list[1][j++];
  //   }
  // }
  t_float *itemStartNonCopied_left = item_left; t_float *itemStartNonCopied_right = item_right;
  while(left_size && right_size) {
    // FIXME: validate that item_left_value and item_right_value never changes.
    //if(item_right_value < item_left_value) {
    if(item_right[0] < item_left[0]) {
      if(item_left != itemStartNonCopied_left) { //! then copy the 'previous' elemnetns from "left", ie, before updating wrt. left.
	assert(item_right == itemStartNonCopied_right);
	assert(item_left > itemStartNonCopied_left);
	//! Note: seems like memmove(..) goes approx. 1.1x faster tahn "memcpy(..)".
	// FIXME: comarpe memmv and memcpy
	const size_t cnt_elements = item_left - itemStartNonCopied_left;
	memmove(arr_result + current_pos - cnt_elements, itemStartNonCopied_left, sizeof(t_float)*cnt_elements);
	//memcpy(arr_result + current_pos - cnt_elements, itemStartNonCopied_left, sizeof(t_float)*cnt_elements);
	//! Then 'reset' the copy-operation:
	itemStartNonCopied_left = item_left;
      }
      //arr_result[current_pos] = item_right[0];
      swaps += left_size;
      right_size--;
      item_right++;
    } else {
      if(item_right != itemStartNonCopied_right) { //! then copy the 'previous' elemnetns from "right", ie, before updating wrt. left.
	assert(item_left == itemStartNonCopied_left);
	assert(item_right > itemStartNonCopied_right);
	// FIXME: comarpe memmv and memcpy
	const size_t cnt_elements = item_right - itemStartNonCopied_right;
	memcpy(arr_result + current_pos - cnt_elements, itemStartNonCopied_right, sizeof(t_float)*cnt_elements);
	//! Then 'reset' the copy-operation:
	itemStartNonCopied_right = item_right;
      }
      //arr_result[current_pos] = item_left[0];
      left_size--;
      item_left++;
    }
    current_pos++;
  }
  
  if(item_left != itemStartNonCopied_left) { //! then copy the 'previous' elemnetns from "left", ie, before updating wrt. left.
    assert(item_right == itemStartNonCopied_right);
    assert(item_left > itemStartNonCopied_left);
    // FIXME: comarpe memmv and memcpy
    const size_t cnt_elements = item_left - itemStartNonCopied_left;
    memmove(arr_result + current_pos - cnt_elements, itemStartNonCopied_left, sizeof(t_float)*cnt_elements);
    //! Then 'reset' the copy-operation:
    itemStartNonCopied_left = item_left;
  }
  if(item_right != itemStartNonCopied_right) { //! then copy the 'previous' elemnetns from "right", ie, before updating wrt. left.
    assert(item_left == itemStartNonCopied_left);
    assert(item_right > itemStartNonCopied_right);
    // FIXME: comarpe memmv and memcpy
    const size_t cnt_elements = item_right - itemStartNonCopied_right;
    memmove(arr_result + current_pos - cnt_elements, itemStartNonCopied_right, sizeof(t_float)*cnt_elements);
    //! Then 'reset' the copy-operation:
    itemStartNonCopied_right = item_right;
  }

  if(left_size) {
    memcpy(arr_result + current_pos, item_left, left_size * sizeof(t_float));
  } else if(right_size) {
    memcpy(arr_result + current_pos, item_right, right_size * sizeof(t_float));
  }
  
  return swaps;
}


//! Use merge-sort for in-place sorting of arr_input data.
//! @return the sort-distance, which is (for some cases) used tor computation of Kendalls Tau.
static long int mergeSort_memoryAcceesOptimized(t_float* arr_input, t_float* arr_tmp, const size_t arr_size, uint *arr_index /* = NULL */) {
    if(arr_size < 2) {
        return 0;
    }

    // FIXME: update our time-benchmark ... test the 'effects' of [below] ... ie, the 'threshold where insertionSort has a high degree of perofmrnace'.
    if(arr_size < 10) {
      return sort_array_insertionSort_slow(arr_input, arr_size);
      //return insertionSort(arr_input, arr_size, arr_index);
    }

    //! First merge sort the two halfs, and then merge the result.
    //! @remarks the coutns are used iot. get an estimate/computation of "Kendalls Tau" (which is not of interest is all sue-cases).
    const size_t half = arr_size / 2;
    long int swaps = 0;
    swaps += mergeSort_memoryAcceesOptimized(arr_input, arr_tmp, half, arr_index);
    swaps += mergeSort_memoryAcceesOptimized(arr_input + half, arr_tmp + half, arr_size - half, arr_index);
    swaps += merge_memoryAcceesOptimized(arr_input, arr_tmp, half, arr_size, arr_index);
    //! Update the result:
    memcpy(arr_input, arr_tmp, arr_size * sizeof(t_float));
    return swaps;
}




/**
   sort_array -- Sorts the array by moving elements backward
   until the list at index i is sorted.
   @Changed: 22.06.2011 by oekseth
*/
static void sort_array_bubbleSort(t_float *array, const uint size) {
  for(uint i = 0; i < size; i++) {
    for(uint j = size-1; j > i; j--) {
      // FIXME: test intrisintistcs for [”elow] ... and then update.
      if(array[j] < array[j-1]) {
	//! Swap:
	const t_float tmp = array[j-1];
	array[j] = array[j-1];
	array[j-1] = tmp;
      }
    }
  }
}


//! Sort a given array.
void sort_array(const t_float *array_in, t_float *array_result, const uint array_size, const enum e_typeOf_sortFunction sort_type) {
  assert(array_in); assert(array_result); assert(array_size > 0);

  //! Copy:
  memcpy(array_result, array_in, array_size * sizeof(t_float));
#if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: consider using a differene num */
  //! Validate that [ªbov€] result is correct:
  for(uint col_id = 0; col_id < array_size; col_id++) {
    if(isOf_interest(array_result[col_id]) ) { //&& isOf_interest(array_result[col_id-1])) {
      assert(array_result[col_id] == array_in[col_id]); //! ie, validate correctness of [above] soritng-routine.
      //assert(arr_result[col_id-1] <= arr_result[col_id]); //! ie, validate correctness of [above] soritng-routine.
    }
  }
#endif

  if( (sort_type == e_typeOf_sortFunction_ideal) || (sort_type == e_typeOf_sortFunction_undef) ) {
    // FIXME: updaaate 'this' when we  know the best-perofrming sort-call/funciton.
    // assert(sizeof(type_float) == sizeof(t_float));
    // sort_merge_sort<type_float>::sort_array((type_float*)array_result, array_size);
    const t_float defaultValue_float = 0;
    t_float *arr_tmp = allocate_1d_list_float(array_size, defaultValue_float); // new t_float[array_size];
    // printf("start the sorting for size=%u, at %s:%d\n", array_size, __FILE__, __LINE__);
    mergeSort(array_result, arr_tmp, array_size, NULL);
#if(configureDebug_useExntensiveTestsIn__tiling == 1) // TODO: consider using a differene num */
  //! Validate that [ªbov€] result is correct:
    for(uint col_id = 1; col_id < array_size; col_id++) {
      if(isOf_interest(array_result[col_id]) && isOf_interest(array_result[col_id-1])) {
	//assert(arr_result[col_id] == array_in[col_id]); //! ie, validate correctness of [above] soritng-routine.
	assert(array_result[col_id-1] <= array_result[col_id]); //! ie, validate correctness of [above] soritng-routine.
      }
    }
#endif
    free_1d_list_float(&arr_tmp);
  } else if(sort_type == e_typeOf_sortFunction_quickSort_POSIX) {
    qsort(array_result, array_size, sizeof(t_float), compare_float);
  } else if(
	    (sort_type == e_typeOf_sortFunction_quickSort_localImplementaiton_indexOverhead)
#ifndef __cplusplus
	    || (sort_type == e_typeOf_sortFunction_quickSort_templateImplementation_default)
	    || (sort_type == e_typeOf_sortFunction_quickSort_templateImplementation_randomized)
#endif
	    ) {
    //uint *arr_index = new uint[array_size];
    const t_float defaultValue_float = 0;
    uint *arr_index = allocate_1d_list_uint(array_size, defaultValue_float); // new t_float[array_size];
    quicksort(array_size, array_in, arr_index, array_result);
    free_1d_list_uint(&arr_index);
    //delete [] arr_index; //! ie, as we no longer need this.
  } else if(sort_type == e_typeOf_sortFunction_quickSort_templateImplementation_default) {
#ifdef __cplusplus
    quick_sort obj; obj.sort_array(array_result, array_size);
#else
    assert(false);
#endif
  } else if(sort_type == e_typeOf_sortFunction_quickSort_templateImplementation_randomized) {
#ifdef __cplusplus
    quick_sort obj;  obj.sort_array_randomized(array_result, array_size);
#else
    assert(false);
#endif
  } else if(sort_type == e_typeOf_sortFunction_mergeSort_internal) {
    const t_float defaultValue_float = 0;
    t_float *arr_tmp = allocate_1d_list_float(array_size, defaultValue_float); // new t_float[array_size];
    mergeSort(array_result, arr_tmp, array_size, NULL);
    free_1d_list_float(&arr_tmp);
    // delete [] arr_tmp; //! ie, as we no longer need this.
  } else if(sort_type == e_typeOf_sortFunction_mergeSort_internal_memoryAcceesOptimized) {
    const t_float defaultValue_float = 0;
    t_float *arr_tmp = allocate_1d_list_float(array_size, defaultValue_float); // new t_float[array_size];
    //t_float *arr_tmp = new t_float[array_size];
    mergeSort_memoryAcceesOptimized(array_result, arr_tmp, array_size, NULL);
    free_1d_list_float(&arr_tmp);
    //delete [] arr_tmp; //! ie, as we no longer need this.
  } else if(sort_type == e_typeOf_sortFunction_mergeSort_list_generic) {
    //assert(sizeof(type_float) == sizeof(t_float));
/* #ifdef __cplusplus */
/*     sort_merge_sort<type_float>::sort_array((type_float*)array_result, array_size); */
  
/* #else */
    fprintf(stderr, "!!\t TODO: cosnider to update the caller ... ie, to call teh fucniton-in-quesiton explcity, givent eh ANSI-C-compatible mdoficaiton recently made by (oekseth, 06. aug. 2016). OBservaiton at [%s]:%s:%d\n", __FUNCTION__, __FUNCTION__, __LINE__);
    assert(false); // FIXME: then add support for this ... ie, then udpate oru caller.
    //
  } else if(sort_type == e_typeOf_sortFunction_insertionSort_default) {
    sort_array_insertionSort_slow(array_result, array_size);
  } else if(sort_type == e_typeOf_sortFunction_insertionSort_bubbleSort) {
    sort_array_bubbleSort(array_result, array_size); //! Note: major difference wrt. this and "sort_array_insertionsort(..)" is the use of 'for-loop' VS 'while-loop', i, e an itneresting comparison-strategy.
  } else {
    assert(false); //! ie, then add support for this.
  }
}

// // FIXME:_ valdiate correctness of [”elow] ... which is used to 'avoid' the need to use FLT_MAx for some calls ... eg, in our "kmeans(..)" comptuations.
// static void replace_


//! @return the identifed distance for the pair in a symmetric matrix
//! @remarks This function searches the distance matrix to find the pair with the shortest distance between them. 
//!          The indices of the pair are returned in ip and jp; 
//! @remarks time-complexity estimated to be "n*n", ie, quadratic.
//! @remarks Memory-access-pattern of this funciton is:  two for-loops which in intship identify/find the pair and distance with the smallest value; in 'ideal' solutions/appraoches one cache-miss, otherwise "n" cache-misses. 
t_float find_closest_pair(const uint n, t_float** distmatrix, uint* ip, uint* jp) {
  //! What we expect:
  assert(distmatrix);
  assert(n > 0);
#if(optimize_parallel_useNonSharedMemory_MPI == 1)
  #error "Update this code-chunk with new/updated code" 
#endif
#if(optimize_parallel_2dLoop_useFast == 1)
#error "Add and test the effoect of parllel wrappers for this ... ie, after we have completed hte 'ordinary' perofmrance-tests"
#endif


  //! Intialize:
  t_float distance = distmatrix[1][0];
  *ip = 1;  *jp = 0;
  //! Find the 'shortest' pair:

  /* // FIXME: write emmory-benchmarks to test the effects of intinitstics on/for this function */

  /* if(true) { */

  /*   // FIXME[optimize]: may you jan-christiaon write an optmization of [”elow] code-chunk? <--  */

  /*   t_float bestScores_distance[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX}; */
  /*   t_float bestScores_point_i[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX}; */
  /*   t_float bestScores_point_j[4] = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX}; */

  /*   // uint idx = 0; __m128 max = {FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX}; */

  /*   for(uint i = 1; i < n; i++) {  */

  /*     assert(false); // FIXME: try to resovle errors in [”elow] 'optimzaiton-code'. */
      
  /*     uint j = 0; */
  /*     if((i+4) > 8) { */
  /* 	const uint last_pos = i-4; //! ie, to 'allow' for overflow. */
  /* 	for(uint j = 0; j < last_pos; j += 1) {  */
  /* 	  // const __m128 value = _mm_load_ps(&distmatrix[i][j]); // can either use an array or __m128 value calculated in loop */
  /* 	  // const __m128 temp1 = _mm_shuffle_ps(value, value, _MM_SHUFFLE(1, 0, 3, 2)); */
  /* 	  // const __m128 temp2 = _mm_max_ps(value, temp1); */
  /* 	  // const __m128 temp3 = _mm_shuffle_ps(temp2, temp2, _MM_SHUFFLE(2, 3, 0, 1)); */
  /* 	  // const __m128 temp4 = _mm_min_ps(temp3, temp2); */
  /* 	  // max = _mm_min_ps(temp4, max); */
        
  /* 	  // unsigned long index; */
  /* 	  // const unsigned long mask = _mm_movemask_ps(_mm_cmpeq_ps(value, max)); */
  /* 	  // const unsigned char bit = _BitScanForward(&index, mask); */

  /* 	  // if (bit) */
  /*         //   idx = i * 4 + index; */

  /* 	  __m128 vec_result_tmp = _mm_loadu_ps(&bestScores_distance[0]); */
  /* 	  const __m128 arr_data = _mm_load_ps(&distmatrix[i][j]); */
  /* 	  //! Find the maximum value: */
  /* 	  __m128 vec_max = _mm_max_ps(vec_result_tmp, arr_data); */

  /* 	  // FIXME: is there a more 'precie' methiod to infer/'get' the assicated index-i and index-k? */

  /* 	  //! Figure out if the values are in the max-set: */
  /* 	  __m128 vec_isInMax_data1 = _mm_cmplt_ps(vec_max, arr_data); */
  /* 	  __m128 vec_isNotInMax_result = _mm_cmpge_ps(vec_result_tmp, arr_data); */
  /* 	  //__m128 vec_isInMax_inverted = _mm_and_ps(vec_result_tmp, _mm_setzero_ps()); */
	  
  /* 	  //! Identify the indices assicated to the max-index, first for the 'new set': */
  /* 	  t_float flt_tmp_i = (t_float)i; */
  /* 	  __m128 vec_data_data_index_i = _mm_mul_ps(vec_isInMax_data1, _mm_load1_ps(&flt_tmp_i)); */
  /* 	  // FIXME: instead of [”elow] try using the _mm_shuffle(...) */
  /* 	  t_float flt_tmp_j[4] = {(t_float)(j+0), (t_float)(j+1), (t_float)(j+2), (t_float)(j+3)}; */
  /* 	  __m128 vec_data_data_index_j = _mm_mul_ps(vec_isInMax_data1, _mm_load_ps(&flt_tmp_j[0])); */

  /* 	  //! Identify the indices assicated to the max-index, thereafter for the 'best-fit set': */
  /* 	  __m128 vec_data_maxUpdated_index_i = _mm_mul_ps(vec_isNotInMax_result, _mm_load_ps(&bestScores_point_i[0])); */
  /* 	  __m128 vec_data_maxUpdated_index_j = _mm_mul_ps(vec_isNotInMax_result, _mm_load_ps(&bestScores_point_j[0])); */

  /* 	  //! Combine the results: as we expect only one of the indeices to be set, we use 'add': */
  /* 	  _mm_store_ps(&bestScores_distance[0], vec_max); //! ie, sotre the max-values. */
  /* 	  _mm_store_ps(&bestScores_point_i[0], _mm_add_ps(vec_data_maxUpdated_index_i, vec_data_data_index_i)); //! ie, max-valeus for store for index=i */
  /* 	  _mm_store_ps(&bestScores_point_j[0], _mm_add_ps(vec_data_maxUpdated_index_j, vec_data_data_index_j)); //! ie, max-valeus for store for index=j */
  /* 	} */
  /*     } */
  /*     //! Handle the not-yet-covered cases: */
  /*     for(; j < i; j++) {  */
  /* 	const t_float temp = distmatrix[i][j]; */
  /* 	if(temp < bestScores_distance[0]) { //! then a shorter distance is found. */
  /* 	  bestScores_distance[0] = temp; */
  /* 	  bestScores_point_i[0] = i; */
  /* 	  bestScores_point_j[0] = j; */
  /* 	} */
  /*     } */
  /*   } */
  /*   //! Identify the min-point: */
  /*   for(uint i = 0; i < 4; i++) { */
  /*     const t_float temp = bestScores_distance[i]; */
  /*     if(temp < distance) { //! then a shorter distance is found. */
  /* 	distance = temp; */
  /* 	*ip = bestScores_point_i[i]; */
  /* 	*jp = bestScores_point_i[i]; */
  /*     } */
  /*   } */
  /* } else { */
  for(uint i = 1; i < n; i++) { 
    for(uint j = 0; j < i; j++) { 
      const t_float temp = distmatrix[i][j];
      if(temp < distance) { //! then a shorter distance is found.
	distance = temp;
	*ip = i;
	*jp = j;
      }
    }
  }
    //  }
  return distance;
}



// FXIME: consider to add support for 'multi-dimensional' distance-spaces .... eg writing/adding support for: "https://en.wikipedia.org/wiki/Minkowski_distance"
// // All rights reserved,
//   // Derrick Pallas
//   // License: zlib
// #include <cmath>
// #include <cstdlib>
// #include <limits>
// #include <vector>
//    template <typename fp_type>
//    fp_type minkowski_distance(const fp_type & p, const std::vector<fp_type> & ds) {
//   // ds contains d[i] = v[i] - w[i] for two vectors v and w
//   fp_type ex = 0.0;
//   fp_type min_d = std::numeric_limits<fp_type>::infinity();
//   fp_type max_d = -std::numeric_limits<fp_type>::infinity();
//   for (int i = 0 ; i < ds.size() ; ++i) {
//     fp_type d = std::mathLib_float_abs(ds[i]);
//     ex += std::pow(d, p);
//     min_d = std::min(min_d, d);
//     max_d = std::max(max_d, d);
//   }
//   return std::isnan(ex) ? ex
//     : !std::isnormal(ex) && std::signbit(p) ? min_d
//     : !std::isnormal(ex) && !std::signbit(p) ? max_d
//     : std::pow(ex, 1.0/p);
//    }

