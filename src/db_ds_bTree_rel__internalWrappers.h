//! *************************************************************

/**
   @struct s_db_node_typeOfKey_rel_case4
   @brief describes a B-tree struct for "int" with "4" children
 **/
typedef struct s_db_node_typeOfKey_rel_case4  {
  int n; /* n < M No. of keys in node will always less than order of Btree */
  struct s_db_node_typeOfKey_rel_case4 *pointers[__M__constant__case_4]; /* (n+1 pointers will be in use) */
  // FIXME: update [below] "keys" using our "s_db_rel" struct.
  s_db_rel_t key[__M__constant__case_4 -1 ]; /*array of names*/
} s_db_node_typeOfKey_rel_case4_t;
/**
   @struct s_db_node_typeOfKey_rel_case16
   @brief describes a B-tree struct for "int" with "4" children
 **/
typedef struct s_db_node_typeOfKey_rel_case16  {
  int n; /* n < M No. of keys in node will always less than order of Btree */
  struct s_db_node_typeOfKey_rel_case16 *pointers[__M__constant__case_16]; /* (n+1 pointers will be in use) */
  // FIXME: update [below] "keys" using our "s_db_rel" struct.
  s_db_rel_t key[__M__constant__case_16 -1 ]; /*array of names*/
} s_db_node_typeOfKey_rel_case16_t;
/**
   @struct s_db_node_typeOfKey_rel_case32
   @brief describes a B-tree struct for "int" with "4" children
 **/
typedef struct s_db_node_typeOfKey_rel_case32  {
  int n; /* n < M No. of keys in node will always less than order of Btree */
  struct s_db_node_typeOfKey_rel_case32 *pointers[__M__constant__case_32]; /* (n+1 pointers will be in use) */
  // FIXME: update [below] "keys" using our "s_db_rel" struct.
  s_db_rel_t key[__M__constant__case_32 -1 ]; /*array of names*/
} s_db_node_typeOfKey_rel_case32_t;
