//! --------------------
for(uint cmpMetric_id = 0; cmpMetric_id < e_kt_matrix_cmpCluster_metric_undef; cmpMetric_id++) {
  assert(globalOffset__betweeDataSets < global_cntRows);
  //e_kt_clusterComparison_t metric_clusterCmp_between = (e_kt_clusterComparison_t)cluster_cmp_id;
  const e_kt_matrix_cmpCluster_metric_t metric_clusterCmp = (e_kt_matrix_cmpCluster_metric_t)cmpMetric_id;
  char typeOf_distanceInference[2000] = {'\0'};      
  sprintf(typeOf_distanceInference, "%s::%s", stringOf_dataSet,
	  getStringOf__e_kt_matrix_cmpCluster_metric_t(metric_clusterCmp));
  //getStringOf__e_kt_clusterComparison_t(metric_clusterCmp));
  //getStringOf__e_kt_clusterComparison_t(metric_clusterCmp_between));	
  //sprintf(typeOf_distanceInference, "%s::%s::randomFraction(%s", stringOf_dataSet,   getStringOf__e_kt_clusterComparison_t(metric_clusterCmp_between), randomFraction);	
  //!
  //! 
  //if( (cmpMetric_id == 0) ) 
  { //! then we 'add' the string-identifiers, ie, 'for each new data-set':
    assert(global_cntRows_current < matrixOf_results.nrows);
    set_string__s_kt_matrix(&matrixOf_results, global_cntRows_current, /*stringTo_add=*/typeOf_distanceInference, /*addFor_column=*/false);
  }

  //! Iterate through the random-fraction-degrees:
  //fprintf(stderr, "\t (start)\t random-fraction for: \"%s\", at %s:%d\n", typeOf_distanceInference, __FILE__, __LINE__);
  for(uint randomFraction = 0; randomFraction < global_cntCols; randomFraction++) {
    {
      

      const uint cnt_clusters_2 = cnt_clusters_1;
      uint *mapOf_vertexToCentroid1 = mapOf_vertexToCentroid1__gold;
      assert(mapOf_vertexToCentroid1 != NULL); 
      uint *mapOf_vertexToCentroid2 = NULL;
      // fprintf(stderr, "\t\t random=%u (start::vector)\t random-fraction for: \"%s\", at %s:%d\n", randomFraction, typeOf_distanceInference, __FILE__, __LINE__);
      if(randomFraction != 0) { //! then we apply randomsiation:
	 uint list_size_cntToSwap = (uint)cnt_vertices*(loint)((t_float)100/(t_float)(global_cntCols - randomFraction));
	 // printf("list_size_cntToSwap=%u, at %s:%d\n", list_size_cntToSwap, __FILE__, __LINE__);
	//const uint list_size_cntToSwap = (uint)((t_float)cnt_vertices/(t_float)global_cntCols);
	 if( (global_cntRows_current == 0) ) { //! then we 'add' the string-identifiers:
	   char typeOf_distanceInference[2000] = {'\0'};      
	   sprintf(typeOf_distanceInference, "randomFraction(%u)", randomFraction);
	   assert(randomFraction < matrixOf_results.ncols);
	   set_string__s_kt_matrix(&matrixOf_results, randomFraction, /*stringTo_add=*/typeOf_distanceInference, /*addFor_column=*/true);
	 }

	assert(list_size_cntToSwap > 0);
	assert(cnt_vertices != 0); //! ie, as we otherwise 'should' update our settigns.
	// TODO: consider seetting [”elow] "config_swapsMayOverlap" to 'false' <-- set to "true" iot. reduce the exeuction-time-overhead	
	//! Note:t he use of the "maxValue_toSetInRandomization" in [below] 'ensures' that we do Not have a 'simple swap' between clsuter-emmberships, eg, to avoid the case where a gold-standard-vector=[0, 0, 0] will always result in a randomziaton-result of [0, 0, 0] ... in this cotnext ntoe taht "-1" is used iot. 'say' that the amx-vluster-id is "cnt_clusters_2 - 1" (ie, uin constrast to a 'simple count').
	assert(cnt_clusters_2 != UINT_MAX); 	assert(cnt_clusters_2 != 0);
	mapOf_vertexToCentroid2 = math_generateDistribution__randomPermutation__uint(mapOf_vertexToCentroid1, cnt_vertices, /*list_size_cntToSwap=*/list_size_cntToSwap, /*config_swapsMayOverlap=*/true, /*maxValue_toSetInRandomization=*/cnt_clusters_2-1); //! where the latter parameter is used to simplify 'rep-rpodcuabilty' of 'these reuslts' (ie, as random-adjustment by def. is random).
	assert(mapOf_vertexToCentroid2);
	if(false) {
	  printf("gold-distribution:\t");
	  for(uint i = 0; i < cnt_vertices; i++) {printf("[%u]=%u, ", i, mapOf_vertexToCentroid2[i]);}
	  printf("\t at %s:%d\n", __FILE__, __LINE__);
	}
	//if( (randomFraction +1)  == global_cntCols) {assert(false);} // FIXME: remove
      } else { mapOf_vertexToCentroid2 = mapOf_vertexToCentroid1;}

      t_float **matrixOf_coOccurence = matrixOf_coOccurence_gold;
      if(randomFraction != 0) { //! then we apply randomsiation:
	const loint list_size_cntToSwap__total = (loint)(cnt_vertices*cnt_vertices)*(loint)((t_float)100/(t_float)(global_cntCols - randomFraction));
	assert(list_size_cntToSwap__total > 0);
	//fprintf(stderr, "\t\t random=%u -> %lld/%u (start::matrix)\t random-fraction for: \"%s\", at %s:%d\n", randomFraction, list_size_cntToSwap__total, cnt_vertices*cnt_vertices, typeOf_distanceInference, __FILE__, __LINE__);
	// TODO: consider seetting [”elow] "config_swapsMayOverlap" to 'false' <-- set to "true" iot. reduce the exeuction-time-overhead
	matrixOf_coOccurence = math_generateDistribution__randomPermutation__matrix(matrixOf_coOccurence_gold, /*nrows=*/cnt_vertices, /*ncols=*/cnt_vertices, list_size_cntToSwap__total, /*config_swapsMayOverlap=*/true); //! where the latter parameter is used to simplify 'rep-rpodcuabilty' of 'these reuslts' (ie, as random-adjustment by def. is random).
	assert(matrixOf_coOccurence);
      }
      assert(matrixOf_coOccurence != NULL);
      assert(mapOf_vertexToCentroid2 != NULL); 
      //! ---- 
      s_kt_matrix_cmpCluster_t obj_cmp; setTo_empty__s_kt_matrix_cmpCluster_t(&obj_cmp);
      const uint max_cntClusters = macro_max(cnt_clusters_1, cnt_clusters_2);


      assert(matrixOf_coOccurence); //! ie, as we expect 'this to be set'.    
      // printf("\t(copmute)\t %s\t\t [%u][%u]\t, at %s:%d\n", string_local, data_1, data_2, __FILE__, __LINE__);
      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      //fprintf(stderr, "\t\t random=%u (start::CCM)\t random-fraction for: \"%s\", at %s:%d\n", randomFraction, typeOf_distanceInference, __FILE__, __LINE__);
      //!
      //! Compute:
      build_coOccurenceMatrix__s_kt_matrix_cmpCluster_t(&obj_cmp, mapOf_vertexToCentroid1, mapOf_vertexToCentroid2, cnt_vertices, /*isTo_includeWeight=*/false, max_cntClusters, matrixOf_coOccurence, matrixOf_coOccurence, config_intraCluster);
      if(!( (obj_cmp.clusterDistances_cluster1.nrows != 0)  && (obj_cmp.clusterDistances_cluster2.nrows != 0) )) {
	assert(false); //! ie, as we then have an in-cosnsitency.
      }
	    
      //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
      //! Comptue for the 'specific' cluster-comparison-score:
      s_kt_matrix_cmpCluster_result_scalarScore_t metric_result;
      computeClusterSimliarity_score__s_kt_matrix_cmpCluster_t(&obj_cmp, metric_clusterCmp, &metric_result); 
      const t_float scalar_score_altTest = metric_result.score_1;
      assert(global_cntRows_current < matrixOf_results.nrows);
      assert(randomFraction < matrixOf_results.ncols);
      matrixOf_results.matrix[global_cntRows_current][randomFraction] = scalar_score_altTest;
      //!
      //! Updat ehte matrix with the score:
      //!
      //! De-allocate:
      free__s_kt_matrix_cmpCluster(&obj_cmp);
      if(mapOf_vertexToCentroid2 != mapOf_vertexToCentroid1) {
	assert(mapOf_vertexToCentroid2);
	free_1d_list_uint(&mapOf_vertexToCentroid2); mapOf_vertexToCentroid2 = NULL;
      }
      if(matrixOf_coOccurence != matrixOf_coOccurence_gold) {
	assert(matrixOf_coOccurence);
	free_2d_list_float(&matrixOf_coOccurence, cnt_vertices); matrixOf_coOccurence = NULL;
      }
    }
	  
    // init__s_kt_matrix_cmpCluster_clusterDistance(&(clusterDistances_cluster), mapOfCluster_local, countArray_size, localMaxCount__clusters, matrixOf_coOccurence, config);
	  
  }
  global_cntRows_current++;
 }      
