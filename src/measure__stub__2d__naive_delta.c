{ //! Test optmized SSE-functions
  const char *stringOf_measureText_base = "optimized-tiling-function: an 'optmized case', ie, wrt. the max-possbile optmizaiton-degrees on computers";
  { 
    const char *centrality_metric_type = "float";
    t_float **matrix_result = allocate_2d_list_float(nrows, ncols, default_value_float);
   //! -------------------------------
    for(char config_useTwoMatricesAsInput = 0; config_useTwoMatricesAsInput < 2; config_useTwoMatricesAsInput++) {
      const uint SM_map_size = 6; const uint SM_map[SM_map_size] = {16, 32, 64, 128, 256, 512};
      for(uint sm_cnt = 1; sm_cnt < SM_map_size; sm_cnt++) {
	const uint SM = SM_map[sm_cnt];
	//printf("SM=%u for nrwos=%u and ncols=%u, at %s:%d\n", SM, nrows, ncols, __FILE__, __LINE__);
	if( (SM > nrows) || (SM > ncols) ) {continue;} //! ie, as we then drop evlauating twr. this case
	assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'..		    
	//! -------------------------------------------	
	t_float **local_matrix_2 = matrix;
	if(config_useTwoMatricesAsInput) { local_matrix_2 = allocate_2d_list_float(nrows, ncols, default_value_float);}

	
	{ //! ------------------------------------------
	  //! Generate the measurement-test:
	  char stringOf_measureText[2000]; memset(stringOf_measureText,  '\0', 2000);
	  sprintf(stringOf_measureText, "%s for correlation-metric: %s and type=%s; use-two-differentMatrices-as-inpnut='%s'", 
		  stringOf_measureText_base, "Euclid", centrality_metric_type,
		  (config_useTwoMatricesAsInput) ? "true" : "false");
	  
	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	  //!
	  //!
	  //! The experiemnt:    
	  //printf("---- start euclid-float, at %s:%d\n", __FILE__, __LINE__);
	  kt_func_metricInner_fast__fitsIntoSSE__euclid__float(matrix, local_matrix_2, nrows, ncols, matrix_result, SM);
	  //printf("ok:---- start euclid-float, at %s:%d\n", __FILE__, __LINE__);

	  //! --------------
	  __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__tiling(stringOf_measureText, nrows, size_of_array, SM, mapOf_timeCmp_forEachBucket[chunk_index]); 	  	  
	}
	{ //! ------------------------------------------
	  //! Generate the measurement-test:
	  char stringOf_measureText[2000]; memset(stringOf_measureText,  '\0', 2000);
	  sprintf(stringOf_measureText, "%s for correlation-metric: %s and type=%s; use-two-differentMatrices-as-inpnut='%s'", 
		  stringOf_measureText_base, "CityBlock", centrality_metric_type,
		  (config_useTwoMatricesAsInput) ? "true" : "false"	 );
	  
	  //! Start the clock:
	  start_time_measurement();  //! ie, start measurement for a 'compelte matrix'
	  //!
	  //!
	  //! The experiemnt:    
	  kt_func_metricInner_fast__fitsIntoSSE__cityBlock__float(matrix, local_matrix_2, nrows, ncols, matrix_result, SM);

	  //! --------------
	  __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__tiling(stringOf_measureText, nrows, size_of_array, SM, mapOf_timeCmp_forEachBucket[chunk_index]); 	  	  
	}
	//! ------------------------------------------
	//! De-allocate:
	if(config_useTwoMatricesAsInput) { free_2d_list_float(&local_matrix_2, nrows);}
      }
    }
    free_2d_list_float(&matrix_result, nrows);
  }
  //! ---------------------------------------------------------------------------------------------------------------
  { const char *centrality_metric_type = "int16";
    const int16_t default_value_int16 = 10;
    int16_t **matrix_int16  = allocate_2d_list_int16_t(nrows, ncols, default_value_int16);
    int16_t **matrix_result = allocate_2d_list_int16_t(nrows, ncols, default_value_int16);
   //! -------------------------------
    for(char config_useTwoMatricesAsInput = 0; config_useTwoMatricesAsInput < 2; config_useTwoMatricesAsInput++) {
      const uint SM_map_size = 6; const uint SM_map[SM_map_size] = {16, 32, 64, 128, 256, 512};
      for(uint sm_cnt = 1; sm_cnt < SM_map_size; sm_cnt++) {
	const uint SM = SM_map[sm_cnt];
	if( (SM > nrows) || (SM > ncols) ) {continue;} //! ie, as we then drop evlauating twr. this case.		    //! -------------------------------------------	
	int16_t **local_matrix_2 = matrix_int16;
	if(config_useTwoMatricesAsInput) { local_matrix_2 = allocate_2d_list_int16_t(nrows, ncols, default_value_float);}

	// FIXME[JC]: may yoy try including [”elow] when the isse wrt. our "kt_func_metricInner_fast__fitsIntoSSE__euclid__int16(..)" is resolved?
	
	/* { //! ------------------------------------------ */
	/*   //! Generate the measurement-test: */
	/*   char stringOf_measureText[2000]; memset(stringOf_measureText,  '\0', 2000); */
	/*   sprintf(stringOf_measureText, "%s for correlation-metric: %s and type=%s; use-two-differentMatrices-as-inpnut='%s'",  */
	/* 	  stringOf_measureText_base, "Euclid", centrality_metric_type, */
	/* 	  (config_useTwoMatricesAsInput) ? "true" : "false"); */
	  
	/*   //! Start the clock: */
	/*   start_time_measurement();  //! ie, start measurement for a 'compelte matrix' */
	/*   //! */
	/*   //! */
	/*   //! The experiemnt:     */
	/*   kt_func_metricInner_fast__fitsIntoSSE__euclid__int16(matrix_int16, local_matrix_2, nrows, ncols, matrix_result, SM); */

	/*   //! -------------- */
	/*   __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__tiling(stringOf_measureText, nrows, size_of_array, SM, mapOf_timeCmp_forEachBucket[chunk_index]); 	  	   */
	/* } */
	/* { //! ------------------------------------------ */
	/*   //! Generate the measurement-test: */
	/*   char stringOf_measureText[2000]; memset(stringOf_measureText,  '\0', 2000); */
	/*   sprintf(stringOf_measureText, "%s for correlation-metric: %s and type=%s; use-two-differentMatrices-as-inpnut='%s'",  */
	/* 	  stringOf_measureText_base, "CityBlock", centrality_metric_type, */
	/* 	  (config_useTwoMatricesAsInput) ? "true" : "false"); */
	  
	/*   //! Start the clock: */
	/*   start_time_measurement();  //! ie, start measurement for a 'compelte matrix' */
	/*   //! */
	/*   //! */
	/*   //! The experiemnt:     */
	/*   kt_func_metricInner_fast__fitsIntoSSE__cityBlock__int16(matrix_int16, local_matrix_2, nrows, ncols, matrix_result, SM); */

	/*   //! -------------- */
	/*   __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__tiling(stringOf_measureText, nrows, size_of_array, SM, mapOf_timeCmp_forEachBucket[chunk_index]); 	  	   */
	/* } */
	//! ------------------------------------------
	//! De-allocate:
	if(config_useTwoMatricesAsInput) { free_2d_list_int16_t(&local_matrix_2, nrows);}
      }
    }
    free_2d_list_int16_t(&matrix_result, nrows);
    free_2d_list_int16_t(&matrix_int16, nrows);
  }
  //! ---------------------------------------------------------------------------------------------------------------
  //! ---------------------------------------------------------------------------------------------------------------
}
//! ****************************************************************************
if(for_slowVersion) {
  { //! test for the extreme case where all mask-valeus are of interest:
    const char *stringOf_measureText_base = "naive-3d-delta-correlations";
    //! -------------------------------------------------------------
    const char *stringOf_proxType = "Euclid-correlation";
#define __localConfig__funcName __kt_euclid_slow
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! -------------------------------------------------------------
    stringOf_proxType = "citbyblock-correlation";
#define __localConfig__funcName __kt_cityblock_slow
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ---   
    //! -------------------------------------------------------------
    stringOf_proxType = "pearson-correlation";
#define __localConfig__funcName correlation
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ------------------------------------------
    stringOf_proxType = "pearson-correlation-absoluteDiff";
#define __localConfig__funcName acorrelation
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ------------------------------------------
    stringOf_proxType = "pearson-correlation--uncentered";
#define __localConfig__funcName ucorrelation
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ------------------------------------------
    stringOf_proxType = "pearson-correlation--uncentered-absolute";
#define __localConfig__funcName uacorrelation
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ------------------------------------------
    //! -------------------------------------------------------------
  }
  { //! Test for the extreme case where none of the cases are of itnerest, ie, where neither the data nor the aritmetics are applied:
    const char *stringOf_measureText_base = "naive-3d-delta-correlations(extreme-case where all mask-valeus are set to '0', ie, where neither the data nor the aritmetics are applied)";
    const char default_value_char = 0;
    char **mask1 = allocate_2d_list_char(nrows, ncols, default_value_char);
    char **mask2 = allocate_2d_list_char(nrows, ncols, default_value_char);
    //! -------------------------------------------------------------
    const char *stringOf_proxType = "pearson-correlation";
#define __localConfig__funcName correlation
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ------------------------------------------
    stringOf_proxType = "pearson-correlation-absoluteDiff";
#define __localConfig__funcName acorrelation
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ------------------------------------------
    stringOf_proxType = "pearson-correlation--uncentered";
#define __localConfig__funcName ucorrelation
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ------------------------------------------
    stringOf_proxType = "pearson-correlation--uncentered-absolute";
#define __localConfig__funcName uacorrelation
#include "measure__stub__2d__naive_delta__stub__naive.c"
    //! ------------------------------------------
    //! -------------------------------------------------------------
    free_2d_list_char(&mask1, nrows);
    free_2d_list_char(&mask2, nrows);
  }
}
if(for_fastVersion) {
  { const char *stringOf_measureText_base = "correlation-fast::one-to-many, ie, one row to all other rows";
#define __localConfig__measureDelta__type__manyToMany 0
#define __localConfig__measureDelta__type__each 0
#include "measure__stub__2d__naive_delta__stub__fast.c"
  }
  { const char *stringOf_measureText_base = "correlation-fast::each, ie, seperately for each correlation-measure";        
#define __localConfig__measureDelta__type__manyToMany 0
#define __localConfig__measureDelta__type__each 1
#include "measure__stub__2d__naive_delta__stub__fast.c"
  }
  { const char *stringOf_measureText_base = "correlation-fast::many-to-many, ie, all rows to all other rows: faciltitates/enalbes the optmizaiton wrt. memory-access and data-pre-computation";
#define __localConfig__measureDelta__type__manyToMany 1
#define __localConfig__measureDelta__type__each 0
#include "measure__stub__2d__naive_delta__stub__fast.c"    
  }  
 }
    //! ****************************************************************************


