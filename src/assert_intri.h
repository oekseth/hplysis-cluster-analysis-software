#ifndef assert_intri_h
#define assert_intri_h

#include "def_intri.h"
#include "libs.h"

/**
   @file assert_intri
   @brief provide correctness-tests for itnrisitnistics macros in our "def_intri.h" (oekseth, 06. juni 2016).
   @author Ole Kristian Ekseth (oekseth)
 **/

//! The main assert function.
void assert_intri_main();

#endif
