#ifndef kt_clusterAlg_kruskal_h
#define kt_clusterAlg_kruskal_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_clusterAlg_kruskal
   @brief provides an itnerface to compute "Minium Spanning Trees" (MST) using Kruskal's algorithm (oekseth, 06. feb. 2017). 
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017). 
   @remarks general:
   -- optimisation: we do Not add explctilit supprot for 'disjoint-optimization-evalaution' given (a) the relative lwo exeuction-time of "kruskal" and (b) the default 'sapse' impelmetnation which we use (in oru Kruskal-impelmetatnion). 
   -- "k-means" support: provide/add supprot for 'breaking' when 'k clusters' are 'reached'
   @remarks algorithm-mdoficiatons: if the "s_kt_clusterAlg_config__ccm" is 'reuqested to be used' then we:
   -- CCM-evlauation as convergence-criteria: make use of "Cluster Comparison Metrics" (CCMs) to ivnestigate if a 'merge in data' improves the cluster-result-accraucy, ie, use the altter as a convrgence-critera wrt. merging of sub-trees.
   -- multiple trees: if k-means-min and/or k-means-min is se tthen we stop merging of vertex-foest-subsets when the cluster-coutn-threshold is reached. Tehrefore, our Kruskal MST approach becomes a permtaution of both "Hiearchical Cluster Algorithms" (HCA) and "k-means" clustering-algorithms.
   @remarks CCM-application: as an optmizaiton-strategy wrt. CCM-applicaiton we use a 'time-point' to start evaluating CCM-scores: to 'only investgiate CCM-merge-beeinficitality' if: (a) the emrged clsuters are above a min-cluster-size, (b) at least M vertices are members of a clsuter 'with vertices otehr than itself'; if the latter cirtiera as 'matched'/'acceptedx' then we asusem 'the MST emrged clsuters' provide a better cluster. In order to 'faciltate' a back-track we use a 'compelte list-iteraiton', an operation which results in a $|V|$ time-cost for each CCM-einveistgioatn. However, asn the CMC-investgiaton has a time-cost of $|E|$ the latter evlauation-obvherehad is isngiciant; of improtance is hterefore the reudce/limti the number of CCM-elvuaiaotn-steps, ie, as examplfied/described in our latter procedure. Inj thsi CCM-evlauaiton we assume that a non-masked matrix is provided/used as input, ie, in cotnast to the/a user-deifned 'masked matrix of simalrity-scores' (eg, using our MIEN-based simlarity-atmrix as ipnut).
   @remarks simliarty(disjoint-forest): in our Kruskal-implenmetnaiton we make use of the same optmized set-based merging-lgocis as used in hpLyssis dsijtoint-forest-impelemtant ("kt_forest_findDisjoint.h"), ie, for which we observe taht "Kruskal" is a permtaution of "disjoitn-forest" where (a) the edges/relationshisp/cells are sorted beofre the iteraiotn and (b) the iteriaont-path is remembered/stored. 
   @remarks simliarty(HCA): what we obseve is that the seleciton of '
k' clsuters from HCA is a permtatuion of "Kruskal": to ceonceptually understand/see the simlarity between "KRuskal" and "HCA" (gneierc): if we modfiy the HCa-kmeans-aprtiion-algorithm into an appraoch where we merge HCA-results using a 'breath-first' appraoch, eg, always selecting the 'min-sum-node-to-merge' ... and then use 'this' as a strategy to merge different distjoint-forest-results ... where we comptue the weight of each cluster using $[\sum\sum d(x \in c_i, c(c(i)))$, $\sum\sum d(x \in c_i, y \in c_i), ...]$ ... the result/algorithm becomes a 'de-facto' "Kruskal" algorithm, ie, hence the itnerest/improtance in using and/or elvauitng Kruskals algoritm-eprformance wrt. an heustic clsuter-emberign-appraoch.
 **/

#include "kt_clusterAlg_config__ccm.h"
#include "kt_clusterAlg_hca_resultObject.h"
#include "kt_clusterAlg_fixed_resultObject.h"

/**
   @brief compute Kruskal MST using a sparse lsit-object obj_sparse as input (oekseth, 06. feb. 2017).
   @param <obj_sparse> is an input-object of nodes: for details see our "kt_clusterAlg_hca_resultObject.h": to simplify red-use we choose to 'mdofity the latter' into a sorted lsit, ie, instead for 'first taking a copy'
   @param <obj_result_hca> if set is updated with the consturcted/inferred tree of relationiships
   @param <obj_result_kMeans> if set is updated witht the distjoitn set of vertices.
   @param <config> if used then we use a mofidefed Kruskal-impelmetnation iot. try optmising the inferred set of clsuters: to split the MST into descrptive sub-units cahracterisisgn core-traits of the entwork
   @param <isAn__adjcencyMatrix> a contral-parameter: used to make the caller explcitly aware of oru expectiaont that the matrix is an ajdency-matrix: if set to false an error is returend.
   @param <unique_vertices> is the nubmer fo qunie vertices in the data-set.
   @return true if the cofngiruaiton went smooth.
   @remarks in thsi implementaiotn we asusme taht "obj_sparse" is a sparse list of relation-pairs, ie,  with a large number of 'holes' (expected to be described using the "T_FLOAT_MAX" value). If the latter asusmption does Not hold then we expect a sgiciant slwo-down wrt. the exeuction. An example-case wrt. the latter 'maksed proeprty' is to: (a) comptue an all-agaisnt-all-amtrix using MINE; (b) comptue the ranks (for each vertex-row); (c) apply a max-rank-filter; (d) call this fucntion.
 **/
bool cluster_inputSparse__kt_clusterAlg_kruskal(s_kt_clusterAlg_hca_resultObject_t *obj_sparse, s_kt_clusterAlg_hca_resultObject_t *obj_result_hca, s_kt_clusterAlg_fixed_resultObject_t *obj_result_kMeans, const s_kt_clusterAlg_config__ccm *config, const bool isAn__adjcencyMatrix, const uint unique_vertices);

/**
   @brief compute Kruskal MST using a matrix obj_1 as input (oekseth, 06. feb. 2017).
   @param <obj_1> is the input-matrix: expected to be an adjcency-matrix, ie, where matrix[i][k] describes the dsitance between vertex(i)--vertex(k) (ie, in cotrnast to ffeature-matrix where the namespace of the rows and columns differs)
   @param <obj_result_hca> if set is updated with the consturcted/inferred tree of relationiships
   @param <obj_result_kMeans> if set is updated witht the distjoitn set of vertices.
   @param <config> if used then we use a mofidefed Kruskal-impelmetnation iot. try optmising the inferred set of clsuters: to split the MST into descrptive sub-units cahracterisisgn core-traits of the entwork
   @param <isAn__adjcencyMatrix> a contral-parameter: used to make the caller explcitly aware of oru expectiaont that the matrix is an ajdency-matrix: if set to false an error is returend.
   @return true if the cofngiruaiton went smooth.
   @remarks in thsi implementaiotn we asusme taht "obj_1" is a matrix with a large number of 'holes' (expected to be described using the "T_FLOAT_MAX" value). If the latter asusmption does Not hold then we expect a sgiciant slwo-down wrt. the exeuction. An example-case wrt. the latter 'maksed proeprty' is to: (a) comptue an all-agaisnt-all-amtrix using MINE; (b) comptue the ranks (for each vertex-row); (c) apply a max-rank-filter; (d) call this fucntion.
 **/
bool cluster_inputMatrix__kt_clusterAlg_kruskal(const s_kt_matrix_t *obj_1, s_kt_clusterAlg_hca_resultObject_t *obj_result_hca, s_kt_clusterAlg_fixed_resultObject_t *obj_result_kMeans, const s_kt_clusterAlg_config__ccm *config, const bool isAn__adjcencyMatrix);


#endif //! EOF
