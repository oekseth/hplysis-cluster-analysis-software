#include "kt_clusterAlg_fixed.h"
#include "kt_distance.h"
//#include "kt_clusterAlg_fixed__alg__miniBatch.c"

#if(configure_isTo_useLog == 1)
static s_log_clusterC_t logFor_kMeans;
#endif


//#define configure_isTo_useLog__local configure_isTo_useLog
#define configure_isTo_useLog__local 0

/* //! @return true if we asusem teh t_float-value is of interest. */
/* static float isOf_interest_func(const float value) { */
/*   return ( (value != T_FLOAT_MAX) && (value != T_FLOAT_MIN_ABS) ); */
/* } */



// //! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
// static float *allocate_1d_list_float(const uint size, const float default_value) {  
//   float *tmp = new float[size];
//   memset(tmp, default_value, size*sizeof(float));
//   return tmp;
// }
// //! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
// static void free_1d_list_float(float **list) {  
//   assert(list);
//   float *tmp = *list;
//   delete [] tmp;
// }


void getclustermeans_slow(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp, const uint transpose) {
#include "func_getclustermeans_slow.c" //! ie, hte function in quesiton.
}

static void getclustermeans_fast(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp, const bool mayUse_zeroAs_emptySign_inComputations, const bool needTo_useMask_evaluation) {
  //! Intiate the dummy/temproary list:
  assert(cmask_tmp);
  for(uint i = 0; i < nclusters; i++) {
    memset(cmask_tmp[i], 0, sizeof(t_float)*ncolumns);
  }
    //    memset(cmask_tmp[0], 0, sizeof(t_float)*ncolumns*nclusters);

#if(optimize_parallel_useNonSharedMemory_MPI == 1)
  #error "Update this code-chunk with new/updated code" 
#endif

  //! Intialise:
  if(!cmask) {
    // FIXME: udpate our systentic benchmarks to evaluate the performance-difference.
    for(uint i = 0; i < nclusters; i++) {
      memset(cdata[i], 0, sizeof(t_float)*ncolumns);
    }
    //memset(cdata[0], 0, sizeof(t_float)*nclusters*ncolumns);
  } else {
    assert(cdata);
    //printf("ncols=%u, nclusters=%u, at %s:%d\n", ncolumns, nclusters, __FILE__, __LINE__);
    for(uint i = 0; i < nclusters; i++) {
      memset(cdata[i], 0, sizeof(t_float)*ncolumns);
    }
    if(cmask) {
      for(uint i = 0; i < nclusters; i++) {
	memset(cmask[i], 0, sizeof(char)*ncolumns);
      }
    }
    /* memset(cdata[0], 0, sizeof(t_float)*ncolumns*nclusters); */
    /* if(cmask) {memset(cmask[0], 0, sizeof(char)*ncolumns*nclusters);} */
  }
  //! Update the sum of weights assicated to each cluster wrt. a cells (head, tail) weights.
  const t_float localConfig__empty_value = (mayUse_zeroAs_emptySign_inComputations) ? 0 : T_FLOAT_MAX;

    
  // FIXME: add parallisation to [below] <-- how may we 'handle' the udpate of the variabesl "cdata" and "cmask_tmp"? <--- consider to use an outer "forr(uint i = 0; i < nclusters; i++)" loop to identify 'interesting' nrow-varaibles ... which woulde cause an overhead of ...??...  <-- instead use seperate containers for each of the treads ... and thereafter emrge the containers. <-- given teh low comptautional compelxity 'of this' consider 'omit such'.
  const uint ncolumns_intri = VECTOR_FLOAT_maxLenght_forLoop(ncolumns);
  const uint ncolumns_intri_mask = VECTOR_CHAR_maxLenght_forLoop(ncolumns);
  for(uint k = 0; k < nrows; k++) {
    // FIXME: try to write a perofrmance-test whe we figure out "how mcuh memory-ovehrehad need to be assicated to cahc-misses for 'sroting the index-lookups-table'  to have a perofmrance-point" (eosekth, 06. june 2016)
    if(clusterid[k] > nclusters) {clusterid[k] = nclusters -1;} //! ie, to fix any such issues (oesketh, 06. feb. 2017).
    const uint i = clusterid[k];
    assert_possibleOverhead(i != UINT_MAX);
    assert_possibleOverhead(i < nclusters);
    
    uint j = 0;
    /* if(needTo_useMask_evaluation == false) {       */
    /*   if(ncolumns_intri > 0) { */
    /* 	for(; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) { */
    /* 	  VECTOR_FLOAT_storeAnd_add_data(&cdata[i][j], &data[k][j]); //! ie, then incremnet cdata with the valeus in data */
    /* 	  VECTOR_FLOAT_storeAnd_add(&cdata[i][j], VECTOR_FLOAT_setFrom_scalar(1)); //! ie, increment for all indeices. */
    /* 	} */
    /*   } */
    /*   for(; j < ncolumns; j++) { */
    /* 	cdata[i][j] += data[k][j]; */
    /* 	cmask_tmp[i][j]++; //! ie, mark the centroid 'as of interest'. */
    /*   } */
    /* } else  */
    {
      if(!mask) { //! Thwn we evaluate wrt. the T_FLOAT_MAX attribute.
	/* if(ncolumns_intri > 0) { */
	/*   for(; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) { */
	/*     VECTOR_FLOAT_TYPE vec_mask; */
	/*     VECTOR_FLOAT_storeAnd_add(&cdata[i][j], VECTOR_FLOAT_dataMask_data1(&data[k][j], vec_mask)); //! ie, then incremnet cdata with the valeus in data if T_FLOAT_MAX is not in the data-set. */
	/*     VECTOR_FLOAT_storeAnd_add(&cmask_tmp[i][j], vec_mask); //! ie, mark the centroid 'as of interest'. */
	/*   } */
	/* } */
	for(; j < ncolumns; j++) {
	  const t_float score = data[k][j];
	  if(isOf_interest(score)) {
	    cdata[i][j] += score;
	    cmask_tmp[i][j]++; //! ie, mark the centroid 'as of interest'.
	  }
	}
      } else { //! then we use the 'char' mask-array:
	/* if(ncolumns_intri > 0) { */
	/*   for(; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) { */
	/*     VECTOR_FLOAT_TYPE vec_mask; */
	/*     VECTOR_FLOAT_storeAnd_add(&cdata[i][j], VECTOR_FLOAT_charMask_data1(&data[k][j], mask[k], j, vec_mask)); //! ie, then incremnet cdata with the valeus in data if mask is set for the cells in quesiton. */
	/*     VECTOR_FLOAT_storeAnd_add(&cmask_tmp[i][j], vec_mask); //! ie, mark the centroid 'as of interest'. */
	/*   } */
	/* } */
	for(; j < ncolumns; j++) {
	  if(mask[k][j]) {
	    cdata[i][j] += data[k][j];
	    cmask_tmp[i][j]++; //! ie, mark the centroid 'as of interest'.
	  }
	}
      }
    }
  }
  //! Infer the 'midpoints' for each centroid:
  for(uint i = 0; i < nclusters; i++) {
    uint j = 0;
    /* if(mayUse_zeroAs_emptySign_inComputations) { */
    /*   if(ncolumns_intri > 0) { */
    /* 	for(; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) {	     */

    /* 	  assert(false); // FIXME: validate that the 'dvidye-by-zero' case is correctly handled <--- try to use an 'xor' case */
    /* 	  VECTOR_FLOAT_storeAnd_div_self_data(&cdata[i][j], &cmask_tmp[i][j]); //! ie, then: cdata[i][j] /= cmask_tmp[i][j]; */
    /* 	} */
    /*   }  */
    /* } */
    if(cmask) { //! then we use a seperate for-loop:
      uint j = 0;
      //! TODO[JC]: consider including [”elow] ... which is current 'commented out' ... challenge is that the the dfiference data-type-sizes, ie, where "cmask" is in in "t_float" while "cmask" is in "char"
      // if(ncolumns_intri_mask > 0) {
      // 	for(; j < ncolumns_intri; j += VECTOR_CHAR_ITER_SIZE) {
      // 	  assert(false); // FIXME: add soemthing	  <--- call our VECTOR_CHAR_getBoolean_aboveZero
      // 	  //VECTOR_CHAR_storeAnd_add(&cmask[i][j], VECTOR_CHAR_convertFrom_t_float(VECTOR_FLOAT_getBoolean_aboveZerocmask_tmp[i], /*pos-dynamic=*/j));
      // 	  VECTOR_CHAR_storeAnd_add(&cmask[i][j], VECTOR_CHAR_convertFrom_t_float_toBoolean(cmask_tmp[i], /*pos-dynamic=*/j));
      // 	}
      // }
      for(; j < ncolumns; j++) {
	if(cmask_tmp[i][j] > 0) {
	  cmask[i][j] = 1;
	}
      }
    }

    {
      for(; j < ncolumns; j++) {
	// FIXME: seperate 'maks' alterantives ... and use _mm_storeu_ps(..)
	if(cmask_tmp[i][j] > 0) {
	  cdata[i][j] /= cmask_tmp[i][j]; //! ie, infer the average-value/'center' of each centroid.
	  //if(cmask) {}
	} else {
	  cdata[i][j] = localConfig__empty_value;
	  if(mayUse_zeroAs_emptySign_inComputations == false) {
	  /*   cdata[i][j] = 0; */
	  /* } else { */
	  /*   cdata[i][j] = T_FLOAT_MAX; */
	    // FIXME: validate [”elow] ... and if [”elow] does not hold .. then update our itanlizaiton-procedure.
	    if(mask) {assert(!mask[i][j]);}
	    assert(isOf_interest(cdata[i][j]) == false);
	  }
	}
      }
    }
  }
}


/**
   @brief identify the center-vertex fo each cluster: calculates the cluster centroids, given to which cluster each element belongs.
   @remarks The  The centroid is defined as the mean over all elements for each dimension.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks a subset of the parameters are described below:
   # mask:       (input) int[nrows][ncolumns] This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
   # clusterid: The cluster number to which each element belongs. Iftranspose == 0, then the dimension of clusterid is equal to nrows (the number of genes). Otherwise, it is equal to ncolumns (the number of microarrays).
   # cdata: used 'in funciton-return': updated with the cluster centroids.
   # cmask: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid i fall corresponding data values of the cluster members are missing.
   @remarks has a time-cost of "nclusters * nrows"
   @return true upon success
 **/
bool getclustermeans__extensiveInputParams(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp,  uint transpose, const bool isTo_copmuteResults_optimal, const bool mayUse_zeroAs_emptySign_inComputations, const bool needTo_useMask_evaluation) {


  /* printf("mask=%p, nrows=%u, ncols=%u, at %s:%d\n", mask, nrows, ncolumns, __FILE__, __LINE__); */

  /* { //! a dummy approach to test for erorrs. */
  /*   char sum = 0; */
  /*   for(uint i = 0; i < nrows; i++) {     for(uint j = 0; j < ncolumns; j++) { sum += mask[i][j];}} */
  /* } */

  assert(cdata);
  if(!transpose && isTo_copmuteResults_optimal) {
    getclustermeans_fast(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, cmask_tmp, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
  } else {
    getclustermeans_slow(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, cmask_tmp, transpose);
  }
  return true;
}

/* ********************************************************************* */

/**
   @brief idetnfy the median scores/weights of each cluster
   @remarks Calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the median over all elements for each dimension.
   @remarks a subset of the parameters are described below:
   # mask:       (input) int[nrows][ncolumns] This array shows which data values are missing. Ifmask[i][j]==0, then data[i][j] is missing.
   # clusterid: The cluster number to which each element belongs. Iftranspose == 0, then the dimension of clusterid is equal to nrows (the number of genes). Otherwise, it is equal to ncolumns (the number of microarrays).
   # cdata: used 'in funciton-return': updated with the cluster centroids.
   # cmask: an array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing.
   @remarks has a time-cost of "nclusters * ncolumns * nrows"
 **/
bool getclustermedians__extensiveInputParams(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp, uint transpose, const bool isTo_copmuteResults_optimal, t_float **arrOf_valuesFor_clusterId_global, const bool mayUse_zeroAs_emptySign_inComputations, const bool needTo_useMask_evaluation) {
  //! What we expect:
  assert(nclusters > 0);
  assert(nrows > 0);
  assert(data);
  /* assert(nclusters <= nrows); */
  /* assert(nclusters <= nclusters); */
  /* if(transpose == 0) { */
  /*   if(nclusters >= nrows) { */
  /*     fprintf(stderr, "!!\t An errnous call: you requested %u clusters to be computed for %u rows: please update tour call: for qeustions contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", nclusters, nrows, __FUNCTION__, __FILE__, __LINE__); */
  /*     return  false; */
  /*   }  */
  /* } else { */
  /*   if(nclusters >= ncolumns) { */
  /*     fprintf(stderr, "!!\t An errnous call: you requested %u clusters to be computed for %u columns: please update tour call: for qeustions contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", nclusters, ncolumns, __FUNCTION__, __FILE__, __LINE__); */
  /*     return  false; */
  /*   } */
  /* } */
  //assert(mask);
  assert(clusterid);
  assert(cdata);
  //assert(cmask);
  assert( (transpose == 0) || (transpose == 1) );

  const t_float localConfig__empty_value = (mayUse_zeroAs_emptySign_inComputations) ? 0 : T_FLOAT_MAX;

  // assert(false); 
  // FIXME[usage]: may we dorp updating the "cmask" attribute ... eg, would it instead be 'e3nough' to investigate the "clsuterid" list? (oekseth, 06. nvo. 2016).

#if(optimize_parallel_2dLoop_useFast == 1)
#error "Add and test the effoect of parllel wrappers for this ... ie, after we have completed hte 'ordinary' perofmrance-tests"
#endif

  if(transpose == 0) {
    //t_float *arrOf_valuesFor_clusterId = allocate_1d_list_float(ncolumns, T_FLOAT_MAX);
    t_float *arrOf_valuesFor_clusterId_tmpInSorting = allocate_1d_list_float(nrows, 0);
    if(isTo_copmuteResults_optimal) {
      t_float **arrOf_valuesFor_clusterId = arrOf_valuesFor_clusterId_global;
      //! Note: we 'allow' the arrOf_valuesFor_clusterId to be 'rpvodied' a glboal emmorya-llcoation in roder to redcue the exeuctione-time.
      if(arrOf_valuesFor_clusterId_global == NULL) {
	const t_float empty_value = 0;
	arrOf_valuesFor_clusterId = allocate_2d_list_float(nclusters, nrows, empty_value);
	for(uint i = 0; i < nclusters; i++) {
	  for(uint k = 0; k < nrows; k++) {
	    arrOf_valuesFor_clusterId[i][k] = T_FLOAT_MAX;
	  }
	}	
      }
      uint *arrOf_valuesFor_clusterId_sizes = allocate_1d_list_uint(nclusters, 0);
      //memset(arrOf_valuesFor_clusterId_sizes, 0, sizeof(uint)*nclusters); //! ie, reset.
      //! Iterate:
      

      //printf("ncolumns=%u, nrows=%u, nclusters=%u, at %s:%d\n", ncolumns, nrows, nclusters, __FILE__, __LINE__);
      for(uint j = 0; j < ncolumns; j++) {
	//! First reset the counters:
	memset(arrOf_valuesFor_clusterId_sizes, 0, sizeof(t_float)*nclusters);
	if(needTo_useMask_evaluation == false) {
	  for(uint k = 0; k < nrows; k++) {
	    const uint i = clusterid[k]; 	
	    if(i == UINT_MAX) {continue;} //! ie, as the clsuter-id is then not set for the vertex.
	    const t_float value = data[k][j];
	    arrOf_valuesFor_clusterId[i][arrOf_valuesFor_clusterId_sizes[i]] = value; arrOf_valuesFor_clusterId_sizes[i]++;
	  }
	} else if(mask == NULL) {
	  for(uint k = 0; k < nrows; k++) {
	    const uint i = clusterid[k]; 	  
	    if(i == UINT_MAX) {continue;} //! ie, as the clsuter-id is then not set for the vertex.
	    assert(i != UINT_MAX);
	    assert(i < nclusters);
	    const t_float value = data[k][j];
	    if(isOf_interest(value)) {
	      const uint pos = arrOf_valuesFor_clusterId_sizes[i];
	      assert(pos < nrows);
	      arrOf_valuesFor_clusterId[i][pos] = value; arrOf_valuesFor_clusterId_sizes[i]++;
	    }
	  }
	} else {
	  for(uint k = 0; k < nrows; k++) {
	    const uint i = clusterid[k];
	    assert(i < nclusters);
	    if(i == UINT_MAX) {continue;} //! ie, as the clsuter-id is then not set for the vertex.
	    if((mask[k][j] != 0) ) {
	      const t_float value = data[k][j];
	      arrOf_valuesFor_clusterId[i][arrOf_valuesFor_clusterId_sizes[i]] = value; arrOf_valuesFor_clusterId_sizes[i]++;	      
	    }
	  }
	}	
	//! Then compute teh medians:
	//! Note: givent he expected high cost of the sorting-oepration in the "get_median(..)" call we do not us intristincts in [”elow]
	assert(transpose == 0); //! ie, an assumption used wr.t [”elow] asserts.
	assert(j < ncolumns);
	for(uint i = 0; i < nclusters; i++) {
	  assert(cdata[i]); //! ie, as we expect latter to be allocated.
	  if(cmask) {assert(cmask[i]);} //! ie, as we expect latter to be allocated.
	  if(arrOf_valuesFor_clusterId_sizes[i] > 0) {
	    cdata[i][j] = get_median(arrOf_valuesFor_clusterId[i], arrOf_valuesFor_clusterId_tmpInSorting, arrOf_valuesFor_clusterId_sizes[i]); //count,cache);
	    if(cmask) {cmask[i][j] = 1;}
	  } else {
	    //if(mayUse_zeroAs_emptySign_inComputations) {
	    cdata[i][j] = localConfig__empty_value;
	    /* } else { */
	    /*   //printf("updates[%u][%u], at %s:%d\n", i, j, __FILE__, __LINE__); */
	    /*   cdata[i][j] = localConfig__empty_value; */
	    /* } */
	    if(cmask) {
	      //cdata[i][j] = 0.;
	      cmask[i][j] = 0;
	    }
	  }
	}
      }
      //! De-allcoate locally reserved values:
      if(arrOf_valuesFor_clusterId_global == NULL) {
	free_2d_list_float(&arrOf_valuesFor_clusterId, nclusters);
      }
      free_1d_list_uint(&arrOf_valuesFor_clusterId_sizes);
    } else { //! then we use the slow strategy
      const uint nelements = (transpose==0) ? nrows : ncolumns; t_float* cache = (t_float*)malloc(nelements*sizeof(double));
      for(uint i = 0; i < nclusters; i++) {
	for(uint j = 0; j < ncolumns; j++) {
	  int count = 0;
	  {
	    for(uint k = 0; k < nrows; k++) {
	      //! Update the cluster-memberships assicated to cluster="i":
	      assert(k < max(ncolumns, nrows));
	      if( (i == clusterid[k]) ) {
		if( (!mask || mask[k][j]) ) {
		  const t_float value = data[k][j];
		  if(value != T_FLOAT_MAX) {
		    assert(value != T_FLOAT_MIN_ABS);
		    cache[count] = value;
		    // cache[count] = data[k][j];
		  count++;
		  }
		}
	      }
	    }
	  }
	  // assert(count == arrOf_counts.get_current_pos());
	  if(count>0) {
	    cdata[j][i] = get_median(cache, arrOf_valuesFor_clusterId_tmpInSorting, count);
	    //cdata[i][j] = graphAlgorithms_distance::get_median(arrOf_counts); //count,cache);
	    if(cmask) {cmask[i][j] = 1;}
	  } else {
	    cdata[i][j] = localConfig__empty_value;
	    /* if(mayUse_zeroAs_emptySign_inComputations) { */
	    /*   cdata[i][j] = 0; */
	    /* } else {cdata[i][j] = T_FLOAT_MAX;} */
	    if(cmask) {
	      //cdata[i][j] = 0.;
	      cmask[i][j] = 0;
	    }
	  }
	  // //! Remvoe the data whcih was inserted:
	  // arrOf_counts.clear_data();
	}
      }
      free(cache);
    }
    free_1d_list_float(&arrOf_valuesFor_clusterId_tmpInSorting);

    // assert(false); // FXIME: remove.

  } else { //! Then transpose is copmuted.
    t_float *arrOf_valuesFor_clusterId_tmpInSorting = allocate_1d_list_float(ncolumns, 0);
    if(isTo_copmuteResults_optimal) {
      t_float **arrOf_valuesFor_clusterId = arrOf_valuesFor_clusterId_global;
      //! Note: we 'allow' the arrOf_valuesFor_clusterId to be 'rpvodied' a glboal emmorya-llcoation in roder to redcue the exeuctione-time.
      if(arrOf_valuesFor_clusterId_global == NULL) {
	const t_float empty_value = 0;
	arrOf_valuesFor_clusterId = allocate_2d_list_float(nclusters, ncolumns, empty_value);
	for(uint i = 0; i < nclusters; i++) {
	  for(uint k = 0; k < ncolumns; k++) {arrOf_valuesFor_clusterId[i][k] = T_FLOAT_MAX;}}
      }
      uint *arrOf_valuesFor_clusterId_sizes = allocate_1d_list_uint(nclusters, 0);
      memset(arrOf_valuesFor_clusterId_sizes, 0, sizeof(uint)*nclusters); //! ie, reset.

      for(uint j = 0; j < nrows; j++) {
	memset(arrOf_valuesFor_clusterId_sizes, 0, sizeof(uint)*nclusters); //! ie, reset.
	int count = 0;
	const uint i = clusterid[j];
	assert(i < nclusters);
	for(uint k = 0; k < ncolumns; k++) {
	  const t_float value = data[j][k];
	  if(isOf_interest(value)) {
	    if(!mask || (mask[j][k] != 0) ) {
	      const t_float value = data[k][j];
	      if(isOf_interest(value)) {
		arrOf_valuesFor_clusterId[i][arrOf_valuesFor_clusterId_sizes[i]] = value; arrOf_valuesFor_clusterId_sizes[i]++;
	      }
	      
	    }
	  }
	}
	//assert(count == arrOf_counts.get_current_pos());
	for(uint i = 0; i < nclusters; i++) {
	  if(arrOf_valuesFor_clusterId_sizes[i] > 0) {
	    cdata[i][j] = get_median(arrOf_valuesFor_clusterId[i], arrOf_valuesFor_clusterId_tmpInSorting, arrOf_valuesFor_clusterId_sizes[i]); //count,cache);
	    if(cmask) {cmask[i][j] = 1;}
	  } else {
	    cdata[i][j] = localConfig__empty_value;
	      //if(mayUse_zeroAs_emptySign_inComputations) {cdata[i][j] = 0;} else {cdata[i][j] = T_FLOAT_MAX;;}
	    if(cmask) {
	      //cdata[i][j] = 0.;
	      cmask[i][j] = 0;
	    }
	  }
	}
      }
          //! De-allcoate locally reserved values:
      if(arrOf_valuesFor_clusterId_global == NULL) {
	free_2d_list_float(&arrOf_valuesFor_clusterId, nclusters);
      }
      free_1d_list_uint(&arrOf_valuesFor_clusterId_sizes);
    } else { //! thenw e sue the 'slow' strategy:
      const uint nelements = (transpose==0) ? nrows : ncolumns; t_float* cache = (t_float*)malloc(nelements*sizeof(double));
      // printf("nrows=%u, nclusters=%u, at %s:%d\n", nrows, nclusters, __FILE__, __LINE__);
      for(uint i = 0; i < nclusters; i++) {
	for(uint j = 0; j < nrows; j++) {
	  int count = 0;
	  for(uint k = 0; k < ncolumns; k++) {
	    if(i==clusterid[k] && (!mask || mask[j][k]) ) {
	      const t_float value = data[j][k];
	      if(isOf_interest(value)) {
		assert(value != T_FLOAT_MIN_ABS);
		cache[count] = value;
		//arrOf_counts.push(value);
		//cache[count] = data[j][k];
		count++;
	      }
	    }
	  }
	  //assert(count == arrOf_counts.get_current_pos());
	  if(count>0) {
	    cdata[j][i] = get_median(cache, arrOf_valuesFor_clusterId_tmpInSorting, count);
	    if(cmask) {cmask[j][i] = 1;}
	  } else {	  
	    cdata[j][i] = localConfig__empty_value;
	      //if(mayUse_zeroAs_emptySign_inComputations) {cdata[j][i] = 0;} else {cdata[j][i] = T_FLOAT_MAX;}
	    if(cmask) {cmask[j][i] = 0;}
	  }
	  // //! Remvoe the data whcih was inserted:
	  // arrOf_counts.clear_data();
	}
      }
      free(cache);
    }    
    free_1d_list_float(&arrOf_valuesFor_clusterId_tmpInSorting);
  }
  // //! De-allcoate locally reserved memory:
  // arrOf_counts.free_memory();

  return true; // ie, as we asusme the operaiton was a success
}
 
/* ********************************************************************* */

/**
   @remarks a wrapper-method to compute/calculcate the cluster centroids, given to which cluster each element belongs. Depending on the argument method, the centroid is defined as either the mean or the median for each dimension over all elements belonging to a cluster.
   @remarks a subset of the parameters are described below:
   # "cdata":  Result: this array contains the cluster centroids.
   # "cmask": Result: This array shows which data values of are missing for each centroid. If cmask[i][j]==0, then cdata[i][j] is missing. A data value is missing for a centroid ifall corresponding data values of the cluster members are missing.
   # "method": Input: 
   - For method=='a', the centroid is defined as the mean over all elements belonging to a cluster for each dimension.
   - For method=='m', the centroid is defined as the median over all elements belonging to a cluster for each dimension.
 **/
int getclustercentroids__extensiveInputParams(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp, uint transpose, char method, const bool isTo_copmuteResults_optimal, const bool mayUse_zeroAs_emptySign_inComputations, const bool needTo_useMask_evaluation) {

 switch(method) 
  { case 'm':
    { const uint nelements = (transpose == 0) ? nrows : ncolumns;
      getclustermedians__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid, cdata, cmask, cmask_tmp, transpose, isTo_copmuteResults_optimal, NULL, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
      return 1;
    }
    case 'a':
      { getclustermeans__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, clusterid,  cdata, cmask, cmask_tmp, transpose, isTo_copmuteResults_optimal, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
      return 1;
    }
  }
  return 0;
}

/* ********************************************************************* */

static void getclustermedoids_slow(const uint nclusters, const uint nelements, t_float** distance, uint clusterid[], uint centroids[], t_float errors[]) {
  for(uint j = 0; j < nclusters; j++) errors[j] = T_FLOAT_MAX;
  for(uint i = 0; i < nelements; i++) {
    const uint j = clusterid[i];
    t_float d = 0.0;
    for(uint k = 0; k < nelements; k++) {
      if( (i == k) || (clusterid[k] != j) ) continue; //! then it is either a self-comparison of not part of the given clsuter.
      //! Update the distance to the other vertices in the same cluster.
      d += ( (i < k) ? distance[k][i] : distance[i][k]); //! then we sume the distances between the centrodi and the orther vertides.
      if(d > errors[j]) break;
    }
    if(d < errors[j]) { //! then the vertex is closer than any of the other centroids, ie, update.
      errors[j] = d;
      centroids[j] = i;
    }
  }
}
static void getclustermedoids_fast(const uint nclusters, const uint nelements, t_float** distance, uint clusterid[], uint centroids[], t_float errors[], uint **arrOf_clusterBuckets_tmp) { 
  assert(arrOf_clusterBuckets_tmp);
  //  const uint cnt_cells = nclusters*nelements;
  //for(uint i = 0; i < cnt_cells; i++) {arrOf_clusterBuckets_tmp[0][i] = UINT_MAX;}

  for(uint j = 0; j < nclusters; j++) errors[j] = T_FLOAT_MAX;
  /**
     //! Note: an hypoteitcal improvement of this function is to:
     step(1): identify the unique vertices in each cluster: constrct a list "uint **arrOf_clusterBuckets", an approach with |nelements| time-cost;
     step(2): extend 'the naive procedure' with an outer for-loop 'to idenitfy a given cluster-id':
     
     From above we observe that the perofrmance-increase is |nclusters|: 
     -- To examplify, we for a list of 100 'unqiue clusters' may manage to reduce the exeuction-time by a factor of 100x, ie, as there is 'no empty branch-instructions' nor 'any empty "clusterid[k]" comparisons'.
   **/
  //! Step(1): collect clusters of vertices:
  const uint empty_0 = 0;
  uint *arrOf_clusterBuckets_tmp_size = allocate_1d_list_uint(nclusters, empty_0);
  for(uint i = 0; i < nelements; i++) {
    const uint j = clusterid[i];
    assert(j != UINT_MAX);
    //printf("j=%u < %u, at %s:%d\n", j, nclusters, __FILE__, __LINE__);
    assert_possibleOverhead(j < nclusters);
    const uint pos = arrOf_clusterBuckets_tmp_size[j];
    arrOf_clusterBuckets_tmp[j][pos] = i;
    arrOf_clusterBuckets_tmp_size[j]++;
  }
  
  //! Step(2): iterate through the clusters:
  for(uint j = 0; j < nclusters; j++) {
    for(uint i_tmp = 0; i_tmp < arrOf_clusterBuckets_tmp_size[j]; i_tmp++) {
      const uint i = arrOf_clusterBuckets_tmp[j][i_tmp]; //! ie, a vertex in the cluster.
      t_float d = 0.0;


      //! Note: as we are not itnerested in a self-comparison, we sepeate our iteration into two for-loops: corresponds to "if( (i == k) || (clusterid[k] != j) ) continue;"
      //! Note(2): as the elements are inserted into teh clsuter-id iaw. their 'internal id', the [below] seperation of the for-loops ensure that the implict constraint "d += ( (i < k) ? distance[k][i] : distance[i][k]);" always holds (ie, withouth the need for explcit correctness-testing).
      //! Note(3): we do ntoe xplictly appluy/wrtie intristincts in [below] given the possiblity of non-sequential memory-access in the arrOf_clusterBuckets_tmp[j] list.
      // FIXME[article]: ... we observe that the use of itnristinitcs 'forces' progrogrammers to write better code, eg, wrt. the seperation of 'if-else-clauses' as seen in [”elow], a seperation which results in an optmizaiton-degree of ...??..

      // FIXME[jc]: may you validate correctness of [”elow] lgocis, ie, when comapred to the 'naive' impelmetnaiton?
	
      for(uint k_tmp = 0; k_tmp < i_tmp; k_tmp++) {
	const uint k = arrOf_clusterBuckets_tmp[j][k_tmp]; //! ie, a vertex in the cluster.
	//! Update the distance to the other vertices in the same cluster.
	d += distance[k][i]; //! then we sume the distances between the centrodi and the orther vertides.
	if(d > errors[j]) break;
      }
      for(uint k_tmp = i_tmp+1; k_tmp < arrOf_clusterBuckets_tmp_size[j]; k_tmp++) {
	const uint k = arrOf_clusterBuckets_tmp[j][k_tmp]; //! ie, a vertex in the cluster.
	//! Update the distance to the other vertices in the same cluster.
	d += distance[i][k]; //! then we sume the distances between the centrodi and the orther vertides.
	if(d > errors[j]) break;
      }


      if(d < errors[j]) { //! then the vertex is closer than any of the other centroids, ie, update.
	errors[j] = d;
	centroids[j] = i;
      }
    }
  }
  free_1d_list_uint(&arrOf_clusterBuckets_tmp_size);
}

/**
   @brief idnetify the centroids of each cluster.
   @param <nclusters> The number of clusters.
   @param <nelements> The total number of elements.
   @param <distance> Number of rows is nelements, number of columns is equal to the row number: the distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
   @param <clusterid> The cluster number to which each element belongs.
   @param <centroids> The index of the element that functions as the centroid for each cluster.
   @param <errors> The within-cluster sum of distances between the items and the cluster centroid.
   @param <isTo_useOptimizedVersion> which is included for debug-purposes to investigate time-effects.
   @remarks 
   - idea: identify the sum of distances to all members in a given cluster: for each clsuter 'allcoate' the centorid which has the minimum/lowest 'sum of distance to other members' (in the same cluster).
   - this function calculates the cluster centroids, given to which cluster each element belongs. The centroid is defined as the element with the smallest sum of distances to the other elements.
 **/
void getclustermedoids__extensiveInputParams(const uint nclusters, const uint nelements, t_float** distance, uint clusterid[], uint centroids[], t_float errors[], const bool isTo_useOptimizedVersion, uint **arrOf_clusterBuckets_tmp) {
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  // FIXME: write a comparison of this funciton with the other distance-metric-funcitons (eg, "Manhatten-distance" and "Euclidian" .... and use both test-data-sets and datasets inferred from our to-be-written litterat-ure-database ... trying to idneityf differnet real-life use-cases.
  // FIXME: consider to improve the algorithmic complexity of [below], ie, given the 'potential' worst-case of [”elow] ... where the code may have a duadratic time-complexity "n*n"
  if(isTo_useOptimizedVersion) {getclustermedoids_fast(nclusters, nelements, distance, clusterid, centroids, errors, arrOf_clusterBuckets_tmp);}
  else {getclustermedoids_slow(nclusters, nelements, distance, clusterid, centroids, errors);}
}

/* ********************************************************************* */

/**
   @brief compute the kmeans clustering of the vertices.
   @remarks ...
   @remarks a subset of the parameters are described below:
   # "dist": specifies/identifies the distance-metric to be used (in the clustering). Defines which distance measure is used, as given by the table:
   - dist=='e': Euclidean distance
   - dist=='b': City-block distance
   - dist=='c': correlation
   - dist=='a': absolute value of the correlation
   - dist=='u': uncentered correlation
   - dist=='x': absolute uncentered correlation
   - dist=='s': Spearman's rank correlation
   - dist=='k': Kendall's tau
   - For other values of dist, the default (Euclidean distance) is used.
   # "npass": the maximal number of iteratives before the algorithm/procedure is 'forced' to converge. The number of times clustering is performed. Clustering is performed npass times, each time starting from a different (random) initial assignment of  genes to clusters. The clustering solution with the lowest within-cluster sum of distances is chosen. Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
   # "clusterid": The cluster number to which a gene or microarray was assigned. Ifnpass==0, then on input clusterid contains the initial clustering assignment from which the clustering algorithm starts. On output, it contains the clustering solution that was found.
   # "error": The sum of distances to the cluster center of each item in the optimal k-means clustering solution that was found.
   # "":
 **/
int kmeans_or_kmedian__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float weight[],  const uint npass, t_float** cdata, char **cmask, uint clusterid[], t_float* error, s_kt_randomGenerator_vector_t *obj_rand__init, uint counts[], uint mapping[], const bool isTo_use_mean, const bool isTo_copmuteResults_optimal, t_float **matrix2d_vertexTo_clusterId) {
  
  
  // FIXME: move [”elow] to the inptu-calling-fucntion
  //s_kt_randomGenerator_vector_t *obj_rand__init = NULL; 
  s_kt_randomGenerator_vector_t *obj_rand = obj_rand__init;  //! ie, the default assumption.
  s_kt_randomGenerator_vector_t __obj_local;
  s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(nrows, ncolumns, data);
  if( (obj_rand__init == NULL) || (obj_rand__init->nelements == 0) ) { //! then we allocate a 'new' object:
    const e_kt_randomGenerator_type_t typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame;
    __obj_local = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nclusters, ncolumns, typeOf_randomNess, /*isTo_initIntoIncrementalBlocks*/false, &obj_matrix_input); //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
    obj_rand = &__obj_local; //! ie, a simple copy-operation.
  }
  assert(obj_rand);

  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton
  // FIXME: write a use-case where "npass=0", ie, where where "clusterid" holds a set of 'pre-assumed' clusder-ids.
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  // FIXME: in order to assess/evaluate 'non-equilibrium' in cluster-assignment .... consider to comptue the "average" and "exepctation" and "STD" between each "npass" (ie, between each radnom assigment of centroid-points) ... eg, using the toala "total" (distance-measure-sum) as 'comparison'. .... and wrt. the latter try to describe/suggest how "non-covergence" (ie, "non-ideal sollutions") may be used to idnetify/describe ideal features, eg, in searches for researhc-articles. To exampliy a set of observations: (a) there is 'no end' to the applicaiton limits/use-cases of "network analsysis" in (b) the understanding of "litterature-researhc-networks" and (c) there is no software-tools to support the latter use-cases. If the latter assumptions/assertions hold, then we 'have identifed' a new research-domain in ....??... Of interest is to automate the latter proceudre/assertion, ie, where we ...??...  
  // printf("corrMetric_prior.metric_id = %u, at %s:%d\n", metric_id, __FILE__, __LINE__);
  const bool transpose = false; //! hiwhc is used iot. reduce complexity of our work (oekseth, 06. des. 2016).
  const uint nelements = (transpose == 0) ? nrows : ncolumns;
  assert(obj_rand->nelements > 0);
  assert_possibleOverhead(nelements <= obj_rand->nelements);
  //printf("elements=%u, at %s:%d\n", nelements, __FILE__, __LINE__);
  //! Note: in [below] we set "ndata" to the number of features to compare: if we compare 'each row', then there are "ncolumns" to compare, ie, which examplains why we 'only' perform a 'suqared investigation' in the inner dat-loop (ie, when calling eihter the "metric" directly of implictly through our "compute_allAgainstAll_distanceMetric(..)" function.
  const uint ndata = (transpose == 0) ? ncolumns : nrows;
  *error = T_FLOAT_MAX;
  uint ifound = 1;
  uint ipass = 0; //! the number of iterations/'passes' in the iterative algorithm: should be less than the user-defined "npass"
  /* Set the metric function as indicated by dist */
  
  //  printf("## npass=%u, at %s:%d\n", npass, __FILE__, __LINE__);

  // t_float (*metric_each)(config_nonRank_each) = setmetric__correlationComparison__oneToMany(metric_id, weight, mask1, mask2, isTo_useImplictMask);  

  // FIXME: make use of [”elow] when comptuign the distance-matrices 
  // FIXME[article]: update our aritlce-test wrt. the latter ... which is combined with mayUse_zeroAs_emptySign_inComputations in order to not include/add extra aritmetic operations for testing the FLT_MAx emtpy-value ... is an example of an optmizaiton-stragy where we 'make use of' the properties assicated to the "0" value (ie, that all muliplications and summations wrt. a "0" distance will be ingored, ie, an imoplict/indirrect use of a mask-proeprty.
  assert(data);
  const bool needTo_useMask_evaluation__input = matrixMakesUSeOf_mask__noSSE(nrows, ncolumns, data, data, mask, mask, transpose, /*iterationIndex_2=*/UINT_MAX);
  //const bool needTo_useMask_evaluation = matrixMakesUSeOf_mask__noSSE(nrows, ncolumns, data, data, mask, mask, transpose, /*iterationIndex_2=*/UINT_MAX);
  //fprintf(stderr, "needTo_useMask_evaluation='%u', at %s:%d\n", needTo_useMask_evaluation, __FILE__, __LINE__);


  /* Set the metric function as indicated by dist */
  //t_float (*metric) (uint, t_float**, t_float**, char**, char**, const t_float[], uint, uint, uint) = graphAlgorithms_distance::setmetric(dist);
  t_float (*metric_each) (config_nonRank_each_t) = setmetric__correlationComparison__each(metric_id, weight, mask, mask, /*needTo_useMask_evaluation=*/true); //! where the latter is set to "true" as teh centordi-vertices 'by defualt' results in a 'mask = true' case.


  //! We use optmized comptuations if a user has requested so, ie, to simplify 'back-comparison' (with previosu releases/code-approaches).
  // FIXME[jc]: the [”elow] cotnraitn 'relates directly to our rank-algorithm' ... ie, please valdiate that both our 'naive' and 'the itrnsitnictc opertimized alogirthm' does not include T_FLOAT_MAX values.

  // assert(false); 
  // FIXME: cosndier idneitfying cases wehre '0' may be safely used wrt. mask-evalaution ... and then update our ....??....
  s_kt_correlationMetric_t obj_metric; 
  obj_metric.metric_id = metric_id;   obj_metric.typeOf_correlationPreStep = typeOf_correlationPreStep;
  const bool mayUse_zeroAs_emptySign_inComputations = ( needTo_useMask_evaluation__input && isTo_copmuteResults_optimal && mayReplace_maskWith__zero__s_kt_correlationMetric_t(obj_metric)); //! ie, if we do not use 'sorted ranks' then a zero-element will 'by defneiton' be ignored, ie, then 'ne need to clutter the view by isng one extra data-structure'.

  // printf("mayUse_zeroAs_emptySign_inComputations=%u, at %s:%d\n", mayUse_zeroAs_emptySign_inComputations, __FILE__, __LINE__);
  
  
  struct s_correlationType_kendall_partialPreCompute_kendall obj_kendallCompute;
  setTo_empty__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);

  const bool isTo_useOptimizedMetricFor_kendall = isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(metric_id);
  if(isTo_useOptimizedMetricFor_kendall) { //(dist == 'k') ) {
    assert(transpose == 0); //! ie, to simplify our code.

    //! Initiate the "s_correlationType_kendall_partialPreCompute_kendall" data-structure:
    //! Note In the 'itnation' we comptue the ranks for the set of rows, ie, may take some seconds for large matrices.
    init__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute, nrows, ncolumns, data, mask, /*needTo_useMask_evaluation=*/needTo_useMask_evaluation__input); 
  }



  //! Note: we choose to 'merge' [below] alternatives given the alternaitve complexity-overhead to write seperate matrix-comptue-implemetnaitosn for "!mask1 && mask2" and "mask1 && !mask2" (oekseth, 06. june 2016).
  const bool needTo_useMask_evaluation_inDistanceComptuation = (mayUse_zeroAs_emptySign_inComputations == false);
  //  const bool needTo_useMask_evaluation_inDistanceComptuation = (true && mayUse_zeroAs_emptySign_inComputations);
  //  const bool needTo_useMask_evaluation_inDistanceComptuation = (needTo_useMask_evaluation && mayUse_zeroAs_emptySign_inComputations);

  /* t_float **distanceMatrix_tmp = NULL;  */
  /* if(isTo_copmuteResults_optimal && !isTo_use_kendall__e_kt_correlationFunction(metric_id)) { //(dist != 'k') ) { */
  /* //if(isTo_copmuteResults_optimal && (dist != 'k')) { */
  /*   assert(transpose == 0); //! ie, to compute data efficently. */
  /*   distanceMatrix_tmp = allocate_2d_list_float(nrows, nclusters, /\*default-value=*\/0); //! where the latter 'dimensions' are due to our expectaiton that "metric(ndata,data,cdata, ... )" will be called  (i,e in the latter variable-order). */
  /* } */

  /* We save the clustering solution periodically and check ifit reappears */
  uint *arrOf_saved = allocate_1d_list_uint(nelements, UINT_MAX);

  t_float **cmask_tmp = NULL;
  //  if(cmask) 
  {
    const uint ndata = (transpose==0) ? ncolumns : nrows;
    if(transpose == 0) {
      cmask_tmp = allocate_2d_list_float(nclusters, ndata, 1);
      // for(uint i = 0; i < nclusters; i++) {
      // 	for(uint j = 0; j < ndata; j++) {
      // 	  cmask_tmp[i][j] = cmask[i][j];
      // 	}
      // }
    } else {
      cmask_tmp = allocate_2d_list_float(nclusters, ndata, 1);
      // for(uint i = 0; i < nclusters; i++) {
      // 	for(uint j = 0; j < cols; j++) {
      // 	  cmask_tmp[i][j] = cmask[i][j];
      // 	}
      // }
    }
  }
  assert(obj_rand);
  const bool isToUse__randomziaitonInFirstIteration = isToUse__randomziaitonInFirstIteration__s_kt_randomGenerator_vector_t(obj_rand);

  //printf("(k-means)\tstart-iteraiont, at %s:%d\n", __FILE__, __LINE__);
      
  //! Iterateivly update the suggestions/clusters:
  do { 
    t_float total = T_FLOAT_MAX;
    uint counter = 0;
    uint period = 10;

    /* Perform the EM algorithm. First, randomly assign elements to clusters. */
    //! Note: if there is only one iteration, then we assume the user has a 'default' assumption of the clusters.
    // FIXME: write an algorithm-update where we try to use a 'semi-random' assigmenet, ie, by usiong existing knowledge describing ....??... 

    // FIXME[time]: what is the time-cost of [below]? <-- consider to pre-compute "npass" such clusters <-- given the possible size, test/evaluate if 'it is possible wrt. exeuciton-time' to load from a binary file (eg, a binary fiel pre-comptued at compile-time). <-- seems like we only need max(nclusters, nelements) memroy, ie, no need for a binary file 
#if(configure_isTo_useLog__local == 1)
    start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_randomAssign); //! ie, intiate the object.
#endif
    //! ...................
    // printf("radnom-change, at %s:%d\n", __FILE__, __LINE__);
    if(npass != 0) {
      if( (ipass > 0) || (isToUse__randomziaitonInFirstIteration == true) ) {
	// FIXME[aritlce]: udpate our artilce wrt. [below]. <-- fixme[jc]: may you evlauate/introspect upon 'how sigicnat these are'?
	//!
	//! Re-compute randomness::
	//printf("\t\tget-randomness, at %s:%d\n", __FILE__, __LINE__);
	randomassign__speicficType__math_generateDistributions(obj_rand);	
	//printf("\t\tok:\tget-randomness, at %s:%d\n", __FILE__, __LINE__);
    //#include "kt_clusterAlg_fixed__stub_randomziationWhenAllVerticesEndSUpInSame.c"
      } else {//! else we asusme the cluster-dids are pre-allocated (oekseth, 06. jan. 2017).
	assert_possibleOverhead(nelements <= obj_rand->nelements);
	for(uint i = 0; i < nelements; i++) {
	  const uint cluster_id = obj_rand->mapOf_columnToRandomScore[i];
	  assert(cluster_id != UINT_MAX);
	  assert(cluster_id <= nclusters); //! ie, to simplify debugging
	  assert(cluster_id < nclusters);
	}
      }
    }
    if(clusterid[0] == UINT_MAX) { //! ie, then assign an 'intal' assignemtn:
      assert(nclusters > 1);
      assert(obj_rand->mapOf_columnToRandomScore != NULL);
      for(uint j = 0; j < nelements; j++) {clusterid[j] = obj_rand->mapOf_columnToRandomScore[j];}
    }
    //! ...................
#if(configure_isTo_useLog__local == 1)
    end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_randomAssign); //! ie, intiate the object.
#endif

    //! Intalize, frist wrt. a 'general reset', and then update the coutns wrt. the number of members in each cluster:
    for(uint i = 0; i < nclusters; i++) counts[i] = 0;
    assert(obj_rand->mapOf_columnToRandomScore);
    assert(obj_rand->nelements == nelements);
    for(uint i = 0; i < nelements; i++) {
      const uint cluster_id = obj_rand->mapOf_columnToRandomScore[i];
      assert(cluster_id != UINT_MAX);
      assert(cluster_id <= nclusters); //! ie, to simplify debugging
      assert(cluster_id < nclusters);
      counts[cluster_id]++;
    }
    if(false) {
      printf("current=[");
      for(uint j = 0; j < nelements; j++) {printf("%u, ", clusterid[j]);} //! ie, update the container 'which is not reset in each while-loop'.
      printf("], at %s:%d\n", __FILE__, __LINE__);
    }
    //! Iterativly suggest new cluster-vertex-centroids, re-allocate the vertices to 'closer' clusters, and then continue 'the wile-lopp' until there is no longer an improvment in the 'total distance to cluster-vertex-centroids':
    /* Start the loop */
    while(true) {
      // printf("(new-loop), at %s:%d\n", __FILE__, __LINE__);
#if(configure_isTo_useLog__local == 1)
      start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_findCenter); //! ie, intiate the object.
#endif

      //! Regularily save teh resutls: for each 'period' in the iteration save the cluster-ids:
      if(counter % period == 0) {/* Save the current cluster assignments */
	//assert(tclusterid[i] >= 0);
	for(uint i = 0; i < nelements; i++) arrOf_saved[i] = obj_rand->mapOf_columnToRandomScore[i];
        if(period < INT_MAX / 2) period *= 2;
      }
      //! Intalise this 'run':
      const t_float previous = total;
      total = 0.0; counter++;

	/* assert(false); // FIXEMT: 'set' [”elow] praemter from a to-be-included internal cofnigruation-funciton. */
	/* const bool needTo_useMask_evaluation = true; */

      //!
      //! Validate that the randomzied scores are inide the expected threshold:
      assert(obj_rand->mapOf_columnToRandomScore);
      assert(obj_rand->mapOf_columnToRandomScore[0] != UINT_MAX);
      assert(obj_rand->mapOf_columnToRandomScore[0] < nclusters);

      //! Identify the 'center-vertex' of each cluster.
      // FIXME[memory]: investigat ethe time-signficance of [below] operaiton.
      if(isTo_use_mean) {
	assert(obj_rand->mapOf_columnToRandomScore);
	getclustermeans__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, obj_rand->mapOf_columnToRandomScore, cdata, cmask, cmask_tmp, transpose, isTo_copmuteResults_optimal, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation__input);
      } else {
	// FIXME: first test teh relative improtance of [”elow] call ... and thereafter use the const bool isTo_copmuteResults_optimal=true, t_float **arrOf_valuesFor_clusterId_global params in [”elow] ... for an 10,000*10,000*180 the difference is 37.63 x
	assert(obj_rand->mapOf_columnToRandomScore);
	getclustermedians__extensiveInputParams(nclusters, nrows, ncolumns, data, mask, obj_rand->mapOf_columnToRandomScore, cdata, cmask, cmask_tmp, transpose, isTo_copmuteResults_optimal, /*arrOf_valuesFor_clusterId_global=*/NULL, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation__input);
      }
      bool needTo_useMask_evaluation__outer = false;
      if(!needTo_useMask_evaluation__input) {
	if(transpose == false) {
	  needTo_useMask_evaluation__outer = matrixMakesUSeOf_mask__noSSE(nclusters, ncolumns, cdata, cdata, NULL, NULL, false, /*iterationIndex_2=*/UINT_MAX);
	} else {
	  needTo_useMask_evaluation__outer = matrixMakesUSeOf_mask__noSSE(nrows, nclusters, cdata, cdata, NULL, NULL, false, /*iterationIndex_2=*/UINT_MAX);
	}
      }
      const bool needTo_useMask_evaluation = (needTo_useMask_evaluation__outer && needTo_useMask_evaluation__input);
#if(configure_isTo_useLog__local == 1)
      end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_findCenter); //! ie, intiate the object.
      start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_findAllDistances); //! ie, intiate the object.
#endif


      // FIXME: boefre startying on [below] ... validate that we'll managwe a perofrmance-improvement .... ie, comapre cost of [ªbove] with [”elow] ... 

      //! Iterate throguh all the vertices, ie, to idetnify the 'cluster-assigment with minmal error':
      // FIXME[memory]: evluate the time-cost of [below] ... and (a) consider to 'pre-compute' the distance-matrix before start of the clsuter-operation, and (b) if the latter has a high time-cost then support 'parallelism' (in our "graphAlgorithms_distance"). <-- for large matrices the datset is expected to have 'an insigificant time-cost', which 'impleis' that hte 'major time-cost' in the clsutering-approach si to ....??...


      { //! Thn either use the [ªbov€] computed valeus, or an comptue 'dsitnaces in the loop':

#if(optimize_parallel_2dLoop_useFast == 1)
#error "Add and test the effoect of parllel wrappers for this ... ie, after we have completed hte 'ordinary' perofmrance-tests"
#endif
#if(optimize_parallel_useNonSharedMemory_MPI == 1)
  #error "Update this code-chunk with new/updated code" 
#endif
	
	// printf(" at %s:%d\n", __FILE__, __LINE__);
	assert(obj_rand->mapOf_columnToRandomScore);
	for(uint i = 0; i < nelements; i++) {
	  /* Calculate the distances */
	  
	  const uint k = obj_rand->mapOf_columnToRandomScore[i];
	  //printf("\t\t v[%u] has clust[%u] w/count=%u, at %s:%d\n", i, k, counts[k], __FILE__, __LINE__);
	  if(counts[k]==1) continue; //! ie, as the cluster then may not be 'imrpvoed' wrt. the centroid-vertex (in a given cluster).
	  /* No reassignment if that would lead to an empty cluster */
	  /* Treat the present cluster as a special case */
	  //! Idnetify the distance between the centroid-vertex and the vertex:
	  assert(i < nelements); assert(k < nelements); //! and if this assertion fails, then update our allcoation-rotuine wrt. our "distanceMatrix_preComputed".

	  // if(cmask) {assert(false);} // FIXME: add support for this case ... eg, by using an 'implcit mask-strategy' for "kendall"
	  char *mask2_vector = (!cmask) ? NULL : cmask[k];

	  const e_cmp_masksAre_used_t masksAre_used = (needTo_useMask_evaluation_inDistanceComptuation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
	  // printf("masksAre_used=%u, at %s:%d\n", masksAre_used, __FILE__, __LINE__);
	  s_allAgainstAll_config_t config_metric; set_metaMatrix(&config_metric, /*isTo_init=*/true, mask, cmask, weight, transpose, masksAre_used, nrows, ncolumns);

	  t_float distance =   T_FLOAT_MAX;
	  assert(transpose == false);
	  if(isTo_copmuteResults_optimal) {
	    distance = kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, cdata, /*index1=*/i, /*index2=*/k, config_metric, metric_each,
					(isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);
	  } else  {
	    if(metric_id == e_kt_correlationFunction_groupOf_minkowski_cityblock) {
	      distance = __kt_cityblock_slow(ncolumns, data, cdata, NULL, NULL, NULL, i, k, transpose);
	    } else if(metric_id == e_kt_correlationFunction_groupOf_minkowski_euclid) {
	      distance = __kt_euclid_slow(ncolumns, data, cdata, NULL, NULL, NULL, i, k, transpose);
	    } else {
	      assert(false); // TODO: add support for this.
	    }
	  }
	  //! --------------------------------------------------------------------------------------------------------
	  //! 
	  //! Then make  use [ªbove] inferred distance:
	  // FIXME: add parallel wrappers for [”elow].
	  //! Iterate through all the clusters: at max "n/2" vertices/clusters to evalaute:
	  for(uint j = 0; j < nclusters; j++) {
	    if(j==k) continue; //! then the centroid-id is the same as the 'original'/'default' vertex-centrodi relationship/cluster-assignment.
	    // FIXME[article]: is it correct to use "j" in [below] call? ... or should it be "tclusterid[j]"? <-- if [below] is wrong, then (a) update [below], (b) update our 'updated' "cluster.c" and (c) 'make a note' in our researhc-artilce that  the code has been improved wrt. quality (ie, bug-fixes): the bug results in clsuters being wrongly comptued, ie, current use-cases 'which has used this software' may need to re-compute their calculatation.s
	    const uint centroid_vertex = j;
	    // FIXME: validate correctness of [below] VS [ªbove].
	    //const uint centroid_vertex = clusterid[j];
	    //fprintf(stderr, "i=%u, nelements=%u, centroid_vertex=%u, nclusters=%u, at %s%d\n", i, nelements, centroid_vertex, nclusters, __FILE__, __LINE__);
	    //assert(i < nelements); assert(centroid_vertex < nelements); //! and if this assertion fails, then update our allcoation-rotuine wrt. our "distanceMatrix_preComputed".

	    // if(cmask) {assert(false);} // FIXME: add support for this case ... eg, by using an 'implcit mask-strategy' for "kendall"
	    char *mask2_vector = (!cmask) ? NULL : cmask[centroid_vertex];

	    const uint index1 = i; const uint index2 = centroid_vertex;
	    //if(index1 != index2) 
	    {
	      
	      t_float tdistance = T_FLOAT_MAX;
	      assert(transpose == false);
	      if(isTo_copmuteResults_optimal) {
		tdistance =   kt_compare__each(metric_id, typeOf_correlationPreStep, nrows, ncolumns, data, cdata, index1, index2, config_metric, metric_each,
					       (isTo_useOptimizedMetricFor_kendall == false) ? NULL : &obj_kendallCompute);
	      } else  {
		if(metric_id == e_kt_correlationFunction_groupOf_minkowski_cityblock) {
		  tdistance = __kt_cityblock_slow(ncolumns, data, cdata, NULL, NULL, NULL, index1, index2, transpose);
		} else if(metric_id == e_kt_correlationFunction_groupOf_minkowski_euclid) {
		  tdistance = __kt_euclid_slow(ncolumns, data, cdata, NULL, NULL, NULL, index1, index2, transpose);
		} else {
		  assert(false); // TODO: add support for this.
		}
	      }
	      

	      if(false && !transpose) { 
		t_float max_val_1 = T_FLOAT_MIN_ABS; for(uint i = 0; i < ncolumns; i++) {if(isOf_interest(data[index1][i])) {max_val_1 = macro_max(data[index1][i], max_val_1);}}
		t_float max_val_2 = T_FLOAT_MIN_ABS; for(uint i = 0; i < ncolumns; i++) {if(isOf_interest(data[index2][i])) {max_val_2 = macro_max(data[index2][i], max_val_2);}}
		t_float min_val_1 = T_FLOAT_MAX; for(uint i = 0; i < ncolumns; i++) {if(isOf_interest(data[index1][i])) {min_val_1 = macro_min(data[index1][i], min_val_1);}}
		t_float min_val_2 = T_FLOAT_MAX; for(uint i = 0; i < ncolumns; i++) {if(isOf_interest(data[index2][i])) {min_val_2 = macro_min(data[index2][i], min_val_2);}}
		//if(tdistance != T_FLOAT_MAX)
		{
		  printf("[%u][c(%u)] = '%f', given max=[%f, %f], min=[%f, %f], at %s:%d\n", index1, index2, tdistance, max_val_1, max_val_2, min_val_1, min_val_2, __FILE__, __LINE__);
		}
	      }
	      //! Then make  use [ªbove] inferred distance:	      
	      if((distance == T_FLOAT_MAX) || ( (tdistance < distance) && (tdistance != T_FLOAT_MAX) ) ) { //! then we have a better/improved cluster-assignment: update the cluster-id for vertex "i":
		// FIXME: instead of [”elow] use _mm_storeu_ps
		if(tdistance != T_FLOAT_MAX) {
		  distance = tdistance;
		  assert(counts[obj_rand->mapOf_columnToRandomScore[i]] > 0); //! ie, to avoid a negative count, where th elatter woudl be a cricla bug wrt. "uint" dat-atype.
		  counts[obj_rand->mapOf_columnToRandomScore[i]]--; //! ie, as the vertex is 'remvoved' from the current/given cluster.
		  obj_rand->mapOf_columnToRandomScore[i] = j;
		  counts[j]++;
		}
	      }
	      if(matrix2d_vertexTo_clusterId != NULL) { //! then we update the distance between teh cluster-and-the-vertex:
		assert_possibleOverhead(matrix2d_vertexTo_clusterId[/*vertex=*/i]);
		const uint icluster = index2;
		matrix2d_vertexTo_clusterId[/*vertex=*/i][/*cluster-id=*/icluster] = macro_min(matrix2d_vertexTo_clusterId[/*vertex=*/i][/*cluster-id=*/icluster], tdistance);
	      }
	    }
	  }
	  //! Update the 'sum of distances' with the shortest/best distance found between each vertex and cluster-centroid-vertex:
	  if( (distance != T_FLOAT_MAX) && (distance != T_FLOAT_MIN_ABS) ) {
	    assert(distance != T_FLOAT_MAX);
	    total += distance;
	  }
	} //! and at this exeuction-point we asusme the algorithm has covnerged.
      }
#if(configure_isTo_useLog__local == 1)
      end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_findAllDistances); //! ie, intiate the object.
#endif
      //! Note: in [”elow] the ">="  is used as a 'safeguard' to handle rounding-errors in some maches, ie, instead of the 'actual' "=" operator.
      if(total >= previous) break; //! then there has not been an improvment, ie, we assuem that the algorithm has 'locally' coverged.

      //! If the result is identical as the previous 'run', then the algorithm has converged:
      // TODO: investigate which cases where [below] condition is needed, ie, givne [ªbove] 'codnition'.
      uint count = 0;
      for(uint i = 0; i < nelements; i++) {
	assert(obj_rand->mapOf_columnToRandomScore[i] >= 0);
        if(arrOf_saved[i] != obj_rand->mapOf_columnToRandomScore[i]) break;
	count++;
      }
      if(count == nelements) break; /* Identical solution found; break out of this loop */
    } //! and at this exeuction-point one of the [ªbove] 'break-points' has been met.
    //! We expect the algorithm to ahve covnerged if the user has requested only one 'pass'/iteration:
    if(npass <= 1) { *error = total; break; }

    for(uint i = 0; i < nclusters; i++) mapping[i] = UINT_MAX;
    uint count = 0;
    //! Iterate through each of the elements/vertices: test/investigate if the clusterr-vertex-centroid-id has changed
    // printf(" at %s:%d\n", __FILE__, __LINE__);
    for(uint i = 0; i < nelements; i++) {
      count++;
      const uint j = obj_rand->mapOf_columnToRandomScore[i]; 
      const uint k = obj_rand->mapOf_columnToRandomScore[i]; //! the 'non-reset' cluster-id.
      assert(k != UINT_MAX);
      //printf("clusterId[%u]=%u, npasS=%u, mapping=%p, at %s:%d\n", k, j, npass, mapping, __FILE__, __LINE__);
      if( //(k != UINT_MAX) && 
	  (mapping[k] == UINT_MAX) ) {
	mapping[k] = j; //! ie, as the mapping has then not been set/allocated for this cluster.
      } else if( //(k != UINT_MAX) && 
		 (mapping[k] != j) ) { //! then the cluster-id has changed (since last iteration).
	if(total < *error) {
	  ifound = 1;
          *error = total;
	  // FIXME: may [below] be correctly placed after this for-loop?
	  // FIXME: in [below] use memset(..) ... ie, compare the difference .... use _mm_storeu_ps(..)
	  // printf("\t\t update clusterid, at %s:%d\n", __FILE__, __LINE__);
          for(uint j = 0; j < nelements; j++) {clusterid[j] = obj_rand->mapOf_columnToRandomScore[j];} //! ie, update the container 'which is not reset in each while-loop'.
        }
	//if(true) {printf("in-break, at  [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);}
        break;
      } /* else { */
      /* 	clusterid[i] = tclusterid[j]; //! ie, ase t a mapping. */
      /* } */
    }
    //if(false) {printf("after-break, at  [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);}
    //! Update for the next iteration:
    if(count == nelements) ifound++; /* break statement not encountered */
  } while (++ipass < npass);
  
  //! ------------------------------------------------------------------------------------------------
  { //! De-allcoate locally reserved memory:
    free_1d_list_uint(&arrOf_saved);
    // if(distanceMatrix_preComputed) {
    //   math_generateDistribution__freedatamask(/*nelements=*/ndata, distanceMatrix_preComputed, mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);    
    // }
    /* if(distanceMatrix_tmp) { */
    /*   free_2d_list_float(&distanceMatrix_tmp); */
    /* } */
    if(cmask_tmp) {
      free_2d_list_float(&cmask_tmp, nclusters);
    }
    if(isTo_copmuteResults_optimal && isTo_use_kendall__e_kt_correlationFunction(metric_id)) { //(dist == 'k') ) {
    //if(isTo_copmuteResults_optimal && (dist == 'k') ) {
      free__s_correlationType_kendall_partialPreCompute_kendall(&obj_kendallCompute);
    } //! else we assuemt he "s_correlationType_kendall_partialPreCompute_kendall" data-structure is empty.
  }
  //! ------------------------------------------------------------------------------------------------
  //! De-allcoate and return:
  if( (obj_rand__init == NULL) || (obj_rand__init->nelements == 0) ) { //! then we de-allocate the object:
    // if(obj_rand__init == NULL) { //! then we allocate a 'new' object:
    free__s_kt_randomGenerator_vector_t(obj_rand); obj_rand = NULL;
  } else {assert(obj_rand == obj_rand__init);}

  //printf("ifound=%d, at %s:%d\n", ifound, __FILE__, __LINE__);
  //! @return the status.
  return ifound;
}

/* ********************************************************************* */
/* ********************************************************************* */


/**
   @brief compute the clustering of the vertices, using a a 'fixed number of clsuters'.
   @remarks The kcluster routine performs k-means or k-median clustering on a given set of elements, using the specified distance measure. The number of clusters is given by the user. Multiple passes are being made to find the optimal clustering solution, each time starting from a different initial clustering.
   @remarks a subset of the parameters are described below:
   # "dist": specifies/identifies the distance-metric to be used (in the clustering). Defines which distance measure is used, as given by the table:
   - dist=='e': Euclidean distance
   - dist=='b': City-block distance
   - dist=='c': correlation
   - dist=='a': absolute value of the correlation
   - dist=='u': uncentered correlation
   - dist=='x': absolute uncentered correlation
   - dist=='s': Spearman's rank correlation
   - dist=='k': Kendall's tau
   - For other values of dist, the default (Euclidean distance) is used.
   # "npass": the maximal number of iteratives before the algorithm/procedure is 'forced' to converge. The number of times clustering is performed. Clustering is performed npass times, each time starting from a different (random) initial assignment of  genes to clusters. The clustering solution with the lowest within-cluster sum of distances is chosen. Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
   # "clusterid": The cluster number to which a gene or microarray was assigned. Ifnpass==0, then on input clusterid contains the initial clustering assignment from which the clustering algorithm starts. On output, it contains the clustering solution that was found.
   # "error": The sum of distances to the cluster center of each item in the optimal k-means clustering solution that was found.
   # "iffound": The number of times the optimal clustering solution was found. The value of ifound is at least 1; its maximum value is npass. If the number of clusters is larger than the number of elements being clustered, *ifound is set to 0 as an error code. Ifa memory allocation error occurs, *ifound is set to -1.
   # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
   # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
**/
void kcluster__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nclusters, uint nrows, uint ncolumns, t_float** data, char** mask, t_float weight[], uint transpose, uint npass, char method,  uint clusterid[], t_float* error, uint* ifound, const bool isTo_use_continousSTripsOf_memory, const bool isTo_invertMatrix_transposed, const bool isTo_useImplictMask, t_float **matrix2d_vertexTo_clusterId, s_kt_randomGenerator_vector_t *obj_rand__init) {


#if(configure_isTo_useLog__local == 1)
  start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMeans_all); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMeans_init); //! ie, intiate the object.
#endif

  const uint nelements = (transpose==0) ? nrows : ncolumns;
  const uint ndata = (transpose==0) ? ncolumns : nrows;
  {
    //const uint nelements = (transpose==0) ? nrows : ncolumns;
    if(nelements < nclusters) { 
      *ifound = 0;
      assert(false); //! ie, as we did not expect this input, ie, more clusters asked for than elements available
      return;
    }
    *ifound = UINT_MAX; //! ie, initilaze.
  }

  bool needTo_useMask_evaluation = true;
  if(isTo_useImplictMask && mask) {
    assert(data);
    needTo_useMask_evaluation = matrixMakesUSeOf_mask(nrows, ncolumns, data, NULL, mask, NULL, transpose, /*iterationIndex_2=*/UINT_MAX);
    if(needTo_useMask_evaluation) {
      s_mask_api_config_t config_mask; setTo_empty__s_mask_api_config(&config_mask);
      applyImplicitMask__updateData__mask_api(data, mask, nrows, ncolumns, config_mask);
    }
    // needTo_useMask_evaluation = graphAlgorithms_distance::update_distanceMatrix_withImplicitMask_fromMaskMatrix(nrows, ncolumns, data, mask);
  }

  // if(mask) { //! 'aovid' the need to use the "mask", ie, use "T_FLOAT_MAX" instead <-- 
  //   for(uint i = 0; i < nrows; i++) {
  //     for(uint j = 0; j < ncolumns; j++) {
  // 	// FIXME: use  _mm_storeu_ps(..) 
  // 	if(mask[i][j] == 0) {
  // 	  data[i][j] = T_FLOAT_MAX; //! ie, to 'aovid the use' of the mask property.
  // 	}
  //     }
  //   }
  // }


  t_float **distanceMatrix_intermediate = NULL; char **mask_dummy = NULL;  //! Then we a transposed matrix out of the input-tranpose
  char **mask_allocated = NULL;
  if((transpose == 1) && isTo_invertMatrix_transposed) {
    //! Allocate memory, ie, using the 'inverse settings' as [above]:
    maskAllocate__makedatamask(/*nrows=*/ncolumns, /*ncols=*/nrows, &distanceMatrix_intermediate, &mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);	  
    //! TODO: consider to remove [below] asserts ... adn then add the parameters to [below] funciotn:
    if(mask) {mask_dummy = allocate_2d_list_char(ncolumns, nrows, 0);     mask_allocated = mask_dummy;}
    //! Compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    matrix__transpose__compute_transposedMatrix(/*nrows=*/nrows, /*ncols=*/ncolumns, /*data1=*/data, mask, distanceMatrix_intermediate, mask_dummy, /*use_SSE=*/true);
    //! Update, ie, 'invert' the variables:
    transpose = 0; const uint tmp = nrows; 
    nrows = ncolumns; ncolumns = tmp;    
    t_float **tmp_matrix = data; data = distanceMatrix_intermediate; distanceMatrix_intermediate = tmp_matrix;
    char **tmp_mask = mask; mask = mask_dummy; mask_dummy = tmp_mask;
  }

  const uint default_value_uint = 0;

  //! Allocate space for the list to: contain the number of elements in each cluster, which is needed to check for empty clusters.
  uint* counts = allocate_1d_list_uint(nclusters, default_value_uint);
  /* Find out ifthe user specified an initial clustering */
  //uint *tclusterid = clusterid;
  uint *mapping = NULL;
  if(npass > 1) {
    // tclusterid = allocate_1d_list_uint(nelements, default_value_uint);
    mapping = allocate_1d_list_uint(nclusters, default_value_uint);
  }

  /* Allocate space to store the centroid data */
  t_float** cdata = NULL;   char **cmask = NULL;
  uint ok = 0;
  
  if(transpose==0) {
    //fprintf(stderr, "allocates cmaks with dims=[%u, %u], where ncolumns=%u, at %s:%d\n", nclusters, ndata, ncolumns, __FILE__, __LINE__);
    ok = maskAllocate__makedatamask(nclusters, ndata, &cdata, &cmask, isTo_use_continousSTripsOf_memory);
  }  else { ok = maskAllocate__makedatamask(ndata, nclusters, &cdata, &cmask, isTo_use_continousSTripsOf_memory); }
  if(!ok)
  { free(counts);
    if(npass>1)
    { 
      //free(tclusterid);
      free(mapping);
      return;
    }
  }

#if(configure_isTo_useLog__local == 1)
  end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMeans_init); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_all); //! ie, intiate the object.
#endif


  //! -------------------------------------
  //! The cluster-operation:
  assert(transpose == false); //! ie, to reduce compleixty in our calls (oekseth, 06. des. 2016).
  if(method=='m') {    
    *ifound = kmeans_or_kmedian__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncolumns, data, /*mask=*/NULL, weight,
						      //transpose, 
						      npass,  cdata, cmask, clusterid, error,
						      obj_rand__init,
						      //tclusterid, 
						      counts, mapping, /*isTo_use_mean=*/true, /*isTo_preComputeDistanceMatrix_ifSpeedIncreases=*/true, matrix2d_vertexTo_clusterId);
  } else {
    *ifound = kmeans_or_kmedian__extensiveInputParams(metric_id, typeOf_correlationPreStep, nclusters, nrows, ncolumns, data, /*mask=*/NULL, weight,
						      //transpose, 
						      npass, cdata, cmask, clusterid, error,
						      obj_rand__init,
						      //tclusterid, 
						      counts, mapping, /*isTo_use_mean=*/false, /*isTo_preComputeDistanceMatrix_ifSpeedIncreases=*/true, matrix2d_vertexTo_clusterId);
  }
#if(configure_isTo_useLog__local == 1)
  end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_all); //! ie, intiate the object.
#endif

  //! -------------------------------------
  { //! De-allcoate locally reserved memory:
    if(npass > 1) {
      assert(mapping);
      //assert(tclusterid);
      //assert(tclusterid != clusterid);
      assert(mapping);
      free_1d_list_uint(&mapping);
      //free_1d_list_uint(&tclusterid);
    }
    
    if(transpose==0) maskAllocate__freedatamask(nclusters, cdata, cmask, isTo_use_continousSTripsOf_memory);
    else maskAllocate__freedatamask(ndata, cdata, cmask, isTo_use_continousSTripsOf_memory);
    assert(counts);
    free_1d_list_uint(&counts);
  }

  //! Ivnestigate if we are to 'invert' the results:
  if((transpose == 1) && isTo_invertMatrix_transposed) {
    assert(distanceMatrix_intermediate);
    //! 'Back-translate' the distance-matrix, ie, compute a transposed matrix, eg, to imrpove the efficency of cluster-comptuations.
    assert(data != distanceMatrix_intermediate); //! Note: at this exeuction-point we assume that "data" is the transpsoied matrix, ie, given our precivous/earlier call to [below] function
    matrix__transpose__compute_transposedMatrix(/*nrows=*/nrows, /*ncols=*/ncolumns, /*data1=*/data, NULL, distanceMatrix_intermediate, NULL, /*use_SSE=*/true);    

    t_float **tmp_matrix = data; data = distanceMatrix_intermediate; distanceMatrix_intermediate = tmp_matrix;
    char **tmp_mask = mask; mask = mask_dummy; mask_dummy = tmp_mask;    
    if(mask_allocated != mask_dummy) {
      free_2d_list_char(&mask_allocated, ncolumns);
    }
    maskAllocate__freedatamask(/*nelements=*/ncolumns, distanceMatrix_intermediate, mask_dummy, /*isTo_use_continousSTripsOf_memory=*/true);    
    
  }

#if(configure_isTo_useLog__local == 1)
  end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMeans_all); //! ie, intiate the object.
  writeOut_object_clusterC(&logFor_kMeans, stdout, e_logCluster_typeOf_writeOut_kMeans, /*stringOf_description=*/NULL); //! ie, intiate the object.
#endif

}


/* *********************************************************************** */

/**
   @brief ...
   @remarks "For some data sets there may be more than one medoid, as with medians. A common application of the medoid is the k-medoids clustering algorithm, which is similar to the k-means algorithm but works when a mean or centroid is not definable. This algorithm basically works as follows. First, a set of medoids is chosen at random. Second, the distances to the other points are computed. Third, data are clustered according to the medoid they are most similar to. Fourth, the medoid set is optimized via an iterative process" ["https://en.wikipedia.org/wiki/Medoid"]. Key-poitns:
   - the algorithm "is more robust to noise and outliers as compared to k-means because it minimizes a sum of pairwise dissimilarities instead of a sum of squared Euclidean distances" 
   ["https://en.wikipedia.org/wiki/K-medoids"]. The latter is due to the training-phase in the SOM: "because in the training phase weights of the whole neighborhood are moved in the same direction, similar items tend to excite adjacent neurons" ["https://en.wikipedia.org/wiki/Self-organizing_map"]. In breif, "SOM may be considered a nonlinear generalization of Principal components analysis (PCA)" ["https://en.wikipedia.org/wiki/Self-organizing_map"].
   - 
   @remarks from the documetnaiton:
   - The kmedoids routine performs k-medoids clustering on a given set of elements, using the distance matrix and the number of clusters passed by the user. 
   - Multiple passes are being made to find the optimal clustering solution, each time starting from a different initial clustering.
   @remarks a subset of the parameters are described below:
   # "nelements": The number of elements to be clustered.
   # "distmatrix":  The distance matrix. To save space, the distance matrix is given in the form of a ragged array. The distance matrix is symmetric and has zeros on the diagonal. See distancematrix for a description of the content.
   - number of rows is nelements, number of columns is equal to the row number
   # "npass": The number of times clustering is performed. 
   - Clustering is performed npass  times, each time starting from a different (random) initial assignment of genes to clusters. 
   - The clustering solution with the lowest within-cluster sum of distances is chosen.
   - Ifnpass==0, then the clustering algorithm will be run once, where the initial assignment of elements to clusters is taken from the clusterid array.
   # "clusterid": the cluster-identites assicated to each vertex
   - On input, ifnpass==0, then clusterid contains the initial clustering assignment from which the clustering algorithm starts; all numbers in clusterid should be between zero and nelements-1 inclusive. Ifnpass!=0, clusterid is ignored on input.
   - On output, clusterid contains the clustering solution that was found: clusterid contains the number of the cluster to which each item was assigned. On output, the number of a cluster is defined as the item number of the centroid of the cluster.
   # "error": The sum of distances to the cluster center of each item in the optimal k-medoids clustering solution that was found.
   # "iffound": If kmedoids is successful: the number of times the optimal clustering solution was found. 
   - The value of ifound is at least 1; its maximum value is npass. 
   - If the user requested more clusters than elements available, ifound is set to 0. If kmedoids fails due to a memory allocation error, ifound is set to -1.
 **/
void kmedoids__extensiveInputParams(uint nclusters, uint nelements, t_float** distmatrix, uint npass, uint clusterid[], uint *centroids__global, t_float* error, uint* ifound, t_float **matrix2d_vertexTo_clusterId, s_kt_randomGenerator_vector_t *obj_rand__init) {
//void kmedoids__extensiveInputParams(uint nclusters, uint nelements, t_float** distmatrix, uint npass, uint clusterid[], uint *centroids__global, t_float* error, uint* ifound, t_float **matrix2d_vertexTo_clusterId, uint *tclusterid__input) {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  // FIXME: consider to improve the algorithmic complexity of [below], ie, given the 'potential' worst-case of [”elow] ... where the code may have a duadratic time-complexity "n*n"
  // FIXME: compare this algorithm to the "kmeans_or_kmedian(...)" algorithm/function.

#if(configure_isTo_useLog__local == 1)
  start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMeans_all); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMeans_init); //! ie, intiate the object.
#endif


  // FIXME: move [”elow] to the inptu-calling-fucntion
  //s_kt_randomGenerator_vector_t *obj_rand__init = NULL; 
  s_kt_randomGenerator_vector_t *obj_rand = obj_rand__init;  //! ie, the default assumption.
  s_kt_randomGenerator_vector_t __obj_local;
  s_kt_matrix_base_t obj_matrix_input = initAndReturn__notAllocate__s_kt_matrix_base_t(nelements, nelements, distmatrix);
  if(obj_rand__init == NULL) { //! then we allocate a 'new' object:
    const e_kt_randomGenerator_type_t typeOf_randomNess = e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame;
    __obj_local = initAndReturn__s_kt_randomGenerator_vector_t(/*maxValue=*/nclusters, nelements, typeOf_randomNess, /*isTo_initIntoIncrementalBlocks*/false, &obj_matrix_input); //! where the latter is set iot. faiclaite the applicaiton/use of use-specifed init-array of scores.
#ifndef NDEBUG
    if(__obj_local.mapOf_columnToRandomScore) {
      //! Validatte that all valeus are 'set' to a score less than the "nclusters" max-value:
      for(uint i = 0; i < nelements; i++) {
	const uint cluster_id = __obj_local.mapOf_columnToRandomScore[i];
	if(cluster_id != UINT_MAX) {
	  assert(cluster_id != UINT_MAX);
	  assert(cluster_id < nclusters);
	}
      }
    }
#endif
    obj_rand = &__obj_local; //! ie, a simple data-copy-operation.
  }


  // FIXME: is the input-matrix always a 'squared' matrix, ie "nelements * nelements" ?
  // FIXME: suggest an approach to reduce the exueciton-time of this function.

  if(nelements < nclusters) {
    /* More clusters asked for than elements available */
    *ifound = 0;
    return;
  } 
  //! Intalise:
  *ifound = UINT_MAX;
  *error = T_FLOAT_MAX;

  /* We save the clustering solution periodically and check ifit reappears */
  const char empty_value = 0;
  uint *arrOf_saved = allocate_1d_list_uint(nelements, /*empty-value=*/empty_value);
  t_float *arrOf_errors = allocate_1d_list_float(nclusters, /*empty-value=*/empty_value); //T_FLOAT_MAX);
  for(uint i = 0; i < nelements; i++) {     arrOf_saved[i] = UINT_MAX;   }
  for(uint i = 0; i < nclusters; i++) {     arrOf_errors[i] = T_FLOAT_MAX;   }
  
  //fprintf(stderr, "# Starts meodi, at %s:%d\n", __FILE__, __LINE__);

  uint *centroids = centroids__global;
  if(centroids__global == NULL) {
    centroids = allocate_1d_list_uint(nclusters, /*empty-value=*/empty_value);
  } else {
    const uint nelements_minusOne = nelements - 1;
    if(nclusters < nelements) {
      for(uint j = 0; j < nelements; j++)  {
	const uint cluster_id = 0;
	centroids[cluster_id] = /*vertex_id=*/j; //! ie, a default intaiton.
	clusterid[j] = cluster_id; //! ie, assicated to the first clsuter.
      }
    }
    for(uint i = 0; i < nclusters; i++) {
      const uint vertex_id = macro_min(i, nelements_minusOne);
      centroids[i] = vertex_id; //! ie, a default intaiton.
      clusterid[vertex_id] = i; //! ie, a 'mapping between these'.
      //centroids[i] = empty_value;
    }
  }
  /* uint* tclusterid = centroids; */
  /* /\* Find out if the user specified an initial clustering *\/ */
  /* if(npass > 1) { */
  /*   if(tclusterid__input == NULL) { */
  /*     tclusterid = allocate_1d_list_uint(nelements, /\*empty-value=*\/empty_value); */
  /*   } else { */
  /*     tclusterid = tclusterid__input; */
  /*   } */
  /* } */

  const bool isTo_useOptimizedVersion = true; // FIXME: make this part of the function-params

  uint **arrOf_clusterBuckets_tmp = allocate_2d_list_uint(nclusters, nelements, empty_value);
  for(uint i = 0; i < nclusters; i++) {
    for(uint k = 0; k < nelements; k++) {arrOf_clusterBuckets_tmp[i][k] = UINT_MAX;}}

#if(configure_isTo_useLog__local == 1)
  end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMeans_init); //! ie, intiate the object.
  start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_all); //! ie, intiate the object.
#endif

  const bool isToUse__randomziaitonInFirstIteration = isToUse__randomziaitonInFirstIteration__s_kt_randomGenerator_vector_t(obj_rand);
  /* bool isToUse__randomziaitonInFirstIteration = (obj_rand->mapOf_columnToRandomScore[j] == UINT_MAX); */
  /* if(isToUse__randomziaitonInFirstIteration == false)  { //! then we investigate if values are set to differne scores: */
  /*   //! Add supprot for using a pre-specified vertex-ampping in k-means-and-k-median-appraoches: */
  /*   uint clusterId_max = 0; uint clusterId_min = UINT_MAX; */
  /*   for(uint j = 0; j < nelements; j++) { */
  /*     clusterId_max = macro_max(clusterId_max, obj_rand->mapOf_columnToRandomScore[j]); */
  /*     clusterId_min = macro_min(clusterId_min, obj_rand->mapOf_columnToRandomScore[j]); */
  /*   } //! ie, update the container 'which is not reset in each while-loop'. */
  /*   if(clusterId_max != clusterId_min) { //! then our 'random fucntion' has suggested all vertices to be 'in the same', ie, then 'explcit update our allcoation': */
  /*     isToUse__randomziaitonInFirstIteration = false; */
  /*   } */
  /* } */


  //! Iterateivly update the suggestions/clusters:
  uint ipass = 0;
  do { 
    t_float total = T_FLOAT_MAX;
    uint counter = 0;
    uint period = 10;


    // FIXME[time]: what is the time-cost of [below]? <-- consider to pre-compute "npass" such clusters <-- given the possible size, test/evaluate if 'it is possible wrt. exeuciton-time' to load from a binary file (eg, a binary fiel pre-comptued at compile-time). <-- seems like we only need max(nclusters, nelements) memroy, ie, no need for a binary file  <-- wrt. xeution-time we obseve that the "randomassign(..)" function has a high time-cost (when in isloation the latter is comapred to the Euclidean distance-metric).

#if(configure_isTo_useLog__local == 1)
    start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_randomAssign); //! ie, intiate the object.
#endif
    //! ...................
    if(npass != 0) {
      if( (ipass > 0) || (isToUse__randomziaitonInFirstIteration == true) ) {
	// math_generateDistribution__randomassign(nclusters, nelements, tclusterid);
	// FIXME[aritlce]: udpate our artilce wrt. [below]. <-- fixme[jc]: may you evlauate/introspect upon 'how sigicnat these are'?
	//!
	//! Re-compute randomness::
	randomassign__speicficType__math_generateDistributions(obj_rand);
#ifndef NDEBUG
	if(obj_rand->mapOf_columnToRandomScore) {
	  //! Validatte that all valeus are 'set' to a score less than the "nclusters" max-value:
	  for(uint i = 0; i < nelements; i++) {
	    const uint cluster_id = obj_rand->mapOf_columnToRandomScore[i];
	    if(cluster_id != UINT_MAX) {
	      assert(cluster_id != UINT_MAX);
	      assert(cluster_id < nclusters);
	    }
	  }
	}
#endif
	//#include "kt_clusterAlg_fixed__stub_randomziationWhenAllVerticesEndSUpInSame.c"
	//} else { //! else we asusme the cluster-dids are pre-allocated (oekseth, 06. jan. 2017).
      } else {//! else we asusme the cluster-dids are pre-allocated (oekseth, 06. jan. 2017).
	for(uint i = 0; i < nelements; i++) {
	  const uint cluster_id = obj_rand->mapOf_columnToRandomScore[i];
	  assert(cluster_id != UINT_MAX);
	  assert(cluster_id <= nclusters); //! ie, to simplify debugging
	  assert(cluster_id < nclusters);
	}
      }
    } else {
      for(uint i = 0; i < nelements; i++) {
	const uint cluster_id = obj_rand->mapOf_columnToRandomScore[i];
	assert(cluster_id != UINT_MAX);
	assert(cluster_id <= nclusters); //! ie, to simplify debugging
	assert(cluster_id < nclusters);
      }
    }
    //! ...................
#if(configure_isTo_useLog__local == 1)
    end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_randomAssign); //! ie, intiate the object.
#endif


    while(true) {
#if(configure_isTo_useLog__local == 1)
      start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_findCenter); //! ie, intiate the object.
#endif
      //! Regularily save teh resutls: for each 'period' in the iteration save the cluster-ids:
      if(counter % period == 0) {/* Save the current cluster assignments */
	//assert(tclusterid[i] >= 0);
	for(uint i = 0; i < nelements; i++) arrOf_saved[i] = obj_rand->mapOf_columnToRandomScore[i];
        if(period < INT_MAX / 2) period *= 2;
      }
      //! Intalise this 'run':
      const t_float previous = total;
      total = 0.0; counter++;

/* #ifndef NDEBUG */
/*       { //! Validate that a cenotrid-vertex is described only for one cluster: */
/* 	for(uint clust_id_in = 0; clust_id_in < nclusters; clust_id_in++) { */
/* 	  const uint vertex_id = centroids[clust_id_in]; */
/* 	  for(uint clust_id_out = 0; clust_id_out < nclusters; clust_id_out++) { */
/* 	    const uint vertex_id_out = centroids[clust_id_out]; */
/* 	    assert(vertex_id_out != UINT_MAX); */
/* 	    if(clust_id_out != clust_id_in) { */
/* 	      assert(vertex_id_out != vertex_id); //! ie, as we asusme a clsuter-vertex-centroid is only the centorid-vertex for one clsuter. */
/* 	    } */
/* 	  } */
/* 	} */
/*       } */
/* #endif */
      //!
      //! Validate that the randomzied scores are inide the expected threshold:
      assert(obj_rand->mapOf_columnToRandomScore);
      assert(obj_rand->mapOf_columnToRandomScore[0] != UINT_MAX);
      assert(obj_rand->mapOf_columnToRandomScore[0] < nclusters);
      //! Identify the 'center-vertex' of each cluster.
      //! Note: in [below] we try to idnetify the cenotrids/metoids which the vertices are most similar to.
      //assert(sizeof(t_float) == sizeof(type_t_float)); //! ie, as we expect this to hold [”elow]
      getclustermedoids__extensiveInputParams(nclusters, nelements, distmatrix, obj_rand->mapOf_columnToRandomScore, centroids, arrOf_errors, isTo_useOptimizedVersion, arrOf_clusterBuckets_tmp);

/* #ifndef NDEBUG */
/*       { //! Validate that a cenotrid-vertex is described only for one cluster: */
/* 	for(uint clust_id_in = 0; clust_id_in < nclusters; clust_id_in++) { */
/* 	  const uint vertex_id = centroids[clust_id_in]; */
/* 	  for(uint clust_id_out = 0; clust_id_out < nclusters; clust_id_out++) { */
/* 	    const uint vertex_id_out = centroids[clust_id_out]; */
/* 	    assert(vertex_id_out != UINT_MAX); */
/* 	    if(clust_id_out != clust_id_in) { */
/* 	      if((vertex_id_out != vertex_id); //! ie, as we asusme a clsuter-vertex-centroid is only the centorid-vertex for one clsuter. */
/* 	    } */
/* 	  } */
/* 	} */
/*       } */
/* #endif */

#if(configure_isTo_useLog__local == 1)
      end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_findCenter); //! ie, intiate the object.
      start_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_findAllDistances); //! ie, intiate the object.
#endif


      //! Iterate throguh all the vertices, ie, to idetnify the 'cluster-assigment with minmal error':
      for(uint i = 0; i < nelements; i++) {
	/* Find the closest cluster */
	//! Note: in contrast to our "kmeans_or_kmedian(...)" function we use the distance-matrix directly, ie, does not use an 'intermeidate' distance-metric.
	t_float distance = T_FLOAT_MAX;
        for(uint icluster = 0; icluster < nclusters; icluster++) {
          const uint j = centroids[icluster];	  
          if(i==j) { //! then the 'current vertex' is the centroid-vertex.
	    //! Note: we set the distance to '0' and then 'brak' the for-loop as 'the vertex j' is in the center-point of a given clsuter.
	    distance = 0.0;
	    // Update the vertex with the assicated cluster-id
            obj_rand->mapOf_columnToRandomScore[i] = icluster;
            break;
          } else {
	    const t_float tdistance = (i > j) ? distmatrix[i][j] : distmatrix[j][i];
	    if(tdistance < distance) { //! then we have a better/improved cluster-assignment: update the cluster-id for vertex "i":
	      // Update the vertex with the assicated cluster-id
	      distance = tdistance;
	      obj_rand->mapOf_columnToRandomScore[i] = icluster;
	    }
	    if(matrix2d_vertexTo_clusterId != NULL) { //! then we update the distance between teh cluster-and-the-vertex:
	      assert_possibleOverhead(matrix2d_vertexTo_clusterId[/*vertex=*/i]);
	      matrix2d_vertexTo_clusterId[/*vertex=*/i][/*cluster-id=*/icluster] = macro_min(matrix2d_vertexTo_clusterId[/*vertex=*/i][/*cluster-id=*/icluster], tdistance);
	    }
	  }
        }
	//! Update the 'sum of distances' with the shortest/best distance found between each vertex and cluster-centroid-vertex:
	//assert(distance != T_FLOAT_MAX);
	if(isOf_interest(distance)) {
	  total += distance;
	}
      } //! and at this exeuction-point we asusme the algorithm has covnerged.
#if(configure_isTo_useLog__local == 1)
      end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_findAllDistances); //! ie, intiate the object.
#endif

      //! Note: in [”elow] the ">="  is used as a 'safeguard' to handle rounding-errors in some maches, ie, instead of the 'actual' "=" operator.
      if(total >= previous) break; //! then there has not been an improvment, ie, we assuem that the algorithm has 'locally' coverged.

      uint count = 0;
      for(uint i = 0; i < nelements; i++) {
	assert(obj_rand->mapOf_columnToRandomScore[i] >= 0);
        if(arrOf_saved[i] != obj_rand->mapOf_columnToRandomScore[i]) break;
	count++;
      }
      if(count == nelements) break; /* Identical solution found; break out of this loop */
    } //! and at this exeuction-point one of the [ªbove] 'brak-points' has been met.

    uint count = 0;
    for(uint i = 0; i < nelements; i++) {
      count++;
      const uint cluster_vertex_id_this = clusterid[i];
      const uint cluster_vertex_id_centroid = centroids[obj_rand->mapOf_columnToRandomScore[i]];
      if(cluster_vertex_id_this != cluster_vertex_id_centroid) {
	if(total < *error) {
	  *ifound = 1;
          *error = total;
          /* Replace by the centroid in each cluster. */
	  // FIXME: why is [below] correct?
          for(uint j = 0; j < nelements; j++)  {
            clusterid[j] = centroids[obj_rand->mapOf_columnToRandomScore[j]];
	  }
        }
        break;
      }
    }
    //! Update for the next iteration:
    if(count == nelements) (*ifound)++; /* break statement not encountered */
  } while (++ipass < npass);

#if(configure_isTo_useLog__local == 1)
  end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMean_iteration_all); //! ie, intiate the object.
#endif


  /* //! De-allcoate locally reserved memory: */
  /* if(npass > 1) { */
  /*   if(tclusterid != tclusterid__input) { */
  /*     free_1d_list_uint(&tclusterid); */
  /*   } */
  /*   tclusterid = NULL; */
  /* } */
 ;
  free_1d_list_uint(&arrOf_saved);
  free_1d_list_float(&arrOf_errors);
  free_2d_list_uint(&arrOf_clusterBuckets_tmp, nclusters);
  assert(clusterid);
  if(clusterid) {
    uint cnt_nonPrinted = 0;
    uint cnt_inBoth = 0;
    //! Then we udpate 'this' to 'esnrue' that we map the vertices to the cluster-ids (and Not to the clsuter-centroid-vertices) (oekseth, 06. feb. 2017).
    for(uint j = 0; j < nelements; j++)  {
      uint clusterId_vertex = UINT_MAX;
      //! find the cluster-id:
      for(uint k = 0; k < nclusters; k++) {
	if(clusterid[j] == centroids[k]) {	  
	  // *Travfprintf(stderr, "vertex[%u] in cluster(%u), at %s:%d\n", j, k, __FILE__, __LINE__);
	  if(clusterId_vertex != UINT_MAX) {
	    if(cnt_inBoth < 5) {
	      if(false) {fprintf(stderr, "(status)\t result-ambiguities: vertex=%u is the centroid of both clsuter(%u) and cluster(%u): indicates non-optimzlaity wrt. your selected k-cluster-count. Therefore, suggest your try updating the clsuter-count (or use a difffernet algorithm supported by the hpLysis clsuter-analsys software. If reading the manual does Not help then pelase cotnact the senior develoepr at [oekseth@gamil.com].  Observaiton at [%s]:%s:%d\n", j, clusterId_vertex, k, __FUNCTION__, __FILE__, __LINE__);} // FIXME: consider instead moving this to the documetnaiton .... to avoiud all to many erorrs during bathc-copmptautiosn.
	    }
	    cnt_inBoth++;
	  }
	  //assert_possibleOverhead(clusterId_vertex == UINT_MAX); //! ie, as we expect 'this' to have been foudn only once.
	  clusterId_vertex = k; //! ie, the cluster-id.
	}
      }
      if(clusterId_vertex != UINT_MAX) {
	clusterid[j] = clusterId_vertex; //! ie, update
      } else { //! then the cluster-appraoch has Not correctly converged.
	if(cnt_nonPrinted < 5) { //! ie, to reduce the clsutter asiscated to 'such' error-messages
	  if(false) {fprintf(stderr, "(status:subset)\t result-ambiguities: vertex=%u/%u assicated to centroid(%u) was Not found to be part of any clusters. Therefore, suggest your try updating the clsuter-count (or use a difffernet algorithm supported by the hpLysis clsuter-analsys software. If reading the manual does Not help then pelase cotnact the senior develoepr at [oekseth@gamil.com].  Observaiton at [%s]:%s:%d\n", j, nelements, clusterid[j], __FUNCTION__, __FILE__, __LINE__);} // FIXME: consider instead moving this to the documetnaiton .... to avoiud all to many erorrs during bathc-copmptautiosn.
	}
	cnt_nonPrinted++;
	// TODO: cosndier dorpping[below]
	clusterid[j] = 0; //nclusters; //! ie, update, setting the vlaue to a 'not-found-cluster
      }
      //assert(clusterId_vertex != UINT_MAX); //! ie, as we expec thte clsuter-id to be known.
      //fprintf(stderr, "vertex[%u] in cluster(%u), at %s:%d\n", j, clusterId_vertex, __FILE__, __LINE__);
      //clusterid[j] = clusterId_vertex; //! ie, update

    }

  }
  if(centroids__global == NULL) {
    free_generic_type_1d(centroids); //delete [] centroids;
  }




#if(configure_isTo_useLog__local == 1)
  end_time_measurement_clusterC(&logFor_kMeans, e_log_clusterC_kMeans_all); //! ie, intiate the object.
  writeOut_object_clusterC(&logFor_kMeans, stdout, e_logCluster_typeOf_writeOut_kMeans, /*stringOf_description=*/NULL); //! ie, intiate the object.
#endif

  //! ------------------------------------------------------------------------------------------------
  //! De-allcoate and return:
  if(obj_rand__init == NULL) { //! then we allocate a 'new' object:
    free__s_kt_randomGenerator_vector_t(obj_rand); obj_rand = NULL;
  } else {assert(obj_rand == obj_rand__init);}


  //! @return the status.
  return;
}

