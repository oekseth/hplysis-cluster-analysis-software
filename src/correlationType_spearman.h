#ifndef correlationType_spearman_h
#define correlationType_spearman_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file correlationType_spearman
   @brief provide lgois for copmtuation of the Spearman correlation-metric
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "mask_api.h"
#include "correlation_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"
#include "correlation_rank_rowPair.h" //! which defines functions used for our "s_correlation_rank_rowPair"
#include "correlation_inCategory_delta.h"
#include "s_allAgainstAll_config.h"

//! A naive Spearman-implemetnaiton using the non-optmized "cluster.c" as a perofmrance-comparison-use-case.
t_float __kt_spearman_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose);

//! Comptue Spearmnas correlation-coefficent for pre-comptued ranks.
//! @return the correlation-socre.
//! @remarks the funciton shoudl be called 'driectly' if (a) there are no mask-properties and (b) there there are muliple calls where teh same rank-resutls are used (eg, when one index is correlated to muliple indexes).
t_float kt_spearman_forPreComputedRanks(const s_allAgainstAll_config_t self, const s_correlation_rank_rowPair_t obj_correlation);
  //! @brief comptue the spearman-corralation between two vertices.
  //! @remarks   The spearman routine calculates the Spearman distance between two rows or columns. The Spearman distance is defined as one minus the Spearman rank correlation.
  //!  @remarks the parameter of "weight" is ignored, but included for consistency with other distance measures.
//! @remarks Memory-access-pattern of this function is:  worst-case results in "4*n" extra/unneccesary cache-misses (due to look-ups in "mask[<dynamic>][fixed]" and "data[<dynamic>][fixed]"), otherwise a constant number of cache-misses;forNonTransposed_useFastImplementaiton
t_float kt_spearman(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose, const s_allAgainstAll_config_t self);


#endif //! EOF
