#ifndef __macroTypeOf_maskTo_use
#error "!!\t The required macro-viralbe was nto defined, i,e plase update your call"
#endif
{
  t_float negatives = 0; uint positives = 0;
  { //! Identify the total number of positive outcomes:    
#if(__macroTypeOf_maskTo_use == 0)
    const uint cnt_features = arrOf_truth_size;
#else 
    uint cnt_features = 0;
#endif
    for(int i = 0; i < arrOf_truth_size; i++) {
      if(
#if(__macroTypeOf_maskTo_use == 0)
	 true
#elif(__macroTypeOf_maskTo_use == 1) 
	 isOf_interest(arrOf_data[i])
#else
	 mask[i]
#endif
	 ) {
#if(__macroTypeOf_maskTo_use != 0) 
	cnt_features++;
#endif
	if(arrOf_truth[i]) {	
	  positives++;
	}
      }
    }
    negatives = (t_float)(cnt_features - positives);    
  }
  const t_float negatives_inverted = (negatives != 0) ? 1/negatives : 0;
  const t_float positives_inverted = (positives != 0) ? 1/positives : 0;

  //! Seperately investigate for each of the min-threshold-points:
  for(uint i = 0; i < arrOf_minThresholds_size; i++) {
    //! Infer sensitivity and specifity:
    t_float cnt_sens = 0;       t_float cnt_spec = 0;
    assert(arrOf_minThresholds);
    for(uint j = 0; j < arrOf_truth_size; j++) {
      if(
#if(__macroTypeOf_maskTo_use == 0)
	 true
#elif(__macroTypeOf_maskTo_use == 1) 
	 isOf_interest(arrOf_data[i])
#else
	 mask[i]
#endif
	 ) {
	const uint pred_values = (arrOf_data[j] > arrOf_minThresholds[i]) ? 1 : 0; //! ie, infer if the values is above or below the 'cut'.

	if(arrOf_truth[j] == 1) {
	  cnt_sens += pred_values;
	} else{cnt_spec += 1 - pred_values;}
      }
    }
    //! Update the result:
    arrOfResult__sens[i] = cnt_sens * (t_float)positives_inverted;
    arrOfResult__spec[i] = cnt_spec * negatives_inverted;
  }
}
//! Reset the macro-parameter:
#undef __macroTypeOf_maskTo_use
