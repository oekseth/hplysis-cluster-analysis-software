assert(isTo_transposeMatrix == false); // ie, to simplify our lgoics.

const uint size_inner = local_matrix->nrows;
assert(size_inner > 0);
  //! Intaite the result-opbject:
uint nxgrid_adjusted = (nxgrid/max_cntForests) + 1;
if(nxgrid_adjusted == 1) {
  fprintf(stderr, "!!\t How are we to correctly handle this case ... ie, 'shall' we then instead use djsijoint-forest-clustering? OBserviaont at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  assert(false); // TODO: thereafter update our "kt_matrix__cluster__stub__avgOrRank.c" .. and "kt_matrix__cluster__stub__avgOrRank.c"
  assert(false); // TODO: handle this case.
 }

assert(isTo_transposeMatrix == false); //! ie, to simplify our logics
s_kt_clusterAlg_SOM_resultObject_t obj_local_result; 
setTo_empty__s_kt_clusterAlg_SOM_resultObject_t(&obj_local_result, 
						  (isTo_transposeMatrix) ? local_matrix->ncols : local_matrix->nrows, 
						  nxgrid_adjusted, nygrid);




//!
//! The Call:
somcluster__extensiveInputParams(obj_metric.metric_id, obj_metric.typeOf_correlationPreStep, local_matrix->nrows, local_matrix->nrows, local_matrix->matrix, /*mask=*/NULL, local_matrix->weight, isTo_transposeMatrix, nxgrid_adjusted, nygrid, inittau, /*niter=*/npass, obj_local_result.celldata, obj_local_result.vertex_clusterid, /*config=*/NULL);


//!
//! Iterate through the vertices and udpate the membership:
#ifndef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
// start: -------- type of mapping-table:
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
const uint cnt_local = arrOf_result_vertexMembers_size;
const uint *mapOf_local = arrOf_result_vertexMembers;
#elif (__disjointConfig__useComplexInference__rowsDiffersFrom == 0)
const uint cnt_local = arrOf_result_vertexMembers_size;
const uint *mapOf_local = arrOf_result_vertexMembers;
#else
const uint cnt_local = arrOf_result_row_size;
const uint *mapOf_local = arrOf_result_row;
#endif
// finish: -------- type of mapping-table:
if(cnt_local > 0) {
  mergeResult__s_kt_clusterAlg_SOM_resultObject_t(obj_result, &obj_local_result, mapOf_local, /*previousCount__nxGrid=*/forest_id*nxgrid);
 }
#else //! then we 'make' a direct-copy of the object:
mergeResult__s_kt_clusterAlg_SOM_resultObject_t(obj_result, &obj_local_result, /*arrOf_result_vertexMembers=*/NULL, /*previousCount__nxGrid=*/0);
#endif
//!
//! De-allcoate:
#ifndef __localConfig__applyLogics_pathTo_codeChunk__isToNotUseMappingTable
// start: -------- type of mapping-table:
#ifndef __disjointConfig__useComplexInference__rowsDiffersFrom
if(arrOf_result_vertexMembers) {
  free_1d_list_uint(&arrOf_result_vertexMembers); arrOf_result_vertexMembers = NULL;
 }	
#elif (__disjointConfig__useComplexInference__rowsDiffersFrom == 0)
if(arrOf_result_vertexMembers) {
  free_1d_list_uint(&arrOf_result_vertexMembers); arrOf_result_vertexMembers = NULL;
 }	
#else
if(arrOf_result_row) {
  free_1d_list_uint(&arrOf_result_row); arrOf_result_row = NULL;
 }	
if(arrOf_result_col) {
  free_1d_list_uint(&arrOf_result_col); arrOf_result_col = NULL;
 }
#endif
// finish: -------- type of mapping-table:
#endif
free__s_kt_clusterAlg_SOM_resultObject_t(&obj_local_result);
