/* #ifndef metricMacro_shannon_h */
/* #define metricMacro_shannon_h */


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_shannon
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "shannon".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/


// FIXME: write perofmrance-teets and correnctess-tests for tehse cases/metrics

//! @return the correlations-core for: Kullback-Lebler
#define correlation_macros__distanceMeasures__shannon__KullbackLeibler(term1, term2) ({ \
  const t_float numerator   = term1; \
  const t_float denumerator = term2; \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(term1) == false); \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(term1) == false); \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(term2) == false); \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(term2) == false); \
  const t_float eqn = ( (term1 != 0) & (term2 != 0) ) ? term1 / term2 : 0; \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(eqn) == false); \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(eqn) == false); \
  const t_float val_log = (eqn != 0) ? mathLib_float_log_abs(eqn) : 0;	\
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(val_log) == false); \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(val_log) == false); \
  const t_float log_left = term1; \
  const t_float result =  log_left * val_log; \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result) == false); \
							      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result) == false); \
  result; })
#define correlation_macros__distanceMeasures__shannon__KullbackLeibler__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__shannon__KullbackLeibler__SSE(term1, term2) ({ \
  const VECTOR_FLOAT_TYPE numerator   = term1; \
  const VECTOR_FLOAT_TYPE denumerator = term2; \
  const VECTOR_FLOAT_TYPE eqn = VECTOR_FLOAT_DIV(term1, term2); \
  const VECTOR_FLOAT_TYPE val_log = VECTOR_FLOAT_LOG(VECTOR_FLOAT_abs(eqn)); \
  const VECTOR_FLOAT_TYPE log_left = term1; \
  VECTOR_FLOAT_TYPE result =  VECTOR_FLOAT_MUL(log_left, val_log);	\
  /*Handle dividye-by-zero-cases: */					\
  result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);		\
  result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);		\
  result; })

//! @return the correlations-core for: Jeffreys
#define correlation_macros__distanceMeasures__shannon__Jeffreys(term1, term2) ({ \
  const t_float numerator   = term1; \
  const t_float denumerator = term2; \
  const t_float eqn = ( (term1 != 0) & (term2 != 0) ) ? term1 / term2 : 0; \
  const t_float val_log = (eqn != 0) ? mathLib_float_log_abs(eqn) : 0;	\
  const t_float log_left = term1 - term2; \
  const t_float result =  log_left * val_log; \
  result; }) //! return.
#define correlation_macros__distanceMeasures__shannon__Jeffreys__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__shannon__Jeffreys__SSE(term1, term2) ({ \
  const VECTOR_FLOAT_TYPE numerator   = term1; \
  const VECTOR_FLOAT_TYPE denumerator = term2; \
  const VECTOR_FLOAT_TYPE eqn = VECTOR_FLOAT_DIV(term1, term2); \
  const VECTOR_FLOAT_TYPE val_log = VECTOR_FLOAT_LOG(VECTOR_FLOAT_abs(eqn)); \
  const VECTOR_FLOAT_TYPE log_left = VECTOR_FLOAT_SUB(term1, term2);	\
  VECTOR_FLOAT_TYPE result =  VECTOR_FLOAT_MUL(log_left, val_log); \
  /*Handle dividye-by-zero-cases: */					\
  result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);		\
  result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);		\
  result; }) //! return.

//! @return the correlations-core for: k-divergense
#define correlation_macros__distanceMeasures__shannon__kDivergence(term1, term2) ({ \
  const t_float numerator   = 2 * term1; \
  const t_float denumerator = term2 + term1; \
  const t_float eqn = ( (term1 != 0) & (term2 != 0) ) ? term1 / term2 : 0; \
  const t_float val_log = (eqn != 0) ? mathLib_float_log_abs(eqn) : 0;	\
  const t_float log_left = term1; \
  const t_float result =  log_left * val_log; \
  result; })
#define correlation_macros__distanceMeasures__shannon__kDivergence__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__shannon__kDivergence__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE numerator   = VECTOR_FLOAT_MUL(VECTOR_FLOAT_SET1(2), term1); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term2, term1); \
      const VECTOR_FLOAT_TYPE eqn = VECTOR_FLOAT_DIV(term1, term2);	\
      const VECTOR_FLOAT_TYPE val_log = VECTOR_FLOAT_LOG(VECTOR_FLOAT_abs(eqn)); \
      const VECTOR_FLOAT_TYPE log_left = term1;				\
      VECTOR_FLOAT_TYPE result =  VECTOR_FLOAT_MUL(log_left, val_log); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; })

//! @return the correlations-core for: Topsoee; difference from "k-diverngece" wrt. the 'qeual weightenting' of both 'feature vector-1' and 'feature-vector-2'
#define correlation_macros__distanceMeasures__shannon__Topsoee(term1, term2) ({ \
  const t_float result_left = correlation_macros__distanceMeasures__shannon__kDivergence(term1, term2); \
  const t_float result_right = correlation_macros__distanceMeasures__shannon__kDivergence(term2, term1); \
  const t_float result = result_left + result_right; \
  result; })
#define correlation_macros__distanceMeasures__shannon__Topsoee__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__shannon__Topsoee__SSE(term1, term2) ({ \
  const VECTOR_FLOAT_TYPE result_left = correlation_macros__distanceMeasures__shannon__kDivergence__SSE(term1, term2); \
  const VECTOR_FLOAT_TYPE result_right = correlation_macros__distanceMeasures__shannon__kDivergence__SSE(term2, term1); \
  const VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_ADD(result_left, result_right); \
  result; })

//! Identify the correlations-core for: Jensen-Shannon
#define correlation_macros__distanceMeasures__shannon__JensenShannon(term1, term2, obj) ({ \
  const t_float result_left = correlation_macros__distanceMeasures__shannon__kDivergence(term1, term2); \
  const t_float result_right = correlation_macros__distanceMeasures__shannon__kDivergence(term2, term1); \
  const t_float result = result_left + result_right; \
  /*! Update the result-objet*/				       \
  obj.numerator += result_left; obj.denumerator += result_right; })
#define correlation_macros__distanceMeasures__shannon__JensenShannon__postProcess(obj) ({ \
      metric_macro_defaultFunctions__postProcess__validateEmptyCase(0.5*(obj.numerator + obj.denumerator));})
#define correlation_macros__distanceMeasures__shannon__JensenShannon__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE result_left = correlation_macros__distanceMeasures__shannon__kDivergence__SSE(term1, term2); \
      const VECTOR_FLOAT_TYPE result_right = correlation_macros__distanceMeasures__shannon__kDivergence__SSE(term2, term1); \
      /*! Update the result-objet*/					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, result_left); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, result_right); })


//! @return the correlations-core for: Jensen adjusted wrt. difference.
// FIXME: in [”elow] validate the correcntess of suing "0" as the 'temorarly result' for cases where the vlaeu is set to "0" ... ie, as the sum of "(0, 10)" may be 'msielading' for cases such as ....??..
/* #define correlation_macros__distanceMeasures__shannon__JensenDifference(scalar_term1, scalar_term2) ({ \ */
/*       const t_float result_left_numerator_1 = (scalar_term1 != 0) ? scalar_term1 : 0; \ */
/*       const t_float result_left_numerator_2 = (scalar_term2 != 0) ? scalar_term2 * mathLib_float_log(scalar_term2) : 0; \ */
/*       const t_float result_left_numerator = result_left_numerator_1 + result_left_numerator_2; \ */
/*       const t_float result_left = result_left_numerator * 0.5;		\ */
/*       printf("FIXME: remove this and use correlation_macros__distanceMeasures__shannon__JensenDifference_old(..), at %s:%d\n", __FILE__, __LINE__); scalar_term1*scalar_term2;}) */

//! @return the correlations-core for: Jensen adjusted wrt. difference.
#define correlation_macros__distanceMeasures__shannon__JensenDifference(scalar_term1, scalar_term2) ({ \
      const t_float result_left_numerator_1 = (scalar_term1 != 0) ? scalar_term1 : 0; \
      const t_float result_left_numerator_2 = (scalar_term2 != 0) ? scalar_term2 * mathLib_float_log_abs(scalar_term2) : 0; \
      const t_float result_left_numerator = result_left_numerator_1 + result_left_numerator_2; \
      const t_float result_left = result_left_numerator * 0.5;		\
  const t_float result_right_leftSide_numerator = scalar_term1 + scalar_term2; \
  const t_float result_right_leftSide = result_right_leftSide_numerator * 0.5; \
  const t_float result_right_rightSide = (result_right_leftSide != 0) ? mathLib_float_log_abs(result_right_leftSide) : 0; \
  const t_float result_right = result_right_leftSide * result_right_rightSide; \
  const t_float result = result_left - result_right_rightSide; \
  result; })

//! @return the correlations-core for: Jensen adjusted wrt. difference.
// FIXME: in [”elow] validate the correcntess of suing "0" as the 'temorarly result' for cases where the vlaeu is set to "0" ... ie, as the sum of "(0, 10)" may be 'msielading' for cases such as ....??..
#define correlation_macros__distanceMeasures__shannon__JensenDifference_old(term1, term2) ({ \
      const t_float result_left_numerator_1 = (term1 != 0) ? term1 * mathLib_float_log_abs(term1) : 0; \
      const t_float result_left_numerator_2 = (term2 != 0) ? term2 * mathLib_float_log_abs(term2) : 0; \
  const t_float result_left_numerator = result_left_numerator_1 + result_left_numerator_2; \
  const t_float result_left = result_left_numerator * 0.5; \
  const t_float result_right_leftSide_numerator = term1 + term2; \
  const t_float result_right_leftSide = result_right_leftSide_numerator * 0.5; \
  const t_float result_right_rightSide = (result_right_rightSide != 0) ? mathLib_float_log_abs(result_right_leftSide) : 0; \
  const t_float result_right = result_right_leftSide * result_right_rightSide; \
  const t_float result = result_left - result_right_rightSide; \
  result; })
#define correlation_macros__distanceMeasures__shannon__JensenDifference__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__shannon__JensenDifference__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE result_left_numerator_1 = VECTOR_FLOAT_MUL(term1, VECTOR_FLOAT_LOG(VECTOR_FLOAT_abs(term1))); \
      const VECTOR_FLOAT_TYPE result_left_numerator_2 = VECTOR_FLOAT_MUL(term2, VECTOR_FLOAT_LOG(VECTOR_FLOAT_abs(term2))); \
      const VECTOR_FLOAT_TYPE result_left_numerator = VECTOR_FLOAT_ADD(result_left_numerator_1, result_left_numerator_2); \
      const VECTOR_FLOAT_TYPE result_left = VECTOR_FLOAT_MUL(result_left_numerator, VECTOR_FLOAT_SET1(0.5)); \
      const VECTOR_FLOAT_TYPE result_right_leftSide_numerator = VECTOR_FLOAT_ADD(term1, term2); \
      const VECTOR_FLOAT_TYPE result_right_leftSide = VECTOR_FLOAT_MUL(result_right_leftSide_numerator, VECTOR_FLOAT_SET1(0.5)); \
      const VECTOR_FLOAT_TYPE result_right_rightSide = VECTOR_FLOAT_LOG(VECTOR_FLOAT_abs(result_right_leftSide)); \
      const VECTOR_FLOAT_TYPE result_right = VECTOR_FLOAT_MUL(result_right_leftSide, result_right_rightSide); \
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_SUB(result_left, result_right_rightSide); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; })



//#endif //! EOF
