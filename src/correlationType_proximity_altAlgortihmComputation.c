#include "correlationType_proximity_altAlgortihmComputation.h"


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

//! Compute the proxmity for a 2x-2x conteitncy-table value and a proximity_type
//! @remarks for details wr.t the proxmity-table see http://www.ibm.com/support/knowledgecenter/SSLVMB_20.0.0/com.ibm.spss.statistics.help/alg_proximities_binary.htm
//t_float kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(uint sumOf_case_1a, uint sumOf_case_1b, uint sumOf_case_2a, uint sumOf_case_2b, const e_correlationType__t proximity_type) {
t_float kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(uint sumOf_case_1a, uint sumOf_case_1b, uint sumOf_case_2a, uint sumOf_case_2b, const e_correlationType__t proximity_type) {
  //! Note: ”[elow] amcro-fucntiosn (which we call) are expected to be found in our "correlation_macros__distanceMeasures_proximity.h":
  if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_russelRao) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__RusselRao(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_simpleMatching) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__SimpleMatching(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_jaccard) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__Jaccard(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_dice_Czekanowski_Sorenson) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__Dice_Czekanowski_Sorenson(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__SokalSneath(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_rogensTanimoto) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__RogersTanimoto(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath_measure2) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__SokalSneath_measure2(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_sokalSneath_measure3) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__SokalSneath_measure3(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else if(proximity_type == e_correlationType_proximity_altAlgortihmComputation_binary_kulczynski) {return correlation_macros__distanceMeasures__proximity_altAlgortihmComputation__Kulczynski(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);}
  else {
    assert(false); //! ie, as we then eed to add supprot for this case
    fprintf(stderr, "!!\t The measure at enum-di=%u is not supported, i,e , please update oru API and/or give the develoepr an heads-up at [oekseth@gamil.com]. Observaiton at [%s]:%s:%d\n", proximity_type, __FUNCTION__, __FILE__, __LINE__);
    return T_FLOAT_MAX;
  }
}


//! Note: In below we use the "uitn" data-type (instead of "bool"): we use uint in ”[elow] to try 'encoruaging' the compilaro to not opmize direclty ... ie, to make use of our 'explcit' branch-statements to tehst thios 'appraoch of cod-eoptizaiton':
#if(configure_performance_distanceMetricComputation_useBranchingWhenNotRequired == 0)
#define __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b) ({ \
      const uint case_1a = (hasFor_1  && hasFor_2);  const uint case_1b = (hasFor_1 && !hasFor_2); \
      const uint case_2a = (!hasFor_1 && hasFor_2);  const uint case_2b = (!hasFor_1 && !hasFor_2); \
      /*! Update the result-varialbes:*/				\
      sumOf_case_1a += case_1a; sumOf_case_1b += case_1b;		\
      sumOf_case_2a += case_2a; sumOf_case_2b += case_2b;		\
    })
#else //! then use a 'branchign-appraoch':
#define __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b) ({ \
      const uint case_1a = (hasFor_1  && hasFor_2);  const uint case_1b = (hasFor_1 && !hasFor_2); \
      const uint case_2a = (!hasFor_1 && hasFor_2);  const uint case_2b = (!hasFor_1 && !hasFor_2); \
      /*! Update the result-varialbes:*/				\
      if(case_1a) {sumOf_case_1a++;}  if(case_1b) {sumOf_case_1b++;}	\
      if(case_2a) {sumOf_case_2a++;}  if(case_2b) {sumOf_case_2b++;}	\
    })
#endif
//! -----------------------------------------------
//! Update wrt. the score-thresholds:
#define __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max) ({ \
      hasFor_1 = (hasFor_1 && (val1 > scoreTrheashold_min));		\
      hasFor_1 = (hasFor_1 && (val1 < scoreTrheashold_min));		\
      hasFor_2 = (hasFor_2 && (val2 > scoreTrheashold_min));		\
      hasFor_2 = (hasFor_2 && (val2 < scoreTrheashold_min));		\
    })


//! -----------------------------------------------




static t_float __kt_proximity_altAlgortihmComputation_slow(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max)  {

  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! Comptue the 2x2 contentinency-table:
  if(transpose == 0) {
    for(uint i = 0; i < ncols; i++)       {
      //! ---
      const t_float weight_local = (weight) ? weight[i] : 1.0;
      //! --- 
      t_float val1 = data1[index1][i];
      bool hasFor_1 =  (!mask1 || mask1[index1][i]) && isOf_interest(val1);      
      val1 *= weight_local; //! ie, adjust the weigth before the score-threholds are applied.

      //! --- 
      t_float val2 = data2[index2][i];
      bool hasFor_2 =  (!mask2 || mask2[index2][i]) && isOf_interest(val2);      
      val2 *= weight_local; //! ie, adjust the weigth before the score-threholds are applied.

      if( (scoreTrheashold_max != scoreTrheashold_min)) { //! then we assume score-trehshodls are to be used
	//! Update wrt. the score-thresholds:
	__local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
      }

      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    //! Note: at this exeuciton-poitn we asusem that the "ncols" refer to the "nrows" an non-tanspsoed 'matrix-interptatiopn':
    for(uint i = 0; i < ncols; i++)       {
      //! ---
      const t_float weight_local = (weight) ? weight[i] : 1;
      //! --- 
      t_float val1 = data1[i][index1];
      bool hasFor_1 =  (!mask1 || mask1[i][index1])  && isOf_interest(val1);      
      val1 *= weight_local; //! ie, adjust the weigth before the score-threholds are applied.      
      //! --- 
      t_float val2 = data2[i][index2];
      bool hasFor_2 =  (!mask2 || mask2[i][index2])  && isOf_interest(val2);      
      val2 *= weight_local; //! ie, adjust the weigth before the score-threholds are applied.

      if( (scoreTrheashold_max != scoreTrheashold_min)) { //! then we assume score-trehshodls are to be used
	//! Update wrt. the score-thresholds:
	__local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
      }

      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }

  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}

//! @return teh max-number fo elemtns in the SSE-intri-loop:
#define __local_get_cntIntri_elements(ncols) ({		      \
    uint cnt_elements_intri = ncols - VECTOR_FLOAT_ITER_SIZE; \
    if(((t_float)ncols/(t_float)VECTOR_FLOAT_ITER_SIZE)*((t_float)VECTOR_FLOAT_ITER_SIZE)) { \
    cnt_elements_intri = ncols; \
    } \
    cnt_elements_intri;})


enum e_case {
  _e_case_1a,   _e_case_1b,
  _e_case_2a,   _e_case_2b,
};

static t_float __kt_proximity_altAlgortihmComputation_fast_noMask_useThresholds(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  
  
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);

  //   VECTOR_INT_TYPE map_vec_sumOf[4] = { //! where we use the "e_case" to idnetify the local vectors.
  //     VECTOR_INT_SET1(0),
  //     VECTOR_INT_SET1(0),
  //     VECTOR_INT_SET1(0),
  //     VECTOR_INT_SET1(0)
  //   };
    
  //   VECTOR_FLOAT_TYPE vec_threshold_min = VECTOR_FLOAT_SET1(scoreTrheashold_min); 
  //   VECTOR_FLOAT_TYPE vec_threshold_max = VECTOR_FLOAT_SET1(scoreTrheashold_max);

  //   for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) { 
  //     //! Get values:
  //     VECTOR_FLOAT_TYPE vec_val1 = VECTOR_FLOAT_LOAD(&data1[index1][col_id]);
  //     VECTOR_FLOAT_TYPE vec_val2 = VECTOR_FLOAT_LOAD(&data2[index2][col_id]);


  //     // //! Update wrt. the score-thresholds:
  //     // VECTOR_FLOAT_TYPE vec1_insideRange = VECTOR_FLOAT_insideRange_notRevertOrder

  //     // VECTOR_FLOAT_TYPE vec_cmp_min_1 = VECTOR_FLOAT_CMPGT(vec_val1, vec_threshold_min);
  //     // VECTOR_FLOAT_TYPE vec_cmp_min_2 = VECTOR_FLOAT_CMPGT(vec_val2, vec_threshold_min);
  //     // //! ---
  //     // VECTOR_FLOAT_TYPE vec_cmp_max_1 = VECTOR_FLOAT_CMPLT(vec_val1, vec_threshold_max);
  //     // VECTOR_FLOAT_TYPE vec_cmp_max_2 = VECTOR_FLOAT_CMPLT(vec_val2, vec_threshold_max);
      

      

  //     assert(false); // FXIME: what are the data-types to use for [”elow]?


  //     assert(false); // FIXME: for explciit masks use VECTOR_FLOAT_MUL(..) after ....??.. wehre the masks are combined through VECTOR_FLOAT_AND(vec_mask, vec_mask_2) <-- consider dropping thsi 'combiatno' <--- ensure that we use the correct 'order' wrt. the comparisons.

  //     assert(false); // FIXME: udpate simialr fucntions for 'implicit mask', where we for the latter use ....??...

  //     // __local_update_scoreThreshold_SSE(val1_vec, val2_vec, hasFor_1_vec, hasFor_2_vec, scoreTrheashold_min, scoreTrheashold_max);

  //     // //! Update the 2x2 contingency-table-values:
  //     // __local_update_2x2x_contingencyTable_SSE(hasFor_1_vec, hasFor_2_vec, map_vec_sumOf);

  //   }

  //   //! Update oru result-cotnaienrs withthe SSE-results:
  //   sumOf_case_1a = VECTOR_INT_storeAnd_horizontalSum(map_vec_sumOf[_e_case_1a]);
  //   sumOf_case_1b = VECTOR_INT_storeAnd_horizontalSum(map_vec_sumOf[_e_case_1b]);
  //   sumOf_case_2a = VECTOR_INT_storeAnd_horizontalSum(map_vec_sumOf[_e_case_2a]);
  //   sumOf_case_2b = VECTOR_INT_storeAnd_horizontalSum(map_vec_sumOf[_e_case_2b]);
  // }
  for(; col_id < ncols; col_id ++) {
    //! --- 
    t_float val1 = data1[index1][col_id];
    bool hasFor_1 =  true;
	  
    //! --- 
    t_float val2 = data2[index2][col_id];
    bool hasFor_2 = true;
	  
    //! Update wrt. the score-thresholds:
    __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	  
    //! Update the 2x2 contingency-table-values:
    __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
  }	

  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}


static t_float __kt_proximity_altAlgortihmComputation_fast_noMask_useThresholds_useWeights(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	      
  //   for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) { 
  //     assert(false); // add somethign 
  //   }
  // }
	  
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 =  true;
      val1 *= weight_local;
	  
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = true;
      val2 *= weight_local;

      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	    
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! ---
      //const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 =  true;
      //val1 *= weight_local;
	  
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = true;
      //val2 *= weight_local;

      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	    
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }

  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}


static t_float __kt_proximity_altAlgortihmComputation_fast_maskMatrix(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:

  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	      
	      
  //   assert(false); // add somethign 
  // }
	    
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = mask1[index1][col_id];
      val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = mask2[index2][col_id];
      val2 *= weight_local;
	      
      // //! Update wrt. the score-thresholds:
      // __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! ---
      //const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = mask1[index1][col_id];
      // val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = mask2[index2][col_id];
      // val2 *= weight_local;
	      
      // //! Update wrt. the score-thresholds:
      // __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }
  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}

static t_float __kt_proximity_altAlgortihmComputation_fast_maskMatrix_useThresholds(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	      	      
	      
  //   assert(false); // add somethign 
  // }
	    
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = mask1[index1][col_id];
      val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = mask2[index2][col_id];
      val2 *= weight_local;
	      
      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! ---
      //const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = mask1[index1][col_id];
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = mask2[index2][col_id];
	      
      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }


  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}

static t_float __kt_proximity_altAlgortihmComputation_fast_maskMatrix_useWeights(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	        
	      
  //   assert(false); // add somethign 
  // }
	    
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = mask1[index1][col_id];
      val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = mask2[index2][col_id];
      val2 *= weight_local;
	      
      // //! Update wrt. the score-thresholds:
      // __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! ---
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = mask1[index1][col_id];
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = mask2[index2][col_id];
	      
      // //! Update wrt. the score-thresholds:
      // __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }
  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}


static t_float __kt_proximity_altAlgortihmComputation_fast_maskMatrix_useWeights_useThresholds(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	        
	      
  //   assert(false); // add somethign 
  // }
	    
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = mask1[index1][col_id];
      val1 *= weight_local;
      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = mask2[index2][col_id];
      val2 *= weight_local;
      
      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! ---
      //const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = mask1[index1][col_id];
      //val1 *= weight_local;
      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = mask2[index2][col_id];
      // val2 *= weight_local;
      
      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }

  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}

//! start: ------------------- implict masks:
static t_float __kt_proximity_altAlgortihmComputation_fast_implicitMask(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	      
	      
  //   assert(false); // add somethign 
  // }
	    
  
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = isOf_interest(val1);
      val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = isOf_interest(val2);
      val2 *= weight_local;
	      
      // //! Update wrt. the score-thresholds:
      // __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max, self);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = isOf_interest(val1);
      //val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = isOf_interest(val2);
      //val2 *= weight_local;
	      
      // //! Update wrt. the score-thresholds:
      // __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max, self);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }
  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}

static t_float __kt_proximity_altAlgortihmComputation_fast_implicitMask_useThresholds(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	      
	      
  //   assert(false); // add somethign 
  // }
	    
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = isOf_interest(val1);
      val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = isOf_interest(val2);
      val2 *= weight_local;
	      
      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! ---
      //const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = isOf_interest(val1);
      //val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = isOf_interest(val2);
      //val2 *= weight_local;
	      
      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }
  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}

static t_float __kt_proximity_altAlgortihmComputation_fast_implicitMask_useWeights(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	      
	      
  //   assert(false); // add somethign 
  // }
	    
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = isOf_interest(val1);
      val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = isOf_interest(val2);
      val2 *= weight_local;
	      
      // //! Update wrt. the score-thresholds:
      // __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! ---
      //const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = isOf_interest(val1);
      //val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = isOf_interest(val2);
      //val2 *= weight_local;
	      
      // //! Update wrt. the score-thresholds:
      // __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }

  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}


static t_float __kt_proximity_altAlgortihmComputation_fast_implicitMask_useWeights_useThresholds(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self) {
  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! --
  uint col_id = 0;
  //!
  //! The lgocis:
  //! Comptue the 2x2 contentinency-table:    
  // if(self.forNonTransposed_useFastImplementaiton_useSSE) {
  //   const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);	      
	      
  //   assert(false); // add somethign 
  // }
	    
  if(weight) {
    for(; col_id < ncols; col_id ++) {
      //! ---
      const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = isOf_interest(val1);
      val1 *= weight_local;
	      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = isOf_interest(val2);
      val2 *= weight_local;
	      
      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  } else {
    for(; col_id < ncols; col_id ++) {
      //! ---
      //const t_float weight_local = weight[col_id];
      //! --- 
      t_float val1 = data1[index1][col_id];
      bool hasFor_1 = isOf_interest(val1);
      //val1 *= weight_local;
      
      //! --- 
      t_float val2 = data2[index2][col_id];
      bool hasFor_2 = isOf_interest(val2);
      //val2 *= weight_local;
	      
      //! Update wrt. the score-thresholds:
      __local_update_scoreThreshold(val1, val2, hasFor_1, hasFor_2, scoreTrheashold_min, scoreTrheashold_max);
	      
      //! Update the 2x2 contingency-table-values:
      __local_update_2x2x_contingencyTable(hasFor_1, hasFor_2, sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b);
    }
  }
  //! @return the result:
  return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
}
//! ---------------------------------------------------------------
//! end: ------------------- implict masks:
//! -------------------------------------------------------------------------------------


/**
   @brief compute a proximity-metric score  between two rows or columns in a matrix.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set "0" if the value is missing.
   @remarks 
   - Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
   - if score-thresohlds are Not to be sued, then set scoreTrheashold_max = scoreTrheashold_min
 **/
t_float kt_proximity_altAlgortihmComputation(const uint ncols, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose, const e_correlationType__t proximity_type, const t_float scoreTrheashold_min, const t_float scoreTrheashold_max, const s_kt_correlationConfig_t self)  {

  //! Intialize:
  uint sumOf_case_1a = 0; uint sumOf_case_1b = 0;
  uint sumOf_case_2a = 0; uint sumOf_case_2b = 0;
  //! Comptue the 2x2 contentinency-table:
  if( (transpose == 0) && self.forNonTransposed_useFastImplementaiton) {



    // FIXME[use-case]: try to write an example where we examplify the use of a/the weightin-matrix to comptue results ... and simialriy write a sue-case where we use jaccard-simiarly either as a steo Before correlation-comtpatuion or After correlation-comptuation.
    //! --
    uint col_id = 0;
	  



    if(s_kt_correlationConfig_needTo_useMask_evaluation(&self) == false) {

      if( (scoreTrheashold_max == scoreTrheashold_min)) { //! then we assume no score-trehshodls are to be used
	sumOf_case_1a = ncols; //! ie, as all values are then of itnerest.
	//! @return the result:
	return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
      } else { //! then the score-threshodlsa are set:
	if(weight == NULL) { //! then do nto 'adjust'	 the weights boerfeo comarping 'against' thre score-threshold:
	  return __kt_proximity_altAlgortihmComputation_fast_noMask_useThresholds(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	} else { //! then we assume the valeus are to be adjusted wrt. teh weight:

	  return __kt_proximity_altAlgortihmComputation_fast_noMask_useThresholds_useWeights(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	}
      }
      //! end: all values are to be considred:
    } else {
      //! Start: evalaute only an explcit subset of values (ie, 'masks' are applied):
      if(mask1 || mask1) {
	assert(mask1); assert(mask2); //! ie, as we then exepct both maks-matrices to be set, ie, to simplfiy our code-writing.
	
	if(weight == NULL) {
	  
	  if( (scoreTrheashold_max == scoreTrheashold_min)) { //! then we assume no score-trehshodls are to be used

	    return __kt_proximity_altAlgortihmComputation_fast_maskMatrix(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);

	  } else {
	    return __kt_proximity_altAlgortihmComputation_fast_maskMatrix_useThresholds(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	  }
	} else { //! then we adjust for the weights
	  if( (scoreTrheashold_max == scoreTrheashold_min)) { //! then we assume no score-trehshodls are to be used
	    return __kt_proximity_altAlgortihmComputation_fast_maskMatrix_useWeights(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	  } else {
	    return __kt_proximity_altAlgortihmComputation_fast_maskMatrix_useWeights_useThresholds(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	  }
	}
      } else { //! then we use implcit masks:
	if(weight == NULL) {	  
	  if( (scoreTrheashold_max == scoreTrheashold_min)) { //! then we assume no score-trehshodls are to be used
	    return __kt_proximity_altAlgortihmComputation_fast_implicitMask(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	  } else {
	    return __kt_proximity_altAlgortihmComputation_fast_implicitMask_useThresholds(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	  }
	} else { //! then we adjust for the weights
	  if( (scoreTrheashold_max == scoreTrheashold_min)) { //! then we assume no score-trehshodls are to be used
	    return __kt_proximity_altAlgortihmComputation_fast_implicitMask_useWeights(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	  } else {
	    return __kt_proximity_altAlgortihmComputation_fast_implicitMask_useWeights_useThresholds(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max, self);
	  }
	}
      }
    }

       
    
    //! @return the result:
    return kt_proximity_altAlgortihmComputation_compute_fromContinencyTable(sumOf_case_1a, sumOf_case_1b, sumOf_case_2a, sumOf_case_2b, proximity_type);
  } else {
    return __kt_proximity_altAlgortihmComputation_slow(ncols, data1, data2, mask1, mask2, weight, index1, index2, transpose, proximity_type, scoreTrheashold_min, scoreTrheashold_max);
  }
}

