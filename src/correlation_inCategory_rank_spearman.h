#ifndef correlation_inCategory_rank_spearman_h
#define correlation_inCategory_rank_spearman_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file correlation_inCategory_rank_spearman.h
   @brief logics to infer Spearman's correlation for a comptue-all-against-all applicaiton-case
   @author Ole Kristina Ekseth (oekseth, 06. sept. 2016).
**/


#include "correlation_inCategory_delta.h"
#include "mask_base.h"
#include "correlation_inCategory_matrix_base.h"
/**
   @brief compute all-against-all for spearman.
   @remarks in this procedure we have updated the spearman-algorithm with the following steps:
 # Step(1): first allocate tempraory matrices, and then seperately compute two sorted matrices.
 # Step(2): use a matrix-iteration to comptue the 'mulitplicaiton' of the sums:
 # Step(3): udse teh 'correlation-approach' to 'adjsut' the sum.
**/
void ktCorr_compute_allAgainstAll_distanceMetric_spearman(const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input,  char** mask1, char** mask2, const t_float weight_[], t_float **resultMatrix, const e_typeOf_measure_spearman_masksAreAligned_t masksAre_aligned, bool transpose,  s_allAgainstAll_config_t *config_allAgainstAll);


#endif //! EOF
