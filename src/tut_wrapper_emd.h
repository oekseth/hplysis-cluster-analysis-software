#ifndef tut_wrapper_emd_h
#define tut_wrapper_emd_h
/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */



//! The main function for logics associated to "image segmentation" and the "earth mover distance" (EMD) (oekseth, 06. feb. 2018).
void tut_wrapper_emd(const int array_cnt, char **array);

#endif //! EOF
