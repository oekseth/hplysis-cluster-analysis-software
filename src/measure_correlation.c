#include "measure_correlation.h"
#include "measure_base.h"
#include "correlationType_kendall.h"
#include "correlation_inCategory_rank_kendall.h"
#include "correlationType_spearman.h"
#include "correlation_inCategory_rank_spearman.h"
#include "correlation_macros__distanceMeasures_proximity_altAlgortihmComputation.h"
#include "correlationType_proximity_altAlgortihmComputation.h"
#include "kt_math_matrix.h"
#include "distance_2rows_slow.h"
#include "kt_distance.h"
#include "kt_distance_cluster.h"
#include "aux_findPartitionFor_tiles.h"

static  const uint arrOf_chunkSizes_size = 3;
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
static  const uint base_size =  256*4; //VECTOR_INT_ITER_SIZE_short;
static  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {1*base_size,      							      
							      2*base_size,
							      2*base_size,
							      /* kilo*base_size,  */
							      /* 2*kilo*base_size, */
							      /* 3*kilo*base_size, 4*kilo*base_size, */
							      //80*kilo*base_size, 160*kilo*base_size,
							      //300*kilo*base_size, 600*kilo*base_size,
};

//! ------------------------------------------------------------
//! Specify the fraction of row-size wrt. the column-size, a column-saize 'specifed' in the "arrOf_chunkSizes" table.
static const uint arrOf_rowMult_size = 9;
static const t_float arrOf_rowMult[arrOf_rowMult_size] = {
  1/16, 1/32,
  1, 16, 32,
  64, 128, 256, 512
};


//! Evalaute exec-time of naive-emtic-impelmentaitons wrt. detla-matrics.
static void metrics_naive(const bool for_maskExplicit, const bool for_maskImplicit, const bool for_kendall, const bool for_spearman, const bool for_delta, const bool for_proximity, const bool for_distanceCluster, const bool for_fastVersion, const bool for_slowVersion) {
  const uint default_value_float = 0;
  t_float *mapOf_timeCmp_forEachBucket = allocate_1d_list_float(arrOf_chunkSizes_size, default_value_float);
  //! ************************************************************************************************************
  //! Perform muliple iteraitons 
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    //! -------------------------------
    const uint size_of_array = arrOf_chunkSizes[chunk_index]; 
    const uint ncols = size_of_array; //(ncols > 256size_of_array;

    for(uint chunk_adjusmentIndex_row = 0; chunk_adjusmentIndex_row < arrOf_rowMult_size; chunk_adjusmentIndex_row++) {
      const uint nrows__ = (uint)( ((t_float)ncols * arrOf_rowMult[chunk_adjusmentIndex_row])); 
      //      const uint nrows__ = (uint)((t_float)VECTOR_INT_ITER_SIZE_short*( ((t_float)ncols * arrOf_rowMult[chunk_adjusmentIndex_row])/(t_float)VECTOR_INT_ITER_SIZE_short) ); 
      //assert(nrows__ != 0);
      const uint nrows = (nrows__ > 2) ? nrows__ : 2;
      assert(nrows > 1);
      printf("---------------------------------------\n# \t nrows=%u and list-size=%u, at %s:%d\n", nrows, size_of_array, __FILE__, __LINE__);
      //s_kt_set_1dsparse_t obj_1; setTo_Empty__s_kt_set_1dsparse_t(&obj_1);

      const loint max_data_size = kilo * kilo * 100; //! ie, 100*1000*1000 elements to try using less than 1,000,000,000 GB of memory 
      if((nrows*ncols) > max_data_size) {
	fprintf(stderr, "The current row-size (%u)(%u) is outside the current max-member-threshold=%llu, ie, consider udpating the latter threadols (eg, if your a are running a large system than a latop with in-sufficient memoery, an observat ion at [%s]:%s:%d\n", nrows, ncols, max_data_size, __FUNCTION__, __FILE__, __LINE__);
	continue;
      }

      //! -------------------------------
      const t_float default_value_float = 100;     const char default_value_char = 10;
      t_float **matrix = allocate_2d_list_float(nrows, size_of_array, default_value_float);
      t_float **matrix_transposed = allocate_2d_list_float(size_of_array, nrows, default_value_float);
      char **mask1 = allocate_2d_list_char(nrows, size_of_array, default_value_char);
      char **mask2 = allocate_2d_list_char(nrows, size_of_array, default_value_char);
      int **mask1_int = allocate_2d_list_int(nrows, size_of_array, default_value_char);
      int **mask2_int = allocate_2d_list_int(nrows, size_of_array, default_value_char);
      int **mask1_int_transposed = allocate_2d_list_int(size_of_array, nrows, default_value_char);
      int **mask2_int_transposed = allocate_2d_list_int(size_of_array, nrows, default_value_char);
      t_float *weight = allocate_1d_list_float(size_of_array, default_value_float);
      t_float *row = NULL;  char **matrix_mask = NULL; char *mask = NULL;

      //if(false)  // FIXME: remove
      { //! Traversal time for data-sets: we expec thtere to  not be any difference.
#include "measure__stub__3d_basic1d_denseMemoryAccessTimeComparison.c"
      }	
      if(for_maskExplicit) { //! Test the tiem-effect assicated to masking for different use-cases:
#include "measure__stub__3d_basic1d_denseMemoryAccessTimeComparison__mask.c"
      }
      if(for_maskImplicit) { //! Test the tiem-effect assicated to masking for different use-cases, ie, for implict masks:
#include "measure__stub__3d_basic1d_denseMemoryAccessTimeComparison__maskImplicit.c"
      }


      if(for_kendall) { //! Compare wrt. comptuation of Kendall:

	if(for_fastVersion) {assert(false); } //! ie, as we then need adding support for this.
	//! Note: in the comptautison of Kendall we evalatue time-complexity both for functions 'deidcatet' to a "two-feature-vector-comparison" and a "all-to-all-feature-comparison" (where the latter is of ciritcal improtance eg, in cluster-analyis)
	if(for_slowVersion) {
#define __localConfig__in2dKendall__evalauteTransposedCase 1
#include "measure__stub__2d__kendall.c" //! a measure-data-set which is also inlcuded wrt. our perf-test for "kt_set_1dsparse" and "kt_set_2dsparse";
	} //! else 'this metic' is implemnted in gener genrlaised 'delta' case.
	//! ----------------
      }
      
      //if(false) { // FIXME: remove
      if(for_spearman) {
	if(for_slowVersion) {
#include "measure__stub__2d__spearman.c"  //! for which we evlauate: "correlation_inCategory_rank_spearman.c" and "correlationType_spearman.c"
	} //! else 'this metic' is implemnted in gener genrlaised 'delta' case.
      }

      if(for_proximity) {
	if(for_slowVersion) {
#include "measure__stub__2d__naive_proximity.c"  //! for which we evlauate: "correlationType_proximity_altAlgortihmComputation.c"
	} //! else 'this metic' is implemnted in gener genrlaised 'delta' case.
      }

      if(for_delta) {
#include "measure__stub__2d__naive_delta.c"  //! for which we evlauate: 'delta' correlaitons (scuh as Euclid, Cityblock and diffnere tpermtuations of Perasons correlation-metric)
      }


      if(for_distanceCluster) {
	//! Test the performance of the "kt_distance_cluster.c" functions
	if(for_slowVersion) {
	  // correlationType_spearman.c correlation_inCategory_rank_spearman.c
	  assert(false); // FIXME: cosndier including below:
	  // #define __localConfig__kt_distance_cluster__iterateThroughOldSubsetOf_metrics 1
	  // #include "measure__stub__2d__naive_kt_distance_cluster.c"  //! for which we evlauate: 'kt_distance_cluster.c' correlaitons (average-clustering) .... where we investigate wrt. 'attributs' such as (a) different data-access-schmes, (b) metrics in our "e_kt_clusterComparison.h" and different sizes of "n1" and "n2"
	} else {
	  assert(false); // FIXME: cosndier including below:
	  // #define __localConfig__kt_distance_cluster__iterateThroughOldSubsetOf_metrics 0 //! ie, then test for the complete range of distnace-emtrics.
	  //#include "measure__stub__2d__naive_kt_distance_cluster.c"  //! for which we evlauate: 'kt_distance_cluster.c' correlaitons (average-clustering) .... where we investigate wrt. 'attributs' such as (a) different data-access-schmes, (b) metrics in our "e_kt_clusterComparison.h" and different sizes of "n1" and "n2"
	}
      }




      //! -------------------------------
      //! De-allcoate:
      free_2d_list_float(&matrix, nrows);
      free_2d_list_float(&matrix_transposed, size_of_array);
      free_2d_list_char(&mask1, nrows);
      free_2d_list_char(&mask2, nrows);
      free_2d_list_int(&mask1_int, nrows);
      free_2d_list_int(&mask2_int, nrows);
      free_2d_list_int(&mask1_int_transposed, size_of_array);
      free_2d_list_int(&mask2_int_transposed, size_of_array);
      free_1d_list_float(&weight);
    }
  }

  //! *****************************************************************************
  //!
  //!  De-allocate locally reserved memory
  free_1d_list_float(&mapOf_timeCmp_forEachBucket);
}





//! Test the correctenss of our approaches
static void test_correctness() {
#ifndef NDEBUG
  //! First validate correctness of the 'tiled-block-seperation-poriton':
  debug_applyInternalCorrectnessTests();
#endif

  { //! A Simple test to explore the eudlid-distance-metric:
    //const uint nrows = 3; const uint ncols = 5;
    //    const uint nrows = 3; const uint ncols = 10;
    const uint nrows = 17; const uint ncols = 2;
    //const uint nrows = 17; const uint ncols = 50;
    //const uint nrows = 10; const uint ncols = 2;
    //const uint nrows = 16; const uint ncols = 2;
    //const uint nrows = 17; const uint ncols = 2;
    //const uint nrows = 20; const uint ncols = 50;
    //const uint nrows = 20; const uint ncols = 200;
    //const uint nrows = 64; const uint ncols = 64;
    uint row_id = 0; uint row_id_out = 1;
    t_float default_value_float = 16;
    t_float **matrix_result = allocate_2d_list_float(nrows, nrows, default_value_float);
    t_float **local_matrix_1 = allocate_2d_list_float(nrows, ncols, default_value_float);
    t_float **local_matrix_2 = allocate_2d_list_float(nrows, ncols, default_value_float);
    for(uint i = 0; i < nrows; i++) {
      for(uint k = 0; k < ncols; k++) {
	local_matrix_1[i][k] = 1 + k;
	local_matrix_2[i][k] = 1 + k;
	//	local_matrix_2[i][k] = 1 + 2*k;
      }
      //      local_matrix_1[row_id_out][i] = i;
    }
    /* for(uint i = 0; i < ncols; i++) { */
    /*   local_matrix_1[row_id][i] = 1; */
    /*   local_matrix_1[row_id_out][i] = i; */
    /* } */
    char **local_mask1 = NULL;
    char **local_mask2 = NULL;
    t_float *local_weight = NULL;
    //! ---
    e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep = e_kt_categoryOf_correaltionPreStep_none;
  

    char config_transpose = 0; const char config_mask = 0;
    { //! Comapre 'one-to-many' with 'each-row':
      e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2;
      bool config_transpose = false;	  
      t_float *arrOf_result = allocate_1d_list_float(nrows, default_value_float);
      char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
      printf("-------------------------------\n\n# compute(1:many): \"%s\"\t with proeprties={%s}, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, __FILE__, __LINE__);
      //bool config_transpose = false;
      {
	e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_false;
	{
	  char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
	  printf("#*******************************\n\n# compute: \"%s\"\t with proeprties={%s}, config_mask=%u, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, config_mask, __FILE__, __LINE__);
	  // e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_true; 
	  /* s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /\*CLS=*\/64, /\*isTo_use_continousSTripsOf_memory=*\/true, /\*iterationIndex_2=*\/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /\*s_inlinePostProcess=*\/NULL, masksAre_used, /\*isTo_use_SIMD=*\/true, e_typeOf_optimization_distance_asFastAsPossible); */
	  /* set_metaMatrix(&self, /\*isTo_init=*\/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used); */
	  s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
	  set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
	  //!
	  //! The call:
	  kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  matrix_result, self, /*s_kt_computeTile_subResults_t=*/NULL);
	}
	s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
	set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
	kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  /*index1=*/row_id, &self, /*optionalStore_kendall=*/NULL, arrOf_result, /*metric_oneToMany_nonRank=*/NULL); // /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 1), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
	for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
	  const t_float sumOf_values = arrOf_result[row_id_out];
	  if(row_id_out != row_id) 
	  { //! Compare: "many:many" and "1:many":
	    const t_float expected_value = matrix_result[row_id][row_id_out];
	    printf("(many-many::compare-results::[%u])\t %f VS %f, at %s:%d\n", row_id_out, sumOf_values, expected_value, __FILE__, __LINE__);
	    assert(expected_value != default_value_float); // TODO: cosnider removign this assert
	    assert(sumOf_values == expected_value);
	  }
	  assert(false == isinf(sumOf_values)); 
	  // assert(false == isnanf(sumOf_values)); //! out-commented to get compiled on a SINTEF mac (oekseth, 06. feb. 2018)
	  if(row_id_out != row_id) {
	    char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
	    printf("#*******************************\n\n# compute: \"%s\"\t with proeprties={%s}, config_mask=%u, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, config_mask, __FILE__, __LINE__);
	    const t_float sumOf_values_out = kt_compare__each__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2, local_mask1, local_mask2, local_weight, /*index1=*/row_id, /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 0), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
	    assert(false == isinf(sumOf_values_out)); 
	    assert(false == isnanf(sumOf_values_out));
	    //! Compare the results:
	    printf("(compare-results::[%u])\t %f VS %f, at %s:%d\n", row_id_out, sumOf_values, sumOf_values_out, __FILE__, __LINE__);
	    assert(sumOf_values == sumOf_values_out);
	  }
	}
      }
      //assert(false); // FXIME: remove.
      //! -----------------------------
      {
	e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_true;
	s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
	set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
	kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  /*index1=*/row_id, &self, /*optionalStore_kendall=*/NULL, arrOf_result, /*metric_oneToMany_nonRank=*/NULL); // /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 1), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
	for(uint i = 0; i < ncols; i++) {
	  const t_float sumOf_values = arrOf_result[i];
	  assert(false == isinf(sumOf_values)); 
	  // assert(false == isnanf(sumOf_values)); //! out-commented to get compiled on a SINTEF mac (oekseth, 06. feb. 2018)
	}
      }
      //! De-allocate:
      free_1d_list_float(&arrOf_result);
    }
    
    //assert(false); // FXIME: remove.

    { //! Compare all-againsta--:
      { //! Test for 'transposed':
	t_float **local_matrix_result = allocate_2d_list_float(ncols, ncols, default_value_float);
	bool config_transpose = true;
	{ //! For two-rows:
	  e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2;
	  t_float sumOf_values = kt_compare__each__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2, local_mask1, local_mask2, local_weight, /*index1=*/row_id, /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 0), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
	}
	//assert(false); // FXIME: remove.
	
	
	{ //! For all-agaisnt-all:
	  e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2;
	  // e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max;
	  char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
	  printf("-------------------------------\n\n# compute: \"%s\"\t with proeprties={%s}, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, __FILE__, __LINE__);
	  {
	    e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_false; 
	    /* s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /\*CLS=*\/64, /\*isTo_use_continousSTripsOf_memory=*\/true, /\*iterationIndex_2=*\/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /\*s_inlinePostProcess=*\/NULL, masksAre_used, /\*isTo_use_SIMD=*\/true, e_typeOf_optimization_distance_asFastAsPossible); */
	    /* set_metaMatrix(&self, /\*isTo_init=*\/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used); */
	    s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
	    set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
	    //!
	    //! The call:
	    kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  local_matrix_result, self, /*s_kt_computeTile_subResults_t=*/NULL);
	  }
	  {
	    e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_true; 
	    /* s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /\*CLS=*\/64, /\*isTo_use_continousSTripsOf_memory=*\/true, /\*iterationIndex_2=*\/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /\*s_inlinePostProcess=*\/NULL, masksAre_used, /\*isTo_use_SIMD=*\/true, e_typeOf_optimization_distance_asFastAsPossible); */
	    /* set_metaMatrix(&self, /\*isTo_init=*\/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used); */
	    s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
	    set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
	    //!
	    //! The call:
	    kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  local_matrix_result, self, /*s_kt_computeTile_subResults_t=*/NULL);
	  }
	}
	//assert(false); // FXIME: remove.
	free_2d_list_float(&local_matrix_result, nrows);
      }



      {
	e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_absoluteDifference_oekseth_forMasks_standardDeviation_pow2;
	  // e_kt_correlationFunction_groupOf_minkowski_chebychev_minInsteadOf_max;
	char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
	printf("-------------------------------\n\n# compute: \"%s\"\t with proeprties={%s}, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, __FILE__, __LINE__);
	e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_false; bool config_transpose = false;
	/* s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /\*CLS=*\/64, /\*isTo_use_continousSTripsOf_memory=*\/true, /\*iterationIndex_2=*\/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /\*s_inlinePostProcess=*\/NULL, masksAre_used, /\*isTo_use_SIMD=*\/true, e_typeOf_optimization_distance_asFastAsPossible); */
	/* set_metaMatrix(&self, /\*isTo_init=*\/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used); */
	s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
	set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
	//!
	//! The call:
	kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  matrix_result, self, /*s_kt_computeTile_subResults_t=*/NULL);
      }

      // assert(false); // FIXME: remvoe.
      
      for(uint metric_index = 0; metric_index < e_kt_correlationFunction_undef; metric_index++) {
	e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_index;
	//e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	if(false == describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id))  {
	  if(isTo_use_kendall__e_kt_correlationFunction(metric_id) == false) {
	    if(isTo_use_MINE__e_kt_correlationFunction(metric_id) == false) {
	      if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1) 
		 metric_id != e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly
#else
		 true
#endif
		 ) {
		char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
		printf("-------------------------------\n\n# compute: \"%s\"\t with proeprties={%s}, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, __FILE__, __LINE__);
		bool config_transpose = false;
		{
		  e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_false; 
		  /* s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /\*CLS=*\/64, /\*isTo_use_continousSTripsOf_memory=*\/true, /\*iterationIndex_2=*\/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /\*s_inlinePostProcess=*\/NULL, masksAre_used, /\*isTo_use_SIMD=*\/true, e_typeOf_optimization_distance_asFastAsPossible); */
		  /* set_metaMatrix(&self, /\*isTo_init=*\/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used); */
		  s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
		  set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
		  //!
		  //! The call:
		  kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  matrix_result, self, /*s_kt_computeTile_subResults_t=*/NULL);
		}
		{
		  e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_true; 
		  /* s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /\*CLS=*\/64, /\*isTo_use_continousSTripsOf_memory=*\/true, /\*iterationIndex_2=*\/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /\*s_inlinePostProcess=*\/NULL, masksAre_used, /\*isTo_use_SIMD=*\/true, e_typeOf_optimization_distance_asFastAsPossible); */
		  /* set_metaMatrix(&self, /\*isTo_init=*\/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used); */
		  s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
		  set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
		  //!
		  //! The call:
		  kt_compute_allAgainstAll_distanceMetric(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  matrix_result, self, /*s_kt_computeTile_subResults_t=*/NULL);
		}
	      }
	    }
	  }
	}
      }
    }
    {
      for(uint metric_index = 0; metric_index < e_kt_correlationFunction_undef; metric_index++) {
	e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_index;
	//e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;
	if(false == describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id))  {
	  if(isTo_use_kendall__e_kt_correlationFunction(metric_id) == false) {
	    if(isTo_use_MINE__e_kt_correlationFunction(metric_id) == false) {
	      if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1) 
		 metric_id != e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly
#else
		 true
#endif
		 ) {
		t_float *arrOf_result = allocate_1d_list_float(nrows, default_value_float);
		char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
		printf("-------------------------------\n\n# compute(1:many): \"%s\"\t with proeprties={%s}, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, __FILE__, __LINE__);
		bool config_transpose = false;
		{
		  e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_false;
		  s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
		  set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
		  kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  /*index1=*/row_id, &self, /*optionalStore_kendall=*/NULL, arrOf_result, /*metric_oneToMany_nonRank=*/NULL); // /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 1), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
		  for(uint row_id_out = 0; row_id_out < nrows; row_id_out++) {
		    const t_float sumOf_values = arrOf_result[row_id_out];
		    assert(false == isinf(sumOf_values)); 
		    // assert(false == isnanf(sumOf_values)); //! out-commented to get compiled on a SINTEF mac (oekseth, 06. feb. 2018)
		    if(row_id_out != row_id) {
		      char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
		      printf("# compute: \"%s\"\t with proeprties={%s}, config_mask=%u, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, config_mask, __FILE__, __LINE__);
		      const t_float sumOf_values_out = kt_compare__each__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2, local_mask1, local_mask2, local_weight, /*index1=*/row_id, /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 0), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
		      assert(false == isinf(sumOf_values_out)); 
		      assert(false == isnanf(sumOf_values_out));
		      //! Compare the results:
		      printf("(compare-results::[%u])\t %f VS %f, at %s:%d\n", row_id_out, sumOf_values, sumOf_values_out, __FILE__, __LINE__);
		      assert(sumOf_values == sumOf_values_out);
		    }
		  }
		}
		{
		  e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_true;
		  s_allAgainstAll_config self; init__s_allAgainstAll_config(&self, /*CLS=*/64, /*isTo_use_continousSTripsOf_memory=*/true, /*iterationIndex_2=*/UINT_MAX, e_allAgainstAll_SIMD_inlinePostProcess_undef, /*s_inlinePostProcess=*/NULL, masksAre_used, /*isTo_use_SIMD=*/true, e_typeOf_optimization_distance_asFastAsPossible);
		  set_metaMatrix(&self, /*isTo_init=*/false, local_mask1, local_mask2, local_weight, config_transpose, masksAre_used, nrows, ncols);
		  kt_compare__oneToMany(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2,  /*index1=*/row_id, &self, /*optionalStore_kendall=*/NULL, arrOf_result, /*metric_oneToMany_nonRank=*/NULL); // /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 1), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
		  for(uint i = 0; i < ncols; i++) {
		    const t_float sumOf_values = arrOf_result[i];
		    assert(false == isinf(sumOf_values)); 
		    // assert(false == isnanf(sumOf_values)); //! out-commented to get compiled on a SINTEF mac (oekseth, 06. feb. 2018)
		  }
		}
		//! De-allocate:
		free_1d_list_float(&arrOf_result);
	      }
	    }
	  }
	}
      }
    }


    { //! Test seperately for minkowski:
      /* VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_POWER(VECTOR_FLOAT_LOAD(&(local_matrix_1[0][0])), 2); */
      e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_minkowski__subCase__zeroOne;
      
      { //! Test the effects of the post-processing:
	t_float result_local = 9.0; 
	t_float power = 0.3333;
	const t_float tmp = correlation_macros__distanceMeasures__minkowski__postProcess(result_local, power);
	//printf("tmp=%f, at %s:%d\n", tmp, __FILE__, __LINE__);
      }

      t_float sumOf_values = kt_compare__each__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2, local_mask1, local_mask2, local_weight, /*index1=*/row_id, /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 0), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
      //assert(false); // FIXME: remove.
    }
    for(uint metric_index = 0; metric_index < e_kt_correlationFunction_undef; metric_index++) {
      e_kt_correlationFunction_t metric_id = (e_kt_correlationFunction_t)metric_index;
      //e_kt_correlationFunction_t metric_id = e_kt_correlationFunction_groupOf_minkowski_euclid;

      if(false == describes_aMetricwithValeusWhichShouldBeBetween_zeroOne__s_kt_correlationMetric_t(metric_id)) {
	if(isTo_use_kendall__e_kt_correlationFunction(metric_id) == false) {
	  if(isTo_use_MINE__e_kt_correlationFunction(metric_id) == false) {
	    if(
#if(globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct == 1)
	       metric_id != e_kt_correlationFunction_groupOf_nonOptimizedPerformanceTest_spearman_or_correlation_sumOnly
#else
		 true
#endif
	       ) {
	      for(uint config_mask = 0; config_mask < 2; config_mask++) {
		char stringOf_data_toAdd[3000] = {'\0'}; include_stringDescription_forEnum_s_template_correlation_tile_classification_t(metric_id, stringOf_data_toAdd);
		printf("# compute: \"%s\"\t with proeprties={%s}, config_mask=%u, at %s:%d\n", get_stringOf_enum__e_kt_correlationFunction_t(metric_id), stringOf_data_toAdd, config_mask, __FILE__, __LINE__);
		t_float sumOf_values = kt_compare__each__extensiveInputParams(metric_id, typeOf_correlationPreStep, nrows, ncols, local_matrix_1, local_matrix_2, local_mask1, local_mask2, local_weight, /*index1=*/row_id, /*index2=*/row_id_out, config_transpose, /*isTo_useImplictMask=*/(config_mask == 0), /*(*metric_each_nonRank)(const config_nonRank_each_t)=*/NULL, /*s_correlationType_kendall_partialPreCompute_kendall_t=*/NULL);
		assert(false == isinf(sumOf_values)); 
		// assert(false == isnanf(sumOf_values)); //! out-commented to get compiled on a SINTEF mac (oekseth, 06. feb. 2018)
	      }
	    }
	  }
	}
      }
    }

    // FIXME: thereafter test for: ... no-mask
    // FIXME: thereafter test for: ... explicit-mask

    //! 
    //! De-allocate:
    free_2d_list_float(&local_matrix_1, nrows);
    free_2d_list_float(&local_matrix_2, nrows);
    free_2d_list_float(&matrix_result, nrows);
  }





  

}






//! The main test-function.
void measure_correlation__main(const int array_cnt, char **array) {
 


  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r


  if(isTo_processAll || (0 == strncmp("test_correctness", array[2], strlen("test_correctness"))) ) {
    test_correctness();
    cnt_Tests_evalauted++;
  }

  const bool for_fastVersion = (isTo_processAll ||
				(
				 (array_cnt >= 3) && array[3] && strlen(array[3]) && (0 == strncmp("for_fastVersion", array[3], strlen("for_fastVersion")))
				 )
				);
  //printf("for_fastVersion=%d, at %s:%d\n", for_fastVersion, __FILE__, __LINE__);
  bool for_slowVersion = true;

  if(isTo_processAll || (0 == strncmp("metrics_naive_maskImplicit", array[2], strlen("metrics_naive_maskImplicit"))) ) {
    const bool for_maskImplicit = true;  const bool for_maskExplicit = false; 
    const bool for_kendall = false; const bool for_spearman = false; const bool for_delta = false; const bool for_proximity = false;  const bool for_distanceCluster = false;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("metrics_naive_maskExplicit", array[2], strlen("metrics_naive_maskExplicit"))) ) {
    const bool for_maskImplicit = true;  const bool for_maskExplicit = false; 
    const bool for_kendall = false; const bool for_spearman = false; const bool for_delta = false; const bool for_proximity = false;  const bool for_distanceCluster = false;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("metrics_naive_mask", array[2], strlen("metrics_naive_mask"))) ) {
    const bool for_maskImplicit = true;  const bool for_maskExplicit = true; 
    const bool for_kendall = false; const bool for_spearman = false; const bool for_delta = false; const bool for_proximity = false;  const bool for_distanceCluster = false;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("metrics_naive_delta", array[2], strlen("metrics_naive_delta"))) ) {
    const bool for_maskImplicit = false;  const bool for_maskExplicit = false; 
    const bool for_kendall = false; const bool for_spearman = false; const bool for_delta = true; const bool for_proximity = false; const bool for_distanceCluster = false;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("metrics_naive_Spearman", array[2], strlen("metrics_naive_Spearman"))) ) {
    const bool for_maskImplicit = false;  const bool for_maskExplicit = false; 
    const bool for_kendall = false; const bool for_spearman = true; const bool for_delta = false; const bool for_proximity = false;  const bool for_distanceCluster = false;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("metrics_naive_Kendall", array[2], strlen("metrics_naive_Kendall"))) ) {
    const bool for_maskImplicit = false;  const bool for_maskExplicit = false; 
    const bool for_kendall = true; const bool for_spearman = false; const bool for_delta = false; const bool for_proximity = false;  const bool for_distanceCluster = false;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("metrics_naive_proximity", array[2], strlen("metrics_naive_proximity"))) ) {
    const bool for_maskImplicit = false;  const bool for_maskExplicit = false; 
    const bool for_kendall = false; const bool for_spearman = false; const bool for_delta = false; const bool for_proximity = true;  const bool for_distanceCluster = false;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("metrics_naive_kt_distance_cluster", array[2], strlen("metrics_naive_kt_distance_cluster"))) ) {
    const bool for_maskImplicit = false;  const bool for_maskExplicit = false; 
    const bool for_kendall = false; const bool for_spearman = false; const bool for_delta = false; const bool for_proximity = false;  const bool for_distanceCluster = true;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("metrics_naive", array[2], strlen("metrics_naive"))) ) {
    const bool for_maskImplicit = false;  const bool for_maskExplicit = false; 
    const bool for_kendall = true; const bool for_spearman = true; const bool for_delta = true; const bool for_proximity = true; const bool for_distanceCluster = true;
    metrics_naive(for_maskImplicit, for_maskExplicit, for_kendall, for_spearman, for_delta, for_proximity, for_distanceCluster, for_fastVersion, for_slowVersion);
    cnt_Tests_evalauted++;
  }

  
  //! ------------------------------------------------------------------------------------------------------------------
  //! Warn  if no parameters matched:
  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}

