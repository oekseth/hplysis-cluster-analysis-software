//!
//! Identify the thresholds:
//! Identify the thresholds:
#ifdef __macro__computeFor__rows
t_float min_cnt_rows = self->thresh_minMax__cellMask_meanAbsDiff_row[0];
//min_cnt_rows *= self->nrows;
if(min_cnt_rows != T_FLOAT_MAX) {
  min_cnt_rows *= 0.01;
 }
t_float max_cnt_rows = self->thresh_minMax__cellMask_meanAbsDiff_row[0];
if(max_cnt_rows != T_FLOAT_MAX) {
  //assert(max_cnt_rows >= min_cnt_rows);
  max_cnt_rows *= 0.01; //! ie, devidie by 100 to change form !%" to flaoting-point.
  //max_cnt_rows *= self->nrows;
 }
#endif
#ifdef __macro__computeFor__cols
//! ------------
t_float min_cnt_cols = self->thresh_minMax__cellMask_meanAbsDiff_column[0];
if(min_cnt_cols != T_FLOAT_MAX) {
  min_cnt_cols *= 0.01;
 }
//min_cnt_columns *= self->ncols;
t_float max_cnt_cols = self->thresh_minMax__cellMask_meanAbsDiff_column[0];
if(max_cnt_cols != T_FLOAT_MAX) {
  //assert(max_cnt_columns >= min_cnt_columns);
  max_cnt_cols *= 0.01; //! ie, devidie by 100 to change form !%" to flaoting-point.
 }
#endif
//! -----------------
//!
//! Apply the filters:
#ifdef __macro__computeFor__rows
const bool isTo_evaluateFor_rows = ( (min_cnt_rows    != T_FLOAT_MAX) || (max_cnt_rows != T_FLOAT_MAX) );
#else 
const bool isTo_evaluateFor_rows = false;
#endif
#ifdef __macro__computeFor__cols
const bool isTo_evaluateFor_cols = ( (min_cnt_cols    != T_FLOAT_MAX) || (max_cnt_cols != T_FLOAT_MAX) );
#else
const bool isTo_evaluateFor_cols = false;
#endif

//! -------------------------------------------------------------------------------------------------------
//! -------------------------------------------------------------------------------------------------------
//! -------------------------------------------------------------------------------------------------------


if( (isTo_evaluateFor_rows || isTo_evaluateFor_cols) ) {
  const uint default_value_float = 0;   const uint default_value_uint = 0;
  t_float *mapOf_valueSums_rows = NULL; t_float *mapOf_valueSums_cols = NULL;
  uint *mapOf_count_rows = NULL; uint *mapOf_count_cols = NULL;
#ifdef __macro__computeFor__rows
  if(isTo_evaluateFor_rows) {mapOf_valueSums_rows = allocate_1d_list_float(self->nrows, default_value_float);}
  if(isTo_evaluateFor_rows) {mapOf_count_rows = allocate_1d_list_uint(self->nrows, default_value_uint);}
#endif
#ifdef __macro__computeFor__cols
  if(isTo_evaluateFor_cols) {mapOf_valueSums_cols = allocate_1d_list_float(self->ncols, default_value_float);}
  if(isTo_evaluateFor_cols) {mapOf_count_cols = allocate_1d_list_uint(self->ncols, default_value_uint);}
#endif


  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    if(self->mapOf_interesting_rows[row_id]) {
      const t_float *__restrict__ row = self->matrix[row_id];
      //! ---
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	if(isOf_interest(row[col_id])) {
	  if(isTo_evaluateFor_rows) {mapOf_valueSums_rows[row_id] += row[col_id]; mapOf_count_rows[row_id]++;}
	  if(isTo_evaluateFor_cols) {mapOf_valueSums_cols[col_id] += row[col_id]; mapOf_count_cols[col_id]++;}
	}
      }
    }
  }
  //! 
  //! Apply the thresholds:
  if(isTo_evaluateFor_rows) {	
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      if(self->mapOf_interesting_rows[row_id]) {
	const t_float ncols_inv = (mapOf_count_rows[row_id]) ? 1/(t_float)mapOf_count_rows[row_id]  : 0; //! ie, the 'average mid-point'
	const t_float avg = (mapOf_valueSums_rows[row_id]) ? mapOf_valueSums_rows[row_id]*ncols_inv : 0;
#ifndef NDEBUG
// #warning "TODO[JC]: please validtat hat [below] 'exuaion' is intutive wrt. the web-page-desicpriton in our clusterC.html ... and thereafter also update our simliari 'handling' of columns .... and in this cotnext suggest/describe how the 'zero' ('0') case may be proeprly handled ... and thereafter update our kt_matrix_filter__func__cellMask__median_percent.c (ie, as the latter has similiar funcoitinaltiy)"
#endif      
#ifndef __macro__adjustValueInsteadOfMask
	const t_float thresh[2] = {avg - avg*min_cnt_rows, avg + avg*max_cnt_rows};
	// printf("[row=%u]\tavg=%f from sum=%f, ncols_inv=%f, ncols=%f/%u, given min-max=[%f, %f], and inputThresholds=[%f, %f], at %s:%d\n", row_id, avg, (float)mapOf_valueSums_rows[row_id], (float)ncols_inv, (float)mapOf_count_rows[row_id], self->ncols, thresh[0], thresh[1], min_cnt_rows, max_cnt_rows, __FILE__, __LINE__);
#endif
	if(ncols_inv == 0) {assert(mapOf_count_rows[row_id] == 0);} //! ie, as we otehrwise migth have a bug.
	if(avg != 0) {
	  for(uint col_id = 0; col_id < self->ncols; col_id++) {	    
	    if(isOf_interest(self->matrix[row_id][col_id])) {
#ifdef __macro__adjustValueInsteadOfMask
	      self->matrix[row_id][col_id] = mathLib_float_abs(avg - self->matrix[row_id][col_id]);
#else
	      if( (self->matrix[row_id][col_id] <= thresh[0]) || (self->matrix[row_id][col_id] >= thresh[1]) ) {
		self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
	      }
#endif
	    }
	  }
	}
      }
    }
  }
  if(isTo_evaluateFor_cols) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      if(self->mapOf_interesting_cols[col_id]) {
	//const uint nrows_inv = 1/self->nrows; //! ie, the 'average mid-point'
	const t_float nrows_inv = (mapOf_count_cols[col_id]) ? 1/(t_float)mapOf_count_cols[col_id]  : 0; //! ie, the 'average mid-point'
	const t_float avg = (mapOf_valueSums_cols[col_id]) ? mapOf_valueSums_cols[col_id]*nrows_inv : 0;
#ifndef __macro__adjustValueInsteadOfMask
	const t_float thresh[2] = {avg - avg*min_cnt_cols, avg + avg*max_cnt_cols};
#endif
	if(nrows_inv == 0) {assert(mapOf_count_cols[col_id] == 0);} //! ie, as we otehrwise migth have a bug.
	if(avg != 0) {
	  //if( (mathLib_float_abs(mapOf_valueSums_cols[col_id]) <= min_cnt_columns) ) {
	  //if( (mapOf_valueSums_cols[col_id] <= min_cnt_columns)  || (mapOf_valueSums_cols[col_id] >= max_cnt_columns) ) {
	  for(uint row_id = 0; row_id < self->nrows; row_id++) {
	    if(isOf_interest(self->matrix[row_id][col_id])) {
#ifdef __macro__adjustValueInsteadOfMask
	      self->matrix[row_id][col_id] = mathLib_float_abs(avg - self->matrix[row_id][col_id]);
#else
	      if( (self->matrix[row_id][col_id] <= thresh[0]) || (self->matrix[row_id][col_id] >= thresh[1]) ) {
		self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
	      }
#endif
	    }
	  }
	}
      }
    }
  }

  //! De-allcoate locally reserved memory:
  if(isTo_evaluateFor_rows) {free_1d_list_float(&mapOf_valueSums_rows);}
  if(isTo_evaluateFor_cols) {free_1d_list_float(&mapOf_valueSums_cols);}
  if(isTo_evaluateFor_rows) {free_1d_list_uint(&mapOf_count_rows);}
  if(isTo_evaluateFor_cols) {free_1d_list_uint(&mapOf_count_cols);}
 }

#undef __macro__computeFor__rows
#undef __macro__computeFor__cols
