
#if(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMax == 1) 		
	const bool isTo_findMaxValue = true; const bool isTo_findMinValue = false;
//printf("(config)\t choose-maX-value=true, at %s:%d\n", __FILE__, __LINE__);
#elif(TEMPLATE_distanceConfiguration_metric_concatenateResults_useMin == 1)		
	const bool isTo_findMaxValue = false; const bool isTo_findMinValue = true;
//printf("(config)\t choose-min-value=true, at %s:%d\n", __FILE__, __LINE__);
#else
	const bool isTo_findMaxValue = false; const bool isTo_findMinValue = false;
//printf("(config)\t sum-values=true, at %s:%d\n", __FILE__, __LINE__);
#endif
