#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "kt_list_1d.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
#include "kt_sparse_sim.h" //! which is used for sparse comptautions of simlairty-metircs.
#include "hpLysis_api.h" //! ie, the aPI used for clsutering.

/**
   @brief how to export (to a file) and import (from a file) a list of pairs/relations, and then use latter as input to a sparse simlairty-copmtaution.
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks a permtuation of our "tut_list_2_pairFloat_readFromFile_sim.c" where we compute a hierahrcical cluster (using the result from the simlairty-metric).
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#endif 
  const uint nrows = 5; const uint ncols = 3; 
  //!
  //! Specify the simlairty-metic: 
  s_kt_correlationMetric_t sim_metric = setTo_empty__andReturn__s_kt_correlationMetric_t();
  sim_metric.metric_id = e_kt_correlationFunction_groupOf_intersection_intersection;
  //! -----------------------------------------------------------------------------------
  //!
  //! Allocate:
  const uint list_size = nrows*ncols;
  s_kt_list_1d_pairFloat_t vec_1 = init__s_kt_list_1d_pairFloat_t(list_size);
  assert(vec_1.list_size == list_size);
  //!
  //! Set values:
  uint curr_pos = 0;
  const t_float distance_default = 1;
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      assert(curr_pos < vec_1.list_size);
      vec_1.list[curr_pos] = MF__initVal__s_ktType_pairFloat(row_id, col_id, distance_default);
      //! What we expect:
      s_ktType_pairFloat_t obj = vec_1.list[curr_pos];
      assert(obj.head == row_id);
      assert(obj.tail == col_id);
      assert(obj.score == distance_default);
      //! Increment:
      curr_pos++;
    }
  }
  assert(curr_pos == list_size);
  const char *file_tmp = "tut_tmpFile.tsv";
  { //! Export the [ªbove] data-set:
    FILE *fileP = fopen(file_tmp, "wb");
    assert(fileP);
    for(uint i = 0; i < vec_1.list_size; i++) {
      s_ktType_pairFloat_t obj = vec_1.list[i];
      fprintf(fileP, "%u\t%u\t%f\n", obj.head, obj.tail, obj.score);
    }
    fclose(fileP);
  }
  //!
  //! First de-allcoate [abov€] and then laod the input-file:
  free__s_kt_list_1d_pairFloat_t(&vec_1);
  //! Read the 'sparse colleciton' from the [ªbove] generated file:
  vec_1 = initFromFile__s_kt_list_1d_pairFloat_t(file_tmp);
  assert(vec_1.list_size == list_size); //! ie, what we expect.

  //!
  //! Convert into a 2d-list:
  s_kt_set_2dsparse_t sparse_1 = convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(&vec_1);
  assert(sparse_1._nrows == nrows); //! ie, gvien our [ªbove] isneriotn-policy.
  //!
  //! Compute simliarty:
  s_kt_sparse_sim obj_sim = init__s_kt_sparse_sim_t(sim_metric);
  //!
  //! Apply logics:
  //! Note: the [”elow] "mat_result" fucntion-call is simliar to our 'sparse-fucntion-call', where latter may be 'called' using: s_kt_set_2dsparse_t sparse_result = computeStoreIn__2dList__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/&sparse_2, /*isTo_rememberScores=*/true, /*weight=*/NULL);
  s_kt_matrix_base_t mat_result = computeStoreIn__matrix__s_kt_sparse_sim_t(&obj_sim, /*first-input=*/&sparse_1, /*second-input=*/NULL, /*weight=*/NULL);
  assert(mat_result.nrows > 0);
  assert(mat_result.ncols > 0);
  //! De-allcoate the sparse cofnigruation-object:
  free__s_kt_sparse_sim_t(&obj_sim);
  //! -------------------------------------------------------------- 
  //!
  { //! Apply cluster-based logics, and then write out the result:
    //! Cosntruct a shallow copy of the amtirx, and then export:
    s_kt_matrix_t mat_shallow = setToEmptyAndReturn__s_kt_matrix_t();
    mat_shallow.matrix = mat_result.matrix;
    mat_shallow.nrows = mat_result.nrows;
    mat_shallow.ncols = mat_result.ncols;
    //! Initate object
    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
    //! Apply cluster using an HCA-based algorithm:
    e_hpLysis_clusterAlg_t clustAlg = e_hpLysis_clusterAlg_HCA_single;
    bool is_ok = cluster__hpLysis_api(&obj_hp, clustAlg, &mat_shallow, /*nclusters=*/UINT_MAX, /*npass=*/10); //! where "nclusters" and "npass" are ignored as we use an HCA-based lagorithm.
    assert(is_ok);
    //! Export the result to a file:
    is_ok = export__hpLysis_api(&obj_hp, /*resultFile=*/"result_tut_hca.tsv", NULL, /*exportFormat=*/e_hpLysis_export_formatOf_similarity_matrix__syntax_TSV, NULL);
    assert(is_ok);
    //! De-allocate:
    free__s_hpLysis_api_t(&obj_hp);
  }
  

  //!
  //! De-allocates:
  free__s_kt_list_1d_pairFloat_t(&vec_1);
  free_s_kt_set_2dsparse_t(&sparse_1);
  free__s_kt_matrix_base_t(&mat_result);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}

