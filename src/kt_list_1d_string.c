#include "kt_list_1d_string.h"

//! Inititates an s_kt_list_1d_string_t object to empty (oekseth, 06. apr. 2017)
void setToEmpty__s_kt_list_1d_string_t(s_kt_list_1d_string_t *self) {
  assert(self);
  self->nrows = 0;
  self->nameOf_rows = NULL;
  self->biggestIndex_inserted_plussOne = 0;
}
//! @return a new-tianted s_kt_list_1d_string_t object (oekseth, 06. apr. 2017).
void init__s_kt_list_1d_string_t( s_kt_list_1d_string_t *self, const uint nrows) {
  assert(self);
  assert(nrows != UINT_MAX); assert(nrows > 0);
  self->nrows = nrows;
  self->biggestIndex_inserted_plussOne = 0;
  self->nameOf_rows =  (char**)alloc_generic_type_2d(char, self->nameOf_rows, self->nrows);
  for(uint i = 0; i < self->nrows; i++) {
    self->nameOf_rows[i] = NULL; //! ie, intiate.
  }
  //init__s_kt_list_1d_string_t(&self, nrows);
  //return self;
}



//! De-allcoate the s_kt_matrix_t object.
void free__s_kt_list_1d_string(s_kt_list_1d_string_t *self) {
  if(self->nameOf_rows) {
    /* if(self->nameOf__allocationScheme__allocatedSeperatelyInEach == false) { */
    /* 	free_2d_list_char(&(self->nameOf_rows));  */
    /* } else  */
    { //! then we de-allcoate seperately for each 'chunk':
      for(uint i = 0; i < self->nrows; i++) {
	if(self->nameOf_rows[i]) {
	  free_1d_list_char(&(self->nameOf_rows[i]));
	}
      }
      free_generic_type_2d(self->nameOf_rows); 
    }
    self->nameOf_rows = NULL;
    self->nrows = 0;
    self->biggestIndex_inserted_plussOne = 0;
  }
}

/**
   @brief specify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos> is the id of the internla column or row to udpdate
   @param <stringTo_add> is the string to copy into this strucutre
 **/
void set_stringConst__s_kt_list_1d_string(s_kt_list_1d_string_t *self, const uint index_pos, const char *stringTo_add) {
  assert(self); 
  char **arr_local = NULL;
  if(!stringTo_add || !strlen(stringTo_add)) {
    fprintf(stderr, "!!\t String=\"%s\" seems empty. In brief we regard the latter as an inocnsistnecy, ie, please udpate your intaiton-funciton. For questions please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringTo_add, __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  {
    if(index_pos >= self->nrows) { //! then we allcoate a enw object, and update the cotnants of this:
      const uint new_size = (index_pos+12)*2;
      s_kt_list_1d_string_t new_obj; init__s_kt_list_1d_string_t(&new_obj, new_size);
      for(uint i = 0; i < self->nrows; i++) {
	new_obj.nameOf_rows[i] = self->nameOf_rows[i]; //! ie, 'grab' the content.
	self->nameOf_rows[i] = NULL; //! ie, clear.
      }      
      //! De-allcoate 'ours':
      free__s_kt_list_1d_string(self);
      //! Updae the pointer:
      *self = new_obj;
    } 
    if(index_pos >= self->nrows) {
      fprintf(stderr, "!!\t For string=\"%s\" you have requested a row-string-inseriton at index=%u >= %u=nrows. In brief we regard the latter as an inocnsistnecy, ie, please udpate your intaiton-funciton. For questions please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringTo_add, index_pos, self->nrows, __FUNCTION__, __FILE__, __LINE__);
      return;
    }
    if(self->nameOf_rows == NULL) {
      self->nameOf_rows =  (char**)alloc_generic_type_2d(char, self->nameOf_rows, self->nrows);
      for(uint i = 0; i < self->nrows; i++) {
	self->nameOf_rows[i] = NULL; //! ie, intiate.
      }
      //self->nameOf_rows =  (char**)alloc_generic_type_2d(char, &(self->nameOf_rows), self->nrows);
      //self->nameOf__allocationScheme__allocatedSeperatelyInEach = true;
    }
    //! Specify the data-structre to update:
    arr_local = self->nameOf_rows;
  }
  //! ------------------
  //!
  //! Allocate memory and copy-paste teh strucutre:
  const uint str_len = strlen(stringTo_add) + 1;
  assert(str_len > 0);
  const char emppty_char = 0;
  char *new_string = allocate_1d_list_char(str_len, emppty_char);
  memcpy(new_string, stringTo_add, str_len-1);
  //! Udpate our 'global' struycutre with the new string:
  assert(arr_local);
  if(arr_local[index_pos] != NULL) {
    // FIXME: is below correct (oekseth, 06.08.2019).
    if(index_pos > self->biggestIndex_inserted_plussOne) {
      free_1d_list_char(&(arr_local[index_pos]));
      arr_local[index_pos] = NULL;
    }
  }
  //  printf("(add-string)\t [%u]=\"%s\", at %s:%d\n", index_pos, new_string, __FILE__, __LINE__);
  arr_local[index_pos] = new_string;
  if(index_pos > self->biggestIndex_inserted_plussOne) {
    self->biggestIndex_inserted_plussOne = index_pos + 1;
  }
}

//! Perform a  lienar searhc for "str_search" and insert if "insertIf_notFound = true" (oekseth, 06. jul. 2017).
//! @returnt he index which the string was inserted at: if not inserted (and not found) then UINT_MAX is returned. 
uint get_indexOf_string_slowSearch__s_kt_list_1d_string_t(s_kt_list_1d_string_t *self, const char *str_search, const bool insertIf_notFound) {
  assert(self);
  assert(str_search); assert(strlen(str_search));
  uint foundAt_index = UINT_MAX;
  if(self && self->biggestIndex_inserted_plussOne) {
    for(uint i = 0; (i < self->biggestIndex_inserted_plussOne) && (foundAt_index == UINT_MAX); i++) {
      const char *str_local = self->nameOf_rows[i];
      //! Note: [”elow] asserts are palced at differne lines to simplify debugign when erors ocurre.
      assert(str_local); 
      assert(strlen(str_local));
      if(strlen(str_local) == strlen(str_search)) {
	if(0 == strcmp(str_local, str_search)) {foundAt_index = i;} //! then the styrings are euqal, ie, for which the strings are found.
      }
    }
  }
  if( (foundAt_index == UINT_MAX) && insertIf_notFound) { //! then add the string:
    set_stringConst__s_kt_list_1d_string(self, self->biggestIndex_inserted_plussOne, str_search);
    foundAt_index = self->biggestIndex_inserted_plussOne;
  }
  return foundAt_index;
}


//! Identify the index for the given string (ie, if the stirng is found) (oekseht, 06. des. 2016)
//! @return row_id if foudn: otherwise UINT_MAX is retunred (ie, as we then assume the string is Not found).
//! @remarks this fucntion is Not set to 'inline' as we asusme there will be now signficnat perfomrance-costs assicated to thsi funciton (ie, due to the relative low number of access which we expect).
uint getIndexOf__atRow__s_kt_list_1d_string(const s_kt_list_1d_string_t *self, const char *stringOf_value) {
  assert(self); assert(stringOf_value); assert(strlen(stringOf_value));
  if(self->nameOf_rows && self->nrows) {
    for(uint row_id = 0; row_id < self->nrows; row_id++) {
      const char *stringOf_cmp = self->nameOf_rows[row_id];
      if(stringOf_cmp && (strlen(stringOf_cmp) == strlen(stringOf_value))) {
	if(0 == strcmp(stringOf_cmp, stringOf_value)) {return row_id;}
      }
    }
  }
  //! At this exec-point we assume the string was Not found:
  return UINT_MAX;
}

//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_string_t initFromFile_sizeKnown_s_kt_list_1d_string_t(const char *input_file, const uint _cnt_rows) {
  s_kt_list_1d_string_t self; init__s_kt_list_1d_string_t(&self, _cnt_rows);
  assert(input_file); assert(strlen(input_file));
  /* if(selfncols == 0) { */
  /*   fprintf(stderr, "!!\t Erronous usage: we expects the matirx to be loaded Before the weight-table, ie, to simplify our cosnsitnecy-checkds: for queisotns please cotnact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__); */
  /*   return self; */
  /* } */
  
  FILE *file = fopen(input_file, "r");
  if (file == NULL) {
    fprintf(stderr, "!!\t Unable to open the file \"%s\", at [%s]:%s:%d\n", input_file, __FUNCTION__, __FILE__, __LINE__);
    return self;
  }
  //  assert(self->weight != NULL);
  uint cnt_rows = 0; uint cnt_tabs = 0;
  uint word_buffer_size = 1280; uint word_buffer_currentPos = 0;
  char empty_0 = '0';
  char *word_buffer = allocate_1d_list_char(word_buffer_size, empty_0); //[128] = {'\0'}; //! ie, as a number having mroe then 128 digits is aboute the T_FLOAT_MAX limit (oesketh, 06. jul. 2017).
  memset(word_buffer, '\0', word_buffer_size);
  char c = '\0';
  while ((c = getc(file)) != EOF) {
    //    if(c == '\t') {cnt_tabs++;}
    //    else 
    if(c == '\n') {
      /* const t_float val = (t_float)atof(word_buffer); */
      /* //! Note: for cases where the first column is a string-id, we use an column-offset of "-1": */
      /* if(false) { */
      /* 	//assert(pos_in_column_0_prev <= 1000); */
      /* 	char buffer_local strncpy(buffer_local, word_buffer, word_buffer_currentPos); */
      /* 	printf("[%u][%u] = \"%s\"=%f, at %s:%d\n", cnt_rows, cnt_tabs-1, buffer_local, val,  __FILE__, __LINE__); */
      /* } */
      //assert(val != INFINITY);       assert(val != NAN);
      //!
      if(word_buffer_currentPos > 1) {
	//! Iterate back-words and remove trailing blank lines:
	bool isWhite = true;
	int pos = (int)word_buffer_currentPos-1;
	for(uint i = 0; i < word_buffer_currentPos; i++) {
	  if(isWhite && isblank(word_buffer[pos])) {
	    assert(pos >= 0);
	    word_buffer[pos] = '\0'; //! ie, replac ehte 'blank' whit a 'line-end-char'.
	    pos--;
	  } else {isWhite = false;}
	}

	//! Insert:
	set_stringConst__s_kt_list_1d_string(&self, cnt_rows, word_buffer);
      }
      //self->weight[cnt_rows] = val;      
      cnt_rows++;
      /* if(cnt_rows > self->ncols) { */
      /* 	fprintf(stderr, "!!\t Your input-weight-file has more elements than the reserved number of columns, ie, where we expected %u <= %u, an error whcih could arise if your are using the 'tranposed option' in-cosnistnely: we expec thte weights to be deifned for the columsn (and not rows) For questions please contact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_rows, self->ncols, __FUNCTION__, __FILE__, __LINE__); */
      /* } */
      //! Reset:
      for(uint i = 0; i < word_buffer_size; i++) {word_buffer[i] = '\0';}
      word_buffer_currentPos = 0;
    } else { //! then inser tthe value in the current word-buffer-chunk:
      if((word_buffer_currentPos > 0) || (isblank(c) == false) ) {
	if(word_buffer_size <= word_buffer_currentPos) {
	  uint new_size = (word_buffer_currentPos + word_buffer_size); //*2;
	  assert(new_size > 0);
	  char *buff_new = allocate_1d_list_char(new_size, empty_0);
	  memset(buff_new, '\0', new_size);
	  memcpy(buff_new, word_buffer, sizeof(char)*word_buffer_size);
	  word_buffer_size = new_size;
	  free_1d_list_char(&word_buffer);
	  word_buffer = buff_new;
	  assert(word_buffer_size > word_buffer_currentPos);
	}
	word_buffer[word_buffer_currentPos] = c; 
	word_buffer_currentPos++;
      } //! else we ignore the character.
    }
  }
  if(cnt_tabs > 0) {
    fprintf(stderr, "!!\t Erronous usage: cnt-tabs=%u in a fiel with %u new-lines: as we expectted only one valeu to be found in each row the latter observaiton is belived to indicate an error: for questions please contact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", cnt_tabs, cnt_rows, __FUNCTION__, __FILE__, __LINE__);
    return self; //false;
  }
  free_1d_list_char(&word_buffer);
  //! -----------------------------
  //!
  //! Reset:
  fclose(file);
  
  //! -----------------------------
  //!
  //! At this exueciton-poitn we asusme the oepraiotn 'wenth smooth':
  return self;  
}

//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_string_t initFromFile__s_kt_list_1d_string_t(const char *input_file) {
  s_kt_list_1d_string_t obj_string = initFromFile_sizeKnown_s_kt_list_1d_string_t(input_file, /*cnt_rows=*/1000);
  return obj_string;

}
