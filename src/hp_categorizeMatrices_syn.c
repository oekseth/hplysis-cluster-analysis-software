#include "hp_categorizeMatrices_syn.h" 
#include "aux_sysCalls.h"

static s_kt_matrix_t __private__exportMatrix(const s_kt_list_1d_float_t *listOf_scores, const uint listOf_scores_size, const uint listOf_scores_size_xmtRandomization, const s_kt_list_1d_string_t listOf_names, const char *fileName_toExportTo, s_kt_matrix_t *mat_result_xmtRandomization) {
  assert(listOf_scores); assert(listOf_scores_size); assert(listOf_names.nrows);
  assert(fileName_toExportTo); assert(strlen(fileName_toExportTo));
  assert(listOf_scores_size_xmtRandomization < listOf_scores_size); //! whcih is sued to simplify our logics.
  //!
  //! Costnruct matrix: 
  assert(listOf_names.nrows > 0);

  uint max_name_id = 0;
  for(uint i = 0; i < listOf_names.nrows; i++) {
    const char *string = listOf_names.nameOf_rows[i];
    if(string) {max_name_id++;}
  }
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(max_name_id, listOf_scores_size);
  //s_kt_matrix_t 
  *mat_result_xmtRandomization = initAndReturn__s_kt_matrix(max_name_id, listOf_scores_size_xmtRandomization);
  //!
  //! Set column-names:
  for(uint i = 0; i < listOf_scores_size; i++) {
    const char *string = get_shortString__e_kt_categorizeMatrices_subCases__cmpFunction_t((e_kt_categorizeMatrices_subCases__cmpFunction_t)i);
    //getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i);
    assert(string); assert(strlen(string));
    //! Set column-names:
    set_stringConst__s_kt_matrix(&mat_result, i, string, /*addFor_column=*/true);
    if(i < listOf_scores_size_xmtRandomization) {
      if(mat_result_xmtRandomization) {
	set_stringConst__s_kt_matrix(mat_result_xmtRandomization, i, string, /*addFor_column=*/true);      
      }
    }
  }
  //!
  //! Iterate:
  for(uint i = 0; i < max_name_id; i++) {
    const char *string = listOf_names.nameOf_rows[i];
    if(!string) { //! then we asusem 'space' has Not been allcoated/erserved for the latter.
      //fprintf(stderr, "[%u] naem nto set, at %s:%d\n", i, __FILE__, __LINE__);
      continue;
    }
    assert(string); assert(strlen(string));
    //! Set row-names:
    set_stringConst__s_kt_matrix(&mat_result, i, string, /*addFor_column=*/false);
    //    if(i < listOf_scores_size_xmtRandomization) {
    if(mat_result_xmtRandomization) {
      set_stringConst__s_kt_matrix(mat_result_xmtRandomization, i, string, /*addFor_column=*/false);
    }
      //    }
    //! 
    //! Update the matrix: 
    for(uint col_id = 0; col_id < listOf_scores_size; col_id++) {
      const uint list_size = listOf_scores[/*property=*/col_id].list_size;
      if(i < list_size) {
	const t_float score = listOf_scores[/*property=*/col_id].list[/*row=*/i]; //! ie, where we for each proerpty 'fetch' the score asiscated tot eh vertex
	mat_result.matrix[i][col_id] = score;
	if(col_id < listOf_scores_size_xmtRandomization) {
	  if(mat_result_xmtRandomization) {
	    mat_result_xmtRandomization->matrix[i][col_id] = score;
	  }
	}
      }
    }
  }  
  { //! Export the 'full' matrix:
    //!
    //! Export: Write out to the "fileName_toExportTo" result-file:
    assert(mat_result.nrows > 0);
    bool is_ok = export__singleCall__s_kt_matrix_t(&mat_result, fileName_toExportTo, NULL);
    assert(is_ok);
    //! 
    //! Comptue devaiotns: Exprot both a 'nroaml' and a transposed/itlted matrix (oekseth, 06. apr. 2017):
    allocOnStack__char__sprintf(2000, fileName_toExportTo_deviation, "%s_%s.tsv", fileName_toExportTo, "deviation");  //! ie, allcoate the "str_filePath".
    is_ok = extractAndExport__deviations__singleCall__transposedAndNonTransposed_tsv__s_kt_matrix_t(&mat_result, fileName_toExportTo_deviation);
    assert(is_ok);
  }
  if(mat_result_xmtRandomization)  { //! Export the 'xmt-randomzionat' matrix:
    //!
    //! Export: Write out to the "fileName_toExportTo" result-file:
    allocOnStack__char__sprintf(2000, fileName_toExportTo_deviation_xmtRandom, "%s_%s.tsv", fileName_toExportTo, "xmtRandom");  //! ie, allcoate the "str_filePath".
    assert(mat_result_xmtRandomization->nrows > 0);
    bool is_ok = export__singleCall__s_kt_matrix_t(mat_result_xmtRandomization, fileName_toExportTo_deviation_xmtRandom, NULL);
    assert(is_ok);
    //! 
    //! Comptue devaiotns: Exprot both a 'nroaml' and a transposed/itlted matrix (oekseth, 06. apr. 2017):
    allocOnStack__char__sprintf(2000, fileName_toExportTo_deviation, "%s_%s.tsv", fileName_toExportTo, "xmtRandom_deviation");  //! ie, allcoate the "str_filePath".
    is_ok = extractAndExport__deviations__singleCall__transposedAndNonTransposed_tsv__s_kt_matrix_t(mat_result_xmtRandomization, fileName_toExportTo_deviation);
    assert(is_ok);
  }
  //!
  //! De-allocate: 
  //free__s_kt_matrix(mat_result_xmtRandomization);
  //free__s_kt_matrix(&mat_result);
  return mat_result;  
}

//! Normalise socres: score(i) = (score(i) - AVG(score))/(max(score)-AVG(score)) ... ie, to avodi 'large differences in scales' from clustteirng the 'ability' of our 'signature-comarpsion-appraoch'
static void __normalizeRow(const t_float *old_data, t_float *result, const uint ncols) {
  assert(old_data); assert(result); assert(ncols > 0);
  t_float avg = 0; t_float max = T_FLOAT_MIN_ABS;
  uint n = 0;
  for(uint i = 0; i < ncols; i++) {
    if( (old_data[i] != T_FLOAT_MAX) && !isinf(old_data[i])) {
      max = macro_max(max, old_data[i]);
      avg += old_data[i];
      n++;
    }
  }
  if((avg!= 0) && n) {avg = avg / n;}
  //!
  //! Set the data:
  for(uint i = 0; i < ncols; i++) {
    if( (old_data[i] != T_FLOAT_MAX) && !isinf(old_data[i])) {
      t_float numerator = old_data[i] - avg;
      t_float de_numerator = max - avg;
      t_float score = 1; //! ie, a center-point. old_data[i]; //! ie, 
      if( (numerator != 0) && (de_numerator != 0) ) {
	score = numerator / de_numerator;
	assert(score != T_FLOAT_MAX);
      }
      result[i] = score;
    }
  }  
}

static void __evaluateAndCompute__hypothesis(s_hp_categorizeMatrices_syn_t *self, const s_kt_matrix_t mat_result, s_kt_correlationMetric_t *arrOf_sim_postProcess, const uint arrOf_sim_postProcess_size, const char *stringOf_resultPath) {
  assert(mat_result.matrix);
  //!
  //! Costnruct matrix: 
  const uint cnt_randomness__sameDsitributions = 3;      const uint cnt_randomness__categoryCnt = 3; 
  const uint hypoEval_rows = arrOf_sim_postProcess_size * (uint)e_kt_categorizeMatrices_subCases__hypo_undef*(cnt_randomness__sameDsitributions + cnt_randomness__categoryCnt + 1);
  const uint hypoEval_cols = e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
  assert(hypoEval_rows > 0);
  s_kt_matrix_t mat_result_hypo = initAndReturn__s_kt_matrix(hypoEval_rows, hypoEval_cols);
  assert(mat_result_hypo.nrows > 0);
  { //! Set the column-names:
    for(uint i = 0; i < hypoEval_cols; i++) {
      const char *string = getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i);
      assert(string); assert(strlen(string));
      //! Set column-names:
      set_stringConst__s_kt_matrix(&mat_result_hypo, i, string, /*addFor_column=*/true);	  
    }
  }
  //fprintf(stderr, "stringOf_resultPath=\"%s\", at %s:%d\n", stringOf_resultPath, __FILE__, __LINE__);
  //!
  //! Iterate:
  uint row_currentPos = 0;
  for(uint sim_id = 0; sim_id < arrOf_sim_postProcess_size; sim_id++) {
    //s_kt_matrix_t mat_result_simMetric = setToEmptyAndReturn__s_kt_matrix_t();
    //! Comptue the simlarity-metic.
    s_kt_matrix_base_t mat_1 = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result);
    s_kt_matrix_base_t mat_result_simMetric = initAndReturn__empty__s_kt_matrix_base_t();
    assert(mat_1.matrix);
    const bool is_ok = apply__hp_distance(arrOf_sim_postProcess[sim_id], &mat_1, NULL, &mat_result_simMetric, init__s_hp_distance__config_t());
    assert(is_ok);
    /* s_kt_matrix_t mat_result_simMetric = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result_simMetric); */
    /* free__s_kt_matrix_base_t(&mat_result_simMetric_base); //! ie, as the later is 'given' to our "mat_result_simMetric_base" object. */
    
    assert(mat_result_simMetric.ncols == mat_result_simMetric.nrows);
    //fprintf(stderr, "stringOf_resultPath=\"%s\", at %s:%d\n", stringOf_resultPath, __FILE__, __LINE__);
    //! Then evlauate hypothesis using our ... 
    for(uint hyp = 0; hyp < e_kt_categorizeMatrices_subCases__hypo_undef; hyp++) {      
      const s_kt_list_1d_uint_t list_hyp = self->listOf_hyp[hyp];
      const char *string_hyp = get_shortString__e_kt_categorizeMatrices_subCases__hypo_t((e_kt_categorizeMatrices_subCases__hypo_t)hyp);
      assert(row_currentPos < hypoEval_rows);
      allocOnStack__char__sprintf(8000, string_head, "%s %s::%s", string_hyp, get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(arrOf_sim_postProcess[sim_id].metric_id), get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t(arrOf_sim_postProcess[sim_id].typeOf_correlationPreStep));
      //! Set row-names:
      set_stringConst__s_kt_matrix(&mat_result_hypo, row_currentPos, string_head, /*addFor_column=*/false);	  	  
      //! 
      //! Inveitgete if the hyptoehssiis used, ei, if differnet vlaues are givne:
      uint value_min = UINT_MAX; uint value_max = 0;
      for(uint i = 0; i < list_hyp.list_size; i++) {
	const uint score = list_hyp.list[i];
	if(score != UINT_MAX) { //T_FLOAT_MAX) {
	  value_min = macro_min(value_min, score);
	  value_max = macro_max(value_max, score);
	  // printf("value_max=%u, given score=%u, at %s:%d\n", value_max, score, __FILE__, __LINE__);
	}
      }
      // printf("string_head=\"%s\", cateogr-range=[%u, %u], at %s:%d\n", string_head, value_min, value_max,  __FILE__, __LINE__);
      if( (value_max != 0) && (value_max != value_min) ) {
	if(list_hyp.list_size < mat_result_simMetric.ncols) { //! ie, where a ">" is merely there result of a 'generic' list-size-increase', ie which expalins why the oeprator " == "  is Not used.
	  //! 
	  //! Then we need to extend the list to 'cover' these cases ... itnroducign an alterntive 'no-described-case'.
	  fprintf(stderr, "!!\t There are 'inserted items' which have Not been cossntely given a hypothesis. To say it more difficult: Not all hyptoesis inserted for the data-rows of |features=\"%s\"|=%u, ie, givne an inptuf-eaturematrix w/dims=[%u, %u]. In brief please request suppro tfor the latter funcotnatliy, contacting senior delvoepoer [eosekth@gmail.com]. Observaiton at [%s]:%s:%d\n", string_hyp, list_hyp.list_size, mat_result_simMetric.nrows, mat_result_simMetric.ncols, __FUNCTION__, __FILE__, __LINE__);
	  assert(false); //! ie, an ehads-up in debugging-mode.
	  continue;
	  /* assert(false); // FIXME: ... to handle 'less than elements' */
	  /* assert(false); // FIXME: ... to handle 'greater-than-elements'	       */
	}
#define __MiF__apply__CCM(list) ({ for(uint i = 0; i < hypoEval_cols; i++) { /*! Comptue hypothesis: */ \
	      t_float scalar_score = T_FLOAT_MAX; const bool is_ok = ccm__singleMatrix__hp_ccm((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i, &mat_result_simMetric, list, &scalar_score); \
	      /*printf("scalar_score=%f, at %s:%d\n", scalar_score, __FILE__, __LINE__);*/ \
	      assert(row_currentPos < hypoEval_rows);			\
	      if( (scalar_score != T_FLOAT_MAX) && (scalar_score != T_FLOAT_MIN_ABS) && (scalar_score != T_FLOAT_MIN) ) {   /*! then store the result in our matrix: */ \
		assert(i < hypoEval_cols);				\
		mat_result_hypo.matrix[row_currentPos][i] = scalar_score; \
	      } } })
	{ //! For the data-set:
	  assert(row_currentPos < hypoEval_rows);
	  __MiF__apply__CCM(list_hyp.list);
	  //! Incrmenet:
	  assert(row_currentPos < hypoEval_rows);
	  row_currentPos++; //! ie, 'to go to a enw inseriotn-row'.
	}
	//! Use a random-sample-appraoch where we use nthe value-dsitribution of the 'true' hyptoehsis-assumption.
	for(uint rand_sample = 0; rand_sample < cnt_randomness__sameDsitributions; rand_sample++) { //! For a random permtaution of the data-set, ie, usign the same dsitribution:
	  //! Set the string-identifer_
	  allocOnStack__char__sprintf(8000, string_head_rand, "%s::rand_swap_%u", string_head, rand_sample);
	  //! Set row-names:
	  set_stringConst__s_kt_matrix(&mat_result_hypo, row_currentPos, string_head_rand, /*addFor_column=*/false);	  	  	      
	  //! Random-assing:
	  uint *arrOf_members = math_generateDistribution__randomPermutation__uint(list_hyp.list, mat_result_simMetric.ncols, /*list_size_cntToSwap=*/mat_result_simMetric.ncols, /*config_swapsMayOverlap=*/true, /*maxValue_toSetInRandomization=*/UINT_MAX);
	  assert(row_currentPos < hypoEval_rows);
	  assert(arrOf_members != list_hyp.list); //! ie, as we expec the lsit to be new-allcoated rahte rthan a copy.
	  __MiF__apply__CCM(arrOf_members);
	  /* uint *arrOf_members = allocate_1d_list_uint(hypoEval_cols, empty_0); */
	  /* for(uint i = 0; i < list_hyp.list_size; i++) {arrOf_members[i] = list_hyp.list[i];} //! ie, copy: */
	  
	  //! De-allocate:
	  assert(arrOf_members); free_1d_list_uint(&arrOf_members); arrOf_members = NULL;
	  //! Incrmenet:
	  row_currentPos++; //! ie, 'to go to a enw inseriotn-row'.
	}
	//! Use a random-sample-appraoch where we use the max-cluster-id as an approxmation of the cluster-coutn to evlauate/use.
	for(uint rand_sample = 0; rand_sample < cnt_randomness__sameDsitributions; rand_sample++) { //! For a random permtaution of the data-set, ie, usign the same dsitribution:
	  //! Set the string-identifer_
	  allocOnStack__char__sprintf(8000, string_head_rand, "%s::rand_rand_%u", string_head, rand_sample);
	  //! Set row-names:
	  set_stringConst__s_kt_matrix(&mat_result_hypo, row_currentPos, string_head_rand, /*addFor_column=*/false);	  	  	      
	  //! Random-assing:
	  const uint empty_0 = 0;
	  uint *arrOf_members = allocate_1d_list_uint(mat_result_simMetric.ncols, empty_0);
	  
	  uint max_cluster_id = 0; for(uint i = 0; i < mat_result_simMetric.ncols; i++) {max_cluster_id = macro_max(max_cluster_id, list_hyp.list[i]);}  max_cluster_id++; //! ie, find max-clsuter-id
	  for(uint i = 0; i < mat_result_simMetric.ncols; i++) {arrOf_members[i] = i;} //! ie, intalise.
	  //for(uint i = 0; i < list_hyp.list_size; i++) {arrOf_members[i] = list_hyp.list[i];} //! ie, copy:
	  math_generateDistribution__randomassign(max_cluster_id, mat_result_simMetric.ncols, list_hyp.list);
	  // uint *arrOf_members = math_generateDistribution__randomPermutation__uint(list_hyp.list, list_hyp.list_size, /*list_size_cntToSwap=*/list_hyp.list_size, /*config_swapsMayOverlap=*/true, /*maxValue_toSetInRandomization=*/UINT_MAX);
	  assert(row_currentPos < hypoEval_rows);
	  __MiF__apply__CCM(arrOf_members);
	  
	  //! De-allocate:
	  assert(arrOf_members); free_1d_list_uint(&arrOf_members); arrOf_members = NULL;
	      //! Incrmenet:
	  row_currentPos++; //! ie, 'to go to a enw inseriotn-row'.
	}
      }
    }
  }
  if(row_currentPos > 0) {
    //fprintf(stderr, "stringOf_resultPath=\"%s\", at %s:%d\n", stringOf_resultPath, __FILE__, __LINE__);
    //!
    //! Export: Write out to the "fileName_toExportTo" result-file:
    allocOnStack__char__sprintf(2000, fileName_toExportTo, "%s_simHypoEval__%s.tsv", stringOf_resultPath, "ccm");  //! ie, allcoate the "str_filePath".
    //fprintf(stderr, "analsye-CCM-scores ... results-into-file=\"%s\", given stringOf_resultPath=\"%s\"\n", fileName_toExportTo, stringOf_resultPath);
    const uint old_nrows = mat_result_hypo.nrows; mat_result_hypo.nrows = row_currentPos; //! ie, to avoid 'emtpy rows' from beign pritned.  
    assert(mat_result_hypo.nrows > 0);
    const bool is_ok = export__singleCall__s_kt_matrix_t(&mat_result_hypo, fileName_toExportTo, NULL);
    assert(is_ok);
    mat_result_hypo.nrows = old_nrows; //! ie, to avodi any 'ptoential' emory-leaks and 'amitnain' the itnegraty/ocnsisnteyc of the object. 
    {//! 
      //! Comptue devaiotns: Exprot both a 'nroaml' and a transposed/itlted matrix (oekseth, 06. apr. 2017):
      allocOnStack__char__sprintf(2000, fileName_toExportTo_deviation, "%s_%s.tsv", fileName_toExportTo, "deviation");  //! ie, allcoate the "str_filePath".
      const bool is_ok = extractAndExport__deviations__singleCall__transposedAndNonTransposed_tsv__s_kt_matrix_t(&mat_result_hypo, fileName_toExportTo_deviation);
      assert(is_ok);
    }
    
    { //! TRanpsoe+rank the data-matrix, an approach used to simplify inveitgiant of the msot sigicnat hypothesis (for eahc cCM):
      s_kt_matrix_t mat_transp = setToEmptyAndReturn__s_kt_matrix_t(); const bool is_ok = init__copy_transposed__thereafterRank__s_kt_matrix(&mat_transp, &mat_result_hypo, true);
      assert(is_ok);
      //! Valdiate some base-dinciatros wrt. the transpoed operaiton: 
      assert(mat_transp.ncols == mat_result_hypo.nrows);
      assert(mat_transp.nrows == mat_result_hypo.ncols);
      assert(mat_transp.ncols >= row_currentPos); //! ie, as the amtrix is transpsioed.
      //!
      //! Export: Write out to the "fileName_toExportTo" result-file:
      allocOnStack__char__sprintf(2000, fileName_trans, "%s_transposedAndRank.tsv", fileName_toExportTo);  //! ie, allcoate the "str_filePath".
      //fprintf(stderr, "analsye-CCM-scores ... results-into-file=\"%s\", given stringOf_resultPath=\"%s\"\n", fileName_toExportTo, stringOf_resultPath);
      const uint old_cols = mat_result_hypo.ncols; mat_result_hypo.ncols = row_currentPos; //! ie, to avoid 'emtpy rows' from beign pritned.
      assert(mat_transp.nrows > 0);
      const bool is_ok_e = export__singleCall__s_kt_matrix_t(&mat_transp, fileName_trans, NULL);
      assert(is_ok_e);
      mat_result_hypo.ncols = old_cols; //! ie, to avodi any 'ptoential' emory-leaks and 'amitnain' the itnegraty/ocnsisnteyc of the object. 
      //!
      //! De-allocate: 
      free__s_kt_matrix(&mat_transp);
    }
  } //! else we assuem no hypotehssi matched the searhc-spec
  //!
  //! De-allocate: 
  free__s_kt_matrix(&mat_result_hypo);
}


void free__s_hp_categorizeMatrices_syn_t(s_hp_categorizeMatrices_syn_t *self, const bool isTo_export, s_kt_correlationMetric_t *arrOf_sim_postProcess, const uint arrOf_sim_postProcess_size) {
  //const char *stringOf_resultPath = self->stringOf_resultPath;
  if(isTo_export) {
    assert(self->stringOf_resultPath);
    allocOnStack__char__sprintf(8000, stringOf_resultPath, "%s_%s", self->stringOf_resultPath, self->stringOf_resultTag);
    if(self->stringOf_resultTag && strlen(self->stringOf_resultTag)) {
      const char *str = self->stringOf_resultTag;
      if(str[strlen(str)-1] == '/') { //! then we need to esnrue taht the directory exists
	allocOnStack__char__sprintf(2000, str_cmd, "mkdir -p %s", stringOf_resultPath); //! ie, creat eht direcotry (if directy does No0t relayd exists).
	MF__system(str_cmd);
      }    
    }
    // fprintf(stderr, "stringOf_resultPath=\"%s\", at %s:%d\n", stringOf_resultPath, __FILE__, __LINE__);
    //!
    //! construct and export matrices:
    
    assert(stringOf_resultPath);
    allocOnStack__char__sprintf(2000, prefix__scores_input, "%s_simToSynt__%s.tsv", stringOf_resultPath, "fromDataInput");  //! ie, allcoate the "str_filePath".
    s_kt_matrix_t mat_result_xmtRandom = setToEmptyAndReturn__s_kt_matrix_t();
    printf("(status::postProccess)\t Export feature-matrices, given stringOf_resultPath=\"%s\", at %s:%d\n", stringOf_resultPath, __FILE__, __LINE__);
    s_kt_matrix_t mat_result = __private__exportMatrix(self->listOf_scores__max, /*size=*/e_kt_categorizeMatrices_subCases__cmpFunction_undef, /*size-xmtRandom=*/(uint)e_kt_categorizeMatrices_subCases__cmpFunction_random_1, self->listOf_names, prefix__scores_input, &mat_result_xmtRandom);
    assert(mat_result_xmtRandom.matrix);

    // fprintf(stderr, "stringOf_resultPath=\"%s\", at %s:%d\n", stringOf_resultPath, __FILE__, __LINE__);    
    //!
    //! 
 
    if(arrOf_sim_postProcess && arrOf_sim_postProcess_size) {//! Apply the 'defualt' set of 'base-simliarty-meitrcs':
      assert(mat_result.matrix);
      printf("(status::postProccess)\t Evaluate differen hypothesis, given stringOf_resultPath=\"%s\", at %s:%d\n", stringOf_resultPath, __FILE__, __LINE__);
      __evaluateAndCompute__hypothesis(self, mat_result, arrOf_sim_postProcess, arrOf_sim_postProcess_size, stringOf_resultPath);
      //! Seprately evaluate for the non-random case:
      allocOnStack__char__sprintf(8000, stringOf_resultPath_xmtRandom, "%s_%s", stringOf_resultPath, "xmtRandom");
      assert(mat_result_xmtRandom.matrix);
      printf("(status::postProccess)\t Evaluate differen hypothesis--xmtRandomness, given stringOf_resultPath=\"%s\", at %s:%d\n", stringOf_resultPath, __FILE__, __LINE__);
      __evaluateAndCompute__hypothesis(self, mat_result_xmtRandom, arrOf_sim_postProcess, arrOf_sim_postProcess_size, stringOf_resultPath_xmtRandom);
    }
    //!
    //! De-allocate
    free__s_kt_matrix(&mat_result);
    free__s_kt_matrix(&mat_result_xmtRandom);
    /* allocOnStack__char__sprintf(2000, prefix__scores_simMe, "%s_simToSynt__%s.tsv", self->stringOf_resultPath, "fromSimMatrix");  //! ie, allcoate the "str_filePath". */
    /* __private__exportMatrix(self->listOf_scores__simMe, /\*size=*\/e_kt_categorizeMatrices_subCases__cmpFunction_undef, self->listOf_names, prefix__scores_simMe); */
  }
  //!
  //! De-allcoate: 
  free__s_kt_list_1d_string(&(self->listOf_names));
  for(uint i = 0; i < e_kt_categorizeMatrices_subCases__cmpFunction_undef; i++) {
    free__s_kt_list_1d_float_t(&(self->listOf_scores__max[i]));
    //free__s_kt_list_1d_float_t(&(self.listOf_scores__simMe[i]));
  }
  for(uint i = 0; i < e_kt_categorizeMatrices_subCases__hypo_undef; i++) {
    free__s_kt_list_1d_uint_t(&(self->listOf_hyp[i]));
  }
}

//! Cosntruct a new sample-data-set withdiemsions usignt eh "ncols" as a 'basis'.
static s_kt_matrix_t __construct_sampleData(const uint ncols, const uint func_id, e_kt_categorizeMatrices_typeOfAdjust_synAndInput_t defaultAdjustment_ofSyn, const t_float syn__adjsutVAleuBy) {
//static s_kt_matrix_t __construct_sampleData(const uint ncols, const uint func_id, const bool isTo_normalize_randomData) {
  //defaultAdjustment_ofSyn = e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization;
  assert(ncols > 0);
  s_kt_matrix_t mat_func = initAndReturn__s_kt_matrix(/*nrows=*/1, /*ncols=*/ncols);
  s_dataStruct_matrix_dense_t obj_struct; init_s_dataStruct_matrix_dense_t_setToEmpty(&obj_struct); 
  obj_struct.matrixOf_data = mat_func.matrix; obj_struct.cnt_columns = ncols; obj_struct.cnt_rows = mat_func.nrows;

  
  s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_undef;

  bool use_random = false; bool use_uniform = false;  bool use_binomial = false; t_float config_binomial_probaility = T_FLOAT_MAX; uint valuOf_noiseLevel = 0; t_float y_adjustment_index = T_FLOAT_MAX;
  enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_undef; 
  //enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax;
  //!
  //! Idneitfy the speific configurations wrt. [ªbove]:
  const t_float __option__yAdjustment = 1.5; //! ie, approx. 0.5*PI
  const e_kt_categorizeMatrices_subCases__cmpFunction_t enum_id = (e_kt_categorizeMatrices_subCases__cmpFunction_t)func_id;
  if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_flat) {typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_flat;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_flat_noise_medium)  {valuOf_noiseLevel = 1; typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_flat_noise_high) {valuOf_noiseLevel = 2;    typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax; y_adjustment_index = __option__yAdjustment;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal) {typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_equal;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal_noise_medium) {valuOf_noiseLevel = 0; typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_and_axx;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_equal_noise_high) {y_adjustment_index = __option__yAdjustment; valuOf_noiseLevel = 2;   typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_and_axx;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_differentCoeff_b) {typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_b;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_linear_differentCoeff_a) {typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_a;;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_sinus) {y_adjustment_index = __option__yAdjustment; typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_curved;} //typeOf_function = s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_sinus_curved) {valuOf_noiseLevel = 2; typeOf_data = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_curved;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_random_1) {use_random = true;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_random_2) {use_random = true;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_random_3) {use_random = true;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_uniform_1) {use_uniform = true;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_uniform_2) {use_uniform = true;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_uniform_3) {use_uniform = true;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_1) {use_binomial = true; config_binomial_probaility = 0.05;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_2) {use_binomial = true; config_binomial_probaility = 0.05;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_05pst_3) {use_binomial = true; config_binomial_probaility = 0.05;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_1) {use_binomial = true; config_binomial_probaility = 0.5;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_2) {use_binomial = true; config_binomial_probaility = 0.5;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_5pst_3) {use_binomial = true; config_binomial_probaility = 0.5;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_1) {use_binomial = true; config_binomial_probaility = 0.2;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_2) {use_binomial = true; config_binomial_probaility = 0.2;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_bionomial_prob_20pst_3) {use_binomial = true; config_binomial_probaility = 0.2;}
  else if(enum_id == e_kt_categorizeMatrices_subCases__cmpFunction_undef) {assert(false);}
  else { //! then we have Not yet ahdnled the enum, ie, an error 'from oru side':
    fprintf(stderr, "!!\t Enum Not supported, i,e inviestigate, givne enum=%u, at %s:%d\n", enum_id, __FILE__, __LINE__);
    assert(false); // TOD: add supprot for this.
  }

  if(use_random) {
    for(uint i = 0; i < ncols; i++) {
      mat_func.matrix[0][i] = rand();
    }
  } else if(use_uniform) {
    for(uint i = 0; i < ncols; i++) {
      const t_float score = (t_float)math_generateDistribution__uniform(); //! which is defined in "cluster.c"
      mat_func.matrix[0][i] = score;
    }
    //constructSample_data_uniform(data_obj, cnt_rows, /*cnt_columns=*/ncols);
  } else if(use_binomial) {
    for(uint i = 0; i < ncols; i++) {
      const int config_cnt_trials = (int)ncols; //! ie, our default assumption.
      //printf("n=%u, p=%f, at %s:%d\n", config_cnt_trials, config_probability, __FILE__, __LINE__);
      const t_float config_probability = config_binomial_probaility;
      const t_float score  = math_generateDistribution__binomial(config_cnt_trials, config_probability); //! which is defined in "cluster.c"
      mat_func.matrix[0][i] = score;
    }
    //constructSample_data_bionmial(data_obj, cnt_rows, /*cnt_columns=*/ncols, config_binomial_probaility);
  } else if(typeOf_data != s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_undef) {
    //!
    //! Then comptue the sample-data:
    //! Build a sample-data-set using different functions 'to construct the data'.
    const uint cnt_rows = 1; const uint cnt_columns = ncols;
    enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel typeOf_noiseLevel = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_medium;
    if(valuOf_noiseLevel == 0) {
      typeOf_noiseLevel = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_low;
    } else if(valuOf_noiseLevel == 1) {
      typeOf_noiseLevel = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_medium;
    } else if(valuOf_noiseLevel == 2) {
      typeOf_noiseLevel = s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_large;
    } else {
      fprintf(stderr, "!!\t noise-levvel supported, i,e inviestigate, givne typeOf_noiseLevel=%u, at %s:%d\n", valuOf_noiseLevel, __FILE__, __LINE__);
      assert(false);
    }
    //!  
    //! Apply logics: 
    static__constructSample_data_by_function_noise(mat_func.matrix, cnt_rows, 0, /*cnt_columns=*/cnt_columns, 0, typeOf_data, typeOf_noiseLevel, y_adjustment_index);
    //static__constructSample_data_by_function_noise(obj_struct, cnt_rows, /*cnt_columns=*/cnt_columns, typeOf_data, typeOf_noiseLevel);
  } else {    
    assert(typeOf_function != s_dataStruct_matrix_dense_typeOf_sampleFunction_undef);
    //! Then construct the input-data using a pre-defined function:
    //printf("compute for typeOf_function=%d, at %s:%d\n", typeOf_function, __FILE__, __LINE__);
    static__constructSample_data_by_function__s_dataStruct_matrix_dense_t(mat_func.matrix, mat_func.nrows, /*row_startPos=*/0, ncols, /*column_startPos=*/0, typeOf_function);      
    
  }
  if(defaultAdjustment_ofSyn != e_kt_categorizeMatrices_typeOfAdjust_synAndInput_none) {
    //    if( use_binomial || use_random || use_uniform) 
    if( (defaultAdjustment_ofSyn == e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization) || use_binomial || use_random || use_uniform )     {
      for(uint i = 0; i < ncols; i++) {
	assert(isOf_interest(mat_func.matrix[0][i]));
	assert(!isinf(mat_func.matrix[0][i]));
      }
      __normalizeRow(mat_func.matrix[0], mat_func.matrix[0], ncols); //! ie, we then nroamlzie data to increase chance of 'simlarity' wrt. 'unkown' data-sets.
      //printf("...., at %s:%d\n", __FILE__, __LINE__);
    } 
    //printf("..--.., at %s:%d\n", __FILE__, __LINE__);
    //! Adjsut the scoes by the specirfried vlaue:
    assert(ncols == mat_func.ncols);
    for(uint i = 0; i < ncols; i++) {
      assert(isOf_interest(mat_func.matrix[0][i]));
      //mat_func.matrix[0][i] += syn__adjsutVAleuBy;
      assert(isOf_interest(mat_func.matrix[0][i]));
    }
    
  }
  
  //! ------------------------------------
  //! 
  //! @return
  return mat_func;
}

//! Cosntruct a file holding the data-set which we evlauate/comapre each data-matrix to. 
void static__exportSynteticDAtaMatrix(const uint ncols, const char *fileName_toExportTo, const e_kt_categorizeMatrices_typeOfAdjust_synAndInput_t defaultAdjustment_ofSyn) {
  assert(fileName_toExportTo); assert(strlen(fileName_toExportTo));
  //!
  //! Costnruct matrix: 
  const uint cnt_func = e_kt_categorizeMatrices_subCases__cmpFunction_undef;
  assert(cnt_func > 0);
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(cnt_func, ncols);
  //!
  //! Iterate through the suntatcic cases:
  for(uint func_id = 0; func_id < cnt_func; func_id++) {
    const char *string = get_shortString__e_kt_categorizeMatrices_subCases__cmpFunction_t((e_kt_categorizeMatrices_subCases__cmpFunction_t)func_id);
    //getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i);
    assert(string); assert(strlen(string));
    //! Set column-names:
    set_stringConst__s_kt_matrix(&mat_result, func_id, string, /*addFor_column=*/false);
    //!
    //! construct a sample-matrix:
    s_kt_matrix_t mat_func = __construct_sampleData(ncols, func_id, defaultAdjustment_ofSyn, /*syn__adjsutVAleuBy=*/0);
    assert(mat_func.nrows == 1);
    assert(mat_func.ncols == ncols);
    //! 
    //! 
    for(uint i = 0; i < ncols; i++) {
      mat_result.matrix[func_id][i] = mat_func.matrix[0][i];
    }
    //! 
    //! De-allocate:
    free__s_kt_matrix(&mat_func);
  }
  //!
  //! Export: Write out to the "fileName_toExportTo" result-file:
  assert(mat_result.nrows > 0);
  const bool is_ok = export__singleCall__s_kt_matrix_t(&mat_result, fileName_toExportTo, NULL);
  assert(is_ok);
  //!
  //! De-allocate: 
  free__s_kt_matrix(&mat_result);
  //return mat_result;  
}

uint add_dataSet__s_hp_categorizeMatrices_syn_t(s_hp_categorizeMatrices_syn_t *self, const s_kt_matrix_t *obj_matrix, const char *stringOf_entry, const s_hp_categorizeMatrices_syn_config_dataObj_t vertexProp) {
  assert(self);
  assert(stringOf_entry); assert(strlen(stringOf_entry));
  assert(obj_matrix);
  assert(obj_matrix->nrows > 0);     assert(obj_matrix->ncols > 0);
  const uint curr_pos = self->current_pos;
  //! -----------------------------------------------------------------------
  //!
  { //! Update our hyptoehssi-cases:
#define __MiF__setGroup(enum_id, group_id) ({set_scalar__s_kt_list_1d_uint_t(&(self->listOf_hyp[enum_id]), curr_pos, (uint)group_id);})
    if(false == isEmpty__s_kt_correlationMetric_t(&(vertexProp.sim_whichIsApplied))) {  //! then we add mappigns to the four different catoregies of sim-metric-data-cateogries
      //fprintf(stderr, "add-sim-metic-gorup-id\n");
      //! Update category:            
      __MiF__setGroup(e_kt_categorizeMatrices_subCases__hypo_simMetric, vertexProp.sim_whichIsApplied.metric_id);
      //! Update category: sim-metic-group:narrow:
      //__MiF__setGroup(e_kt_categorizeMatrices_subCases__hypo_simMetric, vertexProp.sim_whichIsApplied.typeOf_correlationPreStep));
      __MiF__setGroup(e_kt_categorizeMatrices_subCases__hypo_simMetric_inCategory_small, getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__small_t(vertexProp.sim_whichIsApplied));      
      //! Update category: sim-metic-group:medium:
      __MiF__setGroup(e_kt_categorizeMatrices_subCases__hypo_simMetric_inCategory_medium, getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t(vertexProp.sim_whichIsApplied));

    }
    if(vertexProp.data_id != UINT_MAX) {
      //! Update category: 
      __MiF__setGroup(e_kt_categorizeMatrices_subCases__hypo_dataId, vertexProp.data_id);
    }
    if(vertexProp.data_id_category != UINT_MAX) {
      //! Update category: 
      __MiF__setGroup(e_kt_categorizeMatrices_subCases__hypo_dataSet, vertexProp.data_id_category);
    }
  }
  //! -----------------------------------------------------------------------
  //!
  //! Add the string:
  set_stringConst__s_kt_list_1d_string(&(self->listOf_names), self->current_pos, stringOf_entry);
  //!
  //! Iterate through the suntatcic cases:  
  const uint cnt_func = e_kt_categorizeMatrices_subCases__cmpFunction_undef;
  const uint ncols = obj_matrix->ncols; assert(ncols > 0);
  if(self->defaultAdjustment_ofSyn != e_kt_categorizeMatrices_typeOfAdjust_synAndInput_none) { //! then we combien a normalised dat-ainptu-appraoch with use of our tilie-topmzeid simalrity-emtic-ocmatuion-funciton:
    e_kt_categorizeMatrices_typeOfAdjust_synAndInput_t defaultAdjustment_ofSyn = self->defaultAdjustment_ofSyn;
    if(defaultAdjustment_ofSyn == e_kt_categorizeMatrices_typeOfAdjust_synAndInput_undef) { //! then we set the default option:
      // TODO: cosnider using an altenraitve 'defualt option' ... ie, update our expemreinal investigaiton:
      //defaultAdjustment_ofSyn = e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization; //! where latter seems to be assicted with the 'least/lwoest amount of vairance' for the "sytnatic-small" data-set.
      // defaultAdjustment_ofSyn = e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_avg; //! where latter seems to resutl in sicningat icnrease in variance ofr the sytantcit-camm-data.sets
      // defaultAdjustment_ofSyn = e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_max;
      //! Note: for the smallest sytnetic data-sets we for: "min" observe a variance of "Minkowski--linear-diffCoeff-b":0.24 (and otehrwise 0);
      //! Note: for the smallest sytnetic data-sets we for: "avg" observe a variance of "Minkowski--linear-diffCoeff-b":0.4 (and otehrwise 0);
      //! Note: for the smallest sytnetic data-sets we for: "max" observe a variance of "Minkowski--linear-diffCoeff-b":0.14 (and otehrwise 0);
      defaultAdjustment_ofSyn = e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization;
      //defaultAdjustment_ofSyn = e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_min;
    }
    s_kt_matrix_base_t mat_func = initAndReturn__s_kt_matrix_base_t(cnt_func, ncols);
    s_kt_matrix_base_t mat_input = initAndReturn__s_kt_matrix_base_t(obj_matrix->nrows, ncols);
    t_float syn__adjsutVAleuBy = 1;
    if(defaultAdjustment_ofSyn != e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization) {
      uint n_count = 0; t_float sum = 0; t_float value_min = T_FLOAT_MAX; t_float value_max = T_FLOAT_MIN_ABS;      
      for(uint i = 0; i < obj_matrix->nrows; i++) {
	for(uint k = 0; k < obj_matrix->ncols; k++) {      
	  t_float score = obj_matrix->matrix[i][k];
	  if(isOf_interest(score)) {
	    sum += score;
	    value_min = macro_min(value_min, score);
	    value_max = macro_max(value_max, score);
	    n_count++;
	  }
	}
      }
      if(defaultAdjustment_ofSyn == e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_min) {
	syn__adjsutVAleuBy = value_min;
      } else if(defaultAdjustment_ofSyn == e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_max) {
	syn__adjsutVAleuBy = value_max;
      } else if(defaultAdjustment_ofSyn == e_kt_categorizeMatrices_typeOfAdjust_synAndInput_addToSyn_fromMatrix_avg) {
	syn__adjsutVAleuBy = (n_count && (sum != 0.0) ) ? sum / (t_float)n_count : 0;
      } else {
	fprintf(stderr, "!!\t(API-option)\t API-option Not supproted for enum=%u, ie, please investigate. Observioant at %s:%d\n", defaultAdjustment_ofSyn, __FILE__, __LINE__);
	assert(false); //! ie, an heads-up, and a compielroiptmzioatn-aprpaoch as well.
      }
      //printf("syn__adjsutVAleuBy=%f, at %s:%d\n", syn__adjsutVAleuBy, __FILE__, __LINE__);
    }
    for(uint func_id = 0; func_id < cnt_func; func_id++) {
      //
      //! construct a sample-matrix:
      assert(obj_matrix->ncols > 0);
      assert(obj_matrix->nrows > 0);
      s_kt_matrix_t __mat_func = __construct_sampleData(obj_matrix->ncols, func_id, defaultAdjustment_ofSyn, syn__adjsutVAleuBy);
      for(uint i = 0; i < ncols; i++) {
	mat_func.matrix[func_id][i] = __mat_func.matrix[0][i];      
      }
      free__s_kt_matrix(&__mat_func);
    }  
    if(defaultAdjustment_ofSyn == e_kt_categorizeMatrices_typeOfAdjust_synAndInput_normalization) {
    //if(true) {
      for(uint i = 0; i < obj_matrix->nrows; i++) {
	__normalizeRow(obj_matrix->matrix[i], mat_input.matrix[i], ncols); //! ie, we then nroamlzie data to increase chance of 'simlarity' wrt. 'unkown' data-sets.
      }
    } else { //! then a simple copying, ie, as we then assume that the 'comaprsion-data-sets' have been 'dragged' towards this/our data-set (ie, rather than using normalization to 'drag' both data-sets towards the center-points).
      for(uint i = 0; i < obj_matrix->nrows; i++) {
	for(uint k = 0; k < obj_matrix->ncols; k++) {
	  mat_input.matrix[i][k] = obj_matrix->matrix[i][k];
	}
      }
    }
    //! Adjust the value-scores
    const t_float sizeIncrease_each_step = 10;
    for(uint i = 0; i < mat_func.nrows; i++) {
      bool is_odd  = false;
      for(uint k = 0; k < mat_func.ncols; k++) {
	assert(isOf_interest(mat_func.matrix[i][k]));
	if(is_odd) {
	  mat_func.matrix[i][k] *= self->defaultAdjustment_ofSyn__mul_odd;
	  is_odd = false;
	} else {
	  mat_func.matrix[i][k] *= self->defaultAdjustment_ofSyn__mul_even;
	  is_odd = true;
	}
	mat_func.matrix[i][k] *= (i+1)*sizeIncrease_each_step;
      }
    }
    if(self->current_pos == 0) { 
      if(true) { //! then we export the matrix: 
	allocOnStack__char__sprintf(8000, stringOf_resultPath, "%s_%s_%s.tsv", self->stringOf_resultPath, self->stringOf_resultTag, "synData");
	const uint cnt_func = e_kt_categorizeMatrices_subCases__cmpFunction_undef;
	assert(cnt_func == mat_func.nrows); //! ie, what we expect
	
	s_kt_matrix_t mat_base = get_deepCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_func);
	for(uint func_id = 0; func_id < cnt_func; func_id++) {
	  const char *string = get_shortString__e_kt_categorizeMatrices_subCases__cmpFunction_t((e_kt_categorizeMatrices_subCases__cmpFunction_t)func_id);
	  //getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i);
	  assert(string); assert(strlen(string));
	  //! Set column-names:
	  set_stringConst__s_kt_matrix(&mat_base, func_id, string, /*addFor_column=*/false);
	}
	const bool is_ok = export__singleCall__s_kt_matrix_t(&mat_base, stringOf_resultPath, NULL);
	assert(is_ok);
	free__s_kt_matrix(&mat_base);
      }
    }
    //!
    //! Apply lgocis, using our effecitign tiling-approach:
    s_kt_matrix_base_t mat_result        = initAndReturn__empty__s_kt_matrix_base_t();
    s_hp_distance__config_t local_config = init__s_hp_distance__config_t(); local_config.typeOf__postProcessing = e_hp_distance__subLogics_eachVector_select_max; //! ie, where latter is 'used' to cosntruct the result-amtrix holding the 'max-elements':
    
    const bool is_ok = apply__hp_distance(/*obj_metric=*/self->sim_toCompare, &mat_func, &mat_input, &mat_result, local_config);
    assert(mat_result.nrows == 1); assert(mat_result.ncols == cnt_func); //! ie, what we expect, ie, in ”[elow]:
    for(uint func_id = 0; func_id < cnt_func; func_id++) {
      //! Fetch the max-socre, and insert into our strucutre: 
      const t_float score_max = mat_result.matrix[0][func_id];
      //! Update oru resutl-strucutres:
      // printf("[%u]=score_max=%f, at %s:%d\n", func_id, score_max, __FILE__, __LINE__);
      set_scalar__s_kt_list_1d_float_t(&(self->listOf_scores__max[func_id]), curr_pos, score_max);
    }  
    //!
    //! De-allocate:
    free__s_kt_matrix_base_t(&mat_func);
    free__s_kt_matrix_base_t(&mat_input);
    free__s_kt_matrix_base_t(&mat_result);    
  } else {    
    for(uint func_id = 0; func_id < cnt_func; func_id++) {
      //!
      //! construct a sample-matrix:
      assert(obj_matrix->ncols > 0);
      assert(obj_matrix->nrows > 0);
      s_kt_matrix_t mat_func = __construct_sampleData(obj_matrix->ncols, func_id, self->defaultAdjustment_ofSyn, /*syn__adjsutVAleuBy=*/0);
      
      //!
      //! Iterate throught he different data-set-rows:
      t_float score_max = T_FLOAT_MIN_ABS;
      //t_float score_min = T_FLOAT_MAX;     t_float sum = 0;
      t_float *row_func = mat_func.matrix[0];
      assert(row_func);
      
      
      
      for(uint row_id = 0; row_id < obj_matrix->nrows; row_id++) {
	t_float *row_data = obj_matrix->matrix[row_id];
	assert(row_data);
	t_float score = T_FLOAT_MAX; apply__rows_hp_distance(/*obj_metric=*/self->sim_toCompare, row_data, row_func, obj_matrix->ncols, &score);
	if( (score != T_FLOAT_MAX) && (score != T_FLOAT_MIN_ABS) && !isinf(score)) {
	  score_max = macro_max(score_max, score);
	}
      }
      if( (score_max != T_FLOAT_MAX) && (score_max != T_FLOAT_MIN_ABS) && !isinf(score_max)) {
	//!
	//! Update oru resutl-strucutres:
	set_scalar__s_kt_list_1d_float_t(&(self->listOf_scores__max[func_id]), curr_pos, score_max);
      }
      
    }
  }
  //!
  //!  
  //!
  //! Icnrmenet currnet-pos:
  self->current_pos++;
  return curr_pos;
}




