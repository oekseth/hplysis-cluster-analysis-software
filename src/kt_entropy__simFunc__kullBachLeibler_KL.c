  //! Compute: freqs1 = freqs1/sum(freqs1)  and Compute: freqs2 = freqs2/sum(freqs2) 
  t_float sum_1 = __MF__s_kt_entropy__sum(arr1, arr_size);
  t_float sum_2 = __MF__s_kt_entropy__sum(arr2, arr_size);
  t_float sum_H = 0;
  if( (sum_1 != 0) && (sum_2 != 0) )  {
    if(typeOf_freqCompute == e_kt_entropy_frequencyType_empirical) {
      const t_float sum_1_inv = 1/sum_1; //! ie, a eprofmrance-otpmziaotn-strategy: avodi the need for 'dvision' and 'also' avoid the need/reuqiremen for an if-clause-branch.
      const t_float log_score_sum_1 = mathLib_float_log(sum_1);
      const t_float log_score_sum_2 = mathLib_float_log(sum_2);    
#define  __MiF__getScore_log_1() ({__MiF__log(arr1[i]) - log_score_sum_1;})
#define  __MiF__getScore_log_2() ({__MiF__log(arr2[i]) - log_score_sum_2;})
      //#define  __MiF__getScore_log_2() ({MF__s_kt_entropy__log_index_fast(self, arr2[i]) - log_score_sum_2;})
      for(uint i = 0; i < arr_size; i++) {
	//      if(arr[i] > 0) {
	// LR = ifelse(freqs1 > 0, log(freqs1/freqs2), 0)      
#if(1 == 1) 
	const t_float freq_1 = (t_float)arr1[i] * (t_float)sum_1_inv;
#else
	// FIXME: ensure that we do Not comptue [”elow] if the caller have alredy 'comptued/applied this' .... invesitgate how the Entopry-lbirary 'handle such a task' ... 
#endif
	const t_float pair_log = __MiF__getScore_log_1() - __MiF__getScore_log_2(); //! ie, where log(f1/f2) = log(f1) - log(f2) = log(score1/sum1) - log(score2/sum2) = log(score1) - log(sum1) - (log(score2) - log(sum2));
	sum_H += freq_1 *  pair_log;
      }
      //! *****************
#undef __MiF__getScore_log_1
#undef __MiF__getScore_log_2
      //! *****************
    } else if(typeOf_freqCompute == e_kt_entropy_frequencyType_Dirichlet) {
      // FIXME: validte that [”elow] corresonds to: KL.empirical = function(y1, y2, unit=c("log", "log2", "log10")) {return( KL.plugin(freqs.empirical(y1), freqs.empirical(y2), unit=unit) )}
      // FIXME[lgoc]s: add supprot for different "val_adjust" for sum_1 and sum_2 ... or drop latter support .... 
      sum_1 += (t_float)arr_size*(t_float)self->val_adjust; /*! ie, compute: "ya = y+a".*/	
      sum_2 += (t_float)arr_size*(t_float)self->val_adjust; /*! ie, compute: "ya = y+a".*/	
      const t_float sum_1_inv = 1.0/(t_float)sum_1; //! ie, a eprofmrance-otpmziaotn-strategy: avodi the need for 'dvision' and 'also' avoid the need/reuqiremen for an if-clause-branch.
      const t_float log_score_sum_1 = mathLib_float_log(sum_1);
      const t_float log_score_sum_2 = mathLib_float_log(sum_2);    
#define  __MiF__getScore_log_1() ({__MiF__log(arr1[i]) - log_score_sum_1;})
#define  __MiF__getScore_log_2() ({__MiF__log(arr2[i]) - log_score_sum_2;})
      for(uint i = 0; i < arr_size; i++) {
	//      if(arr[i] > 0) {
	// LR = ifelse(freqs1 > 0, log(freqs1/freqs2), 0)      
#if(1 == 1) 
	const t_float val_adjust = self->val_adjust;
	const t_float freq_1 = (t_float)__MF__freqs_Dirichlet__atIndex(arr1, i, sum_1_inv);
#else
	// FIXME: ensure that we do Not comptue [”elow] if the caller have alredy 'comptued/applied this' .... invesitgate how the Entopry-lbirary 'handle such a task' ... 
#endif
	const t_float pair_log = __MiF__getScore_log_1() - __MiF__getScore_log_2(); //! ie, where log(f1/f2) = log(f1) - log(f2) = log(score1/sum1) - log(score2/sum2) = log(score1) - log(sum1) - (log(score2) - log(sum2));
	sum_H += freq_1 *  pair_log;
      }
      //! *****************
#undef __MiF__getScore_log_1
#undef __MiF__getScore_log_2
      //! *****************
    } else {
      sum_1 += (t_float)arr_size*(t_float)self->val_adjust; /*! ie, compute: "ya = y+a".*/	
      sum_2 += (t_float)arr_size*(t_float)self->val_adjust; /*! ie, compute: "ya = y+a".*/	
      //      const t_float sum_1_inv = 1/sum_1; //! ie, a eprofmrance-otpmziaotn-strategy: avodi the need for 'dvision' and 'also' avoid the need/reuqiremen for an if-clause-branch.
      const t_float log_score_sum_1 = mathLib_float_log(sum_1);
      const t_float log_score_sum_2 = mathLib_float_log(sum_2);    
#define  __MiF__getScore_log_1() ({__MiF__log(arr1[i]) - log_score_sum_1;})
#define  __MiF__getScore_log_2() ({__MiF__log(arr2[i]) - log_score_sum_2;})
      for(uint i = 0; i < arr_size; i++) {
	//      if(arr[i] > 0) {
	// LR = ifelse(freqs1 > 0, log(freqs1/freqs2), 0)      
#if(1 == 1) 
	const t_float val_adjust = self->val_adjust;
	const t_float freq_1 = (t_float)arr1[i];
#else
	// FIXME: ensure that we do Not comptue [”elow] if the caller have alredy 'comptued/applied this' .... invesitgate how the Entopry-lbirary 'handle such a task' ... 
#endif
	const t_float pair_log = __MiF__getScore_log_1() - __MiF__getScore_log_2(); //! ie, where log(f1/f2) = log(f1) - log(f2) = log(score1/sum1) - log(score2/sum2) = log(score1) - log(sum1) - (log(score2) - log(sum2));
	sum_H += freq_1 *  pair_log;
      }
      //! *****************
#undef __MiF__getScore_log_1
#undef __MiF__getScore_log_2
      //! *****************
    }
  }
  //!
  //! Post-scaling:
  __MF__s_kt_entropy__postAdjustment();
  //!
  //! @return
  *scalar_retVal = sum_H;

#undef __MiF__log
