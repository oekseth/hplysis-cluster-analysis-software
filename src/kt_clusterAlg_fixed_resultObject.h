#ifndef kt_clusterAlg_fixed_resultObject_h
#define kt_clusterAlg_fixed_resultObject_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "e_kt_clusterAlg_exportFormat.h" //! which is used to enumerate diffferent cluster-export-foramts (oekseth, 06. jan. 2016).
#include "kt_list_1d.h" 
/**
   @file kt_clusterAlg_fixed_resultObject
   @brief a result-object for the k-means clustering (oesketh, 06. nov. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/



/**
   @struct s_kt_clusterAlg_fixed_resultObject
   @brief provide a structured access to the result-objects generated in our k-means clustering-procedure (oekseth, 06. nov. 2016).
 **/
typedef struct s_kt_clusterAlg_fixed_resultObject {
  uint cnt_vertex; 
  uint clusterId_size;
  uint *vertex_clusterId; //! where we assume that each 'vertex' points to a 'vertex-centroid'
  uint *cluster_vertexCentroid; 
  uint *cluster_vertexCount; //!! which if used is the number of vertices in a given cluster.
  t_float *cluster_errors;
  t_float **matrix2d_vertexTo_clusterId;
  //! ----
  t_float sumOf_errors;
  uint cntTotal_clusterAnswerFound; //! the number of times an optmial sollution was found.
  uint cntInsertedBefore__clusters; //! which is used when merging diffneret/muliple cluster-reuslts, eg, for the case where we have dused disjotinf-reost-speeraiton as a 'pre-step' (oekseth, 06. feb. 2017).
} s_kt_clusterAlg_fixed_resultObject_t;



//! Intiate the s_kt_clusterAlg_fixed_resultObject_t object
void setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self);
//! Itnaes and reutnr a s_kt_clusterAlg_fixed_resultObject_t objec t(oekseht, 06. mar. 2017).
static s_kt_clusterAlg_fixed_resultObject_t initAndReturn__s_kt_clusterAlg_fixed_resultObject_t() {
  s_kt_clusterAlg_fixed_resultObject_t self;
  setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(&self);
  return self;
}
//! Intaite the s_kt_clusterAlg_fixed_resultObject_t object
void init__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self, const uint nclusters, const uint cnt_vertex);

//! Intaite the s_kt_clusterAlg_fixed_resultObject_t object with a defualt clsuter-emberships, which is used as a 'first guess', eg, in k-means and k-medoid clsuteirng (eosekth, 06. jan. 2017).
void init__setDefaultClusterVector__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self, const uint nclusters, const uint cnt_vertex, const uint *vertex_clusterId);
//! @return an intiated object which hold the clsuter-mbershsip providdd/given in the the "obj_vertexToCluster" list (oekseth, 06. jul. 2017)
//! @remarks to handle cases where a vertex is Not assited to any clsuters we increment/set the max-cluster-count: to conform to the k-means-implemtantiosn we assign all 'non-matched' vertices into the same clsuter, an appraoch which seems to be correct/better tof IRIS-data-sets.
s_kt_clusterAlg_fixed_resultObject_t init_fromListOfClusterMemberships__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_list_1d_uint_t *obj_vertexToCluster, const uint defualt_vertexCount);

/**
   @brief Print the resutls of the clsutering in a 'human-redable-format' to stream_out (oesketh, 06. jan. 2017).
   @param <mapOf_names>  which if set is used to 'name' the indeanl indices.
   @param <stream_out> sit he file-desctiopror to be used in exprot.
   @param <seperator_cols>  is the seperator to be used to 'dsintish each column, eg, "\t"
   @param <seperator_newLine> is the seperator between each line, eg "\n"
   @param <enum_id> identifies the data-sets which are to be epxortedb
   @return true upon scucess.
**/
bool printHumanReadableResult__extensive__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, char **mapOf_names, FILE *stream_out, const char *seperator_cols, const char *seperator_newLine, const e_kt_clusterAlg_exportFormat_t enum_id);

//! Print the resutls of the clsutering in a 'human-redable-format' to STDOUt: the mapOf_names argument is optional, ie, only used if Not set to NULL
void printHumanReadableResult__toStdout__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, char **mapOf_names);

/**
   @brief merge the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void mergeResult__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self, const s_kt_clusterAlg_fixed_resultObject_t *clusterResults, const uint *keyMap_localToGlobal);


//! De-allcoates the s_kt_clusterAlg_fixed_resultObject_t object
void free__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self);

//#define __MC__inline __attribute__((always_inline))
#define __MC__inline //! where altter is omitted as the SWIG-compiler where complaining (oekseth, 06. apr. 2017)

//! Idneitfy the number of tiems the optmial sollution was found
static inline __MC__inline void get_cnt_optimalSollutionWasFound__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, uint *scalar_result) {
  assert(self);
  *scalar_result = self->cntTotal_clusterAnswerFound;
}
//! Idneitfy the number of sum of errors int eh clsuter-idnetificaiton
static inline __MC__inline void get_cnt_sumOfErrorrs__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, t_float *scalar_result) {
  assert(self);
  *scalar_result = self->sumOf_errors;
}
//! Idneitfy the number of clsuters in this object
static inline __MC__inline void get_cnt_clusters__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, uint *scalar_result) {
  assert(self);
  *scalar_result = self->clusterId_size;
}
//! Idneitfy the number of vertices in this object
static inline __MC__inline void get_cnt_vertices__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, uint *scalar_result) {
  assert(self);
  *scalar_result = self->cnt_vertex;
}
//! Get the cluster-id assicated to the vertex in question.
//! @remarks if the vlaue is not set then we assuem there is a 'zero clusters', ie, for which the scalar_result is then set to UINT_MAX
static inline __MC__inline void get_clusterId__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, const uint row_id, uint *scalar_result) {
  assert(self); assert(row_id < self->cnt_vertex); assert(self->vertex_clusterId);
  *scalar_result = UINT_MAX;
  if(self && self->vertex_clusterId && (row_id < self->cnt_vertex) ) {*scalar_result = self->vertex_clusterId[row_id];}
}

//! Get the cluster-errorr assicated to the cluster-id in question.
//! @remarks if the vlaue is not set then we assuem there is a 'zero cluste-errors', ie, for which the scalar_result is then set to "0"
static inline __MC__inline void get_cluster_error__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self, const uint cluster_id, float *scalar_result) {
  assert(self); assert(cluster_id < self->clusterId_size); assert(self->cluster_errors);
  *scalar_result = 0;
  if(self && self->cluster_errors && (cluster_id < self->clusterId_size) ) {*scalar_result = self->cluster_errors[cluster_id];}
}
//! Get the vertex-centroid-id assicated to the cluster-id in question.
//! @remarks if the vlaue is not set then we assuem there is a 'zero cluste-errors', ie, for which the scalar_result is then set to "UINT_MAX"
static inline __MC__inline void get_vertexCentroid__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self, const uint cluster_id, uint *scalar_result) {
  assert(self); assert(cluster_id < self->clusterId_size); assert(self->cluster_vertexCentroid);
  *scalar_result = UINT_MAX;
  if(self && self->cluster_vertexCentroid && (cluster_id < self->clusterId_size) ) {*scalar_result = self->cluster_vertexCentroid[cluster_id];}
}




#endif //! EOF
