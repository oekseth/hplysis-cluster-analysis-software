#ifndef correlation_api_h
#define correlation_api_h

#include "correlation_api_rowCompare.h"
#include "correlation_inCategory_delta.h"
#include "correlation_inCategory_rank_kendall.h"
#include "correlation_inCategory_matrix_base.h"
#include "correlation_inCategory_rank_spearman.h"
#include "correlationFunc_optimal_compute_allAgainstAll_SIMD.h"


#endif //! EOF
