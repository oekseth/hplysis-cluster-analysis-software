#ifndef kt_clusterAlg_SOM_h
#define kt_clusterAlg_SOM_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_clusterAlg_SOM
   @brief provide logics for computation of Self Organising Maps (SOMs)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "e_kt_correlationFunction.h"


#include "kt_distance.h"

/**
   @struct s_config_som
   @brief provide configauriton-options for applicaiton of the SOM configuration (oekseth, 06. aug. 2016)
   @remarks 
   # "isTo_use_continousSTripsOf_memory" which if set to false 'states' that an in-effctive memory-allcoation procedure is to be used.
   # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
 **/
typedef struct s_config_som {
  bool isTo_use_continousSTripsOf_memory;
  bool isTo_invertMatrix_transposed;
  bool isTo_copmuteResults_optimal;
  bool isTo_useImplictMask;
  //! Note: the "config_somWorker_useImproved_STD" seems to have isngificant time-effect wrt. imrpvoement.
  bool config_somWorker_useImproved_STD;
  //! Note: the "config_somWorker_useImproved_STD_useSSEIntrisinistics" 'provides' a 3x perofrmance-boost (when compared in 'islositon' to the STD-sub-compuation).
  bool config_somWorker_useImproved_STD_useSSEIntrisinistics;
  bool config_somWorker_useImproved___updateClosestPair;
  //! Note: if the complete set of [ªbov€] and [”elow] "useImproved..." paramseters are used, then we call oru "__update_closest_pairs_improved_memoryTiled(..)" with "isTo_useSSE=true": result is a 2x-perofmrance-increase (when compared to the 'naive' impelmetnaiton).
  bool config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics;
  bool config_somWorker_useImproved___updateClosestPair_useMemoryTiling;
  
} s_config_som_t;

//! Intiate the obj with default values
static void s_config_som_init(s_config_som_t *self) {
  assert(self);
  self->isTo_use_continousSTripsOf_memory = true;
  self->isTo_invertMatrix_transposed = true;
  self->isTo_copmuteResults_optimal = true;
  self->isTo_useImplictMask = false;
  self->config_somWorker_useImproved_STD = true;
  self->config_somWorker_useImproved_STD_useSSEIntrisinistics = true;
  self->config_somWorker_useImproved___updateClosestPair = true;
  self->config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics = true;
  self->config_somWorker_useImproved___updateClosestPair_useMemoryTiling = true;
}
//! Intiate the obj with default values
static void s_config_som_init_slow(s_config_som_t *self) {
  assert(self);
  self->isTo_use_continousSTripsOf_memory = true;
  self->isTo_invertMatrix_transposed = true;
  self->isTo_copmuteResults_optimal = true;
  self->isTo_useImplictMask = false;
  self->config_somWorker_useImproved_STD = false;
  self->config_somWorker_useImproved_STD_useSSEIntrisinistics = false;
  self->config_somWorker_useImproved___updateClosestPair = false;
  self->config_somWorker_useImproved___updateClosestPair_useSSEIntrisinistics = false;
  self->config_somWorker_useImproved___updateClosestPair_useMemoryTiling = false;
}

/**
   @namespace graphAlgorithms_som
   @brief provide implementaitons of algorithms using the "Self Organizing Maps" (SOM) as a 'basis'.
   @remarks the "c clustering library" is used as template.
   @author Ole Kristian Ekseth (oekseth).
 **/
//namespace graphAlgorithms_som {
  /**
     @brief compute a "celldata" 'model' from a a training-set "data".
     @remarks in contrast to "kmeans" clustering-algorithm this 'initaiton-algorithm' is not 'requried' to inspect all of the vectors (eg, rows in "data"). The latter is due to the itnerest/need to compute the "celldata" neuron-map, ie, where the "data" is anyhow expected to be a subset of the real-wolrd input-data-set.  
  **/
void somworker(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, float** data, char** mask, float *weights, uint transpose, uint nxgrid, uint nygrid, float inittau, float*** celldata, const uint niter, const s_config_som_t *config/* = NULL*/);
  /**
     @brief assign vectors in a "data" to a "celldata" data-set trained in the "somworker(..)" 'trainer'.
     @remarks ccolect the cluster-ids to ...
  **/
void somassign(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, const uint nrows, const uint ncolumns, t_float** data, char** mask, t_float *weights, uint transpose, const uint nxgrid, const uint nygrid, t_float*** celldata, uint **clusterid, const s_config_som_t *config/* = NULL*/);
  
  /**
     @brief combined a 'trainer' and a 'model-applicaiton' to orngaise/cluster a dataset.
     @remarks The somcluster routine implements a self-organizing map (Kohonen) on a rectangular grid, using a given set of vectors. The distance measure to be used to find the similarity between genes and nodes is given by dist.
     - "SOM forms a semantic map where similar samples are mapped close together and dissimilar ones apart" ["https://en.wikipedia.org/wiki/Self-organizing_map"]
     @remarks in below we describe a few number of the parameters:
     # "nxgrid": The number of grid cells horizontally in the rectangular topology of clusters.
     # "nygrid": The number of grid cells horizontally in the rectangular topology of clusters.
     # "inittau": The initial value of tau, representing the neighborhood function.
     # "niter": The number of iterations to be performed.
     # "celldata": float[nxgrid][nygrid][ncolumns] iftranspose==0; float[nxgrid][nygrid][nrows]    iftranpose==1; The gene expression data for each node (cell) in the 2D grid. This can be interpreted as the centroid for the cluster corresponding to that cell. If celldata is NULL, then the centroids are not returned. Ifcelldata is not NULL, enough space should be allocated to store the centroid data before callingsomcluster.
     # "clusterid": iftranspose==0; int[ncolumns][2] iftranspose==1; For each item (gene or microarray) that is clustered, the coordinates of the cell in the 2D grid to which the item was assigned. Ifclusterid is NULL, the cluster assignments are not returned. Ifclusterid is not NULL, enough memory should be allocated to store the clustering information before calling somcluster.
  **/
void somcluster__extensiveInputParams(const e_kt_correlationFunction_t metric_id, const e_kt_categoryOf_correaltionPreStep_t typeOf_correlationPreStep, uint nrows, uint ncolumns, t_float** data, char** mask, t_float *weight, uint transpose, const uint nxgrid, const uint nygrid, t_float inittau, const uint niter, t_float*** celldata, uint **clusterid, s_config_som_t *config/* = NULL*/);
//};


#endif //! EOF
