#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_distance.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"

/**
   @brief demonstrates the use of 'value-hiding' when computing a similartiy-metirc, ie, to describe the simlairty between: a given vector and a matrix. 
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017).
   @remarks 1-to-many: a simple permtaution of "tut_sim_3_vec_MINE.c" where we comptue for "row(1) x row(...)" wrt. the tow input-matrices.
   @remarks general: use logics in our "hp_distance" to comptue the simalrit between two vectors.
   @remarks an example-applicaiton is seen in (one of) our implementaiotns of the "kmeans++" algorithm ("____init_centers_pySciKitLearn__kmeanspp__s_kt_matrix_base_t(..)"). 
   @remarks simliartiy: comptue the sim-matrix="Gower" between two vectors using our "hp_distance.h" API 
   @remarks "hp_distance": we call the "apply__hp_distance(..)" function;
   @remarks generic: we use our "initSampleOfValues__rand__s_kt_matrix_base_t(..)" (defined in our "kt_matrix_base.h") to simplify the writign of 'statnized' code-examples: simplifeis the 'constuciton' of two uqniue vectors to compare.
   @remarks generalizaiton(example): to simplify the understanidng and re-use of our appraoch we have strucutred our "tut_sim_*" examples to use the same 'example-seutp', ie, where differneces is found wrt. the API-calls in to our "hp_distance", and wrt. different "s_kt_correlationMetric" calls.
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  const uint nrows = 2; const uint ncols = 100; 
  uint nrows_2 = 20;
#endif
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------
  //!
  //! Specify/define the simlairty-metric:
  s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_minkowski_chebychev, /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"
  //s_kt_correlationMetric obj_metric = initAndReturn__s_kt_correlationMetric_t(/*metric=*/ /*pre-step=*/e_kt_categoryOf_correaltionPreStep_none); //! a funtcion and enums defined in our "e_kt_correlationFunction.h"

  //! Allocate two vectors, ie, matrices with demsinos [1, ncols]:
  //! Note: for simplicty we use [”elow] fucntion, defined in our "kt_matrix_base.h", ie, to set the vectors to default valeus, therby reducing the nubmer of code-lines in this use-case-example; in the latter randomenss is used, ie, for which we expect the vectors to be different.
  assert(nrows_2 > 2);   assert(ncols > 0); //! ie, as we otherise have a poinbtelss' call
  s_kt_matrix_base vec_1 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
  s_kt_matrix_base vec_2 = initSampleOfValues__rand__s_kt_matrix_base_t(nrows_2, ncols);
  s_kt_matrix_base vec_result = initAndReturn__empty__s_kt_matrix_base_t();//! ie, set to 'empty'.
  { //! Examplifyi our 'hiding':
    for(uint i = 0; i < 10; i++) {
      //! Use reandomness to dineitfy arbitrary random-values:
      const uint row_id = rand() % vec_1.nrows;
      const uint col_id = rand() % vec_1.ncols;
      //! SEt the random-value:
      vec_1.matrix[row_id][col_id] = T_FLOAT_MAX; //! hwer elatter value desbied 'emptyness' in a matrix, ie,, 'vlaues without data'.
    }
    for(uint i = 0; i < 10; i++) {
      //! Use reandomness to dineitfy arbitrary random-values:
      const uint row_id = rand() % vec_2.nrows;
      const uint col_id = rand() % vec_2.ncols;
      //! SEt the random-value:
      vec_2.matrix[row_id][col_id] = T_FLOAT_MAX; //! hwer elatter value desbied 'emptyness' in a matrix, ie,, 'vlaues without data'.
    }
  }

  //!
  //! Apply logics:
  s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
  //! Compute for row[0] x row[1]:
  hp_config.obj_1_index1 = 0;   hp_config.obj_2_index2 = UINT_MAX;
  const bool is_ok = apply__hp_distance(obj_metric, &vec_1, &vec_2,  &vec_result, /*config=*/hp_config);
  assert(is_ok); //! ie, as we epxec the operaiton to have been 'a success'.
  //!
  //! 'Fetch' out the result:
  printf("# (info-aux)\t Result: input=[%u, %u]x[%u, %u], result=[%u, %u], at %s:%d\n", vec_1.nrows, vec_1.ncols, vec_2.nrows, vec_2.ncols, vec_result.nrows, vec_result.ncols, __FILE__, __LINE__);
  assert(vec_result.nrows == vec_1.nrows);   assert(vec_result.ncols == vec_2.nrows);
  assert(vec_result.matrix);   assert(vec_result.matrix[0]);
  //!
  //! Write out the result:
  t_float max_score = T_FLOAT_MIN_ABS; //! ie, the 'extrem min' value.
  for(uint col_id = 0; col_id < vec_result.ncols; col_id++) {
    const t_float scalar_result = vec_result.matrix[0][col_id]; 
    if(false) {fprintf(stdout, "# Result: sim-score[%u]=\"%f\", at [%s]:%s:%d\n", col_id, scalar_result, __FUNCTION__, __FILE__, __LINE__);}
    max_score = macro_max(max_score, scalar_result);
  }
  fprintf(stdout, "# Result(max): sim-score=\"%f\", at [%s]:%s:%d\n", max_score, __FUNCTION__, __FILE__, __LINE__);

  //!
  //! De-allocate:
  free__s_kt_matrix_base_t(&vec_1);
  free__s_kt_matrix_base_t(&vec_2);
  free__s_kt_matrix_base_t(&vec_result);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
 
