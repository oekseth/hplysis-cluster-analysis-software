/**
   @brief describes an algorithm for threshold-based identificaiton of trajectories in data
   @author Ole Kristian Ekseth (NTNU & Eltorque)
   @author Ole Vegard Solberg (SINTEF)
   @remarks steps taken for exeuction time optmizaiton:
   -- "map_compressedIndex_toGlobalIndex": transforms the glbpoal namespace into a lcoally filtered namespace, thereby reducing the search-space (though more importantly reduces the compelxity of memory allocaiton routines).
   -- 
   -- 
   @TODO before changes to data: 
   -- bug-fix: the Posix quicksort complatins about a stack which is too large; could be a misplaced call (or alterantively that the number of elemtns is too large);
   -- threshold adjustment: explore the effects of entropy, both wrt. the entrpoy emtric itself, and the effects of differnet confifrations (eg, number of elemtns to be sued for blurring, hsitgram colleciton, etc.);
   -- correlation training: imporve the map between the sytnethic data versus differnet metrics for correlation;
   
   @remarks reamining work-blocks (After the current TODOs and FIXMEs are resolved):
   -- training: graduallt ncrease the complecity of the training  through mapping of data to the results of metrics (eg, for correlation. blurrint, and entropy);
   -- exeuciton time: isolate the eprformance bottlencks, and then device + emasure the effect of altutive herustics (eg, for reading of data, use of KD-trees, etc.);
   -- fine tuning: integate herustics (eg, for trajvectory idneitifciaotn through use of "Bayesian NEtworks (BNs, different radius based on entopry scores, etc.)"

**/
//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_entropy.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'acc an API which is used by the highlevel-APIs in our hpLysis-software, both wrt. data-input and data-result.
#include "kt_matrix.h"
#include "hp_distance.h"
#include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis
#include "aux_sysCalls.h"
#include "kt_aux_matrix_norm.h" //! which hold the data-nraomizoant-enums.
#include "hp_image_segment.h"
#include "export_ppm.h" //! which is used for *.ppm file-export.
#include "kt_aux_matrix_blur.h"
//#endif
//! Export data to ppm
//! Note: the default fucntion exports data using grayscales. In the function we tranlsate numbers inot a range '0 ... 1', hence the need for data copy
static void __tut_sintef_1_exportTo_ppm(s_kt_matrix_t &mat_result, const char *file_base, const char *file_pref, const bool isTo_alsoExportTo_tsv) {
  //! Copy:
  s_kt_matrix_t mat_cpy = initAndReturn__copy__s_kt_matrix(&mat_result, true);
  //! Adjust(1): get max.
  t_float score_max = T_FLOAT_MIN_ABS;
  for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
    for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
      const t_float score = mat_cpy.matrix[row_id][col_id];
      if(isOf_interest(score)) {
	score_max = macro_max(score_max, score);
      }
    }
  }
  // printf("score_max:%f\n", score_max);
  if( (score_max != T_FLOAT_MIN_ABS) && (score_max != 0) ) {
    //! Adjust(1):
    t_float score_max_inv = 1/score_max;
    for(uint row_id = 0; row_id < mat_cpy.nrows; row_id++) {
      for(uint col_id = 0; col_id < mat_cpy.ncols; col_id++) {
	const t_float score = mat_cpy.matrix[row_id][col_id];
	if(isOf_interest(score)) {
	  mat_cpy.matrix[row_id][col_id] *= score_max_inv; 
	}
      }
    }
    if(isTo_alsoExportTo_tsv) {
      //if(self.isTo_exportResultFiles) {
      char file_name_result[1000]; memset(file_name_result, '\0', 1000); sprintf(file_name_result, "%s_%s_img.tsv", file_base, file_pref);
      export__singleCall__s_kt_matrix_t(&mat_result, file_name_result, NULL);
      //}
      //export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL);
      //(&mat_cpy, file_name_result, NULL);
      //export__singleCall__formatOf__csvWithoutIndetfiers__s_kt_matrix_t(&mat_cpy, file_name_result, NULL);
    }
    //! Export:
    char file_name_result[1000]; memset(file_name_result, '\0', 1000); sprintf(file_name_result, "%s_%s_img.ppm", file_base, file_pref);
    //	    const char *file_name_result_img = "tmp_img.ppm";
    //     fprintf(stderr, "(info:export)\t export to file=\"%s\", at %s:%d\n", file_name_result, __FILE__, __LINE__);
    ppm_image_t *picture = exportTo_image ( mat_cpy.matrix, mat_cpy.nrows, mat_cpy.ncols, mat_cpy.nameOf_rows, mat_cpy.nameOf_columns);
    ppm_image_write ( file_name_result, picture );
    ppm_image_finalize ( picture );
    /* is_ok = export__singleCall__s_kt_matrix_t(&mat_cpy, file_name_result, NULL); */
    /* assert(is_ok); */
    //! De-allocate:
  }
  free__s_kt_matrix(&mat_cpy);
}

typedef struct s_sintef_1_vectorVariance {
  e_kt_entropy_genericType_t enum_id_entr;
  bool postScaling_entr;
  uint config_maxRadius_low;
  uint config_maxRadius_high;
  bool compute_entropy;
  uint attenuationFactor_radiusPow;
  uint attenuationFactor_radiusPow_rad;  // FIXME: try using a dfferent name for this
} s_sintef_1_vectorVariance_t;


static s_sintef_1_vectorVariance_t init_s_sintef_1_vectorVariance_t() {
  s_sintef_1_vectorVariance_t self;
  self.enum_id_entr = e_kt_entropy_genericType_ML_Shannon;
  self.postScaling_entr = false;
  //! 
  self.config_maxRadius_low = 3;
  self.config_maxRadius_high = 3;
  //!
  self.compute_entropy = true;
  self.attenuationFactor_radiusPow = 0;
  self.attenuationFactor_radiusPow_rad = 0;  // FIXME: try using a dfferent name for this
  //!
  return self;
}
static s_kt_matrix_t _get_entropymatrix(s_sintef_1_vectorVariance_t *self, const s_kt_matrix_t &mat_input) {
  //, const uint config_maxRadius_entropy_low, const uint config_maxRadius_entropy_high) {
  // FIXME: try to genrelaised below ... validate that we ivnetigate all cells.
  //! Intiate:
  assert(mat_input.nrows > 0);
  assert(mat_input.ncols > 0);  
  const uint cnt_ele = 1 + self->config_maxRadius_low + self->config_maxRadius_high;
  //s_kt_list_1d_float_t vec_radiusDivFact = init__s_kt_list_1d_float_t(/*cnt-buckets=*/macro_max(self->config_maxRadius_low, self->config_maxRadius_high));
  // TODO: condier moving below to our "s_sintef_1_vectorVariance_t" ... ie, to avoid overhead wrt. below:
  //for(uint i = 0; < vec_radiusDivFact.list_size; i++) {
  //t_float 
  // self.attenuationFactor_radiusPow
  //!
  s_kt_matrix_t mat_res = setToEmptyAndReturn__s_kt_matrix_t();
  if(self->compute_entropy) {
    mat_res = initAndReturn__s_kt_matrix(/*nrows=*/mat_input.nrows, /*ncols=*/mat_input.ncols);
  } else { //! then we intaite an intermediate matrix holing the scores:
    // FIXME: ... figure out how the feuatre-vectors are to be used. 
    // FIXME: instead of returning a matrix of feature-vectors ... try instead to classify differnet data-chunks iaw. patterns.
    // FIXME: ... alternatively, deduce representaitve feature-veoctrs for each of the clusters ... then compare against these ... eg, usign a KD-tree.
    // FIXME: ... as a start ... construt an ensamble of vectors we compare against (given detials of teh input-data) .... eg "[0,0,0, 1, 1, 1,, 0, 0, 0]", where latter vector depicts a bell-shaped curve ... then use the MINE-metric for data-classificaiton .... write a sperate stand-alone exmaple covering this assumption of clasisficoant (which we use before itnetgraiotn the supprot for correlation). 
    // FIXME: ... 
    mat_res = initAndReturn__s_kt_matrix(/*nrows=*/mat_input.nrows * mat_input.ncols, /*ncols=*/cnt_ele);
    // mat_res.matrix[row_id_global][co] = vec.list[co];
  } 

    
  //!
  //!
  for(uint row_id = 0; row_id < mat_input.nrows; row_id++) {
    for(uint col_id = 0; col_id < mat_input.ncols; col_id++) {
      //! Intiate: 
      s_kt_list_1d_float_t vec = init__s_kt_list_1d_float_t(cnt_ele);
      //! Traverse the neighbourhood radius:
      if(self->config_maxRadius_low > 0) {
	vec.list[self->config_maxRadius_low] = mat_input.matrix[row_id][col_id];
      } else {
	vec.list[0] = mat_input.matrix[row_id][col_id];
      }
      uint offset_radius = 0;
      //!
      //! Step: Collect the entorpy scores: 
      // FIXME: ensure taht the input is scaled .... into integer-numbrs
      // FIXME: wrt. the use of the attenuation-factor ... 
      // FIXME: 
      // FIXME: 
      // FIXME: 
      if(self->config_maxRadius_low > 0) {
	offset_radius = self->config_maxRadius_low;
	for(uint r = 1; r <= self->config_maxRadius_low; r++) {
	  t_float score = 0; //! FIXME: vlaidate this score.
	  const uint c = (int)col_id - (int)r;
	  if( (c >= 0) && (c < mat_input.ncols) ) {
	    score = mat_input.matrix[row_id][c];
	  }
	  vec.list[r-1] = score; 
	}
      }
      if(self->config_maxRadius_high > 0) {
	for(uint r = 1; r <= self->config_maxRadius_high; r++) {
	  t_float score = 0; //! FIXME: vlaidate this score.
	  const uint c = (int)col_id + (int)r;
	  if(c < mat_input.ncols) {
	    score = mat_input.matrix[row_id][c];	    
	  }
	  if( (score != T_FLOAT_MAX) ) {
	    // FIXME: consider using diferent artimetics than below (ie, for adjsuting the sigicance of a score wrt. the radius). 
	    score = score + r*self->attenuationFactor_radiusPow_rad;  // FIXME: try using a dfferent name for this
	  }
	  if( (score != T_FLOAT_MAX) && (self->attenuationFactor_radiusPow > 0) ) {
	    // TODO: when we know n the min--max of th scores ... cosndider moving below to a premcompteud list ... then eprform a simple list-look-up ... and then compare the exec-time-effect fot eh differents trategies.
	    const t_float score_base = score;
	    // TODO: instead of below ... consider making use of an aprpxoiatme "pow(..)" funciton.
	    for(uint k = 0; k < self->attenuationFactor_radiusPow; k++) {
	      // FIXME: below seems wrong: ... copnsider including the radius as a vaariable ... then 'regarding' this strategy as a seperate paramter
	      score = score * score_base;
	    }
	  }
	  vec.list[r+offset_radius] = score;
	  //mat_input.matrix[row_id][col_id] = MF_get_score(col_id);
	}
      }
      //!
      t_float result = 0;	
      if(self->compute_entropy) {
	//! Step: Compute the entropy:
	uint max_theoreticCountSize = 0;
	s_kt_list_1d_uint_t list_count = init__s_kt_list_1d_uint_t(vec.list_size);
	assert(vec.list_size > 0); //! ie, what we expect.
	assert(list_count.list_size > 0); //! ie, what we expect.
	for(uint co = 0; co < list_count.list_size; co++) {
	  const uint thres = 100*1000;
	  if(vec.list[co] < thres) {
	    list_count.list[co] = (uint)vec.list[co]; // FIXME: ... 
	  } else {
	    list_count.list[co] = thres; // FIXME: validate this ... used to remove odd values
	  }
	  max_theoreticCountSize = macro_max(max_theoreticCountSize, list_count.list[co]);
	}
	assert(list_count.list_size > 0); //! ie, what we expect.
	compute_entropy__list__hp_entropy(&list_count, max_theoreticCountSize, self->enum_id_entr, /*isTo_applyPostScaling=*/(bool)self->postScaling_entr, &result);
	//!
	//! De-allocate:
	free__s_kt_list_1d_uint_t(&list_count);	
	//! Update: 
	mat_res.matrix[row_id][col_id] = result;
      } else { //! then compute correlation:
	// FIXME: ... should we instead store the reulsts into a matrix ... ??
	for(uint co = 0; co < vec.list_size; co++) {
	  assert(false); // FIXME: add updated logics for below
	  // mat_res.matrix[row_id_global][co] = vec.list[co];
	}
	//!
	// row_id_global += 1;
      }
      //!
      //! De-allocate:
      free__s_kt_list_1d_float_t(&vec);	      
    }
  }
#if 0
  // FIXME: cosnider removing below block.
  if(self->compute_entropy == false) { //! then we intaite an intermediate matrix holing the scores:
    //! Then compute correlation, for which a post-step is used.
    assert(false); // FIXME: rewrite below ...  consider merging below with the "#define MF_partOf_sameCluster(score1, score2)" function. 
    assert(false); // FIXME: validate bleow:
    //! Get shallow matrix-copies:
    s_kt_matrix_base_t corr_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_vec);
    s_kt_matrix_base_t hypo_shallow = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_hypo);
    assert(hypo_shallow.matrix == mat_hypo.matrix);
    assert(corr_shallow.matrix == mat_corr.matrix);
    s_kt_matrix_t mat_corrHypo = setToEmptyAndReturn__s_kt_matrix_t();
    s_kt_matrix_base_t mat_corrHypo_base = get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&mat_corrHypo);
    //! Compute correlation
    s_hp_distance__config_t hp_config = init__s_hp_distance__config_t();
    bool is_ok = apply__hp_distance(obj_metric, &corr_shallow, &hypo_shallow, &mat_corrHypo_base, hp_config);
    assert(is_ok);
    assert(mat_corrHypo_base.nrows > 0); //! ie, as we expect the operation was sucdcessful
    assert(mat_corrHypo_base.ncols > 0); //! ie, as we expect the operation was sucdcessful
    mat_corrHypo.nrows = mat_corrHypo_base.nrows;
    mat_corrHypo.ncols = mat_corrHypo_base.ncols;
    mat_corrHypo.matrix = mat_corrHypo_base.matrix;
    // mat_res = initAndReturn__s_kt_matrix(/*nrows=*/mat_input.nrows, /*ncols=*/mat_input.ncols);
  }
#endif //!  #if 0
  //!
  //! @return
  return mat_res;
}


//! Note: the below cofniguraitons are sued for explroation of different lien cases:
// FIXME: make use of a different strategy than below ... to make the code more generic.
static uint index_line[2] = {/*start=*/1, /*end=*/3};
//static const uint index_line[2] = {/*start=*/3, /*end=*/6};
static  t_float score_inside = 100;
static  t_float score_outside = 0;
static  t_float score_outside_oddNumbers = 0;
#define MF_get_score(col_id, score_inside, scoe_outside, score_outside_oddNumbers) ({ t_float result = score_inside; if( (col_id < index_line[0]) || (col_id >= index_line[1]) ) {result = score_outside;} result;})
//#define MF_get_score(col_id, score_inside, scoe_outside) ({ t_float result = score_inside; if( (col_id < index_line[0]) || (col_id >= index_line[1]) ) {result = score_outside;} result;})

//! use the input-boundaries as threshold-value, apply clustering, and then export the udpated matrix.
static void tut_sintef_applyThresholding(const s_kt_matrix_t &mat_input, const char *file_base, const char *str_description, const uint config_maxRadius_threshold, const t_float mask_filter, const bool isTo_apply_syntethticInput_tests, const bool isTo_export) {
  //! --------------------
  //!
  //! Configuation:
  const bool config_assumeMostOfPointsAreToBeIngored = true; //! which is used to detemrien if an index-compresison tehcnie is to be used.
  const bool debug = false;
  // TODO: consider instead of "s_kt_list_1d_uint_t" to use a pair(uint, uint) strucutre, ie, to avodi issues wrt. "nrows*ncols >= UINT_MAX"
  s_kt_list_1d_uint_t map_compressedIndex_toGlobalIndex = setToEmpty__s_kt_list_1d_uint_t();
  //! --------------------
  //!  
  //! Define axuialy functions to insert, and access, the comrpessed function.
#define MF_compressed_getFromValue(val) ({				\
      uint index_compressed = UINT_MAX;					\
      for(uint i = 0; (i < map_compressedIndex_toGlobalIndex.current_pos) && (index_compressed == UINT_MAX); i++) { \
	if(map_compressedIndex_toGlobalIndex.list[i] == val) {index_compressed = i;} \
      } index_compressed;}) //! ie, then return the compressed index.
#define MF_compressed_getFromValue_addIfNotFound(val) ({		\
      uint index_compressed = MF_compressed_getFromValue(val);		\
      if(index_compressed == UINT_MAX) { /*! then it was NOT found: add it: */ \
	index_compressed = push__s_kt_list_1d_uint_t(&map_compressedIndex_toGlobalIndex, val); \
      } index_compressed;}) //! ie, then return the compressed index.
  //! --------------------
  //!  
  //! Step: apply clustering:
  s_kt_forest_findDisjoint self; //! which holds the result of the clustering:
  {
    //! Allocate the sparse matrix:
    //! Note: a sparse matrix is sued to reduce the memroy siganture.
    // FIXME: validate below traverse-region:
    const uint cnt_vertices = mat_input.nrows * mat_input.ncols;
    assert(cnt_vertices > mat_input.nrows); //! as we otheyrwsei ahve passed the numerical liimit of "UNSIGNED_INT".
    assert(cnt_vertices > mat_input.ncols); //! as we otheyrwsei ahve passed the numerical liimit of "UNSIGNED_INT".
    s_kt_set_2dsparse_t listOf_relations = setToEmptyAndReturn__s_kt_set_2dsparse_t();
    s_kt_list_1d_pair_t list_1d_relations = setToEmpty__s_kt_list_1d_pair_t();
    if(config_assumeMostOfPointsAreToBeIngored) { //! then we asusem only a subset of the indexes are to be used.
      //listOf_relations = initAndReturn__allocate__s_kt_set_2dsparse_t(/*cnt_vertices=*/1000, /*isTo_allocateFor_scores=*/false);
      //listOf_relations = initAndReturn__allocate__s_kt_set_2dsparse_t(/*cnt_vertices=*/5, /*isTo_allocateFor_scores=*/false);
      list_1d_relations = init__s_kt_list_1d_pair_t(/*cnt-vertices=*/5); //! ie, to 'force' an itnalizaiton.
      map_compressedIndex_toGlobalIndex = init__s_kt_list_1d_uint_t(/*cnt_vertices=*/1000);
    } else { //! then we allcoate for all of the points:
      listOf_relations = initAndReturn__allocate__s_kt_set_2dsparse_t(cnt_vertices, /*isTo_allocateFor_scores=*/false);
    }
    // s_kt_set_2dsparse_t listOf_relations = initAndReturn__allocate__s_kt_set_2dsparse_t(mat_input.nrows, /*isTo_allocateFor_scores=*/false);
    // FIXME: consider usign a different aproach/smeantics for below:
#define MF_partOf_sameCluster(score1, score2) ({bool same_cluster = (score1 == score2); same_cluster;}) //! which is a simplified thesholding. In pracitse, an apprixmate thresholding shoudl be sued, ie, to avodi getting lost in minor disturbances. 
    //#define MF_partOf_sameCluster(score1, score2) ({bool same_cluster = false; if(score1 == score2) {same_cluster = true;} same_cluster;}) //! which is a simplified thesholding. In pracitse, an apprixmate thresholding shoudl be sued, ie, to avodi getting lost in minor disturbances. 
    //! Note: an latenraitve allocaiton strategy (with a coniserable memory footpint overhead) is to use the following accoation rpcoess: s_kt_set_2dsparse_t listOf_relations; allocate__s_kt_set_2dsparse_t(&listOf_relations, nrows, ncols, matrix, mask, isTo_useImplictMask, /*isTo_allocateFor_scores=*/false);    
    // FIXME: how may we use below script in our IoT sensor-AI-network (eg, wrt. Bayseain Networks (BNs))?
    //!
    //! Step: add relations of interest:
    //! Note: the below step describes an itneresting permutation of disjoint clusteirng, hence an opprotunity which may be used as an alteantive to MST cases where the scores are stronly similar.
    //! Nopte: for the disjoint-partion to work we need to handle the issue of the row-IDs mapping to a different namespace that the column-IDs. To address this issue we .... 
    uint row_index_global = 0;
    uint max_index_inserted = 0;
    for(uint row_id = 0; row_id < mat_input.nrows; row_id++) {
      for(uint col_id = 0; col_id < mat_input.ncols; col_id++) {
	const t_float score1 = mat_input.matrix[row_id][col_id];
	if(
	   // FIXME: try to re-inlcdue below when current bugs are resolved.
	   // (col_id <= row_id) || // TODO: validte this assumption ... which is based on the critiera/assumption that 'there is not point in inserteditn recirprocal relationships'. 
	   ( (mask_filter != T_FLOAT_MAX) && (score1 == mask_filter) )
	   ) {
	  // printf("\t(skips)\t [%u][%u]\n", row_id, col_id);
	  row_index_global++; 
	  continue;
	}
	//! Validate correctness of the mapping:
	const uint row_index_global_ex = row_id * mat_input.ncols + col_id;
	assert(row_index_global_ex == row_index_global); //! ie, as we otehrwise have an isntoency wrt. our mapping.
	uint row_index_global_compressed = UINT_MAX;
	if(config_assumeMostOfPointsAreToBeIngored) {
	  row_index_global_compressed = MF_compressed_getFromValue_addIfNotFound(row_index_global);
	  assert(row_index_global_compressed != UINT_MAX); //! ie, as we expect it to be set. 
	}
	//! Traverse the neighbourhood radius:
	for(uint r = 0; r < config_maxRadius_threshold; r++) {
	  // FIXME: try to genrelaised below ... validate that we ivnetigate all cells.
	  // FIXME: extend below to cover voxels (ie, 3d-volumes):
	  enum {c_backward, c_forward, c_none};
#define MF_get_adjustedPos(enum_id, radius) ({int offset = 0;		\
	      /*! Note:  '+1' is used to avoid a self-comparison. */	\
	      if(enum_id == c_backward) {offset = (1+radius) * -1;} /*TODO[exex-time]: consider replcing '*-1' with an alterantive  bit-ship strategy*/ \
	      else if(enum_id == c_forward) {offset = (1+radius);}	\
	      else {assert(enum_id == c_none);} /*which is used to simplify deteciton of future incosnsitnet code-changes*/ \
	      offset;}) /*ie, return the offset*/
		    
#define _map_cases_size 8
	  const int map_cases[_map_cases_size][3] = {
	    //! Note: the moviaiton of the hard-cocding (below) is to simplify the conpeutal validaiton of  the cases we cover:
	    // FIXME: instead of below ... write a for-loop cpaturing below use-cases .... thewn test the exex-time
	    //!  ------------------------------
	    //! Cases: Movements in a signle direction:
	    {/*row=*/c_backward, /*col=*/c_none, /*z=*/c_none}, //! case: "<-"
	    {/*row=*/c_forward, /*col=*/c_none, /*z=*/c_none}, //! case: "->"
	    {/*row=*/c_none, /*col=*/c_backward, /*z=*/c_none}, //! case:"."  (ie, horiz-down)
	    {/*row=*/c_none, /*col=*/c_forward, /*z=*/c_none}, //! case:"|" (ie, horiz-up)
	    //!  ----------
	    //! Cases: diagonal movements.
	    {/*row=*/c_backward, /*col=*/c_backward, /*z=*/c_none}, //! case: diag:backwards-up
	    {/*row=*/c_backward, /*col=*/c_forward, /*z=*/c_none},  //! case: diag:backwards-down
	    {/*row=*/c_forward, /*col=*/c_backward, /*z=*/c_none},  //! case: diag:forward-up
	    {/*row=*/c_forward, /*col=*/c_forward, /*z=*/c_none},   //! case: diag:forward-down		
	    //!  ----------
	    //! Cases:
	    // {/*row=*/c_, /*col=*/c_, /*z=*/c_none}, //! case:		
	    //!  ------------------------------
	  };
	  const uint map_cases_size = _map_cases_size;
#undef _map_cases_size //! ie, to simplify future debugging.
	  for(uint case_id = 0; case_id < map_cases_size; case_id++) { 
	    //! Identify the neibhbour to be used:
	    const int r_row_id = row_id + MF_get_adjustedPos(map_cases[case_id][/*row-index=*/0], /*radius=*/r);
	    const int r_col_id = col_id + MF_get_adjustedPos(map_cases[case_id][/*col-index=*/1], /*radius=*/r);
	    // FIXME: itnegrate the use of the "z" direciotnal attributes in below:
	    if( (r_row_id >= 0) && (r_col_id >= 0)  &&  (r_row_id < mat_input.nrows) && (r_col_id < mat_input.ncols) ) {
	      // TODO: should we adjsut the score wrt. the attenuation score?
	      const t_float score2 = mat_input.matrix[r_row_id][r_col_id];
	      if(MF_partOf_sameCluster(score1, score2)) {
		if( (mask_filter != T_FLOAT_MAX) && (score2 == mask_filter) ) {
		  assert(false); //! ie, as we assuem this case should not be entered at this exec-point
		}	      		
		// FIXME: validte correctness of below:
		const uint col_index_global = r_row_id*mat_input.ncols + r_col_id;
		//const uint col_index_global = r_col_id*mat_input.nrows + r_row_id;
		//! validate a reverse mapping of above.
		//printf("\t %u VS %u:[%u][%u], given size
		if(debug) printf("\t ::[%u][%u]==>[%u][%u]\n", row_id, col_id, r_row_id, r_col_id);
		//! Add the map:
		if(config_assumeMostOfPointsAreToBeIngored) {
		  assert(row_index_global_compressed != UINT_MAX); //! ie, as we expect it to be set. 
		  const uint col_index_global_compressed =  MF_compressed_getFromValue_addIfNotFound(col_index_global);
		  assert(col_index_global_compressed != UINT_MAX);
		  assert(col_index_global_compressed != row_index_global_compressed); //! ie, as this indicaes an error.
		  //! Add:
		  if(true) {
		    push__s_kt_list_1d_pair_t(&list_1d_relations,  row_index_global_compressed, col_index_global_compressed);
		  } else {
		    push__s_kt_set_2dsparse_t(&listOf_relations, row_index_global_compressed, col_index_global_compressed);
		  }
		  //! Update the set of inserted indexes:
		  max_index_inserted = macro_max(max_index_inserted, row_index_global_compressed);
		  max_index_inserted = macro_max(max_index_inserted, col_index_global_compressed);
		} else { //! Add without index-comrpession: 
		  push__s_kt_set_2dsparse_t(&listOf_relations, row_index_global, col_index_global);
		  max_index_inserted = macro_max(max_index_inserted, col_index_global);
		}
	      } else {
		; // printf("\t (ignored)\t [%u][%u]==>[%u][%u]\n", row_id, col_id, r_row_id, r_col_id);
	      }
	    }
	  }
	  // assert(score_outside >= score_inside); //! ie, as we otehrwise need updating below:
	  // if(mat_input.matrix[row_id][col_id] >= score_outside) {
	}      
	//! Increment:
	assert(row_index_global < UINT_MAX);
	//! validate a reverse mapping of above.
	const uint row_index_global_ex_ = row_id * mat_input.ncols + col_id;
	assert(row_index_global_ex_ == row_index_global); //! ie, as we otehrwise have an isntoency wrt. our mapping.
	row_index_global++; 
      }
    }
    //assert(false); // FIXME: remvoe.    
#undef MF_get_adjustedPos
    if(config_assumeMostOfPointsAreToBeIngored) {
      listOf_relations = convertFrom__s_kt_list_1d_pair__s_kt_set_2dsparse_t(&list_1d_relations);
      free__s_kt_list_1d_pair_t(&list_1d_relations);
      //! 
    } //else {
      // listOf_relations.list_size = listOf_relations.current_pos; //! ie, to simplify debugging.
    allocate__s_kt_forest_findDisjoint(&self, /*nrows=*/max_index_inserted+1, &listOf_relations);
      //}
    //! -----------------------------
    //assert(false); // FIXME: remove      
    //!
    //! Infer the disjotin sets:
    self.typeOf_startVertices = e_kt_forest_findDisjoint_configureAlgorithm__typeOfInitialStartVertices_defaultIndexOrder; //! ie, to reduce the cost of the pre-intit-operaiton (eg, wrt. the need for applying sorting).
    graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/false);
    //graph_disjointForests__s_kt_forest_findDisjoint(&self, /*isTo_identifyCentrlaityVertex=*/true);
    //assert(false); // FIXME: remove      
    //! -----------------------------
    //!
    //! Identify the number of itneresting clusters:
    const uint cnt_total_vertices = get_cnt_vertcies_invovlesInDisjointForests__s_kt_forest_findDisjoint(&self);
    const uint cnt_interesting_disjointForests = get_cnt_disjointForests_inMinMaxRange__s_kt_forest_findDisjoint(&self, /*countThreshold_min=*/UINT_MAX, /*countThreshold_max=*/UINT_MAX);
    printf("cnt_total_vertices=%u, cnt_interesting_disjointForests=%u, at %s:%d\n", cnt_total_vertices, cnt_interesting_disjointForests, __FILE__, __LINE__);
    //! Validate the result:
    //! As we have 'stated' that there are 'no masks to use' we expec tall vertices to be found in the same cluster:
    // FIXME: include impoved asserts for below: 
    // assert(cnt_total_vertices == mat_input.nrows); //! ie, the totla number of unique vertices inserted in [above]
    // assert(cnt_interesting_disjointForests == cnt_interesting_disjointForests_expected); //! ie, as we expect the cluster-count to reflect hte maximum possible number of clusters.
    //! -----------------------------
    //! De-allocate:	
    free_s_kt_set_2dsparse_t(&listOf_relations);    
  }

      
  //! -----------------------------
  //! 
  //! Step: map cluster-IDs to the input image:
  s_kt_matrix_t mat_afterCluster = initAndReturn__s_kt_matrix(mat_input.nrows, mat_input.ncols);
  t_float map_clusterID[2] = {/*inside=*/1, /*outside=*/2};
  uint row_index_global = 0;
  for(uint row_id = 0; row_id < mat_input.nrows; row_id++) {
    for(uint col_id = 0; col_id < mat_input.ncols; col_id++) {      
      uint forest_id = UINT_MAX;
      if(config_assumeMostOfPointsAreToBeIngored) {
	const uint compressed_index = MF_compressed_getFromValue(row_index_global);
	if(compressed_index != UINT_MAX) {
	  forest_id = get_foreestId_forVertex__s_kt_forest_findDisjoint(&self, compressed_index);
	}
      } else {
	forest_id = get_foreestId_forVertex__s_kt_forest_findDisjoint(&self, row_index_global);
      }
      t_float score = map_clusterID[1]; //T_FLOAT_MAX;
      //if(forest_id != UINT_MAX)  { score = map_clusterID[0];} // 100.0 + (t_float)forest_id; }
      if(forest_id != UINT_MAX)  { score = 100.0 * (t_float)(forest_id + 1); }
      //if(forest_id != UINT_MAX)  { score = 100.0 + (t_float)forest_id; }
      if(debug) printf("\t [%u][%u]:%u in forest:%u => %u\n", row_id, col_id, (uint)mat_input.matrix[row_id][col_id], forest_id, (uint)score);
      //! Insert:
      mat_afterCluster.matrix[row_id][col_id] = score;
      //! Increment:
      row_index_global++;
    }
  }
  // assert(false); // FIXME: remvoe.
  //! -----
  //! De-allocate:
  free_s_kt_forest_findDisjoint(&self);

  if(isTo_apply_syntethticInput_tests) {
    //!
    //! Step: valdiate the cluster segmentaitons wrt. the input-configurations.
    for(uint row_id = 0; row_id < mat_input.nrows; row_id++) {
      for(uint col_id = 0; col_id < mat_input.ncols; col_id++) {
	if(debug
	   && ((uint)mat_afterCluster.matrix[row_id][col_id] != (uint)map_clusterID[1]) // TODO: cosnider dropping this.
	   )  printf("\t [%u][%u]:%u vs %u:mat_input \t (score_inside:%u,  clustID:%u, mat(afterClust):%u\n", row_id, col_id,
		     (uint)MF_get_score(col_id, score_inside, scoe_outside, score_outside_oddNumbers) ,
			  (uint)mat_input.matrix[row_id][col_id],
			  (uint)score_inside, (uint)map_clusterID[0], (uint)mat_afterCluster.matrix[row_id][col_id]);
	//!
	assert((uint)mat_input.matrix[row_id][col_id] == (uint)MF_get_score(col_id, score_inside, scoe_outside, score_outside_oddNumbers)); //! ie, consitnecy test, ie, as the matrix may have wrongly changed during the rpcoiessing
	if(MF_get_score(col_id, score_inside, scoe_outside, score_outside_oddNumbers) == score_inside) {
	  // FIXME: include below:
	  // assert(mat_afterCluster.matrix[row_id][col_id] == map_clusterID[0]);
	} else { //! then we expect the results to be outside: 
	  //! Note: below is tempoary de-actived as we cluster only for within-regionn
	  // assert(mat_afterCluster.matrix[row_id][col_id] == map_clusterID[1]);
	}
	// mat_input.matrix[row_id][col_id] = MF_get_score(col_id);
      }
    }
  } //! and at this exec-point the syn-tets are completed. 
//!
  //! Step: export the updated matrix as a *.ppm file:
if(isTo_export) {
  __tut_sintef_1_exportTo_ppm(mat_afterCluster, file_base, str_description, /*isTo_alsoExportTo_tsv=*/true);
 }
  //!
  //! De-allocate the data:
  free__s_kt_matrix(&mat_afterCluster);
  free__s_kt_list_1d_uint_t(&map_compressedIndex_toGlobalIndex);
}

static void tut_sintef_testClassificationAccuracy_syntTrainingData(const s_kt_correlationMetric_t &obj_metric_corr) {
  //!
  //! Write a wrapper-fucntion for a non-otpmzied class to comptaution of correlation:
#define MF_Sintef_corr(row_1, row_2) ({					\
      t_float result = T_FLOAT_MAX;					\
      const bool is_ok = apply__rows_hp_distance(obj_metric_corr, (t_float*)row_1, (t_float*)row_2, /*ncols=*/_cnt_cases_features, &result); \
      assert(is_ok);							\
      result;}) //! ie, return.
  //!
  //! Step: construct a set of input-data:
#define _cnt_cases_rows 4
#define _cnt_cases_features 7
  // FIXME: try  mapping below use-cases to those encoutned by both Eltroque and SINTEF.
  t_float mat_classes_cmp[_cnt_cases_rows][_cnt_cases_features]
    = {
    //! Note: below cses tries to emulate an image where the vector catprues teh entropy for a given 'voxel inside a line-region'.
    {0, 0, 0, 1, 0, 0, 0 },
    {0, 0, 1, 1, 1, 0, 0 },
    {0, 0, 0, 10, 0, 0, 0 },
    {0, 0, 10, 10, 10, 0, 0 },
    /*
    //! Note: below cses tries to emulate an image where the vector catprues teh entropy for a given 'voxel outside a line-region'.
    {0, 0, 0, 1, 0, 0, 0 },
    {0, 0, 1, 1, 1, 0, 0 },
    {0, 0, 0, 10, 0, 0, 0 },
    {0, 0, 10, 10, 10, 0, 0 },	
    */
  };
  //! Note: in the experiemetnation of which training-data to use the follwoing 'mamnual' convergence critera are applied: ...??...
  for(uint case_id = 0; case_id < _cnt_cases_rows; case_id++) { //! Apply hardd-coded strategyes for filters:
    //!
    //! Step: Construct a hard-coded ensamble of classes: 
#define mat_classes_size 12
    t_float mat_classes[mat_classes_size][_cnt_cases_features]
      =  {
      //! a positive midpoint: 
      {0, 0, 0, 1, 9, 9, 9 },
      {0, 0, 0, 1, 0, 0, 0 },
      {0, 0, 0, 100, 9, 9, 9 },
      {0, 0, 0, 100, 0, 0, 0 },	    
      //! a negative midpoint
      {1, 1, 1, -1, 1, 1, 1 },
      {1000, 1000, 10000, -1000, 1000, 1000, 100 },
      //! Increase and decrease:
      {1, 2, 3, 4, 5, 6, 7 },
      {100, 200, 300, 400, 500, 600, 700 },
      {7, 6, 5, 4, 3, 2, 1 },
      {700, 600, 500, 400, 300, 200, 100 },
      //! Equal lines:
      {1, 1, 1, 1, 1, 1, 1 },
      {1000, 1000, 10000, 1000, 1000, 1000, 100 },
      // { },
    };
	
    for(uint class_index = 0; class_index < mat_classes_size; class_index++) {
      //!
      //! Step: Compute correlation for [feature-vectors, training-data]:
      const t_float corr_score = MF_Sintef_corr(mat_classes_cmp[case_id], mat_classes[class_index]);	
      //!
      //! Step: visually + programmatic validatation of the results
      printf("\t [case:%u, training:%u] => %f\t (tutSintef::corr-syn-test)\n", case_id, class_index, corr_score);
    }
  }
  //!
  //! Step: Describe a rpogrammtic validation-strategy wrt. above. 
  assert(false); // ... 
  //!
  //! Capture/Devise useful reccomenaitosn from above experiement.
  assert(false); // ...		      
  for(uint case_id = 0; case_id < _cnt_cases_rows; case_id++) { //! Use functions to automatially generate test-data.
    //! Note. This appraoch is motivated by: ...
    //! Note.
    //! Note.	
    assert(false); // ...
  }			  			   
  assert(false); // FIXME: describe a test-setup which programmtially vlaidate the rsults. 
  //!
  //! Capture/Devise useful reccomenaitosn from above experiement.
  assert(false); // ...		 
}

typedef struct s_sintef_1_config_crudeAlg {
  e__blur config_apply_blur_enum; // eg,: e__blur_gaussian_4_accumulator; //! as defined in "kt_aux_matrix_blur.h"
  s_kt_correlationMetric_t obj_metric_corr;
  uint config_maxRadius_threshold;
  uint config_maxRadius_entropy;
  uint config_maxRadius_corr; //   = 3;
  bool isTo_exportResultFiles; //! a swithc used to improve perofmrqance (ie, for use-cases wheere a summary of the scores are sufincet to demtirne the perfomrance of a buling-blocks configuraiotn).
  //!
  s_sintef_1_vectorVariance_t obj_vectorVariance; //
} s_sintef_1_config_crudeAlg_t;

s_sintef_1_config_crudeAlg_t init__s_sintef_1_config_crudeAlg_t() {
  s_sintef_1_config_crudeAlg_t self;
  //!
  self.obj_metric_corr = initAndReturn__s_kt_correlationMetric_t(/*metric=*/e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock, /*prestep=*/e_kt_categoryOf_correaltionPreStep_rank);
  self.config_apply_blur_enum = e__blur_undef; // eg,: e__blur_gaussian_4_accumulator; //! as defined in "kt_aux_matrix_blur.h"
  //!
  self.config_maxRadius_threshold = 1; //! ie, the radius of sampling during thresholding.
  self.config_maxRadius_entropy   = 2; //! which is the radius of the entorpy-construction, resulting in a dimater of "radius + radius + 1".
  self.config_maxRadius_corr   = 3;
  //!
  self.isTo_exportResultFiles = false;
  //!
  // FIXME: exploe the effect of [low, high, both]:
  self.obj_vectorVariance = init_s_sintef_1_vectorVariance_t();  
  self.obj_vectorVariance.enum_id_entr = e_kt_entropy_genericType_ML_Shannon;
  self.obj_vectorVariance.config_maxRadius_low = self.config_maxRadius_entropy;
  self.obj_vectorVariance.config_maxRadius_high = self.config_maxRadius_entropy;
  self.obj_vectorVariance.compute_entropy = true;
  self.obj_vectorVariance.postScaling_entr = true;
  self.obj_vectorVariance.attenuationFactor_radiusPow = 2; //! ie, score/radius^2  
  //!
  //!
  return self;
}

/*********************************************//*********************************************//*********************************************//*********************************************//*********************************************/
static void _tut_sintef_apply_hardCoded_case_xmtAutoTraining(s_sintef_1_config_crudeAlg_t &self, const char *file_base) {
  /*********************************************/
  /****************** CONFIGURATION: ***************************/
  //! Step: Construct a simple shape: a line with empty space in between:
  s_kt_matrix_t mat_input = setToEmptyAndReturn__s_kt_matrix_t();
  //! 
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/3, /*ncols=*/4);
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/3, /*ncols=*/40);
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/10, /*ncols=*/10);
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/10, /*ncols=*/500);
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/10, /*ncols=*/100);
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/10, /*ncols=*/400);
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/30, /*ncols=*/40);
  

  mat_input = initAndReturn__s_kt_matrix(/*nrows=*/300, /*ncols=*/400);
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/300, /*ncols=*/4000);  
  //mat_input = initAndReturn__s_kt_matrix(/*nrows=*/3000, /*ncols=*/4000);  
  //! 
  //! Configuraitons:
  if(true) { //! then we adjsut the eytntic line-size:
    if(mat_input.ncols > 10) {
      if(false) {
	index_line[0] = 2; // (uint)( 0.4*(t_float)mat_input.nrows); 
	index_line[1] = 10; // (uint)( 0.6*(t_float)mat_input.nrows);
      } else {
	index_line[0] =  (uint)( 0.2*(t_float)mat_input.ncols); 
	index_line[1] =  (uint)( 0.6*(t_float)mat_input.ncols);	
      }
      printf("space(line)=[%u, %u]\n", index_line[0], index_line[1]);
    }
  }
  const bool config_apply_blur = (self.config_apply_blur_enum != e__blur_undef); // FIXME: ... 
  const uint config_maxRadius_blur = 3;
  //const e__blur config_apply_blur_enum = e__blur_gaussian_4_accumulator; //! as defined in "kt_aux_matrix_blur.h"
  
  const uint cnt_interesting_disjointForests_expected = 2; //! ie, based on how we consturcted the sytenthci data
  //! Note: the below cofniguraitons are sued for explroation of different lien cases:
  const t_float score_inside = 100;
  const t_float score_outside = 0;
  const t_float score_outside_oddNumbers = 0;
  //! Valite input; do not test for negative valeus as 'uint' is used.
  assert(index_line[0] < mat_input.ncols);
  assert(index_line[1] < mat_input.ncols);
  //! Draw the line:
  for(uint row_id = 0; row_id < mat_input.nrows; row_id++) {
    for(uint col_id = 0; col_id < mat_input.ncols; col_id++) {
      mat_input.matrix[row_id][col_id] = MF_get_score(col_id, score_inside, scoe_outside, score_outside_oddNumbers);
    }
  }

  //!
  //! Step: Export the input image as a *.ppm file:
if(self.isTo_exportResultFiles) {
  __tut_sintef_1_exportTo_ppm(mat_input, file_base, "parsedInputData", /*isTo_alsoExportTo_tsv=*/true);
 }
  /*********************************************/
  /*********************************************/
  if(false) { //! use the input-boundaries as threshold-value, apply clustering, and then export the udpated matrix.      
    const t_float mask_filter = score_outside; //! which is used to omit 'background data' from the clusteirng-process
    //! Note: this step is used/included to (among othrs) visually vlaidate the cluster results.
    tut_sintef_applyThresholding(mat_input, file_base, "parsedInputData-ClusterId", self.config_maxRadius_threshold, mask_filter, /*isTo-apply-syntethticInput-tests=*/true, /*isTo_export=*/self.isTo_exportResultFiles);
  }
  // assert(false); // FIXME: validate the above stpes ... then make use of our already implemtantion functions.
  //!
  //!
  s_kt_matrix_t mat_blur = mat_input;
  if(config_apply_blur) { //! Apply a blurring-step:
      mat_blur = apply_blur__s_kt_matrix_t(&mat_input, self.config_apply_blur_enum, config_maxRadius_blur);
  }
  //! 
  //! Step: Apply entropy:
  s_kt_matrix_t mat_entr = setToEmptyAndReturn__s_kt_matrix_t();
  if(true)  {
    //! Step: Apply entropy:
    //! Compute: 
    mat_entr = _get_entropymatrix(&(self.obj_vectorVariance), mat_blur);
    //! 
    if(true) { //! then export the entorpy-matrix, eg, for vlidaiton:
      /**
	 @brief task: deduce the applicaiton of entropy:
	 @case: data:line, mat(3, 4), entr(shannon), radius(2), attenuation:0, postScaling_entr:false
	 -- result: entropy is equal for all cells, which is due to ...??...
	 @case: data:line, mat(300, 400), entr(shannon), radius(2), attenuation:0, postScaling_entr:false
	 -- entropy(pattern): "[0.046032        0.029947        0.022668        0.018439 ....  0.018439, 0.022668, 0.029947, 0.046032]"
	 -- result: entropy equals in the cells 'in the middle', which is either due to correct line-detection, or ...  which could be due to lack of scaling wrt. the boundary areas.
	 @case: data:line, mat(300, 400), entr(shannon), radius(2), attenuation:0, postScaling_entr:true
	 -- entropy(pattern): 
	 -- result: "[(column:start=lineStart - traverseRadius):: 0.019991        0.013006        0.009845        0.008008        0.006794 ...  ... <and then backwards again until: lineEnd + traverseRadius>]"  ... 
	 @case: data:line, mat(300, 400), entr(shannon), radius(2), attenuation:radius*radius, postScaling_entr:true
	 -- entropy(pattern):
	 -- result: 
       **/
      const char *file_pref = "entropy-result";
      char file_name_result[1000]; memset(file_name_result, '\0', 1000); sprintf(file_name_result, "%s_%s_img.tsv", file_base, file_pref);
      if(self.isTo_exportResultFiles) {
	export__singleCall__s_kt_matrix_t(&mat_entr, file_name_result, NULL);
      }
      //__tut_sintef_1_exportTo_ppm(mat_input, file_base, file_pref, /*isTo_alsoExportTo_tsv=*/true);
      if(true) { //! then export the deviaiton between the columns ... as we expect each of the columns to describe a given row.
	char file_name_result[1000]; memset(file_name_result, '\0', 1000); sprintf(file_name_result, "%s_%s-dev.tsv", file_base, file_pref);
	s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_entr);
	if(false) {
	  if(self.isTo_exportResultFiles) {
	    extractAndExport__deviations__singleCall__tsv__s_kt_matrix_t(&mat_transp, file_name_result);
	  }
	} else { //! then use a home-made strategy (which prints only a minor subset of the data, ehnce simplifying validation):
	  //!
	  { //! Describe different cases to remmeber:
	    //! Note: below is indeded to simplify the capturing of effects wrt. different configuraitons (hecen a mtoation for using simpleid sytnethci data).
	    uint index_empty_lastBefore = 0;
	    uint index_empty_firstAfter = UINT_MAX;
	    //!
	    uint index_outside_lastBefore = 0;
	    uint index_outside_firstAfter = UINT_MAX;	    
	    //!
	    t_float insideBlock_valueMax = T_FLOAT_MIN_ABS;
	    t_float insideBlock_valueMin = T_FLOAT_MAX;
	    //!
	    printf("limit[%u, %u]\n", index_line[0], index_line[1]);
	    for(uint row_id = 0; row_id < mat_transp.nrows; row_id++) {	    
	      for(uint col_id = 0; col_id < mat_transp.ncols; col_id++) {
		const t_float score = mat_transp.matrix[row_id][col_id];
		//! Note: a confusing aspect in below concerns how the 'rows' and 'columsn are 'turedn upsided-wpn' (deut to the transposed matrix we are using). 
		if(isOf_interest(score)) {
		  if( (row_id >= index_line[0]) &&  (row_id < index_line[1]) ) {
		    insideBlock_valueMin = macro_min(insideBlock_valueMin, score);
		    insideBlock_valueMax = macro_max(insideBlock_valueMax, score);
		  } else { //! then the socre is set, while the value is outside
		    // TODO: cosndier adding something.
		    if(row_id >= index_line[1]) {		    
		      index_outside_firstAfter = macro_min(index_outside_firstAfter, row_id);
		    } else {
		      index_outside_lastBefore = macro_max(index_outside_lastBefore, row_id);
		    }		    
		  }
		} else { //! then the score is not set (hence, no variance in inptu-data.
		  if(row_id >= index_line[1]) {		    
		    index_empty_firstAfter = macro_min(index_empty_firstAfter, row_id);
		  } else if(row_id < index_line[0]) {
		    index_empty_lastBefore = macro_max(index_empty_lastBefore, row_id);
		  }  
		}
	      }
	    }
	    //!
	    //! Write out result to stdout, ie, to simplify logging + descirpiotn (eg, of how differnet combiantions influence the accurayc).
	    printf("\t entropyStat\t emptyIndex[%u, %u]\t borderRegion[%u, %u] \t scoresInside[%.2f -- %.2f]\t (%s)\n",
		   index_empty_lastBefore, index_empty_firstAfter,
		   index_outside_lastBefore, index_outside_firstAfter,
		   insideBlock_valueMin, insideBlock_valueMax, file_base);
	  }
	  //!
	  s_kt_matrix_t mat_dev = initAndReturn__s_kt_matrix(/*nrows=*/mat_transp.nrows, /*ncols=*/3);
	  //!
	  //! Set a desiprive col header:
	  set_stringConst__s_kt_matrix(&mat_dev, /*index=*/0, "average", /*addFor_column=*/true);
	  set_stringConst__s_kt_matrix(&mat_dev, /*index=*/1, "min", /*addFor_column=*/true);
	  set_stringConst__s_kt_matrix(&mat_dev, /*index=*/2, "max", /*addFor_column=*/true);
	  //!
	  for(uint row_id = 0; row_id < mat_transp.nrows; row_id++) {
	    t_float sum = 0;
	    uint cnt_interesting = 0;
	    t_float score_min = T_FLOAT_MAX;
	    t_float score_max = T_FLOAT_MIN_ABS;
	    //!
	    for(uint col_id = 0; col_id < mat_transp.ncols; col_id++) {
	      const t_float score = mat_transp.matrix[row_id][col_id];
	      if(isOf_interest(score)) {
		sum += score;
		score_min = macro_min(score_min, score);
		score_max = macro_max(score_max, score);
		cnt_interesting++;
	      }
	    }
	    if(cnt_interesting > 0) { //! Add to the result-matrix:
	      const t_float cnt_inv = 1/(t_float)cnt_interesting;
	      mat_dev.matrix[row_id][0] = cnt_inv * sum;
	      mat_dev.matrix[row_id][1] = score_min;
	      mat_dev.matrix[row_id][2] = score_max;
	      //!
	      //! Set a desiprive row header:
	      char str_row[1000]; memset(str_row, '\0', 1000); sprintf(str_row, "col:%u", row_id);
	      set_stringConst__s_kt_matrix(&mat_dev, row_id, str_row, /*addFor_column=*/false);
	    }
	    
	  }
	  //! Export:
	  if(self.isTo_exportResultFiles) {
	    export__singleCall__s_kt_matrix_t(&mat_dev, file_name_result, NULL);
	  }
	  //! De-allocate:
	  free__s_kt_matrix(&mat_dev);
	}
	free__s_kt_matrix(&mat_transp);
      }
    }
    //! 
    //! Step: Apply disjont clustering, for which we apply our "hp-cluster" algorithm. Export the result (of the image classificaiton) as a *.ppm file (where cluster results are integated).
    // FIXME: valdite belwo call:
    // FIXME: mae use of "mask_filter" based on an emprical ivnestgiaiton.
    const t_float mask_filter = T_FLOAT_MAX; //! which is used to omit 'background data' from the clusteirng-process
    tut_sintef_applyThresholding(mat_entr, file_base, "after-entropy", self.config_maxRadius_threshold, mask_filter, /*isTo-apply-syntethticInput-tests=*/false, /*isTo_export=*/self.isTo_exportResultFiles);    
  } else {mat_entr = mat_input;}

  //! -------------------------------------------------------------------
  //! 
  if(false) {
    assert(false); // FIXME: try differnet entropy-emtrics ...  manually deduce the bst emtric (for this use-case). 
    assert(false); // FIXME: validate the above stpes ... then make use of our already implemtantion functions.
    
    if(true) { //! apply a sytnethic classificaiotn-strategy, and demonstrate how the strategy manages to cpature core-attributes of the first input-data row:
      tut_sintef_testClassificationAccuracy_syntTrainingData(self.obj_metric_corr);
    }
    assert(false); // FIXME: validate the above stpes ... then make use of our already implemtantion functions.    
    //! -----------------------------
    //!
    if(true) { //! Correlation: apply the correlation on the input-data (rahter than the entorpy scores):
      //! Correlation: apply correlation:
      //! Step: Apply disjont clustering, for which we apply our "hp-cluster" algorithm: 
      //! Step: Export the result (of the image classificaiton) as a *.ppm file (where cluster results are integated).      
    }
    assert(false); // FIXME: validate the above stpes ... then make use of our already implemtantion functions.    
    //! Step: Correlation: apply correlation:
    //! Step: Apply disjont clustering, for which we apply our "hp-cluster" algorithm: 
    //! Step: Export the result (of the image classificaiton) as a *.ppm file.    
    assert(false); // FIXME: ... 
  }
  //! -------------------------------------------------------------------
  //!
  //! De-allocate the data:
  if( (mat_entr.matrix != mat_input.matrix) && (mat_entr.matrix != mat_blur.matrix) ) {
    free__s_kt_matrix(&mat_entr);
  }  
  if(config_apply_blur) { //! Apply a blurring-step:
    assert(mat_blur.matrix != mat_input.matrix); //! ie, as we asusme the mat_blur is allcoated.
    free__s_kt_matrix(&mat_blur);
  }
  free__s_kt_matrix(&mat_input);
  
}

#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief examplfies the building blocks used for digital forensics and medical image rechnotiatons.
   @author Ole Kristian Ekseth (oekseth, 06. jul. 2019).
   @remarks among ohters describe basic lgoics wrt. our "hp_entropy.h" API.
   @remarks compilation: g++ -I.  -g -O0  -mssse3   -L .  tut_sintef_1_helloWorld.c  -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_sint; ./tut_sint 1>out_entr.txt
**/
int main()
#else
  static void tut_sintef_1_helloWorld()   
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif   
  //!
  //! Describe data-conditions:
  const char *trainingData_desc = NULL;
  if(false) {
    trainingData_desc = "extreme";
    score_inside = 100;
    score_outside = 0;
  } else { //! then we reduce the valeu-difference, ie, which correspodns to reducing the image-contrast
    // FIXME: is our 'hard-syntetic-observations' valid for cases where input-data is less clear-cut?
    trainingData_desc = "half";
    score_inside = 100;
    score_outside = 50;
    // FIXME: next: try for a dashed line.
    // FIXME: next: 
  }
  assert(trainingData_desc != NULL); //! ie, as we exect the vlaue to be set= "extreme";

  //!
  //! ApplyLogics:
 if(false) { //! then a sytnetic data-casee is applied:
    if(false) { //! then we apply for a single combiantion:
      char file_base[1000]; memset(file_base, '\0', 1000);
      sprintf(file_base, "helloWorld_%s",  trainingData_desc);
      //const char *file_base = "helloWorld";
      // ------------------- pre: the advanced entopry-strategy is working ... and idnetifed an automated strategy for assessing quality of segmetnation. 
      // FIXME[new-synt-data]: an horitzontal line.
      // FIXME[new-synt-data]: an horitzontal line perpeciculat to a veritcal line.
      // FIXME[new-synt-data]: an angled line
      // FIXME[new-synt-data]: a cross: two angled line intersecting each other. 
      // FIXME[new-synt-data]: an arc.
      // FIXME[new-synt-data]: a sinuisoidal curve. 
      // ---
      // FIXME[new-synt-data]: extend above: blur for the cases of 'new field seperating the shapes':['region has an average color', 'region has a linear changing color']. 
      // FIXME[new-synt-data]: .... 
      // ---
      // FIXME[new-synt-data]: 
      // FIXME[new-synt-data]:      
      // -------------------
      s_sintef_1_config_crudeAlg_t self = init__s_sintef_1_config_crudeAlg_t();
      self.isTo_exportResultFiles = true; //! ie, to introspect upon the results
     _tut_sintef_apply_hardCoded_case_xmtAutoTraining(self, file_base);
    } else { //! then muliple combiantiosn are explreod:
      // ---------------
      // FIXME(next-step(before:corr-metrics --> complexImages)): effects of all entropy-metrics VS configuraiton-sapce
      // FIXME(next-step(before:corr-metrics --> complexImages)): effects of blurring VS all entropy-meitrcs.
      // FIXME(next-step(before:corr-metrics --> complexImages)): difference between entropy-cofniguraiton(sample-distance, attenuation-factor)
      // FIXME(next-step(before:corr-metrics --> complexImages)): 
      // ---------------
      // FIXME: describe a mapping between "(angle *row_id, angle * col_id)". 
      // ------------
      // FIXME: implmenet code for matrix rotation. <-- do we need to ratet for all angles?
      // FIXME: udpate hte entropy-funciton ... drop computing entorpy if the 'pair has a score marked as interesting' <-- what if a score is mkaed as both itneresting and NOT ineresting? ... do not cosntruct a sample-vector of cell "matrix[row_id][col_id]" is inside a cluster of poitns (which we know by exploring/evalauting the ...??... cells <-- by using a prior threshold-based clustering-strategy ... where the 'prior' and 'entropy' clusters are combined through ...??..)
      // FIXME(next-step(before:corr-metrics --> complexImages)): ... extend the benchmarking ... evalauting effects of 'sampling-directionality' and 'threshold-cutoff-score'.
      // --- TODO: write a new entropy-function capturing the below complexities ... ie, extending the funciton with: thresholdCuttoff, sparseARrayInsert <-- instead: possible to call the dbscan-funciton directly? <-- if so, then how we we handle hte issues of 'fixed allcoated lists'? <-- consider to instead insert the entorpy-scors into the same/current matrix .... back-rotating the matrix ... and then updating the 'boolean matrix'
      // FIXME(next-step(before:corr-metrics --> complexImages)): rotate: combine results of different data rotations <-- try construcitng an illsutration to capture this task. <-- meaningful/possible to combine these results (eg, by using a value-entropy-threshold) ... then insert reulsts directly into a sparse array 
      // FIXME(next-step(before:corr-metrics --> complexImages)): vector-sampling: [low, high, both] ... consider returning a matrix holding the max-combination. <-- alternative: insert into the 'sparse array' if entorpyScore > threshold.
      // FIXME(next-step(before:corr-metrics --> complexImages)):
      // ---
      // FIXME(next-step(before:corr-metrics --> complexImages)): 
      // FIXME(next-step(before:corr-metrics --> complexImages)):      

      //const uint cnt_metrics = (2*e_kt_entropy_genericType_undef); // FIXME: need to be adjsutd if we start supporting/epxlroing for other metirc-combinations
      for(uint entr_ind = 0; entr_ind < e_kt_entropy_genericType_undef; entr_ind++) {
	for(uint postScale_entr = 0; postScale_entr < 2; postScale_entr++) {
	  for(uint attenu_pow = 0; attenu_pow < 6; attenu_pow++) {
	    for(uint threshold_e = 2; threshold_e < 10; threshold_e++) {
	      s_sintef_1_config_crudeAlg_t self = init__s_sintef_1_config_crudeAlg_t();
	      self.obj_vectorVariance.attenuationFactor_radiusPow = attenu_pow;
	      self.obj_vectorVariance.config_maxRadius_low  = threshold_e;
	      self.obj_vectorVariance.config_maxRadius_high = threshold_e;
	      self.obj_vectorVariance.enum_id_entr = (e_kt_entropy_genericType_t)entr_ind;
	      self.obj_vectorVariance.postScaling_entr = (bool)postScale_entr;
	      // self.obj_vectorVariance.
	      char file_base[1000]; memset(file_base, '\0', 1000);
	      sprintf(file_base, "helloWorldComb_%s_%u_%u_%u_%u",  trainingData_desc, entr_ind, postScale_entr, attenu_pow, threshold_e);
	      //! 
	      //! Apply:
	      _tut_sintef_apply_hardCoded_case_xmtAutoTraining(self, file_base);
	    }
	  }
	}
      }
    }
  } else if(false) { //! then explore the effects of entropy-metrics versus extreme assumptison of configuriaon thresholds:
    //! Note: form the previsu case we observe a linear relationsip between the variables, hence the test-data indicates that it is sufficnet to evlauate extreme cases (ie, for the cases which we have evlauted in below, hence it is unkown if this assumption holds.
    for(uint entr_ind = 0; entr_ind < e_kt_entropy_genericType_undef; entr_ind++) {
      uint postScale_entr = 0;
      for(uint attenu_pow = 0; attenu_pow < 2; attenu_pow++) {
	if(attenu_pow != 0) {
	  attenu_pow = 10;
	}
	for(uint threshold_e = 0; threshold_e < 2; threshold_e++) {
	  if(threshold_e != 0) {threshold_e = 10;}
	  s_sintef_1_config_crudeAlg_t self = init__s_sintef_1_config_crudeAlg_t();
	  self.obj_vectorVariance.attenuationFactor_radiusPow = attenu_pow;
	  self.obj_vectorVariance.config_maxRadius_low  = threshold_e;
	  self.obj_vectorVariance.config_maxRadius_high = threshold_e;
	  self.obj_vectorVariance.enum_id_entr = (e_kt_entropy_genericType_t)entr_ind;
	  self.obj_vectorVariance.postScaling_entr = (bool)postScale_entr;
	  // self.obj_vectorVariance.
	  char file_base[1000]; memset(file_base, '\0', 1000);
	  sprintf(file_base, "helloWorldCombExploreOnlyEntr_%s_%u_%u_%u_%u", trainingData_desc, entr_ind, postScale_entr, attenu_pow, threshold_e);
	  //! 
	  //! Apply:
	  _tut_sintef_apply_hardCoded_case_xmtAutoTraining(self, file_base);
	}
      }
    }
  } else if(false) { //! case: helloWorldCombBlur
    for(uint entr_ind = 0; entr_ind < e_kt_entropy_genericType_undef; entr_ind++) {
      for(uint blur_ind = 0; blur_ind <= e__blur_undef; blur_ind++) {
	//!
	//! Hard-coded assumptions:
	const uint threshold_e    = 10;
	const uint attenu_pow     = 10;
	const uint postScale_entr = false;	
	//!
	//! Configure the object: 	
	s_sintef_1_config_crudeAlg_t self = init__s_sintef_1_config_crudeAlg_t();
	self.config_apply_blur_enum = (e__blur)blur_ind; // eg,: e__blur_gaussian_4_accumulator; //! as defined in "kt_aux_matrix_blur.h"
	self.obj_vectorVariance.attenuationFactor_radiusPow = attenu_pow;
	self.obj_vectorVariance.config_maxRadius_low  = threshold_e;
	self.obj_vectorVariance.config_maxRadius_high = threshold_e;
	self.obj_vectorVariance.enum_id_entr = (e_kt_entropy_genericType_t)entr_ind;
	self.obj_vectorVariance.postScaling_entr = (bool)postScale_entr;
	// self.obj_vectorVariance.
	char file_base[1000]; memset(file_base, '\0', 1000);
	sprintf(file_base, "helloWorldCombBlur_%s_%u_%u_%u_%u_%u", trainingData_desc, entr_ind, postScale_entr, attenu_pow, threshold_e, blur_ind);
	//! 
	//! Apply:
	_tut_sintef_apply_hardCoded_case_xmtAutoTraining(self, file_base);
      }
    }
  } else if(true) { //! case: helloWorldCombBlurAttenu
    for(uint entr_ind = 0; entr_ind < e_kt_entropy_genericType_undef; entr_ind++) {
      for(uint blur_ind = 0; blur_ind <= e__blur_undef; blur_ind++) {
	for(uint attenu_pow_rad = 0; attenu_pow_rad < 2; attenu_pow_rad++) {
	  if(attenu_pow_rad != 0) {
	    attenu_pow_rad = 10;
	  }
	  //!
	  //! Hard-coded assumptions:
	  const uint threshold_e    = 10;
	  const uint attenu_pow     = 10;
	  const uint postScale_entr = false;	
	  //!
	  //! Configure the object: 	
	  s_sintef_1_config_crudeAlg_t self = init__s_sintef_1_config_crudeAlg_t();
	  self.config_apply_blur_enum = (e__blur)blur_ind; // eg,: e__blur_gaussian_4_accumulator; //! as defined in "kt_aux_matrix_blur.h"
	  self.obj_vectorVariance.attenuationFactor_radiusPow = attenu_pow;
	  self.obj_vectorVariance.attenuationFactor_radiusPow_rad = attenu_pow_rad; // FIXME: try using a dfferent name for this
	  self.obj_vectorVariance.config_maxRadius_low  = threshold_e;
	  self.obj_vectorVariance.config_maxRadius_high = threshold_e;
	  self.obj_vectorVariance.enum_id_entr = (e_kt_entropy_genericType_t)entr_ind;
	  self.obj_vectorVariance.postScaling_entr = (bool)postScale_entr;
	  // self.obj_vectorVariance.
	  char file_base[1000]; memset(file_base, '\0', 1000);
	  sprintf(file_base, "helloWorldCombBlurAttenu_%s_%u_%u_%u_%u_%u", trainingData_desc, entr_ind, postScale_entr, attenu_pow, threshold_e, blur_ind);
	  //! 
	  //! Apply:
	  _tut_sintef_apply_hardCoded_case_xmtAutoTraining(self, file_base);
	}
      }
    }    
  } else  { //! then apply the same strategy (as in above), where we explroe teh effect of different combiatnions.
    //! Step(extension): rotate the image, an then ...??...
    //! Step(extension): 
    //! Step(extension): 
    assert(false);
  }
#undef MF_get_score //! ie, to be on the same side wrt. wrong usage in differnet functions.
  //!
  //! @return
#ifndef __M__calledInsideFunction
  //! *************************************************************************
  //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  //! *************************************************************************  
  return true;
#endif
}

  
   
