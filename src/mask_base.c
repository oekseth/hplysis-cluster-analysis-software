#include "mask_base.h"

//! @return true if the same 'inner index' is alwasy of interest for the different row-combinations.
const bool ktMask__vectorsAre_consistentlyInteresting(const uint nrows, const uint ncols, t_float **data1_input, t_float **data2_input, char **mask1, char **mask2, const char transpose) {
  //! then we need to compare 
/* #ifndef __localConfig_use_slow */
/*   bool mayUse_optimized_computaiton = true; //! ie, default assumption */
/* #else */
  bool mayUse_optimized_computaiton = false;
/* #endif */
  // FIXME[article] update our artilce-text wrt. [below] 'heuristics'.
  if(transpose == 0) {
    if(mask1 && mask2) {
      for(uint index1 = 0; index1 < nrows; index1++) {
	if(mayUse_optimized_computaiton) { //! then we contineu.
	  for(uint i = 0; i < ncols; i++) {
	    //const bool isOf_interest = mask1[index1][i];
	    
	    // FIXME: optimize [”eloww] wrt. _mm_load1_ps(..)
	    for(uint index2 = 0; index2 < nrows; index2++) {
	      if(mayUse_optimized_computaiton) { //! then we contineu.
		if(!mask1[index1][i] && mask2[index2][i]) {mayUse_optimized_computaiton = false; }
		else if(mask1[index1][i] && !mask2[index2][i]) {mayUse_optimized_computaiton = false; }
	      }
	    }
	  }
	}
      }
    } else {
      for(uint index1 = 0; index1 < nrows; index1++) {
	if(mayUse_optimized_computaiton) { //! then we contineu.
	  for(uint i = 0; i < ncols; i++) {
	    //const bool isOf_interest = mask1[index1][i];
	    for(uint index2 = 0; index2 < nrows; index2++) {
	      if(mayUse_optimized_computaiton) { //! then we contineu.
		if(!isOf_interest(data1_input[index1][i]) && isOf_interest(data2_input[index2][i])) {mayUse_optimized_computaiton = false; }
		else if(isOf_interest(data1_input[index1][i]) && !isOf_interest(data2_input[index2][i])) {mayUse_optimized_computaiton = false; }
	      }
	    }
	  }
	}
      }
    }
  } else { //! then the transpsoe is evaluated:
    if(mask1 && mask2) {
      for(uint index1 = 0; index1 < ncols; index1++) {
	if(mayUse_optimized_computaiton) { //! then we contineu.
	  for(uint i = 0; i < nrows; i++) {
	    //const bool isOf_interest = mask1[index1][i];
	    for(uint index2 = 0; index2 < ncols; index2++) {
	      if(mayUse_optimized_computaiton) { //! then we contineu.
		if(!mask1[i][index1] && mask2[i][index2]) {mayUse_optimized_computaiton = false; }
		else if(mask1[i][index1] && !mask2[i][index2]) {mayUse_optimized_computaiton = false; }
	      }
	    }
	  }
	}
      }
    } else {
      for(uint index1 = 0; index1 < ncols; index1++) {
	if(mayUse_optimized_computaiton) { //! then we contineu.
	  for(uint i = 0; i < nrows; i++) {
	    //const bool isOf_interest = mask1[index1][i];
	    for(uint index2 = 0; index2 < ncols; index2++) {
	      if(mayUse_optimized_computaiton) { //! then we contineu.
		if(!isOf_interest(data1_input[i][index1]) && isOf_interest(data2_input[i][index2])) {mayUse_optimized_computaiton = false; }
		else if(isOf_interest(data1_input[i][index1]) && !isOf_interest(data2_input[i][index2])) {mayUse_optimized_computaiton = false; }
	      }
	    }
	  }
	}
      }
    }
  }  
  return mayUse_optimized_computaiton;
}

