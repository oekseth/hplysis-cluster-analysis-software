  if(self) {
    for(int i = 0; i <= self->n; i++) {
      if(self->pointers[i]) {
	//!  Resursive call:
	__MiF__free(self->pointers[i]);
	//! De-allocate:
	free((self->pointers[i]));
	self->pointers[i] = NULL;
      }
    }
    self->n = 0; //! ie, 'set to empty'.
  }

