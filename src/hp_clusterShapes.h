#ifndef hp_clusterShapes_h
#define hp_clusterShapes_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_clusterShapes
   @incategory hpLysis__aux
   @brief provide logics to consutrct differnet clsuter-shapes (oekseth, 06. jan. 2017). 
   @author Ole Kristian Ekseth (oekseth, 06. jan. 2017). 
   @remarks the "hp_" prefix (to this file and assicated strucutres) is used to denote the 'non-assication' wrt. Knitting-Tools knitting-appraoch, ie, where "hp_clusterShapes" describes/reprsents a 'super-structure' wrt. clustering.
   @remarks the clsuter-shapes are (among ohtes) used in evlauation of Cluster-Comparison-Metrics (CCMs), eg, wrt "Silhouette", "Rand" and "Dunn.
   @remarks 'provides' shapes of data-distribtuions, eg, used to validate results produced by "kt_matrix_cmpCluster.c", eg, wrt. Dunn and Rand.
 **/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "kt_matrix.h"

/**
   @enum e_hp_clusterShapes_vertexOrder
   @brief describes different vertex-orders which may be used to increase CCM-evlauation-compelxity wqrt. the clsuter-assignments (oekseth, 06. jan. 2017)
   @remarks is the type of vertex-order to apply/use when 'identifying a vertex': used to consturct a mapping-table between an 'itnernal vertex' and a 'globally unique vertex'.
   @remarks use-case: of interst is to invesrtigate the effects of 'keeping' the cluster-sizes 'the same' while permutating/changing 'the vertex-memberships': to assess/test the design of CCMs by comparing the prediction-resutls of using different "e_hp_clusterShapes_vertexOrder_t" vertex-assignments-order. To udnerstand the importanc eof the latter ntoe that most CCMs focus on the simliarity between vertices in different groups (rather than the number and size of clusters). Therefore if two cluster-enumation-sets have the same 'clsuter-topology' though different 'order of the vertices in the clusters' we expect the CCMs to 'yeald' a poorer CCM score (and assicated rank when copared to different appraoches), ie, hence the interest in using our "e_hp_clusterShapes_vertexOrder_t".
 **/
typedef enum e_hp_clusterShapes_vertexOrder {
  e_hp_clusterShapes_vertexOrder_linear,
  e_hp_clusterShapes_vertexOrder_reverse,
  e_hp_clusterShapes_vertexOrder_random,
  e_hp_clusterShapes_vertexOrder_undef
} e_hp_clusterShapes_vertexOrder_t;

/**
   @enum e_hp_clusterShapes_scoreAssignment
   @brief specifies how we are to assign scores to a new-cosntructed similarity-matrix (oekseth, 06. jan. 2017)
   @remarks defines the difference between vertices inside a partciular clsuter VS vetrtices otuside a particular cluster. Describes the seperation which we use to 'provide' a seperation between 'vertices inside clusters' and 'vertex which are outside a partiuclar cluster'. Will have a 'critical' importance on CCMs which use a simlairty-amtrix in their evlauation (eg, wrt. Silhouette and Dunn, while will not aaffect the 'defualt implemetatniobn' of Rands index).
 **/
typedef enum e_hp_clusterShapes_scoreAssignment {
  e_hp_clusterShapes_scoreAssignment_sameScore,
  e_hp_clusterShapes_scoreAssignment_minInside_maxOutside, //! within-scores, and between-sccores, are fixed.
  e_hp_clusterShapes_scoreAssignment_minInside_maxOutside__decreasedScoreForSmallerClusters, //! decreased score for smaller clusters: $strong + (weak - strong)*(1 + strong) / rows$.
  //e_hp_clusterShapes_scoreAssignment_,
  e_hp_clusterShapes_scoreAssignment_undef,
} e_hp_clusterShapes_scoreAssignment_t;


/**
   @struct s_hp_clusterShapes
   @brief a genralized configuraiton of the "s_hp_clusterShapes_" object (oekseth, 06. jan. 2017).
   @remarks used to simplify 'future' 'compelxity-increase' wrt. buildng of gernalized clsuter-configurations.
 **/
typedef struct s_hp_clusterShapes {
  t_float score_weak;
  t_float score_strong;
  //! ----
  s_kt_matrix_t matrix; //! which hold the distance-matrix.
  uint *map_clusterMembers; //! which hold the cluster-members for the vertices: hold the cluster-assignmetns [vertex-id][cluster-id] for the vertices.
  uint map_clusterMembers_size; //! expected to equal "matrix.nrows"  
  //! ----
  uint globaRow_id; //! ie, to 'facilate' the construction of muliple cluster-sets 'in the same chunk'
  e_hp_clusterShapes_scoreAssignment_t typeOf_scoreAssignment;
  uint *map_vertexInternalToRealWorld; //! which is used to 'map' an internal vertex-id to a 'glboal vertex-id': for details see our "e_hp_clusterShapes_vertexOrder_t"
} s_hp_clusterShapes_t;

//! @reutrn a 'shallwo' matrix in the format of "s_kt_matrix_base_t" (oekseth, 06. okt. 2017).
static s_kt_matrix_base_t getShallow_matrix_base__s_hp_clusterShapes_t(const s_hp_clusterShapes_t *self) {
  assert(self);
  return get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(&(self->matrix));
}


//! Initates the "s_hp_clusterShapes_t" object and return (oekseth, 06. jan. 2017).
const s_hp_clusterShapes_t setToEmptyAndReturn__s_hp_clusterShapes_t();

/**
   @brief Initates the "s_hp_clusterShapes_t" object and return (oekseth, 06. jan. 2017).
   @param <total_nrows> is the maximum number of vertices (ie, rows and columns) to be inserted: constructs an adjcency-matrix wrt.t he latter accessed by "self->matrix".
   @param <score_weak> is the score to be asisnged to no-members
   @param <score_strong> is the score to be asisgned to members
   @param <typeOf_vertexOrder> is the type of vertex-order to apply/use when 'identifying a vertex': used to consturct a mapping-table between an 'itnernal vertex' and a 'globally unique vertex'.
   @param <typeOf_scoreAssignment> describes the seperation which we use to 'provide' a seperation between 'vertices inside clusters' and 'vertex which are outside a partiuclar cluster'. Will have a 'critical' importance on CCMs which use a simlairty-amtrix in their evlauation (eg, wrt. Silhouette and Dunn, while will not aaffect the 'defualt implemetatniobn' of Rands index).
   @return a new-itnaited object.
**/
const s_hp_clusterShapes_t init_andReturn__s_hp_clusterShapes_t(const uint total_nrows, const t_float score_weak, const t_float score_strong, const e_hp_clusterShapes_vertexOrder_t typeOf_vertexOrder, const e_hp_clusterShapes_scoreAssignment_t typeOf_scoreAssignment);

//! De-allocates the "s_hp_clusterShapes_t" object.
void free__s_hp_clusterShapes_t(s_hp_clusterShapes_t *self);

//! @return true if the object is empty (oekseth, 06.09.2020).
bool isEmpty__s_hp_clusterShapes_t(const s_hp_clusterShapes_t *self);

/**
   @brief build a sample-matrix with 'a linear increased' seperation between each cluster (oekseth, 06. jan. 2017).
   @param <self> is the object which hold the overall settings (And result-object to store our 'updates' in).
   @param <nrows> is the [n, n] matrix-diemsinos to use: should be less than "total_nrows" intiated in the "init_andReturn__s_hp_clusterShapes_t(..)" funciton.
   @param <cnt_clusters> is the number of clsuters to evlauate/cmoptue for.
   @param <cnt_clusterStartOffset> is the start-postion wrt. the cluster-count: if "0" is used then we 'overlap' witht hte previous insertion (ie, if any previosu calls for the same s_hp_clusterShapes_t object). Simliary for "cnt_clusterStartOffset = cnt_clusters" (and where 'this call in quesiton' is then a 'second call to this function') then the 'insertiosn' are non-overlapping/disjoint from  the earlier cluster-memberships.
   @param <vertexCount__constantToAdd> is the "b" attribute in each "y = ax + b", where "y = nrows_local", "a" is 'dynamcailly inferred in this funciton' and 'x' is the cluster-id 'defined' in the for-loop of [0, cnt_clusters].
   @return true upun success
   @remarks is used to investigate 'extemly obvious' cases wrt. clustering: to describe the relatedness between differences in cluster-sizes VS 'equality' of CCMs. An example of the latter conserns the "Silhouette" CCM which 'mkaes' use of min-cluster-distnace and max-cluster-distance to describe simliarites between two cluster-ensembles. Therefore we would expect that the Silhouette index will be strongly 'skewed' if the distance-matrix does Not reflect the 'skweness' wrt. cluster-membership-shapes'.
**/
bool buildSampleSet__linearSquareIncrease__hp_clusterShapes(s_hp_clusterShapes_t *self, const uint nrows, const uint cnt_clusters, const uint cnt_clusterStartOffset, const t_float vertexCount__constantToAdd);

/**
   @brief build a sample-matrix with 'defined' seperation between each cluster (oekseth, 06. nov. 2016).
   @param <self> is the object which hold the overall settings (And result-object to store our 'updates' in).
   @param <nrows> is the [n, n] matrix-diemsinos to use: should be less than "total_nrows" intiated in the "init_andReturn__s_hp_clusterShapes_t(..)" funciton.
   @param <cnt_clusters> is the number of clsuters to evlauate/cmoptue for.
   @param <cnt_clusterStartOffset> is the start-postion wrt. the cluster-count: if "0" is used then we 'overlap' witht hte previous insertion (ie, if any previosu calls for the same s_hp_clusterShapes_t object). Simliary for "cnt_clusterStartOffset = cnt_clusters" (and where 'this call in quesiton' is then a 'second call to this function') then the 'insertiosn' are non-overlapping/disjoint from  the earlier cluster-memberships.
   @return true upun success
   @remarks 
   - is used to investigate 'extemly obvious' cases wrt. clustering: to redcue the 'oviousness' choose "score_weak =~score_strong"
   - the self->map_clusterMembers is used to 'speicfy the dieal case wrt. tlcuster-asisgments', ie, where "score_weak =~ score_strong" would indiate that there is 'no clear-case wrt. clsuter-assignmetns.
   @remarks Use-cases: facilaites/support different use-cases where the cluster-sizes are 'fixed'.
   "cnt_clusters == nrows": then each vertex is found in seperate clusters.
   "cnt_clusters == nrows/5": then five vertices are found in each of the clusters.
 **/
bool buildSampleSet__disjointSquares__hp_clusterShapes(s_hp_clusterShapes_t *self, const uint nrows, const uint cnt_clusters, const uint cnt_clusterStartOffset);

/**
   @enum e_hp_clusterShapes_clusterAssignType
   @brief specifies the type of cluster-shape to 'build for' in a complex/integral cluster-construciton of heteroenous cluster-sets (oekseth, 06. jan. 2017). 
 **/
typedef enum e_hp_clusterShapes_clusterAssignType {
  e_hp_clusterShapes_clusterAssignType_disjointSquares,
  e_hp_clusterShapes_clusterAssignType_linearIncrease,
  //e_hp_clusterShapes_clusterAssignType_
  e_hp_clusterShapes_clusterAssignType_undef
} e_hp_clusterShapes_clusterAssignType_t;

/**
   @brief update the "s_hp_clusterShapes_t" object with muliple calls to a specific 'case-construciton-function' (oekseth, 06. jan. 2017). 
   @param <self> is the object which hold the overall settings (And result-object to store our 'updates' in).
   @param <nrows> is the [n, n] matrix-diemsinos to use: should be less than "total_nrows" intiated in the "init_andReturn__s_hp_clusterShapes_t(..)" funciton.
   @param <cnt_clusters> is the number of clsuters to evlauate/cmoptue for. Comptued using the equation: "cnt_clusters_fixedToAdd + (t_float)(1.0 + (t_float)call_id) * (t_float)cnt_clusters * linearIncrease_inEachCall__cntClusters". 
   @param <cnt_clusters_fixedToAdd> the 'fixed' cluster-number to 'always add' to equations, ie, the "b" in "y = ax + b". 
   @param <enum_id> is the type of cluster-cases to build for.
   @param <cnt_seperateCalls> is the number of calls to be made to "enum_id"
   @param <isTo_startOnZero> which for muliple calls implies that we 'use biggestInsertedClusterId + 1' as a cluster-start-positio.
   @param <isTo_incrementDisjoint> which if set to true ensures an 'non-overlapping proeprty' for the inserted clsuters (ie, where the latter viable is 'updated' during our comutpaiton).
   @param <linearIncrease_inEachCall__cntClusters> is the factor to increase the "cnt_clusters" with: computed using the formula "(1 + call_id)*cnt_clusters*linearIncrease_inEachCall__cntClusters".
   @return  true upon success
   @remarks use-cases (specific):
   -- "cnt-clusters = 2, isTo-startOnZero = true, isTo-incrementDisjoint = true, enum-id = disjoint-squares, cnt-seperateCalls = 3, linear-increase = 0": a linear increase in the vertex-size, ie, for which the 'number of vertices in each cluster', will increase linearly;
   -- "cnt-clusters = 1, isTo-startOnZero = true, isTo-incrementDisjoint = true, enum-id = disjoint-squares, cnt-seperateCalls = 3, linear-increase = 2": from the latter we 'get' a cluster-configuration where we observe a 'linar increase' in both the cluster-sizes and vertex-size;
   -- "isTo-startOnZero = true, isTo-incrementDisjoint = false,  enum-id = disjoint-squares, cnt-seperateCalls = 3, linear-increase = 0": produce 'overlaps' wrt. the cluster-memberships: in each 'iteration' extend/expand the previous cluster-memberships with a new 'collection'/slot of vertices. 
   -- 
   -- 
   @remarks in this fucniton we make use of the proeprty "[1, 2, 3, .... n] = (n*(n+1))/2", an equaiton defined in Rottman at page 111 (oekseht, 06. jan. 2016).
   @remarks in order to facilaite the 'adjustmen' of the clsuter-size' we use two varialbes, ie, both "cnt_seperateCalls" and "linearIncrease_inEachCall", e, wrt. the use-case  where " nrows[1] > nrows[0] &&  cntClusters[1] > cntClusters[0]". For this 'task' the "linearIncrease_inEachCall__cntClusters" parameter is used, eg, wrt. using a parameter-value " < 1" or " > 1".
   @remaks the idea behidn this function is to describe "muliple cluster-distributions" through the use of a 'fixed clsuter-allcoation-procedre' in cobmaitnion with a 'linear increase in clsuter-vertex-density'. 
 **/
bool buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(s_hp_clusterShapes_t *self, const uint nrows, const uint cnt_clusters, const uint cnt_clusters_fixedToAdd, const e_hp_clusterShapes_clusterAssignType_t enum_id, const uint cnt_seperateCalls,  const bool isTo_startOnZero, const bool isTo_incrementDisjoint, const t_float linearIncrease_inEachCall__cntClusters);







#endif //! EOF
