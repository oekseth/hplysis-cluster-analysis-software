{

  if( (data1_input == data2_input) && (index1 == index2) ) {
    fprintf(stderr, "!!\t A pointless call: the indexes=%u are the same, ie, pelase investgiate correctnes of your call: for quesitons please contact the developer at [oekseth@gmail.com]. Observation made at [%s]:%s:%d\n", index1, __FUNCTION__, __FILE__, __LINE__);
    return T_FLOAT_MAX;
  } //! else the call 'may have a point' if the data-two data-matrices are different (oekseth, 06. des. 2016).

  bool needTo_useMask_evaluation = (self.masksAre_used == e_cmp_masksAre_used_false); //! ie, investigate if the caller has declared that 'all valeus are of itnerest', eg, for the case where the "euclidc distance metric" is used in the k-means procedure, ie, where a acell-dsitance=0 is 'enough' to cpature/descirbe a non-set cell (ie, as the latter value will then be ingored form the comptaution).
  if(self.masksAre_used == e_cmp_masksAre_used_undef) {
    assert(data1_input);
    assert(data2_input);
    needTo_useMask_evaluation = matrixMakesUSeOf_mask(nrows, ncols, data1_input, data2_input, self.mask1, self.mask2, /*transpose=*/false, self.iterationIndex_2);
    self.masksAre_used = (needTo_useMask_evaluation) ? e_cmp_masksAre_used_true : e_cmp_masksAre_used_false;
  } 
  //! -----------------------------
  t_float ret_val = T_FLOAT_MAX;

  s_correlation_rank_rowPair_t obj_correlation; s_correlation_rank_rowPair_setTo_empty(&obj_correlation);
  
  uint ncols_adjusted = ncols;
  if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) {
  //if(isTo_useSpearman_rankPreComputation) {
    if(self.transposed == 0) {
      //! Comptue the ranks for thw two rows:
      s_correlation_rank_rowPair_allocate(&obj_correlation, ncols);
      s_correlation_rank_rowPair_compute_nonTransposed(&obj_correlation, index1, index2, ncols, data1_input, data2_input, self.mask1, self.mask2, /*performance-appraoch=*/e_distance_rank_typeOf_computation_ideal, /*needTo_useMask_evaluation=*/needTo_useMask_evaluation);
      
      /* //! Compute the ranks based on the correlations: */
      /* const t_float result_value = kt_spearman_forPreComputedRanks(self, obj_correlation); */      
      //! Udpate length:
      ncols_adjusted = obj_correlation.cnt_setValues_inRanks;
      assert(ncols_adjusted <= ncols);
    } else {
      //! Comptue the ranks for thw two rows:
      s_correlation_rank_rowPair_allocate(&obj_correlation, nrows);
      s_correlation_rank_rowPair_compute_transposed(&obj_correlation, index1, index2, nrows, data1_input, data2_input, self.mask1, self.mask2, /*performance-appraoch=*/e_distance_rank_typeOf_computation_ideal, /*needTo_useMask_evaluation=*/needTo_useMask_evaluation);
      //! Udpate length:
      ncols_adjusted = obj_correlation.cnt_setValues_inRanks;
      assert(ncols_adjusted <= nrows);
    }
  }


  // printf("use-Kendall-metric=<%u> given metric=%u, at %s:%d\n", isTo_use_kendall__e_kt_correlationFunction(metric_id), metric_id, __FILE__, __LINE__);
  
  if(isTo_use_kendall__e_kt_correlationFunction(metric_id)) {
    // assert(isTo_use_kendall__e_kt_correlationFunction == false);
    /* if(isTo_use_kendall__e_kt_correlationFunction == true) { */
    /*   ret_val = kendall_improvment_kingsAlgorithm(ncols_adjusted, /\*vector1_sorted=*\/obj_correlation.arrOf_valuesSorted_1, self.matrixSorted_column[index1], vector_2, mask2_vector, self.arr1_tmp, self.arr2_tmp,  /\*list_column_index=*\/self.matrixSorted_column_index[index1],  metric_id, /\*needTo_useMask_evaluation=*\/true); //, needTo_useMask_evaluation); */
    /*   assert(false); // FXIME: inlcude a permtaution of [”elow]: */
    /*   // compute_results_applyObjectToExternalRow__s_correlationType_kendall_partialPreCompute_kendall(struct s_correlationType_kendall_partialPreCompute_kendall *self, const uint index1, t_float *vector_2, char *mask2_vector, const e_kt_correlationFunction_t metric_id, const bool needTo_useMask_evaluation); */
    /*   //assert(isTo_useSpearman_rankPreComputation == false); //! ie, as we */
    /* } else { */
    if(optionalStore_kendall != NULL) {

      // FIXME[performance]: write a performance-test wrt. [”elow] ... testing signficance of 'naive impelemtatnion' VS [”elow]
      
      ret_val = compute_results_applyObjectToExternalRow__s_correlationType_kendall_partialPreCompute_kendall(optionalStore_kendall, index1, data2_input[index2], (self.mask2 != NULL) ? self.mask2[index2] : NULL, metric_id, needTo_useMask_evaluation);      
      // printf("Kendall-retVal=%f, at %s:%d\n", ret_val, __FILE__, __LINE__);
      /* ret_val = compute_results_applyObjectToExternalRow__s_correlationType_kendall_partialPreCompute_kendall(optionalStore_kendall, index1, index2, data1_input[index1],  */
      /* 													      /\*vector_2=*\/data2_input[index2],  */
													      
      /* 													      mask2_vector,  metric_id, needTo_useMask_evaluation); */
    } else {
      ret_val = kt_kendall(ncols, data1_input, data2_input, self.mask1, self.mask2, self.weight, index1, index2, self.transposed, metric_id, self); //ktCorr_compute_allAgainstAll_distanceMetric_kendall(nrows, ncols, data1, data2, self.mask1, self.mask2, self.weight, resultMatrix, self.transposed, metric_id, &self);
      // printf("Kendall-retVal=%f, at %s:%d\n", ret_val, __FILE__, __LINE__);
    }
      //}
  } else {
    assert(optionalStore_kendall == NULL); // ie, as we then expect 'this' nto to be set.   
    t_float *row_1 = data1_input[index1]; t_float *row_2 = data2_input[index2];

    bool memIsAllocated = false;
    if( 
       (typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_none)
       //(isTo_useSpearman_rankPreComputation == false) 
       && (self.transposed == 1) ) {
      assert(row_1 != row_2);
      row_1 = get_transposed_row_fromMatrixFloat__matrix_transpose(index1, nrows, ncols, data1_input);
      row_2 = get_transposed_row_fromMatrixFloat__matrix_transpose(index2, nrows, ncols, data2_input);
      //! ---
      ncols_adjusted = nrows; //! ie, as we then expect we are to iterate through the 'vertical' axis.
      memIsAllocated = true;
    } else if  (typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) 
      //(isTo_useSpearman_rankPreComputation) 
      {
	row_1 = obj_correlation.arrOf_ranks_index1;    row_2 = obj_correlation.arrOf_ranks_index2;      
      } /* else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) { */
    /*   assert(false); // fIXME: add support for this */
    /* } */
    if (typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) {
      assert(row_1);       assert(row_2);
      construct_binaryRow__updateDirectly__mask_api(row_1, ncols_adjusted);
      construct_binaryRow__updateDirectly__mask_api(row_2, ncols_adjusted);
    }    

    if(isTo_use_MINE__e_kt_correlationFunction(metric_id) == false) {
      if(isTo_use_directScore__e_kt_correlationFunction(metric_id)) {
	//! Then we call the function found in our "kt_distance__groupOf_direct.c" (oekseth, 06. des. 2016).
	if(nrows == ncols) {
	  bool isTo_evaluate = true;
	  if(self.mask1 || self.mask1) {
	    if(self.mask1 && (
			      // FXIME: validate correctness wrt. [”elow] ... ie, cosndier 'omtititng this'.
			      !self.mask1[index1][0] 
			      //|| !self.mask1[index2][index1]
			      /* !self.mask1[index1][index2]  */
			      /* || !self.mask1[index2][index1] */
			      ) ) {isTo_evaluate = false;}
	    if(self.mask2 && (
			      // FXIME: validate correctness wrt. [”elow] ... ie, cosndier 'omtititng this'.
			      !self.mask2[index2][0] 
			      //!self.mask2[index1][index2] 
			      //|| !self.mask2[index2][index1]
			      ) ) {isTo_evaluate = false;}
	  } 
	  if(isTo_evaluate) {
	    ret_val = corr__computeDirectScore__each(metric_id, /*index1=*/0, /*index2=*/0, &row_1, &row_2, 
						     NULL, //(self.mask1) ? self.mask1[index1] : NULL,
						     NULL, // (self.mask2) ? self.mask2[index2] : NULL,
						     /*weight=*/NULL,
						     ///*weight=*/(self-weight && (nrows == ncols)) ? &(self.weight[index2]), 
						     /*masksAre_used=*/(!self.mask1 || !self.mask2) ? self.masksAre_used : e_cmp_masksAre_used_false);
	  } else {ret_val = T_FLOAT_MAX;} //! ie, as we then assume that the 'direct pair' is Not of itnerest.
	} else {
	  fprintf(stderr, "!!\t Seems like your inptu-data in diemsnions [%u, %u] are Not symmetric, ie, pelase validate correctness of your topology-speciciaiotn. For questions please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", nrows, ncols, __FUNCTION__, __FILE__, __LINE__);
	  ret_val = T_FLOAT_MAX;
	}
      } else {
	if(metric_each_nonRank == NULL) {
	  metric_each_nonRank = setmetric__correlationComparison__each(metric_id, self.weight, self.mask1, self.mask2, needTo_useMask_evaluation);
	}
	
	
	
	config_nonRank_each_t config_metric; init__config_nonRank_each(&config_metric, ncols_adjusted, row_1, row_2, self.weight, 
								       (self.mask1) ? self.mask1[index1] : NULL,
								       (self.mask2) ? self.mask2[index2] : NULL,
								       self);
	
	//fprintf(stderr, "\n\n(info)\t masksAre_used='%u', at %s:%d\n", self.masksAre_used, __FILE__, __LINE__);
	//!
	//! The call:
	// printf("(info)\t (start)\tCompute non-ranked correlation-matrix, where default power-value=%f, at %s:%d\n", self.configMetric_power, __FILE__, __LINE__);    
	//printf("FIXME: include [”elow], at %s:%d\n", __FILE__, __LINE__);
	ret_val = metric_each_nonRank(config_metric);
      }
    } else { //! then we add supprot for MINE correlaiton-meitrc:
      ret_val = __compute_each__mine(ncols_adjusted, row_1, row_2, metric_id);
    }
      // printf("(info)\t (completed)\tCompute non-ranked correlation-matrix, where default power-value=%f, at %s:%d\n", self.configMetric_power, __FILE__, __LINE__);    


    if(memIsAllocated) {
      free_1d_list_float(&row_1); row_1 = NULL;
      free_1d_list_float(&row_2); row_2 = NULL;
    }
    /* if(  */
    /*    (typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_rank) */
    /*    //(isTo_useSpearman_rankPreComputation == false)  */
    /* 	&& (self.transposed == 1) ) { */
    /*   const bool deAllocateBoth = (row_1 != row_2);       */
    /*   free_1d_list_float(&row_1); */
    /*   if(deAllocateBoth) { free_1d_list_float(&row_2); } */
    /*   //printf("de-aloloated local data, at %s:%d\n", __FILE__, __LINE__); */
    /* } else if(typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) { */
    /*   assert(false); // fIXME: add support for this */
    /* } */
  }  

  //! -----------------------------------------
  //!
  //! Finalize:
  if(self.s_inlinePostProcess != NULL) {
    fprintf(stderr, "!!\t Add support for this option, ie, please forward a request to the developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! ie, an heads-up in debug-mode.
  }

  //printf("FIXME: include [”elow], at %s:%d\n", __FILE__, __LINE__);
  // assert(ret_val != T_FLOAT_MAX);
  //! De-alcoate:
  s_correlation_rank_rowPair_free(&obj_correlation);   

  // printf("returns, at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  if(ret_val == T_FLOAT_MIN) {ret_val = T_FLOAT_MAX;}
  return ret_val;
}
