#ifndef kt_list_2d_h
#define kt_list_2d_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_list_2d
   @brief provide a generic list-itnerface to 2d-lists (oekseth, 06. mar. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. aug. 2017).
   @remarks used to simplify API and access in/through different programming-languates
**/

#include "kt_list_1d.h"
#include "kt_list_1d_string.h"
/**
   @enum e_kt_histogram_scoreType
   @brief identifes different types of striates to hold the scores for each histogram vector (oekseth, 06. feb. 2018).
 **/
typedef enum e_kt_histogram_scoreType {
  e_kt_histogram_scoreType_count,
  e_kt_histogram_scoreType_count_averageOnBins,
  e_kt_histogram_scoreType_sum,
  e_kt_histogram_scoreType_undef
} e_kt_histogram_scoreType_t;

//! @return a short (ie, huamnised) stringr-repserntiaoin of the enum
static const char *getStr_e_kt_histogram_scoreType_t(const e_kt_histogram_scoreType_t e) {
  if(e == e_kt_histogram_scoreType_count) {return "count";}
  else if(e == e_kt_histogram_scoreType_count_averageOnBins) {return "countAvgOnBins";}
  else if(e == e_kt_histogram_scoreType_sum) {return "sum";}
  else return NULL;
}

/**
   @struct s_kt_list_2d_uint
   @brief provide a wrapper to accesss a list-strucutre of "uint" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_2d_uint {
  s_kt_list_1d_uint_t *list; uint list_size; uint current_pos;
} s_kt_list_2d_uint_t;
//! @return an itnailted list-object with size "list_size"
s_kt_list_2d_uint_t init__s_kt_list_2d_uint_t(uint cnt_rows, uint list_size);
//! @return an 'empty verison' of our s_kt_list_2d_uint_t object
s_kt_list_2d_uint_t setToEmpty__s_kt_list_2d_uint_t();
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
#ifndef SWIG
s_kt_list_2d_uint_t initFrom_sparseMatrix__s_kt_list_2d_uint_t(const uint row_id, const uint nrows, const uint ncols, t_float **matrix) ;
#endif // #ifndef SWIG
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
//s_kt_list_2d_uint_t initFromFile__s_kt_list_2d_uint_t(const uint row_id, const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, uint index, uint *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, uint index, uint valueTo_set);
//! De-allcoates the s_kt_list_2d_uint_t object.
void free__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self);
//! Push a list-item to the set. (oekseth, 06. jul. 2017).
void push__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, const uint valueTo_set);
#ifndef SWIG
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, const uint valueTo_set);
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id);
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id);
#endif // #ifndef SWIG
//! ***************************************************************************
//! ***************************************************************************

#ifndef SWIG
//! Sort the data-set (oekseth, 06. jul. 2017).
void sort__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id);
#endif // #ifndef SWIG
//! Udpat ethe object-lsit size (oesketh, 06. aug. 2017).
//! @remarks is used to enable a 'comrepssion' of meory-usage, ie, to reduce rednant emmroys-aprce when memory-overla is of interest, eg, used as a psot-ranknig-stpe
void setTo_size__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint list_size);


// ***************************************************************************
// ***********************'***************************************************

/**
   @struct s_kt_list_2d_kvPair
   @brief provide a wrapper to accesss a list-strucutre of "s_ktType_kvPair_t" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_2d_kvPair {
  s_kt_list_1d_kvPair_t *list; uint list_size; uint current_pos;
} s_kt_list_2d_kvPair_t;
//! @return an itnailted list-object with size "list_size"
s_kt_list_2d_kvPair_t init__s_kt_list_2d_kvPair_t(uint cnt_rows, uint list_size);
//! @return an 'empty verison' of our s_kt_list_2d_kvPair_t object
s_kt_list_2d_kvPair_t setToEmpty__s_kt_list_2d_kvPair_t();
//! Set minimum size of the object (oekseth, 0+6. otk. 2017)
void setMinSize__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint min_size);
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
#ifndef SWIG
s_kt_list_2d_kvPair_t initFrom_sparseMatrix__s_kt_list_2d_kvPair_t(const uint row_id, const uint nrows, const uint ncols, t_float **matrix) ;
#endif // #ifndef SWIG
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
//s_kt_list_2d_kvPair_t initFromFile__s_kt_list_2d_kvPair_t(const uint row_id, const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id, uint index, s_ktType_kvPair_t *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id, uint index, s_ktType_kvPair_t valueTo_set);
//! De-allcoates the s_kt_list_2d_kvPair_t object.
void free__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self);
//! Push a list-item to the set. (oekseth, 06. jul. 2017).
void push__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id, const s_ktType_kvPair_t valueTo_set);
#ifndef SWIG
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id, const s_ktType_kvPair_t valueTo_set);
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id);
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id);
#endif // #ifndef SWIG
//! ***************************************************************************
//! ***************************************************************************

//! Sort the data-set (oekseth, 06. jul. 2017).
void sort__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id);
//! Udpat ethe object-lsit size (oesketh, 06. aug. 2017).
//! @remarks is used to enable a 'comrepssion' of meory-usage, ie, to reduce rednant emmroys-aprce when memory-overla is of interest, eg, used as a psot-ranknig-stpe
void setTo_size__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint list_size);
/**
   @brief comptue entropy for all float-scores in a sparse list of values (oekseth, 06. sept. 2017). 
   @param <self> is the sparse lsit of scores to copute enotpry for. 
   @return the cosntruct list-object.
 **/
s_kt_list_1d_float_t translate_toFloats__s_kt_list_2d_kvPair_t(const s_kt_list_2d_kvPair_t *self);
/**
   @brief cosntruct a histogram.
   @param <self> is the object to first wrap into a 1d-lsit and thereafer gneerate the histogram for.   
   @param <cnt_bins> is the number of 'buckets' to be sued wrt. the histogram-cosntruciton
   @param <isTo_forMinMax_useGlobal> which if set to false impleis taht the 'buckets' are ajduted locally for each row.
   @param <enum_id_scoring> the type of scores to be stored in the histogram.
   @return the cosntruct list-object.
 **/
s_kt_list_1d_float_t translate_histogram__s_kt_list_2d_kvPair_t(const s_kt_list_2d_kvPair_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring);

/**
   @brief cosntruct a histogram and exprot the latter into a TSV-format
   @param <self> is the object to first wrap into a 1d-lsit and thereafer gneerate the histogram for.   
   @param <fileP_result> is the file-desiptor to be sued when writing out the result
   @param <strHead> is the ehad-string which is to be used, ie, the row-idnierfer.
   @param <isTo_generateHead> which is to be set to false if a row-header is Not to be prtined (piror to the gneerated hsitogram)
   @param <cnt_bins> is the number of 'buckets' to be sued wrt. the histogram-cosntruciton
 **/
void writeOut_histoGram1d_TSV__s_kt_list_2d_kvPair_t(const s_kt_list_2d_kvPair_t *self, FILE *fileP_result, const char *strHead, const bool isTo_generateHead, const uint cnt_bins);
/**
   @brief insert a 'tiled' block of vertex-row-scores.   
   @param <self> is the object to update.
   @param <listOf_inlineResults> is the tield matrix of scores
   @param <globalStartPos_head> is sued to indetyf the 'real-wolrd' idneitfy of each head-vertex, ie, "globalStartPos_head + [0 .. (chunkSize_index1-1)]";
   @param <globalStartPos_tail> is sued to indetyf the 'real-wolrd' idneitfy of each tail-vertex, ie, "globalStartPos_tail + [0 .. (chunkSize_index2-1)]";
   @param <chunkSize_index1> is the number of vertices in teh 'row' direction.
   @param <chunkSize_index2> is the number of vertices in teh 'column' direction.
   @param <isLast_columnForBlock> which is used to know/indietfy if we are to start the value-trhesohld-filteirng (where latter may Not be appleid untila ll columsn in a row are evlauated, ie, to avodi loss in prediciton-accuracy wrt. correct range-based value-filtering).
   @param <config_cntMin_afterEach> is the minium number of vertices for the set to be of interest: : to ingore this proerpty set the latter variable to UINT_MAX or '0'.
   @param <config_cntMax_afterEach> is the maximum number of vertices for the set to be of interest: to ingore this proerpty set the latter variable to UINT_MAX
   @param <config_ballSize_max> is the maximum simalrity-score for a vertex to be inlcuded: to ingore this proerpty set the latter variable to T_FLOAT_MAX
   @param <isTo_ingore_zeroScores> which if set to true impleis taht we assume a '0' impleis a no-match: otherwise we assume "isOf_interest(..)" cpatures/coveres the 'is-set' attritube/value-proerpty.
   @param <isTo_updateSyemmtricRelations> which is used only if the rank-input-papramters are Not used
   @remarks 
   -- usage: in simlairty-metrics to store the op-ranking vertices, eg, as a pmertuation of our "get_sparse2d_fromKD__kd_tree(..)" ("kd_tree.h")
   -- usage: to comptue accurate clsuter-prediocnts (eg, in DB-SCAN)
 **/
void insertBlockOf_scores__sorted__kt_list_2d(s_kt_list_2d_kvPair_t *self, t_float **listOf_inlineResults, const uint globalStartPos_head, const uint globalStartPos_tail, const uint chunkSize_index1, const uint chunkSize_index2, const bool isLast_columnForBlock, const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores, const bool isTo_updateSyemmtricRelations);

/**
   @brief construct a symmetric data-set-object, ie, where ([key1][key2] != null)--implies--([key2][key1] != null) (oekseth, 06. aug. 2017).
   @param <parent> is the object to construct a symemtric subset for.
   @return the symmetic subset,
 **/
s_kt_list_2d_kvPair_t get_symmetricSubset__onKeys__s_kt_list_2d_kvPair_filterConfig_t(const s_kt_list_2d_kvPair_t *parent);


/**
   @struct s_kt_list_2d_kvPair_filterConfig
   @brief hold the configuraiton-object and resuylt-data-object (oekseth, 06. aug. 2017).
 **/
typedef struct s_kt_list_2d_kvPair_filterConfig {
  uint config_cntMin_afterEach;
  uint config_cntMax_afterEach;
  t_float config_ballSize_max;
  bool isTo_ingore_zeroScores;
  //!
  //! The data-object:
  s_kt_list_2d_kvPair_t self;
} s_kt_list_2d_kvPair_filterConfig_t;

//! @return an intlaized veriosn of the cofnigruation-object (oekseth, 06. aug. 2017).
static s_kt_list_2d_kvPair_filterConfig_t init__s_kt_list_2d_kvPair_filterConfig_t(const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores) {
  s_kt_list_2d_kvPair_filterConfig_t self;
  self.self = setToEmpty__s_kt_list_2d_kvPair_t();
  self.config_cntMin_afterEach = config_cntMin_afterEach;
  self.config_cntMax_afterEach = config_cntMax_afterEach;;
  self.config_ballSize_max = config_ballSize_max;
  self.isTo_ingore_zeroScores = isTo_ingore_zeroScores;
  //! 
  //! @return
  return self;
}

//! @return true if the rank-trehshold is to be sued for the object.
#define MF__isToUse_rankThresholds__s_kt_list_2d_kvPair_filterConfig_t(self) ({bool isTo_use = false; if(self) { \
  if( (self->config_cntMin_afterEach != 0) && (self->config_cntMin_afterEach != UINT_MAX) ) {isTo_use = true;} \
  else if( (self->config_cntMax_afterEach != 0) && (self->config_cntMax_afterEach != UINT_MAX) ) {isTo_use = true;} } isTo_use;})

//! De-allcoate the s_kt_list_2d_kvPair_filterConfig_t object.
static void free__s_kt_list_2d_kvPair_filterConfig_t(s_kt_list_2d_kvPair_filterConfig_t *self) {
  assert(self);
  free__s_kt_list_2d_kvPair_t(&(self->self));
}


/**
   @brief insert a 'tiled' block of vertex-row-scores.   
   @param <self> is the object to update.
   @param <listOf_inlineResults> is the tield matrix of scores
   @param <globalStartPos_head> is sued to indetyf the 'real-wolrd' idneitfy of each head-vertex, ie, "globalStartPos_head + [0 .. (chunkSize_index1-1)]";
   @param <globalStartPos_tail> is sued to indetyf the 'real-wolrd' idneitfy of each tail-vertex, ie, "globalStartPos_tail + [0 .. (chunkSize_index2-1)]";
   @param <chunkSize_index1> is the number of vertices in teh 'row' direction.
   @param <chunkSize_index2> is the number of vertices in teh 'column' direction.
   @param <isLast_columnForBlock> which is used to know/indietfy if we are to start the value-trhesohld-filteirng (where latter may Not be appleid untila ll columsn in a row are evlauated, ie, to avodi loss in prediciton-accuracy wrt. correct range-based value-filtering).
   @param <isTo_updateSyemmtricRelations> which is used only if the rank-input-papramters are Not used
   @remarks 
   -- usage: in simlairty-metrics to store the op-ranking vertices, eg, as a pmertuation of our "get_sparse2d_fromKD__kd_tree(..)" ("kd_tree.h")
   -- usage: to comptue accurate clsuter-prediocnts (eg, in DB-SCAN)
 **/
static void apply__s_kt_list_2d_kvPair_filterConfig_t(s_kt_list_2d_kvPair_filterConfig_t *self, t_float **listOf_inlineResults, const uint globalStartPos_head, const uint globalStartPos_tail, const uint chunkSize_index1, const uint chunkSize_index2, const bool isLast_columnForBlock, const bool isTo_updateSyemmtricRelations) {
  assert(self); assert(listOf_inlineResults);
  //!
  // printf("config_cntMax_afterEach=%u, at %s:%d\n", self->config_cntMax_afterEach, __FILE__, __LINE__);
  //! Apply logics:
  insertBlockOf_scores__sorted__kt_list_2d(&(self->self), listOf_inlineResults, globalStartPos_head, globalStartPos_tail, 
					   chunkSize_index1, chunkSize_index2, isLast_columnForBlock, 
					   //!
					   //! Value-specific configurations: 
					   self->config_cntMin_afterEach,
					   self->config_cntMax_afterEach,
					   self->config_ballSize_max,
					   self->isTo_ingore_zeroScores,
					   isTo_updateSyemmtricRelations
					   );
}

#endif //! EOF
