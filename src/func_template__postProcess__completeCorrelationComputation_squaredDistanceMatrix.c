{
  //! Waht we expect:
  assert(complexResult);
  assert(nrows_to_evaluate > 0);
  assert(nrows_outer_to_evaluate > 0);
  //if(rres_startPos != NULL) {  assert(resultMatrix); }

  long long int current_pos = 0;
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
  assert(complexResult);
#if(__templateInternalVariable__isTo_updateCountOf_vertices == 1) //! then we are to update the count of itnereesting cells.
  assert(complexResult->matrixOf_result_cnt_interesting_cells);
#endif
#endif

  //! Then iterate, udpating the result-object:

#ifndef __macroConfig__postProcess__isToAlwaysUpdateResultMatrix
  assert(nrows_outer_to_evaluate <= SM); // TODO: evlauate correctness of this, and if 'this' is wrong then udpate our "listOf_inlineResults" 'calls' (oekseth, 06. des. 2016).
  assert(nrows_to_evaluate <= SM); // TODO: evlauate correctness of this, and if 'this' is wrong then udpate our "listOf_inlineResults" 'calls' (oekseth, 06. des. 2016).
#endif


  // printf("\n\n## nrows_to_evaluate=[%u+%u, %u+%u], at %s:%d\n", globalStartPos_head, nrows_to_evaluate, globalStartPos_tail, nrows_outer_to_evaluate, __FILE__, __LINE__);
  for(uint row_id = 0; row_id < nrows_to_evaluate; row_id++) {
/* #if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar) */
/*     assert(complexResult->matrixOf_result_cnt_interesting_cells[row_id] != NULL); */
/* #endif */
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
      t_float *__restrict__ rres_startPos = NULL;
      if(listOf_inlineResults) {
	rres_startPos = listOf_inlineResults[row_id];
	//	rres_startPos = listOf_inlineResults[0];
      } else {
	assert(resultMatrix != NULL);
	rres_startPos = &resultMatrix[globalStartPos_head+row_id][globalStartPos_tail]; //! ie, the 'upper square' of the matrix we update.
	//	rres_startPos = &resultMatrix[index1][index2]; //! ie, the 'upper square' of the matrix we update.
      }
      // assert(false); // FIXME: validte correctness of [ªbvoe] indexes ... 
      assert(rres_startPos);
#endif

    for(uint col_id = 0; col_id < nrows_outer_to_evaluate; col_id++) {
      const uint index_global_head = globalStartPos_head + row_id;
      const uint index_global_tail = globalStartPos_tail + col_id;
      //printf("---\n(compute-postProcess-result-for-pair)\t [%u][%u], at %s:%d\n", index_global_head, index_global_tail, __FILE__, __LINE__);
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar)
      assert(rres_startPos); //! eg, defined in the start-section of our "func_template__tile.c"
/* #ifdef __localConfig_calledFrom_allAgainstAll */
/*       assert(current_pos < debug_cnt_resultCells_updated); //! ie, what we expect <-- FIXME: remvoe this. */
/* #endif */
      t_float result = rres_startPos[col_id]; current_pos++;
      //t_float result = rres_startPos[current_pos]; current_pos++;
      // assert_possibleOverhead(isinf(result)  == false); //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
      if(resultMatrix != NULL) {
	/* printf("(compare-values)\t %f VS %f, at [%u/%u][%u/%u], at %s:%d\n", result, resultMatrix[index_global_head][index_global_tail], row_id, nrows_to_evaluate, col_id, nrows_outer_to_evaluate, __FILE__, __LINE__); */
	/* assert(result == resultMatrix[index_global_head][index_global_tail]); */
	result = resultMatrix[index_global_head][index_global_tail];
	// assert_possibleOverhead(isinf(result)  == false);  //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
	//printf("\t\t\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      } 
#ifndef __macroConfig__postProcess__isToAlwaysUpdateResultMatrix
      else {
	assert_possibleOverhead(row_id < SM);
	assert_possibleOverhead(col_id < SM);
	result = listOf_inlineResults[row_id][col_id];
	// assert_possibleOverhead(isinf(result)  == false); //  //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
      }
#endif
      // printf("\t\t result[%u][%u]=%f, at %s:%d\n", index_global_head, index_global_tail, result, __FILE__, __LINE__);
      //if(listOf_inlineResults == NULL) { assert(index1 == globalStartPos_head); assert(rres_startPos = &resultMatrix[index1][index2]);    assert(result == resultMatrix[index_global_head][index_global_tail]);} // FIXME: remove this line
      //t_float result = local_resultMatrix[row_id][col_id]; //! ie, the 'upper square' of the matrix we update.		   		   
      //t_float result = resultMatrix[index_global_head][index_global_tail]; //! ie, the 'upper square' of the matrix we update.		   		   
      // printf("resultNonScaled[%u][%u]=%f, at %s:%d\n", row_id, col_id, result, __FILE__, __LINE__); //! ie, to investigate the 'complaint' about the valeu being used 'un-intalized'.
#else
      s_template_correlation_tile_temporaryResult_t objLocalComplex_nonSSE;
      //#warning" the objLocalComplex_nonSSE is initated"
      //const s_kt_computeTile_subResults_t obj_local = complexResult[row_id][col_id];
#if( (TEMPLATE_local_distanceConfiguration_isTo_updateCountOf_vertices == 1) )
#if( (TEMPLATE_iterationConfiguration_typeOf_mask != 0/* = e_template_correlation_tile_maskType_mask_none*/) )
      //#if(__templateInternalVariable__isTo_updateCountOf_vertices == 1) //! then we are to update the count of itnereesting cells.
      //#if(__templateInternalVariable__isTo_updateCountOf_vertices == 1) //! then we are to update the count of itnereesting cells.
      objLocalComplex_nonSSE.cnt_interesting_cells = complexResult->matrixOf_result_cnt_interesting_cells[row_id][col_id];
#else
      objLocalComplex_nonSSE.cnt_interesting_cells = ncols;
#endif
      // printf("count-is-updated, at %s:%d\n", __FILE__, __LINE__);
#else
      // printf("count-is-Not-updated, at %s:%d\n", __FILE__, __LINE__);
#endif
      objLocalComplex_nonSSE.numerator = complexResult->matrixOf_result_numerator[row_id][col_id];
      objLocalComplex_nonSSE.denumerator = complexResult->matrixOf_result_denumerator[row_id][col_id];
#if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate == 2) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_numeratorAnd_denumeratorComplex)
      objLocalComplex_nonSSE.numeratorLimitedTo_row_1and2 = complexResult->matrixOf_result_numeratorLimitedTo_row_1and2[row_id][col_id];
      objLocalComplex_nonSSE.denumeratorLimitedTo_row_1and2 = complexResult->matrixOf_result_denumeratorLimitedTo_row_1and2[row_id][col_id];
      assert(complexResult->matrixOf_result_denumeratorLimitedTo_row1); 
      assert(complexResult->matrixOf_result_denumeratorLimitedTo_row2);
      objLocalComplex_nonSSE.denumeratorLimitedTo_row1 = complexResult->matrixOf_result_denumeratorLimitedTo_row1[row_id][col_id];
      objLocalComplex_nonSSE.denumeratorLimitedTo_row2 = complexResult->matrixOf_result_denumeratorLimitedTo_row2[row_id][col_id];
#else
      // printf("then we assume no-complex input-objects are to be used, at %s:%d\n", __FILE__, __LINE__);
#endif
      // printf("result-before-adjustment={complex-object}, at %s:%d\n", __FILE__, __LINE__); //! ie, to investigate the 'complaint' about the valeu being used 'un-intalized'.
#endif
      //objLocalComplex_nonSSE. = ; complexResult->matrixOf_result_[row_id][col_id];

      //! --------------------------------------
      //! Post-process:
      t_float result_adjusted = T_FLOAT_MAX;
      //printf("\t\t\tat [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);

      /* { */
      /* 	t_float result = 8; printf("FIXME: remove this at %s:%d\n", __FILE__, __LINE__);   */
#include "template_correlation_tile_applyMetric_postProcessing.c"  
      //      }
      //assert(result_adjusted != T_FLOAT_MAX); //! ie, as we epxect the [ªbove] call to have udpated the variable in question.

      //!
      //! Update the result-matrix with [ªbove] comptuation:
      //printf("result-matrix[%u][%u] = %f, at %s:%d\n", index_global_head, index_global_tail, result_adjusted, __FILE__, __LINE__);
      if(resultMatrix != NULL) {
	// assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result_adjusted) == false); //  //! which is out-commented as we observe for some meitrcs that highvalue-score-datasets (eg, for 'radnom-generarted-numbers') results in"inf" results (oekseth, 06. arp. 2017)
	//assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_adjusted) == false);
	//! Update:
	if(globalProp__dataSetIsSymmeric == false) {
	  resultMatrix[index_global_head][index_global_tail] = result_adjusted;
	} else { //! then we update both:
	  resultMatrix[index_global_head][index_global_tail] = result_adjusted;
	  // FIXME: validate correctness of [”elow] (oekseth, 06. des. 2016).
	  resultMatrix[index_global_tail][index_global_head] = result_adjusted;
	}
      } 
#ifndef __macroConfig__postProcess__isToAlwaysUpdateResultMatrix
      else {
	assert_possibleOverhead(row_id < SM);
	assert_possibleOverhead(col_id < SM);
	if(globalProp__dataSetIsSymmeric == false) {
	  listOf_inlineResults[row_id][col_id] = result_adjusted;
	} else {
	  listOf_inlineResults[row_id][col_id] = result_adjusted;
	  // FIXME: validate correctness of [”elow] (oekseth, 06. des. 2016).
	  listOf_inlineResults[col_id][row_id] = result_adjusted;
	}
      }
#endif
      //debug_cnt_resultCells_updated++;
    }
  }
}
