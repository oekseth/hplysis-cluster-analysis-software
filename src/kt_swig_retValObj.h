#ifndef kt_swig_retValObj_h
#define kt_swig_retValObj_h

/**
   @brief provide a wrapper-object to simplify writign of programmign-language-wrappers to our hpLysis software (eosekth, 06. arp. 2017).
 **/
typedef struct s_kt_swig_retValObj {
  uint head;    uint tail;
  uint score;
  const char *name;
} s_kt_swig_retValObj_t;

#endif
