 //!
//! Identify the thresholds:
const t_float min_cnt_rows    = self->thresh_minMax__cellMask_valueUpperLower_matrix[0];  const t_float max_cnt_rows    = self->thresh_minMax__cellMask_valueUpperLower_matrix[1];

//! -----------------
//!
//! Apply the filters:
const bool isTo_evaluateFor_rows = ( (min_cnt_rows    != T_FLOAT_MIN_ABS) || (max_cnt_rows != T_FLOAT_MAX) ); //! ie, as least one of the valeus shoudl different from the 'extreme cases'.

if( (isTo_evaluateFor_rows) ) {
  const uint default_value_float = 0;
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    if(self->mapOf_interesting_rows[row_id]) { //! then we inspect valeus wrt. the column:
      const t_float *__restrict__ row = self->matrix[row_id];
      //! ---
      for(uint col_id = 0; col_id < self->ncols; col_id++) {
	if(isOf_interest(row[col_id])) {
	  //! Apply the thresholds:
	  if( (row[col_id] <= min_cnt_rows) || (row[col_id] >= max_cnt_rows) ) {
	    self->matrix[row_id][col_id] = get_implicitMask(); //! ie, then mark the amsk as 'not-of-itnerest'
	  }
	}
      }
    }
  }
 }


