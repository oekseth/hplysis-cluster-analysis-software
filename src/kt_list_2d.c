#include "kt_list_2d.h"



//! @return an itnailted list-object with size "list_size"
s_kt_list_2d_uint_t init__s_kt_list_2d_uint_t(uint cnt_rows, uint list_size)  {
  assert(cnt_rows != UINT_MAX);
  //assert(cnt_rows > 0);
  s_kt_list_2d_uint_t self; 
  self.list = NULL;   self.list_size = 0;  self.current_pos = 0;
  if(cnt_rows > 0) {
    self.list_size = cnt_rows;
    self.list = MF__allocate__s_ktType_s_kt_list_1d_uint_t(cnt_rows);
    if( (list_size > 0) && (list_size != UINT_MAX) ) {
      for(uint i = 0; i < cnt_rows; i++) {
	self.list[i] = init__s_kt_list_1d_uint_t(list_size);
      }
    } else {
      for(uint i = 0; i < cnt_rows; i++) {
	self.list[i] = setToEmpty__s_kt_list_1d_uint_t();
      }
    }
  }
  //! @return itnated object:
  return self;
}
//! @return an 'empty verison' of our s_kt_list_2d_uint_t object
s_kt_list_2d_uint_t setToEmpty__s_kt_list_2d_uint_t() {
  return init__s_kt_list_2d_uint_t(0, 0);
}
/* //! intiates a data-set from a psarse matrix */
/* //! @return an itnailted list-object when data is parsed from a list-input-file. */
/* //! @remarks we expect the inptu to consist of only 2 columns. */
/* s_kt_list_2d_uint_t initFrom_sparseMatrix__s_kt_list_2d_uint_t(const uint row_id, const uint nrows, const uint ncols, t_float **matrix) ; */
/* //! @return an itnailted list-object when data is parsed from a list-input-file. */
/* //! @remarks we expect the inptu to consist of only 2 columns. */
/* s_kt_list_2d_uint_t initFromFile__s_kt_list_2d_uint_t(const uint row_id, const char *input_file); */
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, uint index, uint *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_uint();
  if(row_id < self->list_size) {
    if(index < self->list[row_id].list_size) {
      assert(self->list);
      *scalar_result = self->list[row_id].list[index];
      if(false == MF__isEmpty__s_ktType_uint((*scalar_result))) {return true;} 
    }
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, uint index, uint valueTo_set) {
  assert(self);
#include "kt_list_2d__stub__alloc_uint.c" //! ie, enlarge if the size is outside the bounary to set.
  set_scalar__s_kt_list_1d_uint_t(&(self->list[row_id]), index, valueTo_set);
}
//! De-allcoates the s_kt_list_2d_uint_t object.
void free__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self) {
  assert(self);
  if(self->list) {
    for(uint i = 0; i < self->list_size; i++) {
      free__s_kt_list_1d_uint_t(&(self->list[i])); //! ie, to de-allcoat the 'overflow'.
    }
    MF__free__s_kt_list_1d_uint_t((self->list)); 
    self->list = NULL; self->list_size = 0;
  }
  self->current_pos = 0;
}
//! Push a list-item to the set. (oekseth, 06. jul. 2017).
void push__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, const uint valueTo_set)  {
  assert(false == MF__isEmpty__s_ktType_uint(valueTo_set)); //! ie, as otehriwse would be a pointless inseriton.
  assert(valueTo_set != T_FLOAT_MAX);
  //assert(valueTo_set != T_FLOAT_MAX);
  const uint index = row_id;
  assert(index != UINT_MAX);
  if(index >= self->list_size) {
  //  if(self->current_pos >= self->list_size) {
#include "kt_list_2d__stub__alloc_uint.c" //! ie, enlarge if the size is outside the bounary to set.
  }
  assert(self->list);
  //  assert(self->list[row_id]);
  assert(row_id < self->list_size);
  push__s_kt_list_1d_uint_t(&(self->list[row_id]), valueTo_set);
  if(row_id >= self->current_pos) {self->current_pos = row_id + 1;}
}
/* //! Add a vertex to the heap (oekseth, 06. jul. 2017). */
/* void addTo_heap_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, const uint valueTo_set) { */
/*   if(self->current_pos >= self->list_size) { */
/* #include "kt_list_2d__stub__alloc_uint.c" //! ie, enlarge if the size is outside the bounary to set. */
/*   } */
/*   assert(self->list); */
/*   //  assert(self->list[row_id]); */
/*   addTo_heap_max__s_kt_list_1d_uint_t(&(self->list[row_id]), valueTo_set); */
/* } */
/* //! Consutrct a max-heap (oekseth, 06. jul. 2017). */
/* void buildHeap_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id) { */
/*   assert(self->list); */
/*   //  assert(self->list[row_id]); */
/*   buildHeap_max__s_kt_list_1d_uint_t(&(self->list[row_id])); */
/* } */
/* //! Remove the top-node in a max-heap (oekseth, 06. jul. 2017). */
/* void deleteHeapNode_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id)  { */
/*   assert(self->list); */
/*   if( (row_id < self->list_size) ) { // && self->list[row_id]) { */
/*     deleteHeapNode_max__s_kt_list_1d_uint_t(&(self->list[row_id])); */
/*   } */
/* } */
//! ***************************************************************************
//! ***************************************************************************

/* //! Sort the data-set (oekseth, 06. jul. 2017). */
/* void sort__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id) { */
/*   assert(self->list); */
/*   if( (row_id < self->list_size) ) { // && self->list[row_id]) { */
/*     sort__s_kt_list_1d_uint_t(&(self->list[row_id])); */
/*   } */
/* } */


//! Udpate the object-lsit size (oesketh, 06. aug. 2017).
//! @remarks is used to enable a 'comrepssion' of meory-usage, ie, to reduce rednant emmroys-aprce when memory-overla is of interest, eg, used as a psot-ranknig-stpe
void setTo_size__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint list_size) {
  assert(list_size > 0); //§! ie, as the 'free-memory' sohudl otheriwse have been called.
  { //! Enlarge list if nessseary:
    s_kt_list_1d_uint_t *new_list = MF__allocate__s_ktType_s_kt_list_1d_uint_t(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = setToEmpty__s_kt_list_1d_uint_t(); // MF__init__s_ktType_uint(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(self->list_size != 0) {
      const uint cnt_copy = macro_min(list_size, self->list_size);
      for(uint i = 0; i < cnt_copy; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      for(uint i = cnt_copy; i < self->list_size; i++) {
	free__s_kt_list_1d_uint_t(&(self->list[i])); //! ie, to de-allcoat the 'overflow'.
      }
      if(self->list != NULL) {
	MF__free__s_kt_list_1d_uint_t((self->list)); 
      }
    }
    //! Update pointer:
    self->list = new_list;    
    //printf("(change-list-size) \t %u-->%u, at %s:%d\n", self->list_size, list_size, __FILE__, __LINE__);
    self->list_size = list_size;
  }
}


// ***************************************************************************
// ***********************'***************************************************


//! @return an itnailted list-object with size "list_size"
s_kt_list_2d_kvPair_t init__s_kt_list_2d_kvPair_t(uint cnt_rows, uint list_size) {
  assert(cnt_rows != UINT_MAX);
  //assert(cnt_rows > 0);
  s_kt_list_2d_kvPair_t self; 
  self.list = NULL;   self.list_size = 0;  self.current_pos = 0;
  if(cnt_rows > 0) {
    //printf("(change-list-size) \t %u-->%u, at %s:%d\n", self.list_size, cnt_rows, __FILE__, __LINE__);
    self.list_size = cnt_rows;
    self.list = MF__allocate__s_ktType_s_kt_list_1d_kvPair_t(cnt_rows);
    if( (list_size > 0) && (list_size != UINT_MAX) ) {
      for(uint i = 0; i < cnt_rows; i++) {
	self.list[i] = init__s_kt_list_1d_kvPair_t(list_size);
      }
    } else {
      for(uint i = 0; i < cnt_rows; i++) {
	self.list[i] = setToEmpty__s_kt_list_1d_kvPair_t();
      }
    }
  }
  //! @return itnated object:
  return self;
}

//! @return an 'empty verison' of our s_kt_list_2d_kvPair_t object
s_kt_list_2d_kvPair_t setToEmpty__s_kt_list_2d_kvPair_t() {
  return init__s_kt_list_2d_kvPair_t(0, 0);
}

/* //! intiates a data-set from a psarse matrix */
/* //! @return an itnailted list-object when data is parsed from a list-input-file. */
/* //! @remarks we expect the inptu to consist of only 2 columns. */
/* s_kt_list_2d_kvPair_t initFrom_sparseMatrix__s_kt_list_2d_kvPair_t(const uint row_id, const uint nrows, const uint ncols, t_float **matrix) { */

/*   assert(false); // FIXME: add for [”elow]. */

/*   /\* if(nrows && (ncols == 2) && matrix) { *\/ */
/*   /\*   s_kt_list_2d_kvPair_t self = init__s_kt_list_2d_kvPair_t(/\\*size=*\\/nrows); //! ie, the number of kvPairs. *\/ */
/*   /\*   //! *\/ */
/*   /\*   //! Insert kvPairs: *\/ */
/*   /\*   for(uint i = 0; i < nrows; i++) { *\/ */
/*   /\*     assert(matrix[i]); *\/ */
/*   /\*     self.list[i] = MF__initFromRow__s_ktType_kvPair(matrix[i], ncols); *\/ */
/*   /\*   } *\/ */
/*   /\*   //! @return *\/ */
/*   /\*   return self; *\/ */
/*   /\* } else { *\/ */
/*   /\*   assert(false); //! ie, an heads-up. *\/ */
/*   /\*   //! @return *\/ */
/*   /\*   return setToEmpty__s_kt_list_2d_kvPair_t(); *\/ */
/*   /\* } *\/ */
/* } */
/* //! @return an itnailted list-object when data is parsed from a list-input-file. */
/* //! @remarks we expect the inptu to consist of only 2 columns. */
/* s_kt_list_2d_kvPair_t initFromFile__s_kt_list_2d_kvPair_t(const uint row_id, const char *input_file) { */

/* } */
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id, uint index, s_ktType_kvPair_t *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = MF__init__s_ktType_kvPair();
  if(row_id < self->list_size) {
    if(index < self->list[row_id].list_size) {
      assert(self->list);
      *scalar_result = self->list[row_id].list[index];
      if(false == MF__isEmpty__s_ktType_kvPair((*scalar_result))) {return true;} 
    }
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id, uint index, s_ktType_kvPair_t valueTo_set) {
  assert(self);
#include "kt_list_2d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
  set_scalar__s_kt_list_1d_kvPair_t(&(self->list[row_id]), index, valueTo_set);
}
//! De-allcoates the s_kt_list_2d_kvPair_t object.
void free__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self) {
  assert(self);
  if(self->list) {
    for(uint i = 0; i < self->list_size; i++) {
      free__s_kt_list_1d_kvPair_t(&(self->list[i])); //! ie, to de-allcoat the 'overflow'.
    }
    MF__free__s_kt_list_1d_kvPair_t((self->list)); 
    self->list = NULL; self->list_size = 0;
  }
  self->current_pos = 0;
}
//! Set minimum size of the object (oekseth, 0+6. otk. 2017)
void setMinSize__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint min_size) {
  assert(min_size != 0); assert(min_size != UINT_MAX);
  const uint index = min_size; const uint row_id = min_size;
  //  const uint index = self->current_pos;
  assert(index != UINT_MAX);
  if(index >= self->list_size) {
  //  if(self->current_pos >= self->list_size) {
#include "kt_list_2d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
  }
}

//! Push a list-item to the set. (oekseth, 06. jul. 2017).
void push__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id, const s_ktType_kvPair_t valueTo_set) {
  assert(false == MF__isEmpty__s_ktType_kvPair(valueTo_set)); //! ie, as otehriwse would be a pointless inseriton.
  assert(valueTo_set.tail != T_FLOAT_MAX);
  //assert(valueTo_set != T_FLOAT_MAX);
  const uint index = row_id;
  //  const uint index = self->current_pos;
  assert(index != UINT_MAX);
  if(index >= self->list_size) {
  //  if(self->current_pos >= self->list_size) {
#include "kt_list_2d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
  }
  assert(self->list);
  //  assert(self->list[row_id]);
  push__s_kt_list_1d_kvPair_t(&(self->list[row_id]), valueTo_set);
  if(row_id > self->current_pos) {self->current_pos = row_id + 1;}
  //  printf("(insert_push)\t at index[%u]=%u, at %s:%d\n", row_id, self->list[row_id].current_pos-1, __FILE__, __LINE__);
}
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id, const s_ktType_kvPair_t valueTo_set) {
  if(self->current_pos >= self->list_size) {
#include "kt_list_2d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
  }
  assert(self->list);
  //  assert(self->list[row_id]);
  addTo_heap_max__s_kt_list_1d_kvPair_t(&(self->list[row_id]), valueTo_set);
}
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id) {
  assert(self->list);
  //  assert(self->list[row_id]);
  buildHeap_max__s_kt_list_1d_kvPair_t(&(self->list[row_id]));
}
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id) {
  assert(self->list);
  if( (row_id < self->list_size) ) { // && self->list[row_id]) {
    deleteHeapNode_max__s_kt_list_1d_kvPair_t(&(self->list[row_id]));
  }
}
//! ***************************************************************************
//! ***************************************************************************

//! Sort the data-set (oekseth, 06. jul. 2017).
void sort__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint row_id) {
  assert(self->list);
  if( (row_id < self->list_size) ) { // && self->list[row_id]) {
    sort__s_kt_list_1d_kvPair_t(&(self->list[row_id]));
  }
}

//! Udpat ethe object-lsit size (oesketh, 06. aug. 2017).
//! @remarks is used to enable a 'comrepssion' of meory-usage, ie, to reduce rednant emmroys-aprce when memory-overla is of interest, eg, used as a psot-ranknig-stpe
void setTo_size__s_kt_list_2d_kvPair_t(s_kt_list_2d_kvPair_t *self, const uint list_size) {
  assert(list_size > 0); //§! ie, as the 'free-memory' sohudl otheriwse have been called.
  { //! Enlarge list if nessseary:
    s_kt_list_1d_kvPair_t *new_list = MF__allocate__s_ktType_s_kt_list_1d_kvPair_t(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = setToEmpty__s_kt_list_1d_kvPair_t(); // MF__init__s_ktType_kvPair(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(self->list_size != 0) {
      const uint cnt_copy = macro_min(list_size, self->list_size);
      for(uint i = 0; i < cnt_copy; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      for(uint i = cnt_copy; i < self->list_size; i++) {
	free__s_kt_list_1d_kvPair_t(&(self->list[i])); //! ie, to de-allcoat the 'overflow'.
      }
      if(self->list != NULL) {
	MF__free__s_kt_list_1d_kvPair_t((self->list)); 
      }
    }
    //! Update pointer:
    self->list = new_list;    
    //printf("(change-list-size) \t %u-->%u, at %s:%d\n", self->list_size, list_size, __FILE__, __LINE__);
    assert(self->list_size >= list_size);
    self->list_size = list_size;
  }
}

s_kt_list_1d_float_t translate_toFloats__s_kt_list_2d_kvPair_t(const s_kt_list_2d_kvPair_t *self) {
  assert(self);
  //! 
  //! Allocate size
  uint arr_scores_size = 0;
  for(uint head_id = 0; head_id < self->current_pos; head_id++) {
    uint cnt_tails = 0; //self->list[head_id].current_pos;
    if(cnt_tails == 0) {
      const uint size_local = self->list[head_id].list_size;
      if(size_local > 0) {
	for(uint k = 0; k < size_local; k++) {
	  const s_ktType_kvPair_t obj_this = self->list[head_id].list[k];
	  if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	    cnt_tails++;
	  }
	}
      }
    }
    //! Increment count: 
    arr_scores_size += cnt_tails;
  }
  //! Allocate:
  s_kt_list_1d_float_t obj_scores = init__s_kt_list_1d_float_t(arr_scores_size);
  assert(obj_scores.list_size >= arr_scores_size);
  assert(obj_scores.current_pos == 0);
  //! 
  //! Insert scores: 
  for(uint head_id = 0; head_id < self->current_pos; head_id++) {
    uint cnt_tails = self->list[head_id].current_pos;
    if(cnt_tails == 0) {
      cnt_tails = self->list[head_id].list_size;
    }
    if(cnt_tails > 0) {
      for(uint k = 0; k < cnt_tails; k++) {
	const s_ktType_kvPair_t obj_this = self->list[head_id].list[k];
	if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	  assert((obj_scores.current_pos+1) <= obj_scores.list_size);
	  obj_scores.list[obj_scores.current_pos] = obj_this.tail;
	  //	  printf("[%u]=%f, at %s:%d\n", obj_scores.current_pos, obj_scores.list[obj_scores.current_pos], __FILE__, __LINE__);
	  //! Increment:
	  obj_scores.current_pos++;
	}
      }
    }
  }
  return obj_scores;
}


s_kt_list_1d_float_t translate_histogram__s_kt_list_2d_kvPair_t(const s_kt_list_2d_kvPair_t *self, const uint cnt_bins, const bool isTo_forMinMax_useGlobal, e_kt_histogram_scoreType_t enum_id_scoring) {
  assert(self); assert(cnt_bins != UINT_MAX); assert(cnt_bins > 1);
  //! 
  //! Allocate size
  uint arr_scores_size = 0;
  t_float score_min = T_FLOAT_MAX;   t_float score_max = T_FLOAT_MIN_ABS;
  for(uint head_id = 0; head_id < self->current_pos; head_id++) {
    const uint size_local = self->list[head_id].list_size;
    if(size_local > 0) {
      for(uint k = 0; k < size_local; k++) {
	const s_ktType_kvPair_t obj_this = self->list[head_id].list[k];
	if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	  const t_float score = obj_this.tail;
	  if(isOf_interest(score)) {
	    score_min = macro_min(score_min, score);
	    score_max = macro_max(score_max, score);
	    arr_scores_size++;
	  }
	}
      }
    }
  }
  if(arr_scores_size == 0) { return setToEmpty__s_kt_list_1d_float_t(); }
  //const t_float avg_score_each = 1/arr_scores_size;
  //! Allocate:
  s_kt_list_1d_float_t obj_scores = init__s_kt_list_1d_float_t(cnt_bins);
  obj_scores.current_pos = cnt_bins;
  for(uint bin_id = 0; bin_id < cnt_bins; bin_id++) {
    obj_scores.list[bin_id] = 0; //! ie, the defualt count.
  }
  assert(obj_scores.list_size >= cnt_bins);
  if(score_max == score_min) { //! then we reutnr only 'one'
    // printf("[%u]=%f, with score-range=[%f, %f], at %s:%d\n", cnt_bins-1, (t_float)arr_scores_size, score_min, score_max, __FILE__, __LINE__);
    obj_scores.list[cnt_bins-1] = (t_float)arr_scores_size;
  } else {
    const t_float cnt_eachBucket_inv_global = (t_float)cnt_bins / (t_float)(score_max - score_min); //! ie, (cnt=10) / (5-3) = 5. Use-cases: [3: '(3-3)*5 = 0']++, [4: '(4-3)*5 = 5]++, [5: '(5-3)*5 = 10 ]++, ['']++,  
    //! 
    //! Insert scores: 
    for(uint head_id = 0; head_id < self->current_pos; head_id++) {
      uint cnt_tails = self->list[head_id].current_pos;
      if(cnt_tails == 0) {
	cnt_tails = self->list[head_id].list_size;
      }
      if(cnt_tails > 0) {
	t_float cnt_eachBucket_inv = cnt_eachBucket_inv_global;
	t_float score_min_loc = score_min;   t_float score_max_loc = score_max;
	if(isTo_forMinMax_useGlobal) { //! then we are interested in fidnig the local min--max for the given row:
	  for(uint k = 0; k < cnt_tails; k++) {
	  const s_ktType_kvPair_t obj_this = self->list[head_id].list[k];
	  if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	    const t_float score = obj_this.tail;
	    if(isOf_interest(score)) {	  
	      score_min_loc = macro_min(score_min_loc, score);
	      score_max_loc = macro_max(score_max_loc, score);
	    }
	  }
	  //!
	  //! Update min-max:
	  cnt_eachBucket_inv = (t_float)cnt_bins / (t_float)(score_max_loc - score_min_loc); //! ie, (cnt=10) / (5-3) = 5. Use-cases: [3: '(3-3)*5 = 0']++, [4: '(4-3)*5 = 5]++, [5: '(5-3)*5 = 10 ]++, ['']++,  
	  }
	}
	if(score_max_loc == score_min_loc) {continue;} //! ie, as all valeus are then equaøl.
	//! 
	//! 
	for(uint k = 0; k < cnt_tails; k++) {
	  const s_ktType_kvPair_t obj_this = self->list[head_id].list[k];
	  if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	    const t_float score = obj_this.tail;
	    if(isOf_interest(score)) {
	      assert(score >= score_min_loc);
	      assert(score <= score_max_loc);	    
	      uint bucket_id = 0;
	      if(score != score_min_loc) {
		bucket_id = (uint)((score - score_min_loc)*cnt_eachBucket_inv);
		if(bucket_id >= cnt_bins)  {bucket_id = (cnt_bins - 1);}
	      }
	      //! Increment:
	      //	      printf("[%u]++ given score=%f, at %s:%d\n", bucket_id, score, __FILE__, __LINE__);
	      if(enum_id_scoring == e_kt_histogram_scoreType_sum) {
		obj_scores.list[bucket_id] += score; //! ie, the defualt count.
	      } else {
		obj_scores.list[bucket_id] += 1; //! ie, the defualt count.
	      }
	    }
	  }
	}
      }
    }
  }
  if(enum_id_scoring == e_kt_histogram_scoreType_count_averageOnBins) { //! then normalize the results, ie, to avoid 'skwness' wrt. large data-sets: 
    assert(cnt_bins > 0);
    assert(arr_scores_size > 0);
    t_float count_max = 0; 
    for(uint bin_id = 0; bin_id < cnt_bins; bin_id++) {
      count_max = macro_max(count_max, obj_scores.list[bin_id]);
    }
    const t_float average = /*sum-total=*/(t_float)arr_scores_size / /*bins=*/(t_float)cnt_bins;
    //    const t_float average = /*sum-total=*/(t_float)cnt_bins / /*bins=*/(t_float)arr_scores_size;
    //    const t_float average = (t_float)arr_scores_size / (t_float)cnt_bins;
    assert(average != 0);
    const t_float de_numerator = count_max - average;
    for(uint bin_id = 0; bin_id < cnt_bins; bin_id++) {
      t_float numerator = obj_scores.list[bin_id] - average;
      t_float score = 1; //! ie, a center-point. old_data[i]; //! ie, 
      if( (numerator != 0) && (de_numerator != 0) ) {
	score = numerator / de_numerator;
	assert(score != T_FLOAT_MAX);
      }
      //printf("\t\t [%u]=%f, given num=%f, deNum=%f, at %s:%d\n", i, score, numerator, de_numerator, __FILE__, __LINE__);
      obj_scores.list[bin_id] = score;
    }
  }
  return obj_scores;
}
void writeOut_histoGram1d_TSV__s_kt_list_2d_kvPair_t(const s_kt_list_2d_kvPair_t *self, FILE *fileP_result, const char *strHead, const bool isTo_generateHead, const uint cnt_bins) {
  assert(self); assert(cnt_bins != UINT_MAX); assert(cnt_bins > 1);
  assert(fileP_result);
  if(isTo_generateHead) {
    if(strHead && strlen(strHead)) {fprintf(fileP_result, "%s\t", "row-name");}
    for(uint i = 0; i < cnt_bins; i++) {
      fprintf(fileP_result, "bin=%u\t", i);
    }
    fprintf(fileP_result, "\n");
  }
  const bool isTo_forMinMax_useGlobal = true; const e_kt_histogram_scoreType_t enum_id_scoring = e_kt_histogram_scoreType_undef;
  s_kt_list_1d_float_t obj_histo = translate_histogram__s_kt_list_2d_kvPair_t(self, cnt_bins, isTo_forMinMax_useGlobal, enum_id_scoring);
  if(obj_histo.list_size > 0) {
    if(strHead && strlen(strHead)) {fprintf(fileP_result, "%s\t", strHead);}
    for(uint i = 0; i < cnt_bins; i++) {
      const t_float score = obj_histo.list[i];
      if(isOf_interest(score)) {
	fprintf(fileP_result, "%f\t", score);
      } else {
	fprintf(fileP_result, "\t");
      }
    }
    fprintf(fileP_result, "\n");    
  }  
  free__s_kt_list_1d_float_t(&obj_histo);
}

 

/**
   @brief insert a 'tiled' block of vertex-row-scores.   
   @param <self> is the object to update.
   @param <listOf_inlineResults> is the tield matrix of scores
   @param <globalStartPos_head> is sued to indetyf the 'real-wolrd' idneitfy of each head-vertex, ie, "globalStartPos_head + [0 .. (chunkSize_index1-1)]";
   @param <globalStartPos_tail> is sued to indetyf the 'real-wolrd' idneitfy of each tail-vertex, ie, "globalStartPos_tail + [0 .. (chunkSize_index2-1)]";
   @param <chunkSize_index1> is the number of vertices in teh 'row' direction.
   @param <chunkSize_index2> is the number of vertices in teh 'column' direction.
   @param <isLast_columnForBlock> which is used to know/indietfy if we are to start the value-trhesohld-filteirng (where latter may Not be appleid untila ll columsn in a row are evlauated, ie, to avodi loss in prediciton-accuracy wrt. correct range-based value-filtering).
   @param <config_cntMin_afterEach> is the minium number of vertices for the set to be of interest: : to ingore this proerpty set the latter variable to UINT_MAX or '0'.
   @param <config_cntMax_afterEach> is the maximum number of vertices for the set to be of interest: to ingore this proerpty set the latter variable to UINT_MAX
   @param <config_ballSize_max> is the maximum simalrity-score for a vertex to be inlcuded: to ingore this proerpty set the latter variable to T_FLOAT_MAX
   @param <isTo_ingore_zeroScores> which if set to true impleis taht we assume a '0' impleis a no-match: otherwise we assume "isOf_interest(..)" cpatures/coveres the 'is-set' attritube/value-proerpty.
   @remarks 
   -- usage: in simlairty-metrics to store the op-ranking vertices, eg, as a pmertuation of our "get_sparse2d_fromKD__kd_tree(..)" ("kd_tree.h")
   -- usage: to comptue accurate clsuter-prediocnts (eg, in DB-SCAN)
 **/
void insertBlockOf_scores__sorted__kt_list_2d(s_kt_list_2d_kvPair_t *self, t_float **listOf_inlineResults, const uint globalStartPos_head, const uint globalStartPos_tail, const uint chunkSize_index1, const uint chunkSize_index2, const bool isLast_columnForBlock, const uint config_cntMin_afterEach, const uint config_cntMax_afterEach, const t_float config_ballSize_max, const bool isTo_ingore_zeroScores, const bool isTo_updateSyemmtricRelations) {
  assert(self);
  assert(globalStartPos_head != UINT_MAX);
  assert(globalStartPos_tail != UINT_MAX);
  // t_float *result = objCaller_postProcessResult->listOf_result_afterCorrelation; //! ie, the list to update.
  // assert(result);
  //assert((SM % 4) == 0); //! otehrwise udpate [below].
  // assert(false); // FIXME: sub-divide [below] into 'sperate squares'.	    
  /* const uint chunkSize_index2_iterSize = (chunkSize_index2 >= 256) ? macro__get_maxIntriLength_float(chunkSize_index2) : 0; //! where we use an 'extra-voerhead-thresohld' givne teh comptuational overheaf wrt. SSE-functinos (oekseth, 06. sept. 2016). */
  /* //const uint chunkSize_index2_iterSize = macro__get_maxIntriLength_float(chunkSize_index2); */ 
  /* VECTOR_FLOAT_TYPE vec_sumOf = VECTOR_FLOAT_SET1(0); */
  /* t_float scalar_sumVal = 0; */

  { //! Enlarge list if nessseary:
    const uint row_id = chunkSize_index1 + globalStartPos_head;
#include "kt_list_2d__stub__alloc_kvPair.c" //! ie, enlarge if the size is outside the bounary to set.
    assert(row_id < self->list_size);
  }
  {
    const bool isLast_columnForBlock__local = isLast_columnForBlock;  
    //!
    //! Apply logics: 
#include "kt_list_2d__stub__addBlock.c"
  }
  if(isTo_updateSyemmtricRelations && 
     (
      (config_cntMin_afterEach != 0) && (config_cntMin_afterEach != UINT_MAX) 
      &&  
      (config_cntMax_afterEach != 0) && (config_cntMax_afterEach != UINT_MAX)
      )
     ) {
    //fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    /* const bool isLast_columnForBlock__local = false; //! ie, to avoid the sorting-step: */
    /* assert(false); // FIXME: update [”elow] */
    //!
    //! Apply logics: 
#include "kt_list_2d__stub__addBlock__transp.c"
  } else {
    assert(isTo_updateSyemmtricRelations == false); //! ie, what we for cosnsitnecy expect.
  }

  /* //! Merge the sum-values identifed from our SSE-idnetificaiotn with the non-SSE-iterations: */
  /* const t_float scalar_sumVal_alt = VECTOR_FLOAT_storeAnd_horizontalSum(vec_sumOf); */
  //! Update the 'glboal max-val-score':
  //  ((s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)objCaller_postProcessResult)->return_distance += scalar_sumVal +  scalar_sumVal_alt;
}

s_kt_list_2d_kvPair_t get_symmetricSubset__onKeys__s_kt_list_2d_kvPair_filterConfig_t(const s_kt_list_2d_kvPair_t *parent) {
  s_kt_list_2d_kvPair_t self = setToEmpty__s_kt_list_2d_kvPair_t();
  assert(parent);
  if(!parent || !parent->list_size) {return self;}
  //! 
  for(uint row_id = 0; row_id < parent->list_size; row_id++) {
    const uint cnt_tails = parent->list[row_id].current_pos;
    if(cnt_tails == 0) {continue;} //! ie, as no elements are then assotied to 'this'.
    //! 
    bool row_hasVerticesInserted = false;
    for(uint k = 0; k < cnt_tails; k++) {
      const s_ktType_kvPair_t obj_this = parent->list[row_id].list[k];
      if(false == MF__isEmpty__s_ktType_kvPair(obj_this)) {
	const uint tail_id = obj_this.head;
	assert(tail_id != UINT_MAX); 
	bool isA_match = true;
	if(tail_id != row_id) { //! then we 
	  isA_match = false;
	  if(tail_id < parent->list_size) { //! then the parnt maight have knowledge of 'row_id:
	    const uint cnt_tails_tail = parent->list[tail_id].current_pos;
	    for(uint m = 0; m < cnt_tails_tail; m++) {
	      const s_ktType_kvPair_t obj_this_tail = parent->list[tail_id].list[m];
	      const uint tail_id_head = obj_this_tail.head;
	      if(tail_id_head == row_id) {isA_match = true;} //! ie, as the 'symmetric vertex-pair' was then found.
	    }
	  }
	}
	if(isA_match) { //! then we insert the pair
	  row_hasVerticesInserted = true;
	  push__s_kt_list_2d_kvPair_t(&self, row_id, obj_this); //! ie, as the object was symmetric. 
	}
      }
      if(row_hasVerticesInserted == true) {
	self.current_pos = row_id + 1; //! ie, to set an updated iteraiton-point.
      }
    }
  }
  //! 
  //! 
  //! 
  //! 
  //! @return
  return self;
}
