#include "mask_api.h"

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

//! Set the s_mask_api_config_t object to default values.
void setTo_empty__s_mask_api_config(s_mask_api_config_t *self) {
  assert(self);
  //! 
  //! Configruations wrt. data-interpretaiton:
  self->scoreThreshold_min = T_FLOAT_MAX; self->scoreThreshold_max = T_FLOAT_MAX; //! ie, by default no score-thresohdls are applied/used.
  self->typeOf_implictEmptyValue_toSet = e_mask_api_config_typeOf_emptyValue_T_FLOAT_MAX; //! ie, intaited 'a valeu wehre we are always on the safe side': used to 'know' the implict value to set, eg, "0" or T_FLOAT_MAX
  //!
  //! Configruations wrt. SSE time-performance:
  self->typeOf_optimization_SSE = e_mask_api_config_typeOf_SSE_fast;
  self->localConfig_allocateMatrix = true;
}

//! @return teh max-number fo elemtns in the SSE-intri-loop:
#define __local_get_cntIntri_elements(ncols) ({		      \
    uint cnt_elements_intri = ncols - VECTOR_FLOAT_ITER_SIZE; \
    if(((t_float)ncols/(t_float)VECTOR_FLOAT_ITER_SIZE)*((t_float)VECTOR_FLOAT_ITER_SIZE)) { \
    cnt_elements_intri = ncols; \
    } \
    cnt_elements_intri;})



#define __local_cellOF_interest_explicit_minMax(data, isOf_interest, row_id, col_id, scoreTrheashold_min, scoreTrheashold_max) ({ \
	const t_float val1 = data[row_id][col_id];			\
	isOf_interest = (isOf_interest && (val1 > scoreTrheashold_min)); \
	isOf_interest = (isOf_interest && (val1 < scoreTrheashold_max)); \
			 isOf_interest;}) //! ie, return

#define __local_cellOF_interest_explicit(data, mask, row_id, col_id, scoreTrheashold_min, scoreTrheashold_max) ({ \
	bool isOf_interest = mask[row_id][col_id]; \
	__local_cellOF_interest_explicit_minMax(data, isOf_interest, row_id, col_id, scoreTrheashold_min, scoreTrheashold_max);})


//! Explictly update the values in data with emptyValue_toSet for each valeu set to T_FLOAT_MAX
void replace_T_FLOAT_MAX_withNewEmptyValue__mask_api(t_float **data, const uint nrows, const uint ncols, const t_float emptyValue_toSet, const s_mask_api_config_t self) { //__attribute__((always_inline)) {
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      if(data[row_id][col_id] == T_FLOAT_MAX) {
	data[row_id][col_id] = emptyValue_toSet;
      }
    }
  }
}

//! Use the mask-properties in "mask" to set "T_FLOAT_MAX" in the "data" matrix, and then return an updated matrix (where implcit amkss are sued), ie,  for cases where "mask[row_id][column_id] == 0".
//! @return a enw-allcoated matrix with implcit masks (ie, if any).
//! @remarks if score-sthresholds are Not of itnerest, then set the scoreTrheashold_min to the same value as scoreTrheashold_max, eg, to set both parameters to T_FLOAT_MAX
//! @remarks if "mask" is set to "NULL" the we assume the input-matrix 'contains' implict-makss, implict maksses expected to be set to T_FLOAT_MAX
t_float **applyImplicitMask__constructNewMatrix__mask_api(t_float **data, char **mask, const uint nrows, const uint ncols, const s_mask_api_config_t self) {
  //! Comptue:
#define __localConfig__use__uintMask 0
  #include "mask_api__directFunction__implicitMask.c"
#undef __localConfig__use__uintMask
  //! @return the result:
  return new_matrix;
}


//! Use the mask-properties in "mask" to set "T_FLOAT_MAX" in the "data" matrix, and then update "data" with the "T_FLOAT_MAX" for cases where "mask[row_id][column_id] == 0"
void applyImplicitMask_fromUintMask__updateData__mask_api(t_float **data, uint **mask, const uint nrows, const uint ncols, s_mask_api_config_t self) {
  //! Comptue:
#define __localConfig__use__uintMask 1
  self.localConfig_allocateMatrix = false;
  #include "mask_api__directFunction__implicitMask.c"
#undef __localConfig__use__uintMask
  //! @return the result:
  if(self.localConfig_allocateMatrix == false) {
    assert(data == new_matrix);//! ei, as we expec the poitne rhas not changed.
  }
}

//! Use the mask-properties in "mask" to set "T_FLOAT_MAX" in the "data" matrix, and then update "data" with the "T_FLOAT_MAX" for cases where "mask[row_id][column_id] == 0"
//! @remarks if score-sthresholds are Not of itnerest, then set the scoreTrheashold_min to the same value as scoreTrheashold_max, eg, to set both parameters to T_FLOAT_MAX
//! @remarks if "mask" is set to "NULL" the we assume the input-matrix 'contains' implict-makss, implict maksses expected to be set to T_FLOAT_MAX
void applyImplicitMask__updateData__mask_api(t_float **data, char **mask, const uint nrows, const uint ncols, s_mask_api_config_t self) {
  self.localConfig_allocateMatrix = false;
  t_float **data_result = applyImplicitMask__constructNewMatrix__mask_api(data, mask, nrows, ncols, self);
  assert(data == data_result);//! ei, as we expec the poitne rhas not changed.
}

//! 'Translates' a "uint **" boolean mask into a a char-matrix.
//! @remakrs included iot. to be back-compatible with 'old' "cluster.c" library.
char **convert_booleanMatrix_toCharMatrix__mask_api(const uint nrows, const uint ncolumns, uint **mask, const bool isTo_use_continousSTripsOf_memory) {
  assert(nrows > 0); assert(ncolumns > 0); assert(mask); //! ie, what we expect.
  
  //! Allcoate a new mask-matrix:
  const uint default_value_char = 1;
  char **mask_new = allocate_2d_list_char(nrows, ncolumns, default_value_char);

  // FIXME: write a perofmrance-comparison wrt. the overehad of isntrinstincts ... examplfied in/through this funciton .... using the isTo_use_continousSTripsOf_memory parameter.

  if(isTo_use_continousSTripsOf_memory) {
    //! Then we iterate 'trhough the complete list', ie, as we expect all valeus to be 'allocated in one contious chunk':
    const uint cnt_total = nrows * ncolumns;
    const uint i = 0;  //! ie, as we expect 'teh contious strip' astarts at inner-idnex=0.
    uint j = 0;
    const uint cnt_total_intri = VECTOR_CHAR_maxLenght_forLoop(cnt_total);    
    if(cnt_total_intri > 0) {
      for(; j < cnt_total_intri; j += VECTOR_CHAR_ITER_SIZE) {		
	VECTOR_CHAR_storeAnd_add(&mask_new[i][j], VECTOR_CHAR_convertFrom_uint(mask[i], j));
      }
    }
    for(; j < cnt_total; j++) {
      mask_new[i][j] = mask[i][j]; //! ie, a format-conversion
    }
  } else {
    const uint ncols_intri = VECTOR_CHAR_maxLenght_forLoop(ncolumns);
    for(uint i = 0; i < nrows; i++) {
      uint j = 0;      
      if(ncols_intri > 0) {
	// FIXME[ªritcle]: seems like a common programming/coing eror (in our library) is that we use the wrong "< ncols_intri" varialbe ... ie, use the latter as an example of one (of the many) pitfalls/chalelnges in 'suign istnristnicts directly in the soruce-code.
	for(; j < ncols_intri; j += VECTOR_CHAR_ITER_SIZE) {		
	  VECTOR_CHAR_storeAnd_add(&mask_new[i][j], VECTOR_CHAR_convertFrom_uint(mask[i], j));
	}
      }
      for(; j < ncolumns; j++) {
	mask_new[i][j] = mask[i][j]; //! ie, a format-conversion
      }
    }
  }

  //printf("return mask=%p, at %s:%d\n", mask_new, __FILE__, __LINE__);

  //! @return the result:
  return mask_new;
}




//! Identifeis the min-max valeus in a matrix, an evaluation which 'dismisses' the evlauation of mask-values.
//! @remarks if "mask" is set to "NULL" the we assume the input-matrix 'contains' implict-makss: implcit makss are assuemd to 'conform' to the speciaioant in the "self" s_mask_api_config_t input-parameter-object.
//! @remarks in this funciton we make use of the "self.typeOf_implictEmptyValue_toSet" to identify the 'valeus to ignroe' if "scoreThreshold_min == scoreThreshold_max" and if "mask == NULL"
//! @remarks set scalar_max and scalar_min to T_FLOAT_MAX if no valeus are idnietifed wrt. the user-deifned 'search-trehsholds'.
void get_minMax__mask_api(t_float **data, char **mask, const uint nrows, const uint ncols, t_float *scalar_min, t_float *scalar_max, bool *scalar_hasDecimals, s_mask_api_config_t self) {
  //! Configurations:
  const t_float scoreTrheashold_min = self.scoreThreshold_min; const t_float scoreTrheashold_max = self.scoreThreshold_max;
  t_float emptyValue_toSet = T_FLOAT_MAX;
  if(e_mask_api_config_typeOf_emptyValue_0 == self.typeOf_implictEmptyValue_toSet) {emptyValue_toSet = 0;}
  else if(e_mask_api_config_typeOf_emptyValue_T_FLOAT_MAX == self.typeOf_implictEmptyValue_toSet) {emptyValue_toSet = T_FLOAT_MAX;}
  else {assert(false);} //! ie, as we then need to add support for this case.
  // //! Set the SSE-vectors:
  // VECTOR_FLOAT_TYPE vec_spec_emptyValue = VECTOR_FLOAT_SET1(emptyValue_toSet);
  // VECTOR_FLOAT_TYPE vec_threshold_min = VECTOR_FLOAT_SET1(scoreTrheashold_min); VECTOR_FLOAT_TYPE vec_threshold_max = VECTOR_FLOAT_SET1(scoreTrheashold_max);
  // //! SSE-sepcs:
  // const bool isTo_use_sseFast =  ( (ncols > VECTOR_FLOAT_ITER_SIZE * 4) && (self.typeOf_optimization_SSE == e_mask_api_config_typeOf_SSE_fast)); //! ie, if SSE may imrpvoe the eprfomrance then we use 'it'.
  // const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);


  t_float local_min = T_FLOAT_MAX; t_float local_max = T_FLOAT_MIN_ABS;
  assert(data);  assert(nrows); assert(ncols);
  //! The iteration:

  // TODO: consider to add SSE-support in [”elow]:

  t_float sumOf_decimals = 0;

  //! 
  //! The lgoics to idnetify the min-max-value:
  if(mask != NULL) { //! then we assume explcit masks
    if(scoreTrheashold_min == scoreTrheashold_max) { //! then we assume the score-threshodls are not of itnerest
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  if(mask[row_id][col_id]) { //! then the valeu is of interest:
	    if(data[row_id][col_id] < local_min) {local_min = data[row_id][col_id];}
	    if(data[row_id][col_id] > local_max) {local_max = data[row_id][col_id];}	    	    
	    sumOf_decimals += (data[row_id][col_id] - mathLib_float_floor(data[row_id][col_id]));
	  }
	}
      }  
    } else { //! then we apply the score-thresholds:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  const bool isOf_interest = __local_cellOF_interest_explicit(data, mask, row_id, col_id, scoreTrheashold_min, scoreTrheashold_max);
	  if(isOf_interest == true) { //! then the valeu is of interest:
	    if(data[row_id][col_id] < local_min) {local_min = data[row_id][col_id];}
	    if(data[row_id][col_id] > local_max) {local_max = data[row_id][col_id];} 
	    sumOf_decimals += (data[row_id][col_id] - mathLib_float_floor(data[row_id][col_id]));
	  }
	}
      }  
    }
  } else { //! then we assume implict masks are used.
    if(scoreTrheashold_min == scoreTrheashold_max) { //! then we assume the score-threshodls are not of itnerest
      //! Thenw sue a for-loop, ie, as we expec thatat users 'aware' of the possiblty/'topon' to map a 1d-array to ad-matrix would not call this fucniton 'for such a simple case' (eosekth, 06. sept. 2016).
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  if(data[row_id][col_id] != emptyValue_toSet) { //! ie, the valeu is then specifeid as being of interest:
	    assert(data[row_id][col_id] != T_FLOAT_MAX); //! ie, a generalized expectaiton wrt. the applcaiton of this fucntion: not  mandaroy 'criteria', through may indciate erronous use of the "self.typeOf_implictEmptyValue_toSet" proeprty.
	    if(data[row_id][col_id] < local_min) {local_min = data[row_id][col_id];}
	    if(data[row_id][col_id] > local_max) {local_max = data[row_id][col_id];} 
	    sumOf_decimals += (data[row_id][col_id] - mathLib_float_floor(data[row_id][col_id]));
	  } //! else we assuemt eh valeu is implictly specified as 'not being of interest'.
	}
      }
    } else { //! then we apply thes core-trhesholds:
      for(uint row_id = 0; row_id < nrows; row_id++) {
	for(uint col_id = 0; col_id < ncols; col_id++) {
	  bool isOf_interest = true; //! ie, default assumption
	  isOf_interest = __local_cellOF_interest_explicit_minMax(data, isOf_interest, row_id, col_id, scoreTrheashold_min, scoreTrheashold_max);
	  if(isOf_interest == true) {
	    if(data[row_id][col_id] < local_min) {local_min = data[row_id][col_id];}
	    if(data[row_id][col_id] > local_max) {local_max = data[row_id][col_id];} 
	    sumOf_decimals += (data[row_id][col_id] - mathLib_float_floor(data[row_id][col_id]));
	    // data[row_id][col_id] = emptyValue_toSet; //! ie, assign an 'implcit mask' proeprty.
	  }
	}
      }
    }
  }

  //! Handle the 'empty' case:
  if( (local_min == T_FLOAT_MAX) && (local_max == T_FLOAT_MIN_ABS) ) {
    local_max = local_min = T_FLOAT_MAX; //! ie, then sepcify that the value-range is not foudn wrt. the gvien search-critera.
  }

  //! Updat ethe return-scalar-varialbes:
  *scalar_min = local_min;  *scalar_max = local_max;
  *scalar_hasDecimals = (sumOf_decimals != 0); //! ie, where 'floor(value) == value' impleis taht the entity does not 'have' any devcmals (oekseth, 06. okt. 2016).
}


/**
   @brief idnetifies the valeu-range of the data matrix (oekseth, 06. otk. 2016).
   @return an enum-representation of the vlaue-range
 **/
const e_mask_api_valueRangeClassification_t get_valueRange_forMatrix__mask_api(t_float **data, char **mask, const uint nrows, const uint ncols, const s_mask_api_config_t self) {
  t_float scalar_min = T_FLOAT_MAX; t_float scalar_max = T_FLOAT_MIN_ABS; bool scalar_hasDecimals = true;
  get_minMax__mask_api(data, mask, nrows, ncols, &scalar_min, &scalar_max, &scalar_hasDecimals, self);
  
  if(scalar_hasDecimals == false) {
    if(scalar_min >= 0) {
      if( ((char)scalar_max < UCHAR_MAX) ) {return e_mask_api_valueRangeClassification_char_unsigned;}
      else if( ((unsigned short int)scalar_max < USHRT_MAX) ) {return e_mask_api_valueRangeClassification_32_bit_unsigned;}
      else if( ((uint)scalar_max < UINT_MAX) ) {return e_mask_api_valueRangeClassification_32_bit_unsigned;}
      else if( ((long int)scalar_max < ULONG_MAX) ) {return e_mask_api_valueRangeClassification_64_bit_unsigned;}
      else {
	fprintf(stderr, "!!\t Investigate this case, ie, for number=\"%f\", at %s:%d", scalar_max, __FILE__, __LINE__);
	assert(false); 
	return e_mask_api_valueRangeClassification_float; //! ie, as we used "tlfoat" to investigate [ªbove].
      }
    } else {
      if( ((char)scalar_max < CHAR_MAX) &&  ((char)scalar_min > CHAR_MIN) ) {return e_mask_api_valueRangeClassification_char_signed;}
      else if( ((short int)scalar_max < SHRT_MAX)  && ((short int)scalar_min > SHRT_MIN) ) {return e_mask_api_valueRangeClassification_32_bit_signed;}
      else if( ((int)scalar_max < INT_MAX) && ((int)scalar_min > INT_MIN)  ) {return e_mask_api_valueRangeClassification_32_bit_signed;}
      else if( ((long int)scalar_max < LONG_MAX) && ((long int)scalar_min > LONG_MIN) ) {return e_mask_api_valueRangeClassification_64_bit_signed;}
      else {
	fprintf(stderr, "!!\t Investigate this case, ie, for [min, max]=\"[%f, %f]\", at %s:%d", scalar_min, scalar_max, __FILE__, __LINE__);
	assert(false); 
	return e_mask_api_valueRangeClassification_float; //! ie, as we used "tlfoat" to investigate [ªbove].
      }
    }
  } else {
    return e_mask_api_valueRangeClassification_float;
  }
}

//! Identifeis the min-max valeus in a matrix, an evaluation which 'dismisses' the evlauation of mask-values.
//! @remarks if "mask" is set to "NULL" the we assume the input-matrix 'contains' implict-makss: implcit makss are assuemd to 'conform' to the speciaioant in the "self" s_mask_api_config_t input-parameter-object.
//! @remarks in this funciton we make use of the "self.typeOf_implictEmptyValue_toSet" to identify the 'valeus to ignroe' if "scoreThreshold_min == scoreThreshold_max" and if "mask == NULL"
//! @remarks set scalar_max and scalar_min to T_FLOAT_MAX if no valeus are idnietifed wrt. the user-deifned 'search-trehsholds'.
void get_minMax__allValuesOf_interest_mask_api(t_float **data, const uint nrows, const uint ncols, t_float *scalar_min, t_float *scalar_max, s_mask_api_config_t self) {
  // //! Configurations:
  // const t_float scoreTrheashold_min = self.scoreThreshold_min; const t_float scoreTrheashold_max = self.scoreThreshold_max;
  // t_float emptyValue_toSet = T_FLOAT_MAX;
  // if(e_mask_api_config_typeOf_emptyValue_0 == self.typeOf_implictEmptyValue_toSet) {emptyValue_toSet = 0;}
  // else if(e_mask_api_config_typeOf_emptyValue_T_FLOAT_MAX == self.typeOf_implictEmptyValue_toSet) {emptyValue_toSet = T_FLOAT_MAX;}
  // else {assert(false);} //! ie, as we then need to add support for this case.
  // //! Set the SSE-vectors:
  // VECTOR_FLOAT_TYPE vec_spec_emptyValue = VECTOR_FLOAT_SET1(emptyValue_toSet);
  // VECTOR_FLOAT_TYPE vec_threshold_min = VECTOR_FLOAT_SET1(scoreTrheashold_min); VECTOR_FLOAT_TYPE vec_threshold_max = VECTOR_FLOAT_SET1(scoreTrheashold_max);
  //! SSE-sepcs:
  const bool isTo_use_sseFast =  ( (ncols > VECTOR_FLOAT_ITER_SIZE * 4) && (self.typeOf_optimization_SSE == e_mask_api_config_typeOf_SSE_fast)); //! ie, if SSE may imrpvoe the eprfomrance then we use 'it'.
  const uint cnt_elements_intri = __local_get_cntIntri_elements(ncols);

  // FIXME: write a eprfomrance-test to compare this fucntion witht he mask-fucntison ... for the case where all valeus are of itnerest.

  VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_SET1(T_FLOAT_MAX); VECTOR_FLOAT_TYPE vec_max = VECTOR_FLOAT_SET1(T_FLOAT_MIN_ABS);
  t_float local_min = T_FLOAT_MAX; t_float local_max = T_FLOAT_MIN_ABS;
  assert(data);  assert(nrows); assert(ncols);

  for(uint row_id = 0; row_id < nrows; row_id++) {
    uint col_id = 0;
    if(isTo_use_sseFast) {
      for(; col_id < cnt_elements_intri; col_id += VECTOR_FLOAT_ITER_SIZE) {
	VECTOR_FLOAT_TYPE vec_input = VECTOR_FLOAT_LOAD(&data[row_id][col_id]);
	vec_min = VECTOR_FLOAT_MIN(vec_min, vec_input);
	vec_max = VECTOR_FLOAT_MAX(vec_max, vec_input);
      }

      for(; col_id < ncols; col_id++) {
	const t_float val = data[row_id][col_id];
	if(val < local_min) {local_min = val;}
	if(val > local_max) {local_max = val;}
      }
    }  
  }
  const t_float val_SSE_min = VECTOR_FLOAT_storeAnd_horizontalMin(vec_min);
  const t_float val_SSE_max = VECTOR_FLOAT_storeAnd_horizontalMax(vec_max);
  if(val_SSE_max > local_max) {local_max = val_SSE_max;}
  if(val_SSE_min < local_min) {local_min = val_SSE_min;}



  //! Handle the 'empty' case:
  if( (local_min == T_FLOAT_MAX) && (local_max == T_FLOAT_MIN_ABS) ) {
    local_max = local_min = T_FLOAT_MAX; //! ie, then sepcify that the value-range is not foudn wrt. the gvien search-critera.
  }

  //! Updat ethe return-scalar-varialbes:
  *scalar_min = local_min;  *scalar_max = local_max;
}


//! @breif cosntruct a '0|1' matrix from the input-data
//! @return a binary eitehr-or matrix based on the input-data
t_float **construct_binaryMatrix__mask_api(t_float **data, const uint nrows, const uint ncols) {
  assert(data);  assert(nrows); assert(ncols);

  const float default_value_float = 0;
  t_float **result_matrix = allocate_2d_list_float(nrows, ncols, default_value_float);
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      t_float value = data[row_id][col_id];
      if(value != 0) {value = 1;} //! ei, a 'binary tansformation'.
      result_matrix[row_id][col_id] = value;
    }

  }
  return result_matrix;
}

//! @breif cosntruct a '0|1' matrix from the input-data
//! @return a binary eitehr-or matrix based on the input-data
t_float **construct_binaryRow__atIndex__mask_api(t_float **data, const uint index1, const uint nrows, const uint ncols, const bool isTo_storeResult_atIndex0) {
  const float default_value_float = 0;
  t_float **result_matrix = NULL;
  if(isTo_storeResult_atIndex0) {
    uint nrows_local = 1;
    result_matrix = allocate_2d_list_float(nrows_local, ncols, default_value_float);
  } else {
    result_matrix = allocate_2d_list_float(nrows, ncols, default_value_float);
  }
  const uint row_id = index1;
  assert(row_id < nrows);
  const uint row_id_result = (isTo_storeResult_atIndex0) ? 0 : index1;
  for(uint col_id = 0; col_id < ncols; col_id++) {
    t_float value = data[row_id][col_id];
    if(value != 0) {value = 1;} //! ei, a 'binary tansformation'.
    result_matrix[row_id_result][col_id] = value;
  }
  //! @return
  return result_matrix;
}
//! @breif cosntruct a '0|1' row from the input-data
//! @return a binary eitehr-or matrix based on the input-data
t_float *construct_binaryRow__mask_api(const t_float *row, const uint ncols) {
  assert(row); assert(ncols > 0);
  const float default_value_float = 0;
  t_float *result_row = allocate_1d_list_float(ncols, default_value_float);
  for(uint col_id = 0; col_id < ncols; col_id++) {
    t_float value = row[col_id];
    if(value != 0) {value = 1;} //! ei, a 'binary tansformation'.
    result_row[col_id] = value;
  }
  //! @return
  return result_row;
}

//! @breif cosntruct a '0|1' row from the input-data
void construct_binaryRow__updateDirectly__mask_api(t_float *row, const uint ncols) {
  assert(row); assert(ncols > 0);
  for(uint col_id = 0; col_id < ncols; col_id++) {
    t_float value = row[col_id];
    if(value != 0) {value = 1;} //! ei, a 'binary tansformation'.
    row[col_id] = value;
  }
}
