#include "hpLysis_api.h"
#include "hp_clusterFileCollection.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
#include "aux_sysCalls.h"


typedef struct s_clust_foler_1 {
  const char *resultPrefix;
} s_clust_foler_1_t;

static s_clust_foler_1_t init_s_clust_foler_1_t(const char *resultPrefix) {
  s_clust_foler_1_t self;
  self.resultPrefix = resultPrefix;
  //!
  return self;
}

static void _tut_clust_apply(s_clust_foler_1_t *self, const char *filepath, const char *filepath_inFolder) {
  // FIXME: export dot-compatible data.
  // FIXME: ... include CCM-label
  // FIXME: ...
  // FIXME: ...   
  const bool inputMatrix__isAnAdjecencyMatrix = false;
  const uint cnt_Calls_max = 3; const uint arg_npass = 1000;
  //const char *result_file = "test_tut_kCluster.tsv";
  //const uint nrows = 10;     const uint ncols = 10; 
  //const uint nrows = 3;     const uint ncols = 3; 
  //! Note: in [below] we examplify different permtauaions wrt. HCA: algorithm-calls:
  const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_single;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_max;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_average;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__AVG;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_HCA_centroid;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kCluster__SOM;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_disjoint;
  //const e_hpLysis_clusterAlg clusterAlg = e_hpLysis_clusterAlg_kruskal_hca;
  const uint nclusters = UINT_MAX;

  s_kt_matrix_fileReadTuning_t obj_tuneRead = initAndReturn__s_kt_matrix_fileReadTuning_t();
  s_kt_matrix_t obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(/*input_file=*/filepath, obj_tuneRead);
  assert(obj_matrixInput.nrows > 0);
  assert(obj_matrixInput.ncols > 0);

  /* Have a go at the clustering */

  //! Open teh file :
  allocOnStack__char__sprintf(2000, result_file, "%sclust_result-%s-%s", self->resultPrefix, get_stringOf__short__e_hpLysis_clusterAlg_t(clusterAlg), filepath_inFolder);
  FILE *file_out = fopen(result_file, "wb");
  //FILE *file_out = stdout;
  if(file_out == NULL) {fprintf(stderr, "!!\t Unable to open file=\"%s\" (for writing): please investigate (eg, answering quesitosn such as 'does at folder=\"%s\" exists?). Observiaotn at [%s]:%s:%d\n", result_file, self->resultPrefix, __FUNCTION__, __FILE__, __LINE__); assert(false);}

  //!
  //! Make several calls to the k-means clsuteirng, ie, to evlauate wrt. convergence:
  //for(uint cnt_Calls = 0; cnt_Calls < cnt_Calls_max; cnt_Calls++) {
  {
    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);

    // (Symmetric) adjacency matrix                                                                                                                            
    //obj_hp.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = inputMatrix__isAnAdjecencyMatrix;
    //obj_hp.config.corrMetric_prior_use = true; //! ie, use data-as-is.
    /* if(false) { */
    /*   //! Specify that we are to use the k-means++-implmeantion defined/used in the "fast_Kmeans" software: */
    /*   obj_hp.randomConfig.config_init.typeOf_randomNess = e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss; //! where the latter enum is define d in our "math_generateDistribution.h" */
    //}
    
    /* Expect 2 clear clusters */
    // printf("## npass=%u, at %s:%d\n", arg_npass, __FILE__, __LINE__);
    const bool is_also_ok = cluster__hpLysis_api (
						  &obj_hp, clusterAlg, &obj_matrixInput,
						  /*nclusters=*/ nclusters, /*npass=*/ arg_npass
						  );
    assert(is_also_ok);


    //! Export the vertex-clsuterId-memberships:
    printf("\tExports(results)\t to-file\t%s\t at: \t %s:%d\n", result_file, __FILE__, __LINE__);
    bool is_ok_e = export__hpLysis_api(&obj_hp, /*do-not-open-file => stringOf_file=*/NULL, /*file_out=*/file_out, /*exportFormat=*/e_hpLysis_export_formatOf_clusterResults_vertex_toCentroidIds, &obj_matrixInput);
    assert(is_ok_e);

    //! De-allocates the "s_hpLysis_api_t" object.
    free__s_hpLysis_api_t(&obj_hp);	    
  }


  //!
  //! Close the file and the amtrix:
  assert(file_out); 
  if(file_out != stdout) {fclose(file_out); file_out = NULL;}
  free__s_kt_matrix(&obj_matrixInput);
}

/**
   @brief a generic approach to delete all files in a folder (oekseth, 06. feb. 2017)
   @param <pathToFolder> is the folder to remove files from
   @return true upon scucess
   @remarks the motivaiton behind 'this funciotn' is the observiaotn that (a) the "rm folder/\*" command is unable to work for large direcotry-setss and (b) to avoid our appraoch to be strongly biased towards Linux/Bsh. 
**/
static bool _tut_clust_iterate_forFolder(s_clust_foler_1_t *self, const char *pathToFolder) {
  if(!pathToFolder || !strlen(pathToFolder)) {return false;} //! ie, as the path was Not prodived.
  const int result = access(pathToFolder, W_OK);
  if (result != 0) {
    fprintf(stderr, "!!\t Unable to write to folder=\"%s\", ie, please vliadate the altter. Observaiton at [%s]:%s:%d\n", pathToFolder, __FUNCTION__, __FILE__, __LINE__);
    //assert(false);
    return false; //! ie, errornous path.
  } else {     //! 'Empty' the current oflder:      
    /* {  */
    /*   char str[2000]; sprintf(str, "find %s -name \"*\" -print0 | xargs -0 rmrm -f %%s*", self->pathTo_localFolder_storingEachSimMetric);   */
    /* 	  //char str[2000]; sprintf(str, "rm -f %s*", self->pathTo_localFolder_storingEachSimMetric);   */
    /* 	  fprintf(stderr, "(info)\t Clears the file-content in the direotyr to store resutls in \"%s\", at %s:%d\n", str, __FILE__, __LINE__); */
    /* 	  system(str); } */
    /*   } */
    //! 
    //! Src: "http://stackoverflow.com/questions/11007494/how-to-delete-all-files-in-a-folder-but-not-delete-the-folder-using-nix-standar"
    DIR *theFolder = opendir(pathToFolder);
    struct dirent *next_file;
    char filepath[256];
    
    const char last_char_inDir = pathToFolder[strlen(pathToFolder)-1];
    
    while ( (next_file = readdir(theFolder)) != NULL )  {
      if (0==strcmp(next_file->d_name, ".") || 0==strcmp(next_file->d_name, "..")) { continue; } //! ie, to avoid deleitng 'refernce-pointers':
      // build the path for each file in the folder      
      if(last_char_inDir == '/') {
	sprintf(filepath, "%s%s", pathToFolder, next_file->d_name);
      } else {
	sprintf(filepath, "%s/%s", pathToFolder, next_file->d_name);
      }
      printf("\tCluster: %s, at %s:%d\n", filepath, __FILE__, __LINE__);
      _tut_clust_apply(self, filepath, next_file->d_name);
    }
    closedir(theFolder);
    return true;
  }
}


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy

/**
   @brief clusters data from a given directory.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2020).
   @remarks compile: 
   -- g++ -O2 -g tut_clust-from-folder-1-hca.c -L . -l lib_x_hpLysis -Wl,-rpath=. -fPIC -o tut_1.exe
   @remarks Demonstrates features wrt.:
   -- examplify the use of a 'advanced inptu-file specificaiton' through the "s_kt_matrix_fileReadTuning_t" into hpLysis-clustering.  (oekseth, 06. feb. 2017).   
   -- input: use of logics in "s_kt_matrix_t" to load input-files: examplfies both a 'direct' speicficaiton of attributes, and a geneirc stragty using lgoics and data-strucutres deifned in our "hp_clusterFileCollection.h"
   -- computation: use of different correlation/simliarty metrics
   -- result(a): how results from mmuliple clusterings may be exported into one single-unified java-script file.
   -- result(b): an iterative appraoch to print out cluster-conistnecy-scores when compating an input-amtrix to a godl-standard-set of clusters:
   @remarks pertmaution of tut="tut_inputFile.c"
**/
int main(const int argc, char * argv[]) 
#else
  int tut_clust_from_folder_1_hca(const int argc, char * argv[]) 
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
  //! *************************************************************************
    //! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalInit__kt_api();
  //! *************************************************************************
#endif



  // FIXME: rewrite below:

  //! Print details of usage:
  if(argc < 3) {
    fprintf(stderr, "Usage\t %s\t%s\t%s\n", argv[0], "input-file", "result-file");
    return -1;
  } else if(argc > 3) {
    fprintf(stderr, "!! \t Error: \tExpeccted '3' arguments, while you provided '%d' arguments. Usage\t %s\t%s\t%s\n", argc, argv[0], "input-directory", "result-file-prefix");
    return -1;
  }
  const char *input_dir    = argv[1];
  const char *resultPrefix = argv[2];
  printf("resultPrefix:%s\n", resultPrefix);
  //!
  //!
  s_clust_foler_1_t self = init_s_clust_foler_1_t(resultPrefix);
  _tut_clust_iterate_forFolder(&self, input_dir);
  //_tut_clust_apply(&self, input_dir);


  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------
  //! -----------------------------------------------------------------------------------------------------------------------------------


  //!
  //! @return
#ifndef __M__calledInsideFunction
  ////! *************************************************************************
  ////! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
  hpLysis__globalFree__kt_api();
  ////! *************************************************************************  
  return true;
#endif
}
