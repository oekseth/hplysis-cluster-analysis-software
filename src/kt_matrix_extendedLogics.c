#include "kt_matrix_extendedLogics.h"


//! Initates and return an object using random integer (uint) values (oekseth, 06. jan. 2017)
s_kt_matrix_t initAndReturn__randomUint__s_kt_matrix_extendedLogics(const uint nrows, const uint ncols, const uint maxValue, const e_kt_randomGenerator_type_t typeOf_randomNess, const bool isTo_initIntoIncrementalBlocks) {
  s_kt_matrix_t self; 
  if(!nrows || !ncols|| !maxValue) {
    fprintf(stderr, "!!\t Inptu-params not as expected: pelase investigate. OBservation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    setTo_empty__s_kt_matrix_t(&self); 
    assert(false); //! ie, an heads-up
    return self;
  }
  assert(nrows > 0); assert(ncols > 0);
  //! Intiate:
  init__s_kt_matrix(&self, nrows, ncols, false);
  assert(self.matrix);
  s_kt_matrix_base_t obj_matrix_input = initAndReturn__empty__s_kt_matrix_base_t(); //initAndReturn__notAllocate__s_kt_matrix_base_t(obj_1->nrows, obj_1->ncols, obj_1->matrix);
  s_kt_randomGenerator_vector_t obj_rand = initAndReturn__s_kt_randomGenerator_vector_t(maxValue, ncols, typeOf_randomNess, isTo_initIntoIncrementalBlocks, &obj_matrix_input);
  //!
  //! Set random values:
  for(uint row_id = 0; row_id < nrows; row_id++) {
    //!
    //! Re-compute randomness::
    randomassign__speicficType__math_generateDistributions(&obj_rand);
    assert(obj_rand.mapOf_columnToRandomScore);
    assert(self.matrix[row_id]);
    //!
    //! Update return-object:
    for(uint col_id = 0; col_id < ncols; col_id++) {
      assert(obj_rand.mapOf_columnToRandomScore[col_id] != UINT_MAX);
      self.matrix[row_id][col_id] = (t_float)obj_rand.mapOf_columnToRandomScore[col_id];
    }    
  }

  //! De-allocate
  free__s_kt_randomGenerator_vector_t(&obj_rand);

  //! @reutrn
  return self;
}
