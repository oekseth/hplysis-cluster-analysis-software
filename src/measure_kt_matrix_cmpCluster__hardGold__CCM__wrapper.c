      //!
      //! Investgiate the 'impact' for different cases:
      //! 
      //! Intiate result-object:
      const uint empty_0 = 0;
      const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt*nrows_steps_cnt;
      uint *obj_resultCollection_goldCaseClustersAndRows__currentPos = allocate_1d_list_uint(obj_size, empty_0);
      s_kt_matrix_setOf_t obj_resultCollection_goldCaseClustersAndRows = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
      //!
      //! Iterate:
      for(uint row_case = 0; row_case < nrows_steps_cnt; row_case++) {
	uint nrows_total_tmp = nrows_total_min + row_case*nrows_steps_multEach;
	/* { //! Ensure that there are more vertices than clsuters, ie, as otehrwise would be odd: */
	/*   const uint clusterCnt_case = clusterCnt_steps_cnt; */
	/*   const uint max_cnt_Clusters = (clusterCnt_total_min + clusterCnt_case * clusterCnt_steps_multEach); */
	/*   if(nrows_total_tmp < max_cnt_Clusters) {nrows_total_tmp = max_cnt_Clusters*1.2;} */
	/* } */
	const uint nrows_total = nrows_total_tmp;
	assert(nrows_total > 0);
	
	//! 
	//!
	const uint clusterCnt_steps_multEach = nrows_total / clusterCnt_steps_cnt;
	assert(clusterCnt_steps_multEach >= 1);
	//! 
	//! Intiate result-object:
	const uint empty_0 = 0;
	const uint obj_size = cnt_cases*cnt_cases*clusterCnt_steps_cnt;
	uint *obj_resultCollection_goldCaseClusters__currentPos = allocate_1d_list_uint(obj_size, empty_0);
	s_kt_matrix_setOf_t obj_resultCollection_goldCaseClusters = init_resultObject__forAllCmpMetrics__kt_matrix_cmpCluster(/*cnt_datasets=*/obj_size);
	//!
	//! Iterate:
	for(uint clusterCnt_case = 1; clusterCnt_case <= clusterCnt_steps_cnt; clusterCnt_case++) {
	  const uint cnt_clusters_total = clusterCnt_total_min + clusterCnt_case * clusterCnt_steps_multEach;
	  if(cnt_clusters_total > nrows_total) {continue;} //! ie, as we then assume m'this case' has alreayd been evlauated.
	  assert(cnt_clusters_total <= nrows_total); //! assumption: there are more vertices than clsuters, ie, as otehrwise would be odd.
	  char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s.n_%u.c_%u", stringOf_measurement_base, nrows_total, cnt_clusters_total);
	  //!
	  //! Apply logics and write out:
#include "measure_kt_matrix_cmpCluster__hardGold__CCM.c"
	  /* #include "measure_kt_matrix_cmpCluster__hardGold__writeOut.c" */
	}
	{ //!
	  //! Write out: 
	  char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s.n_%u", stringOf_measurement_base, nrows_total);
	  char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__%s.%s", resultDir_path, dataBlock_currentPos__casesOf_nrows++, row_case, stringOf_measurement, "2xGoldCaseAndCluster"); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads    
	  //! Note: in [”elow] we obht exprots and de-allocates:
	  exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClusters, path_w_dir, &obj_resultCollection_goldCaseClustersAndRows, obj_resultCollection_goldCaseClustersAndRows__currentPos, typeOf_extractToPrint);
	  free_1d_list_uint(&obj_resultCollection_goldCaseClusters__currentPos);  obj_resultCollection_goldCaseClusters__currentPos = NULL;
	}
      }
      { //!
	//! Write out: 
	char stringOf_measurement[2000] = {'\0'}; sprintf(stringOf_measurement, "%s", stringOf_measurement_base);
	char path_w_dir[2000] = {'\0'}; sprintf(path_w_dir, "%sid_%u_%u__%s.%s", resultDir_path, cnt_datasetsGlobal__current -1, /*col=*/0, stringOf_measurement, "2xGoldCaseAndClusterAndRows"); //! where "dataBlock_currentPos, cnt_cases, " is used as an 'unique id', ie, when construbiotn a WWW-heatmap for the different matrix-score-spreads    
	//! Note: in [”elow] we obht exprots and de-allocates:
	exportResult__andDeAllocate__kt_matrix_cmpCluster(&obj_resultCollection_goldCaseClustersAndRows, path_w_dir, &obj_resultCollection_goldCaseClustersGlobal, obj_resultCollection_goldCaseClustersGlobal__currentPos, typeOf_extractToPrint);
	free_1d_list_uint(&obj_resultCollection_goldCaseClustersAndRows__currentPos);  obj_resultCollection_goldCaseClustersAndRows__currentPos = NULL;
      }
      //!
      //! 'Release' variables:
#undef __Mi__init__in
#undef __Mi__init__out
#undef __Mi__buildSample__in
#undef __Mi__buildSample__out
