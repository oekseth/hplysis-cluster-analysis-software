#ifndef hp_distance_wrapper_h
#define hp_distance_wrapper_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_distance_wrapper
   @brief a wrapper to our "hp_distance.h" (oekseth, 06. mar. 2017).
   @remarks is used iot. solve 'oterhwise' cyclic code-depdencneids betwen our "math_generateDistribution.h", our "hp_distance.h" and our "kt_matrix.h"
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
**/

#include "kt_matrix.h"
#include "hp_distance.h"

/**
   @brief Compute the simlairty-metric and return the result (eosketh, 06. mar. 2017).
   @return the result-simliarty-matrix.
   @remarks is a wrapper-function to our "apply__hp_distance(..)".
**/
s_kt_matrix_t applyAndReturn__altTypeAsInput__hp_distance_wrapper(const s_kt_correlationMetric_t obj_metric, const s_kt_matrix_t *obj_1, const s_kt_matrix_t *obj_2, const s_hp_distance__config_t local_config);

#endif //! EOF
