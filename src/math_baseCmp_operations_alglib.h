#ifndef math_baseCmp_operations_alglib_h
#define math_baseCmp_operations_alglib_h

/*************************************************************************
ALGLIB 3.10.0 (source code generated 2015-08-19)
Copyright (c) Sergey Bochkanov (ALGLIB project).

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the hpLysis documentation 
the Free Software Foundation (www.fsf.org); either version 2 of the 
License, or 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
hpLysis documentation for more details.

A copy of the hpLysis documentation is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "configure_cCluster.h"
#include "correlation_macros__distanceMeasures.h"

#define ae_machineepsilon T_FLOAT_MIN
//#define ae_machineepsilon 5E-16
#define ae_maxrealnumber  1E300
#define ae_minrealnumber  1E-300
#define ae_pi 3.1415926535897932384626433832795
typedef char ae_bool;
#define ae_true 1
#define ae_false 0
typedef int ae_int_t;
typedef int ae_int32_t;
typedef long int ae_int64_t;
typedef ae_int_t ae_datatype;

/* compiler-specific definitions */
/* #if AE_COMPILER==AE_MSVC */
/* #define ALIGNED __declspec(align(8)) */
/* #elif AE_COMPILER==AE_GNUC */
#define ALIGNED __attribute__((aligned(8)))
/* #else */
/* #define ALIGNED */
/* #endif */


/************************************************************************
x-string (zero-terminated):
    owner       OWN_CALLER or OWN_AE. Determines what to do on realloc().
                If vector is owned by caller, X-interface  will  just set
                ptr to NULL before realloc(). If it is  owned  by  X,  it
                will call ae_free/x_free/aligned_free family functions.

    last_action ACT_UNCHANGED, ACT_SAME_LOCATION, ACT_NEW_LOCATION
                contents is either: unchanged, stored at the same location,
                stored at the new location.
                this field is set on return from X.

    ptr         pointer to the actual data

Members of this structure are ae_int64_t to avoid alignment problems.
************************************************************************/
typedef struct
{
    ALIGNED ae_int64_t     owner;
    ALIGNED ae_int64_t     last_action;
    ALIGNED char *ptr;
} x_string;

/************************************************************************
x-vector:
    cnt         number of elements

    datatype    one of the DT_XXXX values

    owner       OWN_CALLER or OWN_AE. Determines what to do on realloc().
                If vector is owned by caller, X-interface  will  just set
                ptr to NULL before realloc(). If it is  owned  by  X,  it
                will call ae_free/x_free/aligned_free family functions.

    last_action ACT_UNCHANGED, ACT_SAME_LOCATION, ACT_NEW_LOCATION
                contents is either: unchanged, stored at the same location,
                stored at the new location.
                this field is set on return from X interface and may be
                used by caller as hint when deciding what to do with data
                (if it was ACT_UNCHANGED or ACT_SAME_LOCATION, no array
                reallocation or copying is required).

    ptr         pointer to the actual data

Members of this structure are ae_int64_t to avoid alignment problems.
************************************************************************/
typedef struct
{
    ALIGNED ae_int64_t     cnt;
    ALIGNED ae_int64_t     datatype;
    ALIGNED ae_int64_t     owner;
    ALIGNED ae_int64_t     last_action;
    ALIGNED void *ptr;
} x_vector;



typedef struct ae_dyn_block
{
    struct ae_dyn_block * volatile p_next;
    /* void *deallocator; */
    void (*deallocator)(void*);
    void * volatile ptr;
} ae_dyn_block;

/************************************************************************
frame marker
************************************************************************/
typedef struct ae_frame
{
    ae_dyn_block db_marker;
} ae_frame;


/* void ae_frame_make(ae_frame *tmp); */
/* void ae_frame_leave(); */

typedef struct { t_float x, y; } ae_complex;

typedef struct ae_vector
{
    /*
     * Number of elements in array, cnt>=0
     */
    ae_int_t cnt;
    
    /*
     * Either DT_BOOL, DT_INT, DT_REAL or DT_COMPLEX
     */
    ae_datatype datatype;
    
    /*
     * If ptr points to memory owned and managed by ae_vector itself,
     * this field is ae_false. If vector was attached to x_vector structure
     * with ae_vector_attach_to_x(), this field is ae_true.
     */
    ae_bool is_attached;
    
    /*
     * ae_dyn_block structure which manages data in ptr. This structure
     * is responsible for automatic deletion of object when its frame
     * is destroyed.
     */
    ae_dyn_block data;
    
    /*
     * Pointer to data.
     * User usually works with this field.
     */
    union
    {
        void *p_ptr;
        ae_bool *p_bool;
        ae_int_t *p_int;
        t_float *p_t_float;
        ae_complex *p_complex;
    } ptr;
} ae_vector;


void ae_vector_init(ae_vector *dst, ae_int_t size, ae_datatype datatype);
void ae_vector_init_copy(ae_vector *dst, ae_vector *src);
void ae_vector_init_from_x(ae_vector *dst, x_vector *src);
void ae_vector_attach_to_x(ae_vector *dst, x_vector *src);
ae_bool ae_vector_set_length(ae_vector *dst, ae_int_t newsize);
void ae_vector_clear(ae_vector *dst);
void ae_vector_destroy(ae_vector *dst);
void ae_swap_vectors(ae_vector *vec1, ae_vector *vec2);




//! ********************************
#if (configure_performance__alglibCmp__useMacrosinsteadOfFunctions == 1)
//! Then we use knitting-Tools fast fuctniosn for comparison:


#define ae_fp_eq(v1, v2) ({ v1 == v2; })
#define ae_fp_neq(v1, v2) ({ v1 != v2 ; })
#define ae_fp_less(v1, v2) ({ v1 < v2; })
#define ae_fp_less_eq(v1, v2) ({ v1 <= v2 ; })
#define ae_fp_greater(v1, v2) ({ v1 > v2; })
#define ae_fp_greater_eq(v1, v2) ({ v1 >= v2; })

#define  ae_fabs(x) ({ mathLib_float_abs(x); })
			  //ae_int_t ae_iabs(ae_int_t x) ({ ; })
#define   ae_sqr(x) ({ x * x; })
#define   ae_sqrt(x) ({ mathLib_float_sqrt(x); })
#define   ae_sign(x) ({ (x > 0) ? 1 : (x < 0) ? -1 : 0; })
#define   ae_round(x) ({ mathLib_float_floor(x + 0.5); })
#define   ae_trunc(x) ({ x>0 ? ae_ifloor(x) : ae_iceil(x)); })
#define   ae_ifloor(x) ({mathLib_float_floor(x); })
#define   ae_iceil(x) ({mathLib_float_ceil(x); })

#define   ae_maxint(m1, m2) ({ macro_max(m1, m2); })
#define   ae_minint(m1, m2) ({ macro_min(m1, m2); })
#define   ae_maxreal(m1, m2) ({macro_max(m1, m2) ; })
#define   ae_minreal(m1, m2) ({ macro_min(m1, m2); })
#define   ae_randomreal() ({ \
  const int i1 = rand(); int i2 = rand(); \
  const t_float mx = (t_float)(RAND_MAX)+1.0; const t_float tmp0 = i2/mx; \
  const t_float tmp1 = i1+tmp0;						\
  t_float result = tmp1/mx;						\
  result;}) //! ie, return.
#define ae_randominteger(maxv) ({ rand()%maxv; })

#define  ae_sin(x) ({ mathLib_float_sin(x); })
#define  ae_cos(x) ({ mathLib_float_cos(x); })
#define  ae_tan(x) ({ mathLib_float_tan(x); })
#define  ae_sinh(x) ({ mathLib_float_sinh(x); })
#define  ae_cosh(x) ({ mathLib_float_cosh(x); })
#define  ae_tanh(x) ({ mathLib_float_tanh(x); })
#define  ae_asin(x) ({ mathLib_float_asin(x); })
#define  ae_acos(x) ({ mathLib_float_acos(x); })
#define  ae_atan(x) ({ mathLib_float_atan(x); })
#define  ae_atan2(y, x) ({ mathLib_float_atan2(y, x); })

#define  ae_log(x) ({ mathLib_float_log(x); })
#define  ae_pow(x, y) ({ mathLib_float_pow(x); })
#define  ae_exp(x) ({ mathLib_float_exp(x); })

#define sign(x) ({ ae_sign(x); })
#define randomreal() ({ ae_randomreal(); })
#define randominteger(maxv) ({ ae_randominteger(maxv); })
#define round(x) ({ ae_round(x); })
#define trunc(x) ({ ae_trunc(x); })
#define ifloor(x) ({ ae_ifloor(x); })
#define iceil(x) ({ ae_iceil(x); })
#define pi() ({ PI; })
#define sqr(x) ({ ae_sqr(x); })
#define maxint(m1, m2) ({ ae_maxint(m1, m2); })
#define minint(m1, m2) ({ ae_minint(m1, m2); })
#define maxreal(m1, m2) ({ ae_maxreal(m1, m2); })
#define minreal(m1, m2) ({ ae_minreal(m1, m2); })

#define fp_eq(v1, v2) ({ v1 == v2; })
#define fp_neq(v1, v2) ({ v1 != v2; })
#define fp_less(v1, v2) ({ v1 < v2 ; })
#define fp_less_eq(v1, v2) ({ v1 <= v2; })
#define fp_greater(v1, v2) ({ v1 > v2; })
#define fp_greater_eq(v1, v2) ({ v1 >= v2; })

#define fp_isnan(x) ({ isnan(x); })
/* #define fp_isposinf(x) ({ ; }) */
/* #define fp_isneginf(x) ({ ; }) */
#define fp_isinf(x) ({ isinf(x); })
#define fp_isfinite(x) ({ isfinite(x); })

//! -----------------------------------------------------------------
#else //! then we use ALGLIBs slow funciton-calls:

ae_bool ae_fp_eq(t_float v1, t_float v2);
ae_bool ae_fp_neq(t_float v1, t_float v2);
ae_bool ae_fp_less(t_float v1, t_float v2);
ae_bool ae_fp_less_eq(t_float v1, t_float v2);
ae_bool ae_fp_greater(t_float v1, t_float v2);
ae_bool ae_fp_greater_eq(t_float v1, t_float v2);

ae_bool ae_isfinite_stateless(t_float x, ae_int_t endianness);
ae_bool ae_isnan_stateless(t_float x,    ae_int_t endianness);
ae_bool ae_isinf_stateless(t_float x,    ae_int_t endianness);
ae_bool ae_isposinf_stateless(t_float x, ae_int_t endianness);
ae_bool ae_isneginf_stateless(t_float x, ae_int_t endianness);

ae_int_t ae_get_endianness();

ae_bool ae_isfinite(t_float x);
ae_bool ae_isnan(t_float x);
ae_bool ae_isinf(t_float x);
ae_bool ae_isposinf(t_float x);
ae_bool ae_isneginf(t_float x);

t_float   ae_fabs(t_float x);
ae_int_t ae_iabs(ae_int_t x);
t_float   ae_sqr(t_float x);
t_float   ae_sqrt(t_float x);

ae_int_t ae_sign(t_float x);
ae_int_t ae_round(t_float x);
ae_int_t ae_trunc(t_float x);
ae_int_t ae_ifloor(t_float x);
ae_int_t ae_iceil(t_float x);

ae_int_t ae_maxint(ae_int_t m1, ae_int_t m2);
ae_int_t ae_minint(ae_int_t m1, ae_int_t m2);
t_float   ae_maxreal(t_float m1, t_float m2);
t_float   ae_minreal(t_float m1, t_float m2);
t_float   ae_randomreal();
ae_int_t ae_randominteger(ae_int_t maxv);

t_float   ae_sin(t_float x);
t_float   ae_cos(t_float x);
t_float   ae_tan(t_float x);
t_float   ae_sinh(t_float x);
t_float   ae_cosh(t_float x);
t_float   ae_tanh(t_float x);
t_float   ae_asin(t_float x);
t_float   ae_acos(t_float x);
t_float   ae_atan(t_float x);
t_float   ae_atan2(t_float y, t_float x);

t_float   ae_log(t_float x);
t_float   ae_pow(t_float x, t_float y);
t_float   ae_exp(t_float x);

int sign(t_float x);
t_float randomreal();
ae_int_t randominteger(ae_int_t maxv);
//int round(t_float x);
//int trunc(t_float x);
int ifloor(t_float x);
int iceil(t_float x);
t_float pi();
t_float sqr(t_float x);
int maxint(int m1, int m2);
int minint(int m1, int m2);
t_float maxreal(t_float m1, t_float m2);
t_float minreal(t_float m1, t_float m2);

bool fp_eq(t_float v1, t_float v2);
bool fp_neq(t_float v1, t_float v2);
bool fp_less(t_float v1, t_float v2);
bool fp_less_eq(t_float v1, t_float v2);
bool fp_greater(t_float v1, t_float v2);
bool fp_greater_eq(t_float v1, t_float v2);

bool fp_isnan(t_float x);
bool fp_isposinf(t_float x);
bool fp_isneginf(t_float x);
bool fp_isinf(t_float x);
bool fp_isfinite(t_float x);
//! -----------------------------------------------------------------
#endif //! enfif(ALGIB-slwo-fucntions)


t_float nulog1p(t_float x);
t_float nuexpm1(t_float x);
t_float nucosm1(t_float x);


#endif //! EOF 
