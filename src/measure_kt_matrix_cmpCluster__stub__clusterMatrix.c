{ //! Compute simliarty-metric and export:	    
  //!
  //! Start time-measurement:
  start_time_measurement();	      
  s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, /*&fileHandler__localCorrMetrics=*/NULL);
  hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
  char string_local__corr_prior[2000] = {'\0'}; sprintf(string_local__corr_prior, "%sid_%u_%u__case_%u.%s.m_%s_.pre_%u.tag_%s.matrix.tsv", resultDir_path, cnt_counter_y, cnt_counter_x++, data_id, "syntethic", get_stringOf_enum__e_kt_correlationFunction_t(dist_metric_id), dist_enum_preStep, data_tag);
  hp_config.stringOfResultPrefix__exportCorr__prior = string_local__corr_prior;
  hp_config.config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
  //!
  //! Correlate and export:
  const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
  assert(is_ok);
  //!
  //! End time-measurement:
  const t_float timeInSec = end_time_measurement(/*msg=*/string_local__corr_prior, /*prev_time_inSeconds=*/FLT_MAX);
  assert(mapOf_time__pos < groupMedium__cnt_dataSets);
  assert(mapOf_time__pos < groupMedium__cnt_dataSets); assert((uint)metric_isInGroup < groupMedium__cnt_simMetrics);
  //!		
  { //! Update the matrix-resutls using the 'groups' to 'hold' the memberships:
    const t_float valueToUpdate = timeInSec;
    matResult__execTime__min.matrix[metric_isInGroup][mapOf_time__pos] 
      = macro_min(matResult__execTime__min.matrix[metric_isInGroup][mapOf_time__pos], valueToUpdate);
    if(matResult__execTime__max.matrix[metric_isInGroup][mapOf_time__pos] != T_FLOAT_MAX) {
      matResult__execTime__max.matrix[metric_isInGroup][mapOf_time__pos] 
	= macro_max(matResult__execTime__min.matrix[metric_isInGroup][mapOf_time__pos], valueToUpdate);
    } else {matResult__execTime__max.matrix[metric_isInGroup][mapOf_time__pos] = valueToUpdate; }
    if(matResult__execTime__avg.matrix[metric_isInGroup][mapOf_time__pos] != T_FLOAT_MAX) {
      matResult__execTime__max.matrix[metric_isInGroup][mapOf_time__pos] += valueToUpdate;
    } else { matResult__execTime__max.matrix[metric_isInGroup][mapOf_time__pos] = valueToUpdate; }
  }
  //! ---------------------------------------------
  //! De-allocates the "s_hpLysis_api_t" object.
  free__s_hpLysis_api_t(&hp_config);	
}
//! -------------------------------
{ //! Comptue clusters:
  //!
  //! 
  //!
  //! Find the 'case' with the longest esuection-time:
  assert(config_kMeans__cntIterations >= 1);
  t_float timeInSec = 0;
  s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, /*&fileHandler__localCorrMetrics=*/NULL);
  for(uint cnt_test = 0; cnt_test < config_kMeans__cntIterations; cnt_test++) {
    //! Start time-measurement:
    start_time_measurement();	      
    s_hpLysis_api_t hp_config_local = setToEmpty__s_hpLysis_api_t(NULL, /*&fileHandler__localCorrMetrics=*/NULL);
    hp_config_local.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
    //char string_local__corr_prior[2000] = {'\0'}; sprintf(string_local__corr_prior, "%sid_%u_%u__case_%u.%s.m_%s_.pre_%u.tag_%s.matrix.tsv", resultDir_path, cnt_counter_y, cnt_counter_x++, data_id, "syntethic", get_stringOf_enum__e_kt_correlationFunction_t(dist_metric_id), dist_enum_preStep, data_tag);
    const char *string_local__corr_prior = NULL;
    hp_config_local.stringOfResultPrefix__exportCorr__prior = string_local__corr_prior;
    hp_config_local.config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
    //!
    //! Correlate and export:
    assert(obj_dataConfig.k_clusterCount != 0); assert(obj_dataConfig.k_clusterCount != UINT_MAX);
    const uint nclusters = obj_dataConfig.k_clusterCount;
    const bool is_also_ok = cluster__hpLysis_api (
						  &hp_config_local, config__clusterAlg, &obj_matrixInput,
						  /*nclusters=*/ nclusters, /*npass=*/ config_arg_npass
						  );
    assert(is_also_ok);
    //!
    //! End time-measurement:
    const t_float timeInSec_local = end_time_measurement(/*msg=*/string_local__corr_prior, /*prev_time_inSeconds=*/FLT_MAX);
    if(timeInSec_local >= timeInSec) { //! ie, a 'simple seleciton-criteira'.
      timeInSec = timeInSec_local;
      hp_config = hp_config_local;
    } else {free__s_hpLysis_api_t(&hp_config_local);}
  }
  //!
  //! Use the clsuter-results:
  const s_kt_matrix_t matrixOf_coOiccurence_1 = hp_config.matrixResult__inputToClustering;
  uint *mapOf_vertexToCentroid1 = hp_config.obj_result_kMean.vertex_clusterId;
  assert(mapOf_vertexToCentroid1); //! ie, as we expect 'this' was intaited.
  assert(hp_config.obj_result_kMean.cnt_vertex > 0);
  assert(mapOf_time__pos < groupMedium__cnt_dataSets);
  assert(mapOf_time__pos < groupMedium__cnt_dataSets); assert((uint)metric_isInGroup < groupMedium__cnt_simMetrics);
  { //! Export the cluster-mappings to a result-file ... for visual/manual inspection.
    assert(fileDesriptorOut__clusterResults);
    char string_local__corr_prior[2000] = {'\0'}; sprintf(string_local__corr_prior, "%sid_%u_%u__case_%u.%s.m_%s_.pre_%u.tag_%s.matrix.tsv", resultDir_path, cnt_counter_y, cnt_counter_x++, data_id, "syntethic", get_stringOf_enum__e_kt_correlationFunction_t(dist_metric_id), dist_enum_preStep, data_tag);

    fprintf(fileDesriptorOut__clusterResults, "\n#! Export data=\"%s\" given sim-metric=\"%s\" and dist-enum-prestep='%u':\n", data_tag, get_stringOf_enum__e_kt_correlationFunction_t(dist_metric_id), dist_enum_preStep);
    const bool is_ok = export__hpLysis_api(&hp_config, NULL, fileDesriptorOut__clusterResults, config_clusterSets__exportFormat, NULL);
    assert(is_ok); //! ie, what we expect. 
  }
  //!		
  { //! Update the matrix-resutls using the 'groups' to 'hold' the memberships:
    const t_float valueToUpdate = timeInSec;
    matResult__execTimeSimAndCluster__min.matrix[metric_isInGroup][mapOf_time__pos] 
      = macro_min(matResult__execTimeSimAndCluster__min.matrix[metric_isInGroup][mapOf_time__pos], valueToUpdate);
    if(matResult__execTimeSimAndCluster__max.matrix[metric_isInGroup][mapOf_time__pos] != T_FLOAT_MAX) {
      matResult__execTimeSimAndCluster__max.matrix[metric_isInGroup][mapOf_time__pos] 
	= macro_max(matResult__execTimeSimAndCluster__min.matrix[metric_isInGroup][mapOf_time__pos], valueToUpdate);
    } else {matResult__execTimeSimAndCluster__max.matrix[metric_isInGroup][mapOf_time__pos] = valueToUpdate; }
    if(matResult__execTimeSimAndCluster__avg.matrix[metric_isInGroup][mapOf_time__pos] != T_FLOAT_MAX) {
      matResult__execTimeSimAndCluster__max.matrix[metric_isInGroup][mapOf_time__pos] += valueToUpdate;
    } else { matResult__execTimeSimAndCluster__max.matrix[metric_isInGroup][mapOf_time__pos] = valueToUpdate; }
  }
  //! ------------------
  //!
  { //! Update the result-matrix wrt. the "config__ccmMatrixBased__toUse" parameter:
    s_kt_matrix_cmpCluster_clusterDistance_config_t config_local  = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance_config_t();
    s_kt_matrix_cmpCluster_clusterDistance_t obj_local = setToEmpty__s_kt_matrix_cmpCluster_clusterDistance();
    assert(obj_dataConfig.k_clusterCount != 0); assert(obj_dataConfig.k_clusterCount != UINT_MAX);
    if(matrixOf_coOiccurence_1.nrows == matrixOf_coOiccurence_1.ncols) {
      init__s_kt_matrix_cmpCluster_clusterDistance(&obj_local, mapOf_vertexToCentroid1, nrows, nrows, obj_dataConfig.k_clusterCount, matrixOf_coOiccurence_1.matrix, config_local);
    } //! else we assuem teh matrix is Not an adjcency-matrix.
    const t_float score = compute__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local, config__ccmMatrixBased__toUse, /*cmp_type=*/e_kt_matrix_cmpCluster_clusterDistance__config_metric__default);
    if(score != T_FLOAT_MAX) {
      { //! Update the matrix-resutls using the 'groups' to 'hold' the memberships:
	const t_float valueToUpdate = score;
	matResult__ccm__min.matrix[metric_isInGroup][mapOf_time__pos] 
	  = macro_min(matResult__ccm__min.matrix[metric_isInGroup][mapOf_time__pos], valueToUpdate);
	if(matResult__ccm__max.matrix[metric_isInGroup][mapOf_time__pos] != T_FLOAT_MAX) {
	  matResult__ccm__max.matrix[metric_isInGroup][mapOf_time__pos] 
	    = macro_max(matResult__ccm__min.matrix[metric_isInGroup][mapOf_time__pos], valueToUpdate);
	} else {matResult__ccm__max.matrix[metric_isInGroup][mapOf_time__pos] = valueToUpdate; }
	if(matResult__ccm__avg.matrix[metric_isInGroup][mapOf_time__pos] != T_FLOAT_MAX) {
	  matResult__ccm__max.matrix[metric_isInGroup][mapOf_time__pos] += valueToUpdate;
	} else { matResult__ccm__max.matrix[metric_isInGroup][mapOf_time__pos] = valueToUpdate; }
      }
    }
    //!
    //! De-allcoate:
    free__s_kt_matrix_cmpCluster_clusterDistance_t(&obj_local);		  
  }
  { //! Build a sim-matrix for the complete set of CCMs
    assert(cnt_counter_x < groupMedium__cnt_dataSets);
    //! Note: in [”elow] we implcitly compute/constuct "matrixOf_coOccurence_1" using an 'aritifcla construct':
    computeForAllCmpMetrics__subsetNaive__constructArtificalMatrix__kt_matrix_cmpCluster(&matResult__ccm__internal, cnt_counter_x, mapOf_vertexToCentroid1, /*mapOf_vertexToCentroid2=*/obj_dataConfig.mapOf_vertexClusterId, nrows, matrixOf_coOiccurence_1.matrix);
  }
  //! De-allocates the "s_hpLysis_api_t" object.
  free__s_hpLysis_api_t(&hp_config);	
}
if(dist_metricCorrType == e_kt_categoryOf_correaltionPreStep_none) {
  s_hpLysis_api_t hp_config = setToEmpty__s_hpLysis_api_t(NULL, /*&fileHandler__localCorrMetrics=*/NULL);
  hp_config.config.clusterConfig.inputMatrix__isAnAdjecencyMatrix = false;
  char string_local__corr_prior[2000] = {'\0'}; sprintf(string_local__corr_prior, "%sid_%u_%u__case_%u.%s.m_%s_.pre_%u.tag_%s.matrix.tsv", resultDir_path__noPreProcess, cnt_counter_y__sub, cnt_counter_x__sub++, data_id, data_prefix, get_stringOf_enum__e_kt_correlationFunction_t(dist_metric_id), dist_enum_preStep, data_tag);
  hp_config.stringOfResultPrefix__exportCorr__prior = string_local__corr_prior;
  s_kt_correlationMetric_t corrMetric_prior; init__s_kt_correlationMetric_t(&corrMetric_prior, dist_metric_id, dist_metricCorrType);
  hp_config.config.corrMetric_prior = corrMetric_prior; //! ie, set the specified corlreiaotn-meitrc:
  //!
  //! Correlate and export:
  const bool is_ok = filterAmdCorrelate__hpLysis_api(&hp_config, &obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
  assert(is_ok);
  //! 
  //! De-allocates the "s_hpLysis_api_t" object.
  free__s_hpLysis_api_t(&hp_config);	
 }
free__s_kt_matrix(&obj_matrixInput);
	    
//!
//! Increment:
mapOf_time__pos++;
