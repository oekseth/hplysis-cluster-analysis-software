#ifndef kt_distance_slow_h
#define kt_distance_slow_h
#include "graphAlgorithms_aux.h"
#include "correlation_api.h"
#include "matrix_transpose.h"
#include "distance_2rows_slow.h"

//! Compute the weighted euclidian distance for a given amtrix, storing the result in the resultMatrix matrix.
//! @remarks
//!      # "isTo_invertMatrix_transposed" which if set to true makes use of an intermeidate matrix for "transposed==1" parameter: reduces the exeuction-time by at least 9x
//!      # "CLS" describes the level1-memory-cache, which may either be laoded 'in compile-time', or hard-coded: "gcc -DCLS=$(getconf LEVEL1_DCACHE_LINESIZE)", or "getconf LEVEL1_DCACHE_LINESIZE" from the terminal-line.
  void compute_allAgainstAll_distanceMetric_euclid_or_cityBlock(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const bool isTo_use_cityBlock, bool transpose /* = 0 */, const enum e_typeOf_optimization_distance typeOf_optimization /* = e_typeOf_optimization_distance_asFastAsPossible */,  const uint CLS /* = 64 */, const bool isTo_use_continousSTripsOf_memory /* = true */, const uint iterationIndex_2 /* = UINT_MAX */, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation /* = e_allAgainstAll_SIMD_inlinePostProcess_undef */, void *s_inlinePostProcess /* = NULL */, const e_cmp_masksAre_used_t  masksAre_used /* = e_cmp_masksAre_used_undef */) ;
//  void compute_allAgainstAll_distanceMetric_euclid_or_cityBlock(const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const bool isTo_use_cityBlock, bool transpose = 0, const enum e_typeOf_optimization_distance typeOf_optimization = e_typeOf_optimization_distance_asFastAsPossible,  const uint CLS = 64, const bool isTo_use_continousSTripsOf_memory = true, const uint iterationIndex_2 = UINT_MAX, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation = e_allAgainstAll_SIMD_inlinePostProcess_undef, void *s_inlinePostProcess = NULL, const e_cmp_masksAre_used_t  masksAre_used = e_cmp_masksAre_used_undef) ;

  //! Infer an all-against-all distance-matrix for the set of input-data.
void compute_allAgainstAll_distanceMetric_slow(const char dist, const uint nrows, const uint ncolumns, t_float **data1, t_float **data2, t_float weight[], t_float **resultMatrix, char** mask1, char** mask2, uint transposed, const bool isTo_invertMatrix_transposed/* = true*/, const uint CLS /* = 64 */, const bool isTo_use_continousSTripsOf_memory /* = true */, const uint iterationIndex_2 /* = UINT_MAX */, const enum e_allAgainstAll_SIMD_inlinePostProcess typeOf_postProcessing_afterCorrelation /* = e_allAgainstAll_SIMD_inlinePostProcess_undef */, void *s_inlinePostProcess /* = NULL */, const e_cmp_masksAre_used_t  masksAre_used /* = e_cmp_masksAre_used_undef */);
  //! Infer an all-against-all distance-matrix for the set of input-data.
// , UINT_MAX, NULL, e_allagainstall_simd_inlinepostprocess_undef, NULL, e_cmp_masksAre_used_undef
  void compute_allAgainstAll_distanceMetric_slow_configObject(const char dist, const uint nrows, const uint ncolumns, t_float **data1, t_float **data2, t_float weight[], t_float **resultMatrix, char** mask1, char** mask2, uint transposed, 
							      const uint iterationIndex_2 /* = UINT_MAX */, const s_allAgainstAll_config_t config /* = get_init_struct_s_allAgainstAll_config() */);
// UINT_MAX, get_init_struct_s_allAgainstAll_config()

  //! Infer an all-against-all distance-matrix for the set of input-data, for a given row.
  void compute_allAgainstAll_distanceMetric_nonTransposed_forRowAtIndex(const char dist, const uint index, const uint nrows, const uint ncols, t_float **data1, t_float **data2, t_float weight[], t_float **resultMatrix, char** mask1, char** mask2, 
									const uint CLS /* = 64 */, const bool isTo_use_continousSTripsOf_memory /* = true */)
    ;
// 64, true

#endif //! EOF
