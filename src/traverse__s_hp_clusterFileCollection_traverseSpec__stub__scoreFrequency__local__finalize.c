//! Finalize the udpate the of he local frquency-amtrix, and in this contect update the 'glboally mergted' frqunecy-amtrix:
assert(matResult__scoreFrequency__local__currentPos == matResult__scoreFrequency__local.nrows); //! ie, as we then expect all rows to have been visited:
//!
//! Update the global object:
assert(matResult__scoreFrequency.nrows > 0);
assert(obj_counter.cnt__data_x_cluster_x_metPri > 0);
assert(matResult__scoreFrequency.nrows >= matResult__scoreFrequency__local__currentPos);
assert((matResult__scoreFrequency__local.nrows + matResult__scoreFrequency__currentPos) <= matResult__scoreFrequency.nrows); //! ie, to vlaidate that 'space has been allcoated for this'.
s_kt_matrix_t *global = &matResult__scoreFrequency;
s_kt_matrix_t *local = &matResult__scoreFrequency__local;
for(uint row_id = 0; row_id < local->nrows; row_id++) {
  for(uint col_id = 0; col_id < local->ncols; col_id++) {
    assert_possibleOverhead(matResult__scoreFrequency__currentPos < global->nrows);
    global->matrix[matResult__scoreFrequency__currentPos][col_id] = local->matrix[row_id][col_id];	  
  }
  //! 
  { //! Add string-idnetifers:
    const char *string_local = getAndReturn_string__s_kt_matrix(&matResult__scoreFrequency__local, row_id, /*addFor_column=*/false);
    assert(string_local); assert(strlen(string_local));
    //! Udpate the global list:
    set_stringConst__s_kt_matrix(&matResult__scoreFrequency, matResult__scoreFrequency__currentPos, string_local, /*addFor_column=*/false);
    { //! Validate correctness:
      const char *string_local_global = getAndReturn_string__s_kt_matrix(&matResult__scoreFrequency, matResult__scoreFrequency__currentPos, /*addFor_column=*/false);
      assert(string_local_global); assert(strlen(string_local_global));
      assert(strlen(string_local_global) == strlen(string_local));
      assert(0 == strncmp(string_local_global, string_local, strlen(string_local)));
    }
  }
  //!
  //! update the row-counter:
  assert_possibleOverhead(matResult__scoreFrequency__currentPos < global->nrows);
  matResult__scoreFrequency__currentPos++;
 }
//!
{//! Compute a simlairty-amtrix for the object and export:      	
  const char *stringOf_tagSample = mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  mapOf_realLife[data_id].tag;}
  const char *str_suff = strrchr(stringOf_tagSample, '/'); //! ie, the 'beginning' of the file-name.
  if(str_suff) {stringOf_tagSample = str_suff +1;} //! ie, to avodi the '/' to be part of the file-name
  char str_local[2000] = {'\0'}; sprintf(str_local, "%s_scoreFrequency", stringOf_tagSample);	
  //!
  //! Export for for 'input', sim-metric(input), sim-metric(transposed(input)), and 'masked versions' of the latter:
  // TODO: cosnider writing a modiciton of [below] funciton 'dedicated for this purpose' ... using a diffenre tlcuster-alg and testing a set of differetn asusmptions ... eg, wrt. different 'silamirty-schems' for the simalrity-emtrics.
  applyDefaultClustering__andExport(self, &matResult__scoreFrequency__local, /*isTo_transposeMatrix=*/false, /*tag=*/stringOf_tagSample);      
}
