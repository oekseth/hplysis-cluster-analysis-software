#ifndef kt_matrix_h
#define kt_matrix_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_matrix
   @brief a wrapper to provide temrina-access adn high-elvel access to the itnernal data-matrix
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - for a programming-API pelase see our "kt_api.h" header-file
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/


//! Generate a genierc error wrt. funciton-input-aprams.
#define MF__inputError_generic(arg) ({fprintf(stderr, "!!\t %s In brief please validate your call (eg, by reading the docuemtnation). If latter does Not help, then please contact senior developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", arg, __FUNCTION__, __FILE__, __LINE__);})

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
#include "log_clusterC.h"
#include "parse_main.h"
//#include "c_parse/parse_main.h"
#include "kt_longFilesHandler.h" //!" which is only used implicty, eg, by 'callers which are in need of lon file-anems' (eosketh, 06. des. 2016).
#include "math_generateDistribution.h" //! for ranomness-support (oekseth, 06. jan. 2017)
#include "kt_matrix_base.h"
#include "kt_list_1d.h"
#include "kt_swig_retValObj.h"
#include "matrix_deviation.h"
#include "correlation_sort.h"
#include "kt_aux_matrix_blur.h"
#include "kt_aux_matrix_norm.h"
#include "kt_aux_matrix_binning.h"

#ifdef __cplusplus
extern "C" 
#endif


/**
   @struct s_kt_matrix
   @brief a wrapper-object used to add elements to the input-matrix (oekseth, 06. okt. 2016).
   @remarks for examples of use-cases please see our test-ocde in our "measure_terminal.c" (oekseth, 06. nov 2016).
 **/
typedef struct s_kt_matrix {
  t_float **matrix;
  t_float *weight;
  uint ncols; uint nrows;  
  //! ----
  uint *mapOf_rowIds; //! which is used for cases such as 'storing the clsuter-mebership of aparitcula row-vertedx' (oekseth, 06. nvo. 2016).
  uint mapOf_rowIds_biggestIndexInserted; //! which is used during a 'push' operation.
  //! ----
  char **nameOf_rows; char **nameOf_columns; bool nameOf__allocationScheme__allocatedSeperatelyInEach;
  bool isMemoryOwnerOf_attribute_nameOf; //! whihc wehn used is 'used' to avoid 'the strucutre' from de-allocating the "nameOf_columns" and "nameOf_rows" attributes
  //! ----
  FILE *stream_out;
  bool isTo_useMatrixFormat;
  bool isTo_inResult_isTo_useJavaScript_syntax;
  bool isTo_inResult_isTo_useJSON_syntax;
  const char *stringOf_header;
  bool include_stringIdentifers_in_matrix;
  bool isTo_writeOut_stringHeaders;
  //uint cnt_matrices_exported;
  uint cnt_dataObjects_written;  uint cnt_dataObjects_toGenerate; 

  //! ----
  //s_dataStruct_matrix_dense_t data_obj; 
} s_kt_matrix_t;

//! Updates the scalar_result with the "number of rows" in the object (oekseth, 06. apr. 2017). 
//! @remkars incldued to faiclaite SWIG-accesss (eg, from Perl) to our data-strucutre.
static void getProp__nrows__s_kt_matrix_t(s_kt_matrix_t *self, uint *scalar_result) {
  assert(self);
  *scalar_result = self->nrows;
}
//! Updates the scalar_result with the "number of columns" in the object (oekseth, 06. apr. 2017). 
//! @remkars incldued to faiclaite SWIG-accesss (eg, from Perl) to our data-strucutre.
static void getProp__ncols__s_kt_matrix_t(s_kt_matrix_t *self, uint *scalar_result) {
  assert(self);
  *scalar_result = self->ncols;
}

//! Set the s_kt_matrix_t object to empty.
void setTo_empty__s_kt_matrix_t(s_kt_matrix_t *self);
//! @return an nitaited amtrix with No memory-allcoatiosn:
s_kt_matrix_t setToEmptyAndReturn__s_kt_matrix_t();

//! Initates an object
void init__s_kt_matrix(s_kt_matrix_t *self, const uint nrows, const uint ncols, const bool isTo_allocateWeightColumns) ;
//! Initates an object
//! @returnt he new-allcoated object:
static s_kt_matrix_t initAndReturn__s_kt_matrix(const uint nrows, const uint ncols) {
  s_kt_matrix_t self; setTo_empty__s_kt_matrix_t(&self);
  init__s_kt_matrix(&self, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
  return self;
}
//! @returnt he new-allcoated object:
static s_kt_matrix_t initAndReturn_defaultValue__s_kt_matrix(const uint nrows, const uint ncols, const t_float defaultValue) {
  s_kt_matrix_t self; setTo_empty__s_kt_matrix_t(&self);
  init__s_kt_matrix(&self, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
  for(uint i = 0; i < nrows; i++) {
    for(uint k = 0; k < ncols; k++) {
      self.matrix[i][k] = defaultValue;
    }
  }
  return self;
}




/* //! Initates and return an object using random integer (uint) values (oekseth, 06. jan. 2017) */
/* s_kt_matrix_t initAndReturn__randomUint__s_kt_matrix(const uint nrows, const uint ncols, const uint maxValue, const e_kt_randomGenerator_type_t typeOf_randomNess, const bool isTo_initIntoIncrementalBlocks); */
//! Initates an object 'only' wrt. the mapOf_rowIds data-set.
void init__rowMappingTable__s_kt_matrix(s_kt_matrix_t *self, const bool isTo_initCompelte_object, const uint nrows) ;
//! Initates an object both wrt. the mapOf_rowIds and wrt. the matrix (oekseth, 06. feb. 2017).
void init__rowMappingTableAndMatrix__s_kt_matrix(s_kt_matrix_t *self, const bool isTo_initCompelte_object, const uint nrows, const uint ncols);
/**
   @brief builds a sub-set-matrix from an existing superset (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'super-matrix' to copy data from
   @param <arrOf_local_vertexMembers> is the lsit of vertices to build a sub-set for.
   @param <arrOf_local_vertexMembers_size> which is the number of vertex-idneitfies fond in arrOf_local_vertexMembers
   @param <mapOf_globalTo_local> which if != NULL is updated with the 'enw' mapping from 'global' to 'local' vertex-mappings.
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__buildSubset_fromMatrix__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const uint *arrOf_local_vertexMembers, const uint arrOf_local_vertexMembers_size, uint *mapOf_globalTo_local, const bool isTo_updateNames);
/**
   @brief builds a sub-set-matrix from an existing superset (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy__useRanksEachRow__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames);
//! @return a tanspsoed matrix wrt. the inptu-matrix
static s_kt_matrix_t initAndReturn_ranks__s_kt_matrix(const s_kt_matrix_t *superset) {
  s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t();
  const bool is_ok = init__copy__useRanksEachRow__s_kt_matrix(&self, superset, true);
  assert(is_ok);
  return self;
}


/**
   @brief copies a given matrix 'into this' (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames);
//! @reutnr a copy of the prvoided input-matrix:
static s_kt_matrix_t initAndReturn__copy__s_kt_matrix(const s_kt_matrix_t *superset, const bool isTo_updateNames) {
 s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t(); init__copy__s_kt_matrix(&self, superset, /*isTo_updateNames=*/isTo_updateNames); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
 return self;
}



//! @return a nraomzlied veriosn of hte amtrix, ie, allcoating a enw matrix (oekseth, 06. mai. 2017)
static s_kt_matrix_t initAndReturn__copy__normalized__s_kt_matrix_t(const s_kt_matrix_t *superset, const bool isTo_useMedian, const bool isTo_useAbs, const bool isTo_useSTD) {
  assert(superset);
  s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t(); init__copy__s_kt_matrix(&self, superset, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);

  
  for(uint i = 0; i < self.nrows; i++) {
    apply_norm__kt_aux_matrix_norm(superset->matrix[i], self.matrix[i], self.ncols, isTo_useMedian, isTo_useAbs, isTo_useSTD);
  }
  return self;
}


//! @return a nraomzlied veriosn of hte amtrix, ie, allcoating a enw matrix (oekseth, 06. mai. 2017)
static s_kt_matrix_t initAndReturn__copy__normalized__s_kt_matrix_t(const s_kt_matrix_t *superset) {
  assert(superset);
  const bool isTo_useMedian = false; const bool isTo_useAbs = true; const bool isTo_useSTD = false;
  s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t(); init__copy__s_kt_matrix(&self, superset, /*isTo_updateNames=*/true); //initAndReturn__s_kt_matrix(superset->nrows, superset->ncols);
  
  for(uint i = 0; i < self.nrows; i++) {
    apply_norm__kt_aux_matrix_norm(superset->matrix[i], self.matrix[i], self.ncols, isTo_useMedian, isTo_useAbs, isTo_useSTD);
  }
  return self;
}

/**
   @brief use a simple mask-strategy to idneityf the set of interesting valeus (oesketh, 06. des. 2016).
   @param <matrix_input> is the matrix which we updat ethe values of.
   @param <threshold_min> the min-value: if "useRelativeScore_betweenMinMax" is used we expect the valeu to be in range [0 ... 1];
   @param <threshold_max> the max-value: if "useRelativeScore_betweenMinMax" is used we expect the valeu to be in range [0 ... 1];
   @param <useRelativeScore_betweenMinMax> which if set to false impleis that we use the scores 'directly'.
//   @param <thresholds_adjust_0_100> if set then we adjsut the rank-score to reflect the max-rank in the data-set
   @return true upon success.
   @remarks the idea of this funcitoni is to provide a simple-logic fitler-strategy to increase the specififty of the data. Used as part of djsijoint-based-fitlering: a subset of our "kt_matrix_filter"; hope is that this funciton simplies the gneralized applciation of 'prior' filters.
   @remarks for an extnesive lsit of differnet filter-strategies please see our "kt_matrix_filter.h"
   @remarks wrt. the "useRelativeScore_betweenMinMax" argument: in our 'adjustment' we adjsut the threshold-values, eg, for the 'max-function': "threshold_max = avg + abs(maxValue - avg)*threshold_max". This adjustment is 'used' in order to increase spficity/correpsondence between a 'gernalized writing of vlaue-trhesholds' and 'the result of such a filtering-process'. To examplify the latter we oftne observe that 'either all of no valeus are matched if we use AVG or MIN-value or MAX-value (which is due to 'unkown min-point of the avg-value').
 **/
bool kt_matrix__filter__simpleMask__kt_api(s_kt_matrix_t *matrix_input, t_float threshold_min, t_float threshold_max, const bool useRelativeScore_betweenMinMax); //, const bool thresholds_adjust_0_100);





/**
   @brief copies a trasnposed version of a given matrix 'into this' (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy_transposed__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames);
//! @return a tanspsoed matrix wrt. the inptu-matrix
static s_kt_matrix_t initAndReturn_transpos__s_kt_matrix(const s_kt_matrix_t *superset) {
  s_kt_matrix_t self = setToEmptyAndReturn__s_kt_matrix_t();
  const bool is_ok = init__copy_transposed__s_kt_matrix(&self, superset, true);
  assert(is_ok);
  return self;
}
/**
   @brief copies a trasnpose and rank the given matrix 'into this' (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy_transposed__thereafterRank__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames);
/**
   @brief builds a sub-set-matrix from an existing superset (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'sper-matrix' to copy data from
   @param <arrOf_local_vertexMembers_head> is the lsit of vertices to build a sub-set for: for the rows/heads
   @param <arrOf_local_vertexMembers_head_size> which is the number of vertex-idneitfies fond in arrOf_local_vertexMembers_head: for the rows/heads
   @param <arrOf_local_vertexMembers_tail> is the lsit of vertices to build a sub-set for: for the columns/tails
   @param <arrOf_local_vertexMembers_tail_size> which is the number of vertex-idneitfies fond in arrOf_local_vertexMembers_tail: for the columns/tails
   @param <mapOf_globalTo_local> which if != NULL is updated with the 'enw' mapping from 'global' to 'local' vertex-mappings.
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks 
   - is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
   - is different from "init__buildSubset_fromMatrix__s_kt_matrix(..)" wr.t the 'fact' that we asusem the rows-ids are different from teh coolumn-ids
   - wrt. the "mapOf_globalTo_local" we only update "mapOf_globalTo_local" for the 'row' case (ie, as we asusem only the 'rows' are used in the result-table, while ie, as the columsn are expected to describe/present the feautres)
 **/
bool init__buildSubset_rowsDifferentFromColumns__fromMatrix__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const uint *arrOf_local_vertexMembers_head, const uint arrOf_local_vertexMembers_head_size, const uint *arrOf_local_vertexMembers_tail, const uint arrOf_local_vertexMembers_tail_size, uint *mapOf_globalTo_local, const bool isTo_updateNames);



static s_kt_list_1d_float_t convert_matrixToList_1d_s_kt_matrix_t(const s_kt_matrix_t *self) {
  assert(self->nrows > 0);
  assert(self->ncols > 0);
  const uint list_size = self->nrows * self->ncols;
  if(list_size == 0) {return setToEmpty__s_kt_list_1d_float_t();} //! ie, a fall-back
  //! Ivneistaige issues related to overlfow in numbers
  assert(list_size >= self->nrows);
  assert(list_size >= self->ncols);
  s_kt_list_1d_float_t obj = init__s_kt_list_1d_float_t(list_size);
  for(uint i = 0; i < self->nrows; i++) {
    const t_float *row = self->matrix[i];
    assert(row);  
    for(uint j = 0; j < self->ncols; j++) {
      obj.list[obj.current_pos++] = row[j];
    }
  }
  return obj;
}

//! @return a shallow non-memory-allcoated copy of our s_kt_matrix_t object (oesketh, 06. mar. 2017)
static s_kt_matrix_base_t get_shallowCopy__s_kt_matrix_base_t__s_kt_matrix_t(const s_kt_matrix_t *self) {
  assert(self);
  s_kt_matrix_base_t cpy = initAndReturn__notAllocate__s_kt_matrix_base_t(self->nrows, self->ncols, self->matrix);
  return cpy;
}
//! @return a shallow non-memory-allcoated copy of our s_kt_matrix_t object (oesketh, 06. mar. 2017)
static s_kt_matrix_t get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(const s_kt_matrix_base_t *ext) {
  assert(ext);
  s_kt_matrix_t mat_shallow = setToEmptyAndReturn__s_kt_matrix_t();
  mat_shallow.nrows = ext->nrows; 
  mat_shallow.ncols = ext->ncols;
  mat_shallow.matrix = ext->matrix;
  return mat_shallow;
}
//! @return a deep memory-allcoated copy of our s_kt_matrix_t object (oesketh, 06. apr. 2017)
static s_kt_matrix_t get_deepCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(const s_kt_matrix_base_t *ext) {
  assert(ext);
  s_kt_matrix_t mat_deep = initAndReturn__s_kt_matrix(ext->nrows, ext->ncols);
  for(uint i = 0; i < mat_deep.nrows; i++) {
    for(uint j = 0; j < mat_deep.ncols; j++) {
      mat_deep.matrix[i][j] = ext->matrix[i][j];
    }
  }
  return mat_deep;
}
/**
   @brief copy a set of triplets into a 'dense' matrix  (oekseth, 06. mar. 2017).
   @param <self> is the matrix to 'copy from'
   @param <isTo_allocateWeightColumns> which if set to "true" will (a) result in higher memroy-consumption though (b) 'covney' (eg, to our "hp_ccm.h") that the input is an adjcnecy-matrix.
   @return a enw-allcoated matrix 'holding ' the set of 'desent tripelts'.
 **/
static s_kt_matrix_t copy__fromSparseToDense__s_kt_matrix(const s_kt_matrix_t *self, const bool isTo_allocateSquared) {
  assert(self);
  if(self->ncols != 3) {
    allocOnStack__char__sprintf(2000, str_local, "Expected number-of-columns='3', though instead observes that your input-file-1 has '%u' columns. ", self->ncols); //! ie, intate a new variable "str_local". 
    MF__inputError_generic(str_local);
    assert(false);
    if(self->ncols  < 3) {
      return setToEmptyAndReturn__s_kt_matrix_t();
    }
  } else if(self->matrix == NULL) {
    allocOnStack__char__sprintf(2000, str_local, "Matrix is empty (ie, with dims=[%u, %u]), ie, a pointless call. ", self->nrows, self->ncols); //! ie, intate a new variable "str_local". 
    MF__inputError_generic(str_local);
    assert(false);
    return setToEmptyAndReturn__s_kt_matrix_t();

  }
  //!
  //! Find the dimesnions:
  uint max_head = 0;   
  uint max_tail = 0;
  for(uint i = 0; i < self->nrows; i++) {
    const t_float *row = self->matrix[i];
    assert(row);
    s_ktType_pairFloat_t obj = MF__initFromRow__s_ktType_pairFloat(row, self->ncols);
    if(obj.head != UINT_MAX) {
      max_head = macro_max(max_head, obj.head);
    }
    if(obj.tail != UINT_MAX) {
      max_tail = macro_max(max_tail, obj.tail);
    }
  }
  max_head++;  max_tail++;
  //! 
  //! Allocate:
  if(isTo_allocateSquared) {max_head = max_tail = macro_max(max_head, max_tail);}
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(max_head, max_tail);
  for(uint i = 0; i < self->nrows; i++) {
    const t_float *row = self->matrix[i];
    assert(row);
    s_ktType_pairFloat_t rel = MF__initFromRow__s_ktType_pairFloat(row, self->ncols);
    if(rel.head != UINT_MAX) {
      if(rel.tail != UINT_MAX) {
	assert(rel.head < mat_result.nrows);
	assert(rel.tail < mat_result.ncols);
	//! Append:
	mat_result.matrix[rel.head][rel.tail] = rel.score;
      }
    }
  }
  //!
  //! @return
  return mat_result;  
}

/**
   @brief copy desne matrix-format-data-set int a 'sparse' matrix' (oekseth, 06. mar. 2017).
   @return a enw-allcoated matrix 'holding ' the set of 'sparse triplets'
 **/
static s_kt_matrix_t copy__fromDenseToSparse__s_kt_matrix(const s_kt_matrix_t *self) {
  assert(self);
  assert(self->nrows);
  assert(self->ncols);
  const long long int cnt_rows = self->nrows * self->ncols;  
  assert(cnt_rows < (long long int)UINT_MAX);
  if(cnt_rows >= (long long int)UINT_MAX) {
    fprintf(stderr, "!!\t Your have requested a data-format (ie, an input-matrix with dims=[%u, %u]) larger than the defualt size-proeprty of our \"s_kt_matrix_t\" data-strucutre: if the latter is of true need in your work, pelase request the replacement of \"uitn\" to \"long long int\" (in the latter strucutre), cotnacting senior devleoper [oekseth@gmail.com]. Observiaotn at [%s]:%s:%d\n", self->nrows, self->ncols, __FUNCTION__, __FILE__, __LINE__);
    return setToEmptyAndReturn__s_kt_matrix_t();
  }
  //! 
  //! Allocate:
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix((uint)cnt_rows, /*cnt-in-each-triplet=*/3);
  uint curr_pos = 0;
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    for(uint col_id = 0; col_id < self->ncols; col_id++) {
      const t_float score = self->matrix[row_id][col_id];
      if(isOf_interest(score)) { //! then we 'inlcude' the score: 
	s_ktType_pairFloat_t rel = MF__initVal__s_ktType_pairFloat(row_id, col_id, score);
	assert(curr_pos < mat_result.nrows);
	mat_result.matrix[curr_pos][0] = (t_float)rel.head;
	mat_result.matrix[curr_pos][1] = (t_float)rel.tail;
	mat_result.matrix[curr_pos][2] = (t_float)rel.score;
#ifndef NDEBUG
	//! then validate correctness:
	const t_float *row = mat_result.matrix[curr_pos];
	assert(row);
	s_ktType_pairFloat_t cmp = MF__initFromRow__s_ktType_pairFloat(row, self->ncols);
	assert(MF__isEqual__s_ktType_pairFloat(rel, cmp)); //! ie, as we othersdie assuem there is a 'loss of cosnsitency' wrt. our mapping-appraoch.
#endif
	//! Increment:
	curr_pos++;
      }
    }
  }
  //!
  //! @return
  return mat_result;  

}
//! De-allocate sthe nameOf_columns attribute (fi the latter differns from the "nameOf_rows" attribute) (oekseth, 06. des. 2016).
//! @remarks a use-case where we 'replace' teh "nameOf_columns" attribute with a 'driect' poitner-reference to "nameOf_rows" after the correlaiton-matrix is comptued (oekseth, 06. des. 2016)
void free_arrOf_names_columns__s_kt_matrix(s_kt_matrix_t *self);
//! De-allcoate the s_kt_matrix_t object.
void free__s_kt_matrix(s_kt_matrix_t *self);

/**
   @brief specify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos> is the id of the internla column or row to udpdate
   @param <stringTo_add> is the string to copy into this strucutre
   @param <addFor_column> which if faset to false (or '0') implies that we insert into the row-set.
   @remarks we expect the index_pos to be less than "nrows" for "addFor_column==0", and index_pos to be less than ncols for "addFor_column==1";   
 **/
void set_stringConst__s_kt_matrix(s_kt_matrix_t *self, const uint index_pos, const char *stringTo_add, const bool addFor_column);

/**
   @brief specify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos> is the id of the internla column or row to udpdate
   @param <stringTo_add> is the string to copy into this strucutre
   @param <addFor_column> which if faset to false (or '0') implies that we insert into the row-set.
   @remarks we expect the index_pos to be less than "nrows" for "addFor_column==0", and index_pos to be less than ncols for "addFor_column==1";   
 **/
static void set_string__s_kt_matrix(s_kt_matrix_t *self, const uint index_pos, char *stringTo_add, const bool addFor_column) { set_stringConst__s_kt_matrix(self, index_pos, stringTo_add, addFor_column);}
//! A wrapper-fucntion among others used by SWIG-rwapper-languages to simplify data-access (oekset, 06. apr. 2017).
static void set_string__s_kt_matrix__obj(s_kt_matrix_t *self, const uint index_pos, const bool addFor_column, s_kt_swig_retValObj_t *obj) {
  assert(obj); assert(self);
  const char *stringTo_add = obj->name;
  // printf("set[%u]=\"%s\", at %s:%d\n", index_pos, stringTo_add, __FILE__, __LINE__);
  set_stringConst__s_kt_matrix(self, index_pos, stringTo_add, addFor_column);}
/**
   @brief idnetify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos>
   @param <addFor_column> which if faset to false (or '0') implies that we insert into the row-set.
   @param <stringOf_result> is the string to get a memory-pointer to.
   @remarks we expect the index_pos to be less than "nrows" for "addFor_column==0", and index_pos to be less than ncols for "addFor_column==1";   
 **/
static inline 
//__attribute__((always_inline)) 
void get_string__s_kt_matrix(const s_kt_matrix_t *self, const uint index_pos, const bool addFor_column, char **stringOf_result) {
  assert(self);
  *stringOf_result = NULL; //! ie, to 'init' the variable.
  if(addFor_column == false) {
    //assert(self->nameOf_rows);
    //assert(index_pos < self->nrows);
    if( (index_pos < self->nrows) && self->nameOf_rows) {*stringOf_result = self->nameOf_rows[index_pos];}
    //printf("\t\trow[%u]=\"%s\", at %s:%d\n", index_pos, *stringOf_result, __FILE__, __LINE__);
  } else { 
    //assert(self->nameOf_columns);
    //assert(index_pos < self->ncols);
    if( (index_pos < self->ncols) && self->nameOf_columns) {*stringOf_result = self->nameOf_columns[index_pos];}
  }
  // printf("\t\trow[%u]=\"%s\", at %s:%d\n", index_pos, *stringOf_result, __FILE__, __LINE__);
}
//__attribute__((always_inline)) 
static void get_string__s_kt_matrix__swig(const s_kt_matrix_t *self, const uint index_pos, const bool addFor_column, s_kt_swig_retValObj_t *ret_val) {
  assert(ret_val);
  char *stringOf_result = NULL; get_string__s_kt_matrix(self, index_pos, addFor_column, &stringOf_result);
  // printf("get[%u]=\"%s\", at %s:%d\n", index_pos, stringOf_result, __FILE__, __LINE__);
  ret_val->name = stringOf_result;
}
//! @return the assicated string:
static inline 
//__attribute__((always_inline)) 
const char *getAndReturn_string__s_kt_matrix(const s_kt_matrix_t *self, const uint index_pos, const bool addFor_column) {
  assert(self);
  char *stringOf_result = NULL;
  get_string__s_kt_matrix(self, index_pos, addFor_column, &stringOf_result);
  return stringOf_result;
  // printf("\t\trow[%u]=\"%s\", at %s:%d\n", index_pos, *stringOf_result, __FILE__, __LINE__);
}
//! Copy the names:
void copyNames_intoExistingMatrix__s_kt_matrix_t(s_kt_matrix_t *self, const s_kt_matrix_t *superset);

//! Identify the index for the given string (ie, if the stirng is found) (oekseht, 06. des. 2016)
//! @return row_id if foudn: otherwise UINT_MAX is retunred (ie, as we then assume the string is Not found).
//! @remarks this fucntion is Not set to 'inline' as we asusme there will be now signficnat perfomrance-costs assicated to thsi funciton (ie, due to the relative low number of access which we expect).
uint getIndexOf__atRow__s_kt_matrix(const s_kt_matrix_t *self, const char *stringOf_value);
//! Identify the index for the given string (ie, if the stirng is found) (oekseht, 06. des. 2016)
//! @return column_id if foudn: otherwise UINT_MAX is retunred (ie, as we then assume the string is Not found).
//! @remarks this fucntion is Not set to 'inline' as we asusme there will be now signficnat perfomrance-costs assicated to thsi funciton (ie, due to the relative low number of access which we expect).
uint getIndexOf__atColumn__s_kt_matrix(const s_kt_matrix_t *self, const char *stringOf_value);


//! @return a nraomzlied veriosn of hte amtrix, ie, allcoating a enw matrix (oekseth, 06. mai. 2017)
static s_kt_matrix_t initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(const s_kt_matrix_t *superset, e_kt_normType_t enum_id) {
  //! Re-format:
  s_kt_matrix_base_t superset_cpy = initAndReturn__notAllocate__s_kt_matrix_base_t(superset->nrows, superset->ncols, superset->matrix);
  //! Comopute: 
  s_kt_matrix_base_t mat_result_shallow = initAndReturn__copy__normalizedByEnum__kt_aux_matrix_norm(&superset_cpy, enum_id, /*allocate=*/true);
  assert(mat_result_shallow.matrix != NULL);
  assert(mat_result_shallow.matrix != superset->matrix);
  //! Convert
  s_kt_matrix_t mat_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result_shallow);
  //! Add names:
  copyNames_intoExistingMatrix__s_kt_matrix_t(&mat_result, superset);
  assert(mat_result.matrix != superset->matrix);
  //! @return
  return mat_result;  
}


/**
   @brief constructs a 'blurred' veriosn of a given matrix (oekseth, 0p6. feb. 2018).
   @param <self> the input matrix.
   @param <enum_id> the type of blur funciton to apply.
   @param <radius> the radius of the 'blur-field' to apply.
   @return a 'blurred' matrix.
 **/
static s_kt_matrix_t apply_blur__s_kt_matrix_t(s_kt_matrix_t *self, const e__blur enum_id, const t_float radius) {
  assert(self);
  //! Re-format:
  s_kt_matrix_base_t self_cpy = initAndReturn__notAllocate__s_kt_matrix_base_t(self->nrows, self->ncols, self->matrix);
  //! Apply:
  s_kt_matrix_base_t mat_result_shallow = apply_blur__kt_aux_matrix_blur(&self_cpy, enum_id, radius);
  //! Convert
  s_kt_matrix_t mat_result = get_shallowCopy__fromBase__s_kt_matrix_base_t__s_kt_matrix_t(&mat_result_shallow);
  //! @return
  return mat_result;  
}

/**
   @brief updates "self" with the value-frequency-deisitrubioin in "data" (oekseth, 06. feb. 2017).
   @param <self> is the object to update wrt. the feature-matrix-count.
   @param <data> is a 2d-matrix 'treaded'/hanlded as a 1d-list wrt. the score-counts.
   @param <row_id> si the row-id to updated at in "self": should be less than "self->nrows".
   @return true upon success.
   @remarks the 'number of buckets B' is descided by the "ncols" varialbe in the "self" object
   @remarks the funcion is designed to cosntruct a 'frequency-count-matrix': used to 'generate' a frquency-matrix of values divided into B user-deifned buckets, where a 'relative count' is used when udpating a 'data-specific' and a 'global matrix' of counts
 **/
bool extractSubset_atIndex_type_frequencyMatrix__s_kt_matrix_t(s_kt_matrix_t *self, const s_kt_matrix_t *data, const uint row_id);


/**
   @brief merge two matrices and return the new-constructed matrix (oekseth, 06. june. 2017).
   @param <mat_1> is the first inptu-amtrix
   @param <mat_2> is the second inptu-atmrix
   @param <mat_2_prefixInput_rows> optioanl: if set is sued to 'append't eh row-sitrng-naems (ie, if the row-stirng-naems are sued): otehrwise ingored.
   @return the new-sotrnctued matrix.
 **/
s_kt_matrix_t merge_twoMatrices__s_kt_matrix_t(const s_kt_matrix_t *mat_1, const s_kt_matrix_t *mat_2, const char *mat_2_prefixInput_rows);
/**
   @brief merge two matrices, uidpåating the "mat_1" (oekseth, 06. june. 2017).
   @param <mat_1> is the first inptu-amtrix, ie, the "self" object.
   @param <mat_2> is the second inptu-atmrix
   @param <mat_2_prefixInput_rows> optioanl: if set is sued to 'append't eh row-sitrng-naems (ie, if the row-stirng-naems are sued): otehrwise ingored.
 **/
void insertMatrix_intoMatrix__s_kt_matrix_t(s_kt_matrix_t *mat_1, const s_kt_matrix_t *mat_2, const char *mat_2_prefixInput_rows);

//! Update a matrix to a symmetric strucutre, where the 'average' value is used (when updating two cells with valeus) (oekseht, 06. jan. 2017).
//! @remarks as input we expect an adjcency-matrix, ie, as comptuiogn the symmeitrc 'proeprty' may 
void updateTo_symmetric__useAvg__s_kt_matrix_t(s_kt_matrix_t *self);
/**
   @brief Set the default value to all the cells in the matrix
   @param <self> is the object to update
   @param <score_default> is the score to set
**/
void setDefaultValueTo__allCells__kt_matrix(s_kt_matrix_t *self, const t_float score_default);
/**
   @brief Set the default value to all the cells in the matrix which are below min_threshold (oekseth, 06. des. 2016)
   @param <self> is the object to update
   @param <min_threshold> is the min-value whicht he scores need to be [ªbove].
**/
void setDefaultValueTo_empty__whichAreBelowThreshold__s_kt_matrix_t(s_kt_matrix_t *self, const t_float min_threshold);

/**
   @brief remove cells which are not symmetircally realted to each other (oekseth, 06. des. 2016).
   @param <self> is the object to udpate
   @param <vertex_clusterId> hold the 'mappigns' for each vertex
   @param <vertex_clusterId_size> is the number of elmeents in vertex_clusterId
   @param <map_vertexGroups_cols> is the column-to-cluster mappings the columsn: if the 'rows' and 'columns' are in the smae name-space then we assume that "map_vertexGroups_cols == NULL" (ie, as 'using this' then has no point).
   @remarks an applicaiton-case is to use 'this fucntiobn' as a psot-fitler after k-means clstuering, eg, to highlith the different clsuters
 **/
void setDefaultValueTo_empty__removeCellsWithDifferentMembershipId__s_kt_matrix_t(const s_kt_matrix_t *self, const uint *vertex_clusterId, const uint vertex_clusterId_size, const uint *map_vertexGroups_cols);

//! Set the value in the object
void set_cell__s_kt_matrix(s_kt_matrix_t *self, const uint row_id, uint col_id, const t_float value);
//! Get the value in the object
void get_cell__s_kt_matrix(const s_kt_matrix_t *self, const uint row_id, uint col_id, t_float *scalar_result);
//! Set the value in the object
void set_weight__s_kt_matrix(s_kt_matrix_t *self, const uint index, const t_float value) ;


//! Idneitfy the weight assicated to col_id
//! @remarks if the weight is not fouudn then value is set to T_FLOAT_MAX
static inline 
//__attribute__((always_inline)) 
void get_weight__s_kt_matrix(const s_kt_matrix_t *self, const uint col_id, t_float *scalar_result) {
  assert(self); assert(self->weight); assert(col_id < self->ncols);
  *scalar_result = T_FLOAT_MAX; //! ie, a m'default assignment'
  if(self->weight && (col_id < self->ncols) ) {*scalar_result = self->weight[col_id];}
}

//! Push a value to the "mapOf_rowIds" list.
//! @remarks a use-case is when itnating/build input-sets for the "kt_matrix__clusterdistance(..)" function wr.t the two inptu-idnexlists
static inline 
//__attribute__((always_inline)) 
void push_row_member__s_kt_member(s_kt_matrix_t *self, const uint value) {
  assert(self); assert(self->mapOf_rowIds); assert(self->mapOf_rowIds_biggestIndexInserted < self->nrows);
  self->mapOf_rowIds[self->mapOf_rowIds_biggestIndexInserted] = value;
  self->mapOf_rowIds_biggestIndexInserted++; //! ie, increment.
}

/** @brief update the scalar_result with the row-meber assicated to row_id (oekseth, 06. nov. 2016)
    @param <self> is the object which hold the mapåping-table
    @param <row_id> is the row (eg, vertex with a given feature-vecotr) to ge thte id (ieg, the clsuter-id) for.
    @param <scalar_result> is the assicated valeu, eg, the clsuter-embership: if not found the value is set to UINT_MAX
    @remarks an example-usage cosenrsn the 'fetching' of row-ids computed from the "kt_matrix__getclustercentroids(..)" proceudre-call
**/
static inline 
//__attribute__((always_inline)) 
void get_row_member__s_kt_matrix(const s_kt_matrix_t *self, const uint row_id, uint *scalar_result) {
  assert(self); assert(self->mapOf_rowIds); assert(row_id < self->nrows);
  *scalar_result = UINT_MAX; //! ie, a m'default assignment'
  if(self->mapOf_rowIds && (row_id < self->nrows) ) {*scalar_result = self->mapOf_rowIds[row_id];}
}


/**
   @brief load a data-set into the data_objh, either using input-data or a value-range-distribution defined by our "stringOf_sampleData_type" param.
   @param <data_obj> the object to hold the new-cosntructed data-set: if set to a string-value, the other function-arpameters are ginored.
   @param <input_file> which if set ideitnfied the input-file to use
   @param <cnt_rows> the number of rows to be cosnturcted/inferred.
   @param <cnt_columns> the number of features/columns to be cosnturcted/inferred.
   @param <readData_fromStream> which if set implies that the data-'foudn' in "stream" is used to buidl the data-matrix: has prioerty over the stringOf_sampleData_type funciton-parameter.
   @param <stringOf_sampleData_type> identified the type of funciton (eg, 'random' or 'uniform') for which we are to infer value-ranges.
   @param <fractionOf_toAppendWith_sampleData_typeFor_rows> identifies the percentage of sampel-data-dsitribiton rows to append to a 'real-life' data-set: is used to combine either input_file, readData_fromStream or stringOf_sampleData_type_realLife with stringOf_sampleData_type
   @param <fractionOf_toAppendWith_sampleData_typeFor_columns> identifies the percentage of sampel-data-dsitribiton columns to append to a 'real-life' data-set: is used to combine either input_file, readData_fromStream or stringOf_sampleData_type_realLife with stringOf_sampleData_type
   @param <stringOf_sampleData_type_realLife> which if set is used to idnentify 'in-line' real-life data-set which are to be used.
   @param <isTo_transposeMatrix> which if set to true implies that we transpose the matrix.
   @return true on success.
 **/
bool readFrom__build_dataset_fromInputOrSample__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file, uint cnt_rows, uint cnt_columns, const bool readData_fromStream, const char *stringOf_sampleData_type, uint fractionOf_toAppendWith_sampleData_typeFor_rows, uint fractionOf_toAppendWith_sampleData_typeFor_columns, const char *stringOf_sampleData_type_realLife, const bool isTo_transposeMatrix);

/**
   @brief read a matrix into the structure.
   @param <self> is the object to udpate
   @param <input_file> is the file to read.
   @return true upon success.
 **/
bool readFrom__file__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file);

/**
   @struct s_kt_matrix_fileReadTuning_base   
   @brief a strucutre for adnvaced tuning/configuraitons of file-reading (oekseth, 06. feb. 2017).
 **/
typedef struct s_kt_matrix_fileReadTuning {
  bool fileIsRealLife; //! otherwise we assume the string describes one of the 'altenraitve math-functiosn' define din our "constructSample_data_identifiedFromString" function, eg, ["lines-different-ax", "lines-curved", "lines-different-ax-and-axx", "lines-ax-inverse-x", "lines-circle", "lines-sinsoid", "lines-sinsoid-curved"], where the latter may have a 'noise-suffix of ["-medium", "large"].
  uint imaginaryFileProp__nrows;   uint imaginaryFileProp__ncols; //! which are used as the 'idmensions' if "fileIsRealLife" is specified: toerhwise ignored.
  s_kt_matrix_t *mat_concat; //! which if set is used to 'merge' muliple data-sets.
  bool isTo_transposeMatrix;  
  uint fractionOf_toAppendWith_sampleData_typeFor_rows; //! which if set for real-life data implies that we 'append' the data-set with a uniform nosie-distirbution, where a use-case is nto evlauat eht enosei-sensitivty 'be3tween' a data-set, a simlairty-metyric and a cluster-algorithm.
  uint fractionOf_toAppendWith_sampleData_typeFor_columns; //! (simliar as "fractionOf_toAppendWith_sampleData_typeFor_rows"). 
  const char *isTo_exportInputFileAsIs__toFormat__js; //! which if set to true implies that we expert the input-file to a JS result-file; a use-case si to comapre a simalrity-amtrix-cosntructed dat-aset to the orignal input-file.
  bool isTo__copyNamesIfSet;
} s_kt_matrix_fileReadTuning_t;

//! Initates hte "s_kt_matrix_fileReadTuning_t" object and return (oesketh, 06. feb. 2017).
static s_kt_matrix_fileReadTuning_t initAndReturn__s_kt_matrix_fileReadTuning_t() {
  s_kt_matrix_fileReadTuning_t self;
  self.fileIsRealLife = true;
  self.mat_concat = NULL;
  self.isTo_transposeMatrix = false;
  self.fractionOf_toAppendWith_sampleData_typeFor_rows = 0;
  self.fractionOf_toAppendWith_sampleData_typeFor_columns = 0;
  self.imaginaryFileProp__nrows = 100;   self.imaginaryFileProp__ncols = 100; //! which are used as the 'idmensions' if "fileIsRealLife" is specified: toerhwise ignored.
  self.isTo_exportInputFileAsIs__toFormat__js = NULL; //! which if set to true implies that we expert the input-file to a JS result-file; a use-case si to comapre a simalrity-amtrix-cosntructed dat-aset to the orignal input-file.
  self.isTo__copyNamesIfSet = true;
  //! @return
  return self;
}

//#include "kt_matrix_fileReadTuning.h"

//! Parse a data-file suign the configurations from the "s_kt_matrix_fileReadTuning_t" object (oekseth, 06. feb. 2017).
s_kt_matrix_t readFromAndReturn__file__advanced__s_kt_matrix_t(const char *input_file, s_kt_matrix_fileReadTuning_t config);
//! Laod data, intlaizing our "self" s_kt_matrix_t" object (oekseth, 06. apr. 2017). 
static void import__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file) {
  assert(self);
  *self = readFromAndReturn__file__advanced__s_kt_matrix_t(input_file, initAndReturn__s_kt_matrix_fileReadTuning_t());
}
/**
   @brief read a matrix into the structure, and then transpsoe the data (before loading the data-set into memory).
   @param <self> is the object to udpate
   @param <input_file> is the file to read.
   @return true upon success.
 **/
bool readFrom__fileTransposed__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file);

/**
   @brief read a 1d-weigth-table into the weigth-table-list: we expect each 'weight' to be seperated by a newline "\n".
   @param <self> is the object to udpate
   @param <input_file> is the file to read.
   @return true upon success.
   @remarks we expect the number of 'rows' to be less than-or-equal to the number of columns in your input-fiel (where 'column' refers to 'rows' if you are using the transpsoed topioin')
 **/
bool readFrom_1dWeightTable__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file);

/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <stringOf_measureId> which is used to 'name' the Java-script matrix-varaible (whcih is 'written out').
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, FILE *stream, const bool include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders);
/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <stringOf_measureId> 
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders) ;
/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <isLast> which if set to true impleis that we 'finalize' the JSON-array.
   @param <isTo_explcitlyInclude_rowAndColumnHeaders> which if set impleis that we explcutly inlcude the row and column-hders: an example-usage is for the case where we cocnate the inptu-matrix with an 'adjsuted' data-sets, eg, where the 'previosuly fucniton-calls' describes a corelation-matrix-set with 50 per-cent threshodls.
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_JSON__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders, const bool isLast, const char *isTo_explcitlyInclude_rowAndColumnHeaders) ;
/**
   @brief write out the obj to the stream as set of relations.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stringOf_predicate> which if set is used as predicate when writing out the data, eg, to simplify interpreation of the relationships.
   @param <stream> is the poitner to the restul-stream.
   @param <isTo_useJavaScript_syntax> which if set to true implies that we makes use of a java-script syntax.
   @param <isTo_useMatrixFormat> which if set to false impleis that we exprot using a relation-centered format.
 **/
void export_dataSet_s_dataStruct_matrix_dense_relations__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, const char *stringOf_predicate, FILE *stream, const bool isTo_useJavaScript_syntax, const bool isTo_useMatrixFormat);

//! Cofnigure the file-export-routine:
void export_config__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile);
//! An extneisve configruation to the file-export-routine.
void export_config_extensive_s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, const bool isTo_useMatrixFormat, const bool isTo_inResult_isTo_useJavaScript_syntax, const bool isTo_inResult_isTo_useJSON_syntax, const char *stringOf_header, const bool include_stringIdentifers_in_matrix, uint cnt_dataObjects_toGenerate);


//! A fucntion where we use the file-out-configuratiosn in "self" and the matrix-data in data_obj to udpate the result-file.
void export__dataObj_seperateFromFileObj__s_kt_matrix_t(s_kt_matrix_t *self, s_kt_matrix_t *data_obj, const char *StringOf_header);
//! Wirte out the matrix to the format specified through the set fo inptu-aprameters (oekseth, 06. nov. 2016).
void export__setLabel__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header);
//! Wirte out the matrix to the format specified through the set fo inptu-aprameters (oekseth, 06. nov. 2016).
void export__s_kt_matrix_t(s_kt_matrix_t *self); //, FILE *stream_out, const bool isTo_useMatrixFormat, const bool isTo_inResult_isTo_useJavaScript_syntax, const bool isTo_inResult_isTo_useJSON_syntax, const char *stringOf_header, const bool include_stringIdentifers_in_matrix, const bool isTo_writeOut_stringHeaders, const bool isLast);

/**
   @brief export a 1d-weigth-table into from weigth-table-list: we expect each 'weight' to be seperated by a newline "\n".
   @return true upon success.
 **/
bool export_1dWeightTable__estensiveInputPArams__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file, const bool isTo_useJavaScript_syntax, const char *stringOf_header, const bool includeAsStandAlone, const bool isTo_append, const char *stringOf_mainStructOwner, const bool isLast, const bool isJSON);

/**
   @brief export an array of valeus to the specified data-format given when the "export_config_extensive_s_kt_matrix_t(..)" funciton is called (oekseth, 06. des. 2016).
   @param <self> is the object to use wrt. cofnigurations
   @param <arrOf_values> is the set of valeus to export
   @param <arrOf_values_size> is the number of elements in the arrOf_values
   @param <stringOf_label> is the lable to use (eg, the viarlab-enmae).
   @param <stringOf_mainStructOwner> which if used is 'used' to itnaite the data-structure, where the latter is of improtance when gnereiating JSON outputs.
 **/
void export_1dWeightTable__typeOf_uint__useConfigObject__s_kt_matrix_t(s_kt_matrix_t *self, const uint *arrOf_values, const uint arrOf_values_size, const char *stringOf_label, const char *stringOf_mainStructOwner);
/**
   @brief export an array of valeus to the specified data-format given when the "export_config_extensive_s_kt_matrix_t(..)" funciton is called (oekseth, 06. des. 2016).
   @param <self> is the object to use wrt. cofnigurations
   @param <arrOf_values> is the set of valeus to export
   @param <arrOf_values_size> is the number of elements in the arrOf_values
   @param <stringOf_label> is the lable to use (eg, the viarlab-enmae).
   @param <stringOf_mainStructOwner> which if used is 'used' to itnaite the data-structure, where the latter is of improtance when gnereiating JSON outputs.
 **/
void export_1dWeightTable__typeOf_float__useConfigObject__s_kt_matrix_t(s_kt_matrix_t *self, const float *arrOf_values, const uint arrOf_values_size, const char *stringOf_label, const char *stringOf_mainStructOwner);
/**
   @brief export a 1d-weigth-table into a TSV-format from weigth-table-list: we expect each 'weight' to be seperated by a newline "\n".
   @param <self> is the object to udpate
   @param <input_file> is the file to export the data-table into.
   @return true upon success.
 **/
bool export_1dWeightTable__formatOf_tsv__s_kt_matrix_t(s_kt_matrix_t *self, const char *input_file);
//! An explcit approach to clsoe the file-handler.
//! @rmekars an explampel-applciaiton conserns the sue-case where one is interested in 'using the result-fiel before the compelte proeprties of this object is freeed'.
void closeFileHandler__s_kt_matrix_t(s_kt_matrix_t *self);
/**
   @brief a signlce call to exprot the result-file (oekseth, 06. des. 2016).
   @param <self> is the object to 'hold' the export-file
   @param <stringOf_resultFile> which is the anem of the result-file.
   @param <fileHandler> optional: may be set for cases wehre the file-naem 'repsr ents' a complex string of meta-file-proerpteis.
   @return true upon success.
 **/
bool export__singleCall__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler);
/**
   @brief a signlce call to exprot the result-file (oekseth, 06. des. 2016).
   @param <self> is the object to 'hold' the export-file
   @param <stringOf_resultFile> which is the anem of the result-file.
   @param <fileHandler> optional: may be set for cases wehre the file-naem 'repsr ents' a complex string of meta-file-proerpteis.
   @return true upon success.
 **/
bool export__singleCall_firstTranspose__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler);
/**
   @brief a signlce call to exprot the result-file to java-script (oekseth, 06. feb. 2017).
   @param <self> is the object to 'hold' the export-file
   @param <stringOf_resultFile> which is the anem of the result-file.
   @param <fileHandler> optional: may be set for cases wehre the file-naem 'repsr ents' a complex string of meta-file-proerpteis.
   @param <stringOf_id> is the unique variable-suffix to be used
   @return true upon success.
 **/
bool export__singleCall__toFormat_javaScript__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler, const char *stringOf_id);
/**
   @enum e_kt_matrix__exportFormats_t
   @brief A new -trindoused enum used to stadnarize format-file-exports (oekseht, 06. de.s 2016).
**/
typedef enum e_kt_matrix__exportFormats {
  e_kt_matrix__exportFormats_tsvMatrix,
  e_kt_matrix__exportFormats_tex,
} e_kt_matrix__exportFormats_t;


/**
   @brief a signlce call to exprot the result-file (oekseth, 06. jan. 2017).
   @param <self> is the object to 'hold' the export-file
   @param <stringOf_resultFile> which is the anem of the result-file.
   @param <fileHandler> which if set is used to spedify the file-name to be used.
   @param <exportFormat> is sued to describe the format to exprot the data-aresutls to.
   @param <typeOf_extractToPrint> which if Not set to e_kt_matrix__exportFormats___extremeToPrint_undef is used to print an 'extreme case' wrt. the data-input
   @return true upon success.
 **/
bool extractAndExport__deviations__singleCall__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler, const bool config_isToTranspose__whenGeneratingOutput, const e_kt_matrix__exportFormats_t exportFormat, const e_kt_matrix__exportFormats___extremeToPrint_t typeOf_extractToPrint);
//! Comptue devaiotns:  exprot both a 'nroaml' and a transposed/itlted matrix (oekseth, 06. apr. 2017).
static bool extractAndExport__deviations__singleCall__tsv__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile) {
  assert(self); assert(stringOf_resultFile);
  if(self->nrows && self->ncols) {
    bool is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(self, stringOf_resultFile, NULL, /*tranpose=*/false, e_kt_matrix__exportFormats_tsvMatrix, e_kt_matrix__exportFormats___extremeToPrint_undef);
    assert(is_ok);
    return is_ok;
  } else {
    fprintf(stderr, "(info)\t the matrix is empty, ie, for whcih there is no dtata to print. Observiaonit at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return true; //! ie, as we asusme altter is a vlaid dcase.
  }
}
//! Comptue devaiotns:  exprot both a 'nroaml' and a transposed/itlted matrix (oekseth, 06. apr. 2017).
static bool extractAndExport__deviations__singleCall__transposedAndNonTransposed_tsv__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile) {
  assert(self); assert(stringOf_resultFile);
  if(self->nrows && self->ncols) {
    {
      allocOnStack__char__sprintf(2000, fileName_toExportTo, "%s.tsv", stringOf_resultFile);  //! ie, allcoate the "str_filePath".
      bool is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(self, fileName_toExportTo, NULL, /*tranpose=*/false, e_kt_matrix__exportFormats_tsvMatrix, e_kt_matrix__exportFormats___extremeToPrint_undef);
      assert(is_ok);
    }
    {
      allocOnStack__char__sprintf(2000, fileName_toExportTo, "%s_%s.tsv", stringOf_resultFile, "tranposed");  //! ie, allcoate the "str_filePath".
      //! Note: we need to explcitly transpose, ie, as the [”elow] 'transpsoe-fucniton' in oru "matrix_deviation.c" 'only' tilts/transpose the result-output (and Not wrt. the input-values).x
      s_kt_matrix_t mat_transp = setToEmptyAndReturn__s_kt_matrix_t();
      bool is_ok = init__copy_transposed__s_kt_matrix(&mat_transp, self, true);
      //assert(is_ok);
      is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(&mat_transp, fileName_toExportTo, NULL, /*tranpose=*/false, e_kt_matrix__exportFormats_tsvMatrix, e_kt_matrix__exportFormats___extremeToPrint_undef);
      //is_ok = extractAndExport__deviations__singleCall__s_kt_matrix_t(self, fileName_toExportTo, NULL, /*tranpose=*/true, e_kt_matrix__exportFormats_tsvMatrix, e_kt_matrix__exportFormats___extremeToPrint_undef);
      assert(is_ok);
      free__s_kt_matrix(&mat_transp); // FIXME: validate correctness of this      
      return is_ok;
    }
  } else {
    fprintf(stderr, "(info)\t the matrix is empty, ie, for whcih there is no dtata to print. Observiaonit at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return true; //! ie, as we asusme altter is a vlaid dcase.
  }
}
//! A Single call to exprot the result-file using a CSV-syntex without stirng-idneitifers (oekseth, 06. feb. 2017).
bool export__singleCall__formatOf__csvWithoutIndetfiers__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler);


/* //! A Single call to exprot the result-file using a CSV-syntex without stirng-idneitifers (oekseth, 06. feb. 2017). */
/* bool export__singleCall__formatOf__csvWithoutIndetfiers__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler); */


/* #ifdef __cplusplus */
/* } */
/* #endif */

#endif // !EOF
