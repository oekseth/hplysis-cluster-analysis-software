#ifndef rapidMic_core_h
#define rapidMic_core_h

#include "configure_optimization.h" //! Whcih specifies the optmizaitons and macro-configurations
#include "def_intri.h"

/* ApproxOptimizeXAxis
 * Returns the map P: D -> {0, ...,k-1}. See Algorithm 2 in SOM.
 *
 * Parameters
 *   Dx (IN) : x-data sorted in increasing order by Dx-values
 *   Dy (IN) : y-data sorted in increasing order by Dx-values
 *   n (IN) : length of Dx and Dy
 *   Qm (IN) : the map Q computed by EquipartitionYAxis sorted
 *       in increasing order by Dx-values.
 *   q (IN) : number of clumps in Qm
 *   Pm (IN) : the map P computed by GetSuperclumpsPartition
 *       sorted in increasing order by Dx-values.
 *   p (IN) : number of clumps in Pm
 *   x (IN) : grid size on x-values
 *   I (OUT) : the normalized mutual information vector. It
 *       will contain I_{k,2}, ..., I_{k, x}. I must be a
 *       preallocated array of dimension x-1.
 */
int ApproxOptimizeXAxis(t_float *Dx, t_float *Dy, int n, t_sparseVec_counts *Qm, int q, t_sparseVec_counts *Pm, int p, int x, t_float *I);

#endif
