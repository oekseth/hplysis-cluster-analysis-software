  const uint defaultValue = 0;
  //! Intiate teh distance-matrices:
  t_float **matrix_1 = allocate_2d_list_float(max_cntClusters, cnt_vertices, defaultValue);
  t_float **matrix_2 = allocate_2d_list_float(max_cntClusters, cnt_vertices, defaultValue);
  for(uint i = 0; i < max_cntClusters; i++) {
    //= clusterId_toCentroid_2[i] = UINT_MAX;
    for(uint k = 0; k < cnt_vertices; k++) {matrix_1[i][k] = matrix_2[i][k] = T_FLOAT_MAX;}
  }
  uint max_cntClusterFoundFor_1 = UINT_MAX;
  uint max_cntClusterFoundFor_2 = UINT_MAX;
  { //! Intiate teh clsuter-mapping, ie, 'map' a cluster-to-centrodi-mappings into a [cluster-id][vertex-id]='0|1':
    {//! First for the 'first matrix':
      //      printf("\n\n----------------------------------------- matrix-1, at %s:%d\n", __FILE__, __LINE__);
#define localMatrix matrix_1
#define vertexToCentroid mapOf_vertexToCentroid1
#define localMaxCount__clusters max_cntClusterFoundFor_1
#define countArray self->mapOfCluster1
      t_float **matrixOf_coOccurence = matrixOf_coOccurence_1;
      assert(self->mapOfCluster1_size != UINT_MAX);
      //const uint old_size = self->mapOfCluster1_size;
#define countArray_size self->mapOfCluster1_size
#define clusterDistances_cluster self->clusterDistances_cluster1
      //printf("(\t init clusterDistances_cluster1, at %s:%d\n", __FILE__, __LINE__); // FIXME: remove
    //!  --------------------
      if(false) { //! Investigate that the matrix has at least one interesting data-set (oekseth, 06. feb. 2017).
      assert(cnt_vertices > 0);
      long long int cnt_interesting_1 = 0;
      long long int cnt_interesting_2 = 0;
      const uint nrows = cnt_vertices;
      const uint ncols = cnt_vertices;
      for(uint i = 0; i < nrows; i++) {
	for(uint j = 0; j < ncols; j++) {
	  cnt_interesting_1 += isOf_interest(matrixOf_coOccurence_1[i][j]); 
	  cnt_interesting_2 += isOf_interest(matrixOf_coOccurence_2[i][j]);
	} }	  
      printf("(cnt-interesting(%llu and %llu), nrows=countArray_size=%u, at %s:%d\n", cnt_interesting_1, cnt_interesting_2, countArray_size, __FILE__, __LINE__); // FIXME: remoe.
    }
    //!  --------------------
#include "kt_matrix_cmpCluster__studb__vertexClusters_to_clusterVertexMatrix.c"
      //self->mapOfCluster1_size = old_size;
      assert(self->mapOfCluster1_size != UINT_MAX);
      assert(self->mapOfCluster1_size != 0);
      //printf("cnt-clusters=%u, at %s:%d\n", self->mapOfCluster1_size, __FILE__, __LINE__);
      if(matrixOf_coOccurence_1 != NULL) {
	assert(self->clusterDistances_cluster1.nrows != 0);
      }

    }
    //    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);
    {//! Thereafter for the 'second matrix':
      //printf("\n\n----------------------------------------- matrix-2, at %s:%d\n", __FILE__, __LINE__);
#define localMatrix matrix_2
#define vertexToCentroid mapOf_vertexToCentroid2
#define localMaxCount__clusters max_cntClusterFoundFor_2
#define countArray self->mapOfCluster2
      t_float **matrixOf_coOccurence = matrixOf_coOccurence_2;
      //const uint old_size = self->mapOfCluster2_size;
      //#define countArray_size old_size
#define countArray_size self->mapOfCluster2_size
#define clusterDistances_cluster self->clusterDistances_cluster2
      // printf("(\t init clusterDistances_cluster2, at %s:%d\n", __FILE__, __LINE__); // FIXME: remove
#include "kt_matrix_cmpCluster__studb__vertexClusters_to_clusterVertexMatrix.c"
      //self->mapOfCluster2_size = old_size;
      assert(self->mapOfCluster2_size != UINT_MAX);
      assert(self->mapOfCluster2_size != 0);
      if(matrixOf_coOccurence != NULL) {
	; // assert(self->clusterDistances_cluster2.nrows != 0);
      }
	//printf("cnt-clusters=%u, at %s:%d\n", self->mapOfCluster2_size, __FILE__, __LINE__);
    }
  }

//    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //! --------------------------
  //! 
  {//! Copmute the adjcency-matrix
    //! note: in [”elow] we implictly compute 'many:many' wrt. the dot-prodcut and validate the results, ie, if (a) all valeus are set to '1' OR if (isTo_includeWeight == 0).
    assert(max_cntClusterFoundFor_1 != UINT_MAX); //! ie, as we expec tthe latter to be set.
    assert(max_cntClusterFoundFor_1 <= max_cntClusters); //! ie, as we expec tthe latter to be set.
    assert(max_cntClusterFoundFor_1 != 0); //! ie, as we expec tthe latter to be set.
    assert(max_cntClusterFoundFor_1 < 1024); //! ie, as we otherwise should consider using our efficent prodceudre.
    assert(max_cntClusterFoundFor_2 < 1024); //! ie, as we otherwise should consider using our efficent prodceudre.
    assert(cnt_vertices < 1000*124); //! ie, as we otherwise should consider using our efficent prodceudre.
    //! Itnaite the local matrix:
    init__s_kt_matrix(&(self->matrixOf_coOccurence), /*nrows=*/max_cntClusterFoundFor_1, /*ncols=*/max_cntClusterFoundFor_2, /*isTo_allocateWeightColumns=*/false);
    init__s_kt_matrix(&(self->matrixOf_coOccurence_or), /*nrows=*/max_cntClusterFoundFor_1, /*ncols=*/max_cntClusterFoundFor_2, /*isTo_allocateWeightColumns=*/false);

    { //! Set all valeus to '0' if they are set to T_FLOAT_MAX:
      for(uint head_id = 0; head_id < max_cntClusterFoundFor_1; head_id++) {
	for(uint i = 0; i < cnt_vertices; i++) {	    
	  const t_float score_1 = matrix_1[head_id][i];
	  if(!isOf_interest(score_1)) {
	    matrix_1[head_id][i] = 0;
	  }
	}
      }
      for(uint head_id = 0; head_id < max_cntClusterFoundFor_2; head_id++) {
	for(uint i = 0; i < cnt_vertices; i++) {	    
	  const t_float score_2 = matrix_2[head_id][i];
	  if(!isOf_interest(score_2)) {
	    matrix_2[head_id][i] = 0;
	  }
	}
      }
    }
    //!
    //! Thereafter iterate:
    for(uint head_id = 0; head_id < max_cntClusterFoundFor_1; head_id++) {
      for(uint tail_id = 0; tail_id < max_cntClusterFoundFor_2; tail_id++) {
	t_float sumOf_values = 0;
	t_float sumOf_values_or = 0;
	if(isTo_includeWeight) {
	  for(uint i = 0; i < cnt_vertices; i++) {	    
	    const t_float score_1 = matrix_1[head_id][i];
	    const t_float score_2 = matrix_2[tail_id][i];
	    //if(isOf_interest(score_1)  && isOf_interest(score_2) ) 
	    {	    
	      assert_possibleOverhead(score_1 != T_FLOAT_MAX);
	      assert_possibleOverhead(score_2 != T_FLOAT_MAX);
	      const t_float local_score = macro_mul(score_1, score_2);
	      sumOf_values += local_score;
	      // FIXME: disucss with JC correctness of [below] (oekseth, 06. des. 2016).
	      sumOf_values_or += macro_max(local_score, macro_max(score_1, score_2));
	    }
	  }
	} else {
	  for(uint i = 0; i < cnt_vertices; i++) {	    
	    assert_possibleOverhead(matrix_1[head_id][i] != T_FLOAT_MAX);
	    assert_possibleOverhead(matrix_2[tail_id][i] != T_FLOAT_MAX);
	    const char score_1 = (char)matrix_1[head_id][i];
	    const char score_2 = (char)matrix_2[tail_id][i];
	    //printf("\t[%u][%u] += %f=[%u], at %s:%d\n", head_id, tail_id, (t_float)(score_1 & score_2), i, __FILE__, __LINE__);
	    sumOf_values += (score_1 & score_2); //! ie, 'an itnerseciton if both valeus are set'.	    
	    sumOf_values_or += (score_1 | score_2);
	  }
	}
	//!
	//! update the result:
	//printf("\t[%u][%u] = %f, at %s:%d\n-------------------------\n", head_id, tail_id, sumOf_values, __FILE__, __LINE__);
	self->matrixOf_coOccurence.matrix[head_id][tail_id] = sumOf_values;
	self->matrixOf_coOccurence_or.matrix[head_id][tail_id] = sumOf_values_or;
      }
    }
  }
//    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);  

#if(configureDebug_useExntensiveTestsIn__tiling == 1)
  //! --------------------------
  //! 
  //! Valide the result: Compare with  out 'default all-agaisnta-ll-appraoch' ... vlaidating the 'dot-product-metirc' in the latter:
const t_float default_value_float = 0;
assert(max_cntClusterFoundFor_1 > 0);
assert(max_cntClusterFoundFor_2 > 0);
t_float **matrix_debug_result = allocate_2d_list_float(max_cntClusterFoundFor_1, max_cntClusterFoundFor_2, default_value_float);
assert(matrix_debug_result);
  s_allAgainstAll_config_t obj_config; setTo_empty__s_allAgainstAll_config(&obj_config);
  obj_config.iterationIndex_2 = max_cntClusterFoundFor_2;
//printf("max_cntClusterFoundFor_1=%u, max_cntClusterFoundFor_2=%u, at %s:%d\n", max_cntClusterFoundFor_1, max_cntClusterFoundFor_2, __FILE__, __LINE__);
  kt_compute_allAgainstAll_distanceMetric(/*metric=*/e_kt_correlationFunction_groupOf_innerProduct_innerProduct, /*typeOf_correlationPreStep=*/
					  (isTo_includeWeight) ? e_kt_categoryOf_correaltionPreStep_binary : e_kt_categoryOf_correaltionPreStep_none, 
					  /*nrows=*/max_cntClusterFoundFor_1,
					  /*ncols=*/cnt_vertices, 
					  matrix_1, matrix_2,
					  /*resultMatrix=*/matrix_debug_result,
					  obj_config,
					  NULL);

  //!
  //! Validate [above] result wr.t correctness:
  for(uint head_id = 0; head_id < max_cntClusterFoundFor_1; head_id++) {
    for(uint tail_id = 0; tail_id < max_cntClusterFoundFor_2; tail_id++) {
      const t_float first  = self->matrixOf_coOccurence.matrix[head_id][tail_id];
      const t_float second = matrix_debug_result[head_id][tail_id];
      //printf("[%u][%u] = %f VS %f, at %s:%d\n", head_id, tail_id, first, second, __FILE__, __LINE__);
      if(second != T_FLOAT_MAX) {
	assert(first == second);
      } else { //! then we assuem teh 'first' is Not set:
	assert( 
	       (first == 0) || (first == T_FLOAT_MAX)
		);
      }
    }
  }

  //! De-allocate:
  free_2d_list_float(&matrix_debug_result);
#endif

//    fprintf(stderr, "at %s:%d\n", __FILE__, __LINE__);

  //! ----------------------------------------------------------------
  //!
  //! De-allocate:
free_2d_list_float(&matrix_1, max_cntClusters);
free_2d_list_float(&matrix_2, max_cntClusters);
