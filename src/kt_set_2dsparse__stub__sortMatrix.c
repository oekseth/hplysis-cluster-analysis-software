
  uint *arr_index = NULL; 
  t_type *arr_index_localScores = NULL;
  uint max_col_size = 0;
const uint default_value_uint = UINT_MAX;     //const uint default_value_float = ;
  for(uint row_id = 0; row_id < nrows; row_id++) {
    const uint row_size = __MiF__get_rowSize(row_id);
    if(row_size > max_col_size) { max_col_size = row_size; }
  }
  
  if( (matrixOf_scores != NULL) && isTo_sortKeys) {
    uint empty_0 = 0; 
    arr_index = allocate_1d_list_uint(max_col_size, empty_0);

    arr_index_localScores = __MiF__memScore_alloc(max_col_size);
    for(uint i = 0; i < max_col_size; i++) {
      arr_index_localScores[i] = t_type_max;
      arr_index[i] = default_value_uint;
    }
  }

#if(optimize_parallel_useNonSharedMemory_MPI == 1)
  #error "Update this code-chunk with new/updated code" 
#endif
#if(optimize_parallel_2dLoop_useFast == 1)
#error "Add and test the effoect of parllel wrappers for this ... ie, after we have completed hte 'ordinary' perofmrance-tests"
#endif

  assert(max_col_size != 0);

  uint *row_tmp_uint = allocate_1d_list_uint(max_col_size, default_value_uint);
const t_type empty_0 = 0;
t_type *row_tmp_float =  __MiF__memScore_alloc(max_col_size);
//  t_type *row_tmp_float = allocate_1d_list_float(max_col_size, default_value_float);
for(uint i = 0; i < max_col_size; i++) {
  row_tmp_uint[i] = default_value_uint;
  row_tmp_float[i] = t_type_max;
 }

  for(uint row_id = 0; row_id < nrows; row_id++) {
    const uint row_size = __MiF__get_rowSize(row_id);
    //const uint row_size = self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_currentPos] + 1;
    assert(row_size <= max_col_size);
    uint *row = __MiF__get_row(row_id);
    
    //! Apply the sorting:
    if(isTo_sortKeys == true) {
      quicksort_uint(row_size, /*input=*/row, /*arr_index=*/arr_index, /*arr_result=*/row_tmp_uint);
      memcpy(row, row_tmp_uint, sizeof(uint)*row_size);
      if(matrixOf_scores != NULL) {    
	assert(arr_index);
	//! Construct a locall copy, wa copye where we hcange the index-locatiosn:
	for(uint i = 0; i < row_size; i++) {
	  arr_index_localScores[arr_index[i]] = __MiF__get_rowScores_pair(row_id, i); //matrixOf_scores[row_id][i];
	}	
	//! Udpat ethe result wr.t thsi:
	__MiF__copyIntoGlobal_rowScores(row_id);
      }
    } else {
      assert(matrixOf_scores != NULL);
      t_type *row = __MiF__get_rowScores(row_id);
      __MiF__get_apply_sort_forScores();
      memcpy(row, row_tmp_float, sizeof(t_type)*row_size);
      //self->matrixOf_scores[row_id] = row;
    }
  }
  if( (matrixOf_scores != NULL) && isTo_sortKeys) {
    assert(arr_index);
    free_1d_list_uint(&arr_index);
    __MiF__memScore_free(&arr_index_localScores);
    //free_1d_list_float(&arr_index_localScores);
  }
  //! ------------------------
  //! De-allocate:
assert(row_tmp_uint);
free_1d_list_uint(&row_tmp_uint);  
assert(row_tmp_float);
__MiF__memScore_free(&row_tmp_float);
//free_1d_list_float(&row_tmp_float);

#undef t_type
#undef t_type_max
#undef __MiF__memScore_alloc
#undef __MiF__memScore_free
#undef __MiF__get_rowSize
#undef __MiF__get_row
#undef __MiF__get_apply_sort_forScores
#undef __MiF__get_rowScores
#undef __MiF__get_rowScores_pair
#undef __MiF__copyIntoGlobal_rowScores
