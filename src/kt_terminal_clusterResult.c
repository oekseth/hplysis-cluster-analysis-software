#include "kt_terminal_clusterResult.h"
/* 
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
#include "kt_api.h"
#include "kt_forest_findDisjoint.h"
//#include "kt_clusterAlg_hca.h"


//! @return the enum-representaiotn descrinbg the result-format to use (oekseth, 06. ds. 2016)
e_kt_terminal_clusterResult__format_t get_enumType__e_kt_terminal_clusterResult__format_t(const bool isTo_inResult_isTo_useJavaScript_syntax, const bool isTo_inResult_isTo_useJSON_syntax, const bool isTo_useMatrixFormat) {
  if(isTo_inResult_isTo_useJSON_syntax) {return e_kt_terminal_clusterResult__format_JSON;}
  else if(isTo_inResult_isTo_useJavaScript_syntax) {return e_kt_terminal_clusterResult__format_javaScript;}
  else { //! ie, the default result-format-alternative.
    if(isTo_useMatrixFormat) {return e_kt_terminal_clusterResult__format_tsv_matrix;}
    else {return e_kt_terminal_clusterResult__format_tsv_relation;}
  }
}

/**
   @brief intiates the "s_kt_clusterResult_data_t" strucutre (oekseth, 06. des. 201&)
   @param <self> is the object to intaite.
   @param <nrows> is the diemsion of the amtrix: we expect the result-matrix to be in "[nrows, nrows]" diemsion.
   @param <for_som> which is to be set if this result-option is to be epxorted
   @param <for_kMeans> which is to be set if this result-option is to be epxorted
   @param <for_hca> which is to be set if this result-option is to be epxorted
   @param <for_pca> which is to be set if this result-option is to be epxorted
 **/
void init__s_kt_clusterResult_data(s_kt_clusterResult_data_t *self, const uint nrows, bool for_som, bool for_kMeans, bool for_hca, bool for_pca) {
  assert(self);
  if( (nrows == 0) || (nrows == UINT_MAX) ) {
    fprintf(stderr, "!!\t No rows were defined: plese ivnestigate correctness of your api-call. For questions please contac tthe developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  //! --------------------------- 
  //! Intiatie:
  self->nrows = nrows;
  //!
  //! The configuraitons;
  self->for_som = for_som;
  self->for_kMeans = for_kMeans;
  self->for_hca = for_hca;
  self->for_pca = for_pca;
  //! --------------------
  //!
  const char default_value = 0; //! ie, by default assume that all vertices are part of the same clsuter
  //! Allocations:
  self->cnt_dataObjects_toExport = 1; //! ie, as we always asusme the corleaiotn-amtrix is to be exported.
  self->distancesIn_2d_correlationMatrix = allocate_2d_list_float(nrows, nrows, default_value);
  if(for_som) {
    self->cnt_dataObjects_toExport += 4; //! ie, incrmeent the number of data-objects to exprot.
    self->map_1dOf_vertexTypes_partOfCluster__som__centroidId = allocate_1d_list_uint(nrows, default_value);
    self->map_1dOf_vertexTypes_partOfCluster__som_x = allocate_1d_list_uint(nrows, default_value);
    self->map_1dOf_vertexTypes_partOfCluster__som_y = allocate_1d_list_uint(nrows, default_value);
    self->distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix = allocate_2d_list_float(nrows, nrows, default_value);
    self->cnt_SOM_x_added = 0; //! which we ause as an'offset' wrt. different dsijotin-regions.
  } else {
    self->map_1dOf_vertexTypes_partOfCluster__som__centroidId = NULL;
    self->map_1dOf_vertexTypes_partOfCluster__som_x = NULL;
    self->map_1dOf_vertexTypes_partOfCluster__som_y = NULL;
    self->distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix = NULL;
    self->cnt_SOM_x_added = 0; //! which we ause as an'offset' wrt. different dsijotin-regions.
  }
  if(for_kMeans) {
    self->cnt_dataObjects_toExport += 2; //! ie, incrmeent the number of data-objects to exprot.
    self->map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId = allocate_1d_list_uint(nrows, default_value);
    self->distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid = allocate_2d_list_float(nrows, nrows, default_value);
  } else {
    self->map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId = NULL;
    self->distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid = NULL;
  }
  if(for_hca) {
    self->cnt_dataObjects_toExport += 3; //! ie, incrmeent the number of data-objects to exprot.
    self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount = allocate_1d_list_float(nrows, default_value);
    self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum = allocate_1d_list_float(nrows, default_value);
    self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology = allocate_2d_list_float(nrows, nrows, default_value);
  } else {
    self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount = NULL;
    self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum = NULL;
    self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology = NULL;
  }
  if(for_pca) {
    self->cnt_dataObjects_toExport += 1; //! ie, incrmeent the number of data-objects to exprot.
    self->distancesIn_2d_pca_appliedToCorrelationMatrix = allocate_2d_list_float(nrows, nrows, default_value);
  } else {
    self->distancesIn_2d_pca_appliedToCorrelationMatrix = NULL;
  }
  //! --------------------

  // self->distancesIn_2d_ = allocate_2d_list_float(nrows, nrows, default_value);
  //! --------------------
}


/**
   @brief exports the data-set for all formats
   @param <self> is the object to intaite.
   @param <nrows> is the diemsion of the amtrix: we expect the result-matrix to be in "[nrows, nrows]" diemsion.
   @remarks a generalizaiton of the "init__s_kt_clusterResult_data(..)" function.
**/
void init_exportForAllFormats_s_kt_clusterResult_data(s_kt_clusterResult_data_t *self, const uint nrows) {
  init__s_kt_clusterResult_data(self, nrows, true, true, true, true);
}
/**
   @brief de-allocates the s_kt_clusterResult_data_t object
 **/
void free__s_kt_clusterResult_data(s_kt_clusterResult_data_t *self) {
  assert(self);
  assert(self->nrows > 0); //! ie, as we for simplcity assume [below] object has been inlaised with valeus.
  //! ---------------
  if(self->map_1dOf_vertexTypes_partOfCluster__som__centroidId) {free_1d_list_uint(&(self->map_1dOf_vertexTypes_partOfCluster__som__centroidId)); self->map_1dOf_vertexTypes_partOfCluster__som__centroidId = NULL;;}
  if(self->map_1dOf_vertexTypes_partOfCluster__som_x) {free_1d_list_uint(&(self->map_1dOf_vertexTypes_partOfCluster__som_x)); self->map_1dOf_vertexTypes_partOfCluster__som_x = NULL;;}
  if(self->map_1dOf_vertexTypes_partOfCluster__som_y) {free_1d_list_uint(&(self->map_1dOf_vertexTypes_partOfCluster__som_y)); self->map_1dOf_vertexTypes_partOfCluster__som_y = NULL;;}
  if(self->map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId) {free_1d_list_uint(&(self->map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId)); self->map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId = NULL;;}
  if(self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount) {  free_1d_list_float(&(self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount)); self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount = NULL;;}
  if(self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum) {  free_1d_list_float(&(self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum)); self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum = NULL;;}
  //! --------------------
  if(self->distancesIn_2d_correlationMatrix) {free_2d_list_float(&(self->distancesIn_2d_correlationMatrix), self->nrows); self->distancesIn_2d_correlationMatrix = NULL;;}
  if(self->distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix) {free_2d_list_float(&(self->distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix), self->nrows); self->distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix = NULL;;}
  if(self->distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid) {free_2d_list_float(&(self->distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid), self->nrows); self->distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid = NULL;;}
  if(self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology) {free_2d_list_float(&(self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology), self->nrows); self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology = NULL;;}
  if(self->distancesIn_2d_pca_appliedToCorrelationMatrix) {free_2d_list_float(&(self->distancesIn_2d_pca_appliedToCorrelationMatrix), self->nrows); self->distancesIn_2d_pca_appliedToCorrelationMatrix = NULL;}
  //! --------------------
  self->nrows = 0;
}

/**
   @brief update the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void updateClusterResults__kMeans__s_kt_clusterResult_data_t(s_kt_clusterResult_data_t *self, const s_kt_clusterAlg_fixed_resultObject *clusterResults, const uint *keyMap_localToGlobal) {
  assert(self); assert(clusterResults); 
  assert(self->map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId); //! ie, as we expect 'this' to have been intiated
  assert(self->distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid);
  assert(clusterResults->matrix2d_vertexTo_clusterId);
  
  // FIXME: vlaidate [”elow] assumption ... ie, where we assume that each 'vertex' points to a 'vertex-centroid'
  uint *vertex_clusterId = clusterResults->vertex_clusterId;
  assert(vertex_clusterId);
  if(keyMap_localToGlobal != NULL) {assert(self->nrows > clusterResults->cnt_vertex);}
  else {assert(self->nrows == clusterResults->cnt_vertex);}

  //! Iterate:
  for(uint i = 0; i < clusterResults->cnt_vertex; i++) {
    const uint global_id_head = (keyMap_localToGlobal) ? keyMap_localToGlobal[i] : i;
    const uint centroid_id = vertex_clusterId[i];
    const uint global_id_tail = (keyMap_localToGlobal) ? keyMap_localToGlobal[centroid_id] : centroid_id;
    //!
    //! Update the 'vertex-->centroid' mapping-table:
    self->map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId[global_id_head] = global_id_tail;
    //!
    //! Update the 'distance-matrix from teh verteix to the differnet centroids' (eg, used to simplify a users assessment of the k-means clsuters staiblity/importance).
    //const uint local_cluster_id = clusterResults->cluster_vertexCentroid[
    for(uint local_cluster_id = 0; local_cluster_id < clusterResults->clusterId_size; local_cluster_id++) {
      const uint local_centroidVertex = clusterResults->cluster_vertexCentroid[local_cluster_id];
      assert(local_cluster_id != UINT_MAX);
      assert(local_cluster_id < clusterResults->cnt_vertex);
      const uint global_centroidVertex = (keyMap_localToGlobal) ? keyMap_localToGlobal[local_centroidVertex] : local_centroidVertex;
      assert(global_centroidVertex != UINT_MAX);
      assert(global_centroidVertex < clusterResults->cnt_vertex);
      //!
      //! Update the 'ammpign'b etwene teh vertex adn the different cadndiate-centorid-vertices which we evlauyated (in hte processing):
      self->distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid[global_id_head][global_centroidVertex] = clusterResults->matrix2d_vertexTo_clusterId[i][local_cluster_id];          
      // TODO: cosndier to 'add' for "cluster_vertexCount" using "cluster_vertexCentroid" ... and for ... "cluster_errors" using "cluster_vertexCentroid"
      
    }
  }
}

/**
   @brief update the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @param <nygrid> is the number of grid-postion in the x-drieicotn.
   @param <nxgrid> is the number of grid-postion in the y-drieicotn.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void updateClusterResults__SOM__s_kt_clusterResult_data_t(s_kt_clusterResult_data_t *self, const s_kt_clusterAlg_SOM_resultObject_t *clusterResults, const uint *keyMap_localToGlobal, const uint nxgrid, const uint nygrid) {
  assert(self); assert(clusterResults); 
  assert(self->map_1dOf_vertexTypes_partOfCluster__som__centroidId);
  assert(self->map_1dOf_vertexTypes_partOfCluster__som_x);
  assert(self->map_1dOf_vertexTypes_partOfCluster__som_y);
  assert(self->distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix);
  //!
  //! Iterate through the rows and idneitfy the values assicated to the data-sets:
  //! Iterate:
  for(uint i = 0; i < clusterResults->nrows; i++) {
    uint centroid_id = UINT_MAX; //vertex_clusterId[i];
    uint grid_x = UINT_MAX;
    uint grid_y = UINT_MAX;
    t_float score = T_FLOAT_MAX;
    const bool is_ok = get_vertex_clusterPair__s_kt_clusterAlg_SOM_resultObject_t(clusterResults, /*row=*/i, &grid_x, &grid_y, &centroid_id, &score);
    assert(is_ok == true);
    const uint global_id_head = (keyMap_localToGlobal) ? keyMap_localToGlobal[i] : i;
    const uint global_id_tail = (keyMap_localToGlobal) ? keyMap_localToGlobal[centroid_id] : centroid_id;
    //!
    //! Update the 'vertex-->centroid' mapping-table:
    self->map_1dOf_vertexTypes_partOfCluster__som__centroidId[global_id_head] = global_id_tail;    
    assert(grid_x != UINT_MAX);
    self->map_1dOf_vertexTypes_partOfCluster__som_x[global_id_tail] = grid_x + self->cnt_SOM_x_added;
    self->map_1dOf_vertexTypes_partOfCluster__som_y[global_id_tail] = grid_y; //! ie, as we choose the x-driection wrt. the 'offset'.
    //! Set the score:
    self->distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix[global_id_head][global_id_tail] = score; //! ie, to 'ensure' that hte 'centorid becomes the center-point'.
    //const uint global_id_tail = (keyMap_localToGlobal) ? keyMap_localToGlobal[centroid_id] : centroid_id;

  }

  self->cnt_SOM_x_added += nxgrid; //! ie, to 'seprate different inseritons wrt. data-sets'.
}

/**
   @brief update the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @param <nygrid> is the number of grid-postion in the x-drieicotn.
   @param <nxgrid> is the number of grid-postion in the y-drieicotn.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void updateClusterResults__HCA__s_kt_clusterResult_data_t(s_kt_clusterResult_data_t *self, const s_kt_clusterAlg_hca_resultObject_t *clusterResults, const uint *keyMap_localToGlobal) {
  assert(self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology);
  //! Update our score-matrix:
  constructAdjcencencyMatrix_fromNodeTable__kt_clusterAlg_hca(clusterResults, 
							      //NULL,
							      self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology, 
							      self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount,
							      self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum,
							      self->nrows, //! ie, the 'glboal max' wr.t the row.
							      keyMap_localToGlobal, false);

}

/**
   @brief write out the cotnents of the s_kt_clusterResult_data_t object usign the format specifed in the input-argumetns
   @param <self> is the object to to use.
   @param <matrix> is the correlation-matrix which we export.
   @param <resultFormat> is the reuslt-format to exprot the clsuter-results into.
   @param <resultPrefix> is teh file-rpefix to be sued: if "resultPrefix == NULL" then we write out the reseult to the stream_out "stdout" stream
   @param <stream_out> if "stream_out " is used then we use the latter in the result-generation: an example of "stream_out" is if the latter is sset to stdout
   @return true upon success
 **/
bool writeOut__s_kt_clusterResult_data_t(const s_kt_clusterResult_data_t *self, s_kt_matrix_t *matrix, const e_kt_terminal_clusterResult__format_t resultFormat, const char *resultPrefix, FILE *stream_out) {
  assert(self);
  assert(self->cnt_dataObjects_toExport >= 1); //! ie, what we expect.  
  if(self->cnt_dataObjects_toExport == 0) {
    return false; //! ie, as no objects wer5e providced for data-export.
  }

  if(!matrix || !matrix->nrows || !matrix->ncols) {
    const char *error = "we expected the matrix-dimensions to be set ofr both the rows and columns, a case which does Not hold; we will now abort the exprot-generation (of your cluster-results)";
    fprintf(stderr, "!!\t Your input was not as expcted: %s; for details please contract the develoepr at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", error, __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as the input-spec was Not as expected.
  }

  if(!matrix->nameOf_rows || !matrix->nameOf_columns) {
    const char *error = "we expected the row-names and column-names to be set: in contrast the names were not set, ie, your result may be less infromative (as we are using the internal indeices arther than more informative entity-names) when generating the reuslt-set: in brief we continue the exprot-generation (though with an expected increased enotrpy wrt. the result)";
    fprintf(stderr, "!!\t (non-critical)Your input was not as expcted: %s; for details please contract the develoepr at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", error, __FUNCTION__, __FILE__, __LINE__);
  }

  if(matrix->nrows != self->nrows) {
    const char *error = "The number of rows in your input-matrix did Not reflect the number of rows in the internal dat-astrucutre, ie, an API-error (either in your code or in the library your are using); we will now abort the export-generation (of your cluster-results)";
    fprintf(stderr, "!!\t Your input was not as expcted: %s; for details please contract the develoepr at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", error, __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as the input-spec was Not as expected.
  }

  assert(self); 
  //assert(stringOf_dataLabel); assert(strlen(stringOf_dataLabel));   
  const uint cnt_objs_toExport = self->cnt_dataObjects_toExport; //inputMatrix_result.nrows;
  assert(cnt_objs_toExport > 0);
  

  // printf("-------------- cnt_objs_toExport=%u, at %s:%d\n", cnt_objs_toExport, __FILE__, __LINE__);

  if(cnt_objs_toExport > 0) {
    //! --------------------
    //!
    //! Initate:
    const char *stringOf_dataLabel = "clusterExport.hpLysis";
    const char *stringOf_id = stringOf_dataLabel; //"inputData";
    const char *suffix = "tsv";
    
    const bool isTo_useMatrixFormat = (resultFormat == e_kt_terminal_clusterResult__format_tsv_matrix);
    const bool isTo_inResult_isTo_useJavaScript_syntax = (resultFormat == e_kt_terminal_clusterResult__format_javaScript);
    const bool isTo_inResult_isTo_useJSON_syntax = (resultFormat == e_kt_terminal_clusterResult__format_JSON);
    if(stream_out == NULL) {
      if(isTo_inResult_isTo_useJavaScript_syntax) {
	suffix = "js";
      } else if( isTo_inResult_isTo_useJSON_syntax) {
	suffix = "json";
      } 
      const char *stringOf_resultFile = resultPrefix; //stringOf_resultFile;
      if(!stringOf_resultFile || !strlen(stringOf_resultFile)) {
	stringOf_resultFile = "result_hpLysis";
	fprintf(stderr, "!!\t Note: the file-prefix for the result-file(s) was Not set: we therefore use the prefix=\"%s\" when generating your result-files: for questions pelase cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
      }
      assert(strlen(stringOf_resultFile) < 1000); //! ie, givne [”elow] assumption
      char local_stringOf_resultFile[1300] = {'\0'}; 
      sprintf(local_stringOf_resultFile, "%s.%s.%s", stringOf_resultFile, stringOf_dataLabel, suffix);

      //! Update the object:
      export_config_extensive_s_kt_matrix_t(matrix, local_stringOf_resultFile, isTo_useMatrixFormat, isTo_inResult_isTo_useJavaScript_syntax, isTo_inResult_isTo_useJSON_syntax, /*matrix-variable-suffix=*/stringOf_id, 
					    false, //(bool)(stringOf_id != NULL), 
					    /*cnt_dataObjects_toGenerate=*/cnt_objs_toExport); 
      
    } else { //! then we export to the stream sepcified by the user:
      if(stream_out != NULL) {
	matrix->stream_out = stream_out;
      }
      //! Update the object:
      export_config_extensive_s_kt_matrix_t(matrix, NULL, isTo_useMatrixFormat, isTo_inResult_isTo_useJavaScript_syntax, isTo_inResult_isTo_useJSON_syntax, /*matrix-variable-suffix=*/stringOf_id, 
					    false, //(bool)(stringOf_id != NULL), 
					    /*cnt_dataObjects_toGenerate=*/cnt_objs_toExport); 
      if(stream_out != NULL) {
	matrix->stream_out = stream_out;
      }
    }
    // printf("\n\n # exports the input-matrix to file=\"%s\", at %s:%d\n", local_stringOf_resultFile, __FILE__, __LINE__);
      
    //export_config__s_kt_matrix_t(&(self->inputMatrix_result), local_stringOf_resultFile);

    //! --------------------
    //!
    //! Export:
    { //! Export the correlation-matrix:
      export__dataObj_seperateFromFileObj__s_kt_matrix_t(matrix, matrix, /*stringOf_header=*/"distancesIn_2d_correlationMatrix");
      // TODO: cosnider to also add support for exporting centlriatiy-scores from/for [ªbove] correlation-matrix.
    }
    if(self->for_kMeans) {
      //!
      //! Epxort: the 'celuster-vertex-cenotrid-memberships' assicated to each vertex:
      export_1dWeightTable__typeOf_uint__useConfigObject__s_kt_matrix_t(matrix, /*uint-arr=*/self->map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId, /*size=*/self->nrows, /*stringOf_header=*/"map_1dOf_vertexTypes_partOfCluster__kMeans__centroidId", /*label-wrapper=*/NULL);      
      //!
      //! Then export the matrix descignb the simlairty between each 'vertex' adn each 'ptuative vertex-cluster-centroid':
      s_kt_matrix_t matrix_tmp; setTo_empty__s_kt_matrix_t(&matrix_tmp);
      matrix_tmp.matrix = self->distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid;
      matrix_tmp.nrows = self->nrows;       matrix_tmp.ncols = self->nrows;
      export__dataObj_seperateFromFileObj__s_kt_matrix_t(matrix, &matrix_tmp, /*stringOf_header=*/"distancesIn_2d_clusterFrom_kMeans_to_vertexCentroid"); 
    }
    if(self->for_som) {
      //!
      //! Export: the 'celuster-vertex-cenotrid-memberships' assicated to each vertex:
      export_1dWeightTable__typeOf_uint__useConfigObject__s_kt_matrix_t(matrix, /*uint-arr=*/self->map_1dOf_vertexTypes_partOfCluster__som__centroidId, /*size=*/self->nrows, /*stringOf_header=*/"map_1dOf_vertexTypes_partOfCluster__som__centroidId", /*label-wrapper=*/NULL);      
      export_1dWeightTable__typeOf_uint__useConfigObject__s_kt_matrix_t(matrix, /*uint-arr=*/self->map_1dOf_vertexTypes_partOfCluster__som_x, /*size=*/self->nrows, /*stringOf_header=*/"map_1dOf_vertexTypes_partOfCluster__som_x", /*label-wrapper=*/NULL);      
      export_1dWeightTable__typeOf_uint__useConfigObject__s_kt_matrix_t(matrix, /*uint-arr=*/self->map_1dOf_vertexTypes_partOfCluster__som_y, /*size=*/self->nrows, /*stringOf_header=*/"map_1dOf_vertexTypes_partOfCluster__som_y", /*label-wrapper=*/NULL);      
      //!
      //! Then export the matrix descignb the simlairty between each 'vertex' adn each 'ptuative vertex-cluster-centroid':
      s_kt_matrix_t matrix_tmp; setTo_empty__s_kt_matrix_t(&matrix_tmp);
      matrix_tmp.matrix = self->distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix;
      matrix_tmp.nrows = self->nrows;       matrix_tmp.ncols = self->nrows;
      // TODO: cosndier to 'drop' exporting wr.t this 'case' ... ie, as where 'ehre' are exproting the "som[x][y][vertex] = score" .... ie, where a 1d-list could instead be mroe correct ...  
      export__dataObj_seperateFromFileObj__s_kt_matrix_t(matrix, &matrix_tmp, /*stringOf_header=*/"distancesIn_2d_clusterFrom_SOM_eachInternalScoreMatrix"); 
    }
    if(self->for_hca) {
      assert(self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount);
      assert(self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum);
      assert(self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology);
      //!
      //! Export: the 'tree-topology-properties' assicated to each vertex:
      export_1dWeightTable__typeOf_float__useConfigObject__s_kt_matrix_t(matrix, /*uint-arr=*/self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount, /*size=*/self->nrows, /*stringOf_header=*/"map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__nodeCount", /*label-wrapper=*/NULL);      
      export_1dWeightTable__typeOf_float__useConfigObject__s_kt_matrix_t(matrix, /*uint-arr=*/self->map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum, /*size=*/self->nrows, /*stringOf_header=*/"map_1dOf_vertexTypes_hca__distanceToRoot_forMetric__linkScoreSum", /*label-wrapper=*/NULL);      
      //!
      //!
      //! Then export the matrix descignb the simlairty between each 'vertex' adn each 'ptuative vertex-cluster-centroid':
      s_kt_matrix_t matrix_tmp; setTo_empty__s_kt_matrix_t(&matrix_tmp);
      matrix_tmp.matrix = self->distancesIn_2d_clusterFrom__hcaToplogy__all_topology;
      matrix_tmp.nrows = self->nrows;       matrix_tmp.ncols = self->nrows;      export__dataObj_seperateFromFileObj__s_kt_matrix_t(matrix, &matrix_tmp, /*stringOf_header=*/"distancesIn_2d_clusterFrom__hcaToplogy__all_topology");       
    }
    if(self->for_pca) {

      fprintf(stderr, "!!\t FIXME[JC]: add support for PCA-result-generation wehn JC inlclues his impelmetnaiton and 'data-itnerptiraotn-strategy', at %s:%d\n", __FILE__, __LINE__); 

      //!
      //! Write out the matix (even through we at the point of 'coding' assuems the altter is not correclty set), ie, to 'ensure taht oru result is comaptible witht he export-import-setupt in our "g_tree_ds.js" (oekseth, 06. des. 201&).
      s_kt_matrix_t matrix_tmp; setTo_empty__s_kt_matrix_t(&matrix_tmp);
      matrix_tmp.matrix = self->distancesIn_2d_pca_appliedToCorrelationMatrix;
      assert(self->distancesIn_2d_pca_appliedToCorrelationMatrix);
      matrix_tmp.nrows = self->nrows;       matrix_tmp.ncols = self->nrows;      export__dataObj_seperateFromFileObj__s_kt_matrix_t(matrix, &matrix_tmp, /*stringOf_header=*/"distancesIn_2d_pca_appliedToCorrelationMatrix");             
    }

      
    //! --------------------
    //!
    //! Close:
    closeFileHandler__s_kt_matrix_t(matrix);
  } //! else we asusme all corr-matrices are empty.




  //! ------------------------------
  //!
  //! At this exuecitoni-point we assume the aobve logic-operaitons 'wehnt smooth':
  return true; 
}

//! Initaite the s_kt_clusterResult_config_t object to default values.
void init__s_kt_clusterResult_config_t(s_kt_clusterResult_config_t *self) {
  self->isTo_subDivideMatrix_usingDisjointForest = true;
  //! ---
  self->hca_method = 'm';
  self->isToCompute_hca = false;
  //! ---
  self->kmean_isToUse_kMedian = false;
  self->kmean_kmean_isToUse_medoidInComputation = false;
  self->kmean_npass = 1000;
  self->kmean_kmean_nclusters = false;
  self->kmean_result_clusterId = NULL;
  self->kmean_result_error = T_FLOAT_MIN_ABS;
  self->kmean_result_mapOf_iffound = NULL;  
  //! ---
  self->isToCompute_som = false;
  self->som_nygrid = 2;
  self->som_nxgrid = 2;
  self->som_initTau = T_FLOAT_MIN_ABS;
  self->som_niter = 1000;
  //! ---
  self->isToCompute_pca = false;
  //self->
}


/**
   @brief perform clsuter-anlasys and exports the result(s).
   @param <self> is the object to to use.
   @param <matrix> is the data-set to be clustered: may either be 'raw data' or a distance-matrix genrated from a call to our "kt_matrix__distancematrix(..)", where the latter function is defined in our "kt_api.h".
   @param <metric_insideClustering> is the metric to be used during the clsutering (ie, if the specified clsutering-algrotihms makes use of any distnace-metric inside the 'clsuter-alorithm itself').
   @param <resultFormat> is the reuslt-format to exprot the clsuter-results into.
   @param <resultPrefix> is teh file-rpefix to be sued: if "resultPrefix == NULL" then we write out the reseult to the "stdout" stream
   @return true upon success
   @remarks if configured collects cluster-rpedictison from a number of clsutering-algorithms, and thereafter collects the cluster-algorithm-results into a unfiied result-strucutre, a structure exported to the "resultPrefix" file-type using our "s_kt_clusterResult_data_t" data-structure
 **/
bool performClusterAnalysis__andGenerateResults__s_kt_clusterResult_config_t(s_kt_clusterResult_config_t *self, s_kt_matrix_t *matrix, s_kt_correlationMetric_t metric_insideClustering, const e_kt_terminal_clusterResult__format_t resultFormat, const char *resultPrefix) {
//bool performClusterAnalysis__andGenerateResults__s_kt_clusterResult_config_t(s_kt_clusterResult_config_t *self, s_kt_matrix_t *matrix, const e_kt_terminal_clusterResult__format_t resultFormat, const char *resultPrefix, s_kt_correlationMetric_t metric_insideClustering) {
  if(!self || !matrix || !matrix->nrows) {
    fprintf(stderr, "!!\t The inptu was Not as expected: please update your API call. Now aborts the exeuction. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    assert(false); //! which is active only in debug-compilation-softwares.
    return false;
  }
  assert(self);
  assert(matrix);
  assert(matrix->nrows);

  //! ----------------------------------
  //!
  //! Initatie our object:
  s_kt_clusterResult_data_t obj_export; 
  const bool transpose = false; //! ie, as we expect the 'transpsotion' to have been performed in the data-loading
  const uint nrows = (transpose == 0) ? matrix->nrows : matrix->ncols;
  const uint ncols = matrix->ncols;
  init__s_kt_clusterResult_data(&obj_export, nrows, 
				self->isToCompute_som,
				self->kmean_isToUse_kMedian,
				self->isToCompute_hca,
				self->isToCompute_pca);



  //! ----------------------------------
  //!
  //! Perform the cluster-analysis: 
#include "kt_terminal_clusterResult__sub__clustering.c"

  //! ------------------------------
  //!
  //! Then export the result:
  const bool is_ok = writeOut__s_kt_clusterResult_data_t(&obj_export, matrix, resultFormat, resultPrefix, NULL);
  assert(is_ok);
  free__s_kt_clusterResult_data(&obj_export); //! ie, de-allcoate the exprot-object, as we ssume the 'result-object is no longer needed'.

  //! ------------------------------
  //!
  //! At this exuecitoni-point we assume the aobve logic-operaitons 'wehnt smooth':
  return is_ok; 
}
