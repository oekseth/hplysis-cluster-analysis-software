#include "kt_set_2dsparse.h"
#include "correlation_sort.h"

 /*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


//! Intiates an empty object
void setTo_Empty__s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *self) {
  assert(self);
  self->_nrows = 0;
  self->matrixOf_scores = NULL;
  self->matrixOf_id = NULL;
  self->biggestInserted_row_id = UINT_MAX;
}

/**
   @brief allocates an s_kt_set_2dsparse_t object
   @param <self> is the object to allocate
   @param <nrows> is the number of rows to allocate
   @param <ncols> is the number of columsn in the matrix input, ie, ignroed if matrix==NULL
   @param <matrix> if set then we itnaite the object using 'this' input-parameter
   @param <mask> if set then we assume the makss are eplxict, ie, only inlcude for "mask[i][j] == 0"
   @param <isTo_useImplictMask> which if set implies that we investigate for T_FLOAT_MAX
   @param <isTo_allocateFor_scores> which if set to false implies that we do Not expect the scores to be of itnerest, ie, an parameter used to reduce the memory-consutmpion
 **/
void allocate__s_kt_set_2dsparse_t(s_kt_set_2dsparse_t *self, const uint nrows, const uint ncols, t_float **matrix, char **mask, const bool isTo_useImplictMask, const bool isTo_allocateFor_scores) {
  assert(self); setTo_Empty__s_kt_set_2dsparse_t(self);
  if(nrows) {
    const uint min_cnt_cols = 100;
    const uint min_cnt_cols_plussOne = min_cnt_cols + 2;
    assert(min_cnt_cols_plussOne > 0);
    self->_nrows = nrows;
    //! Allcoate for the vertex-ids:
    self->matrixOf_id = alloc_generic_type_2d(uint, self->matrixOf_id, nrows);
    //! The allcoate iteratly for each of the rows, ie, to simplify 'cahgnign the size of each row seperatley':
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const uint default_value_uint = 0;
      self->matrixOf_id[row_id] = allocate_1d_list_uint(min_cnt_cols_plussOne, default_value_uint);
      self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_currentPos] = 0;
      self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_size] = min_cnt_cols; //! ie, set the 'percieved size'.
    }
    //! ---------------------------------------------------------------        
    //! ---------------------------------------------------------------
    if(isTo_allocateFor_scores) { //! then allcoate for the scores:
      self->matrixOf_scores = alloc_generic_type_2d(t_float, self->matrixOf_scores, nrows);
      //! The allcoate iteratly for each of the rows, ie, to simplify 'cahgnign the size of each row seperatley':
      for(uint row_id = 0; row_id < nrows; row_id++) {
	const t_float default_value_float = 0;
	self->matrixOf_scores[row_id] = allocate_1d_list_float(min_cnt_cols, default_value_float);
      }
    }

    if(matrix) { //! Then copy the data:
      if(!isTo_useImplictMask && !mask) {
	//printf("initates for mask-none, at %s:%d\n", __FILE__, __LINE__);
	//! Allocate a temproary list of vertex-dis to be used:
	const uint default_value_uint = 0;
	uint *tmpList_ids = allocate_1d_list_uint(ncols, default_value_uint);
	for(uint col_id = 0; col_id < ncols; col_id++) {tmpList_ids[col_id] = col_id;} //! ie, as we thena ssuemt aht all valeus are of itnerest:
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  if(self->matrixOf_scores) {
	    add_rowSparse__s_kt_set_2dsparse_t(self, row_id, tmpList_ids, ncols, matrix[row_id]);
	  } else {
	    add_rowSparse__s_kt_set_2dsparse_t(self, row_id, tmpList_ids, ncols, NULL);
	  }
	}
	//! De-allcoate the lcoally reserved list ov values:
	free_1d_list_uint(&tmpList_ids);
      } else if(!mask && isTo_useImplictMask) {
	//printf("initates for mask-implicit, at %s:%d\n", __FILE__, __LINE__);
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  add_row_maskImplicit__s_kt_set_2dsparse_t(self, row_id, matrix[row_id], ncols);
	}
      } else if(mask) {
	//printf("initates for mask-explicit, at %s:%d\n", __FILE__, __LINE__);
	assert(isTo_useImplictMask == false);
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  add_row_maskExplicit(self, row_id, matrix[row_id], ncols, mask[row_id]);
	}	
      }
    }
  } else {
    assert(ncols == 0);
    assert(mask == 0);
    assert(matrix == NULL);
  }
}

/**
   @brief Sort the complete number opf tiems in the sparse matrix
   @param <self> is the object to sort
   @param <isTo_sortKeys> which fi set to false impleis that we only sort for the socre-weights, an option which is of interst/importance wrt. builidng of sparse non-rank correlation-matrices.
**/
void sort_eachRow_in_CompleteMatrix__s_kt_set_2dsparse_t(const s_kt_set_2dsparse_t *self, const bool isTo_sortKeys) {
  assert(self); assert(self->_nrows > 0);
  
#define t_type t_float
#define t_type_max T_FLOAT_MAX
#define __MiF__memScore_alloc(cnt) ({allocate_1d_list_float(cnt, empty_0);})
#define __MiF__memScore_free(ptr) ({free_1d_list_float(ptr);})
#define __MiF__get_rowSize(row_id) ({self->matrixOf_id[row_id][e_kt_set_sparse_colAttribute_currentPos] + 1;})
#define __MiF__get_row(row_id) ({ uint *row = &(self->matrixOf_id[row_id][2]); row;})
#define __MiF__get_apply_sort_forScores() ({quicksort(row_size, /*input=*/row, /*arr_index=*/arr_index, /*arr_result=*/row_tmp_float);})
#define __MiF__get_rowScores(row_id) ({t_float *row = self->matrixOf_scores[row_id]; row;})
#define __MiF__get_rowScores_pair(row_id, col_id) ({self->matrixOf_scores[row_id][col_id]; })
#define __MiF__copyIntoGlobal_rowScores(row_id) ({	memcpy(matrixOf_scores[row_id], arr_index_localScores, sizeof(t_float)*row_size); })
  const uint nrows = self->_nrows;
  t_float **matrixOf_scores = self->matrixOf_scores;
  //!
  //! Apply logics:
#include "kt_set_2dsparse__stub__sortMatrix.c"
}
