#ifndef correlation_rank_rowPair_h
#define correlation_rank_rowPair_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file s_correlation_rank_rowPair.h
   @brief a wrapper to construct ranks for two rows of data
**/

#include "correlation_rank.h"

/**
   @struct s_correlation_rank_rowPair
   @brief a wrapper to construct ranks for two rows of data
   @remarks to compute the "s_correlation_rank_rowPair" rnak-strucutre, call our "s_correlation_rank_rowPair_compute_nonTransposed(..)"
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/
typedef struct s_correlation_rank_rowPair {
  t_float *arrOf_valuesSorted_1; t_float *arrOf_valuesSorted_2;
  t_float *arrOf_ranks_index1;   t_float *arrOf_ranks_index2;
  uint cnt_setValues_inRanks; //! which is sued as a 'size-emasurement.
} s_correlation_rank_rowPair_t;

//! Set the row-pair object ot emptyDe-allcoat ethe row-pair object:
void s_correlation_rank_rowPair_setTo_empty(s_correlation_rank_rowPair_t *self);
//! Allocate the rank-data for teh row-pair.
//! @remarks Initiate row row-pari set of ranks:
void s_correlation_rank_rowPair_allocate(s_correlation_rank_rowPair_t *self, const uint ncols);
//! De-allcoat ethe row-pair object:
void s_correlation_rank_rowPair_free(s_correlation_rank_rowPair_t *self);
/**
   @brief comptue the ranks for a row-pair
   @remarks i shte main-funciton of this structure.
 **/
void s_correlation_rank_rowPair_compute_nonTransposed(s_correlation_rank_rowPair_t *self, const uint index1, const uint index2, const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, const bool needTo_useMask_evaluation);
/**
   @brief comptue the ranks for a row-pair for a tansposed matrix.
   @remarks i shte main-funciton of this structure.
 **/
void s_correlation_rank_rowPair_compute_transposed(s_correlation_rank_rowPair_t *self, const uint index1, const uint index2, const uint nrows, t_float **data1, t_float **data2, char **mask1, char **mask2, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, const bool needTo_useMask_evaluation);
#endif //! EOF
