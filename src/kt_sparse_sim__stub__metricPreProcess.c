for(uint row_id = 0; row_id < __MiV__2d_from->_nrows; row_id++) {
  uint arr_size = 0;
  const uint *arr = get_sparseRow_ofIds__s_kt_set_2dsparse_t(__MiV__2d_from, row_id, &arr_size);
  if(arr && arr_size) {
    if( (self->sim_metric.typeOf_correlationPreStep == e_kt_categoryOf_correaltionPreStep_binary) || (__MiV__2d_from->matrixOf_scores == NULL) ) { //! ie, as we expect a cosnsitnecy between the cases. {
      for(uint i = 0; i < arr_size; i++) {
	//assert(val[i] != T_FLOAT_MAX); //! as we expect all values inserted int he sparse list 'to have a point, ie, a value'.
	assert(arr[i] != UINT_MAX); //! as we expect all values inserted int he sparse list 'to have a point, ie, a value'.
	const t_float score = 0;	      
	push__idScorePair__s_kt_set_2dsparse_t(&__MiV__2d_to, row_id, /*col_id=*/arr[i], score);
      }
    } else { //! Then we 'get' the rank:
      uint val_size = 0;
      const t_float *val = get_sparseRow_ofScores__s_kt_set_2dsparse_t(__MiV__2d_from, row_id, &val_size);
      assert(val); assert(val_size);
      //! 
      //! Apply the sorting:
      if(isTo_sortKeys == true) {
	uint *arrOf_ranks = get_rank__uint__correlation_rank(arr_size, arr);
	//getrank_memoryRe_use(arr_size, 
	//quicksort_uint(arr_size, /*input=*/arr, /*arr_index=*/arr_index, /*arr_result=*/row_tmp_uint);	      
	assert(arrOf_ranks);
	//! 
	//! Insert the scores:
	for(uint i = 0; i < arr_size; i++) {		
	  assert(arrOf_ranks[i] != UINT_MAX);
	  const uint tail = arr[i];
	  const t_float score = (t_float)arrOf_ranks[i];
	  push__idScorePair__s_kt_set_2dsparse_t(&__MiV__2d_to, row_id, /*col_id=*/tail, score);
	}
	//! De-allcoates:
	free_1d_list_uint(&arrOf_ranks); arrOf_ranks = NULL;
      } else { //! then we an 'rodnary appraoch where scores are used'.
	t_float *arrOf_ranks = get_rank__float__correlation_rank(val_size, val);
	assert(arrOf_ranks);
	//! 
	//! Insert the scores:
	for(uint i = 0; i < arr_size; i++) {		
	  assert(arrOf_ranks[i] != UINT_MAX);
	  const uint tail = arr[i];
	  const t_float score = (t_float)arrOf_ranks[i];
	  push__idScorePair__s_kt_set_2dsparse_t(&__MiV__2d_to, row_id, /*col_id=*/tail, score);
	}
	//! De-allcoates:
	free_1d_list_float(&arrOf_ranks); arrOf_ranks = NULL;
      }
    }
  }	
 } //! and at this exec-point we have udpated all of our rows.


//! ------------------------------------
#undef __MiV__2d_from
#undef __MiV__2d_to
