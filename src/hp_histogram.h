#ifndef hp_histogram_h
#define hp_histogram_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file hp_histogram.h
   @brief provide logics for comparison of histograms (oekseth, 06. feb. 2018)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2018).
   @remarks among others used in algorithms and applications of image anlaysis. 
**/


//#include "type_2d_float_nonCmp_uint.h"
#include "kt_matrix_base.h"

/**
   @brief constructs a collection of histograms
   @param <> 
   @param <> 
   @param <> 
   @return return a matrix holding the histograms, ie, where each row represents a given histogram. 
   @remarks is concpetaully to the "kmeans++" procedure, ie, where extreme feature vectors are used to idenify boundaries of seperation
 **/
// FIXME: ... how are the rows to merged/unfied wrt. the histogram construciton?
// FIXME: ... what is the meaning/applciaotn of these results?
// FIXME: ... 
// FIXME: ... 


#endif //! EOF
