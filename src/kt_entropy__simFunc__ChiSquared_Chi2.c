  // FIXME[tut+perf]: use 'this fucniton' as a performacne-refernece ... ei, as we 'from this' have not idneitfed/applied any perofmrance-otpmziaotn-strategies .... 
  //! Compute: freqs1 = freqs1/sum(freqs1)  and Compute: freqs2 = freqs2/sum(freqs2) 
  //!Note: correspodns to "chi2.plugin(..)" or "chi2.Dirichlet(...)" or "chi2.empirical(...)" in the R entorpy-package.
  t_float sum_1 = __MF__s_kt_entropy__sum(arr1, arr_size);
  t_float sum_2 = __MF__s_kt_entropy__sum(arr2, arr_size);
  t_float sum_H = 0;

  assert(false); // FIXME: add supprot + usage for our "e_kt_entropy_frequencyType_raw"

  if( (sum_1 != 0) && (sum_2 != 0) )  {
    
    assert(false); // FIXME: ivneistgate correnctess wr.t [”elow] permtautiosn/interptreaitons .... updating our aritlce wr.t such a dicussion .... 

    if(typeOf_freqCompute == e_kt_entropy_frequencyType_empirical) {
      const t_float sum_1_inv = 1/sum_1; //! ie, a eprofmrance-otpmziaotn-strategy: avodi the need for 'dvision' and 'also' avoid the need/reuqiremen for an if-clause-branc.h
      const t_float sum_2_inv = 1/sum_1; //! ie, a eprofmrance-otpmziaotn-strategy: avodi the need for 'dvision' and 'also' avoid the need/reuqiremen for an if-clause-branch.
#define __MiF__getScore_1(index) ({(t_float)arr1[index]*(t_float)sum_1_inv;})
#define __MiF__getScore_2(index) ({(t_float)arr2[index]*(t_float)sum_2_inv;})
      //! Call:
      __MF__sim__chi2();
      //! Reset macros:
#undef __MiF__getScore_1
#undef __MiF__getScore_2
    } else if(typeOf_freqCompute == e_kt_entropy_frequencyType_Dirichlet) {
      sum_1 += (t_float)arr_size*(t_float)self->val_adjust; /*! ie, compute: "ya = y+a".*/	
      sum_2 += (t_float)arr_size*(t_float)self->val_adjust; /*! ie, compute: "ya = y+a".*/	
      const t_float sum_1_inv = 1.0/sum_1; //! ie, a eprofmrance-otpmziaotn-strategy: avodi the need for 'dvision' and 'also' avoid the need/reuqiremen for an if-clause-branc.h
      const t_float sum_2_inv = 1.0/sum_1; //! ie, a eprofmrance-otpmziaotn-strategy: avodi the need for 'dvision' and 'also' avoid the need/reuqiremen for an if-clause-branch.
#define __MiF__getScore_1(index) ({__MF__freqs_Dirichlet__atIndex(arr1, index, sum_1_inv);})
#define __MiF__getScore_2(index) ({__MF__freqs_Dirichlet__atIndex(arr2, index, sum_2_inv);})
      const t_float val_adjust = self->val_adjust;
      //! Call:
      __MF__sim__chi2();
      //! Reset macros:
#undef __MiF__getScore_1
#undef __MiF__getScore_2
    } else {
#define __MiF__getScore_1(index) ({(t_float)arr1[index];})
#define __MiF__getScore_2(index) ({(t_float)arr2[index];})
      //! Call:
      __MF__sim__chi2();
      //! Reset macros:
#undef __MiF__getScore_1
#undef __MiF__getScore_2
    }
  }
  //!
  //! Post-scaling:
  __MF__s_kt_entropy__postAdjustment();
  //!
  //! @return
  *scalar_retVal = sum_H;
