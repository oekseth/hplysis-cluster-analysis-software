{ //! Apply logics for updatingt the correlation-metics:
  if(local_isToMerge) {
    assert(self->inputMatrix_2.nrows > 0);     assert(self->inputMatrix_2.ncols > 0);
    s_kt_matrix_t obj_result; setTo_empty__s_kt_matrix_t(&obj_result);
    assert(false); // FIXME: consider to udpat ehte "config_corr" ... hwo do we Know if thye matrix is an 'ajdcnecy-matrix'?
    s_kt_api_config_t config__corr = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/false, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/false, NULL);
    const bool is_ok = kt_matrix__distancematrix(metric_local, &(self->inputMatrix_1),  &(self->inputMatrix_2),  &obj_result, config__corr); ///*isTo_transposeMatrix=*/false); //! ie, as we expect our file-parser to have (if neccesyar) trtransposed the input-matrix.
    assert(self->inputMatrix_result.matrix == NULL); //! ie, what we expect.
    //! --------------------------------------------
    //! Update the result-matrix:
    self->inputMatrix_result.matrix = obj_result.matrix;
    //! Note: we expect the result is an djcency-matrix:
    self->inputMatrix_result.nrows = obj_result.nrows; 	self->inputMatrix_result.ncols = obj_result.nrows; 
    //! Update the name-mappigns, ie, based on the assumption tha thte result is an acjency-matrix:
    self->inputMatrix_result.nameOf_rows = self->inputMatrix_1.nameOf_rows;
    self->inputMatrix_result.nameOf_columns = self->inputMatrix_2.nameOf_rows;
    self->inputMatrix_result.isMemoryOwnerOf_attribute_nameOf = false; //! ie, to avoid 'the strucutre' from de-allocating the "nameOf_columns" and "nameOf_rows" attributes
    //self->inputMatrix_2.nameOf_rows = false;
    //! --------------------------------------------
    //! 
    //! Then update the variable:
    needToMerge_results_areMerged = true;
    
    // printf("at %s:%d\n", __FILE__, __LINE__);

  } else { //! Then we compute seperatey for each of the matrices:
    if( (self->inputMatrix_2.nrows == 0) && (self->inputMatrix_2.ncols == 0) ) {
      s_kt_matrix_t obj_result; setTo_empty__s_kt_matrix_t(&obj_result);
      // printf("\n\n(merge-first-matrix)\t at %s:%d\n", __FILE__, __LINE__);
      assert(false); // FIXME: consider to udpat ehte "config_corr" ... hwo do we Know if thye matrix is an 'ajdcnecy-matrix'?
      s_kt_api_config_t config__corr = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/false, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/false, NULL);
      const bool is_ok = kt_matrix__distancematrix(metric_local, &(self->inputMatrix_1),  &(self->inputMatrix_1),  &obj_result, config__corr); ///*isTo_transposeMatrix=*/false); //! ie, as we expect our file-parser to have (if neccesyar) trtransposed the input-matrix.
      assert(self->inputMatrix_result.matrix == NULL); //! ie, what we expect.
      
      //! --------------------------------------------
      //! Update the result-matrix:
      self->inputMatrix_result.matrix = obj_result.matrix; 
      //! Note: we expect the result is an djcency-matrix:
      self->inputMatrix_result.nrows = obj_result.nrows; 	self->inputMatrix_result.ncols = obj_result.nrows; 
      //! Update the name-mappigns, ie, based on the assumption tha thte result is an acjency-matrix:
      self->inputMatrix_result.nameOf_rows = self->inputMatrix_1.nameOf_rows; 
      self->inputMatrix_result.nameOf_columns = self->inputMatrix_1.nameOf_rows; self->inputMatrix_1.nameOf_rows = NULL;
      if(self->inputMatrix_1.weight) {free_1d_list_float(&(self->inputMatrix_1.weight)); self->inputMatrix_1.weight = NULL;}
      //self->inputMatrix_result.weight = self->inputMatrix_1.weight; self->inputMatrix_1.weight = NULL;
      //! --------------------------------------------
      //! 
      //! Then update the variable:
      needToMerge_results_areMerged = true;
      //printf("at %s:%d\n", __FILE__, __LINE__);
    } else {
      needToMerge_results_areMerged = false; //! ie, as we update [”elow] seperately
      {
#if(configureDebug_useExntensiveTestsIn__tiling == 1)
#ifndef NDEBUG
	{ //! Tehn we vliadate that no values are set to 'inf(..)'
	  for(uint row_id = 0; row_id < self->inputMatrix_1.nrows; row_id++) {
	    for(uint col_id = 0; col_id < self->inputMatrix_1.ncols; col_id++) {
	      const t_float score = self->inputMatrix_1.matrix[row_id][col_id];
	      assert(isinf(score) == false); //! ie, what we expect.
	    }
	  }
	}
#endif
#endif	
	s_kt_matrix_t obj_result; setTo_empty__s_kt_matrix_t(&obj_result);
	// printf("\n\n(correlate-first-matrix)\t given corr-emtric=%u, at %s:%d\n", metric_local.metric_id, __FILE__, __LINE__);
	assert(false); // FIXME: consider to udpat ehte "config_corr" ... hwo do we Know if thye matrix is an 'ajdcnecy-matrix'?
	s_kt_api_config_t config__corr = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/false, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/false, NULL);
	const bool is_ok = kt_matrix__distancematrix(metric_local, &(self->inputMatrix_1),  &(self->inputMatrix_1),  &obj_result, config__corr); ///*isTo_transposeMatrix=*/false); //! ie, as we expect our file-parser to have (if neccesyar) trtransposed the input-matrix.
#if(configureDebug_useExntensiveTestsIn__tiling == 1)
#ifndef NDEBUG
	{ //! Tehn we vliadate that no values are set to 'inf(..)'
	  for(uint row_id = 0; row_id < self->inputMatrix_1.nrows; row_id++) {
	    for(uint col_id = 0; col_id < self->inputMatrix_1.ncols; col_id++) {
	      const t_float score = self->inputMatrix_1.matrix[row_id][col_id];
	      assert(isinf(score) == false); //! ie, what we expect.
	    }
	  }
	}
#endif
#endif	

	//assert(false); // FIXME: remove

	assert(self->inputMatrix_result.matrix == NULL); //! ie, what we expect.
	free_2d_list_float(&(self->inputMatrix_1.matrix), self->inputMatrix_1.nrows); self->inputMatrix_1.matrix = NULL; //! ie, as we get a 'local copy' of the latter from "obj_result"
	if(self->inputMatrix_1.weight) {free_1d_list_float(&(self->inputMatrix_1.weight)); self->inputMatrix_1.weight = NULL;}
	self->inputMatrix_1.matrix = obj_result.matrix;
	//! Note: we expect the result is an djcency-matrix:
	self->inputMatrix_1.nrows = obj_result.nrows; 	self->inputMatrix_1.ncols = obj_result.nrows; 
	free_arrOf_names_columns__s_kt_matrix(&(self->inputMatrix_1)); //! ie, first de-allcoate the columns: this 'order' given the often-seen-use-case where we 'replace' teh "nameOf_columns" attribute with a 'driect' poitner-reference to "nameOf_rows" after the correlaiton-matrix is comptued (oekseth, 06. des. 2016).
	assert(self->inputMatrix_1.nameOf_columns == NULL);
	//! Update the name-mappigns, ie, based on the assumption tha thte result is an acjency-matrix:
	//self->inputMatrix_1.nameOf_rows = self->inputMatrix_1.nameOf_rows;
	self->inputMatrix_1.nameOf_columns = self->inputMatrix_1.nameOf_rows;

	//printf("at %s:%d\n", __FILE__, __LINE__);
	assert(needToMerge_results_areMerged == false); //! ie, what we expect.
      }    
      {
	// printf("\n\n(merge-second-matrix)\t at %s:%d\n", __FILE__, __LINE__);
      
	s_kt_matrix_t obj_result; setTo_empty__s_kt_matrix_t(&obj_result);
	assert(false); // FIXME: consider to udpat ehte "config_corr" ... hwo do we Know if thye matrix is an 'ajdcnecy-matrix'?
	s_kt_api_config_t config__corr = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/false, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/false, NULL);
	const bool is_ok = kt_matrix__distancematrix(metric_local, &(self->inputMatrix_2),  &(self->inputMatrix_2),  &obj_result, config__corr); ///*isTo_transposeMatrix=*/false); //! ie, as we expect our file-parser to have (if neccesyar) trtransposed the input-matrix.
	assert(self->inputMatrix_result.matrix == NULL); //! ie, what we expect.
	if(self->inputMatrix_2.weight) {free_1d_list_float(&(self->inputMatrix_2.weight)); self->inputMatrix_2.weight = NULL;}
	free_2d_list_float(&(self->inputMatrix_2.matrix), self->inputMatrix_2.nrows); self->inputMatrix_2.matrix = NULL; //! ie, as we get a 'local copy' of the latter from "obj_result"
	self->inputMatrix_2.matrix = obj_result.matrix;
	//! Note: we expect the result is an djcency-matrix:
	self->inputMatrix_2.nrows = obj_result.nrows; 	self->inputMatrix_2.ncols = obj_result.nrows; 
	//! Update the name-mappigns, ie, based on the assumption tha thte result is an acjency-matrix:
	//self->inputMatrix_2.nameOf_rows = self->inputMatrix_2.nameOf_rows;
	free_arrOf_names_columns__s_kt_matrix(&(self->inputMatrix_2)); //! ie, first de-allcoate the columns: this 'order' given the often-seen-use-case where we 'replace' teh "nameOf_columns" attribute with a 'driect' poitner-reference to "nameOf_rows" after the correlaiton-matrix is comptued (oekseth, 06. des. 2016).
	assert(self->inputMatrix_2.nameOf_columns == NULL);
	self->inputMatrix_2.nameOf_columns = self->inputMatrix_2.nameOf_rows;
	// printf("at %s:%d\n", __FILE__, __LINE__);
	assert(needToMerge_results_areMerged == false); //! ie, what we expect.
      }
    }
  }

  //  printf("\n\n# writeOut_corrMetrics=%u, at %s:%d\n", writeOut_corrMetrics, __FILE__, __LINE__);

  if(writeOut_corrMetrics) { //! then write out the matrix:
    //if(self->isTo_inResult_isTo_exportInputData == true) 

    //! Then we export the data-sets to a singular file, ie, for each of the matrices 'which has data' (oekseth, 06. des. 2016).
    const bool is_ok = exportDataSets__toACombinedFile__kt_terminal(self, label_1, isTo_useMatrixFormat);
    //assert(is_ok);

    /* { //! then w 'at the tal' exproit the input-matrix: */
    /*   if(needToMerge_results_areMerged == true) { */
    /* 	export__setLabel__s_kt_matrix_t(&(self->inputMatrix_result), label_1); */
    /*   } else { */
    /* 	export__dataObj_seperateFromFileObj__s_kt_matrix_t(&(self->inputMatrix_result), &(self->inputMatrix_1), label_1); */
    /* 	export__dataObj_seperateFromFileObj__s_kt_matrix_t(&(self->inputMatrix_result), &(self->inputMatrix_2), label_2); */
    /*   } */
    /* } */
  }
}
