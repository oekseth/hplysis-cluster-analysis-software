#ifndef measure_codeStyle_functionRef_h
#define measure_codeStyle_functionRef_h


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file measure_codeStyle_functionRef
   @brief provide lgoic-wrappers to test the time-cost-penalty of using C-style function-references as templates to genealize the code (ie, to make the code more maintalable). 
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016)
 **/
#include "types.h"
#include "def_intri.h"

typedef struct s_measure_codeStyle_functionRef {
  t_float sumOf;
} s_measure_codeStyle_functionRef_t;


typedef struct s_measure_codeStyle_functionRef_largeOverhead {
  t_float sumOf;
  t_float overhead[1000]; //! which is used to itrnoduce an overhead wrt. calls
} s_measure_codeStyle_functionRef_largeOverhead_t;

static void inHeader_applyAritmetics_muliplication(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_t *result) {
  result->sumOf = val1 * val2;
}
inline __attribute__((always_inline)) void inHeader_applyAritmetics_muliplication_alwaysInline(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_t *result) { //__attribute__((always_inline)) {
  result->sumOf = val1 * val2;
}
static inline __attribute__((always_inline)) void inHeader_applyAritmetics_muliplication_alwaysInline_static(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_t *result) { //__attribute__((always_inline)) {
  result->sumOf = val1 * val2;
}


//! -------------------------------------

static void inHeader_applyAritmetics_muliplication_structLargeOverhead(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_largeOverhead_t *result) {
  result->sumOf = val1 * val2;
}
inline __attribute__((always_inline)) void inHeader_applyAritmetics_muliplication_structLargeOverhead_alwaysInline(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_largeOverhead_t *result) {
  result->sumOf = val1 * val2;
}
static inline __attribute__((always_inline)) void inHeader_applyAritmetics_muliplication_structLargeOverhead_alwaysInline_static(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_largeOverhead_t *result) {
  result->sumOf = val1 * val2;
}


//! -------------------------------------

void applyAritmetics_muliplication(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_t *result);
inline void applyAritmetics_muliplication_alwaysInline(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_t *result) __attribute__((always_inline));;

//! -------------------------------------
void applyAritmetics_muliplication_structLargeOverhead(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_largeOverhead_t *result);


//inline  void  applyAritmetics_muliplication_structLargeOverhead_alwaysInline(const t_float val1, const t_float val2, s_measure_codeStyle_functionRef_largeOverhead_t *result)  __attribute__((always_inline));


static void (*getFunction__inHeader__measure_codeStyle_functionRef(char dist)) 
(const t_float, const t_float, s_measure_codeStyle_functionRef_t*)
{ switch(dist)
    { 
    case 'a': return &inHeader_applyAritmetics_muliplication;
    case 'b': return &inHeader_applyAritmetics_muliplication_alwaysInline;
    case 'c': return &inHeader_applyAritmetics_muliplication_alwaysInline_static;

      //default: return &euclid;
  }
  assert(false); //! ie, then handle this case.
  return NULL; /* Never get here */
}

static void (*getFunction__inHeader_largeOverhead__measure_codeStyle_functionRef(char dist)) 
(const t_float, const t_float, s_measure_codeStyle_functionRef_largeOverhead_t*)
{ switch(dist)
    { 
    case 'a': return &inHeader_applyAritmetics_muliplication_structLargeOverhead;
    case 'b': return &inHeader_applyAritmetics_muliplication_structLargeOverhead_alwaysInline;
    case 'c': return &inHeader_applyAritmetics_muliplication_structLargeOverhead_alwaysInline;

      //default: return &euclid;
  }
  assert(false); //! ie, then handle this case.
  return NULL; /* Never get here */
}


static void (*getFunction__measure_codeStyle_functionRef(char dist)) 
(const t_float, const t_float, s_measure_codeStyle_functionRef_t*)
{ switch(dist)
    { 
    case 'a': return &applyAritmetics_muliplication;
      //case 'b': return &applyAritmetics_muliplication_alwaysInline;

      //default: return &euclid;
  }
  assert(false); //! ie, then handle this case.
  return NULL; /* Never get here */
}

static void (*getFunction__largeOverhead__measure_codeStyle_functionRef(char dist)) 
(const t_float, const t_float, s_measure_codeStyle_functionRef_largeOverhead_t*)
{ switch(dist)
    { 
    case 'a': return &applyAritmetics_muliplication_structLargeOverhead;
      // case 'b': return &applyAritmetics_muliplication_structLargeOverhead_alwaysInline;

      //default: return &euclid;
  }
  assert(false); //! ie, then handle this case.
  return NULL; /* Never get here */
}


//! @remarks this function is taken from the "ALGLIB" package and is sued to test (a) the time-cost of the 'static' operaonds for muliple/numerous operations and (b) the 'beneift' of using the "volatile" key-word
bool ae_fp_greater_eq_nonVolatile(double v1, double v2);
//! @remarks this function is taken from the "ALGLIB" package and is sued to test (a) the time-cost of the 'static' operaonds for muliple/numerous operations and (b) the 'beneift' of using the "volatile" key-word
bool ae_fp_greater_eq(double v1, double v2);

#endif //! EOF
