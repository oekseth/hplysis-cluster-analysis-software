#ifndef db_rel_h
#define db_rel_h

#include "def_intri.h"

/**
   @struct s_db_tailRel 
   @brief a generlaie approach to access the 'tail-relation-parts' of semantic relationships (oekseth, 06. mar. 2017)
 **/
typedef struct s_db_tailRel {
  uint rt;  uint tail; uint relation_id; 
  t_float distance;
} s_db_tailRel_t;
//! @return a new-intliased object.
static s_db_tailRel_t init__s_db_tailRel_t() {
  s_db_tailRel_t self;
  self.rt = UINT_MAX;
  self.tail = UINT_MAX;
  self.relation_id = UINT_MAX;
  self.distance = 1;
  //! @return
  return self;
}

#define __M__wildCard__s_db_rel_t UINT_MAX-1

#define __M__init__s_db_tailRel_t() ({s_db_tailRel_t self; self.rt = UINT_MAX; self.tail = UINT_MAX; self.relation_id = UINT_MAX; self.distance = 1; self;}) //! @return
#define __M__initWildCard__s_db_tailRel_t() ({s_db_tailRel_t self; self.rt = __M__wildCard__s_db_rel_t; self.tail = __M__wildCard__s_db_rel_t; self.relation_id = __M__wildCard__s_db_rel_t; self.distance = 1; self;}) //! @return

/**
   @struct s_db_rel
   @brief a generlaie approach to access the 'relation-parts' of semantic relationships (oekseth, 06. mar. 2017)
 **/
typedef struct s_db_rel {
  uint head;
  s_db_tailRel_t tailRel;
} s_db_rel_t;
//! @return a new-intliased object.
static s_db_rel_t init__s_db_rel_t() {
  s_db_rel_t self;
  self.head = UINT_MAX;
  self.tailRel = init__s_db_tailRel_t();
  //! @return
  return self;
}

#define __M__init__s_db_rel_t() ({s_db_rel_t self; self.head = UINT_MAX; self.tailRel = __M__init__s_db_tailRel_t(); self;}) //! @return
#define __M__initWildCard__s_db_rel_t() ({s_db_rel_t self; self.head = __M__wildCard__s_db_rel_t; self.tailRel = __M__initWildCard__s_db_tailRel_t(); self;}) //! @return

#define __M__specialValue__predicate_synonym UINT_MAX-2
#define __M__isWildCart__s_db_rel_t(scalar) ({const int ret_val = (scalar == (__M__wildCard__s_db_rel_t)); ret_val;})
#define __M__isEqual__s_db_rel_t(relation, rel) ({bool ret_val = false; if( (__M__isWildCart__s_db_rel_t(relation.head) || (relation.head == rel.head) ) && (__M__isWildCart__s_db_rel_t(relation.tailRel.rt) || (relation.tailRel.rt == rel.tailRel.rt) ) && (__M__isWildCart__s_db_rel_t(relation.tailRel.tail) || (relation.tailRel.tail == rel.tailRel.tail) ) && (__M__isWildCart__s_db_rel_t(relation.tailRel.relation_id) || (relation.tailRel.relation_id == rel.tailRel.relation_id) ) ) {ret_val = true;} /*printf("head=(%u, %u), ret_Val=%d, at %s:%d\n", relation.head, rel.head, ret_val, __FILE__, __LINE__);*/ ret_val;}) 

//! @return true if the relation of type "s_db_rel_t" has an arbitrary value set.
#define __M__hasArbitraryClauses__s_db_rel_t(relation) ({bool ret_val = false; if( __M__isWildCart__s_db_rel_t(relation.head)  || __M__isWildCart__s_db_rel_t(relation.tailRel.rt) ||  __M__isWildCart__s_db_rel_t(relation.tailRel.tail) || __M__isWildCart__s_db_rel_t(relation.tailRel.relation_id) ) {ret_val = true;} /*printf("head=(%u, %u), ret_Val=%d, at %s:%d\n", relation.head, rel.head, ret_val, __FILE__, __LINE__);*/ ret_val;}) 

#define __M__allValuesInObj__hasArbitraryClauses__s_db_rel_t(relation) ({bool ret_val = false; if( __M__isWildCart__s_db_rel_t(relation.head)  && __M__isWildCart__s_db_rel_t(relation.tailRel.rt) &&  __M__isWildCart__s_db_rel_t(relation.tailRel.tail) && __M__isWildCart__s_db_rel_t(relation.tailRel.relation_id) ) {ret_val = true;} /*printf("head=(%u, %u), ret_Val=%d, at %s:%d\n", relation.head, rel.head, ret_val, __FILE__, __LINE__);*/ ret_val;}) 

#define __M__getScalar__belowOrAbove(val1, val2) ({int ret_val = 0; if(val1 < val2) {ret_val = -1;} else if(val1 > val2) {ret_val = 1;} ret_val;}) //! ie, return.
//! @return an integer descirinb/saying if the "relation" is 'less', equal', or 'above' "rel".
#define __M__belowOrAbove__s_db_rel_t(relation, rel) ({int ret_val = 0; if( (__M__isWildCart__s_db_rel_t(relation.head) || (relation.head == rel.head) )) { if(__M__isWildCart__s_db_rel_t(relation.tailRel.rt) || (relation.tailRel.rt == rel.tailRel.rt) ) { if(__M__isWildCart__s_db_rel_t(relation.tailRel.tail) || (relation.tailRel.tail == rel.tailRel.tail) ) {if(__M__isWildCart__s_db_rel_t(relation.tailRel.relation_id) || (relation.tailRel.relation_id == rel.tailRel.relation_id) ){ret_val = 0;} else  /*relation_id-eval */ { ret_val = __M__getScalar__belowOrAbove(relation.tailRel.relation_id, rel.tailRel.relation_id);}} else  /*tail-eval */ { ret_val = __M__getScalar__belowOrAbove(relation.tailRel.tail, rel.tailRel.tail);}} else /*RT-eval */ { ret_val = __M__getScalar__belowOrAbove(relation.tailRel.rt, rel.tailRel.rt);}} else /*head-eval: */ { ret_val = __M__getScalar__belowOrAbove(relation.head, rel.head);}  ret_val;})
//! Consturct  an ivnerse verison of the relation.
//! Note:  the "rt" and "relaiton_id" are expectedb 'beign the same'
#define __M__getInverse__s_db_rel_t(relation) ({s_db_rel_t relation_inv = relation; relation_inv.head = relation.tailRel.tail; relation_inv.tailRel.tail = relation.head; relation_inv;}) //! ie, return "relation_inv".
//! A list-search to find the 'closest' matchign relationship.
//! @remarks used in our b-tree to fidn the 'split-popint' wrt. inserting a 'new' node.
#define __M__searchPos__s_db_rel(key, key_arr, n) ({uint pos=0; while ( (pos < (uint)n) && (__M__belowOrAbove__s_db_rel_t(/*obj1=*/key, /*obj2=*/key_arr[pos]) == 1) ) { pos++; }; pos;})
//! @return an integer to simplify the use of printf(..) when witing out status-udpaes wrt. oru appraoch.
#define __M__getKeyFroObj__s_db_rel(obj) ({obj.head;})
//! Allocate a new set o s_db_rel_t objects.
#define __MF__allocate__s_db_rel(size) ({assert(size > 0); s_db_rel_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_db_rel_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set o s_db_rel_t objects.
#define __MF__free__s_db_rel(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

#define __MF__getHead__s_db_rel(rel) ({rel.head;})
#define __MF__getPredicate__s_db_rel(rel) ({rel.tailRel.rt;})
#define __MF__getTail__s_db_rel(rel) ({rel.tailRel.tail;})

#define __MF__print__s_db_rel(fileP, rel) ({assert(fileP); fprintf(fileP, "(%u, %u, %u, %f | id=%u), at %s:%d\n", rel.head, rel.tailRel.rt, rel.tailRel.tail, rel.tailRel.distance, rel.tailRel.relation_id, __FILE__, __LINE__);})
#define __MF__print__stdOut__s_db_rel(rel) ({__MF__print__s_db_rel(stdout, rel);})

//! @retunr a 'default' synonym-mapping-relation.
static s_db_rel_t init_synonym(const uint vertex_norm, const uint vertex_syn) {
  s_db_rel_t relation = __M__init__s_db_rel_t(); 
  relation.head = vertex_norm; 
  relation.tailRel.rt = __M__specialValue__predicate_synonym;
  relation.tailRel.tail = vertex_syn;
  //! @return:
  return relation;
}


//! Wraps a flaot-array into a set of values.
static s_db_rel_t __getRelation__fromRow__s_db_rel(const t_float *row, const uint row_size) {
  s_db_rel_t rel = __M__init__s_db_rel_t();
  rel.head = (uint)row[0];
  if(row_size == 2) {
    rel.tailRel.tail = (uint)row[1];
  } else if(row_size == 3) {
    rel.tailRel.rt   = (uint)row[1];
    rel.tailRel.tail = (uint)row[2];
  } else if(row_size == 4) {
    rel.tailRel.rt   = (uint)row[1];
    rel.tailRel.tail = (uint)row[2];
    rel.tailRel.relation_id = (uint)row[3];
  } else if(row_size >= 5) {
    rel.tailRel.rt   = (uint)row[1];
    rel.tailRel.tail = (uint)row[2];
    rel.tailRel.relation_id = (uint)row[3];
    rel.tailRel.distance = (t_float)row[4];
  } 
  return rel;
}


#endif //! EOF
