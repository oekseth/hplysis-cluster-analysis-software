#include "alg_minCut_alt2.h"


//! Src: "http://www.geeksforgeeks.org/articulation-points-or-cut-vertices-in-a-graph/" (oekseh,t 06. mar. 2017)

// A C++ program to find articulation points in an undirected graph


/**
   @struct s_kt_clusterAlg_cutVertices
   @brief idnetifes high-connecitivty-nodes: lists/describes nodes  which remcal results in disjoint forests. 
   @remarks an appraoch where we 'find all nodes which removal  results in a graph-partiion'. To evalaute the 'impact' of each 'node to be remvoed' we may ...??... <--- 
   @todo try improvign the desciptriotn of use-cases and applciatons wrt. this aprpraoch ... a use-case wrt latter is to ....??... <-- consider to scaffold a new tut-example where we use latter to 'topolically describe' a network (eg, by using differnet rank-trehshold wrt. clusteirng) .... where results (of the latter) may be used to .....??... ..... and/or a use-case where we 'seek to find/build a network where approx. X vertices are 'enough' to 'split' the network .... 
 **/
typedef struct s_kt_clusterAlg_cutVertices {
  s_kt_set_2dsparse_t data; //! ie, the set of relationships to evalate.
  uint cnt_vertices;
  bool config__onltyToIdetnify_bridges;
  //! -----------------------------------------
  //!
  //! Local variables:
  uint current_time;
  bool *visited;
  uint *map_timeOf_discovery;  
  uint *low;
  //uint *parent;
  bool *ap;
} s_kt_clusterAlg_cutVertices_t;
 
s_kt_clusterAlg_cutVertices_t setToEmpty_s_kt_clusterAlg_cutVertices_t(const bool config__onltyToIdetnify_bridges) {
  s_kt_clusterAlg_cutVertices_t self;
  self.data = setToEmptyAndReturn__s_kt_set_2dsparse_t();
  self.cnt_vertices = 0;
  self.current_time = 0;
  self.visited = NULL;
  self.map_timeOf_discovery = NULL;
  self.low = NULL;
  //self.parent = NULL;
  self.ap = NULL;
  self.config__onltyToIdetnify_bridges = config__onltyToIdetnify_bridges;
  //! @return
  return self;
}

s_kt_clusterAlg_cutVertices_t init_s_kt_clusterAlg_cutVertices_t(const s_kt_set_2dsparse_t data, const uint cnt_vertices) {
  s_kt_clusterAlg_cutVertices_t self;
  self.data = data;
  self.cnt_vertices = cnt_vertices;
  // Mark all the vertices as not visited
  bool *mapOf_isVisisted = new bool[V];
  int *map_timeOf_discovery = new int[V];
  int *low = new int[V];
  //  uint *parent = new uint[V];
  bool *ap = new bool[V]; // To store articulation points

  const char empty_0  = 0;

  self.current_time = 0;
  self.visited = allocate_1d_list_char(cnt_vertices, empty_0);
  self.map_timeOf_discovery = allocate_1d_list_uint(cnt_vertices, empty_0);
  self.low = allocate_1d_list_uint(cnt_vertices, empty_0);
  //self.parent = NULL;
  self.ap = allocate_1d_list_uint(cnt_vertices, empty_0);
  //! @return
  return self;
}



/* // A class that represents an undirected graph */
/* struct Graph */
/* { */
/*   int V;    // No. of vertices */
/*   // FIXME: replace [”elow] with our our "kt_set_2dsparse.h" ... using our kruskal-impelmetnation as a template */
/*   list<int> *adj;    // A dynamic array of adjacency lists */
/* }; */
 
/* Graph::Graph(int V) */
/* { */
/*   this->V = V; */
/*   adj = new list<int>[V]; */
/* } */
 
/* void Graph::addEdge(int v, int w) */
/* { */
/*   // FIXME: descirbe how a 'score-knowledg'e may be used/utlized.  */
/*   adj[v].push_back(w); */
/*   adj[w].push_back(v);  // Note: the graph is undirected */
/* } */
 
// A recursive function that find articulation points using DFS traversal
// u --> The vertex to be visited next
// visited[] --> keeps tract of visited vertices
// map_timeOf_discovery[] --> Stores map_timeOf_discoveryovery times of visited vertices
// parent[] --> Stores parent vertices in DFS tree
// ap[] --> Store articulation points
static void APUtil(s_kt_clusterAlg_cutVertices_t *self, const uint u, const uint parent_id)
{
  assert(self);
  assert(u < self->cnt_vertices);
  // FIXME: ensure [”elow] is set.
 
  // Count of children in DFS Tree
  uint children = 0;
 
  // Mark the current node as visited
  self->visited[u] = true;
 
  // Initialize discovery time and low value
  self->map_timeOf_discovery[u] = self->low[u] = self->current_time++;
 

  assert(false); // FIXME[cocneptaulze]: before 'impelemtaitnoi' ... compare 'this proceudr'e to both kruskal and disjoint-forest ... writing a comparign wrt. latter. 
  assert(false); // FIXME[cocneptaulze]: before 'impelemtaitnoi' ... change/improve comments wrt. 'this file' .... iot. .... 
  assert(false); // FIXME[code::"alg_minCut_alt2.c"]: change froma  class-strucutre' to struct-appraoch ... and use a "kt_set_2dsparse.h" as a 'base-data-structure'
  assert(false); // FIXME[code::"alg_minCut_alt2.c"]: change name from "alg_minCut_alt2" to ... update repo .... get compling
  assert(false); // FIXME[code::"alg_minCut_alt2.c"]: merge/inklcude fucntion from our *kruskal*" impelmetnaiton ... describe simliarty .... get compiling.
  assert(false); // FIXME[code::"alg_minCut_alt2.c"]: .... 
  assert(false); // FIXME[code::"alg_minCut_alt2.c"]: write a tut-example where we evlauate/compare
  assert(false); // FIXME[code::"alg_minCut_alt2.c"]: .... 
  assert(false); // FIXME[code]:   

  // Go through all vertices aadjacent to this
  uint arrOf_child_size = 0;
  const uint *arrOf_child = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(self->data), /*vertex=*/u, &arrOf_child_size);
  if(arrOf_child_size > 0) {assert(arrOf_child);}
  //! Iterate:
  for(uint i = 0; i < arrOf_child_size; i++) { //! ie, iterate through list of 'adjcent vertices' to "u"
    const uint v = arrOf_child[i];  // v is current adjacent of u
    assert(v < self->cnt_vertices);
    
    // If v is not visited yet, then make it a child of u in DFS tree and recur for it
    if(!self->visited[v]) { //! then a 'recursive step' has Not been made for child="v":
      children++;
      //self->parent[v] = u;
      //! A recursive call: 
      APUtil(self, v, /*parent=*/u);

      //! Merge the tree-sets:
      // TODO: as low[u] is updated 'during the trversal' (while "v" is Not updated), "low[u] != low[v]" 'may be the result' (ie, in contrast to a dsijtoint-forest-impelmetnation). From the latter we infer that "low[..]" describes the min-access-time-point 
      // Check if the subtree rooted with v has a connection to
      // one of the ancestors of u
      self->low[u]  = macro_min(self->low[u], self->low[v]);
      
      // u is an articulation point in following cases
 
      if(self->config__onltyToIdetnify_bridges == false) {
	// (1) u is root of DFS tree and has two or more chilren.
	if( (parent_id == UINT_MAX) //(self->parent[u] == UINT_MAX) 
	    && (children > 1) ) {
	  self->ap[u] = true;
	}
	
	// (2) If u is not root and low value of one of its child is more
	// than discovery value of u.
	if(
	   (parent_id != UINT_MAX) //! then 'this is the result of a root-call'
	   //(self->parent[u] != UINT_MAX) //! then 'this is the result of a root-call'
	   && 
	   (self->low[v] >= self->map_timeOf_discovery[u]) ) {
	  self->ap[u] = true;
	}
      } else { //! then we focus on 'fidnig bridges':
	// If the lowest vertex reachable from subtree 
	// under v is  below u in DFS tree, then u-v 
	// is a bridge
	if(self->low[v] > self>disc[u]) {
	  // FIXME: instead of [”elow] insert latter into a new result-list ... inserting latter into our hca-result-strucutre <--- would 'this result' correspond to an MST?
	  cout << u <<" " << v << endl;
	}
      }
    } else if ( //! then we 'hanlde' the case where 'v' has areldy been visisted by differnet vertex:
	       v != parent_id
	       //v != parent[u]
	       ) { //! then 'the vertex' has already been visisted:
      //! Unify 'this' with other 'conectivity-trees': update using the best-min-time-point which may be 'assicated' to this vertex
      self->low[u]  = min(self->low[u], self->map_timeOf_discovery[v]);
    }
  }
}
 
/**
   @remarks steps taken are:
   step(0) initiate: build edge-graph, and then initate: mapOf_isVisisted, ... 
   step(1) for each vertex which has not earlier been investigated: : 
   step(0) : 
   step(0) : 
 **/

// The function to do DFS traversal. It uses recursive function APUtil()
void apply_s_kt_clusterAlg_cutVertices_t(init_s_kt_clusterAlg_cutVertices_t *self) { 
  // Initialize parent and visited, and ap(articulation point) arrays
  for(uint i = 0; i < self->cnt_vertices; i++) {
    self->parent[i] = UINT_MAX;
    self->mapOf_isVisisted[i] = false;
    self->ap[i] = false;
  }
 
  // Call the recursive helper function to find articulation points
  // in DFS tree rooted with vertex 'i'
  for (uint i = 0; i < self->cnt_vertices; i++) {
    if (self->mapOf_isVisisted[i] == false) {
      APUtil(self, i, /*parent_id=*/UINT_MAX); //mapOf_isVisisted, map_timeOf_discovery, low, parent, ap);
    }
  }
 
  // Now ap[] contains articulation points, print them
  for (int i = 0; i < self->cnt_vertices; i++) {
    if (ap[i] == true) {
      // FIXME: describe ho to 'use this result' ... eg, by 'updating a table holidng the max-sum' of vertices at a given time-point <-- first try scaffolding 'such an appraoch'.
      cout << i << " "; 
    }
  }
}


 
 
// Driver program to test above function
int main()
{
  // Create graphs given in above diagrams
  cout << "\nArticulation points in first graph \n";
  Graph g1(5);
  g1.addEdge(1, 0);
  g1.addEdge(0, 2);
  g1.addEdge(2, 1);
  g1.addEdge(0, 3);
  g1.addEdge(3, 4);
  g1.AP();
 
  cout << "\nArticulation points in second graph \n";
  Graph g2(4);
  g2.addEdge(0, 1);
  g2.addEdge(1, 2);
  g2.addEdge(2, 3);
  g2.AP();
 
  cout << "\nArticulation points in third graph \n";
  Graph g3(7);
  g3.addEdge(0, 1);
  g3.addEdge(1, 2);
  g3.addEdge(2, 0);
  g3.addEdge(1, 3);
  g3.addEdge(1, 4);
  g3.addEdge(1, 6);
  g3.addEdge(3, 5);
  g3.addEdge(4, 5);
  g3.AP();
 
  return 0;
}
