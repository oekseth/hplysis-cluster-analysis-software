#include "kt_clusterAlg_fixed_resultObject.h"
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

//! Intiate the s_kt_clusterAlg_fixed_resultObject_t object
void setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self) {
  assert(self);
  self->cnt_vertex = 0;
  self->clusterId_size = 0;
  self->vertex_clusterId = NULL;
  self->cluster_vertexCentroid = NULL;
  self->cluster_errors = NULL;
  self->cluster_vertexCount = NULL;
  self->sumOf_errors = 0;
  self->cntTotal_clusterAnswerFound = 0; //! the number of times an optmial sollution was found.
  self->matrix2d_vertexTo_clusterId = NULL;  
  self->cntInsertedBefore__clusters = 0; //! which is used when merging diffneret/muliple cluster-reuslts, eg, for the case where we have dused disjotinf-reost-speeraiton as a 'pre-step' (oekseth, 06. feb. 2017).
}

//! Intaite the s_kt_clusterAlg_fixed_resultObject_t object
void  init__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self, const uint nclusters, const uint cnt_vertex) {
  assert(self); assert(nclusters); assert(cnt_vertex);
  self->cnt_vertex = cnt_vertex;
  self->clusterId_size = nclusters;
  assert(nclusters > 0);
  // assert(false); // FIXME: remove!
  const uint default_value_uint = 0;   const t_float default_value_float = 0;
  //! ---
  self->vertex_clusterId = allocate_1d_list_uint(cnt_vertex, default_value_uint);
  for(uint i = 0; i < cnt_vertex; i++) {self->vertex_clusterId[i] = UINT_MAX;}
  //! ---
  self->cluster_vertexCentroid = allocate_1d_list_uint(nclusters, default_value_uint);
  self->cluster_vertexCount = allocate_1d_list_uint(nclusters, default_value_uint);
  self->cluster_errors = allocate_1d_list_float(nclusters, default_value_float);
  for(uint i = 0; i < nclusters; i++) {self->cluster_vertexCentroid[i] = UINT_MAX;}
  self->matrix2d_vertexTo_clusterId = allocate_2d_list_float(cnt_vertex, nclusters, default_value_float);
  for(uint i = 0; i < cnt_vertex; i++) {
    for(uint m = 0; m < nclusters; m++) {
      self->matrix2d_vertexTo_clusterId[i][m] = T_FLOAT_MAX;}}
  self->cntInsertedBefore__clusters = 0; //! which is used when merging diffneret/muliple cluster-reuslts, eg, for the case where we have dused disjotinf-reost-speeraiton as a 'pre-step' (oekseth, 06. feb. 2017).
}

//! Cosntucs a copy of an exisitng object, 'expanding' the number of allcoated clstuer-psotions (oekseth, 06. amr. 2017)
static s_kt_clusterAlg_fixed_resultObject_t __copyAndEnlarge(s_kt_clusterAlg_fixed_resultObject_t *super, const uint cnt_clustersNew) {
  assert(super); assert(super->cnt_vertex);
  //! intiate:
  s_kt_clusterAlg_fixed_resultObject_t self;  init__s_kt_clusterAlg_fixed_resultObject_t(&self, cnt_clustersNew, super->cnt_vertex);
  //! Copy:
  self.cntInsertedBefore__clusters = super->cntInsertedBefore__clusters;
  for(uint i = 0; i < super->cnt_vertex; i++) {
    self.vertex_clusterId[i] = super->vertex_clusterId[i];
    for(uint c = 0; c < super->clusterId_size; c++) {
      if(i == 0) { //! then we copy the clsuter-specific data:
	self.cluster_vertexCentroid[c] = super->cluster_vertexCentroid[c];
	self.cluster_vertexCount[c] = super->cluster_vertexCount[c];
	self.cluster_errors[c] = super->cluster_errors[c];
      }
      //! Copy the matrix-case:
      self.matrix2d_vertexTo_clusterId[i][c] = super->matrix2d_vertexTo_clusterId[i][c];
    }
  }
  //! @return
  return self;
}

//! Intaite the s_kt_clusterAlg_fixed_resultObject_t object with a defualt clsuter-emberships, which is used as a 'first guess', eg, in k-means and k-medoid clsuteirng (eosekth, 06. jan. 2017).
void init__setDefaultClusterVector__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self, const uint nclusters, const uint cnt_vertex, const uint *vertex_clusterId) {
  assert(self); assert(vertex_clusterId);
  //! First defualt intaition:
  init__s_kt_clusterAlg_fixed_resultObject_t(self, nclusters, cnt_vertex);
  //!
  //! Iterate:
  uint min_val = 0; uint max_val = 0;
  for(uint i = 0; i < cnt_vertex; i++) {
    const uint cluster_id = vertex_clusterId[i];
    if(cluster_id != UINT_MAX) {
      min_val = macro_min(min_val, cluster_id);
      max_val = macro_max(max_val, cluster_id);
    }
  }
  if(min_val != max_val) { //! then we udpate the clsuter-ids:
    printf("min_val=%u, max_val=%u, at %s:%d\n", min_val, max_val, __FILE__, __LINE__);
    if(max_val < nclusters) {
      for(uint i = 0; i < cnt_vertex; i++) {
	const uint cluster_id = vertex_clusterId[i];
	if(cluster_id != UINT_MAX) {
	  self->vertex_clusterId[i] = vertex_clusterId[i];
	}
      } //! else we asusem a users has not a 'pre-deifned suggesiton of cluster-emmbersips', ie, a case which we expect to the the defualt-frequenct-use-case (oesketh, 06. jan. 2017).
    } else {
      fprintf(stderr, "!!\t In-consnstent cluster-allocation: while you specified nclusters=%u the clsuters were foudn in range [%u, %u], ie, an inconsistent specificiaotn (when we asusme we start coutning on '0' and Not '1'): if reading the docuemtantion does Not help then please cotnact the senior delveoper at [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", nclusters, min_val, max_val,  __FUNCTION__, __FILE__, __LINE__);
    }
  } else {
    fprintf(stderr, "!!\t In-consnstent cluster-allocation: all clusters were specified with the same cluster-memberships-ids nclusters-range [%u, %u], ie, an pointless and/or inconsistent specificiaotn (when we asusme we start coutning on '0' and Not '1'): if reading the docuemtantion does Not help then please cotnact the senior delveoper at [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", min_val, max_val,  __FUNCTION__, __FILE__, __LINE__);
  }
}

//! @return an intiated object which hold the clsuter-mbershsip providdd/given in the the "obj_vertexToCluster" list (oekseth, 06. jul. 2017)
//! @remarks to handle cases where a vertex is Not assited to any clsuters we increment/set the max-cluster-count.
s_kt_clusterAlg_fixed_resultObject_t init_fromListOfClusterMemberships__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_list_1d_uint_t *obj_vertexToCluster, const uint defualt_vertexCount) {
  assert(obj_vertexToCluster); 
  if(obj_vertexToCluster->list_size == 0) { return initAndReturn__s_kt_clusterAlg_fixed_resultObject_t(); } //! ie, as no valeus are set.
  uint cnt_vertex = defualt_vertexCount;
  if( (defualt_vertexCount == 0) || (defualt_vertexCount == UINT_MAX) ) {
    cnt_vertex = 0;
    for(uint row_id = 0; row_id < obj_vertexToCluster->list_size; row_id++) { //! where 'this logic-chunk' is used to address the problem of 'overlfow-allcoaitons'.
      const uint clust_id = obj_vertexToCluster->list[row_id];
      if(clust_id != UINT_MAX) { cnt_vertex = row_id;}
    }
    cnt_vertex++; //! ie, to relfect the 'nubmer' rather than 'the max index'.
  }
  //! 
  //! Identify the number of clusters and vertieces:
  uint nclusters_base = 0; uint nclusters_xmtMemberships = 0;
  //  s_kt_list_1d_uint_t map_clusterId = setToEmpty__s_kt_list_1d_uint_t(); //! which is used to 'know' the 
  for(uint row_id = 0; row_id < obj_vertexToCluster->list_size; row_id++) {
    const uint clust_id = obj_vertexToCluster->list[row_id];
    if(clust_id != UINT_MAX) { 
      nclusters_base = macro_max(nclusters_base, clust_id); //! ie, as we assuemt eh clsuter-dis are contiusly/densely allcoated/set.
    } else {nclusters_xmtMemberships++;}
  }
  nclusters_base++; //! ie, to relfect the 'nubmer' rather than 'the max index'.
  const uint nclusters = nclusters_base + 1; //nclusters_xmtMemberships; // TODO: validte [”elow] assumption/understanding.
  //! 
  //! 
  if(cnt_vertex == 0) { return initAndReturn__s_kt_clusterAlg_fixed_resultObject_t(); } //! ie, as no valeus are set.
  //! 
  //! 
  s_kt_clusterAlg_fixed_resultObject_t self; init__s_kt_clusterAlg_fixed_resultObject_t(&self, nclusters, cnt_vertex); // = initAndReturn__s_kt_clusterAlg_fixed_resultObject_t();
  //! 
  //! Copy the data-set:
  //! Note: in [below] we explcitly hadle the sue-case where a clsuter-embership is Not set, ie, for whyc the "memcpy(..)" would produce worng/misleiadng/incorrect reuslts:
  uint cluster_id_curr = nclusters_base;
  for(uint row_id = 0; row_id < cnt_vertex; row_id++) {
    uint clust_id = UINT_MAX;
    if(row_id < obj_vertexToCluster->list_size) {
      clust_id = obj_vertexToCluster->list[row_id];
    }
    if(clust_id == UINT_MAX) {clust_id = nclusters_base;}
    //    if(clust_id == UINT_MAX) {clust_id = cluster_id_curr++;}
    //! Copy:
    self.vertex_clusterId[row_id] = clust_id;
  }
    //  memcpy(self.vertex_clusterId, obj_vertexToCluster->list, sizeof(uint)*cnt_vertex);
  //! 
  //! @return 
  return self;
}


//! Print the resutls of the clsutering in a 'human-redable-format' to stream_out (oesketh, 06. jan. 2017).
bool printHumanReadableResult__extensive__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, char **mapOf_names, FILE *stream_out, const char *seperator_cols, const char *seperator_newLine, const e_kt_clusterAlg_exportFormat_t enum_id) {
  if(!self || !self->cnt_vertex || !self->vertex_clusterId) {
    fprintf(stderr, "!!\t Seems like the object does not hold data, ie, please inveisgate your call. Observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  if(!stream_out) {stream_out = stdout;} //! ie, our defualt assumption.
  if(!seperator_cols) {seperator_cols = "\t";}
  if(!seperator_newLine) {seperator_newLine = "\n";}
  assert(stream_out);

  fprintf(stream_out, "#! Write out the k-means hpLysis cluster-result for a data-set with |vertices|=%u and |clusters|=%u, at %s:%d\n", self->cnt_vertex, self->clusterId_size, __FILE__, __LINE__);
  //! -----------
  if( (enum_id == e_kt_clusterAlg_exportFormat_all) || (enum_id == e_kt_clusterAlg_exportFormat_vertex_toCentroidIds)) {
    fprintf(stream_out, "#! centroid-memberships: To begin with we start priting out the centroid-memberships (which were identified during the clustering):\n");
    assert(self->vertex_clusterId);
    for(uint row_id = 0; row_id < self->cnt_vertex; row_id++) {
      const uint clsuter_id = self->vertex_clusterId[row_id];
      if(clsuter_id != UINT_MAX) {
	//assert(clsuter_id != UINT_MAX);
	if(mapOf_names) {
	  //assert(clsuter_id < self->cnt_vertex); //!
	  fprintf(stream_out, "%s%s" "in-clusterCentroid%s" "%u%s", mapOf_names[row_id], seperator_cols, seperator_cols, clsuter_id, seperator_newLine);
	  //fprintf(stream_out, "%s%s" "in-clusterCentroid%s" "%s%s", mapOf_names[row_id], seperator_cols, seperator_cols, mapOf_names[clsuter_id], seperator_newLine);
	} else {
	  fprintf(stream_out, "%u%s" "in-clusterCentroid%s" "%u%s", row_id, seperator_cols, seperator_cols, clsuter_id, seperator_newLine);
	}
      } else {
	if(mapOf_names) {
	  fprintf(stream_out, "%s%s" "in-clusterCentroid%s" "nan%s", mapOf_names[row_id], seperator_cols, seperator_cols, seperator_newLine);
	} else {
	  fprintf(stream_out, "%u%s" "in-clusterCentroid%s" "nan%s", row_id, seperator_cols, seperator_cols,  seperator_newLine);
	}
      }
    }
  }
    //! -----------
  if( (enum_id == e_kt_clusterAlg_exportFormat_all) || (enum_id == e_kt_clusterAlg_exportFormat_vertex_toClusterIds)) {
    fprintf(stream_out, "# cluster-centroids: In below we relate the cluster-IDs to the vertex-centrodis:\n");
    assert(self->cluster_vertexCentroid);
    for(uint local_cluster_id = 0; local_cluster_id < self->clusterId_size; local_cluster_id++) {
      const uint local_centroidVertex = self->cluster_vertexCentroid[local_cluster_id];    
      if(mapOf_names) {
	fprintf(stream_out, "cluster-%u%s" "has-vertex-centroid%s" "%s%s", local_cluster_id, seperator_cols, seperator_cols, mapOf_names[local_cluster_id], seperator_newLine);
      } else {
	fprintf(stream_out, "cluster-%u%s" "has-vertex-centroid%s" "%u%s", local_cluster_id, seperator_cols, seperator_cols, local_cluster_id, seperator_newLine);
      }    
    }
  }
  //! -----------
  if( (enum_id == e_kt_clusterAlg_exportFormat_all) || (enum_id == e_kt_clusterAlg_exportFormat_distanceToCentroids)) {
    fprintf(stream_out, "# centroid-candidate-distances: In below we include the distances to each of the k-means-cluster-candidates, using the syntax of <vertex><tab>{<cluster-score-at-incremental-cluster-index>}:\n");
    assert(self->vertex_clusterId);
    for(uint row_id = 0; row_id < self->cnt_vertex; row_id++) {
      if(mapOf_names) {
	fprintf(stream_out, "%s%s", mapOf_names[row_id], seperator_cols);
      } else {
	fprintf(stream_out, "%u%s", row_id, seperator_cols);
      }
      for(uint local_cluster_id = 0; local_cluster_id < self->clusterId_size; local_cluster_id++) {
	t_float score = self->matrix2d_vertexTo_clusterId[row_id][local_cluster_id];          
	if(score == T_FLOAT_MAX) {score = 0;}
	fprintf(stream_out, "%f%s", score, seperator_cols);
      }
      fprintf(stream_out, "%s", seperator_newLine);
    } 
  }

  //! ----------------------------------------
  //! At this exeuciotn-poitn we asusme the oerpation was a success.
  return true;
}

//! Print the resutls of the clsutering in a 'human-redable-format' to STDOUt.
void printHumanReadableResult__toStdout__s_kt_clusterAlg_fixed_resultObject_t(const s_kt_clusterAlg_fixed_resultObject_t *self, char **mapOf_names) {
  assert(self);
  FILE *stream_out = stdout;
  //! Apply:
  const char *seperator_cols = "\t";   const char *seperator_newLine = "\n";
  printHumanReadableResult__extensive__s_kt_clusterAlg_fixed_resultObject_t(self, mapOf_names, stream_out, seperator_cols, seperator_newLine, e_kt_clusterAlg_exportFormat_all);
}


/**
   @brief merge the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void mergeResult__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self, const s_kt_clusterAlg_fixed_resultObject *clusterResults, const uint *keyMap_localToGlobal) {
  assert(self); assert(clusterResults); 
  assert(self->matrix2d_vertexTo_clusterId); //! ie, as we expect 'this' to have been intiated
  assert(self->vertex_clusterId); //! ie, as we expect 'this' to have been intiated
  assert(clusterResults->matrix2d_vertexTo_clusterId);
  assert(clusterResults->vertex_clusterId);
  assert(self->clusterId_size > 0);
  
  // FIXME: vlaidate [”elow] assumption ... ie, where we assume that each 'vertex' points to a 'vertex-centroid'
  uint *vertex_clusterId = clusterResults->vertex_clusterId;
  assert(vertex_clusterId);
  if(keyMap_localToGlobal != NULL) {assert(self->cnt_vertex > clusterResults->cnt_vertex);}
  else {assert(self->cnt_vertex == clusterResults->cnt_vertex);}

  // fprintf(stderr, "# at %s:%d\n", __FILE__, __LINE__);
  //printf("##\t\t\t keyMap_localToGlobal=%p, at %s:%d\n", keyMap_localToGlobal, __FILE__, __LINE__); 
  {
    const uint total_after = clusterResults->clusterId_size + self->cntInsertedBefore__clusters;
    if(total_after > self->clusterId_size) {
      if(true) { //! then we allocate (oekseth, 06. mar. 2017):
	s_kt_clusterAlg_fixed_resultObject_t tmp = __copyAndEnlarge(self, /*cnt_clusters_new=*/total_after);
	//! De-allocate old:
	free__s_kt_clusterAlg_fixed_resultObject_t(self);
	//! Update:
	*self = tmp;
      } else {
	fprintf(stderr, "!!\t Pleasee reuqest support for this feature (where %u=(%u + %u) > cnt_clusters=%u): seems like we have mvoe clsuters-to-be-inserted than 'the atucal number of clsuters', ie, please cotnat the senior delvoper at [oesketh@gmail.com] iot. get this issue resolved. OBserviaotn at [%s]:%s:%d\n", total_after, clusterResults->clusterId_size, self->cntInsertedBefore__clusters, self->clusterId_size, __FUNCTION__, __FILE__, __LINE__);
	assert(false); //! ie, an heads-up ... where a 'simple-osllution is to first gnerate an object 'which is an over-estiamte' ... and thereafter 'buidl a real object' after all of the dsujiuitnf-roest-oibjects have been updated <--- wrt. paralllel support ... consider to 'make' a thread-safe verison 'fo this merge-fucnitoni ... where we use an telrantive top teh "self->cntInsertedBefore__clusters" object ... or altenriatvly merge 'muliple object-chunks' (eg, if using an MPI-baed appraoch) (oekseth, 06. feb. 2017).
      }
    }
  }

  //! Iterate:
  for(uint i = 0; i < clusterResults->cnt_vertex; i++) {
    const uint global_id_head = (keyMap_localToGlobal) ? keyMap_localToGlobal[i] : i;
    const uint centroid_id = vertex_clusterId[i];
    const uint global_id_tail = (keyMap_localToGlobal) ? keyMap_localToGlobal[centroid_id] : centroid_id;
    //!
    //! Update the 'vertex-->centroid' mapping-table:
    self->vertex_clusterId[global_id_head] = global_id_tail;
    //!
    //! Update the 'distance-matrix from teh verteix to the differnet centroids' (eg, used to simplify a users assessment of the k-means clsuters staiblity/importance).
    //const uint local_cluster_id = clusterResults->cluster_vertexCentroid[
    for(uint local_cluster_id = 0; local_cluster_id < clusterResults->clusterId_size; local_cluster_id++) {
      const uint local_centroidVertex = clusterResults->cluster_vertexCentroid[local_cluster_id];
      if(local_centroidVertex != UINT_MAX) {
	//printf("##\t\t\t local_centroidVertex[%u]=%u, keyMap_localToGlobal=%p, at %s:%d\n", local_cluster_id, local_centroidVertex, keyMap_localToGlobal, __FILE__, __LINE__); 
	assert(local_centroidVertex != UINT_MAX);
	assert(local_centroidVertex < clusterResults->cnt_vertex);
	const uint global_centroidVertex = self->cntInsertedBefore__clusters + local_cluster_id; //(keyMap_localToGlobal) ? keyMap_localToGlobal[local_centroidVertex] : local_centroidVertex;
	// printf("#\t\t [%u]\tglobal_centroidVertex=%u, cnt_clusters=%u (and |currentInputMerge|=%u), cntInsertedBefore__clusters=%u, at %s:%d\n", local_centroidVertex, global_centroidVertex, self->clusterId_size, clusterResults->clusterId_size, self->cntInsertedBefore__clusters, __FILE__, __LINE__); 
	assert(global_centroidVertex < self->clusterId_size); //! ie, given our allocation of [cnt_vertex, nclusters]
	//const uint global_centroidVertex = (keyMap_localToGlobal) ? keyMap_localToGlobal[local_centroidVertex] : local_centroidVertex;
	//printf("##\t\t\t global_centroidVertex=%u, keyMap_localToGlobal=%p, at %s:%d\n", global_centroidVertex, keyMap_localToGlobal, __FILE__, __LINE__); 
	assert(global_centroidVertex != UINT_MAX);
	if(global_centroidVertex >= clusterResults->cnt_vertex) {
	  fprintf(stderr, "!!\tERROR\t##\t\t\t global_centroidVertex=%u, keyMap_localToGlobal=%p, count(vertices):%u, at [%s]:%s:%d\n", global_centroidVertex, keyMap_localToGlobal, clusterResults->cnt_vertex, __FUNCTION__, __FILE__, __LINE__); 
	}
	// assert(global_centroidVertex < clusterResults->cnt_vertex); // FIXME: assertion deactived at july 2020; correct?
	//!
	//! Update the 'ammpign'b etwene teh vertex adn the different cadndiate-centorid-vertices which we evlauyated (in hte processing):
	assert(global_centroidVertex < self->clusterId_size); //! ie, given our allocation of [cnt_vertex, nclusters]
	assert(local_cluster_id < clusterResults->clusterId_size); //! ie, given our allocation of [cnt_vertex, nclusters]
	assert_possibleOverhead(clusterResults->matrix2d_vertexTo_clusterId);
	assert_possibleOverhead(clusterResults->matrix2d_vertexTo_clusterId[i]);
	assert_possibleOverhead(self->matrix2d_vertexTo_clusterId);
	assert_possibleOverhead(self->matrix2d_vertexTo_clusterId[global_id_head]);
	//! Update:
	self->matrix2d_vertexTo_clusterId[global_id_head][global_centroidVertex] = clusterResults->matrix2d_vertexTo_clusterId[i][local_cluster_id];          
	//self->matrix2d_vertexTo_clusterId[global_id_head][global_centroidVertex] = clusterResults->matrix2d_vertexTo_clusterId[i][local_centroidVertex];          
	// TODO: cosndier to 'add' for "cluster_vertexCount" using "cluster_vertexCentroid" ... and for ... "cluster_errors" using "cluster_vertexCentroid"	
      }
    } //! else we asusmet eh altter is not set, eg, as seen wrt. our kmenas-meodi proceudre (oekseth, 06. des. 2017).
  }
  //!
  //! Updat ethe glboal count fo cluster-ids:
  self->cntInsertedBefore__clusters += clusterResults->clusterId_size;
}


//! De-allcoates the s_kt_clusterAlg_fixed_resultObject_t object
void free__s_kt_clusterAlg_fixed_resultObject_t(s_kt_clusterAlg_fixed_resultObject_t *self) {
  assert(self);
  //! De-alloocate:
  if(self->vertex_clusterId) {free_1d_list_uint(&(self->vertex_clusterId));}
  if(self->cluster_vertexCentroid) {free_1d_list_uint(&(self->cluster_vertexCentroid));}
  if(self->cluster_vertexCount) {free_1d_list_uint(&(self->cluster_vertexCount));}
  if(self->cluster_errors) {free_1d_list_float(&(self->cluster_errors));}
  if(self->matrix2d_vertexTo_clusterId) {free_2d_list_float(&(self->matrix2d_vertexTo_clusterId), self->cnt_vertex);}
  //! Tehrafter reset the object:
  setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(self);
}
