#include "hp_evalHypothesis_algOnData.h"
#include "aux_sysCalls.h"

#if(1 == 1)
static const uint __def__setOfAlgsToEvaluate__size = e_hpLysis_clusterAlg_undef;
static const e_hpLysis_clusterAlg_t __def__setOfAlgsToEvaluate[__def__setOfAlgsToEvaluate__size] = {
  e_hpLysis_clusterAlg_kCluster__AVG, //! ie, 'k-mean'
  e_hpLysis_clusterAlg_kCluster__rank, //! ie, 'k-median'
  e_hpLysis_clusterAlg_kCluster__medoid, //! ie, 'k-medoid'
  e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch, //! an implemtnation which is inpsired by the poor implementaions found in the work of ("https://algorithmicthoughts.wordpress.com/2013/07/26/machine-learning-mini-batch-k-means/" and "https://github.com/siddharth-agrawal/Mini-Batch-K-Means")
  //! ------------------------------------------------------------
    e_hpLysis_clusterAlg_disjoint, //! ie, 'use disjoitnness of regions to idneitfy clusters': a fast appraoch which depends on masking of corrleation-valeus to be accurate.
    e_hpLysis_clusterAlg_disjoint_mclPostStrategy, //! which si a pemrtuation of "e_hpLysis_clusterAlg_disjoint_mclPostStrategy" where the MCL-strategy of merging 'non-core-vertices' (in a seprate post-sep after disjtoint-fores-tdineticiaotns) is applied/used (oekseth, 06. jul. 2017).
    e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds, //! which is a permtuation of our "e_hpLysis_clusterAlg_disjoint" where 
    e_hpLysis_clusterAlg_disjoint_MCL, //! ie, the CML-algorithm (oesketh, 06. jul. 2017).
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__AVG, //! use disjoint-forest to find the k-clsuters and therafter apply AVG-cluster-appraoch.
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__rank, //! use disjoint-forest to find the k-clsuters and therafter apply rank-cluster-appraoch.
  e_hpLysis_clusterAlg_kCluster__findK__disjoint__medoid, //! use disjoint-forest to find the k-clsuters and therafter apply medoid-cluster-appraoch.
  //! --------------------
  e_hpLysis_clusterAlg_kCluster__SOM, //! ie, use Self-Organsiing Maps (SOMs) for clustering
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_HCA_single,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_HCA_average,
  e_hpLysis_clusterAlg_HCA_centroid,
  //! ------------------------------------------------------------
  e_hpLysis_clusterAlg_kruskal_hca, //! where we construct a "Minimum Spanning Tree" (MST), and then 'assumes' that sorted order assiated to the constructed tree' describes a hioerarhccial cluster (ie, an itnerpetaiton often seen in research). 
  e_hpLysis_clusterAlg_kruskal_fixed, //! where we set a 'max-count' wrt. the clusters
  e_hpLysis_clusterAlg_kruskal_fixed_and_CCM, //! wherew we use a user-specifeid (or defualt) Cluster Comparison Metric (CCM) to idneityf the min-max-clusters thresholds.
  //! ------------------------------------------------------------
  //! Note: [below] is sued as reference-frames wrt. clustering-acccuracy, eg, to comapre/elvuate the correctness/prediocnt-acucryac of clsutierng (oesketh, 06. jul. 2017).
    e_hpLysis_clusterAlg_random_best,
    e_hpLysis_clusterAlg_random_worst,
    e_hpLysis_clusterAlg_random_average
    };
#else
static const uint __def__setOfAlgsToEvaluate__size = 3 + 4 + 2 + 2;
static const e_hpLysis_clusterAlg_t __def__setOfAlgsToEvaluate[__def__setOfAlgsToEvaluate__size] = {static const uint __def__setOfAlgsToEvaluate__size = 3 + 4 + 2 + 2;
static const e_hpLysis_clusterAlg_t __def__setOfAlgsToEvaluate[__def__setOfAlgsToEvaluate__size] = {
  e_hpLysis_clusterAlg_kCluster__AVG, //! ie, 'k-mean'
  e_hpLysis_clusterAlg_kCluster__rank, //! ie, 'k-median'
  e_hpLysis_clusterAlg_kCluster__medoid, //! ie, 'k-medoid'
  /*     ---    */
  e_hpLysis_clusterAlg_HCA_single,
  e_hpLysis_clusterAlg_HCA_max,
  e_hpLysis_clusterAlg_HCA_average,
  e_hpLysis_clusterAlg_HCA_centroid,
  /*     ---    */
  e_hpLysis_clusterAlg_disjoint_ccm_dbScan_dynamicThresholds,
  e_hpLysis_clusterAlg_disjoint_MCL,
  /*     ---    */
  e_hpLysis_clusterAlg_random_best,
  e_hpLysis_clusterAlg_random_worst,
};
#endif
/*   const bool config__isTo__useDummyDatasetForValidation = false; //! a aprameter used to reduce the exeuction-tiem when 'rpoking' assert(false) and memory-errors (oekseth, 06. feb. 2017). */

static const uint __def__setOfRandomNess_in_size = 4;
static const e_kt_randomGenerator_type_t __def__setOfRandomNess_in[__def__setOfRandomNess_in_size] = {
  e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)"
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_random,
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss,
  e_kt_randomGenerator_type_namedInPacakge__pySciKitLearn__type_kPlussPluss
};
static const uint __def__setOfRandomNess_out_size = 7;
static const e_kt_randomGenerator_type_t __def__setOfRandomNess_out[__def__setOfRandomNess_out_size] = {
  e_kt_randomGenerator_type_hyLysisDefault,
  e_kt_randomGenerator_type_hyLysisDefault__and__handleCasesWhereAllVerticesAreAssignedToSame, //! then use a post-ops to call our "math_generateDistribution__randomassign__handleCasesWhereAllVerticesAreAssignedToSame(..)"
  e_kt_randomGenerator_type_posixRan__srand__time__modulo,
  e_kt_randomGenerator_type_binominal__incrementalInList,
  //! Different 'explicit' procuedrew chih makes use of 'partial randomness' to idneitfy clusters, ie, (a) 'randomness for a subset ov ertices' adn thereafter (b) 'set clsuter-embership to the closest cadnidate/random-assignec-sltuer-vertex':
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_random,
  e_kt_randomGenerator_type_namedInPacakge__fastKmeans__type_kPlussPluss,
  e_kt_randomGenerator_type_namedInPacakge__pySciKitLearn__type_kPlussPluss
};
static const uint __def__arrOf_simAlgs_size = 2;
static const e_kt_correlationFunction_t __def__arrOf_simAlgs[__def__arrOf_simAlgs_size] = {
  e_kt_correlationFunction_groupOf_MINE_mic,
  e_kt_correlationFunction_groupOf_minkowski_euclid,
};


//! Initiates the s_hp_evalHypothesis_algOnData_t evlauation-appraoch by 'setting' default values.
s_hp_evalHypothesis_algOnData_t init__s_hp_evalHypothesis_algOnData_t(const s_hp_clusterFileCollection_t *mapOf_realLife, const uint mapOf_realLife_size) {
  assert(mapOf_realLife_size); assert(mapOf_realLife);
  s_hp_evalHypothesis_algOnData_t self;
  self.arg_npass = 1000;
  self.sizeOf__nrows = 10; self.sizeOf__ncols = 10;
  self.cntIterations__iterativeAlgs = 2;
  //  self.cntIterations__iterativeAlgs = 10;
  self.isTo__testHCA_komb__inKmeans = true;
  self.config__isTo__useDummyDatasetForValidation = false;
  self.setOfAlgsToEvaluate__size = __def__setOfAlgsToEvaluate__size;
  self.setOfAlgsToEvaluate = __def__setOfAlgsToEvaluate;
  self.setOfRandomNess_in_size = __def__setOfRandomNess_in_size;
  self.setOfRandomNess_in = __def__setOfRandomNess_in;
  self.setOfRandomNess_out_size = __def__setOfRandomNess_out_size;
  self.setOfRandomNess_out = __def__setOfRandomNess_out;
  self.arrOf_simAlgs_size = __def__arrOf_simAlgs_size;
  self.arrOf_simAlgs = __def__arrOf_simAlgs;
  //! ---------------- 
  self.mapOf_realLife_size = mapOf_realLife_size;
  self.mapOf_realLife = mapOf_realLife;
  //! ---------------- 
  self.obj_exportMatrix = init__s_hp_exportMatrix_t(NULL, 0);
  //! ----------------   
  //! @return
  return self;
}

//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).
static s_kt_matrix_t __static__readDataFromObject(s_hp_clusterFileCollection data_obj, const uint data_id, const uint sizeOf__nrows, const uint sizeOf__ncols, const bool config__isTo__useDummyDatasetForValidation) {
  //const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
  const char *stringOf_tagSample = data_obj.file_name;
  //const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
  if(stringOf_tagSample == NULL) {stringOf_tagSample =  data_obj.tag;}
  //printf("(data=%u)\t#\t[algPos=%u]\t alg_id=%u, rand_id=%u\t\t %s \t at %s:%d\n", data_id, cnt_alg_counts, alg_id, rand_id, stringOf_tagSample, __FILE__, __LINE__);
  assert(stringOf_tagSample);
  
  s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
  s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
  if(config__isTo__useDummyDatasetForValidation == false) {
    if(data_obj.file_name != NULL) {		
      obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
      if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
    } else {
      if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
	const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
	assert(is_ok);
	if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
      } else {
	fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
      }
    }
  } else { //! then we inveeigate using a dummy data-set:
    obj_matrixInput = initAndReturn__s_kt_matrix(sizeOf__nrows, sizeOf__ncols);
    for(uint i = 0; i < obj_matrixInput.nrows; i++) {
      for(uint k = 0; k < obj_matrixInput.ncols; k++) {
	obj_matrixInput.matrix[i][k] = (t_float)(i*k);
      }
    }
  }
  return obj_matrixInput;
}

bool apply__s_hp_evalHypothesis_algOnData_t(const s_hp_evalHypothesis_algOnData_t *self, const char *stringOf_resultDir) {
  assert(self);
  if(self->config__isTo__useDummyDatasetForValidation == true) {
    fprintf(stderr, "!!\t Uses a dummy data-set for validation, ie, where the reuslt of this call is pointless: do Not use this option if you are interested in clsuter-results (and Not dummy exzeuciton-time emasurements), an observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  if(stringOf_resultDir && strlen(stringOf_resultDir)) { //! then 'remove any old files which may clustter reuslts' ... and vlaidate that hte path 'acutally eixsts':
    const bool ok_remove_old_files = removeFiles_inFolder__aux_sysCalls(stringOf_resultDir);
    assert(ok_remove_old_files);
  }
  const uint mapOf_realLife_size = self->mapOf_realLife_size;
  assert(mapOf_realLife_size > 0);
  
  //! -------------------------------------
  //!
  //! Gdientify allcoataion-space which we 'need':
  uint cnt_alg_counts = 0;
  for(uint sim_id = 0; sim_id < self->arrOf_simAlgs_size; sim_id++) {
    const e_kt_correlationFunction_t sim_pre = self->arrOf_simAlgs[sim_id];
    for(uint alg_id = 0; alg_id < self->setOfAlgsToEvaluate__size; alg_id++) {
      const e_hpLysis_clusterAlg_t clusterAlg = self->setOfAlgsToEvaluate[alg_id];
      if(isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(clusterAlg)) {
	for(uint rand_id_in = 0; rand_id_in < self->setOfRandomNess_in_size; rand_id_in++) {
	  for(uint rand_id = 0; rand_id < self->setOfRandomNess_out_size; rand_id++) {
	    const e_kt_randomGenerator_type_t type_randomness = self->setOfRandomNess_out[rand_id];
	    for(uint cnt_iter = 0; cnt_iter < self->cntIterations__iterativeAlgs; cnt_iter++) {
	      for(uint clusterAlg__firstPass__ = e_hpLysis_clusterAlg_HCA_single; clusterAlg__firstPass__ <= e_hpLysis_clusterAlg_kruskal_hca; clusterAlg__firstPass__ += 1) {
		e_hpLysis_clusterAlg_t clusterAlg__firstPass = (e_hpLysis_clusterAlg_t)clusterAlg__firstPass__;
		if( (self->isTo__testHCA_komb__inKmeans == false) && (clusterAlg__firstPass != e_hpLysis_clusterAlg_kruskal_hca) ) {continue;}
		//! Increment:
		cnt_alg_counts++;
		/* if(clusterAlg__firstPass <= e_hpLysis_clusterAlg_HCA_centroid) { */
	      
		/* } else { */
	      
		/* } */
	      }
	    }
	  }
	}
      } else {
	cnt_alg_counts++;
      }
    }
  }

  //!
  //! Allocate result-container:
  s_kt_matrix_t mat_ccm_1 = initAndReturn__s_kt_matrix(cnt_alg_counts, mapOf_realLife_size);
  s_kt_matrix_t mat_ccm_2 = initAndReturn__s_kt_matrix(cnt_alg_counts, mapOf_realLife_size);
  s_kt_matrix_t mat_time  = initAndReturn__s_kt_matrix(cnt_alg_counts, mapOf_realLife_size);
  //!
  //! Allcoate space for gold-hyptoesesis:
  const uint empty_0 = 0;
  uint *mapOf_goldCluster__truthIs__simMetric = allocate_1d_list_uint(cnt_alg_counts, empty_0);
  uint *mapOf_goldCluster__truthIs__simMetric_category = allocate_1d_list_uint(cnt_alg_counts, empty_0);
  uint *mapOf_goldCluster__truthIs__algorithm = allocate_1d_list_uint(cnt_alg_counts, empty_0);
  uint *mapOf_goldCluster__truthIs__radnomness_in = allocate_1d_list_uint(cnt_alg_counts, empty_0);
  uint *mapOf_goldCluster__truthIs__radnomness_out = allocate_1d_list_uint(cnt_alg_counts, empty_0);

  //! Swet the data-strings:
  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
    const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
    const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
    if(stringOf_tagSample == NULL) {stringOf_tagSample =  self->mapOf_realLife[data_id].tag;}
    char str_local[3000]; sprintf(str_local, "%u_%s", data_id, stringOf_tagSample);
    set_string__s_kt_matrix(&mat_ccm_1, data_id, str_local, /*addFor_column=*/true);
    set_string__s_kt_matrix(&mat_ccm_2, data_id, str_local, /*addFor_column=*/true);
    set_string__s_kt_matrix(&mat_time, data_id, str_local, /*addFor_column=*/true);
  }
  cnt_alg_counts = 0;
  {
    //! 
    //! Seprately ivnestigate ofr diferent nroamlziaiton-appraoches: 
    for(uint isTo_normalize = 0; isTo_normalize <= e_kt_normType_undef; isTo_normalize++) {
      //! Describe the data-marging-strategies: 
      enum {
	ccm_min_score, ccm_min_simMetricId, 
	ccm_max_score, ccm_max_simMetricId,
	ccm_euclid_min_score, 
	//ccm_euclid_max_score,
	//! -----------------
	ccm_undef
      };
      //!
      //! Itniate result-matrices:
      s_kt_matrix_t mat_result[ccm_undef];
      for(uint case_id = 0; case_id < ccm_undef; case_id++) {
	mat_result[case_id] = initAndReturn__s_kt_matrix(mapOf_realLife_size, self->setOfAlgsToEvaluate__size);
	//! Set column-headers:
	for(uint alg_id = 0; alg_id < self->setOfAlgsToEvaluate__size; alg_id++) {
	  const e_hpLysis_clusterAlg_t clusterAlg = self->setOfAlgsToEvaluate[alg_id];
	  const char *str = get_stringOf__short__e_hpLysis_clusterAlg_t(clusterAlg);	  
	  set_stringConst__s_kt_matrix(&(mat_result[case_id]), alg_id, str, /*addFor_column=*/true);
	}
      }

      for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
	//! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
	s_kt_matrix_t obj_matrixInput = __static__readDataFromObject(self->mapOf_realLife[data_id], data_id, self->sizeOf__nrows, self->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
	assert(obj_matrixInput.nrows > 0);
	//! 
	//! Set row-header:
	for(uint case_id = 0; case_id < ccm_undef; case_id++) {
	  const char *str = self->mapOf_realLife[data_id].tag;
	  assert(str); assert(strlen(str));
	  set_stringConst__s_kt_matrix(&(mat_result[case_id]), data_id, str, /*addFor_column=*/false);
	}
	
	//!
	//! Apply noramizaiotn, ie, if requested:
	s_kt_matrix_t mat_copy = obj_matrixInput; //setToEmptyAndReturn__s_kt_matrix_t();
	if(isTo_normalize != e_kt_normType_undef) {
	  mat_copy = initAndReturn__copy__normalizedByEnum__s_kt_matrix_t(&obj_matrixInput, (e_kt_normType_t)isTo_normalize);
	  //free__s_kt_matrix(&obj_matrixInput); //! ie, as we assuem [below] has 'perofmred' a deed copy-oepration.
	  //obj_matrixInput = mat_copy; //! ie, a sahholw copy.
	}
	
	//! 
	for(uint alg_id = 0; alg_id < self->setOfAlgsToEvaluate__size; alg_id++) {
	  const e_hpLysis_clusterAlg_t clusterAlg = self->setOfAlgsToEvaluate[alg_id];
	  //const char *str = get_stringOf__e_hpLysis_clusterAlg_t(clusterAlg);
	  //char str_local[3000]; sprintf(str_local, "%s_%s.s_%u.r_%u_%u.#_%u", get_stringOf__e_hpLysis_clusterAlg_t(clusterAlg), alg_2, sim_id, rand_id_in, rand_id, cnt_iter);
	  
	  
	  for(uint sim_id = 0; sim_id < self->arrOf_simAlgs_size; sim_id++) {
	    const e_kt_correlationFunction_t sim_pre = self->arrOf_simAlgs[sim_id];
	    
	    
	    //! Wirte out proepteis wrt. latter:
	    //! Buidl a file-path: 
	    //allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_subset_%u_simMetricToFeatures_split%u_%u_factor_%u", resultPrefix_input, matrix_count, split_id, factorMul, factor);
	    //! 
	    //! The data-consturciton-call:
	    //	  __writeOutFacts_matrix(synData_spec.matrixOf_features, resultPrefix_local, NULL, NULL, isTo_writeOutTransposed);
	    
	    //! 
	    uint nclusters = self->mapOf_realLife[data_id].k_clusterCount;
	    //if( (nclusters < 2) || (nclusters >= mat_copy.nrows)) {nclusters = 2;}
	    if(nclusters <= 2) {nclusters = UINT_MAX;} //! ie, as we then assume that the k-cluster-count is Not explict set/well-defined (oekseth, 06. jul. 2017).
	    //assert(nclusters != UINT_MAX); 	    assert(nclusters >= 2);
	    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	    obj_hp.config.corrMetric_prior_use = true; //! ie, applky a 'defualt simalrity-emtric'
	    obj_hp.config.corrMetric_prior.metric_id = sim_pre;
	    /* obj_hp.randomConfig.config.typeOf_randomNess = type_randomness; //! ie, the randomenss to be 'used' inside clsuter. */
	    /* obj_hp.randomConfig.config_init.typeOf_randomNess = type_randomness_in; //! ie, the randomenss to be 'used' inside clsuter. */
	    //!
	    //! Start the timer: 
	    start_time_measurement(); //! defined in our "measure_base.h"
	    const e_hpLysis_clusterAlg_t clusterAlg__firstPass = e_hpLysis_clusterAlg_HCA_single;
	    if((clusterAlg__firstPass == e_hpLysis_clusterAlg_kruskal_hca) 
	       || !isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(clusterAlg__firstPass)
	       ){ //e_hpLysis_clusterAlg_HCA_centroid) {	      
	      //printf("\t starts-clusterinig\t at %s:%d\n", __FILE__, __LINE__);
	      const bool is_also_ok = cluster__hpLysis_api (
							    &obj_hp, clusterAlg, &mat_copy,
							    /*nclusters=*/ nclusters, /*npass=*/ self->arg_npass
							    );
	      assert(is_also_ok);
	    } else {
	      //! Then we apply the 'nvoel call' examplifed in this example-case:
	      // printf("\t starts-clusterinig\t at %s:%d\n", __FILE__, __LINE__);
	      const bool is_ok = cluster__kMeansTwoPass__hpLysis_api(&obj_hp, clusterAlg, &obj_matrixInput, nclusters, self->arg_npass, /*cofnig-firstPass*/obj_hp.config, clusterAlg__firstPass);
	      assert(is_ok);	      
	    }	    
	    //! ---
	    assert(cnt_alg_counts < mat_time.nrows); //! ie, the cosnstienc-chekc.
	    //!
	    //! Finalise the timing-oepraiton: 
	    const t_float exex_time = end_time_measurement("Completed Exeution", FLT_MAX); //! defined in our "measure_base.h"
	    mat_time.matrix[cnt_alg_counts][data_id] = exex_time;
	    //! ------------------------------------------
	    //! 
	    //! Compute CCMs:
	    //printf("\t comptue-CCMs\t at %s:%d\n", __FILE__, __LINE__);

	    if(alg_id == 0) { //! ie, only exprot for the 'first' algrotim-case:
	      // FIXME: make [”elow] optioannl ... which is omitted to reduce the clsutter wr.t the result-generiaotn
	      if(false) {
		//! 
		//! Comptue the simlarity-metic and apply clustering, and write out the latter, and write out proepteis wrt. latter:
		//! Buidl a file-path: 	      
		allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_simMetricResult_each_norm_%u_data_%u_%s_simMatrix_%u_%s", stringOf_resultDir, isTo_normalize, data_id, self->mapOf_realLife[data_id].tag, sim_id, get_stringOf_enum__e_kt_correlationFunction_t_xmtPrefix(sim_pre));
		//! Write out the data- exporting into a multiude of data-export-formats:
		writeOut__s_hp_exportMatrix_t(&(self->obj_exportMatrix), obj_hp.matrixResult__inputToClustering, resultPrefix_local, NULL, NULL, self->obj_exportMatrix.isTo_writeOutTransposed);
	      }
	    }
	    //! 
	    //! 
	    { //! Evalaute cluster-accuracy using/through different matrix-based CCMs:
	      const t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, NULL);
	      //if(CCM_score == T_FLOAT_MAX) {CCM_score = 0;}
	      if(CCM_score != T_FLOAT_MAX) {
		//!
		if(sim_pre == e_kt_correlationFunction_groupOf_minkowski_euclid) { //! Update the Euclid-table (which is sued as a reference wrt. a default/esltibhed appraoch.
		  mat_result[ccm_euclid_min_score].matrix[data_id][alg_id] = CCM_score;		  
		}
		//!
		//! Updat ethe min-max-tables. 
		if(mat_result[ccm_min_score].matrix[data_id][alg_id] != T_FLOAT_MAX) {
		  if(mat_result[ccm_min_score].matrix[data_id][alg_id]  > CCM_score) { //! then udapte for: min:
		    mat_result[ccm_min_score].matrix[data_id][alg_id] = CCM_score;
		    mat_result[ccm_min_simMetricId].matrix[data_id][alg_id] = (t_float)sim_pre;
		  }
		  if(mat_result[ccm_max_score].matrix[data_id][alg_id]  < CCM_score) { //! then udapte for: max:
		    mat_result[ccm_max_score].matrix[data_id][alg_id] = CCM_score;
		    mat_result[ccm_max_simMetricId].matrix[data_id][alg_id] = (t_float)sim_pre;
		  }
		} else { //! then we set for all of the cases:
		  //! Set CCM-score:
		  mat_result[ccm_min_score].matrix[data_id][alg_id] 
		    = mat_result[ccm_max_score].matrix[data_id][alg_id] 
		    = CCM_score;
		  //! Set sim-metric-id:
		  mat_result[ccm_min_simMetricId].matrix[data_id][alg_id] 
		    = mat_result[ccm_max_simMetricId].matrix[data_id][alg_id] 
		    = (t_float)sim_pre;
		}
	      }
	      //mat_ccm_1.matrix[cnt_alg_counts][data_id] = CCM_score;
	    }
	    /* { */
	    /*   t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, NULL, NULL); */
	    /*   if(CCM_score == T_FLOAT_MAX) {CCM_score = 0;} */
	    /*   mat_ccm_2.matrix[cnt_alg_counts][data_id] = CCM_score; */
	    /* } */
	    //! 
	    //! Evalaute cluster-accuracy using/through different matrix-based CCMs, ie where we compare with the/a gold-standard data-set:
	    // TODO: consider adding something.

	    //!
	    //! De-allocate
	    free__s_hpLysis_api_t(&obj_hp);	    
	    
	  }
	}
	if(isTo_normalize != e_kt_normType_undef) {
	  free__s_kt_matrix(&mat_copy);
	}
	free__s_kt_matrix(&obj_matrixInput);
      }
      //!
      //! Write out the results:
      for(uint case_id = 0; case_id < ccm_undef; case_id++) {
	const char *str_case = "ccm_min";
	if(case_id == ccm_min_simMetricId) {str_case = "ccm_min_simId";}
	else if(case_id == ccm_max_score) {str_case = "ccm_max";}
	else if(case_id == ccm_max_simMetricId) {str_case = "ccm_max_simId";}
	else if(case_id == ccm_euclid_min_score) {str_case = "ccm_min_Euclid";}
	//else if(case_id == ccm_euclid_max_score) {str_case = "ccm_max_Euclid";}
	else if(case_id != ccm_min_score) {assert(false);} //! ie, as we then need adding support for this
	//! Construc thte string:
	allocOnStack__char__sprintf(2000, resultPrefix_local, "%s_simMetricResult_summary_norm_%u_case_%s", stringOf_resultDir, isTo_normalize, 
				    str_case);
	//! 
	//! Write out the data- exporting into a multiude of data-export-formats:
	writeOut__s_hp_exportMatrix_t(&(self->obj_exportMatrix), mat_result[case_id], resultPrefix_local, NULL, NULL, self->obj_exportMatrix.isTo_writeOutTransposed);
      }
      { //! Then write out a matrix which hold a consnesed summary-proerpty wrt. [above] (oekseth, 06. jul. 2017).
	enum {
	  min_ccmScore, max_ccmScore, 
	  min_ccmScore_Euclid, max_ccmScore_Euclid, 
	  min_algId, min_simId, 
	  max_algId, max_simId,
	  relativeDiff,
	  cnt_summaries
	};
	s_kt_matrix_t mat_result_summary = initAndReturn__s_kt_matrix(mat_result[0].nrows, cnt_summaries);
	for(uint i = 0; i < mat_result_summary.nrows; i++) {
	  const char *str = getAndReturn_string__s_kt_matrix(&mat_result[0], i, /*addFor_column=*/false);
	  if(str && strlen(str)) {
	    set_stringConst__s_kt_matrix(&mat_result_summary, i, str, /*addFor_column=*/false);
	  }
	}
	// --
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/min_ccmScore, "min(ccmScore)", /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/max_ccmScore, "max(ccmScore)", /*addFor_column=*/true);
	// --
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/min_ccmScore_Euclid, "min(ccmScore:euclid)", /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/max_ccmScore_Euclid, "max(ccmScore:euclid)", /*addFor_column=*/true);
	// --
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/min_algId, "min(algId)", /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/min_simId, "min(sim-id)", /*addFor_column=*/true);
	// --
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/max_algId, "max(algId)", /*addFor_column=*/true);
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/max_simId, "max(sim-id)", /*addFor_column=*/true);
	// --
	set_stringConst__s_kt_matrix(&mat_result_summary, /*index=*/relativeDiff, "relative(max/min)", /*addFor_column=*/true);
	//! 
	//! Find the extnrees for each matrix:
	for(uint row_id = 0; row_id < mat_result_summary.nrows; row_id++) {
	  t_float score_max = T_FLOAT_MIN_ABS; uint score_max_index = 0; t_float score_max_index_simId = 0;
	  for(uint col_id = 0; col_id < mat_result[ccm_max_score].ncols; col_id++) {
#define _score mat_result[ccm_max_score].matrix[row_id][col_id]
	    if(isOf_interest(_score)) {
	      if(_score > score_max) {
		score_max = _score;
		score_max_index = col_id;
		score_max_index_simId = mat_result[ccm_max_simMetricId].matrix[row_id][col_id];
	      }
	    }
#undef _score
	  }
	  // -----------------
	  t_float score_max_Euclid = T_FLOAT_MIN_ABS;
	  for(uint col_id = 0; col_id < mat_result[ccm_max_score].ncols; col_id++) {
#define _score mat_result[ccm_euclid_min_score].matrix[row_id][col_id]
	    if(isOf_interest(_score)) {
	      if(_score > score_max_Euclid) {
		score_max_Euclid = _score;
		//score_max_index = col_id;
		//score_max_index_simId = mat_result[ccm_max_score].matrix[row_id][col_id];
	      }
	    }
#undef _score
	  }
	  // -----------------
	  t_float score_min_Euclid = T_FLOAT_MAX;
	  for(uint col_id = 0; col_id < mat_result[ccm_max_score].ncols; col_id++) {
#define _score mat_result[ccm_euclid_min_score].matrix[row_id][col_id]
	    if(isOf_interest(_score)) {
	      if(_score < score_max_Euclid) {
		score_max_Euclid = _score;
		//score_max_index = col_id;
		//score_max_index_simId = mat_result[ccm_max_score].matrix[row_id][col_id];
	      }
	    }
#undef _score
	  }
	  // -----------------
	  t_float score_min = T_FLOAT_MAX; uint score_min_index = 0; t_float score_min_index_simId = 0;
	  for(uint col_id = 0; col_id < mat_result[ccm_min_score].ncols; col_id++) {
#define _score mat_result[ccm_min_score].matrix[row_id][col_id]
	    if(isOf_interest(_score)) {
	      if(_score < score_min) {
		score_min = _score;
		score_min_index = col_id;
		score_min_index_simId = mat_result[ccm_min_simMetricId].matrix[row_id][col_id];
	      }
	    }
#undef _score
	  }
	  t_float score_relative = 1;
	  if(score_max != T_FLOAT_MIN_ABS) {
	    score_relative = score_max / score_min;
	  }
	  //! 
	  //! Add:
	  mat_result_summary.matrix[row_id][min_ccmScore] = score_min;
	  mat_result_summary.matrix[row_id][max_ccmScore] = score_max;
	  // --
	  mat_result_summary.matrix[row_id][min_ccmScore_Euclid] = score_min_Euclid;
	  mat_result_summary.matrix[row_id][max_ccmScore_Euclid] = score_max_Euclid;
	  // --
	  mat_result_summary.matrix[row_id][min_algId] = (t_float)score_min_index;
	  mat_result_summary.matrix[row_id][min_simId] = score_min_index_simId;
	  // --
	  mat_result_summary.matrix[row_id][max_algId] = (t_float)score_max_index;
	  mat_result_summary.matrix[row_id][max_simId] = score_max_index_simId;
	  // --
	  mat_result_summary.matrix[row_id][relativeDiff] = score_relative;
	}	
	//! 
	//!
	{ //! Wrtie out to .tsv:
	//! Construc thte string:
	  allocOnStack__char__sprintf(2000, stringOf_resultFile, "%s_simMetricResult_minMaxsummary_norm_%u.tsv", stringOf_resultDir, isTo_normalize);
	  export__singleCall__s_kt_matrix_t(&mat_result_summary, stringOf_resultFile, NULL); 
	}
	//!
	//! De-allcoate: 
	free__s_kt_matrix(&mat_result_summary);
      }
    }    
  }

  if(false) { //! which is out-commetned to reduce the exueicont-itme (oekseth, 06. jul. 2017)
    for(uint sim_id = 0; sim_id < self->arrOf_simAlgs_size; sim_id++) {
      const e_kt_correlationFunction_t sim_pre = self->arrOf_simAlgs[sim_id];
      for(uint alg_id = 0; alg_id < self->setOfAlgsToEvaluate__size; alg_id++) {
	const e_hpLysis_clusterAlg_t clusterAlg = self->setOfAlgsToEvaluate[alg_id];
	if(isOf_type_interativeRandom__e_hpLysis_clusterAlg_t(clusterAlg)) {
	  for(uint rand_id_in = 0; rand_id_in < self->setOfRandomNess_in_size; rand_id_in++) {
	    const e_kt_randomGenerator_type_t type_randomness_in = self->setOfRandomNess_in[rand_id_in];
	    for(uint rand_id = 0; rand_id < self->setOfRandomNess_out_size; rand_id++) {
	      const e_kt_randomGenerator_type_t type_randomness = self->setOfRandomNess_out[rand_id];
	      for(uint cnt_iter = 0; cnt_iter < self->cntIterations__iterativeAlgs; cnt_iter++) {
		for(uint clusterAlg__firstPass__ = e_hpLysis_clusterAlg_HCA_single; clusterAlg__firstPass__ <= e_hpLysis_clusterAlg_kruskal_hca; clusterAlg__firstPass__ += 1) {
		  e_hpLysis_clusterAlg_t clusterAlg__firstPass = (e_hpLysis_clusterAlg_t)clusterAlg__firstPass__;
		  if( (self->isTo__testHCA_komb__inKmeans == false) && (clusterAlg__firstPass != e_hpLysis_clusterAlg_kruskal_hca) ) {continue;}
		  { //! Set the row-header:
		    const char *alg_2 = get_stringOf__e_hpLysis_clusterAlg_t(clusterAlg__firstPass);
		    if(clusterAlg__firstPass == e_hpLysis_clusterAlg_kruskal_hca) {alg_2 = "none";}
		    char str_local[3000]; sprintf(str_local, "%s_%s.s_%u.r_%u_%u.#_%u", get_stringOf__e_hpLysis_clusterAlg_t(clusterAlg), alg_2, sim_id, rand_id_in, rand_id, cnt_iter);
		    set_string__s_kt_matrix(&mat_ccm_1, cnt_alg_counts, str_local, /*addFor_column=*/false);
		    set_string__s_kt_matrix(&mat_ccm_2, cnt_alg_counts, str_local, /*addFor_column=*/false);
		    set_string__s_kt_matrix(&mat_time, cnt_alg_counts, str_local, /*addFor_column=*/false);
		  }
		  assert(cnt_alg_counts < mat_time.nrows); //! ie, the cosnstienc-chekc.
		  //! Specify the gold-standard-clsuter-emmberships-assumptioons:
		  mapOf_goldCluster__truthIs__radnomness_in[cnt_alg_counts] = rand_id_in; //! ie, randomenss-configruaitons to [”elow]
		  mapOf_goldCluster__truthIs__radnomness_out[cnt_alg_counts] = rand_id; //! ie, randomenss-configruaitons to [”elow]
		  mapOf_goldCluster__truthIs__simMetric[cnt_alg_counts] = sim_id;
		  { //! Set the 'generalized cateogyræ' assicated tot he simlairty-metic:
		    s_kt_correlationMetric_t tmp = setTo_empty__andReturn__s_kt_correlationMetric_t();
		    tmp.metric_id = sim_pre;
		    mapOf_goldCluster__truthIs__simMetric_category[cnt_alg_counts] = getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t(tmp);
		  }
		  mapOf_goldCluster__truthIs__algorithm[cnt_alg_counts] = clusterAlg;
		  //! -------------------------------------
		  //!
		  //! iterate throguh the data:
		  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
		    //! Read either a real-lfie data-set or a sytneitc data-set and then reutrn the reelst (oekseth, 06. jun. 2017).	    
		    s_kt_matrix_t obj_matrixInput = __static__readDataFromObject(self->mapOf_realLife[data_id], data_id, self->sizeOf__nrows, self->sizeOf__ncols, self->config__isTo__useDummyDatasetForValidation);
		    //! 
		    uint nclusters = self->mapOf_realLife[data_id].k_clusterCount;
		    if( (nclusters < 2) || (nclusters >= obj_matrixInput.nrows)) {nclusters = 2;}
		    assert(nclusters != UINT_MAX); 	    assert(nclusters >= 2);
		    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
		    obj_hp.config.corrMetric_prior_use = true; //! ie, applky a 'defualt simalrity-emtric'
		    obj_hp.config.corrMetric_prior.metric_id = sim_pre;
		    obj_hp.randomConfig.config.typeOf_randomNess = type_randomness; //! ie, the randomenss to be 'used' inside clsuter.
		    obj_hp.randomConfig.config_init.typeOf_randomNess = type_randomness_in; //! ie, the randomenss to be 'used' inside clsuter.
		    //!
		    //! Start the timer: 
		    start_time_measurement(); //! defined in our "measure_base.h"
		    bool is_ok = true;
		    if(clusterAlg__firstPass == e_hpLysis_clusterAlg_kruskal_hca) { //e_hpLysis_clusterAlg_HCA_centroid) {	      
		      printf("\t starts-clusterinig\t at %s:%d\n", __FILE__, __LINE__);
		      is_ok = cluster__hpLysis_api (
						    &obj_hp, clusterAlg, &obj_matrixInput,
						    /*nclusters=*/ nclusters, /*npass=*/ self->arg_npass
						    );
		      //assert(is_also_ok);
		    } else {
		      //! Then we apply the 'nvoel call' examplifed in this example-case:
		      printf("\t starts-clusterinig\t at %s:%d\n", __FILE__, __LINE__);
		      //const bool is_ok = 
		      is_ok = cluster__kMeansTwoPass__hpLysis_api(&obj_hp, clusterAlg, &obj_matrixInput, nclusters, self->arg_npass, /*cofnig-firstPass*/obj_hp.config, clusterAlg__firstPass);
		      //assert(is_ok);	      
		    }	    
		    //! ---
		    assert(cnt_alg_counts < mat_time.nrows); //! ie, the cosnstienc-chekc.
		    //!
		    //! Finalise the timing-oepraiton: 
		    const t_float exex_time = end_time_measurement("Completed Exeution", FLT_MAX); //! defined in our "measure_base.h"
		    mat_time.matrix[cnt_alg_counts][data_id] = exex_time;
		    if(is_ok) {
		      //! ------------------------------------------
		      //! 
		      //! Compute CCMs:
		      printf("\t comptue-CCMs\t at %s:%d\n", __FILE__, __LINE__);
		      {
			t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, NULL);
			if(CCM_score == T_FLOAT_MAX) {CCM_score = 0;}
			mat_ccm_1.matrix[cnt_alg_counts][data_id] = CCM_score;
		      }
		      {
			t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, NULL, NULL);
			if(CCM_score == T_FLOAT_MAX) {CCM_score = 0;}
			mat_ccm_2.matrix[cnt_alg_counts][data_id] = CCM_score;
		      }
		    }
		    //!
		    //! De-allocate
		    free__s_hpLysis_api_t(&obj_hp);
		    free__s_kt_matrix(&obj_matrixInput);
		  }
		  cnt_alg_counts++;
		}
	      }
	    }
	  }
	} else {
	  { //! Set the row-header:
	    char str_local[3000]; sprintf(str_local, "%s.s_%u", get_stringOf__e_hpLysis_clusterAlg_t(clusterAlg), sim_id);
	    set_string__s_kt_matrix(&mat_ccm_1, cnt_alg_counts, str_local, /*addFor_column=*/false);
	    set_string__s_kt_matrix(&mat_ccm_2, cnt_alg_counts, str_local, /*addFor_column=*/false);
	    set_string__s_kt_matrix(&mat_time, cnt_alg_counts, str_local, /*addFor_column=*/false);
	  }
	  //! Specify the gold-standard-clsuter-emmberships-assumptioons:
	  assert(cnt_alg_counts < mat_time.nrows); //! ie, the cosnstienc-chekc.
	  //! ---
	  mapOf_goldCluster__truthIs__radnomness_in[cnt_alg_counts] = self->setOfRandomNess_in_size; //! ie, as 'no randomness' is used for HCA-based clustering, ie, a 'cotnrol gorup' to evlaute
	  mapOf_goldCluster__truthIs__radnomness_out[cnt_alg_counts] = self->setOfRandomNess_out_size; //! ie, as 'no randomness' is used for HCA-based clustering, ie, a 'cotnrol gorup' to evlaute
	  mapOf_goldCluster__truthIs__simMetric[cnt_alg_counts] = sim_id;
	  { //! Set the 'generalized cateogyræ' assicated tot he simlairty-metic:
	    s_kt_correlationMetric_t tmp = setTo_empty__andReturn__s_kt_correlationMetric_t();
	    tmp.metric_id = sim_pre;
	    mapOf_goldCluster__truthIs__simMetric_category[cnt_alg_counts] = getEnum__e_kt_correlationMetric_initCategory_timeComplexitySet__medium_t(tmp);
	  }
	  mapOf_goldCluster__truthIs__algorithm[cnt_alg_counts] = clusterAlg;
	  //! -------------------------------------
	  //!
	  //! iterate throguh the data:
	  //! -------------------------------------
	  //!
	  //! iterate throguh the data:
	  for(uint data_id = 0; data_id < mapOf_realLife_size; data_id++) {
	    const s_hp_clusterFileCollection data_obj = self->mapOf_realLife[data_id];
	    const char *stringOf_tagSample = self->mapOf_realLife[data_id].file_name;
	    printf("#\t[algPos=%u]\t %s \t at %s:%d\n", cnt_alg_counts, stringOf_tagSample, __FILE__, __LINE__);
	    if(stringOf_tagSample == NULL) {stringOf_tagSample =  self->mapOf_realLife[data_id].tag;}
	    s_kt_matrix_fileReadTuning_t fileRead_config = data_obj.fileRead_config; //! ie, the løoocal 'spec'.
	    
	    s_kt_matrix_t obj_matrixInput; setTo_empty__s_kt_matrix_t(&obj_matrixInput);
	    if(self->config__isTo__useDummyDatasetForValidation == false) {
	      if(self->mapOf_realLife[data_id].file_name != NULL) {
		obj_matrixInput = readFromAndReturn__file__advanced__s_kt_matrix_t(stringOf_tagSample, fileRead_config);    
	      } else {
		if( (fileRead_config.mat_concat != NULL) && (fileRead_config.mat_concat->nrows > 0)  && (fileRead_config.mat_concat->ncols > 0) ) {
		  const bool is_ok = init__copy__s_kt_matrix(&obj_matrixInput, fileRead_config.mat_concat, /*isTo_updateNames=*/true);
		  assert(is_ok);
		  if(!obj_matrixInput.ncols || !obj_matrixInput.nrows) { assert(false); }
		} else {
		  fprintf(stderr, "!!\t File=\"%s\" has Not any assicated data: neither the file-name nor the mat_concat is set: pelase validate your configuraiton-object. For questionst please cotnat seniopr delvoeper [eosketh@gmail.com]. Observaiton at [%s]:%s:%d\n", stringOf_tagSample, __FUNCTION__, __FILE__, __LINE__);
		}
	      }
	    } else { //! then we inveeigate using a dummy data-set:
	      obj_matrixInput = initAndReturn__s_kt_matrix(self->sizeOf__nrows, self->sizeOf__ncols);
	      for(uint i = 0; i < obj_matrixInput.nrows; i++) {
		for(uint k = 0; k < obj_matrixInput.ncols; k++) {
		  obj_matrixInput.matrix[i][k] = (t_float)(i*k);
		}
	      }
	    }	    
	    uint nclusters = self->mapOf_realLife[data_id].k_clusterCount;
	    if( (nclusters < 2) || (nclusters >= obj_matrixInput.nrows)) {nclusters = 2;}
	    assert(nclusters != UINT_MAX);
	    assert(nclusters >= 2);
	    s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	    obj_hp.config.corrMetric_prior_use = true; //! ie, applky a 'defualt simalrity-emtric'
	    obj_hp.config.corrMetric_prior.metric_id = sim_pre;
	    //!
	    //! Start the timer: 
	    start_time_measurement(); //! defined in our "measure_base.h"
	    
	    const bool is_also_ok = cluster__hpLysis_api (
							  &obj_hp, clusterAlg, &obj_matrixInput,
							  /*nclusters=*/ nclusters, /*npass=*/ self->arg_npass
							  );
	    assert(is_also_ok);
	    //! ---
	    assert(cnt_alg_counts < mat_time.nrows); //! ie, the cosnstienc-chekc.
	    //!
	    //! Finalise the timing-oepraiton: 
	    const t_float exex_time = end_time_measurement("Completed Exeution", FLT_MAX); //! defined in our "measure_base.h"
	    mat_time.matrix[cnt_alg_counts][data_id] = exex_time;
	    //! ------------------------------------------
	    //! 
	    //! Compute CCMs:
	    {
	      t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette, NULL, NULL);
	      if(CCM_score == T_FLOAT_MAX) {CCM_score = 0;}
	      mat_ccm_1.matrix[cnt_alg_counts][data_id] = CCM_score;
	    }
	    {
	      t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns, NULL, NULL);
	      if(CCM_score == T_FLOAT_MAX) {CCM_score = 0;}
	      mat_ccm_2.matrix[cnt_alg_counts][data_id] = CCM_score;
	    }
	    //!
	    //! De-allocate
	  free__s_hpLysis_api_t(&obj_hp);
	  free__s_kt_matrix(&obj_matrixInput);
	  }
	  cnt_alg_counts++;
	}
      }
    }
  //! -------------------------------------------------------------------------
  //!
  //! Prepare data-export:
  char str_filePath[2000] = {'\0'}; 
  if(stringOf_resultDir && strlen(stringOf_resultDir)) {
    if(stringOf_resultDir[strlen(stringOf_resultDir)-1] == '/') {
      sprintf(str_filePath, "%s", stringOf_resultDir);
    } else {sprintf(str_filePath, "%s/", stringOf_resultDir);}
  }  
  //! Specificy an 'itnernal' wrapper-matcro to export the reuslts:
#define __Mi__export(matrix, suffix) ({ \
    char str_conc[2000] = {'\0'}; sprintf(str_conc, "%s%s.tsv", str_filePath, suffix); \
    bool is_ok = export__singleCall__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); \
    memset(str_conc, '\0', 2000); sprintf(str_conc, "%s%s.transposed.tsv", str_filePath, suffix); \
    /*! Then export a tranpsoed 'verison': */ \
    is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); })
  //! -------------------------------------------------------------------------
  //!
  //! Write out the result-matrices, both 'as-is' and 'transposed':
  __Mi__export(&mat_ccm_1, "result__ccm_case_1");
  __Mi__export(&mat_ccm_2, "result__ccm_case_2");
  __Mi__export(&mat_time, "result__time_s");

  { //! Evalaute the accuracy of the different 'predictors'
    const uint mat_case_size = 3;
    const uint gold_case_size = 5;
    const uint nrows = mat_case_size * gold_case_size;
    const uint ncols = e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
    s_kt_matrix_t mat_gold  = initAndReturn__s_kt_matrix(nrows, ncols);
    uint row_id = 0;
    for(uint mat_case = 0; mat_case < mat_case_size; mat_case++) {
      s_kt_matrix_t *obj_matrixInput = NULL;
      const char *str_mat = NULL;
      if(mat_case == 0) {obj_matrixInput = &mat_ccm_1; str_mat = "ccm_1";} 
      else if(mat_case == 1) {obj_matrixInput = &mat_ccm_2; str_mat = "ccm_2";} 
      else if(mat_case == 2) {obj_matrixInput = &mat_time; str_mat = "time";} 
      else {assert(false);}
      for(uint gold_case = 0; gold_case < gold_case_size; gold_case++) {
	uint *list_hypo = NULL;
	const char *str_hypo = NULL;
	if(gold_case == 0) {list_hypo = mapOf_goldCluster__truthIs__simMetric; str_hypo = "simMetric";} 
	else if(gold_case == 1) {list_hypo = mapOf_goldCluster__truthIs__simMetric_category; str_hypo = "simMCategory";} 
	else if(gold_case == 2) {list_hypo = mapOf_goldCluster__truthIs__algorithm; str_hypo = "clustAlg";} 
	else if(gold_case == 3) {list_hypo = mapOf_goldCluster__truthIs__radnomness_in; str_hypo = "random::init";}
	else if(gold_case == 4) {list_hypo = mapOf_goldCluster__truthIs__radnomness_out; str_hypo = "random::inside";}
	else {assert(false);}
	{ //! Set the row-header:
	  char str_local[3000]; sprintf(str_local, "matr_%s.hypo_%s", str_mat, str_hypo);
	  set_string__s_kt_matrix(&mat_gold, row_id, str_local, /*addFor_column=*/false);
	}
	//! -------------------------------------
	//!
	//! iterate throguh the data:
	//! -------------------------------------
	//!
       
	uint nclusters = 0;   assert(list_hypo); for(uint i = 0; i < cnt_alg_counts; i++) {nclusters = macro_max(nclusters, list_hypo[i]);}
	if( (nclusters < 2) || (nclusters >= obj_matrixInput->nrows)) {nclusters = 2;}
	assert(nclusters != UINT_MAX);
	assert(nclusters >= 2);
	s_hpLysis_api_t obj_hp = setToEmpty__s_hpLysis_api_t(NULL, NULL);
	obj_hp.config.corrMetric_prior_use = true; //! ie, applky a 'defualt simalrity-emtric'
	//obj_hp.config.corrMetric_prior.metric_id = sim_pre;
       
	const bool is_ok = filterAmdCorrelate__hpLysis_api(&obj_hp, obj_matrixInput, NULL, /*isTo_applyPreFilters=*/false);
	assert(is_ok);
	/* const bool is_also_ok = cluster__hpLysis_api ( */
	/* 						     &obj_hp, e_hpLysis_clusterAlg_kCluster__medoid, obj_matrixInput, */
	/* 						     /\*nclusters=*\/ nclusters, /\*npass=*\/ arg_npass */
	/* 						     ); */
	assert(is_ok);
	//! ------------------------------------------
	//! 
	//! Compute CCMs:
	for(uint ccm_id = 0; ccm_id < e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef; ccm_id++) {	 
	  if(row_id == 0) { //! Set the row-header:
	    char str_local[3000]; sprintf(str_local, "%s", getString__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id));
	    set_string__s_kt_matrix(&mat_gold, ccm_id, str_local, /*addFor_column=*/true);
	  }
	  t_float CCM_score = scalar_CCMclusterSimilarity__s_hpLysis_api_t(&obj_hp, (e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)ccm_id, list_hypo, NULL);
	  if(CCM_score == T_FLOAT_MAX) {CCM_score = 0;}
	  mat_gold.matrix[row_id][ccm_id] = CCM_score;
	}

	//!
	//! De-allocate
	free__s_hpLysis_api_t(&obj_hp);
	row_id++;
      }
    }
    //!
    //! Epxoert the [ªbove] mappigns of randomness-gneerated scores:
    __Mi__export(&mat_gold, "result__hypothesis");
    free__s_kt_matrix(&mat_gold);
  }
  }

  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_ccm_1);
  free__s_kt_matrix(&mat_ccm_2);
  free__s_kt_matrix(&mat_time);
  free_1d_list_uint(&mapOf_goldCluster__truthIs__simMetric);
  free_1d_list_uint(&mapOf_goldCluster__truthIs__simMetric_category);
  free_1d_list_uint(&mapOf_goldCluster__truthIs__algorithm);
  free_1d_list_uint(&mapOf_goldCluster__truthIs__radnomness_in);
  free_1d_list_uint(&mapOf_goldCluster__truthIs__radnomness_out);
#undef __Mi__export
  return true;
}

