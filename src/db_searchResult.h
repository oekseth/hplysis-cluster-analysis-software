#ifndef db_searchResult_rel_h
#define db_searchResult_rel_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file 
   @brief a wrapper to our "db_searchResult_rel.h" (oekseth, 06. mar. 2017).
 **/

#include "db_searchNode_rel.h"

/**
   @struct s_db_searchResult_rel__config
   @brief A 'static' configuration used in 'basic sematnci searches' (oekseth, 06. mar. 2017)
   @remarks 
   -- simplifies future searhc-updates by 'merigng' muliple/different search-objects.
 **/
typedef struct s_db_searchResult_rel__config {
  const s_db_searchNode_rel_t *search_preSelectionCriteria;
  //bool search_preSelectionCriteria__isToUse;
  const s_db_searchNode_rel_t *search_inRecursive;
  uint threshold__scalar__cntVerticesToInvestigate__max; //! which if set is use das a 'stop-trheshodl' wrt. vertex-inveistgaitons.
  bool configLogics__isToApply__synonymMappings__inFunction; 
  //bool search_inRecursive__isToUse;
} s_db_searchResult_rel__config_t;

//! @return an intlised version of our "s_db_searchResult_rel__config_t" struct.
s_db_searchResult_rel__config_t setToEmpty__s_db_searchResult_rel__config_t();

#define __MS__ofInterest__s_db_searchResult_rel  T_FLOAT_MIN_ABS
//#define __MS__isVisited__s_db_searchResult_rel  T_FLOAT_MIN_ABS + 0.1
#define __MS__NotOfInterest__s_db_searchResult_rel T_FLOAT_MAX
#define MF__notOfInterest__s_db_searchResult_rel(self, index) ({assert(self); assert(index < self->mapOf_visistedVertices.list_size); const bool ret_val = (self->mapOf_visistedVertices.list[index] == __MS__NotOfInterest__s_db_searchResult_rel); { ret_val;}) //! ie, inveseigte if the vertex is Not alreayd 'marked' as of interest.
#define MF__notOfInterest__updateMarkIfNotAlready__s_db_searchResult_rel(self, index) ({assert(self); assert(index < self->mapOf_visistedVertices.list_size); const bool ret_val = (self->mapOf_visistedVertices.list[index] == __MS__NotOfInterest__s_db_searchResult_rel);  if(ret_val) {self->mapOf_visistedVertices.list[index] = __MS__ofInterest__s_db_searchResult_rel;} ret_val;}) //! ie, inveseigte if the vertex is Not alreayd 'marked' as of interest, and 'if not' then udpat ethe mark
#define MF__setAllVertices__toNotBeOfInterest(self) ({for(uint i = 0; i < self->mapOf_visistedVertices.list_size; i++) { self->mapOf_visistedVertices.list[i] = __MS__NotOfInterest__s_db_searchResult_rel;}})  //! ie, intiate to 'not known being of interest'.

//! @return true if "index" has a 'known' distance. 
#define MF__isVisited__s_db_searchResult_rel(self, index) ({assert(self); assert(index < self->mapOf_visistedVertices.list_size); const bool ret_val = ( (self->mapOf_visistedVertices.list[index] != __MS__ofInterest__s_db_searchResult_rel) && (self->mapOf_visistedVertices.list[index] != __MS__NotOfInterest__s_db_searchResult_rel) ); ret_val;}) //! ie, inveseigte if the vertex is of interest.


#define MF__getDistance__s_db_searchResult_rel(self, index) ({assert(self); assert(index < self->mapOf_visistedVertices.list_size); t_float dist = self->mapOf_visistedVertices.list[index]; if( (dist == __MS__ofInterest__s_db_searchResult_rel) || (dist == __MS__NotOfInterest__s_db_searchResult_rel) ) {dist = T_FLOAT_MAX;} dist;}) //! ie, return a 'standized' distnace-meausre.
#define MF__setDistance__s_db_searchResult_rel(self, index, new_dist) ({assert(self); assert(index < self->mapOf_visistedVertices.list_size); self->mapOf_visistedVertices.list[index] = new_dist;}) //! ie, update the distance.
/**
   @struct s_db_searchResult_rel
   @brief hold search sepcifciaotn/configuraiton and the result-object (oekseth, 06. mar. 2017)
   @remarks 
   -- used to simplify the spciciaotn hand 'handling' of 'nestedsed and basic sematnci-searches'.
   -- extends our "s_db_searchNode_rel" with result-data-strucutre
 **/
typedef struct s_db_searchResult_rel {
  s_db_searchResult_rel__config_t config;
  //! -----------------------------------------
  s_kt_list_1d_float_t mapOf_visistedVertices; //! PRovide infomraiton of [preFilter, isVisited, maxDist]. Hold the 'max-traverse-distance' from the netowrks 'search-ends'/leafs to a given node. Node: while we use "__MS__ofInterest__s_db_searchResult_rel" to 'denote' a vertex which is of interest, we use "__MS__NotOfInterest__s_db_searchResult_rel" to describe a 'vertex which is to e ingored'.
  uint result_cnt_nodesExpanded; //! ie, the total number of unqiue vertices which were expanded.
} s_db_searchResult_rel_t;
//! @returns an 'empty and itnlaied version' of our "s_db_searchResult_rel_t" struct (oekseth, 06. mar. 2017)
s_db_searchResult_rel_t setToEmpty__s_db_searchResult_rel_t();
//! @returns an 'itnlaied version' of our "s_db_searchResult_rel_t" struct (oekseth, 06. mar. 2017)
s_db_searchResult_rel_t init__s_db_searchResult_rel_t(const uint cnt_vertices, const s_db_searchNode_rel_t *search_preSelectionCriteria, const s_db_searchNode_rel_t *search_inRecursive);
//! De-allcoates our "s_db_searchResult_rel_t" structure (oekseht, 06. mar. 2017).
void free__s_db_searchResult_rel_t(s_db_searchResult_rel_t *self);

//! construct to new search-objects for different evlauation-test-cases (oekseth, 06. mar. 2017). 
//! @return the new-cosntructed object.
s_db_searchResult_rel_t buildSample__s_db_searchResult_rel_t(const uint cnt_vertices, const e_db_searchNode_rel__sampleUseCases_t enum_id, const s_kt_list_1d_uint_t *listOf_heads, const s_kt_list_1d_uint_t *listOf_predicates, const s_kt_list_1d_uint_t *listOf_tails,  const s_kt_list_1d_uint_t *listOf_preSelection_predictes,  const s_kt_list_1d_uint_t *listOf_preSelection_tails, s_db_searchNode_rel_t *retObj_filterBeforeRecursion, s_db_searchNode_rel_t *retObj_inRecursionTraversal);


#endif //! EOF
