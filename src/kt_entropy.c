#include "kt_entropy.h" 

//! @return the string-represetnation of the enum-id in queisont.
const char *get_stringOfEnum__e_kt_entropy_genericType_t(const e_kt_entropy_genericType_t enum_entropy) {
  if(__isCase(e_kt_entropy_genericType_ML_Shannon)) {return "ML";}
  if(__isCase(e_kt_entropy_genericType_MM_MillerMadow)) {return "MM";}
  if(__isCase(e_kt_entropy_genericType_CS_ChaoShen)) {return "CS";}
  if(__isCase(e_kt_entropy_genericType_Dirichlet_Jeffreys)) {return "Jeffreys";}
  if(__isCase(e_kt_entropy_genericType_Dirichlet_Laplace)) {return "Laplace";}
  if(__isCase(e_kt_entropy_genericType_Dirichlet_SG)) {return "SG";}
  if(__isCase(e_kt_entropy_genericType_Dirichlet_minimax)) {return "minMax";}
  if(__isCase(e_kt_entropy_genericType_shrink)) {return "shrink";}
  if(__isCase(e_kt_entropy_genericType_Simpson)) {return "Simpson";}
  if(__isCase(e_kt_entropy_genericType_Simpson_generic)) {return "SimpsonGeneric";}
  if(__isCase(e_kt_entropy_genericType_Gini)) {return "Gini";}
  if(__isCase(e_kt_entropy_genericType_Hulbert_permutation)) {return "Hulbert";}
  if(__isCase(e_kt_entropy_genericType_HCDT)) {return "HCDT";}
  if(__isCase(e_kt_entropy_genericType_Hill)) {return "Hill";}
  if(__isCase(e_kt_entropy_genericType_Renyi)) {return "Renyi";}
  if(__isCase(e_kt_entropy_genericType_PT_1)) {return "PT1";}
  if(__isCase(e_kt_entropy_genericType_PT_2)) {return "PT2";}
  if(__isCase(e_kt_entropy_genericType_AD)) {return "AD";}
  if(__isCase(e_kt_entropy_genericType_HHI)) {return "HHI";}
  //! ------------------
  if(__isCase(e_kt_entropy_genericType_undef)) {return "undef";}
  /* if(__isCase(e_kt_entropy_genericType_)) {return "";} */
  /* if(__isCase(e_kt_entropy_genericType_)) {return "";} */
  /* if(__isCase(e_kt_entropy_genericType_)) {return "";} */
  /* if(__isCase(e_kt_entropy_genericType_)) {return "";} */
  /* if(__isCase(e_kt_entropy_genericType_)) {return "";} */
  return NULL;
}

//! @return the string-represetnation of the enum-id in queisont.
e_kt_entropy_genericType_t get_enumForString__e_kt_entropy_genericType_t(const char *str_cmp) {
  assert(str_cmp); assert(strlen(str_cmp));
  for(uint enum_entropy = 0; enum_entropy < e_kt_entropy_genericType_undef; enum_entropy++) {
    const char *str_enum = get_stringOfEnum__e_kt_entropy_genericType_t((e_kt_entropy_genericType_t)enum_entropy);
    assert(str_enum); assert(strlen(str_enum));
    if(strlen(str_enum) == strlen(str_cmp)) {
      if(0 == strncmp(str_enum, str_cmp, strlen(str_cmp))) {return (e_kt_entropy_genericType_t)enum_entropy;}
    }
  }
  //!
  //! Thn the enum was Not found.
  return e_kt_entropy_genericType_undef;
}

