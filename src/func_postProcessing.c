{
  if(postProcessingNeedsTo_beEvaluated && (self->s_inlinePostProcess != NULL)) {
    // printf("Post-processing, at %s:%d\n", __FILE__, __LINE__);
    void *s_inlinePostProcess = self->s_inlinePostProcess;	
    assert(s_inlinePostProcess);
    if(self->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights) {
      s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_list_calculateWeights_t*)s_inlinePostProcess;
      const t_float cutoff =  objCaller_postProcessResult->cutoff;
      const t_float exponent =  objCaller_postProcessResult->exponent;
      assert(exponent != 0); //! ie, as the call will otehrsie be errnous.
      t_float *result = objCaller_postProcessResult->listOf_result_afterCorrelation; //! ie, the list to update.
      for(uint index2 = 0; index2 < nrows_adjusted; index2++) {
	const t_float distance = arrOf_tmpResult[index2];
	// FIXME: move [”elow] cofnigruaiton thethe caller ... and then update our 'non-improvec code'.
	const bool isTo_updateAlsoFor_symmetric_elements = false;
	macro__ktCorr__postProcess_afterCorrelation_for_value_weights_slow(distance, cutoff, exponent, index1, index2, result, isTo_updateAlsoFor_symmetric_elements);      
      }
    } else if(self->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_min) {	  
      s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)s_inlinePostProcess;
      for(uint index2 = 0; index2 < nrows_adjusted; index2++) {
	if(objCaller_postProcessResult->return_distance > arrOf_tmpResult[index2]) {
	  objCaller_postProcessResult->return_distance = arrOf_tmpResult[index2];
	  objCaller_postProcessResult->index2_best = index2;
	}
      } 
    } else if(self->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_max) {	  
      s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)s_inlinePostProcess;
      for(uint index2 = 0; index2 < nrows_adjusted; index2++) {
	if(objCaller_postProcessResult->return_distance < arrOf_tmpResult[index2]) {
	  objCaller_postProcessResult->return_distance = arrOf_tmpResult[index2];
	  objCaller_postProcessResult->index2_best = index2;
	}
      } 
    } else if(self->typeOf_postProcessing_afterCorrelation == e_allAgainstAll_SIMD_inlinePostProcess_scalar_value_sum) {	  
      s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t *objCaller_postProcessResult = (s_allAgainstAll_SIMD_inlinePostProcess_scalar_basic_t*)s_inlinePostProcess;
      for(uint index2 = 0; index2 < nrows_adjusted; index2++) {
	objCaller_postProcessResult->return_distance += arrOf_tmpResult[index2];
      } 
    } else {
      fprintf(stderr, "!!\t Add support for this option, ie, please forward a request to the developer [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      assert(false); //! ie, an heads-up in debug-mode.
    }
  }
}
