
//!
//! Build a transpsoed matrix, ie, to simplyf our comptaution-logics:
const t_float default_value_float = 0;
t_float **local_matrix = allocate_2d_list_float(self->nrows, self->ncols, default_value_float);
matrix__transpose__compute_transposedMatrix_float(self->nrows, self->ncols, self->matrix, local_matrix, /*isTo_useIntrisinistic=*/true);
//! -----------------------------
//! 
//! Cofnigure the operation:
const uint cnt_rows = self->ncols;       const uint cnt_cols = self->nrows;
char *local_mapOf_interesting = self->mapOf_interesting_cols;
#define __macro__setValueToEmpty(row_id, col_id) ({ self->matrix[col_id][row_id] = get_implicitMask();}) //! ie, then mark the amsk as 'not-of-itnerest': we assume that the 'col_id' and 'row:_id' asre inverted as we xpect hte matrix-diemsniosn to be transposed
//#define __macro__setValueToEmpty(row_id, col_id) ({ self->matrix[col_id][row_id] = get_implicitMask();}) //! ie, then mark the amsk as 'not-of-itnerest'
//! Apply the logics:
#include "kt_matrix_filter__func__eitherOr__stdPermutations.c"

      
//!
//! De-allocate locally reserved memory:
free_2d_list_float(&local_matrix, self->nrows);
