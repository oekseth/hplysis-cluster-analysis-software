#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "hp_ccm.h" //! which is used as an itnerface/API to comptue simlairty-metrics, an itnerface which 'access' both "e_kt_correlationFunction.h", "kt_distance.h" and our "kt_matrix_base.h"
#include "hp_clusterShapes.h" //! which is used as an itnerface/API to build sysnteitc cluster-cases.
#include "kt_list_1d.h"
/**
   @brief build a matrix of CCM-scores for simliar-shpaed data-sets (oekseth, 06. mar. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks idea is to compare the sensitivty to CCMs through the appliciaotn/use of a sturcutred appraoch for a gradualr increase in cluster-in-accruacies.
   @remarks general: use logics in our "hp_ccm" to comptue the CCM, while our "hp_clustershapes.h" provide lgoics to build a sysntitc data-set (eg, to simplfiy a strucutred appraoch for evlauaiton).
   @remarks in our 'example-configuation' we here examplify:
   -- fixed VS linear-increase clsuter-size: both wrt. the 'impact' of using a matrix 'with different cluster-chunk-sizes' and wrt. 'gruadal increase in cluster-memberships'
   -- max-score-seperation: differences in CCMs for dfiferetn "score_weak" aramters
   -- reverse-matrix-order: how the "isTo_useRverseOrder_inInit" influences the CCM-simliarty-assements.
   @remakrs API-usage: in this example we demosntrate the usage of:
   -- "hp_clustershapes": differnet onfiguraitons to 'build' different default example-clsuter-shapes;
   -- "kt_list_1d": how the '1d-warpper-object' may be used to 'fetch' mulile matrix-mbased CCMs;   
   -- "export__singleCall_firstTranspose__s_kt_matrix_t(..)": how our itnfrace supports/falites the 'use' of a 'tranpsoed epxort' wrt. a 'naive' tSV-result-format.
   @todo update our artilce-text (and assicated appendix-text) wrt. above results
**/
int main() 
#else
  static void apply_tut_ccm_11_compareMatrixBased(const char *filePrefix, const uint config_cnt_casesToEvaluate, const uint config_nrows_base, const uint config_clusterBase, const bool isTo_useRverseOrder_inInit, const bool isTo_setClusterSzei_toFixed_sizes, const t_float score_weak)
#endif
{
#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
    const char *filePrefix = "ccm_syntLinearEval";
    const uint config_cnt_casesToEvaluate = 100;
    const uint config_nrows_base = 10; const uint config_clusterBase = 5;  
    const bool isTo_useRverseOrder_inInit = false;
    const bool isTo_setClusterSzei_toFixed_sizes = true;
    const t_float score_weak = 1000;
#endif
  //! -----------------------------------
  //! 
  assert(config_nrows_base > config_clusterBase);
  const uint nrows = config_nrows_base*config_cnt_casesToEvaluate;  const uint cnt_clusters = config_clusterBase;
  //e_kt_matrix_cmpCluster_clusterDistance__cmpType_t ccm_enum = e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC;
  //! ----------------------------------------------------------------------------
  //! Start: configuration ------------------------------------------------------------------------------------

  s_hp_clusterShapes_t obj_shape;
  //e_hp_clusterShapes_scoreAssignment_t typeOf_scoreAssignment = e_hp_clusterShapes_scoreAssignment_minInside_maxOutside,
  if(isTo_useRverseOrder_inInit == false) {
    obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
  } else {
    obj_shape = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/score_weak, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_reverse, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
  }
  bool is_ok = false;
  if(isTo_setClusterSzei_toFixed_sizes) {
    is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
    assert(is_ok);
  } else {
    uint cnt_clusters_fixedToAdd = cnt_clusters / 5;
    cnt_clusters_fixedToAdd = macro_max(2, cnt_clusters_fixedToAdd);
    e_hp_clusterShapes_clusterAssignType_t enum_id = e_hp_clusterShapes_clusterAssignType_linearIncrease;
    is_ok = buildSampleSet__disjointSquares_caseMultipleCases__hp_clusterShapes(&obj_shape, nrows, cnt_clusters, cnt_clusters_fixedToAdd, enum_id, /*cnt_seperateCalls=*/1, false, true, 2);
    assert(is_ok);
  }
  //! 
  //! Wrap result-object into an s_kt_matrix_base object
  s_kt_matrix_base vec_1 = initAndReturn__notAllocate__s_kt_matrix_base_t(obj_shape.matrix.nrows, obj_shape.matrix.ncols, obj_shape.matrix.matrix);
  //! ----------------------------------------------------------------------
  //!
  //! Allocate result-matrix:
  s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(config_cnt_casesToEvaluate, e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
  
  //!
  //! Iterate through cases:
  for(uint case_id = 1; case_id <= config_cnt_casesToEvaluate; case_id++) {
    printf("case_id=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
    //!
    //! Build a vertex-clsuter-membership-set:
    s_hp_clusterShapes_t obj_shape_2 = init_andReturn__s_hp_clusterShapes_t(nrows, /*score_weak=*/100, /*score_strong=*/1, /*typeOf_vertexOrder=*/e_hp_clusterShapes_vertexOrder_linear, /*typeOf_scoreAssignment=*/e_hp_clusterShapes_scoreAssignment_minInside_maxOutside);
    const uint cnt_clusters = case_id*config_clusterBase;
    bool is_ok = buildSampleSet__disjointSquares__hp_clusterShapes(&obj_shape_2, nrows, cnt_clusters, /*cnt_clusterStartOffset=*/0);
    assert(is_ok);    
    //!
    //! Apply logics: compute the matrix-based CCMs:
    s_kt_list_1d_float obj_result = init__s_kt_list_1d_float_t(/*ncols=*/0); //e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef);
    const uint *map_clusterMembers = obj_shape_2.map_clusterMembers;
    is_ok = ccm__singleMatrix__completeSet__hp_ccm(&vec_1, map_clusterMembers, &obj_result); //! defined in our "hp_ccm.h"
    assert(is_ok);    
    assert(mat_result.ncols == obj_result.list_size);
    //!
    //! Insert results:
    for(uint i = 0; i < mat_result.ncols; i++) {
      mat_result.matrix[case_id-1][i] = obj_result.list[i];
    }
    { //! Set the string-headers:
      char str_local[3000]; sprintf(str_local, "case(%u)", case_id);
      set_string__s_kt_matrix(&mat_result, case_id-1, str_local, /*addFor_column=*/false);      
      if(case_id == 1) { //! Then we 'insert' for the columns
	for(uint i = 0; i < mat_result.ncols; i++) {
	  char str_local[3000]; sprintf(str_local, "%s", getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t((e_kt_matrix_cmpCluster_clusterDistance__cmpType_t)i));
	  set_string__s_kt_matrix(&mat_result, /*col_id=*/i, str_local, /*addFor_column=*/true);      
	}
      }
    }
    //!
    //! De-allocate:
    free__s_hp_clusterShapes_t(&obj_shape_2);
    free__s_kt_list_1d_float_t(&obj_result);
  }

  //! -------------------------------------------------------------------------
  //!
  //! Prepare data-export:
  /* char str_filePath[2000] = {'\0'};  */
  /* if(stringOf_resultDir && strlen(stringOf_resultDir)) { */
  /*   if(stringOf_resultDir[strlen(stringOf_resultDir)-1] == '/') { */
  /*     sprintf(str_filePath, "%s", stringOf_resultDir); */
  /*   } else {sprintf(str_filePath, "%s/", stringOf_resultDir);} */
  /* }   */
  const char *str_filePath = filePrefix;
  //! Specificy an 'itnernal' wrapper-matcro to export the reuslts:
#define __Mi__export(matrix, suffix) ({ \
    char str_conc[2000] = {'\0'}; sprintf(str_conc, "%s%s.tsv", str_filePath, suffix); \
    bool is_ok = export__singleCall__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); \
    memset(str_conc, '\0', 2000); sprintf(str_conc, "%s%s.transposed.tsv", str_filePath, suffix); \
    /*! Then export a tranpsoed 'verison': */ \
    is_ok = export__singleCall_firstTranspose__s_kt_matrix_t(matrix, str_conc, NULL); assert(is_ok); })
  //! -------------------------------------------------------------------------
  //!
  //! Write out the result-matrices, both 'as-is' and 'transposed':
  __Mi__export(&mat_result, "result__ccm");

  //!
  //! De-allocate:
  free__s_kt_matrix(&mat_result);
  free__s_hp_clusterShapes_t(&obj_shape);
  //!
  //! @return
#ifndef __M__calledInsideFunction
  return true;
#endif
}
