#include "correlation_rank_rowPair.h"

//! Set the row-pair object ot emptyDe-allcoat ethe row-pair object:
void s_correlation_rank_rowPair_setTo_empty(s_correlation_rank_rowPair_t *self) {
  assert(self);
  assert(self);
  self->arrOf_valuesSorted_1 = NULL; self->arrOf_valuesSorted_2 = NULL;
  self->arrOf_ranks_index1 = NULL;   self->arrOf_ranks_index2 = NULL;
  self->cnt_setValues_inRanks = 0;
}

//! Allocate the rank-data for teh row-pair.
//! @remarks Initiate row row-pari set of ranks:
void s_correlation_rank_rowPair_allocate(s_correlation_rank_rowPair_t *self, const uint ncols) {
  assert(self);   assert(ncols > 0); 
  const t_float default_value_float = 0;
  self->cnt_setValues_inRanks = 0;
  self->arrOf_valuesSorted_1 = allocate_1d_list_float(ncols, /*default-value=*/0);
  self->arrOf_valuesSorted_2 = allocate_1d_list_float(ncols, /*default-value=*/0);  
  self->arrOf_ranks_index1 = allocate_1d_list_float(ncols, /*default-value=*/0);
  self->arrOf_ranks_index2 = allocate_1d_list_float(ncols, /*default-value=*/0);  
}

//! De-allcoat ethe row-pair object:
void s_correlation_rank_rowPair_free(s_correlation_rank_rowPair_t *self) {
  assert(self);
  if(self->arrOf_valuesSorted_1) {free_1d_list_float(&self->arrOf_valuesSorted_1); self->arrOf_valuesSorted_1 = NULL;}
  if(self->arrOf_valuesSorted_2) {free_1d_list_float(&self->arrOf_valuesSorted_2); self->arrOf_valuesSorted_2 = NULL;}
  const bool both_refersToSame = (self->arrOf_ranks_index1 == self->arrOf_ranks_index2);
  if(self->arrOf_ranks_index1) {free_1d_list_float(&self->arrOf_ranks_index1); self->arrOf_ranks_index1 = NULL;  }
  if(!both_refersToSame && self->arrOf_ranks_index2) {free_1d_list_float(&self->arrOf_ranks_index2); self->arrOf_ranks_index2 = NULL;}
  self->cnt_setValues_inRanks = 0;
}

/**
   @brief comptue the ranks for a row-pair
   @remarks i shte main-funciton of this structure.
 **/
void s_correlation_rank_rowPair_compute_nonTransposed(s_correlation_rank_rowPair_t *self, const uint index1, const uint index2, const uint ncols, t_float **data1, t_float **data2, char **mask1, char **mask2, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, const bool needTo_useMask_evaluation) {
  assert(self);
  const t_float default_value_float = 0;   const uint default_value_uint = 0;

  //! A local allcoation of varialbes:
  t_float *list_compressedValues_1 = allocate_1d_list_float(ncols, /*default-value=*/default_value_float);
  t_float *list_compressedValues_2 = allocate_1d_list_float(ncols, /*default-value=*/default_value_float);
  
  assert(ncols > 0); //assert(result_array); assert(tmp_array); assert(data);
  self->cnt_setValues_inRanks = 0; //! ie, reset.
  //! Idnetify the interesting vertices:  
  if(needTo_useMask_evaluation == false) {
    // FIXME: update our time-benchmarks to evlauate the time-signifance of this 'optionn'.
    memcpy(list_compressedValues_1, data1[index1], sizeof(t_float)*ncols);
    memcpy(list_compressedValues_2, data1[index2], sizeof(t_float)*ncols);
    self->cnt_setValues_inRanks = ncols;
  } else {
    if(mask1 || mask2) {
      assert(mask1); assert(mask2); //! ie, as we then expect both to be set.      
      for(uint i = 0; i < ncols; i++) {
	if(mask1[index1][i] && mask2[index2][i]) {
	  list_compressedValues_1[i] = data1[index1][i];
	  list_compressedValues_2[i] = data2[index1][i];
	  //! 
	  self->cnt_setValues_inRanks++; //! ie, increment.
	} else {
	  list_compressedValues_1[i] = T_FLOAT_MAX;
	  list_compressedValues_2[i] = T_FLOAT_MAX;
	}
      }
    } else {
      for(uint i = 0; i < ncols; i++) {
	if(isOf_interest(data1[index1][i])
	   && isOf_interest(data2[index2][i])
	   ) {
	  
	  list_compressedValues_1[i] = data1[index1][i];
	  list_compressedValues_2[i] = data2[index2][i];
	  //! 
	  self->cnt_setValues_inRanks++; //! ie, increment.
	} else {
	  list_compressedValues_1[i] = T_FLOAT_MAX;
	  list_compressedValues_2[i] = T_FLOAT_MAX;
	}
      }
    }
  }  
  if(self->cnt_setValues_inRanks > 0) {
    //t_float *matrix_tmp_1_vector = allocate_1d_list_float(ncols, /*default-value=*/default_value_float);
    uint *matrix_tmp_1_vector = allocate_1d_list_uint(ncols, /*default-value=*/default_value_uint);
    { 
      //! Comptue fior the first index:
      t_float *list_column = self->arrOf_ranks_index1;
      assert(list_column);
      //! Compute the rank:    
      getrank_memoryRe_use(self->cnt_setValues_inRanks, /*values=*/list_compressedValues_1, /*ranks=*/list_column, /*tmp-index-array=*/matrix_tmp_1_vector, typeOf_performance_sortAppraoch_toUse, self->arrOf_valuesSorted_1);
    }
    { 
      //! Comptue fior the second index:
      t_float *list_column = self->arrOf_ranks_index2;
      assert(list_column);
      //! Reset the data:
      memset_local(matrix_tmp_1_vector, default_value_float, ncols); //! a macro expected to be found in oru "common/def_memAlloc.h"
      //! Compute the rank:    
      getrank_memoryRe_use(self->cnt_setValues_inRanks, /*values=*/list_compressedValues_2, /*ranks=*/list_column, /*tmp-index-array=*/matrix_tmp_1_vector, typeOf_performance_sortAppraoch_toUse, self->arrOf_valuesSorted_2);
    }
    //! De-allcoat the locally reserved data-strucutres:
    free_1d_list_uint(&matrix_tmp_1_vector);
    //free_1d_list_float(&matrix_tmp_1_vector);
  }


  //! De-allcoat the locally reserved data-strucutres:
  free_1d_list_float(&list_compressedValues_1);  
  free_1d_list_float(&list_compressedValues_2);  
}


/**
   @brief comptue the ranks for a row-pair for a tansposed matrix.
   @remarks i shte main-funciton of this structure.
 **/
void s_correlation_rank_rowPair_compute_transposed(s_correlation_rank_rowPair_t *self, const uint index1, const uint index2, const uint nrows, t_float **data1, t_float **data2, char **mask1, char **mask2, const e_distance_rank_typeOf_computation_t typeOf_performance_sortAppraoch_toUse, const bool needTo_useMask_evaluation) {
  assert(self);
  const t_float default_value_float = 0;   const uint default_value_uint = 0;

  //! A local allcoation of varialbes:
  t_float *list_compressedValues_1 = allocate_1d_list_float(nrows, /*default-value=*/default_value_float);
  t_float *list_compressedValues_2 = allocate_1d_list_float(nrows, /*default-value=*/default_value_float);
  
  //assert(ncols > 0); //assert(result_array); assert(tmp_array); assert(data);
  assert(nrows > 0);
  self->cnt_setValues_inRanks = 0; //! ie, reset.
  //! Idnetify the interesting vertices:  
  if(needTo_useMask_evaluation == false) {
    for(uint row_id = 0; row_id < nrows; row_id++) {
      list_compressedValues_1[row_id] = data1[row_id][index1];
      list_compressedValues_2[row_id] = data2[row_id][index2];
    }
    // FIXME: update our time-benchmarks to evlauate the time-signifance of this 'optionn'.
    self->cnt_setValues_inRanks = nrows;
  } else {
    if(mask1 || mask2) {
      assert(mask1); assert(mask2); //! ie, as we then expect both to be set.      
      for(uint i = 0; i < nrows; i++) {
	if(mask1[i][index1] && mask2[i][index2]) {
	  list_compressedValues_1[i] = data1[i][index1];
	  list_compressedValues_2[i] = data2[i][index1];
	  //! 
	  self->cnt_setValues_inRanks++; //! ie, increment.
	} else {
	  list_compressedValues_1[i] = T_FLOAT_MAX;
	  list_compressedValues_2[i] = T_FLOAT_MAX;
	}
      }
    } else {
      for(uint i = 0; i < nrows; i++) {
	if(isOf_interest(data1[i][index1])
	   && isOf_interest(data2[i][index2])
	   ) {
	  
	  list_compressedValues_1[i] = data1[i][index1];
	  list_compressedValues_2[i] = data2[i][index2];
	  //! 
	  self->cnt_setValues_inRanks++; //! ie, increment.
	} else {
	  list_compressedValues_1[i] = T_FLOAT_MAX;
	  list_compressedValues_2[i] = T_FLOAT_MAX;
	}
      }
    }
  }  
  if(self->cnt_setValues_inRanks > 0) {
    //t_float *matrix_tmp_1_vector = allocate_1d_list_float(ncols, /*default-value=*/default_value_float);
    uint *matrix_tmp_1_vector = allocate_1d_list_uint(nrows, /*default-value=*/default_value_uint);
    { 
      //! Comptue fior the first index:
      t_float *list_column = self->arrOf_ranks_index1;
      assert(list_column);
      //! Compute the rank:    
      getrank_memoryRe_use(self->cnt_setValues_inRanks, /*values=*/list_compressedValues_1, /*ranks=*/list_column, /*tmp-index-array=*/matrix_tmp_1_vector, typeOf_performance_sortAppraoch_toUse, self->arrOf_valuesSorted_1);
    }
    { 
      //! Comptue fior the second index:
      t_float *list_column = self->arrOf_ranks_index2;
      assert(list_column);
      //! Reset the data:
      memset_local(matrix_tmp_1_vector, default_value_float, nrows); //! a macro expected to be found in oru "common/def_memAlloc.h"
      //! Compute the rank:    
      getrank_memoryRe_use(self->cnt_setValues_inRanks, /*values=*/list_compressedValues_2, /*ranks=*/list_column, /*tmp-index-array=*/matrix_tmp_1_vector, typeOf_performance_sortAppraoch_toUse, self->arrOf_valuesSorted_2);
    }
    //! De-allcoat the locally reserved data-strucutres:
    free_1d_list_uint(&matrix_tmp_1_vector);
    //free_1d_list_float(&matrix_tmp_1_vector);
  }


  //! De-allcoat the locally reserved data-strucutres:
  free_1d_list_float(&list_compressedValues_1);  
  free_1d_list_float(&list_compressedValues_2);  
}
