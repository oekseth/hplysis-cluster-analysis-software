/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

#include "measure_base.h"
#include "correlation_macros__distanceMeasures.h"



//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
    sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}



int main() {

  assert(false); // FIXME: write performance-test wr.t oupmizaitons found in our "correlation_macros__distanceMeasures.h"! wrt. ... 'pertmatuions' of the "metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide(..)" macro .... and wrt. the altter update our 'correctness-test-file' to try proviking cases where the "metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(..)" cofngiruation is ernrously set for some of the distnace-macros.

  assert(false); // FIXME: ensure taht our funciotns  evlauate/comapre ... the "pow" function ... eg, wrt. "VECTOR_FLOAT_fastpow_0_1" ... comapre with the 'naive' pow-computation. ... where the 'cost-heavy' "math.h" fucntiosn are tested and evalauted wrt. alternative optization-strategies.

  assert(false) // FIXME: compare wrt. our  "correlation_api_rowCompare.cxx" and our "distance_2rows_slow.c" and oru "correlationType_proximity_altAlgortihmComputation.cxx"

  assert(false); // FIXME: add seomthing.

  assert(false); // 
  assert(false); //  <-- write a eprfomrance-test where we compareo "pow(value, 2)" and "value * value" <-- consider to update oru to-be-written macro-dsitance-metric-performance-comparison-and-corretness-test wrt. 'this evlauation'
  assert(false); //      <-- bonus: for "Minkowski" ... write a seperate perfomrance-test where we implement using "pow" (ie, a non-fixed nubmer of pows'.
  assert(false); //      <-- compre with other 'flavours' of our implementaitons .... eg, a 2d-loop to compute an all-against-all-version of "correlationType_proximity_altAlgortihmComputation.cxx" ... and similary for our 'seperte euclid and cityblock-implemetnation'.


  assert(false); // FIXME: comapre our "oekseth_forMasks_std_pow2" and our "matrix_deviation.c" with the 'anvie' deviaotn-algorithm implemented in "ALGLIB" <-- consider udpating our "matrix_deviaiotn.c" with a to-be-wrtiten 1d-step-algorithm for deviaotn

  assert(false); // FIXME: ... comapre teh exeuciton-time-effect of 're-ordering- the muliplcaiton 'calls', eg, for the [”elow] extract from "correlation(..)" in "cluster.c"
  //! ------------start: extract.
      /* { t_float term1 = data1[index1][i]; */
      /*   t_float term2 = data2[index2][i]; */
      /*   t_float w = weight[i]; */
      /*   sum1 += w*term1; */
      /*   sum2 += w*term2; */
      /*   result += w*term1*term2; */
      /*   denom1 += w*term1*term1; */
      /*   denom2 += w*term2*term2; */
      /*   tweight += w; */
      /* } */
  //! ------------end: extract.


  assert(false); // FIXME: ... 

}
