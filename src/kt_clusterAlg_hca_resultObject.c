#include "kt_clusterAlg_hca_resultObject.h"
#include "kt_set_1dsparse.h"

//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
 Node_t *allocate_1d_list_Node(const uint size, const Node_t default_value) {
#ifdef __cplusplus
  Node_t *tmp = new Node_t[size];
#else 
  Node_t *tmp = (Node_t*)malloc(sizeof(Node_t)*size);
#endif
  //! Iteate through the lsit and set the vlaeus to empty:
  assert(size > 0);
  for(uint i = 0; i < size; i++) {
    tmp[i].left = UINT_MAX;
    tmp[i].right = UINT_MAX;
  }

    
  //memset(tmp, default_value, size*sizeof(Node));
  return tmp;
}
//! Ntoe: this proceudre is sued to simplify the 'swapping' between C++ code an C code.
void free_1d_list_Node(Node_t **list) {  
  assert(list);
  Node_t *tmp = *list;
#ifdef __cplusplus
  delete [] tmp;
#else 
  free(tmp);
#endif
}

//! Intiate the s_kt_clusterAlg_hca_resultObject_t object
s_kt_clusterAlg_hca_resultObject_t setToEmptyAndreturn__s_kt_clusterAlg_hca_resultObject_t() {
  s_kt_clusterAlg_hca_resultObject_t self;
  self.mapOf_nodes = NULL;
  self.nrows = 0;
  //! @return
  return self;
}

//! Intiate the s_kt_clusterAlg_hca_resultObject_t object
void setTo_empty__s_kt_clusterAlg_hca_resultObject_t(s_kt_clusterAlg_hca_resultObject_t *self, const uint nrows) {
  assert(self);
  self->mapOf_nodes = NULL;
  self->nrows = nrows;
  if(nrows > 0) {
    Node_t empty_node = initAndReturn_Node_t(/*left=*/INT_MAX, INT_MAX, T_FLOAT_MAX); //(int)row_id,/*right=*/(int)col_id,/*distance=*/obj_1->matrix[row_id][col_id]);
    self->mapOf_nodes = allocate_1d_list_Node(nrows, empty_node);
    assert(self->mapOf_nodes);
  }
}
//! De-allcoates the s_kt_clusterAlg_hca_resultObject_t object
void free__s_kt_clusterAlg_hca_resultObject_t(s_kt_clusterAlg_hca_resultObject_t *self) {
  assert(self);
  if(self->mapOf_nodes != NULL) {
    free_1d_list_Node(&(self->mapOf_nodes)); self->mapOf_nodes = NULL;
  }
  setTo_empty__s_kt_clusterAlg_hca_resultObject_t(self, 0);
}

//! translates a 1d-list into a dense 2d-list of relationships (oekseth, 06. feb. 2017); @return a new-intiated object ;
s_kt_clusterAlg_hca_resultObject_2d_t build_2dStruct__s_kt_clusterAlg_hca_resultObject_2d_t(const s_kt_clusterAlg_hca_resultObject_t *obj_hca) {

  assert(obj_hca);
  assert(obj_hca->mapOf_nodes);  
  //!
  //! Indetify the max-imginary-node:
  int min_node_id = INT_MAX;       int max_node_id = 0;
  for(uint node_id_local = 0; node_id_local < obj_hca->nrows; node_id_local++) {
    const Node_t node = obj_hca->mapOf_nodes[node_id_local];
    min_node_id = macro_min(min_node_id, node.right);
    min_node_id = macro_min(min_node_id, node.left);
    if(node.left != INT_MAX) {
      max_node_id = macro_max(max_node_id, node.left);
    }
    if(node.right != INT_MAX) {
      max_node_id = macro_max(max_node_id, node.right);
    }
    //printf("node.right=%d, max_node_id=%d, at %s:%d\n", node.right, max_node_id, __FILE__, __LINE__);
  }
  if(min_node_id > 0) {
    fprintf(stderr, "!!\t No vertices were set in your input-object: observes case=(min_node_id > 0). If this is nto what you expected, then please investigate. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return initEmpty__s_kt_clusterAlg_hca_resultObject_2d_t();
  }
  assert(min_node_id <= 0); assert(min_node_id <= max_node_id);
  if(max_node_id == min_node_id) {
    fprintf(stderr, "!!\t No vertices were set in your input-object: please investigate. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return initEmpty__s_kt_clusterAlg_hca_resultObject_2d_t();
  }
  //!
  //! 
  //! Intiates the object:
  s_kt_clusterAlg_hca_resultObject_2d_t self = initEmpty__s_kt_clusterAlg_hca_resultObject_2d_t();
  self.offsetToRemove__internalToRealLife = (uint)(-1*min_node_id);
  self.mapOf_internalStartNodes_size = self.offsetToRemove__internalToRealLife + (uint)max_node_id + 1;
  const uint empty_0 = 0;
  self.mapOf_internalStartNodes = allocate_1d_list_uint(self.mapOf_internalStartNodes_size, empty_0);
  allocate__s_kt_set_2dsparse_t(&(self.stack2d), /*nrows=*/self.mapOf_internalStartNodes_size, /*ncols-init=*/20, /*matrix=*/NULL, NULL, false, /*isTo_allocateFor_scores=*/true);
  //! 
  //! 
  //! ----------------------------------------------------
  //!
  //! Insert entites:
  for(uint node_id_local = 0; node_id_local < obj_hca->nrows; node_id_local++) {
    const Node_t node = obj_hca->mapOf_nodes[node_id_local];
    if(node.left == node.right) {continue;} //! ie, to avoid isnerting a self-cyclic loop.
    if( (node.right != INT_MAX) && (node.left != INT_MAX) ) {
      //printf("node.right=%d, max_node_id=%d, at %s:%d\n", node.right, max_node_id, __FILE__, __LINE__);
      assert(node.right <= (int)max_node_id);
      const int local_right = node.right + (int)self.offsetToRemove__internalToRealLife;
      const int local_left  = node.left + (int)self.offsetToRemove__internalToRealLife;
      assert(local_right < self.mapOf_internalStartNodes_size);
      assert(local_left < self.mapOf_internalStartNodes_size);
      //!
      //! Update the counter wrt. the 'incoming nodes':
      self.mapOf_internalStartNodes[local_right]++; // FIXME: why doe we Not alos updae for "local_left"? (oekseeth, 06. feb. 2018).
      //!
      //! Add the left-->right relationship:
      // printf("\t (append-to-list)\t %d(%u)-->%d(%u), w/score=%f, at %s:%d\n", node.left, local_left, node.right, local_right, node.distance, __FILE__, __LINE__);
      push__idScorePair__s_kt_set_2dsparse_t(&(self.stack2d), local_left, local_right, node.distance);
    }  
  }
  //! ---------------------------------------------------------------------------------------
  //!  
  //!  
  //! @return
  return self;
}
//! @return a new-intalized stack of start-nodes wrt. teh self object (oekseth, 06. feb. 2017).
s_kt_set_1dsparse_t constructStackOf_startNodes__s_kt_clusterAlg_hca_resultObject_2d_t(const s_kt_clusterAlg_hca_resultObject_2d_t *self) {
  assert(self); assert(self->mapOf_internalStartNodes);
  assert(self->mapOf_internalStartNodes_size > 0);
  s_kt_set_1dsparse_t obj_stack; allocate__simple__s_kt_set_1dsparse_t(&obj_stack, self->mapOf_internalStartNodes_size);
  uint cnt_added = 0;
  //! Traverse the data-structure, 'settting' the root-node(s):
  for(uint i = 0; i < self->mapOf_internalStartNodes_size; i++) {
    const uint cnt_incominb = self->mapOf_internalStartNodes[i];
    assert(cnt_incominb != UINT_MAX);
    // printf("\t(consider::add-start-node)\t '%u' w/cnt=%u, at %s:%d\n", i, cnt_incominb, __FILE__, __LINE__);
    if(cnt_incominb == 0) {
      //printf("\t\t(add-start-node)\t '%u' w/cnt=%u, at %s:%d\n", i, cnt_incominb, __FILE__, __LINE__);
      push__s_kt_set_1dsparse_t(&obj_stack, /*vertex=*/i);
      cnt_added++;
    }
  }
  if(cnt_added == 0) {
    fprintf(stderr, "!!\t Seems like you have a cyclic network (or alterantivly that the 'tree used for input' cosnists of isolated nodes), ie, please investigate your input: if this warning seems odd, then pelase contact the senior devleoper at [oesketh@gmial.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
  //! @return
  return obj_stack;
}


/**
   @brief merge the self object with the clsuter-results in clusterResults
   @param <self> is the object to update
   @param <clusterResults> is the clsuter-object (holding the clsuter-results for the clsuter-algoritm in question).
   @param <keyMap_localToGlobal> optional: if set then we asusme that the vertices 'are in a local scope which are to be mapped to the global scope'.
   @remarks an example-sueage of the "keyMap_localToGlobal" cosnerns the case where we first 'compress' the clsuter-evlauation using disjotint-forest-algrotims, and thereafter apply k-means clsutering before merging the results 'into one unifed data-structure'
 **/
void mergeResult__s_kt_clusterAlg_hca_resultObject_t(s_kt_clusterAlg_hca_resultObject_t *self, const s_kt_clusterAlg_hca_resultObject *clusterResults, const uint *keyMap_localToGlobal) {
  assert(self); assert(clusterResults);
  assert(self->nrows >= clusterResults->nrows);
  assert(self->mapOf_nodes); assert(clusterResults->mapOf_nodes);
  assert(keyMap_localToGlobal);
  for(uint key_local = 0; key_local < clusterResults->nrows; key_local++) {
    const uint key = keyMap_localToGlobal[key_local];
    const Node_t node_old = clusterResults->mapOf_nodes[key_local];
    //! We expect the ndoe to Not be set 'prior' to this call:
    const Node_t node_new = self->mapOf_nodes[key];
    assert(node_new.left == UINT_MAX);
    assert(node_new.right == UINT_MAX);
    //! Udpat ehte lcoal mapping-proeprties:
    if(node_old.left != UINT_MAX) {self->mapOf_nodes[key].left = keyMap_localToGlobal[node_old.left];}
    if(node_old.right != UINT_MAX) {self->mapOf_nodes[key].right = keyMap_localToGlobal[node_old.right];}
  }
}

/**
   @brief construct an adjcency-matrix based on the Node_t arrOf_nodes table (oekseth, 06. nov. 2016).
   @param <self> the object which 'hold the data' to sue
   @param <matrix_result> an itnaited adjcency-matrix: if the matrix is first used in this call then we expec all cells to have been set to '0'; expected to be an djcency-matrix of diemsions "|nrows|x|nrows|".
   @param <mapOf_1d_distanceToRoot_forMetric__nodeCount> which we update to 'hold' the number of 'node-traversals' from teh root-node; simliar to "matrix_result" the objhect is expected to be pre-allocated to the nrwos size;
   @param <mapOf_1d_distanceToRoot_forMetric__linkScoreSum> which we update to 'hold' the 'sum of link-scores from the root-node' from teh root-node; simliar to "matrix_result" the objhect is expected to be pre-allocated to the nrwos size
   @param <nrows> which is expected to be equal or greater than the arrOf_nodes_size: if the valeus differet we expect the optional mapOf_localIdTo_globalId to be used.
   @param <mapOf_localIdTo_globalId> optional:  if (mapOf_localIdTo_globalId != NULL) we assume mapOf_localIdTo_globalId[node_id] = global_index, where global_index < nrows, and where we update matrix_result[global_index_head][global_index_tail] = simliarty.
   @return true upon success.
   @remarks the "self" object is expected to be the result of a call to an HCA algorithm. The object is (among others) expected to hold arrOf_nodes (ie, the tree of nodes, a tree which may be constructed from a call to our "treecluster__extensiveInputParams(..)" function) and the arrOf_nodes_size (ie, the number of nodes in arrOf_nodes).
 **/
//->mapOf_nodes, clusterResults->nrows
bool constructAdjcencencyMatrix_fromNodeTable__kt_clusterAlg_hca(const s_kt_clusterAlg_hca_resultObject_t *self, t_float **matrix_result, t_float  *mapOf_1d_distanceToRoot_forMetric__nodeCount, t_float *mapOf_1d_distanceToRoot_forMetric__linkScoreSum, const uint nrows, const uint *mapOf_localIdTo_globalId, const bool isTo_addInternalNodesIn__mapAttributeSet) {
  assert(self);
  const Node_t *arrOf_nodes = self->mapOf_nodes; const uint arrOf_nodes_size = self->nrows;
  assert(arrOf_nodes);
  assert(arrOf_nodes_size);
  if(!arrOf_nodes || !arrOf_nodes_size) {return false;} //! ie, as the inptu was not as expected
  // printf("------------, at %s:%d\n", __FILE__, __LINE__);

  //! **************************************************'
  //!
  if(matrix_result) {//! Apply the lgocis, ie, update the adjcency-matrix:
    //! -----------------------
    //!
    //! Validate the cosnsitency of the input-params:
    if(
       (!matrix_result || nrows) ||
       (matrix_result || !nrows)
       ) { //! then we have an in-consistnecy:
      fprintf(stderr, "!!\t Your funciton-input was not as expected, given matrix_result=%p and nrows=%u: please update yopur input-matrix with an intlaised result-matrix. For questions please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", matrix_result, nrows, __FUNCTION__, __FILE__, __LINE__);
      return false; //! ie, as there is no point in contiuing.
    } else {
      if(!matrix_result) {assert(nrows == UINT_MAX);} //! ie, as we then expect the result-set to Not be udpated wr.t the matrix
    }
    if(nrows < arrOf_nodes_size) {
      fprintf(stderr, "!!\t Your funciton-input was not as expected: while we expected arrOf_nodes_size=%u <= nrows=%u this is Not the case: please update yopur input-matrix with an intlaised result-matrix. For questions please cotnact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", arrOf_nodes_size, nrows, __FUNCTION__, __FILE__, __LINE__);
      return false; //! ie, as there is no point in contiuing.
    } 

    //printf("------------, at %s:%d\n", __FILE__, __LINE__);

    for (int i = arrOf_nodes_size; i >= 0; i--) {
      const int row_id_local = arrOf_nodes[i].left;
      const int col_id_local = arrOf_nodes[i].right;
      assert(row_id_local != INT_MAX);
      assert(col_id_local != INT_MAX);
      if( (col_id_local >= 0) && (row_id_local >= 0) ) {
	assert(col_id_local < arrOf_nodes_size);
	const int row_id = (mapOf_localIdTo_globalId) ? mapOf_localIdTo_globalId[row_id_local] : (uint)row_id_local;
	const int col_id = (mapOf_localIdTo_globalId) ? mapOf_localIdTo_globalId[col_id_local] : (uint)col_id_local;
	//! Update:
	matrix_result[row_id][col_id] = arrOf_nodes[i].distance;
	matrix_result[col_id][row_id] = arrOf_nodes[i].distance; //! ie, a symmetric update.
	//printf("%d\t to \t %d \t # at %s:%d\n", row_id, col_id, __FILE__, __LINE__);
      } //! else we asusem the column-id describs an internal-imaginary-vertex.
    }

    /* for(uint node_id_local = 0; node_id_local < arrOf_nodes_size; node_id_local++) { */
    /*   const uint row_id = (mapOf_localIdTo_globalId) ? mapOf_localIdTo_globalId[node_id_local] : node_id_local; */
    /*   assert(row_id < nrows); */
    /*   {//! Update for right: */
    /* 	const int col_id_local = arrOf_nodes[node_id_local].right; */
    /* 	assert(col_id_local != INT_MAX); */
    /* 	if(col_id_local >= 0) { */
    /* 	  assert(col_id_local < arrOf_nodes_size); */
    /* 	  const int col_id = (mapOf_localIdTo_globalId) ? mapOf_localIdTo_globalId[col_id_local] : (uint)col_id_local; */
    /* 	  assert(col_id < nrows); */
    /* 	  //! Update: */
    /* 	  matrix_result[row_id][col_id] = arrOf_nodes[node_id_local].distance; */
    /* 	} //! else we asusem the column-id describs an internal-imaginary-vertex. */
    /*   } */
    /*   {//! Update for left: */
    /* 	const int col_id_local = arrOf_nodes[node_id_local].left; */
    /* 	assert(col_id_local != INT_MAX); */
    /* 	assert(col_id_local < arrOf_nodes_size); */
    /* 	if(col_id_local >= 0) { */
    /* 	  const uint col_id = (mapOf_localIdTo_globalId) ? mapOf_localIdTo_globalId[col_id_local] : (uint)col_id_local; */
    /* 	  assert(col_id < nrows);	   */
    /* 	  //! Update: */
    /* 	  matrix_result[row_id][col_id] = arrOf_nodes[node_id_local].distance; */
    /* 	} //! else we asusem the column-id describs an internal-imaginary-vertex. */
    /*   } */
    /* } */
  }


  //! **************************************************'
  //! -----------------------
  //!
  //! Find the root-node(s):
  s_kt_clusterAlg_hca_resultObject_2d_t hca_2dStack = build_2dStruct__s_kt_clusterAlg_hca_resultObject_2d_t(self);
  if(hca_2dStack.mapOf_internalStartNodes_size == 0) { //
    fprintf(stdout, "(info)\t In the HCA-post-ops, was not able to idneityf root-nodes in the predicted network: could be due to (a) your network is cyclic; alternativly the network cosnists of 'isoalated non-connected nodes' (a case soemthimes occuring for HCA-dataset). If latter result is Not as expected, then please cotnact senior devleoper [oeksethh@gmail.com]. Observiaotni at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  } else 
  //!
  { //! Compute the node-distances:
    assert(mapOf_1d_distanceToRoot_forMetric__nodeCount);
    assert(mapOf_1d_distanceToRoot_forMetric__linkScoreSum);
    //! Initiate the array
    s_kt_set_1dsparse_t obj_stack = constructStackOf_startNodes__s_kt_clusterAlg_hca_resultObject_2d_t(&hca_2dStack);
    //! -----------------------
    //!
    //! -----------------------
    //!
    { //! Traverse the data-set, and update the 'glboal' data-setructure:
      bool has_data = true; uint cnt_evaluated = 0;
      //const uint __scoreIsVisitee = UINT_MAX - 2;
      //assert(mapOf_cntChildsInParents_size < __scoreIsVisitee);
      //!
      //! Allocat eha 'boolean lsit' to ensure taht a vertex is visited only once, ie, as a 'guanrtee' for clyclid relationships.
      const char empty_0 = 0;
      uint *__mapIsVisited = allocate_1d_list_uint(hca_2dStack.mapOf_internalStartNodes_size, empty_0);

      do {
	t_float scalar_dummy = T_FLOAT_MAX;
	const uint node_id_local = pop__s_kt_set_1dsparse_t(&obj_stack, &scalar_dummy);
	if(node_id_local != UINT_MAX) { //! then 'the tiem' is Not the last item.
	  cnt_evaluated++;
	  //!
	  //! Investigate if 'this' is the 'last time' a vertex 'adds this':
	  assert(__mapIsVisited[node_id_local] != UINT_MAX);
	  if(__mapIsVisited[node_id_local] != UINT_MAX) { __mapIsVisited[node_id_local]++; }
	  if( (__mapIsVisited[node_id_local] != UINT_MAX) && (__mapIsVisited[node_id_local] >= hca_2dStack.mapOf_internalStartNodes[node_id_local]) ) {
	    //!
	    //! Then update the stack:
	    uint list_toAdd_size = 0;
	    const uint *list_toAdd = get_sparseRow_ofIds__s_kt_set_2dsparse_t(&(hca_2dStack.stack2d), node_id_local, &list_toAdd_size);
	    //assert(node_id_local >= hca_2dStack.offsetToRemove__internalToRealLife);
	    // printf("vertex[%u = %d] has cnt-children=%u, at %s:%d\n", node_id_local, node_id_local - (int)hca_2dStack.offsetToRemove__internalToRealLife, list_toAdd_size, __FILE__, __LINE__);
	    if(list_toAdd && list_toAdd_size) {	      
	      assert(list_toAdd_size > 0);
	      uint listOf_scores_size = 0;
	      const t_float *listOf_scores = get_sparseRow_ofScores__s_kt_set_2dsparse_t(&(hca_2dStack.stack2d), node_id_local, &listOf_scores_size);
	      assert(listOf_scores); assert(listOf_scores_size == list_toAdd_size);
	      for(uint k = 0; k  < list_toAdd_size; k++) {
		const uint child_index = list_toAdd[k];
		assert(child_index < hca_2dStack.mapOf_internalStartNodes_size);
		// FIXME[web]: is it correct to 'omit' the imaignary nodes <-- cnosider/suggest to udpate our web-itnerface wr.t 'such' an dhten udpate [”elow] (oekseth, 06. feb. 2017).
		{ //! Increment the sum using the 'knowledge from teh parent':
		  //! Note: in [”elow] we itnroduce/supports/handles a compelxtiy (which is stringcly speaking not neccesary for HCA): to supprot the case where there are muliple 'shortest paths' in an heirhariccal tree, a 'support' which is included in order to support 'future needs'.		
		  const bool isA_realNode = ((child_index >= hca_2dStack.offsetToRemove__internalToRealLife) 
					     && (node_id_local >= hca_2dStack.offsetToRemove__internalToRealLife) //! ie, where >=" is used to 'handle' the '0-index-case'.
					     );
		  if(  isTo_addInternalNodesIn__mapAttributeSet || isA_realNode ) {
		    int this_id_xmtImgAdjustment   = (int)node_id_local - (int)hca_2dStack.offsetToRemove__internalToRealLife;
		    int child_id_xmtImgAdjustment  = (int)child_index - (int)hca_2dStack.offsetToRemove__internalToRealLife;		    
		    
		    if(isA_realNode) {
		      assert(this_id_xmtImgAdjustment < (int)self->nrows);
		      assert(child_id_xmtImgAdjustment < (int)self->nrows);
		      assert(this_id_xmtImgAdjustment >= 0);
		      assert(child_id_xmtImgAdjustment >= 0);
		    } else {
		      //! Adjsut the score, using "nrows" as a referecnce:
		      const int offset = (int)self->nrows - 1; //! where '-1' is used as we assume the first imaignary idnex starts at '-1'.
		      if(this_id_xmtImgAdjustment < 0) {
			this_id_xmtImgAdjustment *= -1;  this_id_xmtImgAdjustment += offset;
		      } else {
			assert(this_id_xmtImgAdjustment >= 0);
		      }
		      if(child_id_xmtImgAdjustment < 0) {
			child_id_xmtImgAdjustment *= -1;  child_id_xmtImgAdjustment += offset;
		      } else {
		      assert(child_id_xmtImgAdjustment >= 0);
		      }
		    }
		    {
		      const t_float this_distance = mapOf_1d_distanceToRoot_forMetric__nodeCount[this_id_xmtImgAdjustment] + 1.0;
		      const t_float score_prev = mapOf_1d_distanceToRoot_forMetric__nodeCount[child_id_xmtImgAdjustment];
		      if(score_prev != 0) {
			mapOf_1d_distanceToRoot_forMetric__nodeCount[child_id_xmtImgAdjustment] = macro_min(this_distance, score_prev);
		      } else {
			mapOf_1d_distanceToRoot_forMetric__nodeCount[child_id_xmtImgAdjustment] = this_distance;
		      }
		    }
		    {		      
		      const t_float score_toChild = listOf_scores[k];
		      // assert(score_toChild != T_FLOAT_MAX); //! ei, as it would be odd 'if the distance in/for a known relationship is Not known'.	      
		      if(score_toChild != T_FLOAT_MAX) {
			const t_float thisToChild_distance_sum = mapOf_1d_distanceToRoot_forMetric__linkScoreSum[this_id_xmtImgAdjustment] + score_toChild;
			// TODO[web]: cosnide rinstead to copmtue the 'total sum-of-score of distances' (rather than the 'min-case'):
			const t_float score_prev = mapOf_1d_distanceToRoot_forMetric__linkScoreSum[child_id_xmtImgAdjustment];
			if(score_prev != 0) {
			  mapOf_1d_distanceToRoot_forMetric__linkScoreSum[child_id_xmtImgAdjustment] = macro_min(thisToChild_distance_sum, score_prev);
			} else {
			  mapOf_1d_distanceToRoot_forMetric__linkScoreSum[child_id_xmtImgAdjustment] = thisToChild_distance_sum;
			  //mapOf_1d_distanceToRoot_forMetric__nodeCount[child_id_xmtImgAdjustment] = this_distance;
			}
			// printf("linkDist[%d] = %f, at %s:%d\n", child_id_xmtImgAdjustment, mapOf_1d_distanceToRoot_forMetric__linkScoreSum[child_id_xmtImgAdjustment], __FILE__, __LINE__);
				 //printf("\t (add-children)\t %u-->%u w/distance=%f, and max-rootDistance(Child)=%f, at %s:%d\n", this_id_xmtImgAdjustment, child_id_xmtImgAdjustment, score_toChild, thisToChild_distance_sum, __FILE__, __LINE__);
		      }
		    }
		  }
		}
		//! -----------------------------------------------------------------------------
		//!
		//! Add the child tot he stack:
		push__s_kt_set_1dsparse_t(&obj_stack, /*vertex(local-scope)=*/(uint)child_index);	    
	      }
	    }
	  }
	} else {has_data = false;} //! ie, as we then asusme teh alst element is found.
	if(cnt_evaluated > (hca_2dStack.mapOf_internalStartNodes_size*3)) { //! where "*3" is used as a node may not ahve mreo tahn 2 chidlrne, ie, an overeestimate.
	  fprintf(stderr, "!!\t Seems like your data-set does Not hold a 1:1 mapping between the nodes: though the latter may be correct we for simplicy abort at this eeuxeiocn-point: if the 'latter' is of itnerest then pelase forward a feature-request to [eosekth@gmail.com] (thereby remoivngt htis 'cirtical brea'). Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
	  break;
	}
      } while(has_data == true);


      /* // TODO: cosnider adding supprot for [”elow] ... ie, if 'applciaotns of/for such is dientied'.
	@remarks sub-algorith: "itnerla evlauation of the HCA";
	-- input: a result-object 'holding' the HCA-tree
	// FXIME: try to 'icnorproate' the poeprties of the HCA itself ... ie, asusme taht the clsuters are 'descptive' ... ie, where we ....??... 
	step(a): build a 'transitveClosure[internalVertex] = []' (with weight=<config>) and weight[internalVertex] = 'scalar';
	step(b): 
	step(): 

	@remarks sub-algorith: "cluster-sub-division";
	-- input: ... 
	step(a) closeness: build a simliarty-matrix (between the vertices in the HCA);
	step(b) clusters: idneitfy the min-clusters using/through applicaiotn/idneitfiioant of "Minium Spanning Trees" (MST) (eg, examplfied in/at "http://www.geeksforgeeks.org/greedy-algorithms-set-2-kruskals-minimum-spanning-tree-mst/").
	(..): sort the 'parent' ... 
	(..): 
	(..): 
	step(b) traverse tree: 'only' merge vertices
	step() 

	// FIXME[concpetual::ccm]: ... how how may use [ªbove] 'trantive clsoreu' in simliarty-evlaaution of vertices 
	@remarks sub-algorith: "ccm-evauation";
	-- input: sets assicated to each vertex with the distance to realted vertices <-- todo: shoudl be 'construct' for both 'icnreased generality' and 'decreased generlaity'?
	step(a) build a simliarty-matrix (eg, using Euclid) to vertices with a weight above a cofngi->threshold weight;
	step(b) ...??... 
	step() 

	@remarks sub-algorith: "";
	-- input: 
	step() 
      **/
      if(cnt_evaluated == 0) {
	fprintf(stdout, "(info)\t Was not able to idneityf vertices in your data-set: could be due to (a) your network is cyclic; alternativly the network cosnists of 'isoalated non-connected nodes' (a case soemthimes occuring for HCA-dataset). If latter result is Not as expected, then please cotnact senior devleoper [oeksethh@gmail.com]. Observiaotni at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
      }
	//assert(cnt_evaluated > 0); //! ie, what we expect.
      //assert(cnt_evaluated == arrOf_nodes_size); //! ie, as we expec tall fot eh ndoes to have 'been evlauated'.
    }

    //! -----------------------
    //!
    //! De-allcoate the lcoally reserved data-entites:
    //free_1d_list_uint(&mapOf_cntChildsInParents);
    free_s_kt_set_1dsparse_t(&obj_stack);
    //!
    //! De-allcoate:
    free__s_kt_clusterAlg_hca_resultObject_2d_t(&hca_2dStack);
  }

  //! -----------------------
  //!
  //! @reutrn true, if, as we asusme the operaiotn was a scuccess at this execution-point:
  return true;
}


//! Print the resutls of the clsutering in a 'human-redable-format' to stream_out (oesketh, 06. jan. 2017).
bool printHumanReadableResult__extensive__s_kt_clusterAlg_hca_resultObject_t(const s_kt_clusterAlg_hca_resultObject_t *self, char **mapOf_names, FILE *stream_out, const char *seperator_cols, const char *seperator_newLine, const e_kt_clusterAlg_exportFormat_t enum_id) {
  if(!self || !self->nrows || !self->mapOf_nodes) {
    fprintf(stderr, "!!\t Seems like the object does not hold data, ie, please inveisgate your call. Observiaotn at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return false;
  }
  if(!stream_out) {stream_out = stdout;} //! ie, our defualt assumption.
  if(!seperator_cols) {seperator_cols = "\t";}
  if(!seperator_newLine) {seperator_newLine = "\n";}
  //! -----------
  //! Generate data-knowledge-overview:
  const t_float default_value = 0;
  t_float *mapOf_1d_distanceToRoot_forMetric__nodeCount = allocate_1d_list_float(self->nrows, default_value);  
  bool is_ok = constructAdjcencencyMatrix_fromNodeTable__kt_clusterAlg_hca;
  t_float *mapOf_1d_distanceToRoot_forMetric__linkScoreSum = allocate_1d_list_float(self->nrows, default_value);
  is_ok = constructAdjcencencyMatrix_fromNodeTable__kt_clusterAlg_hca(self, /*matrix_result=*/NULL, mapOf_1d_distanceToRoot_forMetric__nodeCount, mapOf_1d_distanceToRoot_forMetric__linkScoreSum, /*norws=*/UINT_MAX, /*mapOf_localIdTo_globalId=*/NULL, false);
  assert(is_ok);
  
  //! -----------
  //! Write out the results:
  fprintf(stream_out, "# Write out the HCA hpLysis cluster-result for a data-set with |vertices|=%u at %s:%d\n", self->nrows, __FILE__, __LINE__);
  fprintf(stream_out, "# HCA-Topology: in below we inlcude the information assicated to each vertex, using a tabular result-format (where 'inf' and '-' indicates no-relationshiup):\n");
  fflush(stream_out); //! ie, to ensure that all of above meta-props are rittne out before below ... a fix to avoid erornously emrging of data (as seen obserrved at 06.09.2020 by oekseth).
  assert(self->mapOf_nodes);
  for(uint row_id = 0; row_id < self->nrows; row_id++) {
    const Node_t node = self->mapOf_nodes[row_id];
    // printf("%d-->%d, at %s:%d\n", node.left, node.right, __FILE__, __LINE__);
    const bool config_visualizeInternalSyntehtciGleuNoes = true; //! which is a new configuration added in an effort to increase specifity of internal cluster-memberships, eg, where the 'imaginary' ('img') nodes bridges both closlyly related vertices, and distantly related vveritces, from whithc ah 'boolean' sepraiton (resulting for case where "config_visualizeInternalSyntehtciGleuNoes == false") resutls in misldaing/confusing perspevie of eperaiton in data-sets (oekseth, 06.09.2020).
    char str_img_left[2000]; memset(str_img_left, '\0', 2000);
    char str_img_right[2000]; memset(str_img_left, '\0', 2000);
    //! ----
    if(config_visualizeInternalSyntehtciGleuNoes) {
      sprintf(str_img_left, "%s%d", "img",  node.left);
      sprintf(str_img_right, "%s%d", "img", node.right);
    } else {
      sprintf(str_img_left,  "%s", "img");
      sprintf(str_img_right, "%s", "img");
    }
    // --------------------------------------------------------------------------

    if(mapOf_names) {
      // --------------------------------------------------------------------------
      if( (node.right != INT_MAX) && (node.left != INT_MAX) ) {
	fprintf(stream_out, // "%s%s"
		"node-count%s%.3f%s"
		"node-distance%s%.3f%s"
		//mapOf_names[row_id], 		seperator_cols,
		, seperator_cols,
		(mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] : INFINITY,
		seperator_cols,		seperator_cols,
		(mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] : INFINITY,
		seperator_cols
		);
	//! Case: left:
	if( (config_visualizeInternalSyntehtciGleuNoes == false) || (node.left >= 0) ) {
	  fprintf(stream_out,
		  "left-child%s%s%s",
		  seperator_cols,
		  //! ---
		  (node.left >= 0) ? mapOf_names[node.left] : "img",
		  //mapOf_names[node.left],
		  seperator_cols);
	} else {
	  fprintf(stream_out,
		  "left-child%s%s%d%s",
		  seperator_cols,
		  //! ---
		  "img", node.left,
		  //mapOf_names[node.left],
		  seperator_cols);
	}
	//! Case: right:
	if( (config_visualizeInternalSyntehtciGleuNoes == false) || (node.right >= 0) ) {
	  fprintf(stream_out,
		  "right-child%s%s",
		  seperator_cols,
		  (node.right >= 0) ? mapOf_names[node.right] : "img");
	} else {
	  fprintf(stream_out,
		  "right-child%s%s%d%s",
		  seperator_cols,
		  //! ---
		  "img", node.right,
		  //mapOf_names[node.left],
		  seperator_cols);
	}
		
	fprintf(stream_out, "%s", 
		seperator_newLine
		);
	// --------------------------------------------------------------------------
      } else if( (node.right != INT_MAX) ) {
	fprintf(stream_out, //"%s%s"
		"node-count%s%.3f%s"
		"node-distance%s%.3f%s"
		"left-child%s-%s"
		"right-child%s%s"
		"%s", 
		//(node.right >= 0) ? mapOf_names[row_id], 		seperator_cols,
		seperator_cols,
		(mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] : INFINITY,
		seperator_cols, seperator_cols,
		(mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] : INFINITY,
		seperator_cols, seperator_cols,
		//! ---
		/* (node.right >= 0) ? mapOf_names[node.left], */
		seperator_cols, seperator_cols,
		(node.right >= 0) ? mapOf_names[node.right] : str_img_right,
		seperator_newLine
		);
      } else if( (node.left != INT_MAX) ) {
	fprintf(stream_out,
		//"%s%s"
		"node-count%s%.3f%s"
		"node-distance%s%.3f%s"
		"left-child%s%s%s"
		"right-child%s-"
		"%s", 
		//(node.right >= 0) ? mapOf_names[row_id],  		seperator_cols,
		seperator_cols,
		(mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] : INFINITY,
		seperator_cols, seperator_cols,
		(mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] : INFINITY,
		seperator_cols, seperator_cols,
		//! ---
		(node.right >= 0) ? mapOf_names[node.right] : str_img_right,
		seperator_cols, seperator_cols,
		//(node.right >= 0) ? mapOf_names[node.right],
		seperator_newLine
		);
     } else  {
	fprintf(stream_out, // "%s%s"
		"node-count%s%.3f%s"
		"node-distance%s%.3f%s"
		"left-child%s-%s"
		"right-child%s-"
		"%s", 
		// mapOf_names[row_id], seperator_cols,
		seperator_cols,	(mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] : INFINITY, seperator_cols, 
		seperator_cols,	(mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] : INFINITY, seperator_cols, 
		seperator_cols,	seperator_cols, 
		seperator_cols,// seperator_cols,
		seperator_newLine
		//! ---
		//mapOf_names[node.left],
		//mapOf_names[node.right]
		);
      }
    } else { //! then we asusme we are to use 'indexes' tow rite out the node-mapping-result:
      //fprintf(stream_out, "%d\tin-clusterCentroid\t%d\n", row_id, clsuter_id);
      if( (node.right != INT_MAX) && (node.left != INT_MAX) ) {
	fprintf(stream_out, // "%d%s"
		"node-count%s%.3f%s"
		"node-distance%s%.3f%s"
		"left-child%s%d%s"
		"right-child%s%d"
		"%s", 
		//row_id, seperator_cols,
		seperator_cols,(mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] : INFINITY, seperator_cols,
		seperator_cols,(mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] : INFINITY, seperator_cols,
		//! ---
		seperator_cols,node.left, seperator_cols,
		seperator_cols,node.right, //seperator_cols,
		seperator_newLine
		);
      } else if( (node.right != INT_MAX) ) {
	fprintf(stream_out, //"%d%s"
		"node-count%s%.3f%s"
		"node-distance%s%.3f%s"
		"left-child%s-%s"
		"right-child%s%d"
		"%s", 
		// row_id, seperator_cols,
		seperator_cols,(mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] : INFINITY, seperator_cols,
		seperator_cols, (mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] : INFINITY, seperator_cols,
		//! ---
		seperator_cols, 		seperator_cols,
		seperator_cols, node.left,
		//node.right]
		seperator_newLine
		);
      } else if( (node.left != INT_MAX) ) {
	fprintf(stream_out, //  "%d%s"
		"node-count%s%.3f%s"
		"node-distance%s%.3f%s"
		"left-child%s%d%s"
		"right-child%s-"
		"%s", 
		//row_id, 		seperator_cols,
		seperator_cols, (mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] : INFINITY, 		seperator_cols,
		seperator_cols, (mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] : INFINITY, seperator_cols,
		//! ---
		//node.left],
		seperator_cols, node.right, seperator_cols,
		seperator_cols,
		seperator_newLine
		);
     } else  {
	fprintf(stream_out, // "%d%s"
		"node-count%s%.3f%s"
		"node-distance%s%.3f%s"
		"left-child%s-%s"
		"right-child%s-"
		"%s", 
		// row_id, seperator_cols,
		seperator_cols, (mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__nodeCount[row_id] : INFINITY, seperator_cols,
		seperator_cols, (mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] != T_FLOAT_MAX) ? mapOf_1d_distanceToRoot_forMetric__linkScoreSum[row_id] : INFINITY, seperator_cols,
		seperator_cols, seperator_cols,
		seperator_cols, 
		//! ---
		//node.left],
		//node.right]
		seperator_newLine
		);
      }
    }
  }

  //! ----------------------
  //!
  //! De-allocate memory:;
  free_1d_list_float(&mapOf_1d_distanceToRoot_forMetric__nodeCount);
  free_1d_list_float(&mapOf_1d_distanceToRoot_forMetric__linkScoreSum);

  //! ----------------------------------------
  //! At this exeuciotn-poitn we asusme the oerpation was a success.
  return true;
}

//! Print the resutls of the clsutering in a 'human-redable-format' to STDOUt.
void printHumanReadableResult__toStdout__s_kt_clusterAlg_hca_resultObject_t(const s_kt_clusterAlg_hca_resultObject_t *self, char **mapOf_names) {
  assert(self);
  FILE *stream_out = stdout;
  //! Apply:
  const char *seperator_cols = "\t";   const char *seperator_newLine = "\n";
  printHumanReadableResult__extensive__s_kt_clusterAlg_hca_resultObject_t(self, mapOf_names, stream_out, seperator_cols, seperator_newLine, e_kt_clusterAlg_exportFormat_all);
}

