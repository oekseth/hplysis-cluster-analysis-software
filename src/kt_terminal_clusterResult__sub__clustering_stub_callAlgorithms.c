
s_kt_api_config_t config = init__s_kt_api_config_t(/*transpose=*/false, /*isTo_applyDisjointSeperation*/false, /*isTo_parallisaiton__sharedMemory=*/false, /*inputMatrix__isAnAdjecencyMatrix=*/true, NULL);

  //! -----------------------------------------------------------------
  if(self->kmean_isToUse_kMedian) {
    s_kt_clusterAlg_fixed_resultObject_t obj_result; setTo_empty__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
    if(self->kmean_kmean_isToUse_medoidInComputation == false) {
/*transpose=*/false, 
	kt_matrix__kmeans_or_kmedian(metric_insideClustering, local_matrix, &obj_result, /*obj_result_clusterId_columnValue=*/NULL, self->kmean_kmean_nclusters, self->kmean_npass, (false == self->kmean_isToUse_kMedian), config, NULL);
    } else {
      kt_matrix__kmedoids(metric_insideClustering, local_matrix, &obj_result, self->kmean_kmean_nclusters, self->kmean_npass, config, NULL);
    }
    //!
    //! Update the result-object:
    updateClusterResults__kMeans__s_kt_clusterResult_data_t(&obj_export, &obj_result, keyMap_localToGlobal);

      
    //! -----------------------------------------------
    //! De-allocat ehte local object:
    free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
  }
  if(self->isToCompute_hca) {
    s_kt_clusterAlg_hca_resultObject_t obj_result;  setTo_empty__s_kt_clusterAlg_hca_resultObject_t(&obj_result, nrows);
    const e_kt_clusterAlg_hca_type_t method_id = get_enum_fromChar__kt_clusterAlg_hca(self->hca_method);
    const bool is_ok = kt_matrix__hca(metric_insideClustering, local_matrix, &obj_result, method_id, config); ///*transpose=*/transpose);
    //!
    //! Write out the result:
    updateClusterResults__HCA__s_kt_clusterResult_data_t(&obj_export, &obj_result, keyMap_localToGlobal);

    //! -----------------------------------------------
    //! De-allocat ehte local object:
    free__s_kt_clusterAlg_hca_resultObject_t(&obj_result);
  }
  if(self->isToCompute_som) {
    s_kt_clusterAlg_SOM_resultObject_t obj_result;  setTo_empty__s_kt_clusterAlg_SOM_resultObject_t(&obj_result, nrows, self->som_nxgrid, self->som_nygrid);
    kt_matrix__som(metric_insideClustering, local_matrix, &obj_result, 
		   self->som_nxgrid, self->som_nygrid, self->som_initTau, 
		   self->som_niter,
		   config);
    //!
    //! Update the result-object:
    updateClusterResults__SOM__s_kt_clusterResult_data_t(&obj_export, &obj_result, keyMap_localToGlobal, self->som_nxgrid, self->som_nygrid);

    //! -----------------------------------------------
    //! De-allocat ehte local object:
    free__s_kt_clusterAlg_SOM_resultObject_t(&obj_result);
  }

  if(self->isToCompute_pca) {

    fprintf(stderr, "!!\t FIXME[JC]: add support for PCA-result-generation wehn JC inlclues his impelmetnaiton and 'data-itnerptiraotn-strategy', at %s:%d\n", __FILE__, __LINE__); 

    /* assert(false); //! FIXME: update 'this' after JC has udpated/written the PCA-code. */
    /* //! */
    /* //! Write out the result: */
    /* assert(false); // FIXME: write out the reuslt-object. */
    //! -----------------------------------------------
    //! De-allocat ehte local object:
    //free__s_kt_clusterAlg_fixed_resultObject_t(&obj_result);
  }
