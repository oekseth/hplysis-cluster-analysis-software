{


  t_float **matrix = _matrix;

  if(self.isTo_useSSE && (self.isTo_useSSE__matrixIsAllocated_with_overfloatSSE_space == false) ) {
    matrix = kt_mathMacros_SSE__convertToInternalMatrix(_matrix, nrows, ncols);
  }


  //#define __macro__typeOf_eigenVectorType_adjustment __macro__e_kt_eigenVectorCentrality_eachValue_sumOf_scalar
  // FIXEM: amke use of this function ... ie, write a 'pbulic itnerface' to this fucntion ... and test/evlauate the perfomrance 'of this'.


  //t_float *mapOf_weights = NULL;



  /* if( (typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && isTo_useSSE) { */

  /* } else if( (typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && !isTo_useSSE) { */

  /* } else if( (typeOf_eigenVectorType_adjustment == e_kt_eigenVectorCentrality_eachValue_sumOf_scalar) && (typeOf_eigenVectorType_adjustment_postProcessTAble == e_kt_eigenVectorCentrality_postProcess_result) && !isTo_useSSE) { */




  const t_float default_value_float = 0;
  t_float *mapOf_vertexSums = allocate_1d_list_float(nrows, default_value_float);
  t_float *arrOf_result_prev = allocate_1d_list_float(nrows, default_value_float);

  //!
  //! Build eh distance-table.
#if(__config__typeOf_eigenVectorType_adjustment_postProcessTable != __macro__e_kt_eigenVectorCentrality_postProcess_none)
#include "kt_clusterAlg_SVD__func__eigenVector_buildDistanceTable.c"
#else //! then we set the weight-table to "1"
  for(uint row_id = 0; row_id < nrows; row_id++) { mapOf_vertexSums[row_id] = 1;} 
#endif

    bool has_converged = true;
    if(self.typeOf_eigenVectorType_adjustment != e_kt_eigenVectorCentrality_none) { //! then we apply/use power-iteration:
#if(__macro__typeOf_eigenVectorType_use != __macro__e_kt_eigenVectorCentrality_typeOf_weightUse_none)
#include "func__kt_centrality__powerIteration.c"
/* #else */
/* #error "!!\t Investigate consistency in your call" */
#endif
    } else { //! then we assume taht the 'vertex-sums' describes 'teh case'.
      assert(mapOf_vertexSums);
      memcpy(arrOf_result, mapOf_vertexSums, sizeof(t_float)*nrows);
    }


    // FIXME[future]: .... add support for 'leveraging' ... eg, use "(B_prev[row_id] - B_prev[col_id]) / (B_prev[row_id] + B_prev[col_id])" ... ie, merge our 'high-perofmrance-correlation metric/functiosn' with centrliaty-metrics (oekseth, 06. okt. 2016).

  //! --------------------
  //! Apply post-processing rotuiens, ie, to 'smooth' the results:
  //! 
  if(
     (self.typeOf_postAdjustment == e_kt_centrality_typeOf_postAdjustment_meanOffset_sub) || 
     (self.typeOf_postAdjustment == e_kt_centrality_typeOf_postAdjustment_meanOffset_sub_abs) || 
     (self.typeOf_postAdjustment == e_kt_centrality_typeOf_postAdjustment_meanOffset_div) 
     ) {
    t_float sum = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) { sum += arrOf_result[row_id];}
    if(sum != 0) {
      sum = sum / nrows;
      if(self.typeOf_postAdjustment == e_kt_centrality_typeOf_postAdjustment_meanOffset_sub) {	
	for(uint row_id = 0; row_id < nrows; row_id++) { arrOf_result[row_id] -= sum;}
      } else if(self.typeOf_postAdjustment == e_kt_centrality_typeOf_postAdjustment_meanOffset_sub_abs) {	
	for(uint row_id = 0; row_id < nrows; row_id++) { arrOf_result[row_id] = mathLib_float_abs(arrOf_result[row_id] - sum);}
      } else if(self.typeOf_postAdjustment == e_kt_centrality_typeOf_postAdjustment_meanOffset_div) {	
	sum = 1/sum;
	for(uint row_id = 0; row_id < nrows; row_id++) { arrOf_result[row_id] *= sum;}
      } else {
	assert(false); //! ie, as we then need to add support 'for this case'.
      }
    }
  } else if( (self.typeOf_postAdjustment == e_kt_centrality_typeOf_postAdjustment_log) ) {
    const t_float log10_inv = 1/mathLib_float_log(10);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      arrOf_result[row_id] = (arrOf_result[row_id] != 0) ? (mathLib_float_log(arrOf_result[row_id]) * log10_inv) : 0; //new_distance;
    }
  } else if( (self.typeOf_postAdjustment == e_kt_centrality_typeOf_postAdjustment_log_shannon) ) {
    const t_float log10_inv = 1/mathLib_float_log(10);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      const t_float log_value = mathLib_float_log(arrOf_result[row_id]) * log10_inv;
      arrOf_result[row_id] = (arrOf_result[row_id] != 0) ? (arrOf_result[row_id] * log_value) : 0; //new_distance;
    }
  } else if(
	    (self.typeOf_postAdjustment != e_kt_centrality_typeOf_postAdjustment_none) && 
	    (self.typeOf_postAdjustment != e_kt_centrality_typeOf_postAdjustment_undef)
	    ) {
    assert(false); //! ie, as we then need to add support 'for this case'.
  }
  

  if(mapOf_vertexSums) {
    free_1d_list_float(&mapOf_vertexSums);
  }

  if(self.isTo_useSSE && (self.isTo_useSSE__matrixIsAllocated_with_overfloatSSE_space == false) ) {
    free_2d_list_float(&matrix, nrows); //! ie, then de-allcoate the lcoally reserved matrix.
  }


  //arrOf_result_prev.free_memory();
  
  //! @returnt he result to the caller:
  return has_converged;
}


//! -------------------------
#undef __config__compute_eigenVector__isTo_useIncrementallyUpdatedVector
#undef __config__compute_eigenVector__isTo_useSSE
#undef  __macro__typeOf_eigenVectorType_adjustment
#undef __config__typeOf_eigenVectorType_adjustment_postProcessTable
#undef __macro__typeOf_eigenVectorType_use
#undef __macro__typeOf_performanceTuning
#undef __macro_funcTag__accessScores_distanceTable
