#ifndef s_kt_correlationConfig_h
#define s_kt_correlationConfig_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */
/**
   @file s_kt_correlationConfig
   @brief a structure and lgocis to generalize configurations of correlation-tasks.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "mask_api.h"
#include "correlation_base.h"
#include "correlation_sort.h"
#include "correlation_rank.h"

/**
   @struct s_kt_correlationConfig
   @brief defiens the perofrmance-optmizaiton-strategies to be applied wrt. comptuation of correlations.
   @remarks is of specific improtance when using our correlation-impelmetnation to infer genralized optmizaiton-strategies wrt. programming.
   @auhtor Ole Kristine Ekseth (oekseth)
 **/
typedef struct s_kt_correlationConfig {
  bool forNonTransposed_useFastImplementaiton;
  bool forNonTransposed_useFastImplementaiton_useSSE;
  e_cmp_masksAre_used_t masksAre_used; //! which is defined in our "correlation_enums.h" and used to reduce time-cost wrt. 'masking of cells' (ie, to know/identify the values to be used in the input-matrix).
  
  //! Configurations:
  t_float configMetric_power; //! whihc is the default power-valeu to be sued, ie, if any.
} s_kt_correlationConfig_t;

//! @return false if we do Not need to use masks (oekseth, 06. sept. 2016).
static bool s_kt_correlationConfig_needTo_useMask_evaluation(const s_kt_correlationConfig_t *self) {
  assert(self);
  return (self->masksAre_used != e_cmp_masksAre_used_false); //! ie, if the 'no-mask' proeprty is epxlcitly set, then we assume that masks are Not needed to be seud (oekseth, 06. sept. 2016).
}

//! Intiates the "s_kt_correlationConfig" object.
static void s_kt_correlationConfig_init(s_kt_correlationConfig_t *self) {
  self->forNonTransposed_useFastImplementaiton = true;
  self->forNonTransposed_useFastImplementaiton_useSSE = true;
  self->masksAre_used = e_cmp_masksAre_used_undef; //! ie, the devfault assumption.
  self->configMetric_power = 3; //! ie, our default assumption for the "Minkowski" distance-metric
  // self-> = true;
}
//! Intiates the "s_kt_correlationConfig" object.
static void s_kt_correlationConfig_init_useSlow(s_kt_correlationConfig_t *self) {
  self->forNonTransposed_useFastImplementaiton = false;
  self->forNonTransposed_useFastImplementaiton_useSSE = false;
  self->masksAre_used = e_cmp_masksAre_used_true; //! ie, the devfault assumption.
  // self-> = true;
}
//! Intiates the "s_kt_correlationConfig" object.
static void s_kt_correlationConfig_init_useFast(s_kt_correlationConfig_t *self, const e_cmp_masksAre_used_t masksAre_used) {
  self->forNonTransposed_useFastImplementaiton = true;
  self->forNonTransposed_useFastImplementaiton_useSSE = true;
  self->masksAre_used = masksAre_used;
  // self-> = true;
}



#endif //! EOF
