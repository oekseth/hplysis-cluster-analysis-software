/* #ifndef metricMacro_combination_h */
/* #define metricMacro_combination_h */


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_combination
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "combination".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/


// FIXME: write perofmrance-teets and correnctess-tests for tehse cases/metrics

//! @return the correlations-core for: Taneja
#define correlation_macros__distanceMeasures__combination__Taneja(term1, term2) ({ \
 const t_float result_left_numerator = term1 + term2;		\
 const t_float result_left = result_left_numerator * 0.5;	\
 const t_float result_right_denumerator_sqrt_mult = term1 * term2; \
 const t_float result_right_denumerator_sqrt = mathLib_float_sqrt_abs(result_right_denumerator_sqrt_mult); \
 assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_left_numerator) == false); \
 assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_right_denumerator_sqrt_mult) == false); \
 const t_float result_right_denumerator = 2* result_right_denumerator_sqrt; \
 const t_float result_right = ( (result_left_numerator != 0) & (result_right_denumerator != 0) ) ? mathLib_float_log_abs(result_left_numerator / result_right_denumerator) : 0; \
 assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result_right) == false); \
 const t_float result = result_left * result_right;	\
result; })
#define correlation_macros__distanceMeasures__combination__Taneja__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__combination__Taneja__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE result_left_numerator = VECTOR_FLOAT_ADD(term1, term2); \
      const VECTOR_FLOAT_TYPE result_left = VECTOR_FLOAT_MUL(result_left_numerator, VECTOR_FLOAT_SET1(0.5)); \
      const VECTOR_FLOAT_TYPE result_right_denumerator_sqrt_mult = VECTOR_FLOAT_MUL(term1, term2); \
      const VECTOR_FLOAT_TYPE result_right_denumerator_sqrt = VECTOR_FLOAT_SQRT_abs(result_right_denumerator_sqrt_mult); \
      const VECTOR_FLOAT_TYPE result_right_denumerator = VECTOR_FLOAT_MUL(VECTOR_FLOAT_SET1(2), result_right_denumerator_sqrt); \
      const VECTOR_FLOAT_TYPE result_right = VECTOR_FLOAT_DIV(result_left_numerator, result_right_denumerator); \
      VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_MUL(result_left, VECTOR_FLOAT_LOG(VECTOR_FLOAT_abs(result_right))); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; })


//! @return the correlations-core for: Kumar-Johnson
#define correlation_macros__distanceMeasures__combination__KumarJohnson(term1, term2) ({ \
	  const t_float numerator_pow_1 = term1 * term1; \
	  const t_float numerator_pow_2 = term2 * term2; \
	  const t_float numerator_pow_diff = numerator_pow_1 - numerator_pow_2; \
	  const t_float numerator = numerator_pow_diff * numerator_pow_diff; \
	  const t_float denumerator_sqrt_mult = term1 * term2; \
	  const t_float denumerator_sqrt = mathLib_float_pow_abs(denumerator_sqrt_mult, /*(3/2)=*/1.5); \
	  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(denumerator_sqrt) == false); \
	  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(denumerator_sqrt) == false); \
	  const t_float denumerator = 2* denumerator_sqrt;		\
	  const t_float result =  ( (numerator != 0) & (denumerator != 0) ) ? numerator / denumerator : 0; \
	  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isnan(result) == false); \
	  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(isinf(result) == false); \
	  result; })
#define correlation_macros__distanceMeasures__combination__KumarJohnson__postProcess(value) ({ \
      metric_macro_defaultFunctions__postProcess__none(value);})
#define correlation_macros__distanceMeasures__combination__KumarJohnson__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE numerator_pow_1 = VECTOR_FLOAT_MUL(term1, term1); \
      const VECTOR_FLOAT_TYPE numerator_pow_2 = VECTOR_FLOAT_MUL(term2, term2); \
      const VECTOR_FLOAT_TYPE numerator_pow_diff = VECTOR_FLOAT_SUB(numerator_pow_1, numerator_pow_2); \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MUL(numerator_pow_diff, numerator_pow_diff); \
      const VECTOR_FLOAT_TYPE denumerator_sqrt_mult = VECTOR_FLOAT_MUL(term1,  term2); \
      const VECTOR_FLOAT_TYPE denumerator_sqrt = VECTOR_FLOAT_POWER_abs(denumerator_sqrt_mult, /*(3/2)=*/1.5); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MUL(VECTOR_FLOAT_SET1(2), denumerator_sqrt); \
      VECTOR_FLOAT_TYPE result =  VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */				\
      result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(result);	\
      result = VECTOR_FLOAT_dataMask_replaceINF_by_Zero(result);	\
      result; })

//! Idnetify the correlations-core for: a combination of average-chebychev and cityblock
#define correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock(term1, term2, obj) ({ \
  const t_float numerator  = mathLib_float_abs(term1 - term2); \
  obj.numerator += numerator; obj.denumerator = macro_max(obj.denumerator, numerator); \
    })
#define correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock__postProcess(obj) ({ \
      metric_macro_defaultFunctions__postProcess__validateEmptyCase(0.5 * (obj.numerator + obj.denumerator));})
#define correlation_macros__distanceMeasures__combination__averageOfChebyshevAndCityblock__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE numerator  = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      obj.numerator += numerator; obj.denumerator = VECTOR_FLOAT_MAX(obj.denumerator, numerator); \
    })



//#endif //! EOF
