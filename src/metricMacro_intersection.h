/* #ifndef metricMacro_intersection_h */
/* #define metricMacro_intersection_h */

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file metricMacro_intersection
   @brief provide functiosn for comptuation and evaluation of different metrics in class: "intersection".
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/

// FIXME: write perofmrance-teets and correnctess-tests for tehse cases/metrics


//! @return the correlations-core for: Intersection
#define correlation_macros__distanceMeasures__intersection__intersection(term1, term2) ({ \
  const t_float ret_val = macro_min(term1, term2); \
  ret_val; }) //! ie, return.
#define correlation_macros__distanceMeasures__intersection__intersection__SSE(term1, term2) ({ \
  const VECTOR_FLOAT_TYPE vec_input_result = VECTOR_FLOAT_MIN(term1, term2); \
  vec_input_result; }) //! ie, return.
#define correlation_macros__distanceMeasures__intersection__intersectionAlt(term1, term2) ({ \
      const t_float ret_val = mathLib_float_abs(macro_minus(term1, term2)); \
      ret_val; }) //! ie, return.
// FIXME: why is 'itnernsection-alt' divided by "2" ie, mulipled by "0.5" ... ie, why does the latter 'improve the reuslt-accarcy of the metric'?
#define correlation_macros__distanceMeasures__intersection__intersectionAlt__postProcess(value) ({ \
  t_float input_result = value * 0.5;						\
  /*if(input_result != 0) {input_result = mathLib_float_abs(1/input_result); }*/ \
  /*! Adjust the input_result in order to 'reflect' the exepcted resutls of \"correlation_macros__distanceMeasures__intersection__intersection(..)\": */ \
  metric_macro_defaultFunctions__postProcess__1_minusValue(input_result);}) //! ie, adjsut by the overall signicance of the terms invovled.
#define correlation_macros__distanceMeasures__intersection__intersectionAlt__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE ret_val = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2));	\
      ret_val; }) //! ie, return.


/* //! @return the correlations-core for: Intersection (inverted) */
/* #define correlation_macros__distanceMeasures__intersection__intersectionInverted(term1, term2) ({ \ */
/*   const t_float ret_val = mathLib_float_abs(term1 - term2); \ */
/*   ret_val; }) //! ie, return. */
/* #define correlation_macros__distanceMeasures__intersection__intersectionInverted__SSE(term1, term2) ({ \ */
/*       const VECTOR_FLOAT_TYPE vec_input_result = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \ */
/*       vec_input_result; }) //! ie, return. */

//! @return the correlations-core for: Wave Hedges
#define correlation_macros__distanceMeasures__intersection__WaveHedges(term1, term2) ({ \
  const t_float numerator = min(term1, term2); \
  const t_float denumerator = max(term1, term2); \
  const t_float input_result = (denumerator != 0) ? (1 - numerator / denumerator) : 0; \
      /*! Update the input_result-object: */					\
  input_result;})
//! @return the correlations-core for: Wave Hedges
#define correlation_macros__distanceMeasures__intersection__WaveHedges__SSE(term1, term2) ({ \
  const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MIN(term1, term2); \
  const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MAX(term1, term2); \
  const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_DIV(numerator, denumerator); \
  VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET1(1), diff); \
  /*Handle dividye-by-zero-cases: */					\
  input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);		\
  /*! Update the input_result-object: */					\
  input_result;})
//! @return the correlations-core for: Wave Hedges
#define correlation_macros__distanceMeasures__intersection__WaveHedges_alt(term1, term2) ({ \
  const t_float numerator = mathLib_float_abs(term1 - term2); \
  const t_float denumerator = max(term1, term2); \
  const t_float input_result = (denumerator != 0) ? numerator / denumerator : 0; \
      /*! Update the input_result-object: */					\
  input_result;})
//! @return the correlations-core for: Wave Hedges
#define correlation_macros__distanceMeasures__intersection__WaveHedges_alt__SSE(term1, term2) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MAX(term1, term2); \
      VECTOR_FLOAT_TYPE input_result = VECTOR_FLOAT_DIV(numerator, denumerator); \
      /*Handle dividye-by-zero-cases: */				\
      input_result = VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(input_result);	\
      /*! Update the input_result-object: */					\
      input_result;})

/* //! @return the correlations-core for: Wave Hedges */
/* #define correlation_macros__distanceMeasures__intersection__WaveHedgesAbsolute__SSE(term1, term2) ({ \ */
/*     const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_abs(term1, term2); \ */
/*     const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MAX(term1, term2); \ */
/*     const VECTOR_FLOAT_TYPE diff = VECTOR_FLOAT_DIV(numerator, denumerator); \ */
/*     /\*! Update the input_result-object: *\/					\ */
/*     diff;}) */

//! @brief idetnify the correlations-core for: Czekanowski
#define correlation_macros__distanceMeasures__intersection__Czekanowski(term1, term2, obj) ({ \
  const t_float numerator = min(term1, term2); \
  const t_float denumerator = term1 + term2; \
  /*! Update the input_result-object: */					\
  obj.numerator += numerator; obj.denumerator += denumerator;	\
    })

// FXIME: write an 'empriitc study using test-data-set' to investigate the differences in 'prodcued clsuters' when usign differnet "constant" values in [”elow] ... ie, the improtance/sensitivty of the post-processing-funcitons 'assicated to the "Czekanowski" metric'.
#define correlation_macros__distanceMeasures__intersection__Czekanowski__postProcess(obj) ({ \
  const t_float input_result = metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty_multiplyBy_constant(obj, /*constant=*/2); \
  input_result; }) //! ie, return the input_result
//! @brief idetnify the correlations-core for: Czekanowski
#define correlation_macros__distanceMeasures__intersection__CzekanowskiAlt(term1, term2, obj) ({ \
      const t_float numerator = mathLib_float_abs(term1 - term2);			\
      const t_float denumerator = term1 + term2;			\
      /*! Update the input_result-object: */					\
      obj.numerator += numerator; obj.denumerator += denumerator; \
    })
//! @brief idetnify the correlations-core for: Czekanowski
#define correlation_macros__distanceMeasures__intersection__Czekanowski__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MIN(term1, term2); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1, term2); \
      /*! Update the input_result-object: */					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, numerator); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, denumerator); \
    })
//const t_float input_result = metric_macro_defaultFunctions__postProcess__1_minusValue_xmtDefaultAdjustment(metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(obj));
#define correlation_macros__distanceMeasures__intersection__CzekanowskiAlt__postProcess(obj) ({ \
  const t_float input_result = metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide(obj); \
  input_result; })
//! @brief idetnify the correlations-core for: Czekanowski
#define correlation_macros__distanceMeasures__intersection__CzekanowskiAlt__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(term1, term2)); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1, term2); \
      /*! Update the input_result-object: */					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, numerator); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, denumerator); \
    })


//! @brief identify the correlations-core for: Motyka
#define correlation_macros__distanceMeasures__intersection__Motyka(term1, term2, obj) ({ \
    const t_float numerator = min(term1, term2); \
    const t_float denumerator = term1 + term2;				\
      /*! Update the input_result-object: */					\
      obj.numerator += numerator; obj.denumerator += denumerator; \
})
#define correlation_macros__distanceMeasures__intersection__Motyka__postProcess(obj) ({ \
      metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide(obj);})  
//! @brief identify the correlations-core for: Motyka
#define correlation_macros__distanceMeasures__intersection__Motyka__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MIN(term1, term2); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1, term2); \
      /*! Update the input_result-object: */					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, numerator); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, denumerator); \
})
#define correlation_macros__distanceMeasures__intersection__MotykaAlt(term1, term2, obj) ({ \
    const t_float numerator = max(term1, term2); \
    const t_float denumerator = term1 + term2;				\
      /*! Update the input_result-object: */					\
      obj.numerator += numerator; obj.denumerator += denumerator; \
})
//      metric_macro_defaultFunctions__postProcess__1_minusValue_xmtDefaultAdjustment(metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide(obj));})
#define correlation_macros__distanceMeasures__intersection__MotykaAlt__postProcess(obj) ({ \
      metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide(obj);})

//! @brief identify the correlations-core for: MotykaAlt
#define correlation_macros__distanceMeasures__intersection__MotykaAlt__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MAX(term1, term2); \
      const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_ADD(term1, term2); \
      /*! Update the input_result-object: */					\
      obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, numerator); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, denumerator); \
})


//! @return the correlations-core for: Kulczynski
#define correlation_macros__distanceMeasures__intersection__Kulczynski(term1, term2, obj) ({ \
    const t_float numerator = min(term1, term2); \
    const t_float denumerator = mathLib_float_abs(term1 - term2);	\
    /*! Update the input_result-object: */					\
    obj.numerator += numerator; obj.denumerator += denumerator; \
})
#define correlation_macros__distanceMeasures__intersection__Kulczynski__postProcess(obj) ({ \
      metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(obj); })
//! @return the correlations-core for: Kulczynski
#define correlation_macros__distanceMeasures__intersection__Kulczynski__SSE(term1, term2, obj) ({ \
    const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MIN(term1, term2); \
    const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_abs(term1 - term2);	\
    /*! Update the input_result-object: */					\
    obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, numerator); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, denumerator); \
})


//! @brief identify the correlations-core for: Ruzicka
#define correlation_macros__distanceMeasures__intersection__Ruzicka(term1, term2, obj) ({ \
    const t_float numerator = min(term1, term2); \
    const t_float denumerator = max(term1, term2); \
      /*! Update the input_result-object: */					\
      obj.numerator += numerator; obj.denumerator += denumerator; \
})
#define correlation_macros__distanceMeasures__intersection__Ruzicka__postProcess(obj) ({ \
      metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(obj); })
//! @brief identify the correlations-core for: Ruzicka
#define correlation_macros__distanceMeasures__intersection__Ruzicka__SSE(term1, term2, obj) ({ \
      const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MIN(term1, term2); \
    const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MAX(term1, term2); \
      /*! Update the input_result-object: */					\
    obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, numerator); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, denumerator); \
})

//! @brief identify the correlations-core for: Tanimoto
#define correlation_macros__distanceMeasures__intersection__Tanimoto(term1, term2, obj) ({ \
    const t_float numerator = min(term1, term2); \
    const t_float denumerator = max(term1, term2); \
      /*! Update the input_result-object: */					\
    obj.numerator += (numerator - denumerator); obj.denumerator += denumerator; \
})
#define correlation_macros__distanceMeasures__intersection__Tanimoto__postProcess(obj) ({ \
      metric_macro_defaultFunctions__postProcess__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(obj);})
//! @brief identify the correlations-core for: Tanimoto
#define correlation_macros__distanceMeasures__intersection__Tanimoto__SSE(term1, term2, obj) ({ \
    const VECTOR_FLOAT_TYPE numerator = VECTOR_FLOAT_MIN(term1, term2); \
    const VECTOR_FLOAT_TYPE denumerator = VECTOR_FLOAT_MAX(term1, term2); \
    /*! Update the input_result-object: */					\
    obj.numerator = VECTOR_FLOAT_ADD(obj.numerator, numerator); obj.denumerator = VECTOR_FLOAT_ADD(obj.denumerator, denumerator); \
})
#define correlation_macros__distanceMeasures__intersection__TanimotoAlt(term1, term2, obj) ({ \
  obj.denumeratorLimitedTo_row1 += term1;   obj.denumeratorLimitedTo_row2 += term2; \
  obj.denumeratorLimitedTo_row_1and2 = macro_pluss(obj.denumeratorLimitedTo_row_1and2, macro_min(term1, term2)); \
})
#define correlation_macros__distanceMeasures__intersection__TanimotoAlt__postProcess(obj) ({ \
  const t_float numerator   = obj.denumeratorLimitedTo_row1 + obj.denumeratorLimitedTo_row2 - 2* obj.denumeratorLimitedTo_row_1and2; \
  const t_float denumerator = obj.denumeratorLimitedTo_row1 + obj.denumeratorLimitedTo_row2 - obj.denumeratorLimitedTo_row_1and2; \
  metric_macro_defaultFunctions__postProcess__scalar__numeratorDenumerator_divide__subCase__resultMayNeverBeInfininty(numerator, denumerator);})

#define correlation_macros__distanceMeasures__intersection__TanimotoAlt__SSE(term1, term2, obj) ({ \
      obj.denumeratorLimitedTo_row1 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row1, term1); \
      obj.denumeratorLimitedTo_row2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row2, term2); \
      obj.denumeratorLimitedTo_row_1and2 = VECTOR_FLOAT_ADD(obj.denumeratorLimitedTo_row_1and2, VECTOR_FLOAT_MIN(term1, term2)); \
    })

// #endif //! EOF
