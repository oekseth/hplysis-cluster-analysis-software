#include "kt_math_matrix.h"
#include "kt_matrix.h"
#include "kt_matrix_extendedLogics.h"
#include "measure_base.h"
#include "def_memAlloc.h" //! which is sued for emmorya-llcoation-rotuiens.
static t_float  sum_local = 0;

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! Comptue inner distance-metric for euclid
void alt__kt_func_metricInner_fast__fitsIntoSSE__euclid__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  t_float sum = 0;
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      //const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	/* const t_float *__restrict__ row_1 = row_1_base + j; */
	/* const t_float *__restrict__ row_2 = row_2_base + j; */
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1[j+int_j]), VECTOR_FLOAT_LOAD(&row_2[j+int_j])); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[j+int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      //vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1_base[j+int_j]), VECTOR_FLOAT_LOAD(&row_2_base[j+int_j])));
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1[j+int_j]), VECTOR_FLOAT_LOAD(&row_2[j+int_j])));
	      //vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[j+int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }

  //sum_local += su;
}
//! Breif providdes an introduciton to the use of SSE in copmtatuiosn, usign the macros in our hplysis software (oekseth, 06. mar. 2017)
//! Compile: "g++ -I../src/  -g -O0  -mssse3   -L ../src/  tut_sse_matrixMult_lowLevel.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC"
int main() {
  //! cmpo with Python: matrix[768,768] 0.258333333333 0.253333333333  0.055
  const uint nrows = 768*4;
  const uint ncols = nrows;
  if(true) {
    t_float **matrix; matrix = alloc_generic_type_2d(t_float, matrix, nrows);
    t_float **result_matrix; result_matrix = alloc_generic_type_2d(t_float, result_matrix, nrows);
    t_float empty_0 = 0;
    const bool useMEmAlloc__2d  = false;
    if(useMEmAlloc__2d) {
      const t_float default_value = 0;
      matrix = allocate_2d_list_float(nrows, ncols, default_value);
      result_matrix = allocate_2d_list_float(nrows, nrows, default_value);
      for(uint i = 0; i < nrows; i++) {
	for(uint k = 0; k < ncols; k++) {
	  matrix[i][k] = k; //! ie, a 'defualt itnaiton'.
	}
      }
    } else{ 
      for(uint i = 0; i < nrows; i++) {
	
	if(false) {
	  const uint size = ncols; const t_float default_value = 0;
	  //const uint size_chunks = 8;
	  const uint size_chunks = 64;
	  //const uint size_chunks = 32;
	  const int result_data = (int)posix_memalign((void**)&matrix[i], size_chunks, (int)(size) * sizeof(t_float)); 
	  assert((int)result_data == 0); 
	  assert(matrix[i]);
	  // result_matrix[i] = allocate_1d_list_float(nrows, empty_0);
	  //}
	  for(uint k = 0; k < ncols; k++) {
	    matrix[i][k] = k; //! ie, a 'defualt itnaiton'.
	  }
	  { //! A sampe-iteriaotn to vlaidate taht our "_mm_load_ps(..)" (or: "VECTOR_FLOAT_LOAD(..)") 'works without emmroy-errors':
	    const t_float *__restrict__ row_1 = matrix[i];
	    uint int_j = 0; 
	    VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOAD(&row_1[int_j]);
	    VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOAD(&row_1[int_j]);
	    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_1, vec_2);	  
	}
	  //memset_local(/*arr=*/matrix[i], default_value, /*arr-size=*/size); 
	  //ptr;
	} else { matrix[i] = allocate_1d_list_float(ncols, empty_0); }
	result_matrix[i] = allocate_1d_list_float(nrows, empty_0);
	for(uint k = 0; k < ncols; k++) {
	  matrix[i][k] = k; //! ie, a 'defualt itnaiton'.
	}
      }
    }
    start_time_measurement();
    const char *str_local = "simple-plus-memory[SSE+tiling:SM=32]";

    alt__kt_func_metricInner_fast__fitsIntoSSE__euclid__float(matrix, matrix, nrows, ncols, result_matrix, /*SM=*/64);
    //  t_float prev_time_inSeconds__ignore = 
    t_float user_time = 0; t_float system_time = 0;
    t_float prev_time_inSeconds = 0;
    end_time_measurement(/*msg=*/str_local, user_time, system_time, prev_time_inSeconds);
    //! Print sum of elementts:
    t_float sum = 0;
    for(uint i = 0; i < nrows; i++) {
      for(uint k = 0; k < nrows; k++) {
	sum += result_matrix[i][k];
      }
    }
    printf("sum=%f, for matrix=[%u, %u], at %s:%d\n", sum, nrows, ncols, __FILE__, __LINE__);
  } else {
    s_kt_matrix_t mat_result = initAndReturn__s_kt_matrix(nrows, nrows);    
    s_kt_matrix_t mat_val = initAndReturn__randomUint__s_kt_matrix_extendedLogics(nrows, ncols, /*maxVal=*/100, 
										  /*typeof_randomness=*/e_kt_randomGenerator_type_posixRan__srand__time__modulo,
										  //e_kt_randomGenerator_type_hyLysisDefault,
										  /*isTo_initIntoIncrementalBlocks=*/false);
    start_time_measurement();
    const char *str_local = "simple-plus-memory[SSE+tiling:SM=32]";

    alt__kt_func_metricInner_fast__fitsIntoSSE__euclid__float(mat_val.matrix, mat_val.matrix, nrows, ncols, mat_result.matrix, /*SM=*/64);    
    //alt__kt_func_metricInner_fast__fitsIntoSSE__euclid__float(mat_val.matrix, mat_val.matrix, nrows, ncols, mat_result.matrix, /*SM=*/128);    
    //  t_float prev_time_inSeconds__ignore = 
    t_float user_time = 0; t_float system_time = 0;
    t_float prev_time_inSeconds = 0;
    end_time_measurement(/*msg=*/str_local, user_time, system_time, prev_time_inSeconds);
    free__s_kt_matrix(&mat_val);
    free__s_kt_matrix(&mat_result);
  }
  return 0;
}
