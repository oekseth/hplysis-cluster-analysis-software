#ifndef alg_dbScan_brute_h
#define alg_dbScan_brute_h
/**
   @file
   @brief provides access to different DBSCAN non-otpmzied implemtatnions. 
 **/
#include "alg_dbScan_brute_linkedListSet.h"
#include "alg_dbScan_brute_sciKitLearn.h"

/**
   @enum e_alg_dbScan_brute
   @brief idneitfes the brute-force implemtantion to be used.
 **/
typedef enum e_alg_dbScan_brute {
  e_alg_dbScan_brute_linkedListSet,
  e_alg_dbScan_brute_sciKitLearn,
  e_alg_dbScan_brute_splitData_kdTree,
  e_alg_dbScan_brute_splitData_brute_fast,
  e_alg_dbScan_brute_splitData_brute_slow,    
  e_alg_dbScan_brute_hpCluster,    
  //e_alg_dbScan_brute_
  //! ------------------------
e_alg_dbScan_brute_undef
} e_alg_dbScan_brute_t;

//! @return the sting-representaiton of the requested algorithm-id
static const char *get_strShort_e_alg_dbScan_brute_t(const e_alg_dbScan_brute_t alg_id) {
  if(alg_id == e_alg_dbScan_brute_linkedListSet) {return "linkedList";}
  else if(alg_id == e_alg_dbScan_brute_sciKitLearn) {return "sci-kit";}
  else if(alg_id == e_alg_dbScan_brute_splitData_kdTree) {return "splitData-kdTree";}
  else if(alg_id == e_alg_dbScan_brute_splitData_brute_fast) {return "splitData-fast";}
  else if(alg_id == e_alg_dbScan_brute_splitData_brute_slow) {return "splitData-slow";}
  else if(alg_id == e_alg_dbScan_brute_hpCluster) {return "hpCluster";}
  else if(alg_id == e_alg_dbScan_brute_undef) {return "undef";}
  //    else if(alg_id == e_alg_dbScan_brute_) {return "";}
  else { //! then we need adding support for this.
    assert(false);
    return NULL;
  }
}

/**
   @brief comptues disjoint forests (eg, "DBSCAN" or "hp-cluster").
   @param <alg_id> is the algorithm to be sued for a brute non-optmzied coptmaution.
   @param <obj_1> is the covariance simalrity matrix.
   @param <epsilon> where scores less than eplsion is used.
   @param <minpts> the maximum number of poitns which needs to be related to the given vertex 'for it to be sued as a straverse starting point'.
   @return the list of identifed clusters.
 **/
s_kt_list_1d_uint_t computeAndReturn__alg_dbScan_brute(const e_alg_dbScan_brute_t alg_id, const s_kt_matrix_base_t *obj_1, const t_float epsilon, const uint minpts);

/**
   @brief comptues disjoint forests (eg, "DBSCAN" or "hp-cluster").
   @param <alg_id> is the algorithm to be sued for a brute non-optmzied coptmaution.
   @param <obj_1> is the covariance simalrity matrix.
   @param <obj_result> holds the resutls of the clustering
   @param <epsilon> where scores less than eplsion is used.
   @param <minpts> the maximum number of poitns which needs to be related to the given vertex 'for it to be sued as a straverse starting point'.
   @return true upon success.
 **/
bool compute__alg_dbScan_brute(const e_alg_dbScan_brute_t alg_id, const s_kt_matrix_base_t *obj_1, s_kt_clusterAlg_fixed_resultObject_t *obj_result, const t_float epsilon, const uint minpts);

#endif //! EOF: #define alg_dbScan_brute_h
