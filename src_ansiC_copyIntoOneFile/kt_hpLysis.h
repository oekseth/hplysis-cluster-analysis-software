#ifndef kt_hpLysis_h
#define kt_hpLysis_h

/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */

/**
   @file kt_hpLysis
   @brief  a fucntion-wrapper (oekseth, 06. okt. 2016)
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
   @remarks
   - refers directly to the "kt_api.h" in  our main-repo, ie, for an API-interface please refer to the "api.h" header-file.
   - for an extensive itnroduction to applications of our clustering algorithms, please see "www.knittingTools.org/clusterC.cgi"
**/

//!
//! Include the set of functions used in our approach:
#include "../src/kt_api.h"

#endif
