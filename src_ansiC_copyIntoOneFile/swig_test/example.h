typedef struct s_kt_matrix {
  float weight[10];
  int ncols;
  int current_pos;
} s_kt_matrix_t;

/* //! Initates an object */
/* static s_kt_matrix_t init__s_kt_matrix(const int ncols) { */
/*   s_kt_matrix_t self; */
/*   self.ncols = ncols; */
/*   self.current_pos = 0; */
/*   return self; */
/* } */


//! Initates an object
void init__s_kt_matrix(s_kt_matrix_t *self, const int ncols);


//! Set the value in the object
static void set_weight__s_kt_matrix(s_kt_matrix_t *self, const int index, const float value) {
  self->weight[index] = value;
  if( index > self->current_pos) {self->current_pos = index + 1;} //! ie, the max-cnt-tiems in the set.
}

