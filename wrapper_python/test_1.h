/**
   @file
   @brief
 **/


//! Global variables:
extern int My_variable;
extern double density;

#include "vector.h"
#include "particle.h"




/**
   @class Bar
   @brief Access a fixed list, and inherits from class Particle
   @remarks demonstrates how inherited functioons, and varaibles, are accessilbe through the SWIG generated interfaces.  
 **/
class Bar : public class Particle {
  int  x[16];
};

//! Take a vector as input, and return the result:
//! @remarks exxplores the effects of using a 'static' function (rather than placing the funcitoin inside the 'object' file).
static int sumitems(int *first, int nitems) {
  int i, sum = 0;
  for (i = 0; i < nitems; i++) {
    sum += first[i];
  }
  return sum;
}


/**
   @namespace mesh
   @brief explores the usage of templates
 **/
namespace mesh {
  template<class T1, class T2> struct m_pair {
    T1 first;
    T2 second;
  m_pair() : first(T1()), second(T2()) { };
  m_pair(const T1 &f, const T2 &s) : first(f), second(s) { }
  };
}

//! Instantiate some templates

