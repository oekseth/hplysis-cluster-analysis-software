
use strict;
=head
    #brief a sequence of build calls: 
=cut

#!
#! The sequence of calls.
foreach my $cmd (
    "swig -python -shadow py_example.i",
    #"gcc -c py_example_wrap.c `pkg-config --cflags --libs python` -fPIC",
    #"gcc example.c vector.c -o example.o `pkg-config --cflags --libs python` -fPIC",
    "gcc -c vector.c example.c py_example_wrap.c `pkg-config --cflags --libs python` -fPIC",


    #! Note: if the followiung warning is seen, then this could be due to the "-o" <arg> below being set to a wrong value.
    "ld -shared vector.o example.o py_example_wrap.o -o _py_example.so",
#    "ld -shared example.o py_example_wrap.o -o _py_example.so",
    # "gcc -c `perl -MConfig -e 'print join(\" \", \@Config{qw(ccflags optimize cccdlflags)}, \"-I\$Config{archlib}/CORE\")'` example.c pl_example_wrap.c",
    # "gcc `perl -MConfig -e 'print \$Config{lddlflags}'` example.o pl_example_wrap.o -o pl_example.so",
    #!
    #! Test [above] compilation: 
    "python py_tut.py"
    ) {
    printf("cmd: \"%s\"\n", $cmd);
    system($cmd);
}
