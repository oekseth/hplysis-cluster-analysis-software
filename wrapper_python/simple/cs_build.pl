
use strict;
=head
    #brief a sequence of build calls: 
=cut

#!
#! The sequence of calls.
foreach my $cmd (
    "swig -csharp cs_example.i",
    "gcc -c -fpic example.c vector.c cs_example_wrap.c",
    "gcc -shared example.o  vector.o cs_example_wrap.o   -o libcs_example.so",
    "mono-csc -out:cs_tut.exe *.cs", #! where we assuemt that the turoial code is located in "cs_tut.cs"
    #!
    #! Test [above] compilation: 
    "./cs_tut.exe"
    ) {
    printf("cmd: \"%s\"\n", $cmd);
    system($cmd);
}
