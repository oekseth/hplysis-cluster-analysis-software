use strict;

=head
    #brief a sequence of build calls: 
=cut

#!
#! The sequence of calls.
foreach my $cmd (
    "swig -perl5 -shadow pl_example.i",
    "gcc -c `perl -MConfig -e 'print join(\" \", \@Config{qw(ccflags optimize cccdlflags)}, \"-I\$Config{archlib}/CORE\")'` vector.c example.c pl_example_wrap.c",
    "gcc `perl -MConfig -e 'print \$Config{lddlflags}'` vector.o example.o pl_example_wrap.o -o pl_example.so",
    #!
    #! Test [above] compilation: 
    "perl pl_tut.pl"
    ) {
    printf("cmd: \"%s\"\n", $cmd);
    system($cmd);
}
