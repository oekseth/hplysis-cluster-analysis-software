/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

class cs_examplePINVOKE {

  protected class SWIGExceptionHelper {

    public delegate void ExceptionDelegate(string message);
    public delegate void ExceptionArgumentDelegate(string message, string paramName);

    static ExceptionDelegate applicationDelegate = new ExceptionDelegate(SetPendingApplicationException);
    static ExceptionDelegate arithmeticDelegate = new ExceptionDelegate(SetPendingArithmeticException);
    static ExceptionDelegate divideByZeroDelegate = new ExceptionDelegate(SetPendingDivideByZeroException);
    static ExceptionDelegate indexOutOfRangeDelegate = new ExceptionDelegate(SetPendingIndexOutOfRangeException);
    static ExceptionDelegate invalidCastDelegate = new ExceptionDelegate(SetPendingInvalidCastException);
    static ExceptionDelegate invalidOperationDelegate = new ExceptionDelegate(SetPendingInvalidOperationException);
    static ExceptionDelegate ioDelegate = new ExceptionDelegate(SetPendingIOException);
    static ExceptionDelegate nullReferenceDelegate = new ExceptionDelegate(SetPendingNullReferenceException);
    static ExceptionDelegate outOfMemoryDelegate = new ExceptionDelegate(SetPendingOutOfMemoryException);
    static ExceptionDelegate overflowDelegate = new ExceptionDelegate(SetPendingOverflowException);
    static ExceptionDelegate systemDelegate = new ExceptionDelegate(SetPendingSystemException);

    static ExceptionArgumentDelegate argumentDelegate = new ExceptionArgumentDelegate(SetPendingArgumentException);
    static ExceptionArgumentDelegate argumentNullDelegate = new ExceptionArgumentDelegate(SetPendingArgumentNullException);
    static ExceptionArgumentDelegate argumentOutOfRangeDelegate = new ExceptionArgumentDelegate(SetPendingArgumentOutOfRangeException);

    [DllImport("cs_example", EntryPoint="SWIGRegisterExceptionCallbacks_cs_example")]
    public static extern void SWIGRegisterExceptionCallbacks_cs_example(
                                ExceptionDelegate applicationDelegate,
                                ExceptionDelegate arithmeticDelegate,
                                ExceptionDelegate divideByZeroDelegate, 
                                ExceptionDelegate indexOutOfRangeDelegate, 
                                ExceptionDelegate invalidCastDelegate,
                                ExceptionDelegate invalidOperationDelegate,
                                ExceptionDelegate ioDelegate,
                                ExceptionDelegate nullReferenceDelegate,
                                ExceptionDelegate outOfMemoryDelegate, 
                                ExceptionDelegate overflowDelegate, 
                                ExceptionDelegate systemExceptionDelegate);

    [DllImport("cs_example", EntryPoint="SWIGRegisterExceptionArgumentCallbacks_cs_example")]
    public static extern void SWIGRegisterExceptionCallbacksArgument_cs_example(
                                ExceptionArgumentDelegate argumentDelegate,
                                ExceptionArgumentDelegate argumentNullDelegate,
                                ExceptionArgumentDelegate argumentOutOfRangeDelegate);

    static void SetPendingApplicationException(string message) {
      SWIGPendingException.Set(new System.ApplicationException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingArithmeticException(string message) {
      SWIGPendingException.Set(new System.ArithmeticException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingDivideByZeroException(string message) {
      SWIGPendingException.Set(new System.DivideByZeroException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingIndexOutOfRangeException(string message) {
      SWIGPendingException.Set(new System.IndexOutOfRangeException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingInvalidCastException(string message) {
      SWIGPendingException.Set(new System.InvalidCastException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingInvalidOperationException(string message) {
      SWIGPendingException.Set(new System.InvalidOperationException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingIOException(string message) {
      SWIGPendingException.Set(new System.IO.IOException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingNullReferenceException(string message) {
      SWIGPendingException.Set(new System.NullReferenceException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingOutOfMemoryException(string message) {
      SWIGPendingException.Set(new System.OutOfMemoryException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingOverflowException(string message) {
      SWIGPendingException.Set(new System.OverflowException(message, SWIGPendingException.Retrieve()));
    }
    static void SetPendingSystemException(string message) {
      SWIGPendingException.Set(new System.SystemException(message, SWIGPendingException.Retrieve()));
    }

    static void SetPendingArgumentException(string message, string paramName) {
      SWIGPendingException.Set(new System.ArgumentException(message, paramName, SWIGPendingException.Retrieve()));
    }
    static void SetPendingArgumentNullException(string message, string paramName) {
      Exception e = SWIGPendingException.Retrieve();
      if (e != null) message = message + " Inner Exception: " + e.Message;
      SWIGPendingException.Set(new System.ArgumentNullException(paramName, message));
    }
    static void SetPendingArgumentOutOfRangeException(string message, string paramName) {
      Exception e = SWIGPendingException.Retrieve();
      if (e != null) message = message + " Inner Exception: " + e.Message;
      SWIGPendingException.Set(new System.ArgumentOutOfRangeException(paramName, message));
    }

    static SWIGExceptionHelper() {
      SWIGRegisterExceptionCallbacks_cs_example(
                                applicationDelegate,
                                arithmeticDelegate,
                                divideByZeroDelegate,
                                indexOutOfRangeDelegate,
                                invalidCastDelegate,
                                invalidOperationDelegate,
                                ioDelegate,
                                nullReferenceDelegate,
                                outOfMemoryDelegate,
                                overflowDelegate,
                                systemDelegate);

      SWIGRegisterExceptionCallbacksArgument_cs_example(
                                argumentDelegate,
                                argumentNullDelegate,
                                argumentOutOfRangeDelegate);
    }
  }

  protected static SWIGExceptionHelper swigExceptionHelper = new SWIGExceptionHelper();

  public class SWIGPendingException {
    [ThreadStatic]
    private static Exception pendingException = null;
    private static int numExceptionsPending = 0;

    public static bool Pending {
      get {
        bool pending = false;
        if (numExceptionsPending > 0)
          if (pendingException != null)
            pending = true;
        return pending;
      } 
    }

    public static void Set(Exception e) {
      if (pendingException != null)
        throw new ApplicationException("FATAL: An earlier pending exception from unmanaged code was missed and thus not thrown (" + pendingException.ToString() + ")", e);
      pendingException = e;
      lock(typeof(cs_examplePINVOKE)) {
        numExceptionsPending++;
      }
    }

    public static Exception Retrieve() {
      Exception e = null;
      if (numExceptionsPending > 0) {
        if (pendingException != null) {
          e = pendingException;
          pendingException = null;
          lock(typeof(cs_examplePINVOKE)) {
            numExceptionsPending--;
          }
        }
      }
      return e;
    }
  }


  protected class SWIGStringHelper {

    public delegate string SWIGStringDelegate(string message);
    static SWIGStringDelegate stringDelegate = new SWIGStringDelegate(CreateString);

    [DllImport("cs_example", EntryPoint="SWIGRegisterStringCallback_cs_example")]
    public static extern void SWIGRegisterStringCallback_cs_example(SWIGStringDelegate stringDelegate);

    static string CreateString(string cString) {
      return cString;
    }

    static SWIGStringHelper() {
      SWIGRegisterStringCallback_cs_example(stringDelegate);
    }
  }

  static protected SWIGStringHelper swigStringHelper = new SWIGStringHelper();


  static cs_examplePINVOKE() {
  }


  [DllImport("cs_example", EntryPoint="CSharp_new_intArray")]
  public static extern IntPtr new_intArray(int jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_delete_intArray")]
  public static extern void delete_intArray(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_intArray_getitem")]
  public static extern int intArray_getitem(HandleRef jarg1, int jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_intArray_setitem")]
  public static extern void intArray_setitem(HandleRef jarg1, int jarg2, int jarg3);

  [DllImport("cs_example", EntryPoint="CSharp_intArray_cast")]
  public static extern IntPtr intArray_cast(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_intArray_frompointer")]
  public static extern IntPtr intArray_frompointer(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_new_doubleArray")]
  public static extern IntPtr new_doubleArray(int jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_delete_doubleArray")]
  public static extern void delete_doubleArray(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_doubleArray_getitem")]
  public static extern double doubleArray_getitem(HandleRef jarg1, int jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_doubleArray_setitem")]
  public static extern void doubleArray_setitem(HandleRef jarg1, int jarg2, double jarg3);

  [DllImport("cs_example", EntryPoint="CSharp_doubleArray_cast")]
  public static extern IntPtr doubleArray_cast(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_doubleArray_frompointer")]
  public static extern IntPtr doubleArray_frompointer(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_new_doubleP")]
  public static extern IntPtr new_doubleP();

  [DllImport("cs_example", EntryPoint="CSharp_copy_doubleP")]
  public static extern IntPtr copy_doubleP(double jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_delete_doubleP")]
  public static extern void delete_doubleP(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_doubleP_assign")]
  public static extern void doubleP_assign(HandleRef jarg1, double jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_doubleP_value")]
  public static extern double doubleP_value(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_new_intP")]
  public static extern IntPtr new_intP();

  [DllImport("cs_example", EntryPoint="CSharp_copy_intP")]
  public static extern IntPtr copy_intP(int jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_delete_intP")]
  public static extern void delete_intP(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_intP_assign")]
  public static extern void intP_assign(HandleRef jarg1, int jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_intP_value")]
  public static extern int intP_value(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_my_mod")]
  public static extern int my_mod(int jarg1, int jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_add")]
  public static extern void add(int jarg1, int jarg2, out int jarg3);

  [DllImport("cs_example", EntryPoint="CSharp_sub")]
  public static extern int sub(int jarg1, int jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_add_ref")]
  public static extern void add_ref(double jarg1, double jarg2, out double jarg3);

  [DllImport("cs_example", EntryPoint="CSharp_negate")]
  public static extern void negate(ref int jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_send_message")]
  public static extern int send_message(string jarg1, out int jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_get_dimensions")]
  public static extern void get_dimensions(int jarg1, out int jarg2, out int jarg3);

  [DllImport("cs_example", EntryPoint="CSharp_Vector_x_set")]
  public static extern void Vector_x_set(HandleRef jarg1, double jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_Vector_x_get")]
  public static extern double Vector_x_get(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_Vector_y_set")]
  public static extern void Vector_y_set(HandleRef jarg1, double jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_Vector_y_get")]
  public static extern double Vector_y_get(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_Vector_z_set")]
  public static extern void Vector_z_set(HandleRef jarg1, double jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_Vector_z_get")]
  public static extern double Vector_z_get(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_new_Vector")]
  public static extern IntPtr new_Vector();

  [DllImport("cs_example", EntryPoint="CSharp_delete_Vector")]
  public static extern void delete_Vector(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_init_Vector")]
  public static extern IntPtr init_Vector();

  [DllImport("cs_example", EntryPoint="CSharp_merge_returnByValue_Vector")]
  public static extern IntPtr merge_returnByValue_Vector(HandleRef jarg1, HandleRef jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_merge_returnByRef_Vector")]
  public static extern IntPtr merge_returnByRef_Vector(HandleRef jarg1, HandleRef jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_My_variable_set")]
  public static extern void My_variable_set(double jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_My_variable_get")]
  public static extern double My_variable_get();

  [DllImport("cs_example", EntryPoint="CSharp_fact")]
  public static extern int fact(int jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_get_time")]
  public static extern string get_time();

  [DllImport("cs_example", EntryPoint="CSharp_sumitems")]
  public static extern int sumitems(HandleRef jarg1, int jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_Bar_x_set")]
  public static extern void Bar_x_set(HandleRef jarg1, HandleRef jarg2);

  [DllImport("cs_example", EntryPoint="CSharp_Bar_x_get")]
  public static extern IntPtr Bar_x_get(HandleRef jarg1);

  [DllImport("cs_example", EntryPoint="CSharp_new_Bar")]
  public static extern IntPtr new_Bar();

  [DllImport("cs_example", EntryPoint="CSharp_delete_Bar")]
  public static extern void delete_Bar(HandleRef jarg1);
}
