#ifndef example_h
#define example_h

/* Put header files here or function declarations like below */
extern double My_variable;
extern int fact(int n);
extern int my_mod(int x, int y);
extern char *get_time();

#include "vector.h"

// #if(false)


//! Sum items using a dynamically allcoated list as input:
//! @remarks exxplores the effects of using a 'static' function (rather than placing the funcitoin inside the 'object' file).
static int sumitems(int *first, int nitems) {
  int i, sum = 0;
  for (i = 0; i < nitems; i++) {
    sum += first[i];
  }
  return sum;
}

/**
   @class Bar
   @brief Access a fixed list
   @remarks demonstrates the support for copying internal objecs, hence users are given the 'impression' that they are working with Python objects.
 **/
struct Bar {
  int  x[16];
};


//! ---------------
//! ---------------
//! ---------------
//! A fucntion to store the result in the last variable.
extern void add(int x, int y, int *result);
//! @returns a value
extern int sub(int *x, int *y);
//! Takes an argument as both input, and return value.
extern void negate(int *x);
//! Demonstrates combinioant of return value, and 'reference'.
extern int send_message(char *text, int *success);
//extern int send_message(char *text, int len, int *success);
//! Exemplfifies the use of mulitple return values.
extern void get_dimensions(int dummy1, int *rows, int *columns);
//! Turning C-references into low-level language (eg, Python or Perl).
extern void add_ref(double a, double b, double *c);

#endif
