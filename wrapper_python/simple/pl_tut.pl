use pl_example;

#! Static variables:
print "\nStatic variables:\n";
print $pl_example::My_variable,"\n";
#! Update, and then inspect:
$pl_example::My_variable = 5;
print $pl_example::My_variable, "\n";

#!
#! Functions:
print "\nFunctions:", "\n";
print pl_example::fact(5),"\n";
print pl_example::get_time(),"\n";
# FIXME: [below] procude wrong results ... are there any better strategy than 'packling' the result-structure into a result-object? <-- seems like this now works.
print "mod:", pl_example::my_mod(731,3), "\n"; #! 2
my @arr = pl_example::my_mod(7100,3);
printf("arr=[\"%s\"]\n", join("\n", @arr));



#!
#! Structs:simple:
print "\nStructs:simple:\n";
my $v = pl_example::init_Vector();
#! Update the varialbles:
$v->{x} = 3.5;
$v->{y} = 7.2;
print $v->{x}, $v->{y}, $v->{z}, "\n"; # where we exepct: 7.8 -4.5 0.0
#! Intiates to default:
$v = pl_example::init_Vector();
print $v->{x}, $v->{y}, $v->{z}, "\n";
#! Call the 'objct merge' funciton:
my $v2 = pl_example::merge_returnByRef_Vector($v, $v);
print $v2->{x}, $v2->{y}, $v2->{z}, "\n";
#! Call a permuation whcih is expected to cause a memory leak (as "v3" is 'behidn-the-scene' a new-allcoated, though untracked, memory reference). 
my $v3 = pl_example::merge_returnByValue_Vector($v, $v);
print $v3->{x}, $v3->{y}, $v3->{z}, "\n";

#!
#! Demonstrate support for dynamically allocated arrays: 
my $arr = pl_example::intArray->new(10000000);         # Array of 10-million integers
#use Data::Dumper; print Dumper($arr);
=head
print "\nArrays:allocation:\n";
# FIXME: figur out how getting [below] to work for Perl.

for(my $i = 0; $i < 10000; $i++) {        # Set some values
    $arr->[i] = $i;
}
my $sum = pl_example::sumitems($arr,10000);
print $sum, "\n";
=cut

#! Demonstrate support for shallow copying of pointers
print "\nArrays:shallow-copy:\n";
#! Note: [below] intiation of struct "Bar" describes an altrantive itnaiton strategy (when compared to how we intiated a "Vector" struct).
my $b = pl_example::Bar->new();
print $b->{x}; #! _801861a4_p_int
my $c = pl_example::Bar->new();
print $c->{x}; #! _801861a4_p_int
$c->{x} = $b->{x};   # Copy contents of b.x to c.x
print $c->{x}, "\n"; #! _801861a4_p_int
# FIXME: how may the pointer items be accessed?
#print c.x(1)
print $c->{x}, "\n";
#print $c->{x}->[1], "\n";
#print c.x(1)
# print pl_example::intP_value()
#pC = ctypes.cast( c.x, ctypes.POINTER( ctypes.c_int ))
# print c.x[0] # .__long__() #[0]
#print c.x[0].__long__() #[0]

#!
#!
#!
{
    # FIXME: why does [below] return-vale correctly 'work'?
    my $b = pl_example::sub(7,4);
    print "sub: b: $b\n";
    my $a = pl_example::add(3,4);
    print "add: $a\n";
    $a = pl_example::add_ref(3,4);
    print "add: $a\n";   
    $a = pl_example::negate(3);
    print "neg(3): $a\n";

    
    my ($bytes, $success) = pl_example::send_message("Hello World"); #, 1);
    printf("bytes=$bytes, success=$success\n");

    # FIXME: figure out why [below] does Not work    
    my $dummy = 10;
    my ($r,$c) = pl_example::get_dimensions($dummy); #, 1);
    printf("r=$r, c=$c\n");
    # my ($bytes, $success) = pl_example::send_message("Hello World");
}

