%module py_example


 %{
   /* Includes the header in the wrapper code */
    #include "vector.h"
    #include "example.h"
   %}

 /*! Add support for dyanmically allocated arrays: */
%include "carrays.i"
%array_class(int, intArray);
%array_class(double, doubleArray);

/*! Logics for accessing pointers: */
/*! url: http://www.bnikolic.co.uk/blog/cpp-swig-pointeraccess.html*/
%include "cpointer.i"
%pointer_functions(double ,doubleP)
%pointer_functions(int ,intP)

 /**
--------------
 **/
//! Turning C-references into Python
 /*
# Perl script
$a = 3.5;
$b = 7.5;
$c = 0.0;          # Output value
add_ref($a,$b,\$c);    # Place result in c (Except that it doesn't work)
 */
%typemap(in) double * (double dvalue) {
  SV* tempsv;
  if (!SvROK($input)) {
    croak("expected a reference\n");
  }
  tempsv = SvRV($input);
  if ((!SvNOK(tempsv)) && (!SvIOK(tempsv))) {
    croak("expected a double reference\n");
  }
  dvalue = SvNV(tempsv);
  $1 = &dvalue;
 }

%typemap(argout) double * {
  SV *tempsv;
  tempsv = SvRV($input);
  sv_setnv(tempsv, *$1);
 }

//! re-define some functions:
%include "typemaps.i"
int my_mod(int INPUT, int INPUT);
/*
%ignore add;
%ignore sub;
%ignore negate;
*/
void add(int, int, int *OUTPUT);
int  sub(int *INPUT, int *INPUT);
void add_ref(double INPUT, double INPUT, double *OUTPUT);

void negate(int *INOUT);
//%apply int *OUTPUT { int *success }; //! For: "send_message(..)":
//int send_message(char *text, int *success);

%apply int *OUTPUT { int *success };
int send_message(char *text, int *success);
//int send_message(char *INPUT, int INPUT, int *OUTPUT); // char *text, int *success);
//int send_message(char *INPUT, int INPUT, int *OUTPUT);
//int send_message(char *text, int *success);
//%apply int *OUTPUT { int *rows, int *columns }; //! for:
//void get_dimensions(int INPUT, int *rows, int *columns);
void get_dimensions(int INPUT, int *OUTPUT, int *OUTPUT);


/* Parse the header file to generate wrappers */
 %include "vector.h"
 %include "example.h"
