using System;
 public class runme {
      static void Main() {	
	Console.WriteLine("var={0}", cs_example.My_variable);
	cs_example.My_variable = 5; //.set(5);
	//!
	//! Accesses functions: 
	Console.WriteLine("var={0}", cs_example.My_variable);
	Console.WriteLine("fact={0}", cs_example.fact(5));
	Console.WriteLine("time={0}", cs_example.get_time());

	//! Initates a vecotr: 
	Vector o = cs_example.init_Vector();
	o.x = 5;
	Console.WriteLine("vec={0},{1},{2}", o.x, o.y, o.z);
	Vector m = cs_example.merge_returnByValue_Vector(o, o);
	Console.WriteLine("vec(after-add--byVal)={0},{1},{2}", m.x, m.y, m.z);
	//! 
	m = cs_example.merge_returnByRef_Vector(o, o);
	Console.WriteLine("vec(after-add--byRef)={0},{1},{2}", m.x, m.y, m.z);
	//cs_example.Vector vec;
	{
	  int res;
	  //! 
	  cs_example.add(1, 2, out res);
	  Console.WriteLine("add(1+2)={0}", res);
	  //! 
	  double res_d;
	  cs_example.add_ref(1.0, 2.0, out res_d);
	  Console.WriteLine("add_ref(1+2)={0}", res_d);
	  //! 
	  res = cs_example.sub(1, 2);
	  Console.WriteLine("sub(1+2)={0}", res);
	  //! 
	  res = 2; cs_example.negate(ref res);
	  Console.WriteLine("negate(2)={0}", res);
	  //!
	  //! 
	  { //! Explore C-list access--and--usage:
	    int size = 10;
	    intArray c = new intArray(size); //! where latter is sedefined in our "cs_example.i"	    
	    for (int i=0; i<size; i++)
	      c.setitem(i, 2*i);                    // Assign values
	    int sum = cs_example.sumitems(c.cast(), size);          // Pass to C
	    Console.WriteLine("sum({0})={1}", size, sum);
	  }
	}
      }
}
