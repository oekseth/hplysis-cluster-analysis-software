use strict;

=head
    #brief a sequence of build calls: 
    #remarks
    -- for documentation details, see http://www.swig.org/Doc1.3/Java.html
=cut

#!
#! The sequence of calls.
foreach my $cmd (
    "swig -java java_example.i", #! where "shadow" is Not used as it si implctly part of the call. 
    "gcc -c example.c java_example_wrap.c -I/usr/lib/jvm/java-9-openjdk-amd64/include/linux/ -I/usr/lib/jvm/java-9-openjdk-amd64/include/ -fPIC",
    "gcc -shared example.o  java_example_wrap.o   -o example.dll",
    #!
    #! Test [above] compilation: 
    "javac java_example_tut.java",
    "java java_example_tut"
    # FIXME: export LD_LIBRARY_PATH=. 
    ) {
    printf("cmd: \"%s\"\n", $cmd);
    system($cmd);
}
