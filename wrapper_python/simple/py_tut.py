import py_example

#! Static variables:
print "\nStatic variables:"
print py_example.cvar.My_variable
#! Update, and then inspect:
py_example.cvar.My_variable = 5
print py_example.cvar.My_variable

#!
#! Functions:
print "\nFunctions:"
print py_example.fact(5) #! 120
print py_example.my_mod(7,3) #! 1
print py_example.get_time() #! (today).

#!
#! Structs:simple:
print "\nStructs:simple:"
v = py_example.Vector()
#! Update the varialbles:
v.x = 3.5
v.y = 7.2
print v.x, v.y, v.z # where we exepct: 7.8 -4.5 0.0
#! Intiates to default:
v = py_example.init_Vector()
print v.x, v.y, v.z
#! Call the 'objct merge' funciton:
v2 = py_example.merge_returnByRef_Vector(v, v)
print v2.x, v2.y, v2.z
#! Call a permuation whcih is expected to cause a memory leak (as "v3" is 'behidn-the-scene' a new-allcoated, though untracked, memory reference). 
v3 = py_example.merge_returnByValue_Vector(v, v)
print v3.x, v3.y, v3.z

#!
#! Demonstrate support for dynamically allocated arrays: 
print "\nArrays:allocation:"
a = py_example.intArray(10000000)         # Array of 10-million integers
for i in xrange(10000):        # Set some values
    a[i] = i
sum = py_example.sumitems(a,10000)
print sum

#!
#! Demonstrate function-rewriting by SWIGs "*.i" file.
print "\nFunction re-writing:"
b = py_example.sub(7,4)
print "sub: b: ", b
a = py_example.add(3,4)
print "add: ", a
a = py_example.add_ref(3,4)
print "add: ", a
a = py_example.negate(3)
print "neg(3): ", a
bytes, success = py_example.send_message("Hello World") #, 1);
print "bytes=", bytes, " , success=", success
# FIXME: figure out why [below] does Not work
dummy = 10
r,c = py_example.get_dimensions(dummy) #, 1);
print "r=", r, ", c=", c


#! Demonstrate support for shallow copying of pointers
print "\nArrays:shallow-copy:"
b = py_example.Bar()
print b.x #! _801861a4_p_int
c = py_example.Bar()
print c.x #! _801861a4_p_int
c.x = b.x             # Copy contents of b.x to c.x
print c.x #! _801861a4_p_int
# FIXME: how may the pointer items be accessed?
#print c.x(1)
print c.x[1]
#print c.x(1)
# print py_example.intP_value()
#pC = ctypes.cast( c.x, ctypes.POINTER( ctypes.c_int ))
# print c.x[0] # .__long__() #[0]
#print c.x[0].__long__() #[0]
