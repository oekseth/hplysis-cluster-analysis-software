// import java_example;
import java.util.Properties;
import java.util.Enumeration;


public class java_example_tut {
    public static void main(String argv[]) {
	Properties p = System.getProperties();
	Enumeration keys = p.keys();
	while (keys.hasMoreElements()) {
	    String key = (String)keys.nextElement();
	    String value = (String)p.get(key);
	    System.out.println(key + " : " + value);
	}
	//System.loadLibrary("java_example");
	//System.out.println(java_example.getMy_variable());
	// System.out.println(java_example.fact(5));
	// System.out.println(java_example.get_time());
    }
}
