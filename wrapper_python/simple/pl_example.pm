# This file was automatically generated by SWIG (http://www.swig.org).
# Version 2.0.12
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

package pl_example;
use base qw(Exporter);
use base qw(DynaLoader);
package pl_examplec;
bootstrap pl_example;
package pl_example;
@EXPORT = qw();

# ---------- BASE METHODS -------------

package pl_example;

sub TIEHASH {
    my ($classname,$obj) = @_;
    return bless $obj, $classname;
}

sub CLEAR { }

sub FIRSTKEY { }

sub NEXTKEY { }

sub FETCH {
    my ($self,$field) = @_;
    my $member_func = "swig_${field}_get";
    $self->$member_func();
}

sub STORE {
    my ($self,$field,$newval) = @_;
    my $member_func = "swig_${field}_set";
    $self->$member_func($newval);
}

sub this {
    my $ptr = shift;
    return tied(%$ptr);
}


# ------- FUNCTION WRAPPERS --------

package pl_example;

*new_doubleP = *pl_examplec::new_doubleP;
*copy_doubleP = *pl_examplec::copy_doubleP;
*delete_doubleP = *pl_examplec::delete_doubleP;
*doubleP_assign = *pl_examplec::doubleP_assign;
*doubleP_value = *pl_examplec::doubleP_value;
*new_intP = *pl_examplec::new_intP;
*copy_intP = *pl_examplec::copy_intP;
*delete_intP = *pl_examplec::delete_intP;
*intP_assign = *pl_examplec::intP_assign;
*intP_value = *pl_examplec::intP_value;
*my_mod = *pl_examplec::my_mod;
*add = *pl_examplec::add;
*sub = *pl_examplec::sub;
*add_ref = *pl_examplec::add_ref;
*negate = *pl_examplec::negate;
*send_message = *pl_examplec::send_message;
*get_dimensions = *pl_examplec::get_dimensions;
*init_Vector = *pl_examplec::init_Vector;
*merge_returnByValue_Vector = *pl_examplec::merge_returnByValue_Vector;
*merge_returnByRef_Vector = *pl_examplec::merge_returnByRef_Vector;
*fact = *pl_examplec::fact;
*get_time = *pl_examplec::get_time;
*sumitems = *pl_examplec::sumitems;

############# Class : pl_example::intArray ##############

package pl_example::intArray;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_example );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = pl_examplec::new_intArray(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_examplec::delete_intArray($self);
        delete $OWNER{$self};
    }
}

*getitem = *pl_examplec::intArray_getitem;
*setitem = *pl_examplec::intArray_setitem;
*cast = *pl_examplec::intArray_cast;
*frompointer = *pl_examplec::intArray_frompointer;
sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_example::doubleArray ##############

package pl_example::doubleArray;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_example );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = pl_examplec::new_doubleArray(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_examplec::delete_doubleArray($self);
        delete $OWNER{$self};
    }
}

*getitem = *pl_examplec::doubleArray_getitem;
*setitem = *pl_examplec::doubleArray_setitem;
*cast = *pl_examplec::doubleArray_cast;
*frompointer = *pl_examplec::doubleArray_frompointer;
sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_example::Vector ##############

package pl_example::Vector;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_example );
%OWNER = ();
%ITERATORS = ();
*swig_x_get = *pl_examplec::Vector_x_get;
*swig_x_set = *pl_examplec::Vector_x_set;
*swig_y_get = *pl_examplec::Vector_y_get;
*swig_y_set = *pl_examplec::Vector_y_set;
*swig_z_get = *pl_examplec::Vector_z_get;
*swig_z_set = *pl_examplec::Vector_z_set;
sub new {
    my $pkg = shift;
    my $self = pl_examplec::new_Vector(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_examplec::delete_Vector($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_example::Bar ##############

package pl_example::Bar;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_example );
%OWNER = ();
%ITERATORS = ();
*swig_x_get = *pl_examplec::Bar_x_get;
*swig_x_set = *pl_examplec::Bar_x_set;
sub new {
    my $pkg = shift;
    my $self = pl_examplec::new_Bar(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_examplec::delete_Bar($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


# ------- VARIABLE STUBS --------

package pl_example;

*My_variable = *pl_examplec::My_variable;
1;
