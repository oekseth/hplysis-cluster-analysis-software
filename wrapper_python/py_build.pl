
use strict;
=head
    #brief a sequence of build calls: 
=cut

#!
#! The sequence of calls.
foreach my $cmd (
#    "swig -c++ -python -shadow py_example.i",
    "swig -c++ -python -shadow py_mesh.i",
    "g++ -c vector.cxx particle.cxx mesh.cxx  py_mesh_wrap.cxx `pkg-config --cflags --libs python` -fPIC",
    "g++ -shared vector.o particle.o mesh.o py_mesh_wrap.o -o _py_mesh.so",

    #! Test [above] compilation: 
    "python py_tut.py"
    ) {
    printf("cmd: \"%s\"\n", $cmd);
    system($cmd);
}
