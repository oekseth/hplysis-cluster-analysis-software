//%module py_mesh
 %module py_example


 %{
   /* Includes the header in the wrapper code */
      #include "vector.h"
    #include "example.h"
   %}

 /*! Add support for dyanmically allocated arrays: */
%include "carrays.i"
%array_class(int, intArray);
%array_class(double, doubleArray);

/*! Logics for accessing pointers: */
/*! url: http://www.bnikolic.co.uk/blog/cpp-swig-pointeraccess.html*/
%include "cpointer.i"
%pointer_functions(double ,doubleP)
%pointer_functions(int ,intP)


/* Parse the header file to generate wrappers */
 %include "vector.h"
 %include "example.h"
