#! @return an itnail config-export for our function (oekseth, 06. mar. 2017).
def config():
    return {'ncluster' => 0, 
            #'stringOf_enum' => None,
            'stringOf_exportFile' => None,
            'stringOf_exportFile__format' => None
            }
"""
   @brief converts a 'high-level' matrix into the hpLysis "s_kt_matrix_t" C/C++ matrix-object (oekseth, 06. mar. 2017).
   @return an internal matrix based on a a 'language-specific' matrix.
   @remarks 
   -- howto: for an example wrt. different use-case-apttersn pelase see our associated tutoirals: if latter does Not help, then we suggest contacting senior developer [oekseth@gmail.com]
   -- application: interfaces the hpLysis high-performance cluster-analsysis-library
   -- sampleInput: matrix = [[12,7], [4 ,5], [3 ,8]]
"""
def insertMatrix__intoInternal(matrix):
    #!
    #! Find the min-max-diemsions:
    max_row_id = range(len(matrix))
    max_col_id = 0
    for i in range(len(matrix)):
        cnt_cols = range(len(matrix[0]))
        if(cnt_cols > max_col_id):
            max_col_id = cnt_cols;
    max_col_id++;
    #!
    #! Intiate "s_kt_matrix_t" object: 
    # FIXME: update [below].
    mat_hpLysis = None
    #!
    #! Insert the values into "s_kt_matrix_t":
    for i in range(len(matrix)):
        # iterate through columns
        for j in range(len(matrix[0])):
            score = matrix[i][j]
            if(score != None):
                print "FIXME: # FIXME: add soemthing"
            else:
                print "FIXME: # FIXME: add soemthing"
    #!
    #! @return
    return mat_hpLysis

"""
    @brief the main funciton of thos hpLysis API (oekseth, 06. mar. 2017).
    @param <stringOf_enum> is the lgocial oepraiton to apply
    @param <matrix_input_1> the first input-matrix.
    @param <matrix_input_2> optional: the second input-matrix
    @param <optional__config> optional: if used follow tempatel (or call) our "config()" function.
    @return the new-constructed data-set.
"""
def compute_matrix(stringOf_enum, matrix_input_1, matrix_input_2, optional__config = config()) :
    if(stringOf_enum == None):
        print "!!\t Input-argument: stringOf_enum Not given"
    if(matrix_input_1 == None):
        print "!!\t Input-argument: matrix_input_1 Not given"
    
    #! Convert 'high-levle' amtrix into n internal matrix: 
    mat_1 = insertMatrix__intoInternal(matrix_input_1)
    if(mat_1 != None):
        mat_2 = insertMatrix__intoInternal(matrix_input_2)
        if(mat_2 != None):
            # FIXME: call:
            print "FIXME"

	    # FIXME: de-allcoate [ªbove]
        else:
            # FIXME: call:
            print "FIXME"

	# FIXME: iterate through hte return-matrix and 'insert' into 'our' matrix.
        matrix_result = []
	# FIXME: de-allcoate [ªbove]
	
	#! @return the result-matrix
	#! Note: a 'amtrix' is sued to simplify the 'specifciaiton' fo different result-varialbes, ie, a 'genreliased approch'.
	return {
	    matrix => matrix_result,
	    };
    else:
        print "!!\t Input-argument: was Not able to find meaningful valeus in matrix_input_1: please investigate the examples we provide"
        return None
    
