# This file was automatically generated by SWIG (http://www.swig.org).
# Version 2.0.12
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

package pl_mesh;
use base qw(Exporter);
use base qw(DynaLoader);
package pl_meshc;
bootstrap pl_mesh;
package pl_mesh;
@EXPORT = qw();

# ---------- BASE METHODS -------------

package pl_mesh;

sub TIEHASH {
    my ($classname,$obj) = @_;
    return bless $obj, $classname;
}

sub CLEAR { }

sub FIRSTKEY { }

sub NEXTKEY { }

sub FETCH {
    my ($self,$field) = @_;
    my $member_func = "swig_${field}_get";
    $self->$member_func();
}

sub STORE {
    my ($self,$field,$newval) = @_;
    my $member_func = "swig_${field}_set";
    $self->$member_func($newval);
}

sub this {
    my $ptr = shift;
    return tied(%$ptr);
}


# ------- FUNCTION WRAPPERS --------

package pl_mesh;

*new_doubleP = *pl_meshc::new_doubleP;
*copy_doubleP = *pl_meshc::copy_doubleP;
*delete_doubleP = *pl_meshc::delete_doubleP;
*doubleP_assign = *pl_meshc::doubleP_assign;
*doubleP_value = *pl_meshc::doubleP_value;
*new_intP = *pl_meshc::new_intP;
*copy_intP = *pl_meshc::copy_intP;
*delete_intP = *pl_meshc::delete_intP;
*intP_assign = *pl_meshc::intP_assign;
*intP_value = *pl_meshc::intP_value;
*init_Vector = *pl_meshc::init_Vector;
*merge_returnByValue_Vector = *pl_meshc::merge_returnByValue_Vector;
*merge_returnByRef_Vector = *pl_meshc::merge_returnByRef_Vector;
*fact = *pl_meshc::fact;
*my_mod = *pl_meshc::my_mod;
*get_time = *pl_meshc::get_time;
*sumitems = *pl_meshc::sumitems;

############# Class : pl_mesh::intArray ##############

package pl_mesh::intArray;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = pl_meshc::new_intArray(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_meshc::delete_intArray($self);
        delete $OWNER{$self};
    }
}

*getitem = *pl_meshc::intArray_getitem;
*setitem = *pl_meshc::intArray_setitem;
*cast = *pl_meshc::intArray_cast;
*frompointer = *pl_meshc::intArray_frompointer;
sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_mesh::doubleArray ##############

package pl_mesh::doubleArray;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = pl_meshc::new_doubleArray(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_meshc::delete_doubleArray($self);
        delete $OWNER{$self};
    }
}

*getitem = *pl_meshc::doubleArray_getitem;
*setitem = *pl_meshc::doubleArray_setitem;
*cast = *pl_meshc::doubleArray_cast;
*frompointer = *pl_meshc::doubleArray_frompointer;
sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_mesh::Vector ##############

package pl_mesh::Vector;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_mesh );
%OWNER = ();
%ITERATORS = ();
*swig_x_get = *pl_meshc::Vector_x_get;
*swig_x_set = *pl_meshc::Vector_x_set;
*swig_y_get = *pl_meshc::Vector_y_get;
*swig_y_set = *pl_meshc::Vector_y_set;
*swig_z_get = *pl_meshc::Vector_z_get;
*swig_z_set = *pl_meshc::Vector_z_set;
sub new {
    my $pkg = shift;
    my $self = pl_meshc::new_Vector(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_meshc::delete_Vector($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_mesh::Particle ##############

package pl_mesh::Particle;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = pl_meshc::new_Particle(@_);
    bless $self, $pkg if defined($self);
}

*get_type = *pl_meshc::Particle_get_type;
*swig_type_get = *pl_meshc::Particle_type_get;
*swig_type_set = *pl_meshc::Particle_type_set;
*swig_r_get = *pl_meshc::Particle_r_get;
*swig_r_set = *pl_meshc::Particle_r_set;
*swig_v_get = *pl_meshc::Particle_v_get;
*swig_v_set = *pl_meshc::Particle_v_set;
*swig_f_get = *pl_meshc::Particle_f_get;
*swig_f_set = *pl_meshc::Particle_f_set;
sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_meshc::delete_Particle($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_mesh::Bar ##############

package pl_mesh::Bar;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = pl_meshc::new_Bar(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_meshc::delete_Bar($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_mesh::MyData ##############

package pl_mesh::MyData;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = pl_meshc::new_MyData(@_);
    bless $self, $pkg if defined($self);
}

*__call__ = *pl_meshc::MyData___call__;
sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_meshc::delete_MyData($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : pl_mesh::mesh_ii ##############

package pl_mesh::mesh_ii;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( pl_mesh );
%OWNER = ();
%ITERATORS = ();
*swig_first_get = *pl_meshc::mesh_ii_first_get;
*swig_first_set = *pl_meshc::mesh_ii_first_set;
*swig_second_get = *pl_meshc::mesh_ii_second_get;
*swig_second_set = *pl_meshc::mesh_ii_second_set;
sub new {
    my $pkg = shift;
    my $self = pl_meshc::new_mesh_ii(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        pl_meshc::delete_mesh_ii($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


# ------- VARIABLE STUBS --------

package pl_mesh;

*My_variable = *pl_meshc::My_variable;
*density = *pl_meshc::density;
1;
