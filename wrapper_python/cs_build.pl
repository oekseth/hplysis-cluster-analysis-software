
use strict;
=head
    #brief a sequence of build calls: 
=cut

#!
#! The sequence of calls.
foreach my $cmd (
    "swig -c++ -csharp cs_mesh.i",
    "g++ -c -fPIC vector.cxx particle.cxx mesh.cxx cs_mesh_wrap.cxx",
    "g++ -shared vector.o particle.o  mesh.o cs_mesh_wrap.o   -o libcs_mesh.so",
    "mono-csc -out:cs_tut.exe *.cs", #! where we assuemt that the turoial code is located in "cs_tut.cs"
    #!
    #! Test [above] compilation: 
    "./cs_tut.exe"
    ) {
    printf("cmd: \"%s\"\n", $cmd);
    system($cmd);
}
