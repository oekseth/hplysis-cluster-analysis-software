use strict;

=head
    #brief a sequence of build calls: 
=cut

if(1 == 2)
{    
#!
#! The sequence of calls.
foreach my $cmd (
    "swig -c++ -perl5 -shadow example.i",
    #"swig -c++ -perl5  pl_mesh.i",
    "g++ -c `perl -MConfig -e 'print join(\" \", \@Config{qw(ccflags optimize cccdlflags)}, \"-I\$Config{archlib}/CORE\")'` vector.cxx particle.cxx mesh.cxx example_wrap.cxx -fPIC",
#    "g++ -c `perl -MConfig -e 'print join(\" \", \@Config{qw(ccflags optimize cccdlflags)}, \"-I\$Config{archlib}/CORE\")'` example.cpp vector.cxx particle.cxx mesh.cxx example_wrap.cxx -fPIC",
    "g++ -shared vector.o particle.o mesh.o example_wrap.o -o p_mesh.so `perl -MConfig -e 'print \$Config{lddlflags}'` ",
#    "g++ -shared vector.o particle.o mesh.o example_wrap.o -o example.so `perl -MConfig -e 'print \$Config{lddlflags}'` ",
#    "g++ -shared example.o vector.o particle.o mesh.o example_wrap.o -o example.so `perl -MConfig -e 'print \$Config{lddlflags}'` ",
    #!
    #! Test [above] compilation: 
    "perl pl_tut.pl"
    ) {
    printf("cmd: \"%s\"\n", $cmd);
    system($cmd);
}
}

#if(1 == 2)
{    
#!
#! The sequence of calls.
foreach my $cmd (
    "swig -c++ -perl5 -shadow pl_mesh.i",
    #"swig -c++ -perl5  pl_mesh.i",
    "g++ -c `perl -MConfig -e 'print join(\" \", \@Config{qw(ccflags optimize cccdlflags)}, \"-I\$Config{archlib}/CORE\")'` vector.cxx particle.cxx mesh.cxx pl_mesh_wrap.cxx -fPIC",
    "g++ -shared vector.o particle.o mesh.o pl_mesh_wrap.o -o pl_mesh.so `perl -MConfig -e 'print \$Config{lddlflags}'` ",
    #!
    #! Test [above] compilation: 
    "perl pl_tut.pl"
    ) {
    printf("cmd: \"%s\"\n", $cmd);
    system($cmd);
}
}
