# This file was automatically generated by SWIG (http://www.swig.org).
# Version 2.0.12
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

package py_mesh;
use base qw(Exporter);
use base qw(DynaLoader);
package py_meshc;
bootstrap py_mesh;
package py_mesh;
@EXPORT = qw();

# ---------- BASE METHODS -------------

package py_mesh;

sub TIEHASH {
    my ($classname,$obj) = @_;
    return bless $obj, $classname;
}

sub CLEAR { }

sub FIRSTKEY { }

sub NEXTKEY { }

sub FETCH {
    my ($self,$field) = @_;
    my $member_func = "swig_${field}_get";
    $self->$member_func();
}

sub STORE {
    my ($self,$field,$newval) = @_;
    my $member_func = "swig_${field}_set";
    $self->$member_func($newval);
}

sub this {
    my $ptr = shift;
    return tied(%$ptr);
}


# ------- FUNCTION WRAPPERS --------

package py_mesh;

*init_Vector = *py_meshc::init_Vector;
*merge_returnByValue_Vector = *py_meshc::merge_returnByValue_Vector;
*merge_returnByRef_Vector = *py_meshc::merge_returnByRef_Vector;
*fact = *py_meshc::fact;
*my_mod = *py_meshc::my_mod;
*get_time = *py_meshc::get_time;
*sumitems = *py_meshc::sumitems;

############# Class : py_mesh::intArray ##############

package py_mesh::intArray;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( py_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = py_meshc::new_intArray(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        py_meshc::delete_intArray($self);
        delete $OWNER{$self};
    }
}

*getitem = *py_meshc::intArray_getitem;
*setitem = *py_meshc::intArray_setitem;
*cast = *py_meshc::intArray_cast;
*frompointer = *py_meshc::intArray_frompointer;
sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : py_mesh::doubleArray ##############

package py_mesh::doubleArray;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( py_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = py_meshc::new_doubleArray(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        py_meshc::delete_doubleArray($self);
        delete $OWNER{$self};
    }
}

*getitem = *py_meshc::doubleArray_getitem;
*setitem = *py_meshc::doubleArray_setitem;
*cast = *py_meshc::doubleArray_cast;
*frompointer = *py_meshc::doubleArray_frompointer;
sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : py_mesh::Vector ##############

package py_mesh::Vector;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( py_mesh );
%OWNER = ();
%ITERATORS = ();
*swig_x_get = *py_meshc::Vector_x_get;
*swig_x_set = *py_meshc::Vector_x_set;
*swig_y_get = *py_meshc::Vector_y_get;
*swig_y_set = *py_meshc::Vector_y_set;
*swig_z_get = *py_meshc::Vector_z_get;
*swig_z_set = *py_meshc::Vector_z_set;
sub new {
    my $pkg = shift;
    my $self = py_meshc::new_Vector(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        py_meshc::delete_Vector($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : py_mesh::Particle ##############

package py_mesh::Particle;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( py_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = py_meshc::new_Particle(@_);
    bless $self, $pkg if defined($self);
}

*get_type = *py_meshc::Particle_get_type;
*swig_type_get = *py_meshc::Particle_type_get;
*swig_type_set = *py_meshc::Particle_type_set;
*swig_r_get = *py_meshc::Particle_r_get;
*swig_r_set = *py_meshc::Particle_r_set;
*swig_v_get = *py_meshc::Particle_v_get;
*swig_v_set = *py_meshc::Particle_v_set;
*swig_f_get = *py_meshc::Particle_f_get;
*swig_f_set = *py_meshc::Particle_f_set;
sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        py_meshc::delete_Particle($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : py_mesh::Bar ##############

package py_mesh::Bar;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( py_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = py_meshc::new_Bar(@_);
    bless $self, $pkg if defined($self);
}

sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        py_meshc::delete_Bar($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


############# Class : py_mesh::MyData ##############

package py_mesh::MyData;
use vars qw(@ISA %OWNER %ITERATORS %BLESSEDMEMBERS);
@ISA = qw( py_mesh );
%OWNER = ();
%ITERATORS = ();
sub new {
    my $pkg = shift;
    my $self = py_meshc::new_MyData(@_);
    bless $self, $pkg if defined($self);
}

*__call__ = *py_meshc::MyData___call__;
sub DESTROY {
    return unless $_[0]->isa('HASH');
    my $self = tied(%{$_[0]});
    return unless defined $self;
    delete $ITERATORS{$self};
    if (exists $OWNER{$self}) {
        py_meshc::delete_MyData($self);
        delete $OWNER{$self};
    }
}

sub DISOWN {
    my $self = shift;
    my $ptr = tied(%$self);
    delete $OWNER{$ptr};
}

sub ACQUIRE {
    my $self = shift;
    my $ptr = tied(%$self);
    $OWNER{$ptr} = 1;
}


# ------- VARIABLE STUBS --------

package py_mesh;

*My_variable = *py_meshc::My_variable;
*density = *py_meshc::density;
1;
