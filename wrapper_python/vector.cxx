#include "vector.h"


//! Merge two vectors, and return by value:
//! @remarks this result is expected to result in a meory leak, as Python 'behdin-the-scene' (through SWIG generated wrappers) allocates an "Vector *" object,
struct Vector merge_returnByValue_Vector(struct Vector v1, struct Vector v2) {
  //! Note: below is a simplficaiton, ie, where w drop testing for 'emptyness'.
  struct Vector merged = {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
  return merged;
}

//! Merge two vectors, and return by reference
struct Vector *merge_returnByRef_Vector(struct Vector v1, struct Vector v2) {
  struct Vector *merged = new Vector;
  assert(merged);
  //! Note: below is a simplficaiton, ie, where w drop testing for 'emptyness'.
  *merged = merge_returnByValue_Vector(v1, v2);
  return merged;
}


