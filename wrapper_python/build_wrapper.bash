rm -f hpLysis_subset.py;
#swig -includeall -importall -python -I../src/  hpLysis_subset.i 
swig -python -I../src/  hpLysis_subset.i;
# --
g++ -I../src/ -L../src/ -l liblib_x_hpLysis.so -c `pkg-config --cflags --libs python` -pthread -Wall -fPIC  -fopenmp -mssse3 -Wall -fopenmp -mssse3 -Wno-unused-variable -Wno-unused-value -fpermissive -fopenmp -Wno-unused-but-set-variable -Wno-address -Wno-unused-function hpLysis_subset_wrap.c;

COMPILER_PARAMS="-pthread -Wall -fPIC  -fopenmp -mssse3 -Wall -fopenmp -mssse3 -Wno-unused-variable -Wno-unused-value -fpermissive -fopenmp -Wno-unused-but-set-variable -Wno-address -Wno-unused-function";
#! Note: for a brief introduciton to compiler-params, see "http://tldp.org/LDP/abs/html/varassignment.html".
LOCAL_FILESET=$(cat ../wrapper_perl/compile_depend.tsv) #! where latter is generated using our "perl aux_buildListOf_compilerDependencies.pl"
#g++ -shared $COMPILER_PARAMS    $LOCAL_FILESET   -o _hpLysis_subset.so;
g++ -shared $COMPILER_PARAMS   hpLysis_subset_wrap.o  $LOCAL_FILESET  -o _hpLysis_subset.so;
#g++ -shared $COMPILER_PARAMS  `perl -MConfig -e 'print $Config{lddlflags}'`  $LOCAL_FILESET  hpLysis_subset_wrap.o -o _hpLysis_subset.so;

