/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class cs_mesh {
  public static SWIGTYPE_p_double new_doubleP() {
    IntPtr cPtr = cs_meshPINVOKE.new_doubleP();
    SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
    return ret;
  }

  public static SWIGTYPE_p_double copy_doubleP(double value) {
    IntPtr cPtr = cs_meshPINVOKE.copy_doubleP(value);
    SWIGTYPE_p_double ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_double(cPtr, false);
    return ret;
  }

  public static void delete_doubleP(SWIGTYPE_p_double obj) {
    cs_meshPINVOKE.delete_doubleP(SWIGTYPE_p_double.getCPtr(obj));
  }

  public static void doubleP_assign(SWIGTYPE_p_double obj, double value) {
    cs_meshPINVOKE.doubleP_assign(SWIGTYPE_p_double.getCPtr(obj), value);
  }

  public static double doubleP_value(SWIGTYPE_p_double obj) {
    double ret = cs_meshPINVOKE.doubleP_value(SWIGTYPE_p_double.getCPtr(obj));
    return ret;
  }

  public static SWIGTYPE_p_int new_intP() {
    IntPtr cPtr = cs_meshPINVOKE.new_intP();
    SWIGTYPE_p_int ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_int(cPtr, false);
    return ret;
  }

  public static SWIGTYPE_p_int copy_intP(int value) {
    IntPtr cPtr = cs_meshPINVOKE.copy_intP(value);
    SWIGTYPE_p_int ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_int(cPtr, false);
    return ret;
  }

  public static void delete_intP(SWIGTYPE_p_int obj) {
    cs_meshPINVOKE.delete_intP(SWIGTYPE_p_int.getCPtr(obj));
  }

  public static void intP_assign(SWIGTYPE_p_int obj, int value) {
    cs_meshPINVOKE.intP_assign(SWIGTYPE_p_int.getCPtr(obj), value);
  }

  public static int intP_value(SWIGTYPE_p_int obj) {
    int ret = cs_meshPINVOKE.intP_value(SWIGTYPE_p_int.getCPtr(obj));
    return ret;
  }

  public static Vector init_Vector() {
    Vector ret = new Vector(cs_meshPINVOKE.init_Vector(), true);
    return ret;
  }

  public static Vector merge_returnByValue_Vector(Vector v1, Vector v2) {
    Vector ret = new Vector(cs_meshPINVOKE.merge_returnByValue_Vector(Vector.getCPtr(v1), Vector.getCPtr(v2)), true);
    if (cs_meshPINVOKE.SWIGPendingException.Pending) throw cs_meshPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public static Vector merge_returnByRef_Vector(Vector v1, Vector v2) {
    IntPtr cPtr = cs_meshPINVOKE.merge_returnByRef_Vector(Vector.getCPtr(v1), Vector.getCPtr(v2));
    Vector ret = (cPtr == IntPtr.Zero) ? null : new Vector(cPtr, false);
    if (cs_meshPINVOKE.SWIGPendingException.Pending) throw cs_meshPINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public static int My_variable {
    set {
      cs_meshPINVOKE.My_variable_set(value);
    } 
    get {
      int ret = cs_meshPINVOKE.My_variable_get();
      return ret;
    } 
  }

  public static double density {
    set {
      cs_meshPINVOKE.density_set(value);
    } 
    get {
      double ret = cs_meshPINVOKE.density_get();
      return ret;
    } 
  }

  public static int fact(int n) {
    int ret = cs_meshPINVOKE.fact(n);
    return ret;
  }

  public static int my_mod(int x, int y) {
    int ret = cs_meshPINVOKE.my_mod(x, y);
    return ret;
  }

  public static string get_time() {
    string ret = cs_meshPINVOKE.get_time();
    return ret;
  }

  public static int sumitems(SWIGTYPE_p_int first, int nitems) {
    int ret = cs_meshPINVOKE.sumitems(SWIGTYPE_p_int.getCPtr(first), nitems);
    return ret;
  }

}
