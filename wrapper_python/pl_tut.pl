use strict;

=head
use example;
print "var:", $example::My_variable, "\n";
=cut

=head
use p_mesh;
print "var:", $p_mesh::My_variable, "\n";
=cut

#=head
use pl_mesh;
#! Static variables:
print "\nStatic variables:\n";
print "var:", $pl_mesh::My_variable, "\n";
$py_mesh::My_variable = 5;
print "var:", $pl_mesh::My_variable, "\n";

#!
#! Functions:
print "\nFunctions:", "\n";
print pl_mesh::fact(5),"\n";
print pl_mesh::my_mod(7,3),"\n"; #! 1
print pl_mesh::get_time(),"\n"; #! (today).

#!
#! Structs:simple:
print "\nStructs:simple:\n";
my $v = pl_mesh::init_Vector();
#! Update the varialbles:
$v->{x} = 3.5;
$v->{y} = 7.2;
print $v->{x}, $v->{y}, $v->{z}, "\n"; # where we exepct: 7.8 -4.5 0.0
#! Intiates to default:
$v = pl_mesh::init_Vector();
print $v->{x}, $v->{y}, $v->{z}, "\n";
#! Call the 'objct merge' funciton:
my $v2 = pl_mesh::merge_returnByRef_Vector($v, $v);
print $v2->{x}, $v2->{y}, $v2->{z}, "\n";
#! Call a permuation whcih is expected to cause a memory leak (as "v3" is 'behidn-the-scene' a new-allcoated, though untracked, memory reference). 
my $v3 = pl_mesh::merge_returnByValue_Vector($v, $v);
print $v3->{x}, $v3->{y}, $v3->{z}, "\n";


#! Exemplifies the access to namespaces, and templates:
print "\nnamespace--templates:\n";
#p1 = mesh.m_pair(3,4)
my $p1 = pl_mesh::mesh_ii->new(3,4);
#p1 = mesh.mesh_ii(3,4)
#p1 = py_mesh.mesh.mesh_ii(3,4)
print "init:", $p1->{first}, ", ", $p1->{second}, "\n";
$p1->{first} = 1;
$p1->{second} = -1;
print "set to (1,-1): ", $p1->{first}, ", ", $p1->{second}, "\n";
