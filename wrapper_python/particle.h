#ifndef particle_h
#define particle_h
/**
   @file particle.h
   @brief 
   -- motivation: exemplifies the use of structs (or: classes) inside a different class. 
   -- compilation: g++ -c -Wall -Werror -fpic -Wno-unused-function particle.cxx
 **/
#include "vector.h"

/**
   @class Particle
   @brief bais class based functions for data access
   @remarks
 **/
class Particle {
 public:
  Particle();
  //  ~Particle();
  //! Sets the type of the particle.
  //! @remarks explores the ability of using the "&" in conjuction with SWIG and Python.
  void get_type(int &type);
  int type;
  struct Vector r;
  struct Vector v;
  struct Vector f;
};

/* class test { */
/*   int k; */
/* }; */


#endif //! EOF
