#import py_example
import py_mesh

#! Static variables:
print "\nStatic variables:"
print py_mesh.cvar.My_variable
#! Update, and then inspect:
py_mesh.cvar.My_variable = 5
print py_mesh.cvar.My_variable

#!
#! Functions:
print "\nFunctions:"
print py_mesh.fact(5) #! 120
print py_mesh.my_mod(7,3) #! 1
print py_mesh.get_time() #! (today).

#!
#! Structs:simple:
print "\nStructs:simple:"
v = py_mesh.Vector()
#! Update the varialbles:
v.x = 3.5
v.y = 7.2
print v.x, v.y, v.z # where we exepct: 7.8 -4.5 0.0
#! Intiates to default:
v = py_mesh.init_Vector()
print v.x, v.y, v.z
#! Call the 'objct merge' funciton:
v2 = py_mesh.merge_returnByRef_Vector(v, v)
print v2.x, v2.y, v2.z
#! Call a permuation whcih is expected to cause a memory leak (as "v3" is 'behidn-the-scene' a new-allcoated, though untracked, memory reference). 
v3 = py_mesh.merge_returnByValue_Vector(v, v)
print v3.x, v3.y, v3.z

#!
#! Demonstrate support for dynamically allocated arrays: 
print "\nArrays:allocation:"
a = py_mesh.intArray(10000000)         # Array of 10-million integers
for i in xrange(10000):        # Set some values
    a[i] = i
sum = py_mesh.sumitems(a,10000)
print sum

# Create a MyData object with 3 elements
print "\nVector-access:"
x=py_mesh.MyData(3)
# Check what the value of second element is
print "init-value(1):", py_mesh.doubleP_value(x(1))
# Assign value 3 to second element
py_mesh.doubleP_assign(x(1), 3) #! set at index(1).
# Check what the value of second element is
print "get-value", py_mesh.doubleP_value(x(1))

#! Exemplifies the access to namespaces, and templates:
print "\nnamespace--templates:"
#p1 = mesh.m_pair(3,4)
p1 = py_mesh.mesh_ii(3,4)
#p1 = mesh.mesh_ii(3,4)
#p1 = py_mesh.mesh.mesh_ii(3,4)
print "init:", p1.first, p1.second
p1.first = 1
p1.second = -1
print "set to (1,-1):", p1.first, p1.second


#! Demonstrate support for shallow copying of pointers
print "\nArrays:shallow-copy:"
b = py_mesh.Bar()
print b.x #! _801861a4_p_int
c = py_mesh.Bar()
print c.x #! _801861a4_p_int
c.x = b.x             # Copy contents of b.x to c.x
print c.x #! _801861a4_p_int
# FIXME: how may the pointer items be accessed?
#print c.x(1)
print c.x[1]
#print c.x(1)
# print py_mesh.intP_value()
#pC = ctypes.cast( c.x, ctypes.POINTER( ctypes.c_int ))
# print c.x[0] # .__long__() #[0]
#print c.x[0].__long__() #[0]
