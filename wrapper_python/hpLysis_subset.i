 %module hpLysis_subset

 

 /*! Add support for dyanmically allocated arrays: */
%include "carrays.i"
%array_class(int, intArray);
%array_class(unsigned int, uintArray);
%array_class(float, floatArr);


%include cpointer.i // Grab the SWIG pointer library: "http://www.swig.org/Doc1.1/HTML/Typemaps.html" and foudn on our system at "/usr/share/swig2.0/perl5/carrays.i" and oru "/usr/share/swig2.0/perl5/typemaps.i"


   
   
 %{
#include "types.h"
#include "types_base.h"
#include "log_clusterC.h"
#include "e_kt_correlationFunction.h"
#include "kt_longFilesHandler.h"
#include "kt_metric_aux.h"
#include "hp_ccm.h"
#include "hp_distance.h"
#include "kt_sparse_sim.h"
#include "hp_api_fileInputTight.h"
#include "kt_terminal.h"


#include "kt_matrix.h"
#include "kt_matrix_cmpCluster.h"
#include "kt_list_1d.h"
#include "kt_list_1d_string.h"
#include "hpLysis_api.h"


/*    //#include "def_typeOf_float.h" */
/* #include "kt_api.h" */
/* // FIXME: make use of [”elow] ... ie, provide access/support for computign the 'eigen-vector-centrliaty' */
#include "e_kt_eigenVectorCentrality.h"
#include "kt_centrality.h"
#include "kt_clusterAlg_hca__node.h"
#include "parse_main.h"
#include "kt_clusterAlg_fixed_resultObject.h" //! which hold 'access' to the k-means-clsuter-object (oekseth, 06. otk. 2016)
#include "kt_clusterAlg_hca_resultObject.h" //! (oekseth, 06. nov. 2016).
#include "kt_clusterAlg_SOM_resultObject.h" //! (oekseth, 06. nov. 2016).
#include "kt_clusterAlg_hca__node.h" //! (oekseth, 06. nov. 2016).
#include "e_kt_clusterAlg_hca_type.h"
#include "kt_matrix_filter.h"
#include "kt_clusterResult_condensed.h"

#include "hp_api_fileInputTight.h"
#include "kt_swig_retValObj.h"

  
     #include "kt_list_1d.h"
#include "kt_list_1d_string.h"

#include "hp_ccm.h"
#include "hp_distance.h"
#include "kt_sparse_sim.h"
#include "kt_api.h"

  %}
%inline %{
  typedef float t_float;
  //  typedef unsigned char uint8;


  %}

%include "types.h"
%include "types_base.h"
 //%include "def_typeOf_float.h"
%include "e_kt_correlationFunction.h"
%include "kt_metric_aux.h"
%include "kt_list_1d.h"
%include "kt_list_1d_string.h"
%include "kt_list_2d.h"

 //FIXME: make use of [”elow] ... ie, provide access/support for computign the 'eigen-vector-centrliaty'
%include "e_kt_eigenVectorCentrality.h"
%include "kt_centrality.h"
%include "kt_clusterAlg_hca__node.h"
%include "log_clusterC.h"
%include "kt_terminal.h"
%include "kt_matrix.h"
%include "kt_matrix_cmpCluster.h"

%include "hp_api_fileInputTight.h"
%include "kt_swig_retValObj.h"


%include "parse_main.h"
%include "kt_clusterAlg_fixed_resultObject.h"
%include "kt_clusterAlg_hca_resultObject.h" //! (oekseth, 06. nov. 2016).
%include "kt_clusterAlg_SOM_resultObject.h" //! (oekseth, 06. nov. 2016).
%include "kt_clusterAlg_hca__node.h" //! (oekseth, 06. nov. 2016).
%include "e_kt_clusterAlg_hca_type.h"
%include "kt_matrix_filter.h"
%include "kt_clusterResult_condensed.h"


%include "hp_ccm.h"
%include "hp_distance.h"
%include "kt_sparse_sim.h"
%include "kt_api.h"
%include "hpLysis_api.h"

/*


*/
