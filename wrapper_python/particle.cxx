#include "particle.h"



Particle::Particle() :
  type(-1),
  r(init_Vector()),
  v(init_Vector()),
  f(init_Vector())
{}

void Particle::get_type(int &type) {
  type = this->type;
}
