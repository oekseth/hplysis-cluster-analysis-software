#ifndef vector_h
#define vector_h

/**
   @file vector.h
   @brief holds a three dimnesional tuplet
   @remarks 
   -- motivation: used to investigate accessor fucntions in SWIG
   -- compilation: g++ -c -Wall -Werror -fpic -Wno-unused-function vector.cxx
 **/

#include <assert.h>

/**
   @struct Vector
   @brief holds a three dimenionsal vector of points
   @remarks a bais function for access, and oficaiton, of class based variables.
 **/
struct Vector {
  double x,y,z;
};

//! @returns an intalized vector
static Vector init_Vector() {
  struct Vector self = {-1, -1, -1};
  return self;
}
//! Merge two vectors, and return by value:
//! @remarks this result is expected to result in a meory leak, as Python 'behdin-the-scene' (through SWIG generated wrappers) allocates an "Vector *" object,
struct Vector merge_returnByValue_Vector(struct Vector v1, struct Vector v2);

//! Merge two vectors, and return by reference
struct Vector *merge_returnByRef_Vector(struct Vector v1, struct Vector v2);


#endif //! EOF
