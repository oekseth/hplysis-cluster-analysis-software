#ifndef mesh_h
#define mesh_h
/**
   @file mesh.h
   @brief explores inheritance, templates, and namespace
   @remarks 
   -- compilation(c++): g++ vector.cxx particle.cxx mesh.cxx -Wall -Werror -fpic -Wno-unused-function -fpermissive -shared -o mesh.so
   -- debug: to see the functions inside a shared file (on Linux) write: nm -D <name of shared object file>.
   -- compilation() -- python(): 

   -- compilation() -- ()
   -- compilation() -- ()
 **/



#include "vector.h"
//#include "particle.h"


//! Global variables:
extern int My_variable;
extern double density;
//!
//! Functions: 
extern int fact(int n);
extern int my_mod(int x, int y);
extern char *get_time();

//! Sum items using a dynamically allcoated list as input:
//! @remarks exxplores the effects of using a 'static' function (rather than placing the funcitoin inside the 'object' file).
static int sumitems(int *first, int nitems) {
  int i, sum = 0;
  for (i = 0; i < nitems; i++) {
    sum += first[i];
  }
  return sum;
}



/**
   @class Bar
   @brief Access a fixed list, and inherits from class Particle
   @remarks demonstrates 
   - how inherited functioons, and varaibles, are accessilbe through the SWIG generated interfaces.  
   - the support for copying internal objecs, hence users are given the 'impression' that they are working with Python objects.
 **/
// FIXME: .... 
class Bar // : public Particle
{
  int  x[16];
};




/**
   @namespace mesh
   @brief explores the usage of templates
 **/
namespace mesh {
  template<class T1, class T2> struct m_pair {
    T1 first;
    T2 second;
  m_pair() : first(T1()), second(T2()) { };
  m_pair(const T1 &f, const T2 &s) : first(f), second(s) { }
  };
}

//! Instantiate some templates
//typedef mesh::m_pair<int, int> mesh_ii;
//typedef mesh::m_pair<int, double> mesh_id;

#include <vector>

/**
   @brief demonstrates an approach to access list-items.
   @remarks 
   -- src: http://www.bnikolic.co.uk/blog/cpp-swig-pointeraccess.html
**/
class MyData {
  std::vector<double> d;
 public:
  MyData(int n) {d.resize(n);} 
  double& operator()(int i) {return d[i];}
};

#endif //! EOF
