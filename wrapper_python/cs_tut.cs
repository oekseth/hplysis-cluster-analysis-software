using System;
 public class runme {
      static void Main() {	
	Console.WriteLine("var={0}", cs_mesh.My_variable);

	cs_mesh.My_variable = 5; //.set(5);
	//!
	//! Accesses functions: 
	Console.WriteLine("var={0}", cs_mesh.My_variable);
	Console.WriteLine("fact={0}", cs_mesh.fact(5));
	Console.WriteLine("time={0}", cs_mesh.get_time());

	//! Initates a vecotr: 
	Vector o = cs_mesh.init_Vector();
	o.x = 5;
	Console.WriteLine("vec={0},{1},{2}", o.x, o.y, o.z);
	Vector m = cs_mesh.merge_returnByValue_Vector(o, o);
	Console.WriteLine("vec(after-add--byVal)={0},{1},{2}", m.x, m.y, m.z);
	//! 
	m = cs_mesh.merge_returnByRef_Vector(o, o);
	Console.WriteLine("vec(after-add--byRef)={0},{1},{2}", m.x, m.y, m.z);
	//!
	//! 
	{ //! Explore C-list access--and--usage:
	  int size = 10;
	  intArray c = new intArray(size); //! where latter is sedefined in our "cs_mesh.i"	    
	  for (int i=0; i<size; i++)
	    c.setitem(i, 2*i);                    // Assign values
	  int sum = cs_mesh.sumitems(c.cast(), size);          // Pass to C
	  Console.WriteLine("sum({0})={1}", size, sum);
	}

	{ //! Tests access to both tempaltes, and namespace:
	  mesh_ii obj = new mesh_ii(3,4);
	  Console.WriteLine("namespace::template::init=({0}, {1})", obj.first, obj.second);
	  obj.first = 1; obj.second = -1;
	  Console.WriteLine("namespace::template::afterSet=({0}, {1})", obj.first, obj.second);
	}
	
	//cs_mesh.Vector vec;
	/*
	{
	  int res;
	  //! 
	  cs_mesh.add(1, 2, out res);
	  Console.WriteLine("add(1+2)={0}", res);
	  //! 
	  double res_d;
	  cs_mesh.add_ref(1.0, 2.0, out res_d);
	  Console.WriteLine("add_ref(1+2)={0}", res_d);
	  //! 
	  res = cs_mesh.sub(1, 2);
	  Console.WriteLine("sub(1+2)={0}", res);
	  //! 
	  res = 2; cs_mesh.negate(ref res);
	  Console.WriteLine("negate(2)={0}", res);
	}
	*/
      }
}
