#include "config_nonRank_oneToMany.h"


//! Initaites the config_nonRank_oneToMany_t to empty.
void setTo_empty___config_nonRank_oneToMany(config_nonRank_oneToMany_t *self) {
  self->index1 = 0; 
  self->nrows = 0;
  self->ncols = 0;
  self->data1 = NULL; 
  self->data2 = NULL;
  self->mask1 = NULL; 
  self->mask2 = NULL;
  self->weight = NULL;
  self->typeOf_metric = e_kt_correlationFunction_undef;
  self->typeOf_metric_correlation = e_typeOf_metric_correlation_pearson_undef;
  self->config_allAgainstAll = NULL;
  self->arrOf_result = NULL;
}

//! @brief intiate an object of type config_nonRank_oneToMany_t
//! @return an intiated object (ie, which is set to default empty values) wrt. the config_nonRank_oneToMany_t structure-type.
config_nonRank_oneToMany_t get_init__config_nonRank_oneToMany() {
  config_nonRank_oneToMany_t self; setTo_empty___config_nonRank_oneToMany(&self);
  return self;
}

//! Initiate the config_nonRank_oneToMany_t object with the complete lsit of specificaitons.
void init__config_nonRank_oneToMany(config_nonRank_oneToMany_t *self, const uint index1, const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], const e_kt_correlationFunction_t typeOf_metric, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, s_allAgainstAll_config_t *config_allAgainstAll, t_float *arrOf_result) {
  self->index1 = index1; 
  self->nrows = nrows;
  self->ncols = ncols;
  self->data1 = data1; 
  self->data2 = data2;
  self->mask1 = mask1; 
  self->mask2 = mask2;
  self->weight = weight;;
  self->typeOf_metric = typeOf_metric;
  self->typeOf_metric_correlation = typeOf_metric_correlation;
  self->config_allAgainstAll = config_allAgainstAll;
  self->arrOf_result = arrOf_result;
}

