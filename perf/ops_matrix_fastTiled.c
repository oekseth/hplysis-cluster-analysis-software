#ifndef __MiF__ops_each_preStep_1
#define __MiF__ops_each_preStep_1 ;
#endif
#ifndef __MiF__ops_each_preStep_2
#define __MiF__ops_each_preStep_2 ;
#endif

void __funcName__fast(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  VECTOR_FLOAT_TYPE vec_default = VECTOR_FLOAT_SET1(0); //! which is used to test different mult-funcitons.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    //const t_float *__restrict__ row_2_base = matrix[col_id + int_col_id];
	    /* const t_float *__restrict__ row_1 = row_1_base + j; */
	    /* const t_float *__restrict__ row_2 = row_2_base + j; */

	    //const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOAD(..)" instead of the "VECTOR_FLOAT_LOAD(..)" call:
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    uint int_j = 0;
	    /*! Apply pre-steps, ie, if any:*/
	    __MiF__ops_each_preStep_1; 	      __MiF__ops_each_preStep_2;
	    /*! Perform the 'main internal' operation:*/
	    VECTOR_FLOAT_TYPE vec_result = __MiF__ops_each; 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      /*! Apply pre-steps, ie, if any:*/
	      __MiF__ops_each_preStep_1; 	      __MiF__ops_each_preStep_2;
	      //! Perform the 'main internal' operation:*/
	      vec_result = VECTOR_FLOAT_ADD(vec_result, __MiF__ops_each);
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
#undef __funcName__fast
#undef __MiF__ops_each
#undef __MiF__ops_each_preStep_1
#undef __MiF__ops_each_preStep_2
