#ifndef types_base_h
#define types_base_h
/**
  @file
  @brief defines a set of base-tyeps which are used (oekseth, 06. sept. 2016).
  @ingroup common
  @author Ole Kristian Ekseth (oekseth)
**/

//! May have maximum 65.535 'indexes': Halving the size, without posing constraints, e.g. for the number of iummediate ancestors.
typedef unsigned short int lowint;
typedef short int lowint_s;
//! For convenience, giving the system easier job casting error if value is negative.
typedef unsigned int uint; 

//! For convenience, giving the system easier job casting error if value is negative.
typedef unsigned char uchar; 

//! Handling memory references: set to either 34b or 64b, dependening on the archictecture of the system.
typedef unsigned long int uli;
/**
  @brief Defines the maximum size of the overlap.
  @todo Changed to 'short int' as valgrind complains on 'usigned short int'.
  @remarks If changed from other type, MPI structures using this must be updated appropriatly, as the complainer does not have means detecting such inconsistencies!.
*/
typedef uint overlap_t; 

//! Holding the internal address of a data in a file, requires a a large number. 
typedef unsigned long long int loint; 
//typedef long long unsigned int loint; 
//! Holding the internal address of a data in a file, requires a a large number. 
typedef long int lint; 


#endif //! EOF
