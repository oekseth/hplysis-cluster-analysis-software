#ifndef __MiF__ops_each_preStep_1
#define __MiF__ops_each_preStep_1 ;
#endif
#ifndef __MiF__ops_each_preStep_2
#define __MiF__ops_each_preStep_2 ;
#endif
#ifndef __MiF__ops_each_block_1
#define __MiF__ops_each_block_1 ;
#endif

#ifndef __MiF_opsBlock
//! Then we define a default construct for coputing results.
#define __MiF_opsBlock() ({						\
      uint j = 0; t_float scalar_result = 0; \
      for(; j < size_adjusted; j++) { \
	__MiF__ops_each_preStep_1; \
	__MiF__ops_each_preStep_2; \
	__MiF__ops_each; /*! ie, apply the logics.*/ \
      } \
      scalar_result;})
#endif

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void __funcName__fast(t_float **matrix, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(result); assert(size_adjusted);
  __MiF__ops_each_block_1 //! a prestep
    //! 
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {      
      //! Apply logics:
      result[row_id][col_id] = __MiF_opsBlock();
    }
  }
}
#undef __funcName__fast
#undef __MiF__ops_each
#undef __MiF__ops_each_preStep_1
#undef __MiF__ops_each_preStep_2
#undef __MiF_opsBlock
#undef __MiF__ops_each_block_1
