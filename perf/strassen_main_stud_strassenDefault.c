
void strassen(vertexType **a, vertexType **b, vertexType **c, int tam);
void sum(vertexType **a, vertexType **b, vertexType **result, int tam);
void subtract(vertexType **a, vertexType **b, vertexType **result, int tam);
vertexType  **allocate_matrix(int size, const vertexType def_value);
void dealloc(vertexType **matrix, int size);

void strassen(vertexType **a, vertexType **b, vertexType **c, int tam) {
  const vertexType def_value = (vertexType)0.0;
  // trivial case: when the matrix is 1 X 1:
  if (tam == 1) {
    c[0][0] = a[0][0] * b[0][0];
    return;
  }

  // other cases are treated here:
  int newTam = tam/2;
  vertexType **a11, **a12, **a21, **a22;
  vertexType **b11, **b12, **b21, **b22;
  vertexType **c11, **c12, **c21, **c22;
  vertexType **p1, **p2, **p3, **p4, **p5, **p6, **p7;

  // memory allocation:
  a11 = allocate_matrix(newTam, def_value);
  a12 = allocate_matrix(newTam, def_value);
  a21 = allocate_matrix(newTam, def_value);
  a22 = allocate_matrix(newTam, def_value);

  b11 = allocate_matrix(newTam, def_value);
  b12 = allocate_matrix(newTam, def_value);
  b21 = allocate_matrix(newTam, def_value);
  b22 = allocate_matrix(newTam, def_value);

  c11 = allocate_matrix(newTam, def_value);
  c12 = allocate_matrix(newTam, def_value);
  c21 = allocate_matrix(newTam, def_value);
  c22 = allocate_matrix(newTam, def_value);

  p1 = allocate_matrix(newTam, def_value);
  p2 = allocate_matrix(newTam, def_value);
  p3 = allocate_matrix(newTam, def_value);
  p4 = allocate_matrix(newTam, def_value);
  p5 = allocate_matrix(newTam, def_value);
  p6 = allocate_matrix(newTam, def_value);
  p7 = allocate_matrix(newTam, def_value);

  vertexType **aResult = allocate_matrix(newTam, def_value);
  vertexType **bResult = allocate_matrix(newTam, def_value);

  int i, j;

  //dividing the matrices in 4 sub-matrices:
  for (i = 0; i < newTam; i++) {
    for (j = 0; j < newTam; j++) {
      a11[i][j] = a[i][j];
      a12[i][j] = a[i][j + newTam];
      a21[i][j] = a[i + newTam][j];
      a22[i][j] = a[i + newTam][j + newTam];

      b11[i][j] = b[i][j];
      b12[i][j] = b[i][j + newTam];
      b21[i][j] = b[i + newTam][j];
      b22[i][j] = b[i + newTam][j + newTam];
    }
  }

  // Calculating p1 to p7:

  sum(a11, a22, aResult, newTam); // a11 + a22
  sum(b11, b22, bResult, newTam); // b11 + b22
  strassen(aResult, bResult, p1, newTam); // p1 = (a11+a22) * (b11+b22)

  sum(a21, a22, aResult, newTam); // a21 + a22
  strassen(aResult, b11, p2, newTam); // p2 = (a21+a22) * (b11)

  subtract(b12, b22, bResult, newTam); // b12 - b22
  strassen(a11, bResult, p3, newTam); // p3 = (a11) * (b12 - b22)

  subtract(b21, b11, bResult, newTam); // b21 - b11
  strassen(a22, bResult, p4, newTam); // p4 = (a22) * (b21 - b11)

  sum(a11, a12, aResult, newTam); // a11 + a12
  strassen(aResult, b22, p5, newTam); // p5 = (a11+a12) * (b22)

  subtract(a21, a11, aResult, newTam); // a21 - a11
  sum(b11, b12, bResult, newTam); // b11 + b12
  strassen(aResult, bResult, p6, newTam); // p6 = (a21-a11) * (b11+b12)

  subtract(a12, a22, aResult, newTam); // a12 - a22
  sum(b21, b22, bResult, newTam); // b21 + b22
  strassen(aResult, bResult, p7, newTam); // p7 = (a12-a22) * (b21+b22)

  // calculating c21, c21, c11 e c22:

  sum(p3, p5, c12, newTam); // c12 = p3 + p5
  sum(p2, p4, c21, newTam); // c21 = p2 + p4

  sum(p1, p4, aResult, newTam); // p1 + p4
  sum(aResult, p7, bResult, newTam); // p1 + p4 + p7
  subtract(bResult, p5, c11, newTam); // c11 = p1 + p4 - p5 + p7

  sum(p1, p3, aResult, newTam); // p1 + p3
  sum(aResult, p6, bResult, newTam); // p1 + p3 + p6
  subtract(bResult, p2, c22, newTam); // c22 = p1 + p3 - p2 + p6

  // Grouping the results obtained in a single matrix:
  for (i = 0; i < newTam ; i++) {
    for (j = 0 ; j < newTam ; j++) {
      c[i][j] = c11[i][j];
      c[i][j + newTam] = c12[i][j];
      c[i + newTam][j] = c21[i][j];
      c[i + newTam][j + newTam] = c22[i][j];
    }
  }

  // deallocating memory (free):
  dealloc(a11, newTam);
  dealloc(a12, newTam);
  dealloc(a21, newTam);
  dealloc(a22, newTam);

  dealloc(b11, newTam);
  dealloc(b12, newTam);
  dealloc(b21, newTam);
  dealloc(b22, newTam);

  dealloc(c11, newTam);
  dealloc(c12, newTam);
  dealloc(c21, newTam);
  dealloc(c22, newTam);

  dealloc(p1, newTam);
  dealloc(p2, newTam);
  dealloc(p3, newTam);
  dealloc(p4, newTam);
  dealloc(p5, newTam);
  dealloc(p6, newTam);
  dealloc(p7, newTam);
  dealloc(aResult, newTam);
  dealloc(bResult, newTam);

} // end of Strassen function

void sum(vertexType **a, vertexType **b, vertexType **result, int tam) {
  int i, j;
  for (i = 0; i < tam; i++) {
    for (j = 0; j < tam; j++) {
      result[i][j] = a[i][j] + b[i][j];
    }
  }
}

void subtract(vertexType **a, vertexType **b, vertexType **result, int tam) {
  int i, j;
  for (i = 0; i < tam; i++) {
    for (j = 0; j < tam; j++) {
      result[i][j] = a[i][j] - b[i][j];
    }
  }
}
vertexType **allocate_matrix(const int size, const vertexType def_value) {
  vertexType **temp = vertexType_new_2d(size, size, def_value);
  return temp;
  /* vertexType **temp = new int*[size]; */
  /* for ( int i = 0 ; i < size ; i++) */
  /*   { */
  /*     temp[i] = new int[size]; */
  /*   } */
  /* return (temp); */
}
void dealloc (vertexType **matrix,int size) {
  if (matrix == NULL) {
    return;
  }
  vertexType_free_2d(&matrix, size);
  /* for ( int i = 0 ; i < size ; i++) */
  /*   { */
  /*     delete[] matrix[i]; */
  /*   } */
  /* delete[] matrix; */
}
//! ---------------------------
//!
//! Reset generic defintions.
#undef vertexType
#undef vertexType_new_2d
#undef vertexType_free_2d
#undef strassen
#undef sum
#undef subtract
#undef dealloc
#undef allocate_matrix
