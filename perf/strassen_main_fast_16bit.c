//! ------------------------------------------------------------------------
void dot_fast_dataTypes_16bit_load_mult(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT16_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT16_ITER_SIZE) {
	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])));
	      //	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT16_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}

//! ------------------------------------------------------------------------
void dot_fast_dataTypes_32bit_load_mult(int32_t **matrix, int32_t **matrix_2, const uint nrows, const uint ncols, int32_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int32_t *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int32_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int32_t *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const int32_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int32_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT_LOADU(..)" instead of the "VECTOR_INT_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT_TYPE vec_result = VECTOR_INT_MUL(VECTOR_INT_LOADU(&row_1[int_j]), VECTOR_INT_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_INT_TYPE vec_result = VECTOR_INT_MUL(VECTOR_INT_LOADU(&row_1[int_j]), VECTOR_INT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT_ITER_SIZE) {
	      vec_result = VECTOR_INT_ADD(vec_result, VECTOR_INT_MUL(VECTOR_INT_LOADU(&row_1[int_j]), VECTOR_INT_LOADU(&row_2[int_j])));
	      //	      vec_result = VECTOR_INT_ADD(vec_result, VECTOR_INT_MUL(VECTOR_INT_LOADU(&row_1[int_j]), VECTOR_INT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}



//! ------------------------------------------------------------------------
void dot_fast_dataTypes_float_load_mult(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOADU(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	      //	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}



//! ------------------------------------------------------------------------
void dot_fast_dataTypes_16bit_load_mult_xmtSSE(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; int16_t vec_result = macro_mul(row_1[int_j], row_2[int_j]);
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += 1;
	    for(; int_j < SM; int_j += 1) {
	      vec_result = macro_pluss(vec_result, macro_mul(row_1[int_j], row_2[int_j]));
	      //	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += vec_result;
	  }
	}
      }
    }
  }
}
//! ------------------------------------------------------------------------
void dot_fast_dataTypes_32bit_load_mult_xmtSSE(int32_t **matrix, int32_t **matrix_2, const uint nrows, const uint ncols, int32_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int32_t *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int32_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int32_t *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const int32_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int32_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT32_LOADU(..)" instead of the "VECTOR_INT32_LOADU(..)" call:
	    uint int_j = 0; int32_t vec_result = macro_mul(row_1[int_j], row_2[int_j]);
	    //uint int_j = 0; VECTOR_INT32_TYPE vec_result = VECTOR_INT32_MUL(VECTOR_INT32_LOADU(&row_1[int_j]), VECTOR_INT32_LOADU(&row_2[int_j])); 
	    int_j += 1;
	    for(; int_j < SM; int_j += 1) {
	      vec_result = macro_pluss(vec_result, macro_mul(row_1[int_j], row_2[int_j]));
	      //	      vec_result = VECTOR_INT32_ADD(vec_result, VECTOR_INT32_MUL(VECTOR_INT32_LOADU(&row_1[int_j]), VECTOR_INT32_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += vec_result;
	  }
	}
      }
    }
  }
}


//! ------------------------------------------------------------------------
void dot_fast_dataTypes_float_load_mult_xmtSSE(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT32_LOADU(..)" instead of the "VECTOR_INT32_LOADU(..)" call:
	    uint int_j = 0; t_float vec_result = macro_mul(row_1[int_j], row_2[int_j]);
	    //uint int_j = 0; VECTOR_T_FLOATYPE vec_result = VECTOR_INT32_MUL(VECTOR_INT32_LOADU(&row_1[int_j]), VECTOR_INT32_LOADU(&row_2[int_j])); 
	    int_j += 1;
	    for(; int_j < SM; int_j += 1) {
	      vec_result = macro_pluss(vec_result, macro_mul(row_1[int_j], row_2[int_j]));
	      //	      vec_result = VECTOR_INT32_ADD(vec_result, VECTOR_INT32_MUL(VECTOR_INT32_LOADU(&row_1[int_j]), VECTOR_INT32_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += vec_result;
	  }
	}
      }
    }
  }
}





//! Comptue inner distance-metric for cityblock
void dot_fast_dataTypes_16bit_load_cityblock(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    //const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))); 
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT16_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT16_ITER_SIZE) {
	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT16_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
