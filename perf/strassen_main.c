/**
   @brief 
   @author Ole Kristian Ekseth
   @remarks 
   - for autmation call our "x_measure_all.pl"
 **/
#ifndef t_float
// #define t_float double
#endif
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_typeOf_float.h"
#include "def_intri.h"
#include "kt_mathMacros.h"
#include "types_base.h"
#include "types.h"

#ifndef macro_pluss
#define macro_pluss(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_add
#define macro_add(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_minus
#define macro_minus(term1, term2) ({term1 - term2;})
#endif
#ifndef macro_mul
#define macro_mul(term1, term2) ({term1 * term2;})
#endif

typedef int int32_t;

//#include <iostream>
//#include <cstdlib>  //! for random number generator
//#include <ctime>
//#include <fstream> //!for writing to the file
#include <math.h>
//using namespace std;


// #include "strassen_main_stud_DFT_2d.c"

//! Src: "https://pastebin.com/USRQ5tuy"
#define vertexType int
#define vertexType_new_2d  allocate_2d_list_int
#define vertexType_free_2d free_2d_list_int
//! Spedify internal function-names: 
#define strassen strassen_int
#define sum sum_int
#define subtract subtract_int
#define dealloc dealloc_int
#define allocate_matrix allocate_matrix_int
//! The call:
#include "strassen_main_stud_strassenDefault.c"
//! -------------------
#define vertexType t_float
#define vertexType_new_2d  allocate_2d_list_float
#define vertexType_free_2d free_2d_list_float
//! Spedify internal function-names: 
#define strassen strassen_float
#define sum sum_float
#define subtract subtract_float
#define dealloc dealloc_float
#define allocate_matrix allocate_matrix_float
//! The call:
#include "strassen_main_stud_strassenDefault.c"
//! -----------------------------------------
#include "strassen_main_fast_16bit.c" //!< which provides fast implemetnaitons of 'muliplciation' and 'citbylock', where the muliplcaiton-operaion assumes that a transpsoed matrix is provided (where the transposion takes insigcicant time, hence ingored from the evlauation).

//! Src: "https://pastebin.com/HqHtFpq9"
void compute_regular_int32(const int nrows, const int ncols, int **A, int **B, int **product) {
  for(int row = 0; row < nrows; row++) {
    for(int col = 0; col < ncols; col++) {
      product[row][col] = 0;
      for(int inner = 0; inner < nrows; inner++) {
	product[row][col] = macro_pluss(product[row][col], macro_mul(A[row][inner], B[inner][col]));
      }
      // cout << product[row][col] << " ";
    }
    // cout << "\n";
  }
}
void compute_regular_float(const int nrows, const int ncols, t_float **A, t_float **B, t_float **product) {
  for(int row = 0; row < nrows; row++) {
    for(int col = 0; col < ncols; col++) {
      product[row][col] = 0;
      for(int inner = 0; inner < nrows; inner++) {
	product[row][col] = macro_pluss(product[row][col], macro_mul(A[row][inner], B[inner][col]));
      }
      // cout << product[row][col] << " ";
    }
    // cout << "\n"; 
  }
}  

//! Src: "https://pastebin.com/HqHtFpq9"
int main(const int array_cnt, char **array) {
  //! A simple input validation:
  if(array_cnt < 3) {
    fprintf(stderr, "Usage: %s <operation> <matrix-dimension: number> \n", array[0]);
    return -1;
  }
  assert((array[1]));
  assert((array[2]));
  assert(strlen(array[1]));
  assert(strlen(array[2]));
  //! 
  const uint nrows = (uint)atoi(array[2]); const uint ncols = nrows;
  // printf("nrows=%s, at %s:%d\n", array[2], __FILE__, __LINE__);
  assert(nrows > 0); 
  t_float **matrix      = NULL;   t_float **mat_result  = NULL;
  int16_t **matrix_int16      = NULL;   int16_t **mat_result_int16  = NULL;
  int **matrix_int32      = NULL;   int **mat_result_int32  = NULL;
  if(strstr(array[1], "16bit")) {
    int16_t def_value = 0;
    matrix_int16 = allocate_2d_list_int16_t(nrows, ncols, def_value);
    mat_result_int16 = allocate_2d_list_int16_t(nrows, nrows, def_value);
    assert(matrix_int16);
    assert(mat_result_int16);
    //!
    //! Set score:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	matrix_int16[row_id][col_id] = (1 + row_id) / (1 + col_id);
      }
    }
  } else if(strstr(array[1], "int_32bit")) {
    int def_value = 0;
    matrix_int32 = allocate_2d_list_int(nrows, ncols, def_value);
    mat_result_int32 = allocate_2d_list_int(nrows, nrows, def_value);
    assert(matrix_int32);
    assert(mat_result_int32);
    //!
    //! Set score:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	matrix_int32[row_id][col_id] = (1 + row_id) / (1 + col_id);
      }
    }    
  } else {
    t_float def_value = 0;
    matrix = allocate_2d_list_float(nrows, ncols, def_value);
    mat_result = allocate_2d_list_float(nrows, nrows, def_value);
    assert(matrix);
    assert(mat_result);
    //!
    //! Set score:
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < ncols; col_id++) {
	matrix[row_id][col_id] = (1 + row_id) / (1 + col_id);
      }
    }
  }  


  //!
  //! 
#define __cmp(str)  ( (strlen(str) == strlen(array[1])) && (0 == strcmp(str, array[1])) ) 
  if(__cmp("strassen_float")) {
    assert(matrix); assert(mat_result);
    assert(nrows == ncols); //! ie, what we expect in below: 
    strassen_float (matrix, matrix, mat_result, nrows);
  } else if(__cmp("strassen_int_32bit")) { //! takes approx. the same time as "strassen_float"
    assert(matrix_int32); assert(mat_result_int32);
    assert(nrows == ncols); //! ie, what we expect in below: 
    strassen_int (matrix_int32, matrix_int32, mat_result_int32, nrows);
  } else if(__cmp("matrix_naive_int_32bit")) {
    assert(matrix_int32); assert(mat_result_int32);
    assert(nrows == ncols); ////! ie, what we expect in below: 
    compute_regular_int32(nrows, ncols, matrix_int32, matrix_int32, mat_result_int32);
  } else if(__cmp("matrix_naive_float")) { //! goas approx 3x slower than "int32".
    assert(matrix); assert(mat_result);
    assert(nrows == ncols); //! ie, what we expect in below: 
    compute_regular_float(nrows, ncols, matrix, matrix, mat_result);
  } else if(__cmp("fast_int_16bit-mult-64")) {
    const uint SM = 64;
    assert(mat_result_int16); assert(matrix_int16); 
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix_int16);
    assert(mat_result_int16);
    dot_fast_dataTypes_16bit_load_mult(matrix_int16, matrix_int16, nrows, ncols, mat_result_int16, SM);
  } else if(__cmp("fast_int_32bit-mult-64")) {
    const uint SM = 64;
    assert(mat_result_int32); assert(matrix_int32); 
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix_int32);
    assert(mat_result_int32);
    dot_fast_dataTypes_32bit_load_mult(matrix_int32, matrix_int32, nrows, ncols, mat_result_int32, SM);
  } else if(__cmp("fast_float-mult-64")) {
    const uint SM = 64;
    assert(mat_result); assert(matrix); 
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix);
    assert(mat_result);
    dot_fast_dataTypes_float_load_mult(matrix, matrix, nrows, ncols, mat_result, SM);        
  } else if(__cmp("fast_int_16bit-mult-64-xmtSSE")) {
    const uint SM = 64;
    assert(mat_result_int16); assert(matrix_int16); 
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix_int16);
    assert(mat_result_int16);
    dot_fast_dataTypes_16bit_load_mult_xmtSSE(matrix_int16, matrix_int16, nrows, ncols, mat_result_int16, SM);
  } else if(__cmp("fast_int_32bit-mult-64-xmtSSE")) {
    const uint SM = 64;
    assert(mat_result_int32); assert(matrix_int32); 
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix_int32);
    assert(mat_result_int32);
    dot_fast_dataTypes_32bit_load_mult_xmtSSE(matrix_int32, matrix_int32, nrows, ncols, mat_result_int32, SM);
  } else if(__cmp("fast_float-mult-64-xmtSSE")) {
    const uint SM = 64;
    assert(mat_result); assert(matrix); 
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix);
    assert(mat_result);
    dot_fast_dataTypes_float_load_mult_xmtSSE(matrix, matrix, nrows, ncols, mat_result, SM);            
  } else if(__cmp("fast_int_16bit_mult-128")) {
    const uint SM = 128;
    assert(mat_result_int16); assert(matrix_int16);
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix_int16);
    assert(mat_result_int16);
    dot_fast_dataTypes_16bit_load_mult(matrix_int16, matrix_int16, nrows, ncols, mat_result_int16, SM);
  } else if(__cmp("fast_int_16bit-cityblock-64")) { //! goes 15x+ faster than "matrix_naive_int_32bit" and approx. 50x+ faster than the more frequent "matrix_naive_float" ... comptuation-time of "multiplciation" and "iytblock" has approx. the same time-cost. 
    const uint SM = 64;
    assert(mat_result_int16); assert(matrix_int16);
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix_int16);
    assert(mat_result_int16);
    dot_fast_dataTypes_16bit_load_cityblock(matrix_int16, matrix_int16, nrows, ncols, mat_result_int16, SM);
  } else if(__cmp("fast_int_16bit_cityblock-128")) {
    const uint SM = 128;
    assert(mat_result_int16); assert(matrix_int16);
    assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
    //!
    assert(matrix_int16);
    assert(mat_result_int16);
    dot_fast_dataTypes_16bit_load_cityblock(matrix_int16, matrix_int16, nrows, ncols, mat_result_int16, SM);    
  } else if(__cmp("DFT_brute_float")) {                    
    assert(matrix); assert(mat_result);
    const t_float def_value = 0; t_float **mat_result_i = allocate_2d_list_float(nrows, nrows, def_value);
    //! 
    assert(false); // FIXME: include below:
    // dft_2d(nrows, ncols, matrix, mat_result, mat_result_i);
    //!
    free_2d_list_float(&mat_result_i, nrows);
  } else {
    fprintf(stderr, "label=\"%s\" not known, at %s:%d\n", array[1], __FILE__, __LINE__);
    assert(false); //! ie, then add support for this paramter. 
  }
  //!
  //! Avoid compiation-paramters from 'dropping the compuaiton ... ie, comptue a scalar wr.t the results.
  t_float sum = 0;
  if(mat_result) {
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < nrows; col_id++) {
	sum += mat_result[row_id][col_id];
      }
    }
  }
  if(mat_result_int16) {
    assert(mat_result_int16); assert(matrix_int16);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < nrows; col_id++) {
	sum += (t_float)mat_result_int16[row_id][col_id];
      }
    }
  }
  if(matrix_int32) {
    assert(matrix_int32); assert(mat_result_int32);
    assert(mat_result_int32);
    for(uint row_id = 0; row_id < nrows; row_id++) {
      for(uint col_id = 0; col_id < nrows; col_id++) {
	sum += (t_float)mat_result_int32[row_id][col_id];
      }
    }
    free_2d_list_int(&matrix_int32, nrows);
    free_2d_list_int(&mat_result_int32, nrows);
  }  
  fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
  //!
  //! De-allocate:
  if(matrix) {
    free_2d_list_float(&matrix, nrows);
    free_2d_list_float(&mat_result, nrows);
  }
  if(matrix_int16) {
    free_2d_list_int16_t(&matrix_int16, nrows);
    free_2d_list_int16_t(&mat_result_int16, nrows);
  }
#undef __cmp
  return 1;  


  return 0;
}
