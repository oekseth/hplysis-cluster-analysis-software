use strict;
use Time::HiRes qw(usleep ualarm gettimeofday tv_interval);
use Carp;

=head
    @brief captures time cost of different optmization strateiges.
    @remarks higlights: core points which are evaluated:
    () hpLysis--benefit: 
    () HPC--implementation--paradigms: impact/inlfuence of different strategies
    () correctness--applicability--exeuction-time: compiler versions, compiler parameters, .... 
    () : 
    () : 

=cut

my @arrOf_gcc = split("\n", 'dpkg -l | grep gcc | awk \'{print $2}\'');;
# my @arrOf_g_cxx = split("\n", 'dpkg -l | grep g++ | awk \'\{print \$2\}\'');;
printf("arrOf_gcc=\"%s\", at %s:%d\n", join("\t", @arrOf_gcc), __FILE__, __LINE__);

my @arrOf_genericCompilerStrategy = ( "-O0",    "-O2", "-O3");
my @arrOf_compilerFlags = (
    "", #! ie, 'no-option'.
    "-floop-block --with-ppl --with-cloog",
    "-ftree-loop-distribution --with-ppl --with-cloog",
    "-ftree-parallelize-loops=2", #! ie, two threads.
    "-funroll-loops",
    "-funroll-all-loops",
    "-fwhole-program",
    "-ffast-math",
    "-funsafe-math-optimizations",
    "-fassociative-math",
    "-ffinite-math-only",
    "-fno-signed-zeros",
    "-fno-trapping-math",
    "-fpeel-loops",
    "-funswitch-loops",
    "-fsection-anchors",
    # "",
    # "",
    # "",
    # "",
    "-fbranch-probabilities", #! where a "-fprofile-arcs -fprofile-generate -fprofile-use" pre-compiaton needs to first be applied
    ## "",
    );

sub eval_impl_euclid_cpp {
    my ($resultFile_midFix, $file_main, $arrOf_depend, $arrOf_bashParams, $arrOf_nrows, $fixed_compileParams) = @_;
    
    my $result_file = "measure_comp_" . $resultFile_midFix . ".tsv";
    open(FILE_OUT, ">$result_file") or die("Unable to open the input-file $result_file\n");
    #! Add headeR:
    printf(FILE_OUT "#! row-name:\t%s\t%s\t%s\t%s\t%s\t%s\n", "compiler", "compiler_option_base", "compiler_option", "case_id", "nrows", "duration");

    if(!defined($fixed_compileParams)) {$fixed_compileParams = "";}
    my $index_local = 0;
    foreach my $compilerVersion ("gcc", ,"gcc-4.8", "gcc-4.9",    "gcc-6",  "gcc-7",  "gcc-8",
				 "g++", "g++-4.7", "g++-4.8", "g++-4.9",  "g++-6",  "g++-7",  "g++-8", ) {
	foreach my $genericCompilerStrategy (	    @arrOf_genericCompilerStrategy) {	    
	    foreach my $fineTuned_compilerArg (
		@arrOf_compilerFlags
		) {
		#! 
		#! 
		my $cmd_comp        = $compilerVersion . " " . $file_main . " -std=c99 -lm -std=gnu99 -mssse3 " . $genericCompilerStrategy . " " . $fixed_compileParams;
		my $stringOf_depend = "";
		if($fineTuned_compilerArg =~/branch/) {
		    if(scalar(@{$arrOf_depend})) {
			foreach my $file_depend (@{$arrOf_depend}) {
			    #!
			    #! Note: ofr a brief introduction ot the compiation precess, see "https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html".
			    my $cmd_comp_depend = $compilerVersion . " " . $file_depend . "-c " . " -std=c99 -lm -std=gnu99 -mssse3 " . $genericCompilerStrategy . " " . $fixed_compileParams;;
			    #! 
			    #! The a pre-compaiotnø-stpe:
			    my $cmd_comp_pre = $cmd_comp_depend . " " . "-fprofile-arcs -fprofile-generate -fprofile-use";
			    #printf("compiles: %s\t at %s:%d\n", $cmd_comp_pre, __FILE__, __LINE__);
			    system($cmd_comp_pre);
			    my ($file_prefix) = ($file_depend =~ /^(.+\.)[a-z]+$/);
			    $stringOf_depend .= $file_prefix . "o "; #! ie, an 'object file'.			    
			}		    
		    }
		    # printf("stringOf_depend=\"%s\", at %s:%d\n", $stringOf_depend, __FILE__, __LINE__);
		    #! 
		    #! The a pre-compaiotnø-stpe:
		    my $cmd_comp_pre = $cmd_comp . " " . "-fprofile-arcs -fprofile-generate -fprofile-use " . $stringOf_depend;
		    #printf("compiles: %s\t at %s:%d\n", $cmd_comp_pre, __FILE__, __LINE__);
		    system($cmd_comp_pre);		    
		}
		#!
 		#! The the 'main' pre-comptaution-strategy.
		$cmd_comp .= " " . $fineTuned_compilerArg . " " . $stringOf_depend;
		if(scalar(@{$arrOf_depend})) {
		    foreach my $file_depend (@{$arrOf_depend}) {
			my $cmd_comp = $compilerVersion . " " . $file_depend .  "-c " . " -std=c99 -lm -std=gnu99 -mssse3 " . $genericCompilerStrategy . " " . $fineTuned_compilerArg . " " . $fixed_compileParams;;
			#! 
			#! The a pre-compaiotnø-stpe:
			#printf("compiles: %s\t at %s:%d\n", $cmd_comp, __FILE__, __LINE__);
			system($cmd_comp);
			#croak("...");
		    }		    
		}
		# printf("stringOf_depend=\"%s\", at %s:%d\n", $stringOf_depend, __FILE__, __LINE__);
		#printf("compiles: %s\t at %s:%d\n", $cmd_comp, __FILE__, __LINE__);
		system($cmd_comp);
		#croak("...");		
		#! Evalute different cases: 
		foreach my $nrows (@{$arrOf_nrows}) {
		    foreach my $case_id (@{$arrOf_bashParams}) {
			my $duration = 1000000.0; #! ie, an upper-bound wrt. the time:
			#my $duration = time - $start;
			for(my $test_i = 0; $test_i < 5; $test_i++) { #! then apply multiple runs:
			    #! 
			    # measure elapsed time
			    # (could also do by subtracting 2 gettimeofday return values)
			    my $t0 = [gettimeofday];
			    #my $start = time;
			    my $cmd = "./a.out " . $case_id . " " . $nrows . " 2>err.txt";
			    #printf("evaluate: %s\t at %s:%d\n", $cmd, __FILE__, __LINE__);
			    system($cmd);
			    my $duration_curr = tv_interval ($t0, [gettimeofday]);
			    if($duration_curr < $duration) {
				$duration = $duration_curr;
			    }
			}
			#!
			#! Write out:
			printf(FILE_OUT "index:%d\t%s\t%s\t%s\t%s\t%s\t%f\n", $index_local, $compilerVersion, $genericCompilerStrategy, $fineTuned_compilerArg, $case_id, $nrows, $duration);
			printf("index:%d\t%s\t%s\t%s\t%s\t%s\t%f\n", $index_local, $compilerVersion, $genericCompilerStrategy, $fineTuned_compilerArg, $case_id, $nrows, $duration);
			$index_local++;
		    }
		}
	    }
	}
    }
    close(FILE_OUT);
}


if(1 == 2)
{
    #!
    if(1 == 2) 
    { #! then apply:
	my $resultFile_midFix = "worstVSextreme_main" . time();
	my $file_main = "ops_matrix.c";
	my @arrOf_depend;
	my @arrOf_bashParams = (
	    #"small_basicCases__language", "basicCases__language"
	    "dot_slow", 
	    "dot_fast-32",
	    "dot_fast-64",
	    "dot_fast-128");
	my @arrOf_nrows = (
	    #(62*2) 
	    #2*128
	    5*128
	    , 10*128
	    , 15*128
	    , 20*128
	    , 25*128
	    );
	if(1 == 1) {
	    @arrOf_nrows = (
		#(62*2) 
		#2*128
		5*128
		, 10*128 
		);	
	}
	eval_impl_euclid_cpp($resultFile_midFix, $file_main, \@arrOf_depend, \@arrOf_bashParams, \@arrOf_nrows);
    } elsif(1 == 2) {
	my $resultFile_midFix = "worstVSextreme_nonSSE_";
	my $file_main = "ops_matrix.c";
	my @arrOf_depend;
	my @arrOf_bashParams;
	my @arrOf_nrows = (
	    #(62*2) 
	    #2*128
	    5*128
	    , 10*128
	    , 15*128
	    , 20*128
	    , 25*128
	    # , 10*128
	    # , 15*128
	    # , 20*128
	    # , 25*128
	    );
	if(1 == 2) {
	    $resultFile_midFix .= "ifClause";
	    @arrOf_bashParams = (
		#"small_basicCases__language", "basicCases__language"
		"dot_slow_IfClauses",
		"dot_slow_IfClausesNan", 
		);
	} elsif(1 == 2) {
	    $resultFile_midFix .= "ifClauseAssert";
	    @arrOf_bashParams = (
		"dot_slow_IfClausesNanAssert"
		#"dot_slow_wAssertsIfClauses",
		#"dot_slow_wAssertsIfClausesNan", 
		#"dot_slow_wAsserts", 
		);
	} elsif(1 == 2) { #! then a 'mask-matrix' is used ... as an alternative approach to comparing with  a given value (eg, 'inf' or 'nan') ... where the compiler is 'provide' the possible to comptuanally infer that 'all valeus are of interes't ... ie, where the if-etencne may be dropped 'if a pre-step is applied'.
	    $resultFile_midFix .= "ifClauseCharMaskAllTrue";
	    #	$resultFile_midFix .= "ifClauseMaskAssert";
	    @arrOf_bashParams = (
		#"small_basicCases__language", "basicCases__language"
		"dot_slow_IfClausesMaskAllTrue",
		#"dot_slow_IfClausesMaskAssert", 
		);
	} elsif(1 == 2) { #! then a 'mask-matrix' is used ...
	    $resultFile_midFix .= "ifClauseCharMaskAllFalse";
	    @arrOf_bashParams = (
		"ifClauseMaskAllFalse");	
	} elsif(1 == 2) {
	    $resultFile_midFix .= "fast_xmtSSE";
	    @arrOf_bashParams = (
		"dot_fast_xmtSSE-32",
		"dot_fast_xmtSSE-64",
		"dot_fast_xmtSSE-128",
		"dot_fast_xmtSSE-128-wAsserts"
		);
	} elsif(1 == 2) {
	    $resultFile_midFix .= "fast_xmtSSE-ifClause";
	    @arrOf_bashParams = (
		#"small_basicCases__language", "basicCases__language"
		#"dot_fast_xmtSSE-128-nan",
		"dot_fast_xmtSSE-128-empty-if",
		"dot_fast_xmtSSE-128-empty-if-wAsserts",
		#! where motviation of [”elow] is to test if 'asserts' introdue a perfroamcne epentaly
		#"dot_fast_xmtSSE-128-wAsserts"
		);
	} elsif(1 == 2) { #! then we evaluate the effect of data-compression rates'.	

	    # FIXME: now uses 'loadu(...)' .... try reoslving the isseus wrt. memory-segfault ...then updating 'this' code ...
	    $resultFile_midFix .= "fast_dataTypes_type"; #! then use differnet tdata-types (and memory laod funcitons).
	    @arrOf_bashParams = (	    
		# FIXME: try also adding support for different data-types
		"dot_fast_dataTypes_float_load-128", #! which is cotnrast to 'earlier' emasurements uses "load(..)", hence shoudl opriudce results faster (which is asusme dot be irrespeictive of compilator apramters).
		"dot_fast_dataTypes_16bit_load-128",
		#! Note: itnital results idnicates that hte choise of copiler apramters have sigciant/storng ifnleucne on the relative time-cost of "cityblock", hence ....??....
		"dot_fast_dataTypes_16bit_load_cityBlock-128", #! Note: #define VECTOR_INT16_abs(vec) ({VECTOR_INT16_MAX(VECTOR_INT16_SUB(VECTOR_INT16_SET_zero(), vec), vec);}) vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))));
		);
	    if(1 == 1) {
		@arrOf_nrows = (
		    #(62*2) 
		    #2*128
		    5*128
		    , 10*128 
		    );	
	    }	
	} elsif(1 == 1) {
	    $resultFile_midFix .= "slow_transposed";
	    @arrOf_bashParams = (
		"dot_slow_transposed" #! where 'this' changes the order of the "row--column" iteration, ie, "column-->row" instead of "row-->column" data traversal.
		);
	    if(1 == 1) {
		@arrOf_nrows = (
		    #(62*2) 
		    #2*128
		    5*128
		    , 10*128 
		    );	
	    }		
	} elsif(1 == 2) {
	    $resultFile_midFix .= "fast_dataTypes_artimeticComplexity";
	    #! Motivation: below results are of interest wrt. choosing the proper simlairty metric: we often observe that large efforts are sepnd on gaining an improvement factor of lkess than 2x. These results clealry idnciates that algorithms such as k-means may be improved by fa cotr of ...??.... (when  choposing the proper simalirty metirc and compilation parameter). ... eg, whre we observe an approx. 3x perofrmance difference when the 'fastet' strategy is used compared to "dot_fast_2Add-2Abs-128  1920    2.604814" (for 'gcc version 5.4') .... where compelte set of gcc versions is seen at "https://gcc.gnu.org/releases.html" .... in our approach we seek to evaluate the eprformanc eof older compielrs .... as the soruce code used for large scal analyssi often 'lags behind' the most modern source ocode sfotware .... when stydying the effects of compielr verisons, we observe how ....??...
	    #! Motivation: ... a mapping from 'tables of throughput and latency of comptuer hardware' to compilerconfiguraiton setts ... providing insight into he .... 
	    #! Motivation: ... ot idneitfy the "Pareto threshold" \cite{} for new algorithms/software ... 
	    @arrOf_bashParams = ( #! then we explore the impact of selecting the simlairty metircs iwth lowest comptaitonal complexity, eg, as observed from "dot_fast_dataTypes_16bit_load-128" VS "dot_fast_dataTypes_16bit_load_cityBlock-128"
				  "dot_fast_2Add-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__2Add
				  "dot_fast_3Add-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__3Add
				  #! ---
				  "dot_fast_2Add-1Abs-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__2Add_1Abs
				  "dot_fast_2Add-2Abs-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__2Add_2Abs 
				  #! ---
				  "dot_fast_1Add-1Max-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__1Add_1Max
				  "dot_fast_2Add-1Max-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__1Add_2Max
				  #! ---
				  "dot_fast_2Add1Mult-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__2Add_1Mul
				  "dot_fast_2Add2Mult-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__2Add_2Mul 
				  "dot_fast_2Add3Mult-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__2Add_3Mul
		);
	} elsif(1 == 2) {
	    #! Note: in [blow] we use the fastet straty idenifed in [ªbvoe] step, ie, using ...??...
	    $resultFile_midFix .= "fast_maskImplicit";
	    @arrOf_bashParams = (
		# FIXME: descirbe different SSE SIMD strategies for masking (ie, to re-write if-stencnes into SSE SIMD logics) ... evaluating effects of different strateiges.
		#! Note: [below] explores the overhead in tme-cost by copmputing 'relative Euclid'.
		"float_mask_char_pair_implicit_removeHidden_getCount", #! ie, VECTOR_FLOAT_dataMask_pair(vec_mul, vec_term1, vec_term2);
		# VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCount(vec_input_1, vec_input_2);
		#vec_result = VECTOR_FLOAT_ADD(vec_result, vec_maskCount); // VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
		# "float_mask_char_pair_", #! ie, VECTOR_FLOAT_dataMask_implicit_updatePair(vec_input_1, vec_input_2);
		"float_mask_char_pair_implicit_removeHidden",
		# VECTOR_FLOAT_dataMask_implicit_updatePair(vec_input_1, vec_input_2); //! ie, seet to 'zero' the valeus which are set to T_FLOAT_MAX
		#
		"float_mask_char_pair_replaceZeroFrom_nan", #! ie, VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(vec_result);
		# 
		# 
		"float_mask_char_pair_replaceZeroFrom_inf", #! ie, VECTOR_FLOAT_dataMask_replaceINF_by_Zero(vec_result);
		# VECTOR_FLOAT_dataMask_replaceINF_by_Zero(VECTOR_FLOAT_DIV(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
		"float_mask_char_explicit_case_1_andSum",
		"float_mask_char_explicit_case_2_andSum",
		);
	} elsif(1 == 2) {
	    $resultFile_midFix .= "fast_maskImplicit_omiZeros";
	    @arrOf_bashParams = (
		"float_mask_char_pair_zeroFilter_case1_add", #! ie, VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(vec_term1, vec_term2, funcRef_float);
		# VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]), VECTOR_FLOAT_ADD);
		"float_mask_char_pair_zeroFilter_case1_div", #! ie, VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(vec_term1, vec_term2, funcRef_float);
		# VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]), VECTOR_FLOAT_DIV);
		"float_mask_char_pair_case2_div", #! ie, VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_1(vec_term1, vec_term2, funcRef_float);
		# VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_1(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]), VECTOR_FLOAT_DIV);

		"float_mask_char_pair_case3_div", #! ie, VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_2(vec_term1, vec_term2, funcRef_float);
		# VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_2(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]), VECTOR_FLOAT_DIV);

		#"", #! ie, VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d_old_1(vec_term1, funcRef_float_1d);
		"float_mask_char_pair_case0_div", #! ie, VECTOR_FLOAT_omitZeros_fromComputation_DIV(vec_term1, vec_term2);
		# VECTOR_FLOAT_omitZeros_fromComputation_DIV(VECTOR_FLOAT_LOAD(&row_1[int_j]), VECTOR_FLOAT_LOAD(&row_2[int_j]));
		);
	} else {
	    croak("\t Add support fo this");
	}
	#     } elsif(1 == 2) {
	# 	$resultFile_midFix .= "fast_maskExplicit";
	#         @arrOf_bashParams = (

	
	

	# 	    #! -----------------------------------
	# =head	    
	# 	    	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_charMask_pair_old(vec_mul, rmask1, rmask2, mask_index);
	# 		  //! ------------------------------ end: input.
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2, vec_mask_combined);
	#     VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2), vec_mask_combined);

	#     	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, char_mask_1, char_mask_2);
	# 	const char *__restrict__ mask1 = listOf_values_char_in;
	# 	const char *__restrict__ mask2 = listOf_values_char_out;
	# 	  VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, mask1, mask2);

	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef_atIndex(vec_input_1, vec_input_2, char_mask_1, char_mask_2, index_charTable);

	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_charMask_pair(vec_mul, rmask1, rmask2, mask_index);

	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_charMask_data1(data_ref, rmask1, mask_index, vec_mask);
	
	# 	    "float_mask_char_pair", #! ie,  VECTOR_FLOAT_charMask_data1_vec(vec_term1, rmask1, mask_index, vec_mask);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_charMask_data1_implit(data_ref, rmask1, mask_index);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_charMask_data1_implit_old(data_ref, rmask1, mask_index) ;
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_data1(data_ref, mask) ;
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_data1_vec(vec_term1, mask);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_data1_maskImplicit(data_ref);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_combined);



	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2, vec_mask_combined);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, char_mask_1, char_mask_2) ;
	# 	  VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &mask1, &mask2);

	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef_atIndex(vec_input_1, vec_input_2, char_mask_1, char_mask_2, index_charTable);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_dataMask_explicit_updatePair(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2);

	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_func_maskTransform_explcitToImplicit(vec_input, vec_mask, vec_spec_emptyValue);
	# 	    "float_mask_char_pair", #! ie, VECTOR_FLOAT_func_maskTransform_implicitMask_notRevertOrder(vec_input, vec_specEmptyValue_inInput, vec_spec_emptyValue);
	# =cut	   	    
	# 	    #"dot_fast_mask_alt_",
	#     # } elsif(1 == 2) {
	#     # 	$resultFile_midFix .= "fast_mask";
	#     #     @arrOf_bashParams = (
	#     # 	    # FIXME: descirbe different SSE SIMD strategies for masking (ie, to re-write if-stencnes into SSE SIMD logics) ... evaluating effects of different strateiges.
	#     # 	    "dot_fast_mask_alt_",
	#     # 	    );
	#     # # } elsif(1 == 2) {
	#     # # 	$resultFile_midFix .= "_xmtSSEAssert";
	#     # #     @arrOf_bashParams = (
	#     # # 	#"small_basicCases__language", "basicCases__language"
	#     # # 	"dot_fast_xmtSSE-32",
	#     # # 	"dot_fast_xmtSSE-64",
	#     # # 	"dot_fast_xmtSSE-128",
	#     # # 	"dot_fast_xmtSSE-128_wAsserts"
	#     # # 	    ;)
	#     # }
	my @arrOf_depend;

	#!
	#! Call:
	$resultFile_midFix .= "." . time();
	eval_impl_euclid_cpp($resultFile_midFix, $file_main, \@arrOf_depend, \@arrOf_bashParams, \@arrOf_nrows);
	# # } elsif(1 == 1) {
	# #     my $resultFile_midFix = "worstVSextreme_nonSSE_" . time();
	# #     my $file_main = "ops_matrix.c";
	# #     my @arrOf_depend;
	# #     my @arrOf_bashParams = (
	# # 	"",
	# # 	"",
	# # 	"", 
	# # 	) ;
	# #     my @arrOf_nrows = (
	# # 	#(62*2) 
	# # 	#2*128
	# # 	5*128
	# # 	, 10*128
	# # 	, 15*128
	# # 	, 20*128
	# # 	, 25*128
	# # 	);
	# #     eval_impl_euclid_cpp($resultFile_midFix, $file_main, \@arrOf_depend, \@arrOf_bashParams, \@arrOf_nrows);
	# # } elsif(1 == 1) { #! different SSE SIMD instructions.
	# #     ; # FIXME
	# #     croak("FIXME: add suppor tfor this");
	# # } elsif(1 == 1) { #! then evaluate the abilities of compiler to improve "tranposed data access"
	# #     ; # FIXME: 
	# #     croak("FIXME: add suppor tfor this");
    } elsif(1 == 2) { #! time-cost of logiarhtm ... and randomzeid ... calls .... eg, "rand()" VS "uniform(..)".
	my $resultFile_midFix = "defaultAritmethics_";
	my $file_main = "ops_matrix.c";
	my @arrOf_depend;
	my @arrOf_bashParams;
	#!
	my $isTo_useSmallData = 1; #! whcih is used to toggle which row-siees to explore.
	#!
	# FIXME: compile seoerately for 'float' and 'double' ... use latter results to ....??..
	
	if(1 == 2) {
	    #! Motivaiton: idnetify the perofmrance impact of different ....??...
	    #! Note: [”elow] 'extends' Euclid (ie, 1-add + 1-mult) with the bleo waritmethci operaiotns:
	    #! 
	    $resultFile_midFix .= "adjust_log";
	    @arrOf_bashParams = (
		#! Reference metircs:
		"defaultAritmethics_float_abs",
		"defaultAritmethics_float_ceil",
		"defaultAritmethics_float_floor",
		#! Our primary interest:
		"defaultAritmethics_float_log",
		"defaultAritmethics_float_log2",
		"defaultAritmethics_float_log_abs", #! mathLib_float_log_abs(..)
		"defaultAritmethics_float_log2_abs", #! mathLib_float_log_abs(..)
		"defaultAritmethics_float_log_shannon", #! ie, the 'extra' cost of using an extra muliplcaiton operaiton.
		);
	} elsif(1 == 2) {	#! thene valuat ehte time-cost of differentr trigonomertric functiosn:
	    $resultFile_midFix .= "adjust_trigon";
	    @arrOf_bashParams = (
		"defaultAritmethics_float_sqrt", #! as a reference: #! mathLib_float_sqrt(..)
		"defaultAritmethics_float_sqrt_abs", #! as a reference: #! mathLib_float_sqrt_abs(..)
		"defaultAritmethics_float_cos",
		"defaultAritmethics_float_sin",
		"defaultAritmethics_float_atan",
		"defaultAritmethics_float_exp",
		"defaultAritmethics_float_pow2",
		"defaultAritmethics_float_pow3",
		"defaultAritmethics_float_pow4",
		#! ------------- 
		"defaultAritmethics_rand_srand",
		);
	} elsif(1 == 1) { #! where we in [”elow] investigate the ifleucne of both 'oduble' and 'float'
	    $resultFile_midFix .= "fixedAritmetics_tile";
	    @arrOf_bashParams = (
		"dot_fast_1Add_1Add-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__2Add
		"dot_fast_1Add_1Div-128",
		"dot_fast_1Add_1Mul-128",
		);
	} elsif(1 == 2) {	#! 
	    $resultFile_midFix .= "adjust_trigon_differentDims";
	    @arrOf_bashParams = ( #! where motviaiton is to 'get' the realtive ifnlunce of strategies such as "defaultAritmethics_float_pow4" (ie, simplifying comparing, adnv evlauation, of simliarty emtrics impacts on exeucion time) ... eg, wrt. choosing the simlairty-metric with the hgiehst accurayc, and lowest comptuation cost. 
				  "defaultAritmethics_float_sqrt", #! as a reference: #! mathLib_float_sqrt(..)
				  "defaultAritmethics_float_sqrt_abs", #! as a reference: #! mathLib_float_sqrt_abs(..)
				  "defaultAritmethics_float_exp",
		);
	} elsif(1 == 2) {	#! then evlauate for different 'pow' funciton calls:
	    $resultFile_midFix .= "adjust_pow";
	    @arrOf_bashParams = (
		);
	} elsif(1 == 2) {
	    $resultFile_midFix .= "rand";
	    @arrOf_bashParams = (
		"defaultAritmethics_rand_srand",
		"defaultAritmethics_rand_uniform",
		"defaultAritmethics_rand_bionom_1",
		"defaultAritmethics_rand_bionom_2",
		"defaultAritmethics_rand_bionom_3",
		"",
		);
	} elsif(1 == 2) {
	    # FIXME: consider dropping [beow]
	    # TODO: cosnider to include seprate evlaution-functions for: "http://en.cppreference.com/w/cpp/numeric/random(..)".
	    $resultFile_midFix .= "randGNU";
	    @arrOf_bashParams = (
		"defaultAritmethics_rand_srandom",
		"defaultAritmethics_rand_drand48", # drand48(void);
		# "defaultAritmethics_rand_", #  double erand48(unsigned short xsubi[3]);
		"defaultAritmethics_rand_lrand48", # long int lrand48(void);
		# long int nrand48(unsigned short xsubi[3]);
		"defaultAritmethics_rand_mrand48", #  #long int mrand48(void);
		#long int jrand48(unsigned short xsubi[3]);
		#void srand48(long int seedval);
		# unsigned short *seed48(unsigned short seed16v[3]);
		);	
	} elsif(1 == 2) {
	    # FIXME: consider dropping [beow]
	    $resultFile_midFix .= "randGnu";
	    @arrOf_bashParams = (
		#! FIXME: isntall [below], and compare with different ranom number generators https://www.gnu.org/software/gsl/manual/html_node/Unix-random-number-generators.html#Unix-random-number-generators
		""
		);
	} else {
	    croak("TODO: add support for this");
	}
	#!
	#! Set the row size:
	my @arrOf_nrows = (
	    #(62*2) 
	    #2*128
	    1*128
	    , 2*128
	    , 3*128
	    , 4*128
	    , 5*128
	    );
	if($isTo_useSmallData == 0) { #! ie, then a largr nrows-set.
	    @arrOf_nrows = (
		5*128
		, 10*128
		, 15*128
		, 20*128
		, 25*128
		#!
		);
	} else {
	    $resultFile_midFix .= "_small"; #! ie, to 'sperate this' from the 'default' row-sizes.
	}
	#! gcc ops_matrix.c -lm -DVECTOR_CONFIG_USE_FLOAT=0
	for(my $isTo_useFloat = 0; $isTo_useFloat < 2; $isTo_useFloat++) {
	    # FIXME: add ....s seprate comptuation for 'dobule' and 'float'
	    # FIXME: add .... the "-lm" param (to use mathamtcial libraryes).
	    my @arrOf_depend;
	    #! Call:
	    my $fixed_compileParams = "-lm";
	    if($isTo_useFloat) {
		$resultFile_midFix .= "_float"; 
	    } else { 
		$resultFile_midFix .= "_double";
		$fixed_compileParams .= " -DVECTOR_CONFIG_USE_FLOAT=0";
	    }
	    $resultFile_midFix .= "." . time();
	    eval_impl_euclid_cpp($resultFile_midFix, $file_main, \@arrOf_depend, \@arrOf_bashParams, \@arrOf_nrows, $fixed_compileParams);
	    ; # FIXME
	}
	#croak("FIXME: add suppor tfor this");
    } elsif(1 == 2) { #! time-cost of funciton inlining. 
	my $resultFile_midFix = "defaultAritmethics_";
	my $file_main = "ops_matrix.c";
	my @arrOf_depend;
	my @arrOf_bashParams;
	#!
	my $isTo_useSmallData = 0; #! whcih is used to toggle which row-siees to explore.
	if(1 == 1) {
	    #! Motivaiton: testing cost of function calls, and different inlining strateiges:
	    #! Note: [”elow] 'extends' Euclid (ie, 1-add + 1-mult) with the bleo waritmethci operaiotns:
	    #! 
	    $resultFile_midFix .= "funcCalls_sameFile_block_1Add";
	    #! Note: exepcted to be defined in our "dot_slow_funcCalls_sameFile.c", which uses loigcs in our "ops_matrix_slow_euclid.c"
	    #! Spefiy fucntiosn where the 'inner block' is in the same function.
	    #! Note: we do Not expect any sifngicant eprformance-differences when usign this appraoch: due to our exepciaotn that the blocks are sufivnly large (for effecitive hardware utlizaiotn).
	    #! #include "dot_slow_funcCalls_sameFile_blocks.c"
	    @arrOf_bashParams = (
		# FIXME: dicuss why 'inline refrences to pointeers' does Not compile.
		"dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default"
		, "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead"
		, "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer"
		, "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_static"
		, "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static"
		, "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer_static"
		, "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_inline"
		, "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline"
		);
	} elsif(1 == 2) {
	    @arrOf_bashParams = (
		# , " dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer_inline"
		"dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default"
		, "dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead"
		, "dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer"
		, "dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_static"
		, "dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static"
		, "dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer_static"
		, "dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_inline"
		, "dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline"
		);
	} elsif(1 == 2) {
	    croak("\t Add for 'inner-each'");
	    @arrOf_bashParams = (
		# , "dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer_inline"
		
		# "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_static",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_inline",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_inline_alwaysInline",
		# #! ---- 
		# "dot_slow_1Add_1Add_funcCalls_sameFile_inBlockEach_default",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_inBlockEach_default_funcPointer",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_inBlockEach_default_blockOverhead",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_inBlockEach_static",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_inBlockEach_inline_default",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_inBlockEach_inline_alwaysInline",
		# "dot_slow_1Add_1Add_funcCalls_sameFile_inBlockEach_inline_alwaysInline_funcPointer",
		#! --- 
		#"dot_slow_2Add_1Add_funcCalls_sameFile_innerBlock",
		#"dot_slow_2Add_1Add_funcCalls_sameFile_inBlockEach",	    
		)
		;
	} elsif(1 == 2) {
	    croak("\t Add for 'defined in a different file' ... ie, loading of objects");
	} else {
	    croak("TODO: add support for this");
	}
	#!
	#! Set the row size:
	my @arrOf_nrows = (
	    #(62*2) 
	    #2*128
	    1*128
	    , 2*128
	    , 3*128
	    , 4*128
	    , 5*128
	    );
	if($isTo_useSmallData == 0) { #! ie, then a largr nrows-set.
	    @arrOf_nrows = (
		5*128
		, 10*128
		, 15*128
		, 20*128
		, 25*128
		#!
		);
	} else {
	    $resultFile_midFix .= "_small"; #! ie, to 'sperate this' from the 'default' row-sizes.
	}
	#! gcc ops_matrix.c -lm -DVECTOR_CONFIG_USE_FLOAT=0
	for(my $isTo_useFloat = 0; $isTo_useFloat < 2; $isTo_useFloat++) {
	    # FIXME: add ....s seprate comptuation for 'dobule' and 'float'
	    # FIXME: add .... the "-lm" param (to use mathamtcial libraryes).
	    my @arrOf_depend;
	    #! Call:
	    my $fixed_compileParams = "-lm";
	    if($isTo_useFloat) {
		$resultFile_midFix .= "_float"; 
	    } else { 
		$resultFile_midFix .= "_double";
		$fixed_compileParams .= " -DVECTOR_CONFIG_USE_FLOAT=0";
	    }
	    $resultFile_midFix .= "." . time();
	    eval_impl_euclid_cpp($resultFile_midFix, $file_main, \@arrOf_depend, \@arrOf_bashParams, \@arrOf_nrows, $fixed_compileParams);
	    ; # FIXME
	}	
	; # FIXME
	#croak("FIXME: add suppor tfor this");    
    } elsif(1 == 1) { #! 
	; # FIXME
	croak("FIXME: add suppor tfor this");
    }
} #! end-of-"ops_matrix" case.

#! ***************************************************************************
#! ***************************************************************************
#! ***************************************************************************

{ #! Test: Strassen:
    my $resultFile_midFix = "strassen_";
    my $file_main = "strassen_main.c";
    my @arrOf_depend;
    @arrOf_compilerFlags = (
	"",
	"-funroll-all-loops");
    my @arrOf_bashParams;
    my $isTo_useSmallData = 0;
    if(1 == 2)  {
	@arrOf_bashParams = (
	    "fast_int_16bit-mult-64"
	    );
	$isTo_useSmallData = 0;
	$resultFile_midFix .= "fastMatrixMulSSE_int16";
    } elsif(1 == 2)  {
	@arrOf_bashParams = (
	    "fast_int_32bit-mult-64"
	    );
	$isTo_useSmallData = 0;
	$resultFile_midFix .= "fastMatrixMulSSE_int32";
    } elsif(1 == 1)  {
	@arrOf_bashParams = (
	    "fast_float-mult-64"
	    );
	$isTo_useSmallData = 0;
	$resultFile_midFix .= "fastMatrixMulSSE_float";	
    } elsif(1 == 2)  {
	@arrOf_bashParams = (
	    "fast_int_16bit-mult-64-xmtSSE"
	    );
	$isTo_useSmallData = 0;
	$resultFile_midFix .= "fastMatrixMulSSE_int16_xmtSSE";
    } elsif(1 == 2)  {
	@arrOf_bashParams = (
	    "fast_int_32bit-mult-64-xmtSSE"
	    );
	$isTo_useSmallData = 0;
	$resultFile_midFix .= "fastMatrixMulSSE_int32_xmtSSE";
    } elsif(1 == 1)  {
	@arrOf_bashParams = (
	    "fast_float-mult-64-xmtSSE"
	    );
	$isTo_useSmallData = 0;
	$resultFile_midFix .= "fastMatrixMulSSE_float_xmtSSE";		
    } elsif(1 == 2) { #! 
	$isTo_useSmallData = 1;
	$resultFile_midFix .= "naiveMatrixMul_int";
	@arrOf_bashParams = (
	    "matrix_naive_int_32bit"
	    );
    } elsif(1 == 2) { #! 
	$isTo_useSmallData = 1;
	$resultFile_midFix .= "naiveMatrixMul_float";
	@arrOf_bashParams = (
	    "matrix_naive_float"
	    );
    } elsif(1 == 2) { #! 
	$isTo_useSmallData = 1;
	$resultFile_midFix .= "strassen_float";
	@arrOf_bashParams = (
	    "strassen_float"
	    );	
    } elsif(1 == 1) { #! 
	$isTo_useSmallData = 1;
	$resultFile_midFix .= "strassen_int_32bit";
	@arrOf_bashParams = (
	    "strassen_int_32bit"
	    );		
    } elsif(1 == 1) { #! 
	; # FIXME
	croak("FIXME: add suppor tfor this");
    }
    {
	#!
	#! Set the row size:
	my @arrOf_nrows = (	    1*128 	    , 2*128 	    );
	if($isTo_useSmallData == 0) { #! ie, then a largr nrows-set.
	    @arrOf_nrows = ();
	    for(my $i = 19; $i < 20; $i++) {
		push(@arrOf_nrows, 128 * $i * 1);
		#push(@arrOf_nrows, 128 * $i * 2);
	    }
	    #croak("..@arrOf_nrows.");
	} else {
	    $resultFile_midFix .= "_small"; #! ie, to 'sperate this' from the 'default' row-sizes.
	    @arrOf_nrows = ();
	    for(my $i = 1; $i < 2; $i++) {
		push(@arrOf_nrows, 128 * $i * 5);
#		push(@arrOf_nrows, 128 * $i * 10);
	    }	    
	}
	#! gcc ops_matrix.c -lm -DVECTOR_CONFIG_USE_FLOAT=0
	for(my $isTo_useFloat = 0; $isTo_useFloat < 2; $isTo_useFloat++) {
	    # FIXME: add ....s seprate comptuation for 'dobule' and 'float'
	    # FIXME: add .... the "-lm" param (to use mathamtcial libraryes).
	    my @arrOf_depend;
	    #! Call:
	    my $fixed_compileParams = "-lm";
	    my $resultFile_midFix_local = $resultFile_midFix; #! ie, use the same 'base' ofr every run.
	    if($isTo_useFloat) {
		$resultFile_midFix_local .= "_float"; 
	    } else { 
		$resultFile_midFix_local .= "_double";
		$fixed_compileParams .= " -DVECTOR_CONFIG_USE_FLOAT=0";
	    }
	    $resultFile_midFix_local .= "." . time();
	    #croak("..@arrOf_nrows.");
	    foreach my $strategy ("O2", "O3") { #! then seperate image-cosntruciton between the different strategies: 
		$resultFile_midFix_local .= $strategy;
		@arrOf_genericCompilerStrategy = ( "-" . $strategy);
		eval_impl_euclid_cpp($resultFile_midFix_local, $file_main, \@arrOf_depend, \@arrOf_bashParams, \@arrOf_nrows, $fixed_compileParams);
	    }
	}	
    }
} #! end-of-"strassen_main" case.

# FIXME: ... when [above] is completed ... generate figures ... then explore different combaintiosn 'of the apramter spce' ... to see if it is psosil to get an even higher speed-up. 
    # FIXME[use-case]:: to what extend is time-comparison in debug-mode rrpesentative for the time-improvements in optmzied modes (ie, as daily devopment + testing is noramlly perofmred in debug mode)??
