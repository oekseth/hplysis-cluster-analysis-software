#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_memAlloc.h"
#include "def_typeOf_float.h"
#include "def_intri.h"
#include "kt_mathMacros.h"
#include "types_base.h"
#include "types.h"

#ifndef macro_pluss
#define macro_pluss(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_add
#define macro_add(term1, term2) ({term1 + term2;})
#endif
#ifndef macro_minus
#define macro_minus(term1, term2) ({term1 - term2;})
#endif
#ifndef macro_mul
#define macro_mul(term1, term2) ({term1 * term2;})
#endif
//! -------------------------------------------------------------------------------
//!
//! Type-independent speficiations of functions:
#define t_tileType t_float
#define t_tileType_alloc allocate_2d_list_float
#define t_tileType_free free_2d_list_float
#define s_matrix_tile s_matrix_tile_float
#define s_matrix_tile_t s_matrix_tile_float_t
#define init__s_matrix_tile_t init__s_matrix_tile_t_float
#define free__s_matrix_tile_t free__s_matrix_tile_t_float
#define s_matrix_tile_setOf s_matrix_tile_setOf_float
#define init__s_matrix_tile_setOf_t init__s_matrix_tile_setOf_t_float
#define free__s_matrix_tile_setOf_t free__s_matrix_tile_setOf_t_float
#define Euclid__s_matrix_tile_setOf_t Euclid__s_matrix_tile_setOf_t_float
//! -------------------------------------------------------------------------------

/**
   @struct
   @brief 
   @remarks motivation is to test if incrased localibyt in the data processin speeds up the computation.
 **/
typedef struct s_matrix_tile {
  t_tileType **matrix; //!< the tiled matrix.
  uint nrows; //! the tiled number of rows in the matrix.
  uint ncols; //! the tiled number of columns in the matrix.
  uint nrows_start;
  // uint ncols_start;
} s_matrix_tile_t;

/**
   @brief 
 **/
s_matrix_tile_t init__s_matrix_tile_t(t_tileType **matrix, const uint blockSize_1, const uint blockSize_2, t_tileType paddingValue, const uint row_start,  const uint col_start, uint nrows, const uint ncols) {
  s_matrix_tile_t self = {NULL, blockSize_1, blockSize_2, row_start};
  //! Allocate:
  self.matrix = t_tileType_alloc(blockSize_1, blockSize_2, paddingValue);
  //! 
  //! Idnetify the number, and type, of entitesi to copy:
  uint ncols_cpySize = blockSize_2;
  if((col_start + blockSize_2) > ncols) {
    ncols_cpySize = ncols - col_start;
  }
  nrows = macro_min(nrows, (row_start + blockSize_1));
  // ncols = macro_min(ncols, (col_start + blockSize_2));
  //! 
  //! Copy:
  uint row_id_local = 0;
  for(uint row_id = row_start; row_id < nrows; row_id++) {
    assert(matrix[row_id]);
    assert(self.matrix[row_id_local]);
    assert(row_id_local < blockSize_1);
    memcpy(self.matrix[row_id_local], &(matrix[row_id][col_start]), ncols_cpySize);
    //! 
    row_id_local++;
  }  
  //!
  return self;
}

/**
   @brief de-allocate the oject
 **/
void free__s_matrix_tile_t(s_matrix_tile_t *self) {
  assert(self);
  assert(self->matrix);
  if(self->matrix) {
    t_tileType_free(&(self->matrix), self->nrows);
    self->nrows = self->ncols = 0;
    self->matrix = NULL;
  }
}



/**
   @struct
   @brief 
 **/
typedef struct s_matrix_tile_setOf {
  s_matrix_tile_t **tiles;
  uint nrows; //! the tiled number of rows in the matrix
  uint ncols; //! the tiled number of columns in the matrix
  uint tiles_nrows; //! the tiled number of rows in the tiled-matrix-collection.
  uint tiles_ncols; //! the tiled number of columns in the tiled-matrix-collection.  
} s_matrix_tile_setOf_t;

/**
   @brief 
 **/
s_matrix_tile_setOf_t init__s_matrix_tile_setOf_t(t_tileType **matrix, const uint nrows, const uint ncols, const uint blockSize_1, const uint blockSize_2, t_tileType paddingValue) {
  s_matrix_tile_setOf_t self = {NULL, nrows, ncols, 0, 0};
  self.tiles_nrows = nrows / blockSize_1;
  self.tiles_ncols = ncols / blockSize_2;
  //! Allocate:
  alloc_generic_type_2d(s_matrix_tile_t, self.tiles, self.tiles_nrows); 

  //! Copy:
  assert(self.tiles);
  //!
  //!
  assert((nrows % blockSize_1) == 0); // TODO: add a hanlder when this assumåption fails.
  assert((ncols % blockSize_2) == 0); // TODO: add a hanlder when this assumåption fails.    
  uint row_start =  0;
  for(uint block_1 = 0; block_1 < self.tiles_nrows; block_1++) {
    //! Allocate: 
    self.tiles[block_1] = alloc_generic_type_1d_xmtMemset(s_matrix_tile_t, self.tiles[block_1], self.tiles_ncols);
    assert(self.tiles[block_1]);
    uint col_start = 0;
    //! Allocate + fill: 
    for(uint block_2 = 0; block_2 < self.tiles_ncols; block_2++) {
      //! Allocate: 
      self.tiles[block_1][block_2] = init__s_matrix_tile_t(matrix, blockSize_1, blockSize_2, paddingValue, row_start, col_start, nrows, ncols);
      col_start += blockSize_2;
    }
    row_start += blockSize_1;
  }
  //!
  //!  
  return self;
}


/**
   @brief de-allocate
 **/
void free__s_matrix_tile_setOf_t(s_matrix_tile_setOf_t *self) {
  assert(self);
  assert(self->tiles);
  for(uint block_1 = 0; block_1 < self->tiles_nrows; block_1++) {
    assert(self->tiles[block_1]);  
    for(uint block_2 = 0; block_2 < self->tiles_ncols; block_2++) {
      free__s_matrix_tile_t(&(self->tiles[block_1][block_2]));
    }
    assert(self->tiles[block_1]);
    free_generic_type_1d(self->tiles[block_1]);
    self->tiles[block_1] = NULL;
  }
  free_generic_type_2d(self->tiles);
  //  free_generic_type_2d(s_matrix_tile_t, self->tiles, self->tiles_nrows);
  self->tiles = NULL;
  self->tiles_nrows = 0; self->tiles_ncols = 0;
}



/**
   @brief compute Euclid.
   @remarks 
   - default: dim:[10, 10], implies 10*10*100=1000+ operations;
   - this w/block-size=5: 
   -- block-evaluations: blocks=10/5=2 => 2*2*2 = 8
   -- each block1 x block2: dim=[5,5] => 5*5*5 = 125;
   -- total: 125*8 = 1000
 **/
void Euclid__s_matrix_tile_setOf_t(s_matrix_tile_setOf_t *self, t_tileType **mat_result) {
  assert(self);
  assert(self->tiles);
  for(uint block_1a = 0; block_1a < self->tiles_nrows; block_1a++) {
    //    for(uint block_1b = 0; block_1b < self.tiles_ncols; block_1b++)
      {
      for(uint block_2a = 0; block_2a < self->tiles_nrows; block_2a++) {
	for(uint block_2b = 0; block_2b < self->tiles_ncols; block_2b++) {
	  //! --------------------      
	  const s_matrix_tile_t block_1 = self->tiles[block_1a][block_2b];
	  const s_matrix_tile_t block_2 = self->tiles[block_2a][block_2b];
	  //! --------------------
	  //! Intersect the two blocks:
	  for(uint int_row_id = 0; int_row_id < block_1.nrows; int_row_id++) {
	    const t_tileType *__restrict__ row_1 = block_1.matrix[int_row_id];
	    //const t_tileType *__restrict__ row_1 = row_1_base + int_row_id;
	    for(uint int_col_id = 0; int_col_id < block_2.nrows; int_col_id ++) {
	      const t_tileType *__restrict__ row_2 = block_2.matrix[int_col_id];
	      //! Iterate through the column:
	      // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	      uint int_j = 0; t_tileType vec_result = macro_mul(row_1[int_j], row_2[int_j]);
	      //uint int_j = 0; VECTOR_T_FLOATYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	      int_j += 1;
	      for(; int_j < block_1.ncols; int_j += 1) {
		vec_result = macro_pluss(vec_result, macro_mul(row_1[int_j], row_2[int_j]));
		//	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])));
	      }
	      //! Update the partial result:
	      // FIXME: drop [”elow] asserts.
	      // assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	      mat_result[block_1.nrows_start + int_row_id][block_2.nrows_start + int_col_id] += vec_result;      
	    }
	  }
	}
      }
    }
  }
}

#if false
#endif

int main() {  }

//! ---------------------------------------------------------------------
//!
//! Reset:
#undef t_tileType
#undef t_tileType_alloc
#undef t_tileType_free
#undef s_matrix_tile
#undef s_matrix_tile_t
#undef init__s_matrix_tile_t
#undef free__s_matrix_tile_t
#undef s_matrix_tile_setOf
#undef init__s_matrix_tile_setOf_t
#undef free__s_matrix_tile_setOf_t
#undef Euclid__s_matrix_tile_setOf_t
