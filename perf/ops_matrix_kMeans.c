#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_typeOf_float.h"
#include "def_intri.h"
#include "kt_mathMacros.h"
#include "types_base.h"
#include "types.h"
#include "e_kt_correlationFunction.h"
#include "kt_matrix_base.c"
#define not_include_headerFiles //! to avoid a complex chain of compilations.
// #include "s_kt_correlationConfig.h"

#include "math_generateDistribution.c"
//#if(false) // FIXME: remove.

#include "log_clusterC.c" //! ie, the code for the log-functianlity. 
#include "kt_randomGenerator_vector.c"



//! Allcoate a distance-matrix and an assicated mask-set.
uint maskAllocate__makedatamask(const uint nrows, const uint ncols, float*** data, char*** mask, const bool isTo_use_continousSTripsOf_memory) { 
  assert(nrows != 0);
  assert(ncols != 0);
  const uint total_lenght = nrows * ncols;
  
  float **pdata = NULL; char **pmask = NULL;

  assert(nrows < 100000000); //! else a try-catch-block may be of itnerest.b


  if(isTo_use_continousSTripsOf_memory) {
    pdata = allocate_2d_list_float(nrows, ncols, /*empty-value=*/0);
    // FIXME: consider to incldue [”elow].
    pmask = allocate_2d_list_char(nrows, ncols, /*empty-value=*/0);    
  } else { // Then we use a sub-optimal memory-allocaiton-appraoch:
    pdata = alloc_generic_type_2d(t_float, pdata, nrows);
    pmask = alloc_generic_type_2d(char, pmask, nrows);
/* #ifdef __cplusplus */
/*     pdata = new float*[nrows]; // pmask = new char*[nrows]; //pmask = new char*[nrows]; */
/* #else  */
/*     pdata = (float*)malloc(sizeof(float*)*nrows); */
/*     // FIXME: consider to incldue [”elow]. */
/*     pmask = (uint*)malloc(sizeof(uint*)*nrows); */
/* #endif */
    for(uint i = 0; i < nrows; i++) {      
      //printf("\t i=%u, given n=%u, at %s:%d\n", i, nrows, __FILE__, __LINE__);
      pdata[i] = allocate_1d_list_float(ncols, /*empty-value=*/0);
      pmask[i] = allocate_1d_list_char(ncols, /*empty-value=*/0);
      // FIXME: consider to incldue [”elow].
    }
  }

  //! Update:
  *data = pdata; *mask = pmask;

  //! @return true upon success.
  return 1;
}

/* ---------------------------------------------------------------------- */

//! De-allocate the data allcoated in "makedatamask(..)"
void maskAllocate__freedatamask(const uint n, float** data, char** mask, const bool isTo_use_continousSTripsOf_memory) { 
  assert(data);
  //assert(mask);  
  if(isTo_use_continousSTripsOf_memory) {
    free_2d_list_float(&data, n);
    if(mask) {
      free_2d_list_char(&mask, n);
    }
  } else { // Then we use a sub-optimal memory-allocaiton-appraoch:
    for(uint i = 0; i < n; i++) {
      free_1d_list_float(&data[i]);
      if(mask) {free_1d_list_char(&mask[i]);}
    }
#ifdef __cplusplus
    delete [] data; if(mask) {delete [] mask;}
#else
    free(data); if(mask) free(mask);
#endif
  }
}



/**
   @brief compute the weighted "City Block" distance between two rows or columns in a matrix. 
   @remarks City Block distance is defined as the absolute value of X1-X2 plus the absolute value of Y1-Y2 plus..., which is equivalent to taking an "up and over" path.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float __kt_cityblock_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], const uint index1, const uint index2, const uint transpose) {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  t_float result = 0.;   t_float tweight = 0;
  if(transpose == 0) {/* Calculate the distance between two rows */
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) {
	const t_float term = data1[index1][i] - data2[index2][i];
	const t_float weight_local = (weight) ? weight[i] : 1;
        result = result + weight_local*mathLib_float_abs(term);
        tweight += weight_local;
      }
    }
  } else {
    //! Note[memory-access-patterns]: (same as for "graphAlgorithms_distance::spearman(..)");
    for(uint i = 0; i < n; i++) {
      if( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) {
	const t_float term = data1[i][index1] - data2[i][index2];
	const t_float weight_local = (weight) ? weight[i] : 1;
        result = result + weight_local*mathLib_float_abs(term);
        tweight += weight_local;
      }
    }
  }
  if(!tweight) return 0; /* usually due to empty clusters */
  result /= tweight;
  return result;
}


/**
   @brief compute the weighted euclidian distance between two rows or columns in a matrix.
   @remarks the mask1 and mask2 parameters are used to indicate vectors/rows/columns which are missing: set ot "0" ifthe value is missing.
   @remarks Memory-access-pattern of this function is:  (same as for "graphAlgorithms_distance::spearman(..)");
 **/
t_float __kt_euclid_slow(uint n, t_float** data1, t_float** data2, char** mask1, char** mask2, const t_float weight[], uint index1, uint index2, uint transpose)  {
  // FIXME: write an 'itnroduction' to [below] algorithm using wikiepdias simplified description;
  // FIXME: write an assert-class text-fucntion for this funciton;
  // FIXME: write a 'time-benchmark' for this fucniton, ie, to assess the relationship between topology, data-size and time-consumption.
  assert(data1);   assert(data2);
  t_float result = 0.;
  t_float tweight = 0;
  if(transpose == 0) { /* Calculate the distance between two rows */
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare two lists: same number of cache-misses for both the 'default' "C-clustering-library" memorya-allcoation-rotuien and KnittingTools 'improved'/'ideal' memory-allocation-scheme.    
    for(uint i = 0; i < n; i++)       {
      if( ( (!mask1 || mask1[index1][i]) && (!mask2 || mask2[index2][i]) ) ) {	
	//if(isOf_interest(data1[index1][i]) && isOf_interest(data2[index2][i]) ) 
	{
	  const t_float term = data1[index1][i] - data2[index2][i];
	  const t_float weight_local = (weight) ? weight[i] : 1;
	  result += weight_local*term*term;
	  tweight += weight_local;
	}
      }
    }
  } else {
    //! Note[memory-access-patterns]: one for-loop which sequentially for each index compare the same column in mulitple row: if the column-size does not signficantly exceed the "L2" memory-cache-size (for a given computer-memory-hardware) then we expect an 'ideal' memory-allocation-scheme to provide/give a "linear" "n" speed-up (for every call to this function).
    for(uint i = 0; i < n; i++) {
      if( ( (!mask1 || mask1[i][index1]) && (!mask2 || mask2[i][index2]) ) ) {
	//if(isOf_interest(data2[i][index1]) && isOf_interest(data2[i][index2]) )
	  {
	  t_float term = data1[i][index1] - data2[i][index2];
	  const t_float weight_local = (weight) ? weight[i] : 1;
	  result += weight_local *term*term;
	  tweight += weight_local;
	}
      }
    }
  }
  if(!tweight) return 0; /* usually due to empty clusters */
  result /= tweight;
  return result;
}


void getclustermeans_slow(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp, const uint transpose) {
#include "func_getclustermeans_slow.c" //! ie, hte function in quesiton.
}


static void getclustermeans_fast(const uint nclusters, const uint nrows, const uint ncolumns, t_float** data, char** mask, uint clusterid[], t_float** cdata, char **cmask, t_float **cmask_tmp, const bool mayUse_zeroAs_emptySign_inComputations, const bool needTo_useMask_evaluation) {
  //! Intiate the dummy/temproary list:
  assert(cmask_tmp);
  for(uint i = 0; i < nclusters; i++) {
    memset(cmask_tmp[i], 0, sizeof(t_float)*ncolumns);
  }
    //    memset(cmask_tmp[0], 0, sizeof(t_float)*ncolumns*nclusters);

#if(optimize_parallel_useNonSharedMemory_MPI == 1)
  #error "Update this code-chunk with new/updated code" 
#endif

  //! Intialise:
  if(!cmask) {
    // FIXME: udpate our systentic benchmarks to evaluate the performance-difference.
    for(uint i = 0; i < nclusters; i++) {
      memset(cdata[i], 0, sizeof(t_float)*ncolumns);
    }
    //memset(cdata[0], 0, sizeof(t_float)*nclusters*ncolumns);
  } else {
    assert(cdata);
    //printf("ncols=%u, nclusters=%u, at %s:%d\n", ncolumns, nclusters, __FILE__, __LINE__);
    for(uint i = 0; i < nclusters; i++) {
      memset(cdata[i], 0, sizeof(t_float)*ncolumns);
    }
    if(cmask) {
      for(uint i = 0; i < nclusters; i++) {
	memset(cmask[i], 0, sizeof(char)*ncolumns);
      }
    }
    /* memset(cdata[0], 0, sizeof(t_float)*ncolumns*nclusters); */
    /* if(cmask) {memset(cmask[0], 0, sizeof(char)*ncolumns*nclusters);} */
  }
  //! Update the sum of weights assicated to each cluster wrt. a cells (head, tail) weights.
  const t_float localConfig__empty_value = (mayUse_zeroAs_emptySign_inComputations) ? 0 : T_FLOAT_MAX;

    
  // FIXME: add parallisation to [below] <-- how may we 'handle' the udpate of the variabesl "cdata" and "cmask_tmp"? <--- consider to use an outer "forr(uint i = 0; i < nclusters; i++)" loop to identify 'interesting' nrow-varaibles ... which woulde cause an overhead of ...??...  <-- instead use seperate containers for each of the treads ... and thereafter emrge the containers. <-- given teh low comptautional compelxity 'of this' consider 'omit such'.
  const uint ncolumns_intri = VECTOR_FLOAT_maxLenght_forLoop(ncolumns);
  const uint ncolumns_intri_mask = VECTOR_CHAR_maxLenght_forLoop(ncolumns);
  for(uint k = 0; k < nrows; k++) {
    // FIXME: try to write a perofrmance-test whe we figure out "how mcuh memory-ovehrehad need to be assicated to cahc-misses for 'sroting the index-lookups-table'  to have a perofmrance-point" (eosekth, 06. june 2016)
    if(clusterid[k] > nclusters) {clusterid[k] = nclusters -1;} //! ie, to fix any such issues (oesketh, 06. feb. 2017).
    const uint i = clusterid[k];
    assert_possibleOverhead(i != UINT_MAX);
    assert_possibleOverhead(i < nclusters);
    
    uint j = 0;
    /* if(needTo_useMask_evaluation == false) {       */
    /*   if(ncolumns_intri > 0) { */
    /* 	for(; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) { */
    /* 	  VECTOR_FLOAT_storeAnd_add_data(&cdata[i][j], &data[k][j]); //! ie, then incremnet cdata with the valeus in data */
    /* 	  VECTOR_FLOAT_storeAnd_add(&cdata[i][j], VECTOR_FLOAT_setFrom_scalar(1)); //! ie, increment for all indeices. */
    /* 	} */
    /*   } */
    /*   for(; j < ncolumns; j++) { */
    /* 	cdata[i][j] += data[k][j]; */
    /* 	cmask_tmp[i][j]++; //! ie, mark the centroid 'as of interest'. */
    /*   } */
    /* } else  */
    {
      if(!mask) { //! Thwn we evaluate wrt. the T_FLOAT_MAX attribute.
	/* if(ncolumns_intri > 0) { */
	/*   for(; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) { */
	/*     VECTOR_FLOAT_TYPE vec_mask; */
	/*     VECTOR_FLOAT_storeAnd_add(&cdata[i][j], VECTOR_FLOAT_dataMask_data1(&data[k][j], vec_mask)); //! ie, then incremnet cdata with the valeus in data if T_FLOAT_MAX is not in the data-set. */
	/*     VECTOR_FLOAT_storeAnd_add(&cmask_tmp[i][j], vec_mask); //! ie, mark the centroid 'as of interest'. */
	/*   } */
	/* } */
	for(; j < ncolumns; j++) {
	  const t_float score = data[k][j];
	  if(isOf_interest(score)) {
	    cdata[i][j] += score;
	    cmask_tmp[i][j]++; //! ie, mark the centroid 'as of interest'.
	  }
	}
      } else { //! then we use the 'char' mask-array:
	/* if(ncolumns_intri > 0) { */
	/*   for(; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) { */
	/*     VECTOR_FLOAT_TYPE vec_mask; */
	/*     VECTOR_FLOAT_storeAnd_add(&cdata[i][j], VECTOR_FLOAT_charMask_data1(&data[k][j], mask[k], j, vec_mask)); //! ie, then incremnet cdata with the valeus in data if mask is set for the cells in quesiton. */
	/*     VECTOR_FLOAT_storeAnd_add(&cmask_tmp[i][j], vec_mask); //! ie, mark the centroid 'as of interest'. */
	/*   } */
	/* } */
	for(; j < ncolumns; j++) {
	  if(mask[k][j]) {
	    cdata[i][j] += data[k][j];
	    cmask_tmp[i][j]++; //! ie, mark the centroid 'as of interest'.
	  }
	}
      }
    }
  }
  //! Infer the 'midpoints' for each centroid:
  for(uint i = 0; i < nclusters; i++) {
    uint j = 0;
    /* if(mayUse_zeroAs_emptySign_inComputations) { */
    /*   if(ncolumns_intri > 0) { */
    /* 	for(; j < ncolumns_intri; j += VECTOR_FLOAT_ITER_SIZE) {	     */

    /* 	  assert(false); // FIXME: validate that the 'dvidye-by-zero' case is correctly handled <--- try to use an 'xor' case */
    /* 	  VECTOR_FLOAT_storeAnd_div_self_data(&cdata[i][j], &cmask_tmp[i][j]); //! ie, then: cdata[i][j] /= cmask_tmp[i][j]; */
    /* 	} */
    /*   }  */
    /* } */
    if(cmask) { //! then we use a seperate for-loop:
      uint j = 0;
      //! TODO[JC]: consider including [”elow] ... which is current 'commented out' ... challenge is that the the dfiference data-type-sizes, ie, where "cmask" is in in "t_float" while "cmask" is in "char"
      // if(ncolumns_intri_mask > 0) {
      // 	for(; j < ncolumns_intri; j += VECTOR_CHAR_ITER_SIZE) {
      // 	  assert(false); // FIXME: add soemthing	  <--- call our VECTOR_CHAR_getBoolean_aboveZero
      // 	  //VECTOR_CHAR_storeAnd_add(&cmask[i][j], VECTOR_CHAR_convertFrom_t_float(VECTOR_FLOAT_getBoolean_aboveZerocmask_tmp[i], /*pos-dynamic=*/j));
      // 	  VECTOR_CHAR_storeAnd_add(&cmask[i][j], VECTOR_CHAR_convertFrom_t_float_toBoolean(cmask_tmp[i], /*pos-dynamic=*/j));
      // 	}
      // }
      for(; j < ncolumns; j++) {
	if(cmask_tmp[i][j] > 0) {
	  cmask[i][j] = 1;
	}
      }
    }

    {
      for(; j < ncolumns; j++) {
	// FIXME: seperate 'maks' alterantives ... and use _mm_storeu_ps(..)
	if(cmask_tmp[i][j] > 0) {
	  cdata[i][j] /= cmask_tmp[i][j]; //! ie, infer the average-value/'center' of each centroid.
	  //if(cmask) {}
	} else {
	  cdata[i][j] = localConfig__empty_value;
	  if(mayUse_zeroAs_emptySign_inComputations == false) {
	  /*   cdata[i][j] = 0; */
	  /* } else { */
	  /*   cdata[i][j] = T_FLOAT_MAX; */
	    // FIXME: validate [”elow] ... and if [”elow] does not hold .. then update our itanlizaiton-procedure.
	    if(mask) {assert(!mask[i][j]);}
	    assert(isOf_interest(cdata[i][j]) == false);
	  }
	}
      }
    }
  }
}



void __ops_matrix_kMeans(const uint nclusters, const uint nrows, const uint ncols, t_float** data, const uint npass, s_kt_randomGenerator_vector_t *obj_rand, uint mapping[], uint clusterid[], uint counts[], s_log_clusterC_t *logFor_kMeans, t_float *error) 
{ //! the kmeans-algorithm
  start_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMeans_all); //! ie, intiate the object.
  start_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMeans_init); //! ie, intiate the object.
  
  const uint ndata = ncols; // (transpose == 0) ? ncols : nrows;
  const uint nelements = nrows; 
  // FIXME: de-allcoate [”elow]
  t_float** cdata = NULL;   char **cmask = NULL;
  const bool isTo_use_continousSTripsOf_memory = true;
  bool ok = maskAllocate__makedatamask(nclusters, ndata, &cdata, &cmask, true);
  assert(ok);
  
  *error = T_FLOAT_MAX;
  uint ifound = 1;
  uint ipass = 0; //! the number of iterations/'passes' in the iterative algorithm: should be less than the user-defined "npass"

  /* We save the clustering solution periodically and check ifit reappears */
  uint *arrOf_saved = allocate_1d_list_uint(nelements, UINT_MAX);

  t_float **cmask_tmp = NULL;
  //  if(cmask) 
  {
    const uint ndata = ncols; 
    cmask_tmp = allocate_2d_list_float(nclusters, ndata, 1);
  }
  end_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMeans_init); //! ie, intiate the object.
  start_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMean_iteration_all); //! ie, intiate the object.
  //!
  //! 
  do { 
    t_float total = T_FLOAT_MAX;    uint counter = 0;     uint period = 100; //! where "period" seems Not to inlfuce the cost of exeuction-time

    /* Perform the EM algorithm. First, randomly assign elements to clusters. */
    //! Note: if there is only one iteration, then we assume the user has a 'default' assumption of the clusters.
    start_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMean_iteration_randomAssign); //! ie, intiate the object.

    //! ...................
    if(npass != 0) {
      const bool isToUse__randomziaitonInFirstIteration = isToUse__randomziaitonInFirstIteration__s_kt_randomGenerator_vector_t(obj_rand);
      if( (ipass > 0) || (isToUse__randomziaitonInFirstIteration == true) ) {
	//!
	//! Re-compute randomness::
	randomassign__speicficType__math_generateDistributions(obj_rand);	
      } else {//! else we asusme the cluster-dids are pre-allocated (oekseth, 06. jan. 2017).
	assert_possibleOverhead(nelements <= obj_rand->nelements);
	for(uint i = 0; i < nelements; i++) {
	  const uint cluster_id = obj_rand->mapOf_columnToRandomScore[i];
	  assert(cluster_id != UINT_MAX);
	  assert(cluster_id <= nclusters); //! ie, to simplify debugging
	  assert(cluster_id < nclusters);
	}
      }
    }
    if(clusterid[0] == UINT_MAX) { //! ie, then assign an 'intal' assignemtn:
      assert(nclusters > 1);
      assert(obj_rand->mapOf_columnToRandomScore != NULL);
      for(uint j = 0; j < nelements; j++) {clusterid[j] = obj_rand->mapOf_columnToRandomScore[j];}
    }
    //! ...................
    end_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMean_iteration_randomAssign); //! ie, intiate the object.

    //! Intalize, frist wrt. a 'general reset', and then update the coutns wrt. the number of members in each cluster:
    for(uint i = 0; i < nclusters; i++) counts[i] = 0;
    assert(obj_rand->mapOf_columnToRandomScore);
    assert(obj_rand->nelements == nelements);
    for(uint i = 0; i < nelements; i++) {
      const uint cluster_id = obj_rand->mapOf_columnToRandomScore[i];
      assert(cluster_id != UINT_MAX);
      assert(cluster_id <= nclusters); //! ie, to simplify debugging
      assert(cluster_id < nclusters);
      counts[cluster_id]++;
    }
    //! Iterativly suggest new cluster-vertex-centroids, re-allocate the vertices to 'closer' clusters, and then continue 'the wile-lopp' until there is no longer an improvment in the 'total distance to cluster-vertex-centroids':
    const uint transpose = false;
    while(true) {
      start_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMean_iteration_findCenter); //! ie, intiate the object.


      //! Regularily save teh resutls: for each 'period' in the iteration save the cluster-ids:
      if(counter % period == 0) {/* Save the current cluster assignments */
	for(uint i = 0; i < nelements; i++) arrOf_saved[i] = obj_rand->mapOf_columnToRandomScore[i];
        if(period < INT_MAX / 2) period *= 2;
      }
      //! Intalise this 'run':
      const t_float previous = total;
      total = 0.0; counter++;

      //! Identify the 'center-vertex' of each cluster.
      //! Note: the below approaches produces reuslts in approx. the same exeuciton-time (oekseht, 06. aug. 2018).
      if(true) {
	//! Note: for "nrows=ncols=10,000" the improvement of [below] is insigificant ... 
	const bool mayUse_zeroAs_emptySign_inComputations = true; //! A heursitcs, where correctness is based on the observaiton that many metrics, such as 'euclid' and cityblock', would procue correct results.
	const bool needTo_useMask_evaluation = false;
	getclustermeans_fast(nclusters, nrows, ncols, data, /*mask=*/NULL, clusterid, cdata, cmask, cmask_tmp, mayUse_zeroAs_emptySign_inComputations, needTo_useMask_evaluation);
      } else {
	getclustermeans_slow(nclusters, nrows, ncols, data, /*mask=*/NULL, clusterid, cdata, cmask, cmask_tmp, transpose);
      }
      //! Update timer:
      end_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMean_iteration_findCenter); //! ie, intiate the object.
      start_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMean_iteration_findAllDistances); //! ie, intiate the object.

      //! 
      //! Iterate throguh all the vertices, ie, to idetnify the 'cluster-assigment with minmal error':
      { //! Thn either use the [ªbov€] computed valeus, or an comptue 'dsitnaces in the loop':	
	assert(obj_rand->mapOf_columnToRandomScore);
	for(uint i = 0; i < nelements; i++) {
	  const uint k = obj_rand->mapOf_columnToRandomScore[i];
	  if(counts[k]==1) continue; //! ie, as the cluster then may not be 'imrpvoed' wrt. the centroid-vertex (in a given cluster).
	  t_float distance = 0;
	  if(true) {
	    distance = __kt_cityblock_slow(ncols, data, cdata, NULL, NULL, NULL, i, k, transpose);
	  } else {
	    distance = __kt_euclid_slow(ncols, data, cdata, NULL, NULL, NULL, i, k, transpose);
	  }
	  //! --------------------------------------------------------------------------------------------------------
	  //! 
	  //! Then make  use [ªbove] inferred distance:
	  //! Iterate through all the clusters: at max "n/2" vertices/clusters to evalaute:
	  for(uint j = 0; j < nclusters; j++) {
	    if(j==k) continue; //! then the centroid-id is the same as the 'original'/'default' vertex-centrodi relationship/cluster-assignment.
	    const uint centroid_vertex = j;
	    char *mask2_vector = (!cmask) ? NULL : cmask[centroid_vertex];
	    const uint index1 = i; const uint index2 = centroid_vertex;
	    //if(index1 != index2) 
	    {
	      
	      t_float tdistance = T_FLOAT_MAX;
	      const uint transpose = false;
	      if(true) {
		tdistance = __kt_cityblock_slow(ncols, data, cdata, NULL, NULL, NULL, index1, index2, transpose);
	      } else {
		tdistance = __kt_euclid_slow(ncols, data, cdata, NULL, NULL, NULL, index1, index2, transpose);
	      } 	      
	      //! Then make  use [ªbove] inferred distance:	      
	      if((distance == T_FLOAT_MAX) || ( (tdistance < distance) && (tdistance != T_FLOAT_MAX) ) ) { //! then we have a better/improved cluster-assignment: update the cluster-id for vertex "i":
		// FIXME: instead of [”elow] use _mm_storeu_ps
		if(tdistance != T_FLOAT_MAX) {
		  distance = tdistance;
		  assert(counts[obj_rand->mapOf_columnToRandomScore[i]] > 0); //! ie, to avoid a negative count, where th elatter woudl be a cricla bug wrt. "uint" dat-atype.
		  counts[obj_rand->mapOf_columnToRandomScore[i]]--; //! ie, as the vertex is 'remvoved' from the current/given cluster.
		  obj_rand->mapOf_columnToRandomScore[i] = j;
		  counts[j]++;
		}
	      }
	      /* if(matrix2d_vertexTo_clusterId != NULL) { //! then we update the distance between teh cluster-and-the-vertex: */
	      /* 	assert_possibleOverhead(matrix2d_vertexTo_clusterId[/\*vertex=*\/i]); */
	      /* 	const uint icluster = index2; */
	      /* 	// matrix2d_vertexTo_clusterId[/\*vertex=*\/i][/\*cluster-id=*\/icluster] = macro_min(matrix2d_vertexTo_clusterId[/\*vertex=*\/i][/\*cluster-id=*\/icluster], tdistance); */
	      /* } */
	    }
	  }
	  //! Update the 'sum of distances' with the shortest/best distance found between each vertex and cluster-centroid-vertex:
	  if( (distance != T_FLOAT_MAX) && (distance != T_FLOAT_MIN_ABS) ) {
	    assert(distance != T_FLOAT_MAX);
	    total += distance;
	  }
	} //! and at this exeuction-point we asusme the algorithm has covnerged.
      }
      end_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMean_iteration_findAllDistances); //! ie, intiate the object.

      //! Note: in [”elow] the ">="  is used as a 'safeguard' to handle rounding-errors in some maches, ie, instead of the 'actual' "=" operator.
      if(total >= previous) break; //! then there has not been an improvment, ie, we assuem that the algorithm has 'locally' coverged.

      //! If the result is identical as the previous 'run', then the algorithm has converged:
      uint count = 0;
      for(uint i = 0; i < nelements; i++) {
	assert(obj_rand->mapOf_columnToRandomScore[i] >= 0);
        if(arrOf_saved[i] != obj_rand->mapOf_columnToRandomScore[i]) break;
	count++;
      }
      if(count == nelements) break; /* Identical solution found; break out of this loop */
    } //! and at this exeuction-point one of the [ªbove] 'break-points' has been met.
    //! We expect the algorithm to ahve covnerged if the user has requested only one 'pass'/iteration:
    if(npass <= 1) { *error = total; break; }

    for(uint i = 0; i < nclusters; i++) mapping[i] = UINT_MAX;
    uint count = 0;
    //! Iterate through each of the elements/vertices: test/investigate if the clusterr-vertex-centroid-id has changed
    for(uint i = 0; i < nelements; i++) {
      count++;
      const uint j = obj_rand->mapOf_columnToRandomScore[i]; 
      const uint k = obj_rand->mapOf_columnToRandomScore[i]; //! the 'non-reset' cluster-id.
      assert(k != UINT_MAX);
      if( //(k != UINT_MAX) && 
	  (mapping[k] == UINT_MAX) ) {
	mapping[k] = j; //! ie, as the mapping has then not been set/allocated for this cluster.
      } else if((mapping[k] != j) ) { //! then the cluster-id has changed (since last iteration).
	if(total < *error) {
	  ifound = 1;
          *error = total;
          for(uint j = 0; j < nelements; j++) {clusterid[j] = obj_rand->mapOf_columnToRandomScore[j];} //! ie, update the container 'which is not reset in each while-loop'.
        }
        break;
      }
    }
    //! Update for the next iteration:
    if(count == nelements) ifound++; /* break statement not encountered */
  } while (++ipass < npass);
  
  //! ------------------------------------------------------------------------------------------------
  { //! De-allcoate locally reserved memory:
    free_1d_list_uint(&arrOf_saved);
    if(cmask_tmp) {
      free_2d_list_float(&cmask_tmp, nclusters);
    }
  }
  maskAllocate__freedatamask(nclusters, cdata, cmask, isTo_use_continousSTripsOf_memory);
  //! -------------------------------------------------------------------------------------------
  end_time_measurement_clusterC(logFor_kMeans, e_log_clusterC_kMeans_all); //! ie, intiate the object.
  //! @return the status.
  //return ifound;  
}

//#endif


/**
   @remarks 
   @remarks ... to verifyu predoducialibyt of the resutls, mulipel runs are appleid for each paramter combiatnios. We observe less than 3\% variation, hence the results are reproducable. To ivnestigate importance of different data-topolgoeis, we explore both randomzied data, and linear combiatniosn, observing how the disticntve difference amogn paramter configruations are reflected in the resutls. 
   @remarks ... the results demonstraes the importance of fast simliarty metrics, a case which is of speific importnace for big-data. Table \ref{} demonstrates how ... 
   @remarks ... the eueciotn time of clsuter algorithms depends on their data input ... for big-data approaches opne exepcs that the number of catoreiges (to deivide data in) increases with increasing data sets. A use-case (to illustrate the latter assumption) is seen for .... 
   @remarks motviaiton is to idneitfy the paramters delaying eueion of k-means, hence to describe the optimizaiton space.    
   \begin{enumerate}
   \item nrows: 
   \item ncols: ... when (the number of) columns equals (the number of) rows, the exection time ... 
   \item nclusters: ... an increase in cluster count increases the importance of the distance matrix ... does not influence the exeuion-time of fiding the 'mean', while influcnes the time to compute distance matrix (between the clusters and all the rows) 
   \item npass:
   \item mean-implementation-type: 
   \item random-iter-type: 
   \item data-input-type: 
   \end{enumerate}
 **/
int main() {

  uint nclusters = 3;
  const uint nrows = 1500; //! ie, the rows in the IRIS data-set.
  //const uint nrows = 150; //! ie, the rows in the IRIS data-set.
  //  const uint nrows = 1000;
  //const uint ncols = nrows; //! ie, the numbere of ..
  //const uint ncols = ; //! ie, the numbere of ..
  const uint ncols = 4; //! ie, the number of value-columns in the IRIS data-set.
  s_kt_matrix_base_t obj_mat = initSampleOfValues__rand__s_kt_matrix_base_t(nrows, ncols);
  //  t_float ** matrix = ,
    const uint npass = 1000;
  //const uint npass = 100;  
  // FIXME: explore different combiantions of [below]:
  /**
     -- for [1500,1500,iter=1000,clusters=3] the choise of random-fucntion is not of importance... 
     -- for [1500,1500,iter=1000,clusters=30] the choise of ...
     -----a) rand=mod, data=linear:  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out 
     #! Result(k-Means): summary: [all]='301.100006's, [init]='0.000000's, [k-Means-innerIteration-all]='0.000000's, [k-Means-innerIteration-randomAssign]='0.000000's, [k-Means-innerIteration-findCentroids]='22.860397's, [k-Means-innerIteration-applyMetric-findClosestToCenter]='278.221924's, at log_clusterC.c:214 
     -----b) rand=mod, data=rand:  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  ... same as above.
     -----c) rand=mod, data='duplicates: half of the rows equal':  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  .. same as above. 
     -----d) rand=uniform, data='duplicates: half of the rows equal':  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  ... goes approx. 30\% faster (than a "rand=mod" case).
     -------------------------------------------------------
     -- for [1500,4,iter=1000,clusters=30] the choise of ... approx. 100x faster than [above].
     -----) rand=uniform, data='duplicates: half of the rows equal':  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  
     -----) rand=mod, data='duplicates: half of the rows equal':  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  
     -- for [150,4,iter=1000,clusters=30] the choise of ... approx. 10x faster than above.
     -----) rand=uniform, data='duplicates: half of the rows equal':  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  
     -----) rand=mod, data='duplicates: half of the rows equal':  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  
     -- for [150,150,iter=1000,clusters=30] the choise of ... approx. 10x longer time than [150, 4].
     -----) rand=uniform, data='duplicates: half of the rows equal':  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  
     -----) rand=mod, data='duplicates: half of the rows equal':  gcc -O2 ops_matrix_kMeans.c -lm; time ./a.out  
     -- for [150,150,iter=1000,clusters=3] the choise of ... time changes from 30:3.17 to 3:0.52 ... ie, a 4x-difference
     -- for [150,4,iter=1000,clusters=3] the choise of ...  ... time changs from 30:0.23 to 3:0.04 ... ie, a 4x-difference ... ie, linear with [above]
     -- for [150,4,iter=1000,clusters=30] the choise of ... approx. 
     -----)  
     -----)  
     -- for [1500,1500,iter=100,clusters=30]: 
     -----)  fast: 
     -----)  
   **/
  e_kt_randomGenerator_type_t typeOf_randomNess  =
      e_kt_randomGenerator_type_posixRan__srand__time__modulo; //! data=rand, where [1500,1500,iter=1000,clusters=3] taks 0m52.112s to complete, and where cost(random)=0.08s.
    //    e_kt_randomGenerator_type_posixRan__srand__time__uniform; //! data=rand, where [1500,1500,iter=1000,clusters=3] taks 0m39.914s--0m43.405s to complete, and where cost(random)=0.03s--0.07s.
  // e_kt_randomGenerator_type_binominal__pst__07,
  s_kt_randomGenerator_vector_t obj_rand = initAndReturn__s_kt_randomGenerator_vector_t(nclusters, nrows, typeOf_randomNess, /*isTo_initIntoIncrementalBlocks*/false, &obj_mat);
  if(false) { //! then we use a linear-incrase in the number of columsn to apply:
    for(uint i = 0; i < obj_mat.nrows; i++) {
      for(uint k = 0; k < obj_mat.ncols; k++) {
	obj_mat.matrix[i][k] = (t_float)k;
      }
    }
  } else if(true) { //! then we mark half of the rows as identical
    const uint rows_half = (uint)((t_float)obj_mat.nrows * 0.5);
    assert(rows_half > 0);
    for(uint i = 0; i < rows_half; i++) {
      for(uint k = 0; k < obj_mat.ncols; k++) {
	obj_mat.matrix[i+rows_half][k] = obj_mat.matrix[i][k]; //! ie, cop
      }
    }
  }


  //! Initate:
  uint def_value = 0;
  uint *clusterid = allocate_1d_list_uint(nrows, def_value);
  uint *mapping = allocate_1d_list_uint(nclusters, def_value);
  uint *counts = allocate_1d_list_uint(nclusters, def_value);  
  for(uint i = 0; i < nclusters; i++) {  mapping[i] = UINT_MAX;}
  s_log_clusterC_t logFor_kMeans; s_log_clusterC_init(&logFor_kMeans);
  //!
  //! Apply logics:
  t_float error = 0;
  __ops_matrix_kMeans(nclusters,  obj_mat.nrows,  obj_mat.ncols, obj_mat.matrix, npass, &obj_rand, mapping, clusterid, counts, &logFor_kMeans, &error);
  //!
  if(true) {
    writeOut_object_clusterC(&logFor_kMeans, stdout, e_logCluster_typeOf_writeOut_kMeans, /*stringOf_description=*/NULL); //! ie, intiate the object.
    writeOut_object_clusterC(&logFor_kMeans, stderr, e_logCluster_typeOf_writeOut_kMeans, /*stringOf_description=*/NULL); //! ie, intiate the object.
  }
  { //! write otu results to avodi the compielr from asusmign that 'above call' is a dummy operaiton safe to ingore:
    t_float sum = 0;
    for(uint i = 0; i < nrows; i++) { sum += (t_float)clusterid[i];}
    fprintf(stderr, "sum=%f, at %s:%d\n", sum, __FILE__, __LINE__);
  }
  //!
  //! De-allocate:
  free__s_kt_randomGenerator_vector_t(&obj_rand);
  free__s_kt_matrix_base_t(&obj_mat);
  assert(clusterid); free_1d_list_uint(&(clusterid));
  assert(mapping); free_1d_list_uint(&(mapping));
  assert(counts); free_1d_list_uint(&(counts));
  //!
  //! 
  return 0;
}
