print(__doc__)

import numpy as np

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
import time

nrows = 1000*10
#nrows = 1000*20
#nrows = 750*10
#nrows = 750*40
ncols = 10
#ncols = 512
#ncols = 30000
#100 000 000 = 10^8 = (10^4)^2
centers = [[1, 1, 1, 10], [-1, -1, -1, 20], [1, -1, -1, 30], [1, -1, 1, 40], [-1, -1, 1, -90]]
#centers = [[1, 1, 1], [-1, -1, -1], [1, -1, 1]]
#centers = [[1, 1], [-1, -1], [1, -1]]
X, labels_true = make_blobs(n_samples=nrows, centers=centers, cluster_std=0.4,
                            random_state=0)
#print("len(X)=%d, len(X[0])=%d" % len(X), len(X[0]))
#X = StandardScaler().fit_transform(X)
X = np.random.rand(nrows, ncols)

#! ---------------------------------------------------------------
#!
#!
print("len(X)=%d" % len(X))
print("len(X[0])=%d" % len(X[0]))
t0 = time.time()
#db = DBSCAN(eps=0.4, min_samples=10).fit(X)
db = DBSCAN(eps=0.4, min_samples=10, algorithm='ball_tree').fit(X)
#db = DBSCAN(eps=0.4, min_samples=10, algorithm='brute').fit(X)
#db = DBSCAN(eps=0.3, min_samples=10).fit(X)
#db = DBSCAN(eps=.2, metric='euclidean', algorithm='brute').fit(X)
#db = DBSCAN(eps=.4, metric='euclidean', algorithm='brute').fit(X)
#db = DBSCAN(eps=.2, metric='canberra', algorithm='auto').fit(X)
#db = DBSCAN(eps=.2, metric='canberra', algorithm='brute').fit(X)
#db = DBSCAN(eps=.2, metric='canberra').fit(X)
#db = DBSCAN(eps=.4, metric='canberra', algorithm='brute').fit(X)
#db = DBSCAN(eps=.2, metric='canberra', algorithm='brute').fit(X)
#!
#! Complete:
t1 = time.time()
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('Time: %.2fs' % (t1 - t0)).lstrip('0')
print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
if(False):
    print("Adjusted Mutual Information: %0.3f"
          % metrics.adjusted_mutual_info_score(labels_true, labels))
    print("Silhouette Coefficient: %0.3f"
          % metrics.silhouette_score(X, labels))

if(False): #! then we plot teh result:
    import matplotlib.pyplot as plt
# Black removed and is used for noise instead.
    unique_labels = set(labels)
    colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = 'k'

        class_member_mask = (labels == k)

        xy = X[class_member_mask & core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=14)

        xy = X[class_member_mask & ~core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=6)

    plt.title('Estimated number of clusters: %d' % n_clusters_)
    #! Plot the exec-time in the lower right corder:
    plt.text(.99, .01, ('%.2fs' % (t1 - t0)).lstrip('0'),
          transform=plt.gca().transAxes, size=15,
              horizontalalignment='right')
    plt.show()
