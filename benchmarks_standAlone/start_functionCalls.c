#include "measure_codeStyle_functionRef_api.h"
#include "kt_matrix.c"
/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "measure.h"


// static const uint kilo = 1000;
//static clock_t clock_time_start; static clock_t clock_time_end;
// static struct tms tms_start;
// static  struct tms tms_end;
static uint matrix_block_id = 0;
static uint matrix_currentMeasurement_id = 0;

static const uint arrOf_cnt_buckets_size = 10; static const uint arrOf_cnt_buckets[arrOf_cnt_buckets_size] = {1, 5, 10, 20, 40,
													      80, 160, 320, 740, 1000};
static s_kt_matrix_t mat_result_each;
const uint kilo_local = 1000*10;

//! Intailise the meausrements: 
static void  __local__init_measurementResults(const uint cnt_measurements) {
  matrix_block_id = 0;
  matrix_currentMeasurement_id = 0;
  mat_result_each = initAndReturn__s_kt_matrix(cnt_measurements, arrOf_cnt_buckets_size);
}
//! 
static void  __local__add_measurementResults(const char *str_measurement, const t_float time_s) {
  assert(matrix_currentMeasurement_id < mat_result_each.nrows);
  assert(matrix_block_id < mat_result_each.ncols);
  if(
     (matrix_currentMeasurement_id < mat_result_each.nrows)
     && 
     (matrix_block_id < mat_result_each.ncols)
     ) {
    


     if( (time_s != T_FLOAT_MAX) && (time_s != FLT_MAX) ) {
      assert(matrix_block_id < mat_result_each.ncols);
      assert(matrix_currentMeasurement_id < mat_result_each.nrows);
      //!
      //! Set the string-headers:
      { //! Column: 
    	const char *str = getAndReturn_string__s_kt_matrix(&mat_result_each, /*index=*/matrix_block_id, /*for-column=*/true);
    	if(!str || !strlen(str)) { //! then we set the string:
	  assert(matrix_block_id < arrOf_cnt_buckets_size);
	  if(matrix_block_id >= arrOf_cnt_buckets_size) {
	    fprintf(stderr, "index-error, at %s:%d\n", __FILE__, __LINE__);
	  }
    	  const uint size_of_array = arrOf_cnt_buckets[matrix_block_id] * kilo_local*1000;
    	  char str_local[1000]; memset(str_local, '\0', 1000);
	  sprintf(str_local, "size=%u", size_of_array);
	  	    set_stringConst__s_kt_matrix(&mat_result_each, /*index=*/matrix_block_id, str_local, /*for-column=*/true);
    	}
      }
      { //! Row: 
	const char *str = getAndReturn_string__s_kt_matrix(&mat_result_each, /*index=*/matrix_currentMeasurement_id, /*for-column=*/false);
	if(!str || !strlen(str)) { //! then we set the string:
    	  set_stringConst__s_kt_matrix(&mat_result_each, /*index=*/matrix_currentMeasurement_id, str_measurement, /*for-column=*/false);
    	}
      }
      //!
      //! Set the value:    
      fprintf(stderr, "#\t set[%u][%u]=%f, at %s:%d\n", matrix_currentMeasurement_id, matrix_block_id, time_s, __FILE__, __LINE__);
      fprintf(stdout, "#\t set[%u][%u]=%f, at %s:%d\n", matrix_currentMeasurement_id, matrix_block_id, time_s, __FILE__, __LINE__);
      mat_result_each.matrix[matrix_currentMeasurement_id][matrix_block_id] = time_s;
     }
  }
  matrix_currentMeasurement_id++;
}
//! 
static void  __local__writeOut_measurementResults() {
  { //! Write result into a latex-comparible data-format (oekseth, 06. jun. 2017): 
    /* s_kt_matrix_t mat_transp = initAndReturn_transpos__s_kt_matrix(&mat_result); */
    // const uint cnt_sizeComb = listOf_rows_size * listOf_cols_size;
    // assert(mat_result.nrows == cnt_sizeComb);
    //!
    //! Iterate through the result-cases: 
    fprintf(stderr, "#! ----------- \n\n#!  Export the reuslts, at %s:%d\n\n", __FILE__, __LINE__);
    fprintf(stderr, "#! Columns=[");
    for(uint cnt_buckets_index = 0; cnt_buckets_index < mat_result_each.ncols; cnt_buckets_index++) {        
      fprintf(stderr, "%u:%s, ", cnt_buckets_index, getAndReturn_string__s_kt_matrix(&mat_result_each, /*index=*/cnt_buckets_index, /*for-column=*/true));
    }
    fprintf(stderr, "], and where the measurements are:\n");
    for(uint case_id = 0; case_id < mat_result_each.nrows; case_id++) {
      fprintf(stderr, "\n\n#!  %s\n", getAndReturn_string__s_kt_matrix(&mat_result_each, /*index=*/case_id, /*for-column=*/false));
      assert(mat_result_each.ncols == arrOf_cnt_buckets_size);
      for(uint cnt_buckets_index = 0; cnt_buckets_index < mat_result_each.ncols; cnt_buckets_index++) {        
	const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * kilo_local*1000;
	const loint cnt_comparisons = size_of_array;
	const t_float time_s = mat_result_each.matrix[case_id][cnt_buckets_index];
	if(
	   (time_s != T_FLOAT_MAX) 
	   && 
	   (time_s != 0) ) {
	  fprintf(stderr, "(%lld, %f) ", cnt_comparisons, time_s);
	}
      }
      fprintf(stderr, "\n");
    }
  }
  free__s_kt_matrix(&mat_result_each);
}
/**
   @brief Start the measurement with regard to time.
   @remarks 
   - Uses the global vairalbe in this file for this prupose.
   - Clears the cache before starting this measurement, causing a small slowdown in the outside performance, though not included in the estimates returned.
**/
// static  void start_time_measurement() {
// #ifndef NDEBUG
//   //! Clears the cache by allocating 100MB of memory- and the de-allocating it:
//   // TODO: Consier adding random access to it for further obfuscating of earlier memory accesses.
//   uint size = 1024*1024*100;
//   char *tmp = new char[size];
//   assert(tmp);
//   memset(tmp, 7, size);
//   delete [] tmp; tmp = NULL; size = 0;
// #endif
//   tms_start = tms();
//   clock_time_start = times(&tms_start);
//   // if(() == -1) // starting values
//   //   err_sys("times error");
// }
/**
   @brief Ends the measurement with regard to time.
   @param <msg> If string is given, print the status information to stdout.
   @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called.
   @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called.
   @return the clock tics on the system since the 'start_time_measurement()' method was called.
**/
static float end_time_measurement(const char *msg, float &user_time, float &system_time, const float prev_time_inSeconds) {
  clock_time_end = times(&tms_end); 
  long clktck = 0; clktck =  sysconf(_SC_CLK_TCK);
  const clock_t t_real = clock_time_end - clock_time_start;
  const float time_in_seconds = t_real/(float)clktck;
  const float diff_user = tms_end.tms_utime - tms_start.tms_utime;
  if(diff_user) user_time   = (diff_user)/((float)clktck);
  const float diff_system = tms_end.tms_stime - tms_start.tms_stime;
  system_time = diff_system/((float)clktck);
  if(msg) {
    // printf("time_in_seconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
    if( (prev_time_inSeconds == FLT_MAX) || (time_in_seconds == 0) ) {
      printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg);
    } else {
      //float diff = prev_time_inSeconds / time_in_seconds;
      float diff = time_in_seconds / prev_time_inSeconds;
      // printf("\tprev_time_inSeconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
      printf("%f x \ttick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg);
    }
    __local__add_measurementResults(msg, time_in_seconds);
  }
  //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg);
  return (float)time_in_seconds;
}


/**
   @brief Ends the measurement with regard to time.
   @return the clock tics on the system since the 'start_time_measurement()' method was called.
**/
static float __end_time_measurement(const char *msg, const float prev_time_inSeconds) {
  float user_time = 0, system_time=0;
  return end_time_measurement(msg, user_time, system_time, prev_time_inSeconds);
}



//! Ends the time measurement for the array:
static float __i__assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    char *string = new char[1000]; assert(string); memset(string, '\0', 1000);
    sprintf(string, "(finding %u relations with %s)", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = __end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}

static t_float __get_artimetnic_results(const t_float val1, const t_float val2) {
  return val1 * val2;
}

inline __attribute__((always_inline)) static t_float __get_artimetnic_results_inline(const t_float val1, const t_float val2) {
  return val1 * val2;
}

inline static t_float __get_artimetnic_results_inline_alt2(const t_float val1, const t_float val2)  __attribute__((always_inline));

inline static t_float __get_artimetnic_results_inline_alt2(const t_float val1, const t_float val2) {
  return val1 * val2;
}

static void apply_tests() {
  __local__init_measurementResults(/*cnt-max-measurements=*/30);
  //! The call: 
  matrix_block_id = 0;
  for(uint cnt_buckets_index = 0; cnt_buckets_index < arrOf_cnt_buckets_size; cnt_buckets_index++) {        
    matrix_currentMeasurement_id = 0; //! ie, reset.
    //! ---------------- 
    const uint size_of_array = arrOf_cnt_buckets[cnt_buckets_index] * kilo_local*1000;
    printf("----------\n\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    
    //! Tes tthe 'ideal' case:
    const char *stringOf_measureText = "Multiplication: ideal case";
    start_time_measurement();  //! ie, start measurement
    //! -------------
    t_float result = 0;
    for(uint i = 0; i < size_of_array; i++) {
      result = (i * i);
    }
    //! -------------        
    const float prev_time_inSeconds = __i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, FLT_MAX);
    printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
    
    { //! Test wrt. the voaltiel key-word using a boolean operand-comparison:
    //! --------------------------------
      const double second_var = size_of_array*0.3;
      { const char *stringOf_measureText = "Boolean: ideal case";
	start_time_measurement();  //! ie, start measurement
	t_float result = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  const double x = (double)i; 	  const double y = second_var;
	  result += (x >= y);
	}
	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      { const char *stringOf_measureText = "Boolean: function from external header-file: from-external-header";
	start_time_measurement();  //! ie, start measurement
	t_float result = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  const double x = (double)i; 	  const double y = second_var;
	  result += ae_fp_greater_eq_nonVolatile(x, y);
	}
	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      { const char *stringOf_measureText = "Boolean: function from external header-file: from-external-header: volatile";
	start_time_measurement();  //! ie, start measurement
	t_float result = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  const double x = (double)i; 	  const double y = second_var;
	  result += ae_fp_greater_eq(x, y);
	}
	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }

    } 
    //! --------------------------------
    { const char *stringOf_measureText = "Multiplication: function in 'this' code-file: default";
      start_time_measurement();  //! ie, start measurement
      t_float result = 0;
      for(uint i = 0; i < size_of_array; i++) {
	result += __get_artimetnic_results(i, i);
      }
      __i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
      printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
    }
    //! --------------------------------
    { const char *stringOf_measureText = "Multiplication: function in 'this' code-file: inline";
      start_time_measurement();  //! ie, start measurement
      t_float result = 0;
      for(uint i = 0; i < size_of_array; i++) {
	result += __get_artimetnic_results_inline(i, i);
      }
      __i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
    }
    //! --------------------------------
    { const char *stringOf_measureText = "Multiplication: function in 'this' code-file: inline-alt2";
      start_time_measurement();  //! ie, start measurement
      t_float result = 0;
      for(uint i = 0; i < size_of_array; i++) {
	result += __get_artimetnic_results_inline_alt2(i, i);
      }
      __i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
    }

    { //! Case(a): no overhead and speciifed in header-file:
      //! --------------------------------
      { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-header: static";
	start_time_measurement();  //! ie, start measurement
	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_t*) = getFunction__inHeader__measure_codeStyle_functionRef(/*type=*/'a');
	t_float result = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  s_measure_codeStyle_functionRef_t obj;
	  func_ref(i, i, &obj); 	result += obj.sumOf;
	}
	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-header: inline";
	start_time_measurement();  //! ie, start measurement
	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_t*) = getFunction__inHeader__measure_codeStyle_functionRef(/*type=*/'b');
	t_float result = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  s_measure_codeStyle_functionRef_t obj;
	  func_ref(i, i, &obj); 	result += obj.sumOf;
	}
	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      //! --------------------------------
      { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-header: static inline";
	start_time_measurement();  //! ie, start measurement
	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_t*) = getFunction__inHeader__measure_codeStyle_functionRef(/*type=*/'c');
	t_float result = 0;
	for(uint i = 0; i < size_of_array; i++) {
	  s_measure_codeStyle_functionRef_t obj;
	  func_ref(i, i, &obj); 	result += obj.sumOf;
	}
	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
    }
    { //! Case(b): overhead and speciifed in header-file:
      //! --------------------------------
      { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-header: static";
    	start_time_measurement();  //! ie, start measurement
    	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_largeOverhead_t*) = getFunction__inHeader_largeOverhead__measure_codeStyle_functionRef(/*type=*/'a');
    	t_float result = 0;
    	for(uint i = 0; i < size_of_array; i++) {
    	  s_measure_codeStyle_functionRef_largeOverhead_t obj;
    	  func_ref(i, i, &obj); 	result += obj.sumOf;
    	}
    	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
    	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-header: inline";
    	start_time_measurement();  //! ie, start measurement
    	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_largeOverhead_t*) = getFunction__inHeader_largeOverhead__measure_codeStyle_functionRef(/*type=*/'b');
    	t_float result = 0;
    	for(uint i = 0; i < size_of_array; i++) {
    	  s_measure_codeStyle_functionRef_largeOverhead_t obj;
    	  func_ref(i, i, &obj); 	result += obj.sumOf;
    	}
    	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
    	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      //! --------------------------------
      { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-header: static inline";
    	start_time_measurement();  //! ie, start measurement
    	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_largeOverhead_t*) = getFunction__inHeader_largeOverhead__measure_codeStyle_functionRef(/*type=*/'c');
    	t_float result = 0;
    	for(uint i = 0; i < size_of_array; i++) {
    	  s_measure_codeStyle_functionRef_largeOverhead_t obj;
    	  func_ref(i, i, &obj); 	result += obj.sumOf;
    	}
    	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
    	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
    }
    //! ------------------------------------------
    { //! Case(a): no overhead and speciifed in header-file:
      //! --------------------------------
      { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-code-file: static";
    	start_time_measurement();  //! ie, start measurement
    	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_t*) = getFunction__measure_codeStyle_functionRef(/*type=*/'a');
    	t_float result = 0;
    	for(uint i = 0; i < size_of_array; i++) {
    	  s_measure_codeStyle_functionRef_t obj;
    	  func_ref(i, i, &obj); 	result += obj.sumOf;
    	}
    	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
    	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
      // { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-code-file: inline";
      // 	start_time_measurement();  //! ie, start measurement
      // 	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_t*) = getFunction__measure_codeStyle_functionRef(/*type=*/'b');
      // 	t_float result = 0;
      // 	for(uint i = 0; i < size_of_array; i++) {
      // 	  s_measure_codeStyle_functionRef_t obj;
      // 	  func_ref(i, i, &obj); 	result += obj.sumOf;
      // 	}
      // 	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
      // 	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      // }
      //! --------------------------------
    }


    // { //! Case(b): overhead and speciifed in header-file:
    //   //! --------------------------------
      { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-code-file: static";
    	start_time_measurement();  //! ie, start measurement
    	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_largeOverhead_t*) = getFunction__largeOverhead__measure_codeStyle_functionRef(/*type=*/'a');
    	t_float result = 0;
    	for(uint i = 0; i < size_of_array; i++) {
    	  s_measure_codeStyle_functionRef_largeOverhead_t obj;
    	  func_ref(i, i, &obj); 	result += obj.sumOf;
    	}
    	__i__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
    	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
      }
    //   // { const char *stringOf_measureText = "Multiplication: function from external header-file: from-external-code-file: inline";
    //   // 	start_time_measurement();  //! ie, start measurement
    //   // 	void (*func_ref) (const t_float, const t_float, s_measure_codeStyle_functionRef_largeOverhead_t*) = getFunction__largeOverhead__measure_codeStyle_functionRef(/*type=*/'b');
    //   // 	t_float result = 0;
    //   // 	for(uint i = 0; i < size_of_array; i++) {
    //   // 	  s_measure_codeStyle_functionRef_largeOverhead_t obj;
    //   // 	  func_ref(i, i, &obj); 	result += obj.sumOf;
    //   // 	}
    //   // 	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_time_inSeconds);      
    //   // 	printf("(ignore)\t result=%f, at %s:%d\n", result, __FILE__, __LINE__);
    //   // }
    //   //! --------------------------------
    // }
      //blockSize=
      matrix_block_id++;
  }
  fprintf(stderr, "# Write out meausrements, at %s:%d\n", __FILE__, __LINE__);
  __local__writeOut_measurementResults();

  assert(false); // FIXME: test the 'called' functions.
}


 //! The main-function to test our "measure_codeStyle_functionRef_api"
void main__measure_codeStyle_functionRef(int array_cnt, char **array) {


  // printf("-----, at %s:%d\n", __FILE__, __LINE__);
  //! Appply the performance-tests:
  apply_tests();

  assert(false); // FIXME: add soemthing ... use the 'sytntiec' asa tmplate.
  
}
