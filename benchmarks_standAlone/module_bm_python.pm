#!/usr/bin/perl
package module_bm_python;
use Carp;
use strict;
use Time::HiRes qw(usleep ualarm gettimeofday tv_interval);

sub apply {
    my ($dim_base_size, $dim_iter, $cnt_call_each) = @_;
    #!
    #!

    #!
    #! Note: belwo cofnigurations are fectehd from "x_measure_scriptLanguages.pl".
    #!
    #my $file_name =  "tut_main_matrixBM.py";
    my $file_name =  "tut_main_matrixBM_xmtNumPy.py";
    #!
    my @arr_compiler = (
	# FIXME: seems like there is no difference in exec-time between different nrows ... for "python3.7", "python3.8" ... due to ...??...
"python2.7", "python3.6", "python3.7", "python3.8");
    my @arr_func = (
    #"_3d_plus_dsList", #! ie, three for-loops: in each of the 3d-ops, two '+' operaitons are performed, where a dataStructure=list is accessed
    "_3d_mult", #! data is not accssed; two '+' operaitons are performed
	"_3d_multAndPlus_dsList", #! in each of the 3d-ops, one '+' and one '*' is performed; calculations reflects the ones performed in the cityblock-metric.
    #"_3d_plus_dsList_2ifBranchInner", #! ie, three for-loops: in each of the 3d-ops, two '+' operaitons are performed, where a dataStructure=list is accessed; for every artimetic operaiton an if-branch-test is performed.
	#"_3d_multAndPlus_dsList_2ifBranchInner", #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; for every artimetic operaiton an if-branch-test is performed.
	#"_3d_multAndPlus_dsList_callFunc_onVector_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; the funciton is called for each of the vectors, ie, instead of trhee for-loops, only two for-loops are performed inside teh main-body; for every artimetic operaiton an if-branch-test is performed.
	#"_3d_multAndPlus_dsList_callFunc_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; each of the artimetic operaitosn are eprformed in a single funciotn; this reflects the strategy of using external libriares for computing similiarites (ie, a joiint-full strategy); for every artimetic operaiton an if-branch-test is performed.
	#"_skLearn_euclidean_distances",  #! call sci-kit Learn's euclidean-funciton
	#"_skLearn_manhattan_distances", #! call sci-kit Learn's manhatten-funciton
	#"_skLearn_paired_cityblock", #! uses the 'pairwise' sci-kit learn module to copmute cityblock (or: mahatten) dsitance
	#"_skLearn_paired_cosine",  #! uses the 'pairwise' sci-kit learn module to copmute distance
	#"_skLearn_metric_minkowski",  #! uses the 'pairwise' sci-kit learn module to copmute Minkowski-distance
	#"",  #! uses the 'pairwise' sci-kit learn module to copmute distance    
	#"",  
	#"",    
	);
    if(1 == 2) {
	    #my @arr_compiler = ("python2.7", "python3.6", "python3.7", "python3.8");
	    #my $file_name =  "tut_main_matrixBM.py";
    #my $file_name =  "tut__altLAng_sciPy__listAccess.py";
	     @arr_func = (
    "_3d_plus_dsList", #! ie, three for-loops: in each of the 3d-ops, two '+' operaitons are performed, where a dataStructure=list is accessed
    "_3d_multAndPlus_dsList", #! in each of the 3d-ops, one '+' and one '*' is performed; calculations reflects the ones performed in the cityblock-metric.
    "_3d_mult", #! data is not accssed; two '+' operaitons are performed
    "_3d_plus_dsList_2ifBranchInner", #! ie, three for-loops: in each of the 3d-ops, two '+' operaitons are performed, where a dataStructure=list is accessed; for every artimetic operaiton an if-branch-test is performed.
    "_3d_multAndPlus_dsList_2ifBranchInner", #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; for every artimetic operaiton an if-branch-test is performed.
    "_3d_multAndPlus_dsList_callFunc_onVector_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; the funciton is called for each of the vectors, ie, instead of trhee for-loops, only two for-loops are performed inside teh main-body; for every artimetic operaiton an if-branch-test is performed.
    "_3d_multAndPlus_dsList_callFunc_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; each of the artimetic operaitosn are eprformed in a single funciotn; this reflects the strategy of using external libriares for computing similiarites (ie, a joiint-full strategy); for every artimetic operaiton an if-branch-test is performed.
    "_skLearn_euclidean_distances",  #! call sci-kit Learn's euclidean-funciton
    "_skLearn_manhattan_distances", #! call sci-kit Learn's manhatten-funciton
    "_skLearn_paired_cityblock", #! uses the 'pairwise' sci-kit learn module to copmute cityblock (or: mahatten) dsitance
    "_skLearn_paired_cosine",  #! uses the 'pairwise' sci-kit learn module to copmute distance
    #"_skLearn_metric_minkowski",  #! uses the 'pairwise' sci-kit learn module to copmute Minkowski-distance
    #"",  #! uses the 'pairwise' sci-kit learn module to copmute distance    
    #"",  
    #"",    
	);
    }
    #! -----------------------
    my $result_file_min = "res-x_tut_2_python_min.tsv";
    my $result_file_max = "res-x_tut_2_python_max.tsv";
    open(FILE_OUT_MIN, ">$result_file_min") or die("Unable to open the input-file $result_file_min\n");
    open(FILE_OUT_MAX, ">$result_file_max") or die("Unable to open the input-file $result_file_max\n");
    #!
    #! Add header:
    printf(FILE_OUT_MIN "#! Compiler \t Function \t Dimension\t Time\n");
    printf(FILE_OUT_MAX "#! Compiler \t Function \t Dimension\t Time\n");
    #--------------
    foreach my $comp (@arr_compiler) {
	foreach my $fun (@arr_func) {
	    for(my $i = 1; $i <= $dim_iter; $i++) {
		my $nrows = $dim_base_size * $i; #! eg, 64, 128, ...
		#! ---------
		my $t_min = undef;
		my $t_max = undef;
		for(my $iter = 0; $iter < $cnt_call_each; $iter++) {
		    my $t0 = [gettimeofday];
		    my $cmd = "time $comp $file_name $fun $nrows"; #! eg, python tut_main_matrixBM.py _3d_mult 1000
		    printf("CMD\t " . $cmd . "\n ");
		    system($cmd);
		    my $duration_curr = tv_interval ($t0, [gettimeofday]);
		    if(!defined($t_min) || ($t_min > $duration_curr) ) {
			$t_min = $duration_curr;
		    }
		    if(!defined($t_max) || ($t_max < $duration_curr) ) {
			$t_max = $duration_curr;
		    }
		    #croak("...");
		}
		#!
		#! Update result-files:
		printf(FILE_OUT_MIN "%s\t%s\t%u\t%.2f\n", $comp, $fun, $nrows, $t_min);
		printf(FILE_OUT_MAX "%s\t%s\t%u\t%.2f\n", $comp, $fun, $nrows, $t_max);
	    }
	}
    }
    #!
    #! CLOSE:
    close(FILE_OUT_MIN);
    close(FILE_OUT_MAX);
}


#! ============================= EOF:
1;
    
