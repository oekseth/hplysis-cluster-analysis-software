### How do I get set up? ###



```
#!bash

#!
#! Compute extreme prediction-differences; explore effects of different implementation-strategies.
#! Note: "x_measure_extreme.pl" calls "strassen_main.c" (which contains the c-doe to execute).
perl x_measure_extreme.pl

#! All: Explores (nearly) all the compilation-parameters for
#! Note: assumes that a number of gcc and g++ versions are already installed.
#! Note: "x_measure_all.pl" calls "strassen_main.c" (which contains the c-doe to execute).
perl x_measure_all.pl

#!
#! Python: Applies a large number of different Python-implementation patterns:
python tut__altLAng_sciPy__listAccess.py

#!
#! Python: Applies a given implementation pattern
#! Note: in below call the following pattern is tested: ...  
python tut_main_matrixBM.py _3d_mult 100
#!
#! See all functions which can be called:
python tut_main_matrixBM.py help 100
#!
#! Apply for a subset of the scripts (and compilers):
perl x_tut_2_python.pl;


#!
#!
#! Perl: See all functions which can be called:
perl tut_main_matrixBM.pl help 100;
#! Note: in below call the following pattern is tested: ...  
perl tut_main_matrixBM.pl _3d_multAndPlus_dsList 100
#!
#! Perform computation for *all* implementations, given matrixDim=100:
perl tut_main_matrixBM.pl all 100

#!
#! Java: see all arguments:
javac tut__altLAng_simMetric.java; java -classpath . altLAng_simMetric help
#!
#! Explore the time of Cityblock-computation, for case where we ignore the time-cost of memory-accesses, and where rows=columns=1000
javac tut__altLAng_simMetric.java; java -classpath . altLAng_simMetric _3d_mult 1000
#!
#! Apply for a subset of the scripts (and compilers):
perl x_tut_3_java.pl;


#!
#! R: see all arguments:
Rscript tut__altLAng_simMetric.R help
#!
#! Compute for *all* cases, given rows=columns=100:
Rscript tut__altLAng_simMetric.R all 100
#! Apply for a subset of the scripts (and compilers):
perl x_tut_4_r.pl;

#!
#! C/C++: perform evaluation:
#!
#! Apply a macro-based strategy for automated software-construction (to explore different combinations):
perl x_measure_all.pl;
#! Apply for a subset of the scripts (and compilers):
perl x_tut_4_C.pl;

#!
#! Measures execution-time for the non-C/C++ language-permutations:
perl x_measure_scriptLanguages.pl

```