assert(self);
assert(self->nrows == 0);     assert(self->ncols == 0);
assert(superset->nrows > 0);  assert(superset->ncols > 0);

//! Initaite:
init__s_kt_matrix(self, /*nrows=*/superset->ncols, /*ncols=*/superset->nrows, /*isTo_allocateWeightColumns=*/false);  //! where the 'weights' are Not copied as we expect to use the 'ranks' in our data-set-copy'.
assert(self->nrows == superset->ncols);     assert(self->ncols == superset->nrows); //! ei, as we epxec thte data-set-object to have been transposed.

/* #if(__localConfig__copyWeights == 1) */
/* if(superset->weight != NULL) { //! then copy the weights: */
/*   for(uint local_id = 0; local_id < superset->ncols; local_id++) {       */
/*     const uint global_vertex = local_id;   */
/*     t_float score = 0; */
/*     //! Udpate the 'new' object with the 'knowledge' from the superset-object. */
/*     get_weight__s_kt_matrix(superset, global_vertex, &score); */
/*     set_weight__s_kt_matrix(self, local_id, score); */
/*   }     */
/*  } */
/* #endif */

//!
//! Udpate the name-mappigns:
if(isTo_updateNames) {
  if(superset->nameOf_columns != NULL) {
    bool addFor_column = false;
    for(uint local_id = 0; local_id < self->nrows; local_id++) {      
      const uint global_vertex = local_id;
      char *stringTo_add = NULL;
      get_string__s_kt_matrix(superset, global_vertex, !addFor_column, &stringTo_add);
      if(stringTo_add) {
	set_string__s_kt_matrix(self, local_id, stringTo_add, addFor_column);
      }
    }
  }
  //! ---------
  if(superset->nameOf_rows != NULL) {
    bool addFor_column = true;
    for(uint local_id = 0; local_id < self->ncols; local_id++) {      
      const uint global_vertex = local_id;
      char *stringTo_add = NULL;
      get_string__s_kt_matrix(superset, global_vertex, !addFor_column, &stringTo_add); 
      if(stringTo_add) {
	set_string__s_kt_matrix(self, local_id, stringTo_add, addFor_column);
      }
    }
  }
 }

//! First copy the data-set:
for(uint row_id = 0; row_id < self->nrows; row_id++) {
  for(uint col_id = 0; col_id < self->ncols; col_id++) {
    self->matrix[row_id][col_id] = superset->matrix[col_id][row_id]; //! ie, a 'transpsoed' copy-operaiton.
  }
 }
//! -----------------------
//!
#if(__localConfig__useRanks == 1)
//! Comptue the rank and thereafter insert
for(uint row_id = 0; row_id < self->nrows; row_id++) {
  //! Get the ranks:
  t_float *list_rank = get_rank__correlation_rank(self->ncols, self->matrix[row_id]);  //! ie, used the 'already transpsoed matrix as input'.
  assert(list_rank);
  //! Insert the ranks:
  for(uint col_id = 0; col_id < self->ncols; col_id++) {
    self->matrix[row_id][col_id] = list_rank[col_id];
  }
  //! De-allcoat ethe locally reserved memory:
  free_1d_list_float(&list_rank); list_rank = NULL; //! ie, de-allcoate.
 }
#endif

//#undef __localConfig__copyWeights
#undef __localConfig__useRanks

