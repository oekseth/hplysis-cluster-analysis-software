
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! Comptue inner distance-metric for euclid
//! Note: [below] funciotn is also define in our "kt_math_matrix.c": we include 'this' directlyiot. evlauate the exueciotnt-iem-difference when using an optimized compilation-approach.
static void kt_func_metricInner_fast__fitsIntoSSE__euclid__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    //const t_float *__restrict__ row_2_base = matrix[col_id + int_col_id];
	    /* const t_float *__restrict__ row_1 = row_1_base + j; */
	    /* const t_float *__restrict__ row_2 = row_2_base + j; */

	    //const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOAD(..)" instead of the "VECTOR_FLOAT_LOAD(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
