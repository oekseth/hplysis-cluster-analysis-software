from __future__ import print_function
import time

currentPos = 0
def compute_matrix(nrows, ncols):    
    global currentPos
    numOfIterations = 1
#! -------------------------------------------------------
    #!
    #! Iterate:
    for i in xrange(numOfIterations):
            a = time.clock() 
            sum = 0;
            for k in xrange(nrows):
                for l in xrange(nrows):
                    for i in xrange(ncols):
                        sum += (k * l)
            b = time.clock()
            print("time[nrows=" , nrows , "]: " , ( b - a ))
            #exec_time_pyIter_sum_mul.append( b - a )
#!
#! Deimsnions:
#cnt_cases = 40
cnt_cases = 10
minSize = 128
multFact__nrows = 1
multFact__ncols = 1
for i in xrange(cnt_cases):
    case_id = i
    nrows = minSize + 128*case_id*multFact__nrows
    ncols = nrows #minSize + 128*case_id*multFact__ncols
    # print("case_id=", case_id, "nrows=", nrows)
    compute_matrix(nrows, ncols)
