#include "kt_math_matrix.h"
#include "kt_mathMacros.h"
#include "mask_api.h"
#include "types_base.h"
#include "types.h"

//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate" where we comptue the 'min' valeu for each 'run', ie, an overehad wrt. summations.
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_adjusted; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_transposed[col_id];
      for(uint j = 0; j < size_adjusted; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    uint int_j = 0; t_float scalar_result = VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))); 
	    //	    uint int_j = 0; t_float scalar_result = VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1[int_j]), VECTOR_FLOAT_LOAD(&row_2[int_j]))); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {	
	      // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOAD(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	      scalar_result += VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	      //	      scalar_result += VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1[int_j]), VECTOR_FLOAT_LOAD(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += scalar_result;
	  }
	}
      }
    }
  }
}


//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE" with tiling
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  //const bool isSymmetric = matrix_transposed
  for(uint row_id = 0; row_id < size_adjusted; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
    
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = size_adjusted;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_transposed[col_id];
      for(uint j = 0; j < size_adjusted; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOAD(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1[int_j]), VECTOR_FLOAT_LOAD(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}


//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow" with tiling
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__tiling(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_adjusted; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_transposed[col_id];
      for(uint j = 0; j < size_adjusted; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    uint int_j = 0; t_float scalar_result = row_1[int_j] * row_2[int_j];
	    int_j++;
	    for(; int_j < SM; int_j++) {	
	      scalar_result += row_1[int_j] * row_2[int_j];
	    }
	    //! Update the partial result:
	    const uint global_row_id = row_id+int_row_id;
	    const uint global_col_id = col_id+int_col_id;
#if(0) // then we inlcude a set of itnernal-tests, internal tests whcih are normally not include,d ie, to avoid cløutteirng our perofmrance-comparsion-tests.
	    assert( (col_id + SM) <= size_adjusted);
	    assert(int_col_id < SM);
	    assert(global_row_id < size_adjusted); assert(global_col_id < size_adjusted);
#endif
	    //! Update:
	    result[global_row_id][global_col_id] += scalar_result;
	  }
	}
      }
    }
  }
}


//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow" with SSE
 void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      const t_float *__restrict__ row_1 = matrix[row_id];
      const t_float *__restrict__ row_2 = matrix_transposed[row_id];
      uint j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[j]), VECTOR_FLOAT_LOADU(&row_2[j])); j += VECTOR_FLOAT_ITER_SIZE;
      for(; j < size_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[j]), VECTOR_FLOAT_LOADU(&row_2[j]))); 
      }
      result[row_id][col_id] = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
    }
  }
}


//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput" with a "__restrict__" guineklien-keyword to the compiler
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      const t_float *__restrict__ row_1 = matrix[row_id];
      const t_float *__restrict__ row_2 = matrix_transposed[row_id];
      uint j = 0; t_float scalar_result = row_1[j] * row_2[j]; j++;     
      for(; j < size_adjusted; j++) {
	scalar_result += row_1[j] * row_2[j];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}


//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow_transposedAsInput(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix_transposed[col_id][j]; j++;
      for(; j < size_adjusted; j++) {
	scalar_result += matrix[row_id][j] * matrix_transposed[col_id][j];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}

//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow(t_float **matrix, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix[j][col_id]; j++;
      for(; j < size_adjusted; j++) {
	scalar_result += matrix[row_id][j] * matrix[j][col_id];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! Comptue inner distance-metric for euclid
void kt_func_metricInner_fast__fitsIntoSSE__euclid__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    //const t_float *__restrict__ row_2_base = matrix[col_id + int_col_id];
	    /* const t_float *__restrict__ row_1 = row_1_base + j; */
	    /* const t_float *__restrict__ row_2 = row_2_base + j; */

	    //const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOAD(..)" instead of the "VECTOR_FLOAT_LOAD(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}

//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //    for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOADU(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
//! ------------------------------------------------------------------------
void kt_func_metricInner_fast__fitsIntoSSE__euclid__int16(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT16_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT16_ITER_SIZE) {
	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT16_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__int16(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))); 
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT16_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT16_ITER_SIZE) {
	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT16_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
/* //! ------------------------------------------------------------------------ */
/* //! Comptue inner distance-metric for euclid */
/* void kt_func_metricInner_fast__fitsIntoSSE__euclid__char(char **matrix, const uint nrows, const uint ncols, char **result, const uint SM) { */

/* } */
//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__char(char **matrix, char **matrix_2, const uint nrows, const uint ncols, char **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const char *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const char *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const char *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const char *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_CHAR_LOAD(..)" instead of the "VECTOR_CHAR_LOAD(..)" call:

/* #ifndef NDEBUG */
/* #warning TODO[JC]: what is the differnece between "__m64" and "__m128i" ... ... why is there 'such a difnferec'e? .... and how may this difference be tuilized/ and voercomed? ... and how may the "_mm_max_epu8(..)" be combined with "_mm_sub_epi8(..)"? (oekseth, 06.. otk. 2016). */
/* #endif */
	    assert(false); // FXIEM: include [”elow] when we have larned how to combine "__m64" and "__m128i" wrt. the SSe-function-logics.
	    /* uint int_j = 0; VECTOR_CHAR_TYPE_alt vec_result =  */
	    /* 		      //VECTOR_CHAR_abs */
	    /* 		      _mm_abs_epi8 */
	    /* 		      (VECTOR_CHAR_SUB(VECTOR_CHAR_LOAD_alt(&row_1[int_j]), VECTOR_CHAR_LOAD_alt(&row_2[int_j]))); */
	    /* //uint int_j = 0; VECTOR_CHARYPE vec_result = VECTOR_CHAR_MUL(VECTOR_CHAR_LOAD(&row_1[int_j]), VECTOR_CHAR_LOAD(&row_2[int_j]));  */
	    /* int_j += VECTOR_CHAR_ITER_SIZE; */
	    /* for(; int_j < SM; int_j += VECTOR_CHAR_ITER_SIZE) { */
	    /*   vec_result = VECTOR_CHAR_ADD(vec_result, VECTOR_CHAR_abs(VECTOR_CHAR_SUB(VECTOR_CHAR_LOAD(&row_1[int_j]), VECTOR_CHAR_LOAD(&row_2[int_j])))); */
	    /* } */
	    /* //! Update the partial result: */
	    /* result[row_id+int_row_id][col_id+int_col_id] += VECTOR_CHAR_storeAnd_horizontalSum(vec_result); */
	  }
	}
      }
    }
  }
}


//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------




//! @return the determinant for a 2x2x matrix.
#define __macro__get_determinant_case__2x2x(matrix) ({matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];})

//! start:---------------------------------------------
//! Simplfy funciton-generation:
#define kt_math_matrixinternal_inner(fun, suffix) fun ## _ ## suffix
//#define kt_math_matrixinternal_extensive(fun, midfix, suffix) kt_math_matrixinternal_inner(fun, midfix, suffix) 
/* #define determinant 1 */
/* #define slow 0 */
//#define kt_math__derminant(suffix, type) kt_math_matrixinternal_determinant(suffix, determinant,type)
#define kt_math__derminant_slow(suffix) kt_math_matrixinternal_inner(determinant_slow, suffix)
#define kt_math__derminant_usePow(suffix) kt_math_matrixinternal_inner(determinant_usePow, suffix)
#define kt_math__derminant(suffix) kt_math_matrixinternal_inner(determinant, suffix)
//! --
#define kt_math__coFactor_slow(suffix) kt_math_matrixinternal_inner(__coFactor_slow, suffix)
#define kt_math__coFactor_usePowTable(suffix) kt_math_matrixinternal_inner(__coFactor_usePow, suffix)
#define kt_math__coFactor(suffix) kt_math_matrixinternal_inner(__coFactor, suffix)

//! :end---------------------------------------------


#define type_float 1



//! Copmtue the detminant usign recrusive calls
static t_float kt_math__derminant_slow(type_float)(t_float **a,const uint n) {
//static t_float __determinant_slow(t_float **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant_slow(type_float)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 0
#define __compute_inverse__fastImplementaiton_memAlloc 0
#define __compute_inverse__fastImplementaiton 0
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType t_float
#define allocate_1d_list_dType allocate_1d_list_float
#define allocate_2d_list_dType allocate_2d_list_float
#define free_1d_list_dType free_1d_list_float
#define free_2d_list_dType free_2d_list_float
#include "kt_math_matrix__func__determinant.c"
  return(det);
}


//! Copmtue the detminant usign recrusive calls
static char kt_math__derminant_slow(type_char)(char **a,const uint n) {
//static t_float __determinant_slow(t_float **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant_slow(type_char)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 0
#define __compute_inverse__fastImplementaiton_memAlloc 0
#define __compute_inverse__fastImplementaiton 0
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType char
#define allocate_1d_list_dType allocate_1d_list_char
#define allocate_2d_list_dType allocate_2d_list_char
#define free_1d_list_dType free_1d_list_char
#define free_2d_list_dType free_2d_list_char


#include "kt_math_matrix__func__determinant.c"

  /* //! De-allcoate the locally reserved matrix: */
  /* free_2d_list_char(&a); */

  //! @return
  return(det);
}


//! Copmtue the detminant usign recrusive calls
static uchar kt_math__derminant_slow(type_uchar)(uchar **a,const uint n) {
//static t_float __determinant_slow(t_float **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant_slow(type_uchar)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 0
#define __compute_inverse__fastImplementaiton_memAlloc 0
#define __compute_inverse__fastImplementaiton 0
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType uchar
#define allocate_1d_list_dType allocate_1d_list_uchar
#define allocate_2d_list_dType allocate_2d_list_uchar
#define free_1d_list_dType free_1d_list_uchar
#define free_2d_list_dType free_2d_list_uchar

  /* //! Allocate a uchar-matrix */
  /* const t_dType default_value = 0; */
  /* t_dType **a = allocate_2d_list_dType(n, n, default_value); */

#include "kt_math_matrix__func__determinant.c"

  /* //! De-allcoate the locally reserved matrix: */
  /* free_2d_list_uchar(&a); */

  //! @return
  return(det);
}
//! Copmtue the detminant usign recrusive calls
static lowint_s kt_math__derminant_slow(type_lowint_s)(lowint_s **a,const uint n) {
//static t_float __determinant_slow(t_float **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant_slow(type_lowint_s)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 0
#define __compute_inverse__fastImplementaiton_memAlloc 0
#define __compute_inverse__fastImplementaiton 0
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType lowint_s
#define allocate_1d_list_dType allocate_1d_list_lowint_s
#define allocate_2d_list_dType allocate_2d_list_lowint_s
#define free_1d_list_dType free_1d_list_lowint_s
#define free_2d_list_dType free_2d_list_lowint_s

  /* //! Allocate a lowint_s-matrix */
  /* const t_dType default_value = 0; */
  /* t_dType **a = allocate_2d_list_dType(n, n, default_value); */

#include "kt_math_matrix__func__determinant.c"

  /* //! De-allcoate the locally reserved matrix: */
  /* free_2d_list_lowint_s(&a); */

  //! @return
  return(det);
}
//! Copmtue the detminant usign recrusive calls
static lowint kt_math__derminant_slow(type_lowint)(lowint **a,const uint n) {
//static t_float __determinant_slow(t_float **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant_slow(type_lowint)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 0
#define __compute_inverse__fastImplementaiton_memAlloc 0
#define __compute_inverse__fastImplementaiton 0
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType lowint
#define allocate_1d_list_dType allocate_1d_list_lowint
#define allocate_2d_list_dType allocate_2d_list_lowint
#define free_1d_list_dType free_1d_list_lowint
#define free_2d_list_dType free_2d_list_lowint

  /* //! Allocate a lowint-matrix */
  /* const t_dType default_value = 0; */
  /* t_dType **a = allocate_2d_list_dType(n, n, default_value); */

#include "kt_math_matrix__func__determinant.c"

  /* //! De-allcoate the locally reserved matrix: */
  /* free_2d_list_lowint(&a); */

  //! @return
  return(det);
}





//! Copmtue the detminant usign recrusive calls
static t_float kt_math__derminant(type_float)(t_float **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant(type_float)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType t_float
#define allocate_1d_list_dType allocate_1d_list_float
#define allocate_2d_list_dType allocate_2d_list_float
#define free_1d_list_dType free_1d_list_float
#define free_2d_list_dType free_2d_list_float
#include "kt_math_matrix__func__determinant.c"
}

//! Copmtue the detminant usign recrusive calls
static char kt_math__derminant(type_char)(char **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant(type_char)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType char
#define allocate_1d_list_dType allocate_1d_list_char
#define allocate_2d_list_dType allocate_2d_list_char
#define free_1d_list_dType free_1d_list_char
#define free_2d_list_dType free_2d_list_char
#include "kt_math_matrix__func__determinant.c"
}
//! Copmtue the detminant usign recrusive calls
static uchar kt_math__derminant(type_uchar)(uchar **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant(type_uchar)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType uchar
#define allocate_1d_list_dType allocate_1d_list_uchar
#define allocate_2d_list_dType allocate_2d_list_uchar
#define free_1d_list_dType free_1d_list_uchar
#define free_2d_list_dType free_2d_list_uchar
#include "kt_math_matrix__func__determinant.c"
}
//! Copmtue the detminant usign recrusive calls
static lowint kt_math__derminant(type_lowint)(lowint **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant(type_lowint)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType lowint
#define allocate_1d_list_dType allocate_1d_list_lowint
#define allocate_2d_list_dType allocate_2d_list_lowint
#define free_1d_list_dType free_1d_list_lowint
#define free_2d_list_dType free_2d_list_lowint
#include "kt_math_matrix__func__determinant.c"
}
//! Copmtue the detminant usign recrusive calls
static lowint_s kt_math__derminant(type_lowint_s)(lowint_s **a,const uint n) {
#define __compute_inverse__functNameCofactor kt_math__derminant(type_lowint_s)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType lowint_s
#define allocate_1d_list_dType allocate_1d_list_lowint_s
#define allocate_2d_list_dType allocate_2d_list_lowint_s
#define free_1d_list_dType free_1d_list_lowint_s
#define free_2d_list_dType free_2d_list_lowint_s
#include "kt_math_matrix__func__determinant.c"
}



//! Copmtue the detminant usign recrusive calls
static t_float kt_math__derminant_usePow(type_float)(t_float **a,const uint n, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_float)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType t_float
#define allocate_1d_list_dType allocate_1d_list_float
#define allocate_2d_list_dType allocate_2d_list_float
#define free_1d_list_dType free_1d_list_float
#define free_2d_list_dType free_2d_list_float
#include "kt_math_matrix__func__determinant.c"
  return(det);
}
//! Copmtue the detminant usign recrusive calls
static char kt_math__derminant_usePow(type_char)(char **a,const uint n, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_char)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType char
#define allocate_1d_list_dType allocate_1d_list_char
#define allocate_2d_list_dType allocate_2d_list_char
#define free_1d_list_dType free_1d_list_char
#define free_2d_list_dType free_2d_list_char
#include "kt_math_matrix__func__determinant.c"
  return(det);
}
//! Copmtue the detminant usign recrusive calls
static uchar kt_math__derminant_usePow(type_uchar)(uchar **a,const uint n, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_uchar)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType uchar
#define allocate_1d_list_dType allocate_1d_list_uchar
#define allocate_2d_list_dType allocate_2d_list_uchar
#define free_1d_list_dType free_1d_list_uchar
#define free_2d_list_dType free_2d_list_uchar
#include "kt_math_matrix__func__determinant.c"

  //assert(false); // FIXME: remove

  return(det);
}
//! Copmtue the detminant usign recrusive calls
static lowint kt_math__derminant_usePow(type_lowint)(lowint **a,const uint n, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_lowint)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType lowint
#define allocate_1d_list_dType allocate_1d_list_lowint
#define allocate_2d_list_dType allocate_2d_list_lowint
#define free_1d_list_dType free_1d_list_lowint
#define free_2d_list_dType free_2d_list_lowint
#include "kt_math_matrix__func__determinant.c"
  return(det);
}

//! Copmtue the detminant usign recrusive calls
static lowint_s kt_math__derminant_usePow(type_lowint_s)(lowint_s **a,const uint n, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_lowint_s)
#define __compute_inverse__fastImplementaiton_slow_smallValuesNotIfClause 1
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType lowint_s
#define allocate_1d_list_dType allocate_1d_list_lowint_s
#define allocate_2d_list_dType allocate_2d_list_lowint_s
#define free_1d_list_dType free_1d_list_lowint_s
#define free_2d_list_dType free_2d_list_lowint_s
#include "kt_math_matrix__func__determinant.c"
  return(det);
}



//! **********************************************************************************
//! **********************************************************************************
//! **********************************************************************************

//! -------------------------------------------------------------------------
//! Idnietfy the co-factor.
static void kt_math__coFactor(type_float)(t_float **a,const uint n,t_float **b) {
#define __compute_inverse__functNameCofactor kt_math__derminant(type_float)
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType t_float
#define allocate_1d_list_dType allocate_1d_list_float
#define allocate_2d_list_dType allocate_2d_list_float
#define free_1d_list_dType free_1d_list_float
#define free_2d_list_dType free_2d_list_float
#include "kt_math_matrix__func__coFactor.c"
  //  return(det);
}
//! Idnietfy the co-factor.
static void kt_math__coFactor_usePowTable(type_float)(t_float **a,const uint n,t_float **b, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_float)
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType t_float
#define allocate_1d_list_dType allocate_1d_list_float
#define allocate_2d_list_dType allocate_2d_list_float
#define free_1d_list_dType free_1d_list_float
#define free_2d_list_dType free_2d_list_float
  //#define __compute_inverse__dataType t_float
#include "kt_math_matrix__func__coFactor.c"
  //  return(det);
}

//! Idnietfy the co-factor.
static void kt_math__coFactor_slow(type_float)(t_float **a,const uint n,t_float **b) {
#define __compute_inverse__functNameCofactor kt_math__derminant_slow(type_float)
#define __compute_inverse__fastImplementaiton_memAlloc 0
#define __compute_inverse__fastImplementaiton 0
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType t_float
#define allocate_1d_list_dType allocate_1d_list_float
#define allocate_2d_list_dType allocate_2d_list_float
#define free_1d_list_dType free_1d_list_float
#define free_2d_list_dType free_2d_list_float
#include "kt_math_matrix__func__coFactor.c"
}
//! -------------------------------------------------------------------------
//! Idnietfy the co-factor.
static void kt_math__coFactor(type_char)(char **a,const uint n,char **b) {
#define __compute_inverse__functNameCofactor kt_math__derminant(type_char)
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType char
#define allocate_1d_list_dType allocate_1d_list_char
#define allocate_2d_list_dType allocate_2d_list_char
#define free_1d_list_dType free_1d_list_char
#define free_2d_list_dType free_2d_list_char
#include "kt_math_matrix__func__coFactor.c"
  //  return(det);
}
//! Idnietfy the co-factor.
static void kt_math__coFactor_usePowTable(type_uchar)(uchar **a,const uint n,uchar **b, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_uchar)
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType uchar
#define allocate_1d_list_dType allocate_1d_list_uchar
#define allocate_2d_list_dType allocate_2d_list_uchar
#define free_1d_list_dType free_1d_list_uchar
#define free_2d_list_dType free_2d_list_uchar
  //#define __compute_inverse__dataType uchar
#include "kt_math_matrix__func__coFactor.c"
  //  return(det);
}
//! Idnietfy the co-factor.
static void kt_math__coFactor_usePowTable(type_char)(char **a,const uint n, char **b, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_char)
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType char
#define allocate_1d_list_dType allocate_1d_list_char
#define allocate_2d_list_dType allocate_2d_list_char
#define free_1d_list_dType free_1d_list_char
#define free_2d_list_dType free_2d_list_char
  //#define __compute_inverse__dataType char
#include "kt_math_matrix__func__coFactor.c"
  //  return(det);
}
//! Idnietfy the co-factor.
static void kt_math__coFactor_usePowTable(type_lowint)(lowint **a,const uint n,lowint **b, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_lowint)
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType lowint
#define allocate_1d_list_dType allocate_1d_list_lowint
#define allocate_2d_list_dType allocate_2d_list_lowint
#define free_1d_list_dType free_1d_list_lowint
#define free_2d_list_dType free_2d_list_lowint
  //#define __compute_inverse__dataType lowint
#include "kt_math_matrix__func__coFactor.c"
  //  return(det);
}
//! Idnietfy the co-factor.
static void kt_math__coFactor_usePowTable(type_lowint_s)(lowint_s **a,const uint n,lowint_s **b, const t_float *mapOf_pow) {
#define __compute_inverse__functNameCofactor kt_math__derminant_usePow(type_lowint_s)
#define __compute_inverse__fastImplementaiton_memAlloc 1
#define __compute_inverse__fastImplementaiton 1
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 1
#define t_dType lowint_s
#define allocate_1d_list_dType allocate_1d_list_lowint_s
#define allocate_2d_list_dType allocate_2d_list_lowint_s
#define free_1d_list_dType free_1d_list_lowint_s
#define free_2d_list_dType free_2d_list_lowint_s
  //#define __compute_inverse__dataType lowint_s
#include "kt_math_matrix__func__coFactor.c"
  //  return(det);
}

//! Idnietfy the co-factor.
static void kt_math__coFactor_slow(type_lowint)(lowint **a,const uint n,lowint **b) {
#define __compute_inverse__functNameCofactor kt_math__derminant_slow(type_lowint)
#define __compute_inverse__fastImplementaiton_memAlloc 0
#define __compute_inverse__fastImplementaiton 0
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType lowint
#define allocate_1d_list_dType allocate_1d_list_lowint
#define allocate_2d_list_dType allocate_2d_list_lowint
#define free_1d_list_dType free_1d_list_lowint
#define free_2d_list_dType free_2d_list_lowint
#include "kt_math_matrix__func__coFactor.c"
}
//! Idnietfy the co-factor.
static void kt_math__coFactor_slow(type_lowint_s)(lowint_s **a,const uint n,lowint_s **b) {
#define __compute_inverse__functNameCofactor kt_math__derminant_slow(type_lowint_s)
#define __compute_inverse__fastImplementaiton_memAlloc 0
#define __compute_inverse__fastImplementaiton 0
#define __compute_inverse__fastImplementaiton__usePreComputedPowTable 0
#define t_dType lowint_s
#define allocate_1d_list_dType allocate_1d_list_lowint_s
#define allocate_2d_list_dType allocate_2d_list_lowint_s
#define free_1d_list_dType free_1d_list_lowint_s
#define free_2d_list_dType free_2d_list_lowint_s
#include "kt_math_matrix__func__coFactor.c"
}





/*
   Transpose of a square matrix, do it in place
*/
static void __transpose(t_float **a,const uint n)
{
  uint i,j;
  t_float tmp;

  for (i=1;i<n;i++) {
    for (j=0;j<i;j++) {
      tmp = a[i][j];
      a[i][j] = a[j][i];
      a[j][i] = tmp;
    }
  }
}

/**
   @brief comtue the inverse matrix (oekseth, 06. okt. 2016).
   @param <matrix> is the matrix to inverse
   @param <size> is the matrix-size of an nxn matrix.
   @param <self> is the configuration of the inverse-matrix-opertiaont: for default value-configuraiton use the call to "getEmptyObject__s_kt_math_matrix_config_inverse()".
   @return the inversed matrix.
 **/
t_float **get_inverseMatrix(t_float **matrix, const uint size, const s_kt_math_matrix_config_inverse_t self) {
  const t_float default_value_float = 0;
  t_float **result_matrix = allocate_2d_list_float(size, size, default_value_float);

  //void **compressedMatrix = NULL;
  e_mask_api_valueRangeClassification_t type_valueRange = e_mask_api_valueRangeClassification_undef;
  if(self.isToUse_valueRangeCompression) {
    s_mask_api_config_t mask_self; setTo_empty__s_mask_api_config(&mask_self);
    type_valueRange = get_valueRange_forMatrix__mask_api(matrix, NULL, size, size, mask_self);
  }

#define __MiF__copy() ({for(uint i = 0; i < size; i++) {for(uint k = 0; k < size; k++) {result_matrix[i][k] = matrix_local_result[i][k];}}})
  
  t_float *mapOf_pow = NULL;
  if(self.in_coFactor_isToUse_preComputedPows) {
    const uint pow_size = 2*size + 2;
    mapOf_pow = allocate_1d_list_float(pow_size, default_value_float);
    for(uint i = 0; i < pow_size; i++) { mapOf_pow[i] = pow(-1.0, i); } //! which is used to check if number is odd.
  }
  if(self.typeOf_optimization == e_kt_math_matrix__typeOfComputationInverse_coFactor) {
    //! Comptue the determinant:
    t_float div_factor = 0;
    if( self.in_coFactor_isToUse_fastMemAlloc && self.in_coFactor_isToUse_preComputedPows && self.isToUse_fast_genericImplmentation__forLoops && self.isToUse_fast_genericImplmentation__ifClauseOrder) {
      
      if(type_valueRange == e_mask_api_valueRangeClassification_char_signed) {
	//! Allocate a char-matrix
	const char default_value = 0;
	char **matrix_local = allocate_2d_list_char(size, size, default_value);
	const long long unsigned int total_size = size*size;
	for(long long unsigned int i = 0; i < total_size; i++) {matrix_local[0][i] = matrix[0][i];} //! ie, copy:
	//! The operation:	
	div_factor = (t_float)kt_math__derminant_usePow(type_char)(matrix_local, size, mapOf_pow);
	//! De-allcoate the locally allcoated matrix:
	free_2d_list_char(&matrix_local, size);
      } else if(type_valueRange == e_mask_api_valueRangeClassification_char_unsigned) {
	//! Allocate a uchar-matrix
	const uchar default_value = 0;
	uchar **matrix_local = allocate_2d_list_uchar(size, size, default_value);
	const long long unsigned int total_size = size*size;
	for(long long unsigned int i = 0; i < total_size; i++) {matrix_local[0][i] = matrix[0][i];} //! ie, copy:
	//! The operation:	
	div_factor = (t_float)kt_math__derminant_usePow(type_uchar)(matrix_local, size, mapOf_pow);
	//! De-allcoate the locally allcoated matrix:
	free_2d_list_uchar(&matrix_local, size);
      } else if(type_valueRange == e_mask_api_valueRangeClassification_16_bit_unsigned) {
	//! Allocate a lowint-matrix
	const lowint default_value = 0;
	lowint **matrix_local = allocate_2d_list_lowint(size, size, default_value);
	const long long unsigned int total_size = size*size;
	for(long long unsigned int i = 0; i < total_size; i++) {matrix_local[0][i] = matrix[0][i];} //! ie, copy:
	//! The operation:	
	div_factor = (t_float)kt_math__derminant_usePow(type_lowint)(matrix_local, size, mapOf_pow);
	//! De-allcoate the locally allcoated matrix:
	free_2d_list_lowint(&matrix_local, size);
      } else if(type_valueRange == e_mask_api_valueRangeClassification_16_bit_signed) {
	//! Allocate a lowint_s-matrix
	const lowint_s default_value = 0;
	lowint_s **matrix_local = allocate_2d_list_lowint_s(size, size, default_value);
	const long long unsigned int total_size = size*size;
	for(long long unsigned int i = 0; i < total_size; i++) {matrix_local[0][i] = matrix[0][i];} //! ie, copy:
	//! The operation:	
	div_factor = (t_float)kt_math__derminant_usePow(type_lowint_s)(matrix_local, size, mapOf_pow);
	//! De-allcoate the locally allcoated matrix:
	free_2d_list_lowint_s(&matrix_local, size);
      } else {
	div_factor = kt_math__derminant_usePow(type_float)(matrix, size, mapOf_pow);
      }
    } else if( !self.in_coFactor_use_fast ) {
      div_factor = kt_math__derminant_slow(type_float)(matrix, size);
    } else {
      div_factor = kt_math__derminant(type_float)(matrix, size);
    }
    // assert(div_factor != 0); //! ie, what we expect:
    if(div_factor != 0) {
      //! Compute the co-factor matrix:
      if( self.in_coFactor_isToUse_fastMemAlloc && self.in_coFactor_isToUse_preComputedPows && self.isToUse_fast_genericImplmentation__forLoops && self.isToUse_fast_genericImplmentation__ifClauseOrder) {
	if(type_valueRange == e_mask_api_valueRangeClassification_char_signed) {
	  //! Allocate a char-matrix
	  const char default_value = 0;
	  char **matrix_local = allocate_2d_list_char(size, size, default_value);
	  char **matrix_local_result = allocate_2d_list_char(size, size, default_value);
	  const long long unsigned int total_size = size*size;
	  for(long long unsigned int i = 0; i < total_size; i++) {matrix_local[0][i] = matrix[0][i];} //! ie, copy:
	  //! The operation:	
	  kt_math__coFactor_usePowTable(type_char)(matrix_local, size, matrix_local_result, mapOf_pow);
	  //div_factor = (t_float)kt_math__derminant_usePow(type_char)(matrix_local, size, mapOf_pow);
	  //! De-allcoate the locally allcoated matrix:
	  free_2d_list_char(&matrix_local, size);
	  __MiF__copy(); // matrix[0][i] = matrix_local_result[0][i];} free_2d_list_char(&matrix_local_result); //! ie, copy and de-allcoate.
	} else if(type_valueRange == e_mask_api_valueRangeClassification_char_unsigned) {
	  //! Allocate a uchar-matrix
	  const uchar default_value = 0;
	  uchar **matrix_local = allocate_2d_list_uchar(size, size, default_value);
	  uchar **matrix_local_result = allocate_2d_list_uchar(size, size, default_value);
	  const long long unsigned int total_size = size*size;
	  for(long long unsigned int i = 0; i < total_size; i++) {matrix_local[0][i] = matrix[0][i];} //! ie, copy:
	  //! The operation:	
	  kt_math__coFactor_usePowTable(type_uchar)(matrix_local, size, matrix_local_result, mapOf_pow);
	  //div_factor = (t_float)kt_math__derminant_usePow(type_uchar)(matrix_local, size, mapOf_pow);
	  //! De-allcoate the locally allcoated matrix:
	  free_2d_list_uchar(&matrix_local, size);
	  __MiF__copy(); // matrix[0][i] = matrix_local_result[0][i];} free_2d_list_uchar(&matrix_local_result, size); //! ie, copy and de-allcoate.
	} else if(type_valueRange == e_mask_api_valueRangeClassification_16_bit_unsigned) {
	  //! Allocate a lowint-matrix
	  const lowint default_value = 0;
	  lowint **matrix_local = allocate_2d_list_lowint(size, size, default_value);
	  lowint **matrix_local_result = allocate_2d_list_lowint(size, size, default_value);
	  const long long unsigned int total_size = size*size;
	  for(long long unsigned int i = 0; i < total_size; i++) {matrix_local[0][i] = matrix[0][i];} //! ie, copy:
	  //! The operation:	
	  kt_math__coFactor_usePowTable(type_lowint)(matrix_local, size, matrix_local_result, mapOf_pow);
	  //div_factor = (t_float)kt_math__derminant_usePow(type_lowint)(matrix_local, size, mapOf_pow);
	  //! De-allcoate the locally allcoated matrix:
	  free_2d_list_lowint(&matrix_local, size);
	  __MiF__copy(); // matrix[0][i] = matrix_local_result[0][i];} free_2d_list_lowint(&matrix_local_result, size); //! ie, copy and de-allcoate.
	} else if(type_valueRange == e_mask_api_valueRangeClassification_16_bit_signed) {
	  //! Allocate a lowint_s-matrix
	  const lowint_s default_value = 0;
	  lowint_s **matrix_local = allocate_2d_list_lowint_s(size, size, default_value);
	  lowint_s **matrix_local_result = allocate_2d_list_lowint_s(size, size, default_value);
	  const long long unsigned int total_size = size*size;
	  for(long long unsigned int i = 0; i < total_size; i++) {matrix_local[0][i] = matrix[0][i];} //! ie, copy:
	  //! The operation:	
	  kt_math__coFactor_usePowTable(type_lowint_s)(matrix_local, size, matrix_local_result, mapOf_pow);
	  //div_factor = (t_float)kt_math__derminant_usePow(type_lowint_s)(matrix_local, size, mapOf_pow);
	  //! De-allcoate the locally allcoated matrix:
	  free_2d_list_lowint_s(&matrix_local, size);
	  __MiF__copy(); // matrix[0][i] = matrix_local_result[0][i];} free_2d_list_lowint_s(&matrix_local_result, size); //! ie, copy and de-allcoate.
	} else { 
	  kt_math__coFactor_usePowTable(type_float)(matrix, size, result_matrix, mapOf_pow);
	}
      } else if( !self.in_coFactor_use_fast ) {
      	kt_math__coFactor_slow(type_float)(matrix, size, result_matrix);
      } else {
	kt_math__coFactor(type_float)(matrix, size, result_matrix);
      }
      const t_float div_factor_inv = 1/div_factor;
      //! Update the matrix:
      for(uint row_id = 0; row_id < size; row_id++) {
	for(uint col_id = 0; col_id < size; col_id++) {
	  result_matrix[row_id][col_id] *= div_factor_inv;
	}
      }
      //! Transpose the result:
      __transpose(result_matrix, size);
      //! ---------- and at this exeuciton-poitn we asusme the inverse (of the input-matrix) is correctly comptued.
    } else {assert(default_value_float == 0);} //! ie, to avodi any unnceccary memory-calls.        
  } else {

    assert(false); // FIXME: add support for this.
  }
  if(self.in_coFactor_isToUse_preComputedPows) {
    assert(mapOf_pow);
    free_1d_list_float(&mapOf_pow);
  }

  return result_matrix;
}



//! Compute the LU-de-composed matrix
static void LU(t_float **matrix, const uint n, const bool isTo_useRestrictKeyWord) {

  assert(false); // FIXME: make use of this ... eg, in the cotnext of "http://www.physics.iitm.ac.in/~sunil/nummet/programs/applications/matrices/LU-inverse.c" .... when we have understood why the latter code-chunk is in-complete wrt. matrix-inverse-comptaution (oekseth, 06. otk. 2016).

  if(isTo_useRestrictKeyWord) {
    for(uint k=0; k<=n-1; k++) {    
      const t_float k_inv = (matrix[k][k]) ? 1/matrix[k][k] : 0;
      for(uint j=k+1; j<=n; j++) {
	const t_float *__restrict__ row_j = matrix[j];
	const t_float *__restrict__ row_k = matrix[k];
	t_float x =  matrix[j][k] * k_inv;
	for(uint i=k; i <=n; i++) {  
	  //! Note: "j > k", ie, the transposed matrix will never be updated 'during the inner looop'	
	  matrix[j][i] = row_j[i]- x*row_k[i];
	}
	//! Note: "j > k", ie, the transposed matrix will never be updated 'during the inner looop'	
	matrix[j][k] = x;
      }
    }    
  } else {
    for(uint k=0; k<=n-1; k++) {    
      const t_float k_inv = (matrix[k][k]) ? 1/matrix[k][k] : 0;
      for(uint j=k+1; j<=n; j++) {
	t_float x= matrix[j][k] * k_inv;
	for(uint i=k; i <=n; i++) {  
	  //! Note: "j > k", ie, the transposed matrix will never be updated 'during the inner looop'	
	  matrix[j][i] = matrix[j][i]- x*matrix[k][i];
	}
	//! Note: "j > k", ie, the transposed matrix will never be updated 'during the inner looop'	
	matrix[j][k] = x;
      }
    }    
  }
}


