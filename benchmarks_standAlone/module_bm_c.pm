#!/usr/bin/perl
package module_bm_c;
use Carp;
use strict;
use Time::HiRes qw(usleep ualarm gettimeofday tv_interval);

sub apply {
    my ($dim_base_size, $dim_iter, $cnt_call_each) = @_;
    #!
    #!
    #! Note: belwo cofnigurations are fectehd from "x_measure_scriptLanguages.pl".
    #!
    #my @arr_compiler = ("perl"); #! ie, as our test-system (ubunut 18) did not have any alternative Perl-veriosns in its package-management-system.
    #my $file_name =  "tut_main_matrixBM.pl";
    #!
    # FIXME: difference between below?
    #my $file_name = "ops_matrix.c";
    #my $file_name = "strassen_main.c";
    my @arr_compiler = (
	"gcc", ,"gcc-4.8", "gcc-5",  "gcc-6",  "gcc-7",  "gcc-8",		 "g++", , "g++-4.8",  "g++5", "g++-6",  "g++-7",  "g++-8", 
	);
    #!
    #! -----------------------
    my $result_file_min = "res-x_tut_2_c_min.tsv";
    my $result_file_max = "res-x_tut_2_c_max.tsv";
    open(FILE_OUT_MIN, ">$result_file_min") or die("Unable to open the input-file $result_file_min\n");
    open(FILE_OUT_MAX, ">$result_file_max") or die("Unable to open the input-file $result_file_max\n");
    #!
    #! Add header:
    printf(FILE_OUT_MIN "#! Compiler \t Function \t Dimension\t Time\t Code-file\n");
    printf(FILE_OUT_MAX "#! Compiler \t Function \t Dimension\t Time\t Code-file\n");
    #printf(FILE_OUT_MIN "#! Compiler \t Function \t Dimension\t Time\n");
    #printf(FILE_OUT_MAX "#! Compiler \t Function \t Dimension\t Time\n");
    #--------------
    foreach my $file_name ("ops_matrix.c", "strassen_main.c") {
	    #!
	my @arr_func = (
	    "dot_slow_transposed", #! where 'this' changes the order of the "row--column" iteration, ie, "column-->row" instead of "row-->column" data traversal.
	    "dot_slow_IfClausesMaskAllTrue",
	    "ifClauseMaskAllFalse", #! an extreme case of data-elvaution ... where none of the cells are intersitng .. hence, no need to apply any arithemtics ... imporatnce of being warea of the data used as input ... 
	    "dot_slow_IfClauses",
	    "dot_slow_IfClausesNan",
	    #!
	    #! Tiling:
	    "dot_fast-64", #! SM=64
	    "dot_fast-32", #! SM=32
	    "dot_fast_xmtSSE-32", #! SM=32
	    "dot_fast_1Add_1Add-128", #! kt_func_metricInner_fast__fitsIntoSSE__float__2Add
	    "dot_fast_xmtSSE-128-wAsserts", # -- use-cases ... does ''aserts(..)' slwo down code when compield in optmzied mode?
	    );
	if($file_name eq "strassen_main.c") {
	    @arr_func = (
		#! Effects of naive strateiges:
		"matrix_naive_float",
		#! Deos data-comrpession help?
		"fast_int_16bit-mult-64",
		"fast_int_32bit-mult-64",
		#!
		#! Value compression:
		"fast_int_16bit-mult-64-xmtSSE", #! does value-copmpression help for cases where SIMD is not used?
		"dot_fast_dataTypes_16bit_load-128", #! effects of setting SM=128 --- improtance, and sensitvyt, of ...
		#!
		#! Effects of Strassen's algorithm?
		"strassen_float",
		"strassen_int_32bit",
		);
	}
	#!
	#!
	#! ----------
	foreach my $comp (@arr_compiler) {
	    #my $cmd = $comp . 'javac ' . $file_name;
	    #printf("CMD\t " . $cmd . "\n ");
	    #system($cmd);
	    foreach my $strategy ("-O2", "-O3") { #! then seperate image-cosntruciton between the different strategies: 
		my $cmd        = $comp . " " . $file_name . " -std=c99 -lm -std=gnu99 -mssse3 " . $strategy; # . " " . $fixed_compileParams;
		printf("CMD\t " . $cmd . "\n ");
		system($cmd);
		foreach my $fun (@arr_func) {
		    for(my $i = 1; $i <= $dim_iter; $i++) {
			my $nrows = $dim_base_size * $i; #! eg, 64, 128, ...
			#! ---------
			my $t_min = undef;
			my $t_max = undef;
			for(my $iter = 0; $iter < $cnt_call_each; $iter++) {
			    my $t0 = [gettimeofday];
			    #my $cmd = "time " . $comp . "java -classpath . $class_name $fun $nrows"; #! eg, java tut_main_matrixBM.py _3d_mult 1000
			    my $case_id = $fun;
			    my $cmd = "./a.out " . $case_id . " " . $nrows . " 2>err.txt";
			    #my $cmd = "time $comp $file_name $fun $nrows"; #! eg, java tut_main_matrixBM.py _3d_mult 1000
			    printf("CMD\t " . $cmd . "\n ");
			    system($cmd);
			    my $duration_curr = tv_interval ($t0, [gettimeofday]);
			    if(!defined($t_min) || ($t_min > $duration_curr) ) {
				$t_min = $duration_curr;
			    }
			    if(!defined($t_max) || ($t_max < $duration_curr) ) {
				$t_max = $duration_curr;
			    }
			    #croak("...");
			}
			#!
			#! Update result-files:
			my $comp_s = $comp . " " . $strategy;
			printf(FILE_OUT_MIN "%s\t%s\t%u\t%.2f\t%s\n", $comp, $fun, $nrows, $t_min, $file_name);
			printf(FILE_OUT_MAX "%s\t%s\t%u\t%.2f\t%s\n", $comp, $fun, $nrows, $t_max, $file_name);
			#printf(FILE_OUT_MIN "%s\t%s\t%u\t%.2f\n", $comp_s, $fun, $nrows, $t_min);
			#printf(FILE_OUT_MAX "%s\t%s\t%u\t%.2f\n", $comp_s, $fun, $nrows, $t_max);
		    }
		}
	    }
	}
    }
    #!
    #! CLOSE:
    close(FILE_OUT_MIN);
    close(FILE_OUT_MAX);    
}

#! ============================= EOF:
1;
