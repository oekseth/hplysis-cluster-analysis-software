#ifndef kt_mathMacros_h
#define kt_mathMacros_h


/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_mathMacros
   @brief the main access point for for macros wrt. math-opteraitons on matrices using SSE
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"


//! @remarks we expect the matrix-rows to be deivsialbe by "nrows", ie, that 'zeros' are padded if neeccesary.
#define kt_mathMacros_SSE__transpose(nrows, ncols, matrix, result) ({\
  for (uint i = 0; i < ncols; i += 1) { \
    t_float *__restrict__ res = result[i];  \
  for (uint j = 0; j < nrows; j += VECTOR_FLOAT_ITER_SIZE) { \
	VECTOR_FLOAT_STORE(&res[j], VECTOR_FLOAT_setFrom_columns(matrix, j, i)); \
  } } } ) //! ie, complete
#define kt_mathMacros_SSE__transpose_uint(nrows, ncols, matrix, result) ({\
  for (uint i = 0; i < ncols; i += 1) { \
    uint *__restrict__ res = result[i];  \
  for (uint j = 0; j < nrows; j += VECTOR_UINT_ITER_SIZE) { \
	VECTOR_UINT_STORE(&res[j], VECTOR_UINT_setFrom_columns(matrix, j, i)); \
  } } } ) //! ie, complete
#define kt_mathMacros_SSE__transpose_char(nrows, ncols, matrix, result) ({\
  for (uint i = 0; i < ncols; i += 1) { \
    char *__restrict__ res = result[i];  \
  for (uint j = 0; j < nrows; j += VECTOR_CHAR_ITER_SIZE) { \
	VECTOR_CHAR_STORE(&res[j], VECTOR_CHAR_setFrom_columns(matrix, j, i)); \
  } } } ) //! ie, complete

//! @remarks we expect the matrix-rows to be deivsialbe by "ncols", ie, that 'zeros' are padded if neeccesary.
#define kt_mathMacros_SSE__dotProduct_vector(ncols, _rmul1, _rmul2) ({	\
  /*! Intiate: */ \
  const t_float *__restrict__ mul1 = _mul1; const t_float *__restrict__ rmul2 = _rmul2; \
  uint j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&rmul1[j]), VECTOR_FLOAT_LOADU(&rmul2[j])); j = VECTOR_FLOAT_ITER_SIZE; \
  for (; j < ncols; j += VECTOR_FLOAT_ITER_SIZE) { \
    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&rmul1[j]), VECTOR_FLOAT_LOADU(&rmul2[j]))); } \
  VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); } ) //! ie, return the scalar of the value.




// FIXME: merge [ªbove] with "TransposeMatrix(..)" in "math.cxx"

// FIMXE: describe how z-score is used .... and consider supporting 'this' in our "e_kt_correlationFunction.h"

// FIXME[perofrmance]: compare the perofrmance of [”elow] 'gaussian' functiosn to oru 'seperate C impelmetnation of the nroamlziation-rpcoesudre ... use the difference wrt. the performance-reuslts as an example of 'synetic perofmrnac-eoptizmaiotn-strategies'.


#define kt_mathMacros_SSE__normalize(_row_input, _row_result, ncols_adjusted) ({ \
      const t_float *__restrict__ rmul1 = _row_input; \
      t_float * row_result = _row_result; \
      uint j = 0;  VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&rmul1[j]); \
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_1, vec_1); j += VECTOR_FLOAT_ITER_SIZE; \
      for(j; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) { \
	vec_1 = VECTOR_FLOAT_LOADU(&rmul1[j]);	\
	vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(vec_1, vec_1)); } \
      const t_float scalar = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); \
      t_float result = 0; \
      if(scalar > 0) { \
      const t_float sqrt_inv = 1/mathLib_float_sqrt(scalar); \
      const VECTOR_FLOAT_TYPE vec_sqrt_inv = VECTOR_FLOAT_SET1(sqrt_inv); \
      j = 0;  VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&rmul1[j]); \
      VECTOR_FLOAT_STORE(&row_result[j], VECTOR_FLOAT_MUL(vec_1, vec_sqrt_inv)); j += VECTOR_FLOAT_ITER_SIZE; \
      for(j; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {		\
	vec_1 = VECTOR_FLOAT_LOADU(&rmul1[j]);	\
	VECTOR_FLOAT_STORE(&row_result[j], VECTOR_FLOAT_MUL(vec_1, vec_sqrt_inv));  \
	} \
      } else {for(uint j = 0; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) { VECTOR_FLOAT_STORE(&row_result[j], VECTOR_FLOAT_SET_zero());}} \
    }) //! ie, 'return' as the row_result is updated.

//! Compute: ret[i] += row[i][j] * v[i] 
#define kt_mathMacros_SSE__matrixTransposed_mult_vector(matrix, nrows_adjusted, ncols_adjusted, _row_input, _row_result) ({ \
  const t_float *__restrict__ row_input  = _row_input; \
   t_float *__restrict__ row_result = _row_result; \
  for(uint i = 0; i < nrows_adjusted; i++) { \
  const t_float *__restrict__ row = matrix[i]; \
  uint j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row[j]), VECTOR_FLOAT_LOADU(&row_input[j])); \
  for (; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {		\
    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row[j]), VECTOR_FLOAT_LOADU(&row_input[j]))); \
  } row_result[i] = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); } }) //! ie, store seperately for each row.



//! Compute: ret[j] += row[i][j] * v[i] 
#define kt_mathMacros_SSE__vector_mult_vector(_row, ncols_adjusted, _row_input, row_result) ({ \
  const t_float *__restrict__ row = _row; \
  const t_float *__restrict__ row_input = _row_input; \
  for (uint j = 0; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {	\
    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_result[j]), VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row[j]), VECTOR_FLOAT_LOADU(&row_input[j]))); \
    VECTOR_FLOAT_STORE(&row_result[j], vec_result);} })

#define kt_mathMacros_SSE__matrix_mult_vector(matrix, nrows_adjusted, ncols_adjusted, _row_input, _row_result) ({ \
  const t_float *__restrict__ row_input  = _row_input; \
   t_float *__restrict__ row_result = _row_result; \
   /*! Reset the vector: */ \
   for(uint j = 0; j < nrows_adjusted; j++) {row_result[j] = 0; } \
   /*! Stat the 2d-for-loop: */ \
   for(uint i = 0; i < ncols_adjusted; i++) {  \
     const t_float *__restrict__ row = matrix[i];			\
     for (uint j = 0; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {		\
       VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_result[j]), VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row[j]), VECTOR_FLOAT_LOADU(&row_input[j]))); \
       VECTOR_FLOAT_STORE(&row_result[j], vec_result);			\
     } } }) //! ie, store seperately for each row.


#define kt_mathMacros_SSE__vec_mul_scalar(_row_input, ncols_adjusted, scalar, _row_result) ({ \
  const t_float *__restrict__ row_input  = _row_input; \
   t_float *__restrict__ row_result = _row_result; \
   /*! Stat the 1d-for-loop: */						\
   const VECTOR_FLOAT_TYPE vec_scalar = VECTOR_FLOAT_SET1(scalar); \
   for (uint j = 0; j < ncols; j += VECTOR_FLOAT_ITER_SIZE) {		\
     VECTOR_FLOAT_STORE(&row_result[j], VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_input[j]), vec_scalar));} }) //! ie, iterate through the for-loop.
// FIXME[critila]: validate correctness of [”elow] "mean" computations.


//! Compute: row[j] += row[j] - (row_input_1[index] * row_input_2[j]);
//       VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row[j]), VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_input_1[index]), VECTOR_FLOAT_LOADU(&row_input_2[j]))); 
#define kt_mathMacros_SSE__row_subtractFrom_self_2InputVectors(row, ncols_adjusted, row_input_1, row_input_2, index) ({ \
     for (uint j = 0; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {		\
       VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row[j]), VECTOR_FLOAT_MUL(VECTOR_FLOAT_SET1(row_input_1[index]), VECTOR_FLOAT_LOADU(&row_input_2[j]))); \
     /*! Update the input-matrix: */ \
     VECTOR_FLOAT_STORE(&row[j], vec_result);	\
     } })
//! Compute: matrix[i][j] += matrix[i][j] - (row_input_1[i] * row_input_2[j]);
#define kt_mathMacros_SSE__matrix_subtractFrom_self_2InputVectors(matrix, nrows_adjusted, ncols_adjusted, _row_input_1, _row_input_2) ({ \
  const t_float *__restrict__ row_input_1  = _row_input_1; \
  const t_float *__restrict__ row_input_2  = _row_input_2; \
   /*! Stat the 2d-for-loop: */ \
   for(uint i = 0; i < nrows_adjusted; i++) {  \
     t_float *__restrict__ row = matrix[i];				\
     kt_mathMacros_SSE__row_subtractFrom_self_2InputVectors(row, ncols_adjusted, row_input_1, row_input_2, /*index=*/i);    } }) //! ie, store seperately for each row.

//! Copy the inptu-vector to the output-vector.
#define kt_mathMacros_SSE__row_copy(row_input, row_result, ncols_adjusted) ({ \
      memcpy(row_result, row_input, sizeof(t_float)*ncols_adjusted); })

//#define kt_mathMacros_SSE__matrix_row_copy(


// const t_float default_value_float = 0;   t_float *row_result = allocate_1d_list_float(nrows_adjusted, default_value_float); 
//! @return the non-adjusted mean for the vlaeus, ie, the sum of valeus without adjustment to the 'mean'.
#define kt_mathMacros_SSE__computeGauss__mean(matrix, nrows_adjusted, nrows, ncols_adjusted, ncols, _row_result) ({ \
      t_float *__restrict__ row_result = _row_result;			\
      /*! Reset the vector: */						\
      /*for(uint j = 0; j < ncols_adjusted; j++) {row_result[j] = 0; }*/ \
      /*! Start the 2d-for-loop: */					\
      for (uint i = 0; i < nrows_adjusted; i += 1) {			\
	const t_float *__restrict__ row = matrix[i];			\
	uint j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOADU(&row[j]); j += VECTOR_FLOAT_ITER_SIZE; \
	for(j; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) { vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOADU(&row[j]));} \
	row_result[i] =   VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);} \
      /*! Copmute: B[i] = B[i] * (1/ncols): */						\
      const t_float ncols_inverse = 1/ncols; for(uint j = 0; j < nrows; j++) {row_result[j] *= ncols_adjusted; } \
      row_result;}) //! ie, return the sum of valeus seperately for each row.
//! @return the 'non_Adjusted men' for a row (instead of  a matrix):
#define kt_mathMacros_SSE__computeGauss__mean__forRow(_row, ncols_adjusted, ncols_inverted) ({ \
      const t_float *__restrict__ rmul1 = _row;				\
      uint j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_LOADU(&rmul1[j]); j += VECTOR_FLOAT_ITER_SIZE; \
      for(j; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) { vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOADU(&rmul1[j]));} \
      VECTOR_FLOAT_storeAnd_horizontalSum(vec_result) / ncols_inverted;}) //! ie, return the sum of valeus seperately for the/a given row.

//! Compute(1): result[i] += (M[i][j] - row_mean[i])^2
//! Compute(2): result[i] = sqrt(result[i] / (ncols - 1))
#define kt_mathMacros_SSE__computeGauss__STD(matrix, nrows_adjusted, ncols_adjusted, ncols, mapOf_means) ({ \
  const t_float default_value_float = 0; \
  t_float *row_result = allocate_1d_list_float(nrows_adjusted, default_value_float); \
  for (uint i = 0; i < nrows_adjusted; i += 1) {					\
  const t_float *__restrict__ rmul1 = matrix[i]; \
  const VECTOR_FLOAT_TYPE vec_mean = VECTOR_FLOAT_SET1(mapOf_means[i]); \
  uint j = 0; VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&rmul1[j]), vec_mean);  \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_diff, vec_diff); j += VECTOR_FLOAT_ITER_SIZE; \
  /*! Compute: result[i] += (M[i][j] - row_mean[i])^2 */ \
  for(j; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) { \
    vec_diff = VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&rmul1[j]), vec_mean); \
    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(vec_diff, vec_diff)); } \
  row_result[i] = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); }	\
  /*! Compute: result[i] = sqrt(result[i] / (ncols - 1)) */		\
  const t_float ncols_inv = 1/(ncols - 1); const VECTOR_FLOAT_TYPE vec_ncols_inv = VECTOR_FLOAT_SET1(ncols_inv);  \
  for(uint j = 0; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {	\
    VECTOR_FLOAT_STORE(&row_result[j], VECTOR_FLOAT_SQRT(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_result[j]), vec_ncols_inv))); \
  } row_result;}) //! ie, return the sum of valeus seperately for each row.


//! @return the 'non_Adjusted men' for a row (instead of  a matrix):
#define kt_mathMacros_SSE__computeGauss__STD__vector(_row, scalar_mean, ncols_adjusted, ncols_inv) ({ \
      const t_float *__restrict__ rmul1 = _row;		\
      /*const t_float *__restrict__ row_mean = _row_mean;	*/	\
      const VECTOR_FLOAT_TYPE vec_mean = VECTOR_FLOAT_SET1(scalar_mean); \
      uint j = 0; VECTOR_FLOAT_TYPE vec_diff = VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&rmul1[j]), vec_mean); \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_diff, vec_diff); j += VECTOR_FLOAT_ITER_SIZE; \
  /*! Compute: result[i] += (M[i][j] - row_mean[i])^2 */ \
  for(j; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) { \
    vec_diff = VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&rmul1[j]), vec_mean); \
    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(vec_diff, vec_diff)); } \
  t_float result = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result); 	\
  /*! Compute: result[i] = sqrt(result[i] / (ncols - 1)) */		\
  result = mathLib_float_sqrt(result *ncols_inv); }) //! ie, return the result.





//! Compute: matrix[i][j] = (matrix[i][j] - mean[i]) * ptstd_inv[i];
#define kt_mathMacros_SSE__computeGauss__ZScores(matrix, nrows_adjusted, ncols_adjusted, _row_mean, _row_std) ({ \
      float *__restrict__ row_mean = _row_mean;				\
      float *__restrict__ row_std = _row_std;				\
      for(uint i = 0; i < nrows_adjusted; i++) {			\
	float *__restrict__ row = matrix[i];				\
	const VECTOR_FLOAT_TYPE vec_mean = VECTOR_FLOAT_SET1(row_mean[i]); \
	const VECTOR_FLOAT_TYPE vec_std_inv = VECTOR_FLOAT_SET1( (row_mean[i] != 0) ? 1/row_mean[i] : 0); \
	for(uint j = 0; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {			\
	  VECTOR_FLOAT_STORE(&row[j], VECTOR_FLOAT_MUL(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row[j]), vec_mean), vec_std_inv)); } } \
    })

#define kt_mathMacros_SSE__computeGauss__ZScores__vector__inverted(_row, ncols_adjusted, row_mean, _row_std, _row_result) ({ \
  float *__restrict__  row = _row; 					\
  float *__restrict__ row_mean = _row_mean;				\
  float *__restrict__ row_std = _row_std;				\
  float *__restrict__ row_result = _row_result;				\
  for(uint j = 0; j < ncols_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {				\
    VECTOR_FLOAT_STORE(&row_result[j], VECTOR_FLOAT_DIV(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row[j]), VECTOR_FLOAT_LOADU(&row_mean[j])), VECTOR_FLOAT_LOADU(&row_std[j]))); } })



//! Convert from a genrelized format ot an SSE-optmizd data-access-format
#define kt_mathMacros_SSE__convertToInternalMatrix(matrix, nrows, ncols) ({ \
  const t_float default_value_float = 0; \
  /*! Pad the matrix with zeros, ie, if neccdesary:*/ \
  const uint nrows_adjusted = (1+(nrows /VECTOR_FLOAT_ITER_SIZE))*VECTOR_FLOAT_ITER_SIZE; \
  assert(nrows_adjusted >= nrows); \
  const uint ncols_adjusted = (1+(ncols /VECTOR_FLOAT_ITER_SIZE))*VECTOR_FLOAT_ITER_SIZE; \
  assert(ncols_adjusted >= ncols); \
  t_float **resultMatrix = allocate_2d_list_float(nrows_adjusted, ncols_adjusted, default_value_float); \
  /*! Copy: */ \
  for(uint row_id = 0; row_id < nrows; row_id++) { \
    memcpy(resultMatrix[row_id], matrix[row_id], sizeof(t_float)*ncols); \
  } resultMatrix;}) //! ie, return the updated matrix.


// FIXME: write permtuationss for [”elow]:

// FIXME: make use of [”elow] "ConvertMatrixFormat(..)" in oru udpated/improved algorithm ... covnering to 'our' format.



// FIXME: include [”elow] (oekseth, 06. sept. 2016).
// FIXME: tehreafter update the "maths.cxx" file using non-SSE-code


//#define kt_mathMacros_SSE__


#endif //! EOF

