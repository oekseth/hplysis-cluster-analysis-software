#ifndef kt_matrix_extendedLogics_h
#define kt_matrix_extendedLogics_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_matrix_extendedLogics
   @brief provide a set of wrapper-functions to our "kt_matrix.h" iot. simplify the applicaiton/speifciaiton of generlaizeed lgocisl operaitons  (oekseth, 06. feb. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. feb. 2017). 
 **/

#include "kt_matrix.h"
#include "kt_randomGenerator_vector.h"


/**
   @brief Initates an object using random integer (uint) values (oekseth, 06. jan. 2017)
   @param <nrows> is the number of rows to intate
   @param <ncols> is the number of columns to intate
   @param <maxVal> is the 'max-value -1' to be be 'used' when itnli<aing the values
   @param <typeOf_randomNess> is the type of randomenss to be used in the idneitifciaotn of random vectors.
   @param <isTo_initIntoIncrementalBlocks> which if set to false re-uses ealrier randomziaotn.
   @rmekars to illsutrate the usage (of this function) note that in our k-means clsutering-rpoceudre the "maxVal" is the "nclusters" argument.
**/
s_kt_matrix_t initAndReturn__randomUint__s_kt_matrix_extendedLogics(const uint nrows, const uint ncols, const uint maxValue, const e_kt_randomGenerator_type_t typeOf_randomNess, const bool isTo_initIntoIncrementalBlocks);

#endif //! EOF
