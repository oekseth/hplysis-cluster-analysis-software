use strict;
use Time::HiRes qw(usleep ualarm gettimeofday tv_interval);
use Carp;

=head
    @brief captures time cost of different optmization strateiges.
=cut

# sub set_compStrategy {
#     my($flags) = @_;
#     return {
# 	flags => $flags,
#     };
# }



sub set_measurement {
    my ($flags_fast, $flags_slow, $case) = @_;
    return {
	flags_fast => $flags_fast,
	flags_slow => $flags_slow,
	case => $case,
    };
}

my @arr_para;
sub add_measurement {
    my ($case, $flags_fast, $flags_slow) = @_;
    push(@arr_para, set_measurement($flags_fast, $flags_slow, $case));
}

sub eval_time {
    my ($resultFile_midFix, $file_main, $arrOf_nrows) = @_;

    #!
    #! Add list of shortest-time-observations:
    my @matrix_slow;     my @matrix_fast;
    my @arr_best; my @arr_worst;
    for(my $case_id = 0; $case_id < scalar(@arr_para); $case_id++) {
	#my $row_ind = 0;
	push(@arr_best,  undef);
	push(@arr_worst, undef);
	my @row_1; my @row_2;
	foreach my $nrows (@{$arrOf_nrows}) {
	    push(@row_1, undef);
	    push(@row_2, undef);
	}
	push(@matrix_slow, \@row_1);
	push(@matrix_slow, \@row_2);
    }
    
    #!
    #! Iterate:     
    for(my $is_slow = 0; $is_slow < 2; $is_slow++) {
	my $result_file = "measure_comp_" . $resultFile_midFix;
	if($is_slow) { $result_file .= "_slow.tsv";}
	else { $result_file .= "_fast.tsv";}
	open(FILE_OUT, ">$result_file") or die("Unable to open the input-file $result_file\n");
	#! Add headeR:
	printf(FILE_OUT "#! row-name:\t%s\t%s\t%s\n", "parameter", "nrows", "duration");	
	#!
	#! 
	for(my $case_id = 0; $case_id < scalar(@arr_para); $case_id++) {
	    # printf("stringOf_depend=\"%s\", at %s:%d\n", $stringOf_depend, __FILE__, __LINE__);
	    #printf("compiles: %s\t at %s:%d\n", $cmd_comp, __FILE__, __LINE__);
	    my $cmd_comp_base = "";
	    if($is_slow) {$cmd_comp_base = $arr_para[$case_id]->{flags_slow};}
	    else         {$cmd_comp_base = $arr_para[$case_id]->{flags_fast};}
	    my $cmd_comp = $cmd_comp_base . " -std=c99 -lm -std=gnu99 -mssse3 $file_main";
	    system($cmd_comp);
	    if(!(-x "a.out")) {
		croak("!!\t Exeuctable Not found when compiling: \"$cmd_comp\"");
	    }
	    # next; # FIXME: remove.
	    #croak("...");		
	    #! Evalute different cases: 
	    my $row_index = 0;
	    #		for(my $case_id = 0; $case_id < scalar(@arr_para); $case_id++) 
	    foreach my $nrows (@{$arrOf_nrows}) {
		{
		    my $str_case = $arr_para[$case_id]->{case};
		    #foreach my $case_id (@{$arrOf_bashParams}) {
		    my $duration = 1000000.0; #! ie, an upper-bound wrt. the time:
		    #my $duration = time - $start;
		    for(my $test_i = 0; $test_i < 5; $test_i++) { #! then apply multiple runs:
			#! 
			# measure elapsed time
			# (could also do by subtracting 2 gettimeofday return values)
			my $t0 = [gettimeofday];
			#my $start = time;
			my $cmd = "./a.out " . $str_case . " " . $nrows . " 2>err.txt";
			#printf("evaluate: %s\t at %s:%d\n", $cmd, __FILE__, __LINE__);
			system($cmd);
			my $duration_curr = tv_interval ($t0, [gettimeofday]);
			if($duration_curr < $duration) {
			    $duration = $duration_curr;
			}
		    }
		    #!
		    #! Write out:
		    printf(STDOUT   "%s\t%s\t%d\t%.2f\n", $cmd_comp_base, $str_case, $nrows, $duration);
		    printf(FILE_OUT "%s\t%s\t%d\t%.2f\n", $cmd_comp_base, $str_case, $nrows, $duration);
		    #! Remember: 
		    if($is_slow) {$matrix_slow[$case_id]->[$row_index] = $duration;}
		    else         {$matrix_fast[$case_id]->[$row_index] = $duration;}
		    #! Set extreme scores:
		    if(!defined($arr_best[$row_index]) || ($duration < $arr_best[$row_index]->{score})) {
			$arr_best[$row_index] = {score => $duration, id => $cmd_comp_base, case => $arr_para[$case_id]->{case}};
		    }
		    if(!defined($arr_worst[$row_index]) || ($duration > $arr_worst[$row_index]->{score})) {
			$arr_worst[$row_index] = {score => $duration, id => $cmd_comp_base, case => $arr_para[$case_id]->{case}};
		    }		    
		    # printf(FILE_OUT "index:%d\t%s\t%s\t%s\t%s\t%s\t%.2f\n", $index_local, $compilerVersion, $genericCompilerStrategy, $fineTuned_compilerArg, $case_id, $nrows, $duration);
		    # printf("index:%d\t%s\t%s\t%s\t%s\t%s\t%.2f\n", $index_local, $compilerVersion, $genericCompilerStrategy, $fineTuned_compilerArg, $case_id, $nrows, $duration);
		    #$index_local++;
		    $row_index++;
		}
	    }
	}
	close(FILE_OUT);
    }
    #! 
    #! Compute the relative scores:
    { #! Case: relative importance of worst--best compilation strategy (seperately for each implemtantion pattern): 
	#! Note: ...
	my $result_file = "measure_comp_" . $resultFile_midFix; $result_file .= "_localBestWorst.tsv";
	printf("CASE: %s\n", $result_file);
	open(FILE_OUT, ">$result_file") or die("Unable to open the input-file $result_file\n");
	#! Add header:
	printf(FILE_OUT "#! row-name:\t%s\t%s\t%s\t%s\n", "compilation(slow)", "compilation(fast)", "nrows", "duration(worst/best)");
	my $row_index = 0;
	foreach my $nrows (@{$arrOf_nrows}) {
	    for(my $case_id = 0; $case_id < scalar(@arr_para); $case_id++) {
		my $str_case = $arr_para[$case_id]->{case};
		my $score = 0;
		if( ($matrix_slow[$case_id]->[$row_index] != 0) && ($matrix_fast[$case_id]->[$row_index] != 0) ) {
		    my $score = $matrix_slow[$case_id]->[$row_index] / $matrix_fast[$case_id]->[$row_index];
		    printf(STDOUT     "%s\t%s\t%s\t%d\t%.2f\n", $str_case, $arr_para[$case_id]->{flags_slow},  $arr_para[$case_id]->{flags_fast}, $nrows, $score);
		    printf(FILE_OUT   "%s\t%s\t%s\t%d\t%.2f\n", $str_case, $arr_para[$case_id]->{flags_slow},  $arr_para[$case_id]->{flags_fast}, $nrows, $score);
		}
	    }
	    $row_index++;
	}
	close(FILE_OUT);	
    }
    { #! Case: relative importance of worst--best compilation strategy, when comapred to the best straetgy:
	#! Note: ...
	my $result_file = "measure_comp_" . $resultFile_midFix; $result_file .= "_globalWorst_localBest.tsv";
	printf("CASE: %s\n", $result_file);
	open(FILE_OUT, ">$result_file") or die("Unable to open the input-file $result_file\n");
	#! Add header:
	printf(FILE_OUT "#! row-name:\t%s\t%s\t%s\t%s\n", "compilation(slow)", "compilation(fast)", "nrows", "duration");
	my $row_index = 0;
	foreach my $nrows (@{$arrOf_nrows}) {
	    for(my $case_id = 0; $case_id < scalar(@arr_para); $case_id++) {
		my $str_case = $arr_para[$case_id]->{case};
		my $score = 0;
		if( ($arr_worst[$row_index] != 0) && ($matrix_fast[$case_id]->[$row_index] != 0) ) {
		    my $score = $arr_worst[$row_index] / $matrix_fast[$case_id]->[$row_index];
		    if(defined($score) 
		       # && !isNaN($score)
			) {
			printf(STDOUT     "%s\t%s\t%s\t%d\t%.2f\n", $str_case, $arr_para[$case_id]->{flags_slow},  $arr_para[$case_id]->{flags_fast}, $nrows, $score);
			printf(FILE_OUT   "%s\t%s\t%s\t%d\t%.2f\n", $str_case, $arr_para[$case_id]->{flags_slow},  $arr_para[$case_id]->{flags_fast}, $nrows, $score);
		    }
		}
	    }
	    $row_index++;
	}
	close(FILE_OUT);	
    }
    { #! How different cases are influenced by matrix dimensions, both itnerally for a given impmetantion pattern, and globally wrt. the relative effect:
	#! Note: [case, compiler, Max relative difference[each], Max-relative-difference[all]
	#! Note: provides insight into the effect of choosing different compilation paramters; ...
	#! Note: ...
	my $result_file = "measure_comp_" . $resultFile_midFix; $result_file .= "_localImprovements_globalLocalCmp.tsv";
	printf("CASE: %s\n", $result_file);
	open(FILE_OUT, ">$result_file") or die("Unable to open the input-file $result_file\n");
	#! Add header:
	printf(FILE_OUT "#! row-name:\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", "compilation(slow)", "compilation(fast)", "local(min)", "local(max)", 
	       "globalMin(min)","globalMin(max)",
	       "globalMax(min)","globalMax(max)",
	    );
	for(my $case_id = 0; $case_id < scalar(@arr_para); $case_id++) {
	    my $str_case = $arr_para[$case_id]->{case};
	    my $row_index = 0;
	    my @score_local  = (undef, undef);
	    #! Below are used to caputre the spread in predictions
	    my @score_globalMin_localMin = (undef, undef); #! ...
	    my @score_globalMax_localMin = (undef, undef); #! ...
	    foreach my $nrows (@{$arrOf_nrows}) {
		if( ($matrix_slow[$case_id]->[$row_index] != 0) && ($matrix_fast[$case_id]->[$row_index] != 0) ) {
		    my $score = $matrix_slow[$case_id]->[$row_index] / $matrix_fast[$case_id]->[$row_index];
		    #! Find exteme min--max wrt. the local difference: 
		    if(!defined($score_local[0]) || ($score_local[0] > $score)) {$score_local[0] = $score;}
		    if(!defined($score_local[1]) || ($score_local[1] < $score)) {$score_local[1] = $score;}
		}
		if( ($arr_best[$row_index] != 0) && ($matrix_fast[$case_id]->[$row_index] != 0) ) {
		    my $score =  $matrix_fast[$case_id]->[$row_index] / $arr_best[$row_index];
		    #! Find exteme min--max wrt. the global minimum: 
		    if(!defined($score_globalMin_localMin[0]) || ($score_globalMin_localMin[0] > $score)) {$score_globalMin_localMin[0] = $score;}
		    if(!defined($score_globalMin_localMin[1]) || ($score_globalMin_localMin[1] < $score)) {$score_globalMin_localMin[1] = $score;}
		}    
		if( ($arr_worst[$row_index] != 0) && ($matrix_fast[$case_id]->[$row_index] != 0) ) {
		    my $score = $arr_worst[$row_index] / $matrix_fast[$case_id]->[$row_index];
		    #! Find exteme min--max wrt. the global maximum:
		    if(!defined($score_globalMax_localMin[0]) || ($score_globalMax_localMin[0] > $score)) {$score_globalMax_localMin[0] = $score;}
		    if(!defined($score_globalMax_localMin[1]) || ($score_globalMax_localMin[1] < $score)) {$score_globalMax_localMin[1] = $score;}
		}
		#! 
		$row_index++;
	    }
	    #!
	    #! Write out: 
	    printf(STDOUT     "%s\t%s\t%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $str_case, $arr_para[$case_id]->{flags_slow},  $arr_para[$case_id]->{flags_fast},
		   $score_local[0], $score_local[1],
		   $score_globalMin_localMin[0], $score_globalMin_localMin[1],
		   $score_globalMax_localMin[0], $score_globalMax_localMin[1]
		);
	    printf(FILE_OUT   "%s\t%s\t%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n", $str_case, $arr_para[$case_id]->{flags_slow},  $arr_para[$case_id]->{flags_fast},
		   $score_local[0], $score_local[1],
		   $score_globalMin_localMin[0], $score_globalMin_localMin[1],
		   $score_globalMax_localMin[0], $score_globalMax_localMin[1]
		);	    
	}
	close(FILE_OUT);		
    }

    { #! Case: For different matrix-sieze the best- and worst relative performance
	#! Note: captures the infleunce of different optmiaiton strategies for increasing matrix size.
	my $result_file = "measure_comp_" . $resultFile_midFix; $result_file .= "_eachDim_extremeAttributes.tsv";
	printf("CASE: %s\n", $result_file);
	open(FILE_OUT, ">$result_file") or die("Unable to open the input-file $result_file\n");
	#! Add header:
	printf(FILE_OUT "#! row-name:\t%s\t%s\t%s\t%s\t%s\n", "time(fast)", "time(worst)", "relativeDiff", "case(fast)", "case(slow)");
	my $row_index = 0;
	foreach my $nrows (@{$arrOf_nrows}) {
	    my $relativeDiff = $arr_worst[$row_index]->{score} / $arr_best[$row_index]->{score} ;
	    printf(STDOUT "%d\t%.2f\t%.2f\t%.2f\t%s\t%s\n", $nrows,
		   $arr_best[$row_index]->{score},
		   $arr_worst[$row_index]->{score},
		   $relativeDiff,
		   $arr_best[$row_index]->{id}  . "--" . $arr_best[$row_index]->{case},
		   $arr_worst[$row_index]->{id} . "--" . $arr_worst[$row_index]->{case}
		);
	    printf(STDOUT "%d\t%.2f\t%.2f\t%.2f\t%s\t%s\n", $nrows,
		   $arr_best[$row_index]->{score},
		   $arr_worst[$row_index]->{score},
		   $relativeDiff,
		   $arr_best[$row_index]->{id}  . "--" . $arr_best[$row_index]->{case},
		   $arr_worst[$row_index]->{id} . "--" . $arr_worst[$row_index]->{case}
		);	    
	    #! 
	    $row_index++;
	}
	close(FILE_OUT);	
    }
}    

#if(1 == 2)
{ #! For each 'matrix-muliplicaiton strategy' set best--worst strategies:
    add_measurement("fast_int_16bit-mult-64", "gcc-8 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "gcc-7 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    add_measurement("fast_int_32bit-mult-64", "g++-7 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "g++-4.7 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    add_measurement("fast_float-mult-64", "g++-8 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "gcc-7 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    #! -------
    add_measurement("fast_int_16bit-mult-64-xmtSSE", "g++-8 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "g++-4.9 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    add_measurement("fast_int_32bit-mult-64-xmtSSE", "g++-7 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "g++-5.5 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    #add_measurement("fast_int_32bit-mult-64-xmtSSE", "g++-8 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "gcc-7 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    add_measurement("fast_float-mult-64-xmtSSE", "g++-8 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "gcc-4.9 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    #! -------
    add_measurement("matrix_naive_int_32bit", "g++-7 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "g++-5.5 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    add_measurement("matrix_naive_float", "g++-8 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "gcc-6 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    #! -------
    # add_measurement("strassen_float", "gcc-6 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "gcc-5.5 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    # add_measurement("strassen_int_32bit", "gcc-6 -O3 -funroll-all-loops -DVECTOR_CONFIG_USE_FLOAT=1", "gcc-4.7 -O2 -DVECTOR_CONFIG_USE_FLOAT=0");
    #! -------
}

#!
#! Apply measurements:
my @arrOf_nrows = ();
for(my $i = 1; $i < 15; $i++) {
    push(@arrOf_nrows, 128 * $i * 2);
    #		push(@arrOf_nrows, 128 * $i * 10);
}	    
eval_time("measureStrassenWorstBest_", "strassen_main.c", \@arrOf_nrows);
