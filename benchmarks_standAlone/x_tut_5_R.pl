use strict;
use Time::HiRes qw(usleep ualarm gettimeofday tv_interval);
use Carp;
# -----------
use FindBin;
use lib $FindBin::Bin;
use File::Basename;
use lib dirname (__FILE__);
# -------------
require module_bm_r;


#my $dim_base_size = 64;
#my $dim_iter = 10;
my $dim_base_size = 896;
my $dim_iter = 1;
my $cnt_call_each = 5;
#!
#! Apply:
module_bm_r::apply($dim_base_size, $dim_iter, $cnt_call_each);
