
//! 
// --------------------------- "1Add":-----------------------------------

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_block_1 ;
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead
#define __MiF__ops_each_block_1 s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead;;
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted, s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted, obj_overhead);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast  dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF__ops_each_block_1 t_float (*ref_func)(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) = __MiC__local; // = __MiC__local;


#define __MiF_opsBlock() ({ref_func(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_static
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_block_1 ;
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_static, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
static t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static
#define __MiF__ops_each_block_1 s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead;;
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
static t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted, s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted, obj_overhead);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast  dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer_static
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer_static, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
static t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF__ops_each_block_1 t_float (*ref_func)(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) = __MiC__local; // = __MiC__local;


#define __MiF_opsBlock() ({ref_func(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_inline
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_block_1 ;
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_inline, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
inline __attribute__((always_inline)) t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline
#define __MiF__ops_each_block_1 s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead;;
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
inline __attribute__((always_inline)) t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted, s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted, obj_overhead);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#if(false)
#define __funcName__fast  dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer_inline
#define __MiF__ops_each scalar_result += (matrix[row_id][j] + matrix[col_id][j]);
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_1Add_funcCalls_sameFile_innerBlock_default_funcPointer_inline, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
// inline __attribute__((always_inline))
t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF__ops_each_block_1 t_float (*ref_func)(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) = __MiC__local; // = __MiC__local;

#define __MiF_opsBlock() ({ref_func(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local
#endif


//! 
// --------------------------- "4Add":-----------------------------------
#define __MiF__ops_inner scalar_result += (matrix[row_id][j] + matrix[col_id][j] + matrix[row_id][j] + matrix[col_id][j] + matrix[col_id][j]);


//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_block_1 ;
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead
#define __MiF__ops_each_block_1 s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead;;
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted, s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted, obj_overhead);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast  dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF__ops_each_block_1 t_float (*ref_func)(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) = __MiC__local; // = __MiC__local;


#define __MiF_opsBlock() ({ref_func(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_static
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_block_1 ;
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_static, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
static t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static
#define __MiF__ops_each_block_1 s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead;;
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_static, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
static t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted, s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted, obj_overhead);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast  dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer_static
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer_static, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
static t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF__ops_each_block_1 t_float (*ref_func)(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) = __MiC__local; // = __MiC__local;


#define __MiF_opsBlock() ({ref_func(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_inline
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_block_1 ;
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_inline, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
inline __attribute__((always_inline)) t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline
#define __MiF__ops_each_block_1 s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead;;
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_blockOverhead_inline, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
inline __attribute__((always_inline)) t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted, s_measure_codeStyle_functionRef_largeOverhead_t obj_overhead) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF_opsBlock() ({__MiC__local(matrix, row_id, col_id, size_adjusted, obj_overhead);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#if(false)
#define __funcName__fast  dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer_inline
#define __MiF__ops_each __MiF__ops_inner
#define __MiF__ops_each_preStep_1 ;
#define __MiF__ops_each_preStep_2 ;
#define __MiC__local __MiF__addSuffixToFuncName(dot_slow_1Add_4Add_funcCalls_sameFile_innerBlock_default_funcPointer_inline, local)
//#define __MiC__local __MiF__addSuffixToFuncName_main(local)
// inline __attribute__((always_inline))
t_float __MiC__local(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) {
  uint j = 0; t_float scalar_result = 0; 
  for(; j < size_adjusted; j++) { 
    __MiF__ops_each_preStep_1; 
    __MiF__ops_each_preStep_2; 
    __MiF__ops_each; /*! ie, apply the logics.*/ 
  } 
  return scalar_result;
}
#define __MiF__ops_each_block_1 t_float (*ref_func)(t_float **matrix, const int row_id, const int col_id, const int size_adjusted) = __MiC__local; // = __MiC__local;

#define __MiF_opsBlock() ({ref_func(matrix, row_id, col_id, size_adjusted);})
#include "ops_matrix_slow_euclid.c"
#undef __MiC__local
#endif


#if(false)
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
#define __funcName__fast 
#define __MiF__ops_each __MiF__ops_inner
#include "ops_matrix_slow_euclid.c"
#endif
//!
//!
//! 
#undef __MiF__ops_inner
