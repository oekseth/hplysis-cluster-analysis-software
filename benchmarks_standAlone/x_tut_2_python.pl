use strict;
#use Time::HiRes qw(usleep ualarm gettimeofday tv_interval);
use Carp;
# -----------
use FindBin;
use lib $FindBin::Bin;
use File::Basename;
use lib dirname (__FILE__);
# -------------
require module_bm_python;
#!
#! Apply:


my $dim_base_size = 64;
my $dim_iter = 10;
my $cnt_call_each = 5;
module_bm_python::apply($dim_base_size, $dim_iter, $cnt_call_each);


