#!/usr/bin/perl 
#use strict;
#use warnings;
use warnings;
use strict;
use POSIX;
use Carp;
#use Time::HiRes qw( gettimeofday tv_interval);

# -----------------------------------
my @arr_func = (
    "all", #! ie, apply all functions
    #! 
    "_3d_multAndPlus_dsList", #! in each of the 3d-ops, one '+' and one '*' is performed; calculations reflects the ones performed in the cityblock-metric.
    "_3d_multAndPlus_dsList_2ifBranchInner", #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; for every artimetic operaiton an if-branch-test is performed.
    "_3d_multAndPlus_dsList_callFunc_onVector_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; the funciton is called for each of the vectors, ie, instead of trhee for-loops, only two for-loops are performed inside teh main-body; for every artimetic operaiton an if-branch-test is performed.
    "_3d_multAndPlus_dsList_callFunc_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; each of the artimetic operaitosn are eprformed in a single funciotn; this reflects the strategy of using external libriares for computing similiarites (ie, a joiint-full strategy); for every artimetic operaiton an if-branch-test is performed.
    );

# -----------------------------------
sub __computePair {
    my($score_1, $score_2) = @_;
    if(defined($score_1) && defined($score_2)) {
	return ($score_1*$score_2);
    }
    return 0;
}
sub __callColFunc_pairFuncCall {
    my($ncols, $matrix, $row_in, $row_out) = @_;
    my $sum = 0;
    for(my $col = 0; $col < $ncols; $col++) {
	my $score_1 = $matrix->[$row_in][$col];
	my $score_2 = $matrix->[$row_out][$col];
	#! Sum:
	$sum += __computePair($score_1, $score_2);
    }
    #! @return
    return $sum;
}

sub __callColFunc {
    my($ncols, $matrix, $row_in, $row_out) = @_;
    my $sum = 0;
    for(my $col = 0; $col < $ncols; $col++) {
	my $score_1 = $matrix->[$row_in][$col];
	my $score_2 = $matrix->[$row_out][$col];
	#! Sum:
	if(defined($score_1) && defined($score_2)) {
	    $sum += ($score_1*$score_2);
	}
    }
    #! @return
    return $sum;
}


sub _3d_multAndPlus_dsList {
    my($nrows, $ncols, $matrix) = @_;
    my $sum = 0.0;
    #! --------------------------
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	#my $sum = 0;
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    for(my $col = 0; $col < $ncols; $col++) {
		my $score_1 = $matrix->[$row_in][$col];
		my $score_2 = $matrix->[$row_out][$col];
		#! Sum:
		$sum += ($score_1*$score_2);
	    }
	}
    }
    #! --------------------------
    return $sum;
}
#! ------------------------------------

sub _3d_multAndPlus_dsList_2ifBranchInner {
    my($nrows, $ncols, $matrix) = @_;
    my $sum = 0.0;
    #! --------------------------
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	#my $sum = 0;
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    for(my $col = 0; $col < $ncols; $col++) {
		my $score_1 = $matrix->[$row_in][$col];
		my $score_2 = $matrix->[$row_out][$col];
		#! Sum:
		if(defined($score_1) && defined($score_2)) {
		    $sum += ($score_1*$score_2);
		}
	    }
	}
    }
    #! --------------------------
    return $sum;
}
#! ------------------------------------

sub _3d_multAndPlus_dsList_callFunc_onVector_2ifBranchInner {
    my($nrows, $ncols, $matrix) = @_;
    my $sum = 0.0;
    #! --------------------------
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    $sum += __callColFunc($ncols, $matrix, $row_in, $row_out);
	}
    }
    #! --------------------------
    return $sum;
}

#! ------------------------------------

sub _3d_multAndPlus_dsList_callFunc_2ifBranchInner {
    my($nrows, $ncols, $matrix) = @_;
    my $sum = 0.0;
    #! --------------------------
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	#my $sum = 0;
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    $sum += __callColFunc_pairFuncCall($ncols, $matrix, $row_in, $row_out);
	}
    }
    #! --------------------------
    return $sum;
}
#! ------------------------------------
#! ------------------------------------
#! ------------------------------------

sub all {
    my($nrows, $ncols, $matrix) = @_;
    printf("Apply all functions:\n");
    my $sum = 0.0;
    my $i = 0;
    foreach my $val (@arr_func) {
	if($val ne "all") { #! ie, not a self-reference:
	    printf("\tAPPLY\t%d)\t%s %s\n",  $i, $val, $nrows);
	    my $action = $val;
	    my $func = __PACKAGE__->can($action);
	    $sum += $func->($nrows, $ncols, $matrix);
	}
	$i++;
    }
    return $sum;
}    

#! ************************************************************************************'
#! ---------------------------
sub compute__simMatrix {
    my($nrows, $ncols, $func) = @_;
    #! Intiate the matrix:
    my @matrix = ();        
    my @row_currMeasurement = ("matrix[" . $nrows . "," . $ncols . "]"); #! ie, the name of the rwo.
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	my @row = ();
	for(my $col = 0; $col < $ncols; $col++) {
	    push(@row, $col);
	}
	push(@matrix,\@row);
    }
    #! 
    #! Make the funciton-call:
    #printf("\tin-func\tfunc-name:\"%s\", at %s:%d\n", $func, __FILE__, __LINE__);	
    #my $score = __PACKAGE__->can($func)->($nrows, $ncols, \@matrix);
    my $score = $func->($nrows, $ncols, \@matrix);
    printf("score=%f\n", $score);
}

sub help {
    my($nrows, $ncols) = @_;
    printf("Name of possible functions to call:\n");
    my $i = 0;
    foreach my $val (@arr_func) {
        printf("\t%d)\t%s %s\n",  $i, $val, $nrows);
	$i++;
    }
}
#! ************************************************************************************'

{ 
    my ($name, $nrows) = @ARGV;
    
    if (not defined $name) {
	die "Need name\n";
    }
    my $action = $name;
    if($name eq "help") {
	__PACKAGE__->can($action)->($nrows, $nrows);
    #} elsif($name eq "apply-all") {
    } else {	
	#printf("func-name:\"%s\", at %s:%d\n", $action, __FILE__, __LINE__);
	#my $function = $action; #__PACKAGE__->can($action);
	my $function = __PACKAGE__->can($action);
	#my $function = \&(__PACKAGE__->can($action));
	#printf("func-name:\"%s\", at %s:%d\n", $action, __FILE__, __LINE__);	
	#!
	#! The call:
	compute__simMatrix($nrows, $nrows, $function);
	#compute__simMatrix($nrows, $nrows, $name);
    }
}
