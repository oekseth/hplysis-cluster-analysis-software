//#include "measure_intri.h"
/* #include <time.h> */
/* #include "types.h" */
/* #include "measure.h" */
#include "measure_base.h"
#include "def_intri.h"
#include "libs.h"

/**
   @file measure_intri
   @brief include a set of performance-evlauations wrt. basic intrisnitistc using Intels SSE (oekseth, 06. juni 2016).
   @author Ole Kristian Ekseth (oekseth)
**/



static float prev_result_ofTimeMEasurement = 0;


static inline __m128i
_mm_bswap_epi16 (__m128i x)
{
  // Swap upper and higher byte in each 16-bit word:
  return _mm_or_si128(
		      _mm_slli_epi16(x, 8),
		      _mm_srli_epi16(x, 8));
}


/**
   @brief Test the overhead assicated to the "_mm_store_ps(..)" funciton.
**/
static void evaluate_store_VS_union_1d_typeARti__add() {
#define arrOf_chunkSizes_size 6
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint base_size = 0.5*kilo * VECTOR_INT_ITER_SIZE_short;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*base_size,      100*base_size,     
							kilo*base_size,    5*kilo*base_size, 
							10*kilo*base_size, 50*kilo*base_size,
							//80*kilo*base_size, 160*kilo*base_size,
							//300*kilo*base_size, 600*kilo*base_size,
  };



  //! *************************************************************************************
  printf("************************************************************************************* # Test 1d-list using add-artimeticss, at %s:%d\n", __FILE__, __LINE__);
  //! -------------------------------------- Test for 'add':
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    //! Allocate list:
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    float relative_tickTime = FLT_MAX;
    const int default_value = 1; 
    int *listOf_values_32Bit_int = allocate_1d_list_int(size_of_array, default_value);
    //memset((void*)listOf_values_32Bit_int, 1, sizeof(listOf_values_32Bit_int)); //size_of_array); //*sizeof(int)); 
    //    memset((void*)listOf_values_32Bit_int, (int)default_value, sizeof(listOf_values_32Bit_int)); //size_of_array); //*sizeof(int)); 
    //    memset((void*)listOf_values_32Bit_int, (int)default_value, size_of_array); //*sizeof(int)); 
    printf("default_value=%d, size_of_array=%d, list[0]=%d, at %s:%d\n", default_value, size_of_array, listOf_values_32Bit_int[0], __FILE__, __LINE__);
    assert(listOf_values_32Bit_int[0] == default_value); 
    assert(listOf_values_32Bit_int[0] == default_value);
    { const char *stringOf_measureText = "Naive sum-of-values wrt. integers";
      //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
      // FIXME[article]: ... seems like integer-code goes faster than the flatoing-point code, ie, as a 'cotnradiction' both to [ªbove] assertion and wr.t time-emasurements in our "mine.c" .... the 'difference' may be explained by .... 

      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      int sumOf_values = 0;
      for(uint i = 0; i < size_of_array; i++) {
	sumOf_values += listOf_values_32Bit_int[i];
	//assert(listOf_values_32Bit_int[i] > 0);
	//assert(sumOf_values > 0);
      }
      assert(sumOf_values > 0);
      //! Complete:
      relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }

    //! -----------------------------------------------------------
    { const char *stringOf_measureText = "SSE sum-of-values wrt. ints 32-bit";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      VECTOR_INT_TYPE vec_result = VECTOR_INT_SET1(0);
      for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	//vec_result = _mm_add_epi32(vec_result, VECTOR_INT_LOAD(&listOf_values_32Bit_int[i]));
      	vec_result = VECTOR_INT_loadAnd_add_vec_memRef(vec_result, &listOf_values_32Bit_int[i]);
      }
      const int sumOf_values = VECTOR_INT_storeAnd_horizontalSum(vec_result);
      assert(sumOf_values > 0);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    free_1d_list_int(&listOf_values_32Bit_int);
    //! -----------------------------------------------------------
    { const char *stringOf_measureText = "SSE sum-of-values wrt. ints 16-bit";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Note(2): the result of the operaiton will be misleading, ie, as we are summing over more elements than the maximum number fo bitesin a "short int". In brief we 'apply' our approach in order to test/evlaute the effects of 'byte-consentration', ie, to compare the signficanc e of floating-point-opeaitons vS integer-operaitons.
      short int *listOf_values_32Bit_int_short = allocate_1d_list_int_short(size_of_array, default_value);
      //! Start tiem-emasuremetns:
      start_time_measurement();
      
      VECTOR_INT_TYPE_short vec_result = VECTOR_INT_SET1_short(0);
      for(uint i = 0; i < size_of_array; i += VECTOR_INT_ITER_SIZE_short) {
      	//vec_result = _mm_add_epi32(vec_result, VECTOR_INT_LOAD(&listOf_values_32Bit_int[i]));
      	vec_result = VECTOR_INT_loadAnd_add_vec_memRef_short(vec_result, &listOf_values_32Bit_int_short[i]);
	// assert(listOf_values_32Bit_int_short[i] > 0);
      }
      const short int sumOf_values = VECTOR_INT_storeAnd_horizontalSum_short(vec_result);
      // assert(sumOf_values > 0); //! <-- an assertion which is not expected to hold, ie, as the expected sum of size_of_array elements is expected to be greater than 'fits' into 16 bits.
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      //! De-allocate locally reserved data:
      free_1d_list_int_short(&listOf_values_32Bit_int_short);
    }
    { const char *stringOf_measureText = "SSE div-of-values wrt. ints 16-bit";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Note(2): the result of the operaiton will be misleading, ie, as we are summing over more elements than the maximum number fo bitesin a "short int". In brief we 'apply' our approach in order to test/evlaute the effects of 'byte-consentration', ie, to compare the signficanc e of floating-point-opeaitons vS integer-operaitons.
      short int *listOf_values_32Bit_int_short = allocate_1d_list_int_short(size_of_array, default_value);
      //! Start tiem-emasuremetns:
      start_time_measurement();
      
      VECTOR_INT_TYPE_short vec_result = VECTOR_INT_SET1_short(1000); //! ie, to aovid 'by zero' for every case.
      for(uint i = 0; i < size_of_array; i += VECTOR_INT_ITER_SIZE_short) {
      	//vec_result = _mm_add_epi32(vec_result, VECTOR_INT_LOAD(&listOf_values_32Bit_int[i]));
      	vec_result = VECTOR_INT_DIV_short_notInverseResult(vec_result, VECTOR_INT_LOAD_short(&listOf_values_32Bit_int_short[i]));
	// assert(listOf_values_32Bit_int_short[i] > 0);
      }
      const short int sumOf_values = VECTOR_INT_storeAnd_horizontalSum_short(vec_result);
      // assert(sumOf_values > 0); //! <-- an assertion which is not expected to hold, ie, as the expected sum of size_of_array elements is expected to be greater than 'fits' into 16 bits.
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      //! De-allocate locally reserved data:
      free_1d_list_int_short(&listOf_values_32Bit_int_short);
    }


    float *listOf_values_float = allocate_1d_list_float(size_of_array, /*default-value=*/1);
    //! --------------------------------------
    { const char *stringOf_measureText = "Naive sum-of-values wrt. floats";
      //! Note: we exepct this to go slightly faster than the 'int' appraoch:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      float sumOf_values = 0;
      for(uint i = 0; i < size_of_array; i++) {
	sumOf_values += listOf_values_float[i];
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE sum-of-values wrt. floats";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
      for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_result, &listOf_values_float[i]);
      }
      float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { //! Test the possbility of accessing different 'parts' of the foor-loop, ie, 'through' the inpdencce-of-lements-poerpty wrt. our 'adding' (oekseth, 06. sept. 2016):
      { const char *stringOf_measureText = "SSE sum-of-values wrt. floats: two operations each loop, ie, test parallelism: two-operations-in-each-loop-part";
	const uint size_of_array_half = 0.5*size_of_array;
	//! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	const uint step_size = VECTOR_FLOAT_ITER_SIZE * 2;
	for(uint i = 0; i < size_of_array; i += step_size) {
	  vec_result = VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_result, &listOf_values_float[i]);
	  vec_result = VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_result, &listOf_values_float[i+ VECTOR_FLOAT_ITER_SIZE]); //! ie, at 'the other end'
	}
	float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      { const char *stringOf_measureText = "SSE sum-of-values wrt. floats: two operations each loop, ie, test parallelism: each part";
	const uint size_of_array_half = 0.5*size_of_array;
	//! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	for(uint i = 0; i < size_of_array_half; i += VECTOR_FLOAT_ITER_SIZE) {
	  vec_result = VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_result, &listOf_values_float[i]);
	  vec_result = VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_result, &listOf_values_float[i+size_of_array_half]); //! ie, at 'the other end'
	}
	float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
    }
    //! --------------------------------------
    free_1d_list_float(&listOf_values_float);
  }
}

/**
   @brief Test the overhead assicated to the "_mm_store_ps(..)" funciton.
**/
static void evaluate_store_VS_union_1d_typeARti__mult() {
  #undef arrOf_chunkSizes_size
#define arrOf_chunkSizes_size 6
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint base_size = 0.5*kilo * VECTOR_INT_ITER_SIZE_short;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*base_size,      100*base_size,     
							kilo*base_size,    5*kilo*base_size, 
							10*kilo*base_size, 50*kilo*base_size,
							/* 80*kilo*base_size, 160*kilo*base_size, */
							/* 300*kilo*base_size, 600*kilo*base_size, */
  };

  //! *************************************************************************************
  printf("************************************************************************************* # Test 1d-list using muliplication-artimeticss, at %s:%d\n", __FILE__, __LINE__);
  //! -------------------------------------- Test for 'multiplication':
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    //! Allocate list:
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    float relative_tickTime = FLT_MAX;
    { const char *stringOf_measureText = "Naive (mult-or-div)-of-values wrt. integers";
      //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
      // FIXME[article]: ... seems like integer-code goes faster than the flatoing-point code, ie, as a 'cotnradiction' both to [ªbove] assertion and wr.t time-emasurements in our "mine.c" .... the 'difference' may be explained by .... 
      int *listOf_values_32Bit_int = allocate_1d_list_int(size_of_array, /*default-value=*/1);
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      int sumOf_values = 0;
      for(uint i = 0; i < size_of_array; i++) {
	sumOf_values *= listOf_values_32Bit_int[i];
      }
      //! Complete:
      relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      free_1d_list_int(&listOf_values_32Bit_int);
    }
    float *listOf_values_float = allocate_1d_list_float(size_of_array, /*default-value=*/1);
    //! --------------------------------------
    { const char *stringOf_measureText = "Naive mult-of-values wrt. floats";
      //! Note: we exepct this to go slightly faster than the 'int' appraoch:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      float sumOf_values = 0;
      for(uint i = 0; i < size_of_array; i++) {
	sumOf_values *= listOf_values_float[i];
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "Naive div-of-values wrt. floats-50-per-cent-divideByZero";
      //! Note: we exepct this to go slightly faster than the 'int' appraoch:
      const float div_factor_errors = 0.5;
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      float sumOf_values = 0;
      uint i = 0; const uint size_of_array_part1 = (uint)((float)size_of_array * div_factor_errors);
      for(; i < size_of_array_part1; i++) {
	sumOf_values /= listOf_values_float[i];
      }
      sumOf_values = 100;
      for(; i < size_of_array; i++) {
	sumOf_values /= listOf_values_float[i];
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "Naive div-of-values wrt. floats-0-per-cent-divideByZero";
      //! Note: we exepct this to go slightly faster than the 'int' appraoch:
      const float div_factor_errors = 0.5;
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      float sumOf_values = 0;
      uint i = 0; // const uint size_of_array_part1 = (uint)((float)size_of_array * div_factor_errors);
      /* for(; i < size_of_array_part1; i++) { */
      /* 	sumOf_values /= listOf_values_float[i]; */
      /* } */
      sumOf_values = 100;
      for(; i < size_of_array; i++) {
	sumOf_values /= listOf_values_float[i];
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "Naive mul-of-values wrt. floats-0-per-cent-divideByZero";
      //! Note: we exepct this to go slightly faster than the 'int' appraoch:
      const float div_factor_errors = 0.5;
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      float sumOf_values = 0;
      uint i = 0; // const uint size_of_array_part1 = (uint)((float)size_of_array * div_factor_errors);
      // for(; i < size_of_array_part1; i++) {
      // 	sumOf_values /= listOf_values_float[i];
      // }
      sumOf_values = 100;
      for(; i < size_of_array; i++) {
	sumOf_values *= listOf_values_float[i];
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE mult-of-values wrt. floats";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
      for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_loadAnd_mult_vec_memRef(vec_result, &listOf_values_float[i]);
      }
      float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalMult(vec_result);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE div-of-values wrt. floats-0-per-cent-divideByZero";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      const float div_factor_errors = 0.5;
      //! Start tiem-emasuremetns:
      start_time_measurement();
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
      uint i = 0; // const uint size_of_array_part1 = (uint)((float)size_of_array * div_factor_errors);
      // for(; i < size_of_array_part1; i += VECTOR_FLOAT_ITER_SIZE) {
      // 	vec_result = VECTOR_FLOAT_loadAnd_mult_vec_memRef(vec_result, &listOf_values_float[i]);
      // }
      vec_result = VECTOR_FLOAT_SET1(T_FLOAT_MAX);
      for(; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_DIV(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[i]));
      }
      for(; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_DIV(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[i]));
      }
      float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalMult(vec_result);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
      printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE div-of-values wrt. floats-50-per-cent-divideByZero";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      const float div_factor_errors = 0.5;
      //! Start tiem-emasuremetns:
      start_time_measurement();
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
      uint i = 0; const uint size_of_array_part1 = (uint)((float)size_of_array * div_factor_errors);
      for(; i < size_of_array_part1; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_loadAnd_mult_vec_memRef(vec_result, &listOf_values_float[i]);
      }
      vec_result = VECTOR_FLOAT_SET1(T_FLOAT_MAX);
      for(; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_loadAnd_mult_vec_memRef(vec_result, &listOf_values_float[i]);
      }
      for(; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_loadAnd_mult_vec_memRef(vec_result, &listOf_values_float[i]);
      }
      float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalMult(vec_result);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------
    { const char *stringOf_measureText = "SSE mul-of-values wrt. floats-0-per-cent-mulideByZero";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      const float mul_factor_errors = 0.5;
      //! Start tiem-emasuremetns:
      start_time_measurement();
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
      uint i = 0; // const uint size_of_array_part1 = (uint)((float)size_of_array * mul_factor_errors);
      // for(; i < size_of_array_part1; i += VECTOR_FLOAT_ITER_SIZE) {
      // 	vec_result = VECTOR_FLOAT_loadAnd_mult_vec_memRef(vec_result, &listOf_values_float[i]);
      // }
      vec_result = VECTOR_FLOAT_SET1(T_FLOAT_MAX);
      for(; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_MUL(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[i]));
      }
      for(; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
      	vec_result = VECTOR_FLOAT_MUL(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[i]));
      }
      float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalMult(vec_result);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE mul-of-values wrt. floats-50-per-cent-mulideByZero";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      const float mul_factor_errors = 0.5;
      //! Start tiem-emasuremetns:
      start_time_measurement();
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
      uint i = 0; const uint size_of_array_part1 = (uint)((float)size_of_array * mul_factor_errors);
      for(; i < size_of_array_part1; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_MUL(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[i]));
      }
      vec_result = VECTOR_FLOAT_SET1(T_FLOAT_MAX);
      for(; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_MUL(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[i]));
      }
      // for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
      // 	vec_result = VECTOR_FLOAT_DIV(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[i]));
      // }
      float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalMult(vec_result);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    free_1d_list_float(&listOf_values_float);
  }

}

//! Extend "evaluate_store_VS_union_1d(..)" wrt. "_max", "_min" and 'realtive' add-functions.
void evaluate_store_VS_union_2d() {
  #undef arrOf_chunkSizes_size
#define arrOf_chunkSizes_size 6
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint sizeOf_innerBlocks = 64; //! ie, to be a muliple of "2" ... and which 'allows' the use of 'char' to comptue max-valeus for.
  const uint base_size = 0.5*sizeOf_innerBlocks * 20 * VECTOR_INT_ITER_SIZE_short;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {10*base_size,      100*base_size,     
							kilo*base_size,    5*kilo*base_size, 
							10*kilo*base_size, 40*kilo*base_size,
							//80*kilo*base_size, 160*kilo*base_size,
							//300*kilo*base_size, 600*kilo*base_size,
  };

  
  //! --------------------------------------
  printf("************************************************************************************* # Test 1d-list wrt. horizontal sums, at %s:%d\n", __FILE__, __LINE__);
  //! ------------------------
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    //! Allocate list:
    printf("----------\n# \t list-size=%u, at %s:%d\n", size_of_array, __FILE__, __LINE__);
    float relative_tickTime = FLT_MAX;
    int *listOf_values_32Bit_int = allocate_1d_list_int(size_of_array, /*default-value=*/1);
    { const char *stringOf_measureText = "Naive sum-of-values wrt. integers";
      //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	int sumOf_inner = 0;
	for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
	  sumOf_inner += listOf_values_32Bit_int[m];
	}
	sumOf_values += sumOf_inner;
      }
      //! Complete:
      relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    const short int default_value = 5;
    short int *listOf_values_32Bit_int_short = allocate_1d_list_int_short(size_of_array, default_value);
    { const char *stringOf_measureText = "Naive sum-of-values wrt. integers-short";
      //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	int sumOf_inner = 0;
	for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
	  sumOf_inner += listOf_values_32Bit_int_short[m];
	}
	sumOf_values += sumOf_inner;
      }
      //! Complete:
      relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! New type: 
    //! --------------------------------------
    { const char *stringOf_measureText = "Naive maxsum-of-values wrt. integers-short";
      //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
    	int sumOf_inner = 0;
    	for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
    	  if(sumOf_inner < listOf_values_32Bit_int_short[i]) {sumOf_inner = listOf_values_32Bit_int_short[m];}
    	}
    	sumOf_values += sumOf_inner;
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    float *listOf_values_float = allocate_1d_list_float(size_of_array, /*default-value=*/1);
    //! New type: 
    //! --------------------------------------
    { const char *stringOf_measureText = "Naive sum-of-values wrt. floats";
      //! Note: we exepct this to go slightly faster than the 'int' appraoch:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
    	float sumOf_inner = 0;
    	for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
    	  sumOf_inner += listOf_values_float[m];
    	}
    	sumOf_values += sumOf_inner;
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "Naive maxsum-of-values wrt. floats";
      //! Note: we exepct this to go slightly faster than the 'int' appraoch:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! The 'operation':
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
    	float sumOf_inner = 0;
    	for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
    	  if(sumOf_inner < listOf_values_float[m]) {sumOf_inner = listOf_values_float[m];}
    	}
    	sumOf_values += sumOf_inner;
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE sum-of-values wrt. ints: use loadu(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_INT_TYPE vec_result = VECTOR_INT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; m += VECTOR_INT_ITER_SIZE, i+= VECTOR_INT_ITER_SIZE) {
	  vec_result = VECTOR_INT_ADD(vec_result, VECTOR_INT_LOADU(&listOf_values_32Bit_int[m]));
    	  // vec_result = VECTOR_INT_loadAnd_add_vec_memRef(vec_result, &listOf_values_32Bit_int[i]);
    	}
    	sumOf_values += VECTOR_INT_storeAnd_horizontalSum(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE sum-of-values wrt. ints: use load(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
    	VECTOR_INT_TYPE vec_result = VECTOR_INT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; m += VECTOR_INT_ITER_SIZE, i+= VECTOR_INT_ITER_SIZE) {
    	  vec_result = VECTOR_INT_ADD(vec_result, VECTOR_INT_LOAD(&listOf_values_32Bit_int[m]));
    	  // vec_result = VECTOR_INT_loadAnd_add_vec_memRef(vec_result, &listOf_values_32Bit_int[i]);
    	}
    	sumOf_values += VECTOR_INT_storeAnd_horizontalSum(vec_result);
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE maxsum-of-values wrt. ints: use loadu(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_INT_TYPE vec_result = VECTOR_INT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_INT_ITER_SIZE, m += VECTOR_INT_ITER_SIZE) {
    	  vec_result = VECTOR_INT_MAX(vec_result, VECTOR_INT_LOADU(&listOf_values_32Bit_int[m]));
    	}
    	sumOf_values += VECTOR_INT_storeAnd_horizontalMax(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE maxsum-of-values wrt. ints: use load(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
    	VECTOR_INT_TYPE vec_result = VECTOR_INT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_INT_ITER_SIZE, m += VECTOR_INT_ITER_SIZE) {
    	  vec_result = VECTOR_INT_MAX(vec_result, VECTOR_INT_LOAD(&listOf_values_32Bit_int[m]));
    	}
    	sumOf_values += VECTOR_INT_storeAnd_horizontalMax(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! New type: 
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE sum-of-values wrt. ints-short";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_INT_TYPE_short vec_result = VECTOR_INT_SET1_short(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_INT_ITER_SIZE_short, m += VECTOR_INT_ITER_SIZE_short) {
    	  vec_result = VECTOR_INT_ADD(vec_result, VECTOR_INT_LOADU(&listOf_values_32Bit_int_short[m]));
    	}
    	sumOf_values += VECTOR_INT_storeAnd_horizontalSum_short(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE maxsum-of-values wrt. ints-short: use loadu(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      short int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_INT_TYPE_short vec_result = VECTOR_INT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_INT_ITER_SIZE_short, m += VECTOR_INT_ITER_SIZE_short) {
    	  vec_result = VECTOR_INT_MAX_short(vec_result, VECTOR_INT_LOADU_short(&listOf_values_32Bit_int_short[m]));
    	}
    	sumOf_values += VECTOR_INT_storeAnd_horizontalMax_short(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE maxsum-of-values wrt. ints-short: use load(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      short int sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_INT_TYPE_short vec_result = VECTOR_INT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_INT_ITER_SIZE_short, m += VECTOR_INT_ITER_SIZE_short) {
    	  vec_result = VECTOR_INT_MAX_short(vec_result, VECTOR_INT_LOAD_short(&listOf_values_32Bit_int_short[m]));
    	}
    	sumOf_values += VECTOR_INT_storeAnd_horizontalMax_short(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    // //! New type: 
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE sum-of-values wrt. floats: use loadu(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; m += VECTOR_FLOAT_ITER_SIZE, i+= VECTOR_FLOAT_ITER_SIZE) {
    	  vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOADU(&listOf_values_float[m]));
    	}
    	sumOf_values += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE sum-of-values wrt. floats: use load(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; m += VECTOR_FLOAT_ITER_SIZE, i+= VECTOR_FLOAT_ITER_SIZE) {
    	  vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[m]));
    	}
    	sumOf_values += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE max-sum-of-values wrt. floats: use loadu(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_FLOAT_ITER_SIZE, m += VECTOR_FLOAT_ITER_SIZE) {
    	  vec_result = VECTOR_FLOAT_MAX(vec_result, VECTOR_FLOAT_LOADU(&listOf_values_float[m]));
    	}
    	sumOf_values += VECTOR_FLOAT_storeAnd_horizontalMax(vec_result);
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE max-sum-of-values wrt. floats: use load(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_FLOAT_ITER_SIZE, m += VECTOR_FLOAT_ITER_SIZE) {
    	  vec_result = VECTOR_FLOAT_MAX(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[m]));
    	}
    	sumOf_values += VECTOR_FLOAT_storeAnd_horizontalMax(vec_result);
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE sum-of-values wrt. floats-alt1: use loadu(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_FLOAT_ITER_SIZE, m += VECTOR_FLOAT_ITER_SIZE) {
    	  vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOADU(&listOf_values_float[m]));
    	}
    	sumOf_values += VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE sum-of-values wrt. floats-alt1: use load(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_FLOAT_ITER_SIZE, m += VECTOR_FLOAT_ITER_SIZE) {
    	  vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[m]));
    	}
    	sumOf_values += VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec_result);
      }

      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    { const char *stringOf_measureText = "SSE max-sum-of-values wrt. floats-alt1: use loadu(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_FLOAT_ITER_SIZE, m += VECTOR_FLOAT_ITER_SIZE) {
    	  vec_result = VECTOR_FLOAT_MAX(vec_result, VECTOR_FLOAT_LOADU(&listOf_values_float[m]));
    	}
    	sumOf_values += VECTOR_FLOAT_storeAnd_horizontalMax_alt(vec_result);
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "SSE max-sum-of-values wrt. floats-alt1: use load(..)";
      //! Note: test the 'impact' of intrisnticts wrt. usage of the "_mm_store_ps(..)" intel SSE function:
      //! Start tiem-emasuremetns:
      start_time_measurement();
      float sumOf_values = 0;
      for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {      
    	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
    	for(uint m = 0; m < sizeOf_innerBlocks; i += VECTOR_FLOAT_ITER_SIZE, m += VECTOR_FLOAT_ITER_SIZE) {
    	  vec_result = VECTOR_FLOAT_MAX(vec_result, VECTOR_FLOAT_LOAD(&listOf_values_float[m]));
    	}
    	sumOf_values += VECTOR_FLOAT_storeAnd_horizontalMax_alt(vec_result);
      }
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
    }
    //! --------------------------------------
    //! De-allocate locally reserved data:
    free_1d_list_float(&listOf_values_float);
    free_1d_list_int(&listOf_values_32Bit_int);
    free_1d_list_int_short(&listOf_values_32Bit_int_short);
  }







  // assert(false); // FIXEM: in our "mine.c" ... update all oruc alls to "VECTOR_FLOAT_SET1(..)" ... sett eh latter to 'the start-value' ... and then iterate ... ie,to reduce the number of artimetic operaitons ... and compare the tiem-effect.



}


//! Evlaute the cost of using SSE3 wrt. 'char-masks'
//! Note[result]: we observe that the 'optmal SSE implemetnaiotn'r resutls in a noticable/signficant slowdown, ie, wrt. applicaiotn/use of char-masks.
// FIXME[jc]: may we use 'above descirbed result (based on [”elow] funciton) as a 'engative result' ... eg, to illustrat ethe challegnes in uinsng SSE of life-science software-optmizaiton?
static void evaluate_mask_char_oneVector() {
  #undef arrOf_chunkSizes_size
#define arrOf_chunkSizes_size 1
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint sizeOf_innerBlocks = 64; //! ie, to be a muliple of "2" ... and which 'allows' the use of 'char' to comptue max-valeus for.
  const uint base_size = 0.5*sizeOf_innerBlocks * 20 * VECTOR_FLOAT_ITER_SIZE;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {
    10 *kilo * base_size
    // 10*base_size,      100*base_size,     
    // kilo*base_size,    5*kilo*base_size, 
    // 10*kilo*base_size, 40*kilo*base_size,
							//80*kilo*base_size, 160*kilo*base_size,
							//300*kilo*base_size, 600*kilo*base_size,
  };

  
  //! --------------------------------------
  printf("************************************************************************************* # Test 1d-list wrt. horizontal sums, at %s:%d\n", __FILE__, __LINE__);
  //! ------------------------
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    //! Allocate list:
    const t_float default_value_float = 1;     const t_float default_value_char = 1;
    t_float *listOf_values_float  = allocate_1d_list_float(size_of_array, default_value_float);
    char *listOf_values_char_in  = allocate_1d_list_char(size_of_array, default_value_char);
    char *listOf_values_char_out = allocate_1d_list_char(size_of_array, default_value_char);
    //! Iteraitvly specify the number of 'zero-values' wrt. the 'inner':
    const uint arrOf_fraction_size = 5;
    t_float arrOf_fraction[arrOf_fraction_size];
    arrOf_fraction[0] = .05;
    arrOf_fraction[1] = .1;
    arrOf_fraction[2] = .25;
    arrOf_fraction[3] = .5;
    arrOf_fraction[4] = .75;
    for(uint frac_in = 0; frac_in < arrOf_fraction_size; frac_in++) {
      { //! Update the 'inner' value-ranges:
	const uint frac_threshold = (uint)(1.0 / arrOf_fraction[frac_in]);
	assert(frac_threshold > 0);
	uint curr_pos = 0;
	for(uint k = 0; k < size_of_array; k++) {
	  if(curr_pos < frac_threshold) {
	    listOf_values_char_in[k] = 1;;
	    curr_pos++;
	  } else {
	    listOf_values_char_in[k] = 0.0;
	    curr_pos = 0; //! ie, reset.
	  }
	}
      }
      //! Iterate throught eh 'outer' frac-thresholds:
      for(uint frac_out = 0; frac_out < arrOf_fraction_size; frac_out++) {
	{ //! Update the 'outer' value-ranges:
	  const uint frac_threshold = (uint)(1.0 / arrOf_fraction[frac_in]);
	  assert(frac_threshold > 0);
	  uint curr_pos = 0;
	  for(uint k = 0; k < size_of_array; k++) {
	    if(curr_pos < frac_threshold) {
	      listOf_values_char_out[k] = 1;	      
	      curr_pos++;
	    } else {
	      listOf_values_char_out[k] = 0.0;
	      curr_pos = 0; //! ie, reset.
	    }
	  }
	}
	

	printf("----------\n# \t list-size=%u, given (frac-in, frac-out)=(%f, %f), at %s:%d\n", size_of_array, arrOf_fraction[frac_in], arrOf_fraction[frac_out], __FILE__, __LINE__);
	float relative_tickTime = FLT_MAX;
	const t_float default_value_float = 1;
	
	{ const char *stringOf_measureText = "Naive char-mask";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  float sumOf_values = 0;
	  for(uint i = 0; i < size_of_array; i++) {
	  //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    float sumOf_inner = 0;
	    //for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
	    if( (listOf_values_char_in[i] != 0) && (listOf_values_char_out[i] != 0) ) {
	      sumOf_inner += listOf_values_float[i];
	    }
		//}
	    sumOf_values += sumOf_inner;
	  }
	  //! Complete:
	  relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{ const char *stringOf_measureText = "Naive char-mask xmt evaluation";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  float sumOf_values = 0;
	  for(uint i = 0; i < size_of_array; i++) {
	  //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    float sumOf_inner = 0;
	    //for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
	    sumOf_inner += listOf_values_float[i];
		//}
	    sumOf_values += sumOf_inner;
	  }
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}

	{
	  const char *stringOf_measureText = "SSE-char-mask alt-1";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_resultIsInverted(VECTOR_FLOAT_LOAD(&listOf_values_float[i]), &listOf_values_char_in[i], &listOf_values_char_out[i]);
	    vec_result = VECTOR_FLOAT_ADD(vec_result, vec_tmp);
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-char-mask alt-1 w/overhead=revertOrder";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(VECTOR_FLOAT_LOAD(&listOf_values_float[i]), &listOf_values_char_in[i], &listOf_values_char_out[i]);
	    vec_result = VECTOR_FLOAT_ADD(vec_result, vec_tmp);
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-char-mask alt-2 w/overhead=revertOrder";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    VECTOR_FLOAT_TYPE vec_tmp = VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_old(VECTOR_FLOAT_LOAD(&listOf_values_float[i]), &listOf_values_char_in[i], &listOf_values_char_out[i]);
	    vec_result = VECTOR_FLOAT_ADD(vec_result, vec_tmp);
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
      }
    }
    free_1d_list_float(&listOf_values_float);
    free_1d_list_char(&listOf_values_char_in);
    free_1d_list_char(&listOf_values_char_out);
  }
}

//! Evlaute the cost of using SSE3 wrt. dvision-by-zero cases:
//! Note[®esult]: from below time-meausrements we observe that there is no perofmranc-imrepovemen if masks needs to be 'appliued' for every 'case', ie, the cost of the if-stencnes is outweighted by the increase cost of the SSE-overhead. <-- teh latter assertion need to be udpated ... ie, as we for some cases 'get' a perofmrance-icnrease of 1.1x
// FIXME[jc]: may we use 'above descirbed result (based on [”elow] funciton) as a 'engative result' ... eg, to illustrat ethe challegnes in uinsng SSE of life-science software-optmizaiton?
static void evaluate_mask_divByZero() {
  #undef arrOf_chunkSizes_size
#define arrOf_chunkSizes_size 1 
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint sizeOf_innerBlocks = 64; //! ie, to be a muliple of "2" ... and which 'allows' the use of 'char' to comptue max-valeus for.
  const uint base_size = 0.5*sizeOf_innerBlocks * 20 * VECTOR_FLOAT_ITER_SIZE;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {
    10 *kilo * base_size
    // 10*base_size,      100*base_size,     
    // kilo*base_size,    5*kilo*base_size, 
    // 10*kilo*base_size, 40*kilo*base_size,
							//80*kilo*base_size, 160*kilo*base_size,
							//300*kilo*base_size, 600*kilo*base_size,
  };

  
  //! --------------------------------------
  printf("************************************************************************************* # Test 1d-list wrt. horizontal sums, at %s:%d\n", __FILE__, __LINE__);
  //! ------------------------
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    //! Allocate list:
    const t_float default_value_float = 1;
    t_float *listOf_values_float_in  = allocate_1d_list_float(size_of_array, default_value_float);
    t_float *listOf_values_float_out = allocate_1d_list_float(size_of_array, default_value_float);
    //! Iteraitvly specify the number of 'zero-values' wrt. the 'inner':
    const uint arrOf_fraction_size = 5;
     t_float arrOf_fraction[arrOf_fraction_size]; //    = {0.05, 0.1, .25, .05, .075};
    uint pos = 0; 
    arrOf_fraction[pos++] = 0.05;
    arrOf_fraction[pos++] = 0.1;
    arrOf_fraction[pos++] = 0.25;
    arrOf_fraction[pos++] = 0.5;
    arrOf_fraction[pos++] = 0.75;
    for(uint frac_in = 0; frac_in < arrOf_fraction_size; frac_in++) {
      { //! Update the 'inner' value-ranges:
	const uint frac_threshold = (uint)(1.0 / arrOf_fraction[frac_in]);
	assert(frac_threshold > 0);
	uint curr_pos = 0;
	for(uint k = 0; k < size_of_array; k++) {
	  if(curr_pos < frac_threshold) {
	    listOf_values_float_in[k] = (t_float)k;
	    curr_pos++;
	  } else {
	    listOf_values_float_in[k] = 0.0;
	    curr_pos = 0; //! ie, reset.
	  }
	}
      }
      //! Iterate throught eh 'outer' frac-thresholds:
      for(uint frac_out = 0; frac_out < arrOf_fraction_size; frac_out++) {
	{ //! Update the 'outer' value-ranges:
	  const uint frac_threshold = (uint)(1.0 / arrOf_fraction[frac_in]);
	  assert(frac_threshold > 0);
	  uint curr_pos = 0;
	  for(uint k = 0; k < size_of_array; k++) {
	    if(curr_pos < frac_threshold) {
	      listOf_values_float_out[k] = (t_float)k;
	      curr_pos++;
	    } else {
	      listOf_values_float_out[k] = 0.0;
	      curr_pos = 0; //! ie, reset.
	    }
	  }
	}
	

	printf("----------\n# \t list-size=%u, given (frac-in, frac-out)=(%f, %f), at %s:%d\n", size_of_array, arrOf_fraction[frac_in], arrOf_fraction[frac_out], __FILE__, __LINE__);
	float relative_tickTime = FLT_MAX;
	const t_float default_value_float = 1;
	
	{ const char *stringOf_measureText = "Naive divsion";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  float sumOf_values = 0;
	  for(uint i = 0; i < size_of_array; i++) {
	  //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    float sumOf_inner = 0;
	    //for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
	    if( (listOf_values_float_in[i] != 0) && (listOf_values_float_out[i] != 0) ) {
	      sumOf_inner += listOf_values_float_in[i] / listOf_values_float_out[i];
	    }
		//}
	    sumOf_values += sumOf_inner;
	  }
	  //! Complete:
	  relative_tickTime = __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{ const char *stringOf_measureText = "Naive divsion-xmt-nanValues";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  float sumOf_values = 0;
	  for(uint i = 0; i < size_of_array; i++) {
	  //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    float sumOf_inner = 0;
	    //for(uint m = 0; m < sizeOf_innerBlocks; i++, m++) {
	    sumOf_inner += listOf_values_float_in[i] / listOf_values_float_out[i];
		//}
	    sumOf_values += sumOf_inner;
	  }
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division: never-0; load(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_DIV(VECTOR_FLOAT_LOAD(&listOf_values_float_in[i]), VECTOR_FLOAT_LOAD(&listOf_values_float_out[i])));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division: never-zero; loadu(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_DIV(VECTOR_FLOAT_LOADU(&listOf_values_float_in[i]), VECTOR_FLOAT_LOADU(&listOf_values_float_out[i])));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division: load(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_omitZeros_fromComputation_DIV(VECTOR_FLOAT_LOAD(&listOf_values_float_in[i]), VECTOR_FLOAT_LOAD(&listOf_values_float_out[i])));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division(slow): loadu(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_omitZeros_fromComputation_DIV_old_2(VECTOR_FLOAT_LOADU(&listOf_values_float_in[i]), VECTOR_FLOAT_LOADU(&listOf_values_float_out[i])));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	 
 	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division(alt-macro:slow): loadu(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(VECTOR_FLOAT_LOADU(&listOf_values_float_in[i]), VECTOR_FLOAT_LOADU(&listOf_values_float_out[i]), VECTOR_FLOAT_DIV));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division(alt-macro:slow-v1): loadu(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_1(VECTOR_FLOAT_LOADU(&listOf_values_float_in[i]), VECTOR_FLOAT_LOADU(&listOf_values_float_out[i]), VECTOR_FLOAT_DIV));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division(alt-macro:slow-v2): loadu(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_2(VECTOR_FLOAT_LOADU(&listOf_values_float_in[i]), VECTOR_FLOAT_LOADU(&listOf_values_float_out[i]), VECTOR_FLOAT_DIV));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division: loadu(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_omitZeros_fromComputation_DIV(VECTOR_FLOAT_LOADU(&listOf_values_float_in[i]), VECTOR_FLOAT_LOADU(&listOf_values_float_out[i])));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "naive-division-subset: divide-by-zero (and not: divide-on-zero)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  float sumOf_values = 0;
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    if(listOf_values_float_out[i] != 0) {
	      sumOf_values += listOf_values_float_in[i] / listOf_values_float_out[i];
	    }
	  }
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{  const char *stringOf_measureText = "SSE-division-subset: divide-by-zero (and not: divide-on-zero): loadu(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(VECTOR_FLOAT_DIV(VECTOR_FLOAT_LOADU(&listOf_values_float_in[i]), VECTOR_FLOAT_LOADU(&listOf_values_float_out[i]))));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division-subset: divide-by-zero (and not: divide-on-zero): load(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(VECTOR_FLOAT_DIV(VECTOR_FLOAT_LOAD(&listOf_values_float_in[i]), VECTOR_FLOAT_LOAD(&listOf_values_float_out[i]))));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	//! ---------------------------------------
	{  const char *stringOf_measureText = "SSE-division-subset: divide-on-zero: loadu(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_dataMask_replaceINF_by_Zero(VECTOR_FLOAT_DIV(VECTOR_FLOAT_LOADU(&listOf_values_float_in[i]), VECTOR_FLOAT_LOADU(&listOf_values_float_out[i]))));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
	{
	  const char *stringOf_measureText = "SSE-division-subset: divide-on-zero: load(..)";
	  //! Note: givent ehicnreased comptue-hardware-efforts wrt. floating-point-numbers we expect 'sums fo floats' to 'go/behave' faster hta 'sums of ints', ie, 'this test' is expected to results in lower/slower performance:
	  //! Start tiem-emasuremetns:
	  start_time_measurement();
	  //! The 'operation':
	  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0); //! ei, itnaliszes.
	  const uint size_of_array_intri = size_of_array - (uint)VECTOR_FLOAT_ITER_SIZE;
	  for(uint i = 0; i < size_of_array_intri; i += VECTOR_FLOAT_ITER_SIZE) {
	    //for(uint i = 0; i < (size_of_array - sizeOf_innerBlocks); i++) {
	    vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_dataMask_replaceINF_by_Zero(VECTOR_FLOAT_DIV(VECTOR_FLOAT_LOAD(&listOf_values_float_in[i]), VECTOR_FLOAT_LOAD(&listOf_values_float_out[i]))));
	  }
	  const float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  //! Complete:
	  __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, relative_tickTime);	  
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
	}
      }
    }
    free_1d_list_float(&listOf_values_float_in);
    free_1d_list_float(&listOf_values_float_out);
  }
}



//! Evlaute the cost of using SSE3 wrt. 'char-masks'
//! Note[result]: we observe that the 'optmal SSE implemetnaiotn'r resutls in a noticable/signficant slowdown, ie, wrt. applicaiotn/use of char-masks.
// FIXME[jc]: may we use 'above descirbed result (based on [”elow] funciton) as a 'engative result' ... eg, to illustrat ethe challegnes in uinsng SSE of life-science software-optmizaiton?
static void evaluate_mask_char_twoVectors__explicit() {
  #undef arrOf_chunkSizes_size
#define arrOf_chunkSizes_size 1
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint sizeOf_innerBlocks = 64; //! ie, to be a muliple of "2" ... and which 'allows' the use of 'char' to comptue max-valeus for.
  const uint base_size = 0.5*sizeOf_innerBlocks * 20 * VECTOR_FLOAT_ITER_SIZE;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {
    10 *kilo * base_size
    // 10*base_size,      100*base_size,     
    // kilo*base_size,    5*kilo*base_size, 
    // 10*kilo*base_size, 40*kilo*base_size,
							//80*kilo*base_size, 160*kilo*base_size,
							//300*kilo*base_size, 600*kilo*base_size,
  };

  
  //! --------------------------------------
  printf("************************************************************************************* # Test 1d-list wrt. horizontal sums, at %s:%d\n", __FILE__, __LINE__);
  //! ------------------------
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    //! Allocate list:
    const t_float default_value_float = 1;     const t_float default_value_char = 1;
    t_float *listOf_values_float_1  = allocate_1d_list_float(size_of_array, default_value_float);
    t_float *listOf_values_float_2  = allocate_1d_list_float(size_of_array, default_value_float);
    char *listOf_values_char_in  = allocate_1d_list_char(size_of_array, default_value_char);
    char *listOf_values_char_out = allocate_1d_list_char(size_of_array, default_value_char);

    { const char *stringOf_measureText = "naive-mask::explicit:";
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! -----------
      t_float sum = 0;
      const t_float * buffer1 = listOf_values_float_1;       const t_float * buffer2 = listOf_values_float_2;
      const char *mask1 = listOf_values_char_in;  const char *mask2 = listOf_values_char_out;
      for(uint i = 0; i < size_of_array; i += 1) {
	if(mask1[i] && mask2[i]) {if(buffer1[i] && buffer2[i]) { sum++; } }
      }
      assert(sum != 0);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sum, __FILE__, __LINE__);
    }
    { const char *stringOf_measureText = "naive-mask::explicit and use restrict key-word";
      //! Start tiem-emasuremetns:
      start_time_measurement();
      //! -----------
      t_float sum = 0;
      const t_float *__restrict__ buffer1 = listOf_values_float_1;
      const t_float *__restrict__ buffer2 = listOf_values_float_2;
      const char *__restrict__ mask1 = listOf_values_char_in;
      const char *__restrict__ mask2 = listOf_values_char_out;
      //! ---
      for(uint i = 0; i < size_of_array; i += 1) {
	if(mask1[i] && mask2[i]) {if(buffer1[i] && buffer2[i]) { sum++; } }
      }
      assert(sum != 0);
      //! Complete:
      __assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sum, __FILE__, __LINE__);
    }
    { //! Mask-count and 'set(..)'
      { //! A 'case' wherew we evlaaute the correctness wrt. our 'slow explcit amsk-implemntaiton':
	const char *stringOf_measureText = "SSE-get-mask-count::explicit(slow): set-no-match to zero where set(..) is used instead of load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_SET(buffer1[0+i], buffer1[1+i], buffer1[2+i], buffer1[3+i]);
	  VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_SET(buffer2[0+i], buffer2[1+i], buffer2[2+i], buffer2[3+i]);     
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_mask_combined);
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      {    /*! Case: an explicit maks (with valeus set to eitehr "0" or "1"): include the count of itneresting vertices/valeus  */
	const char *stringOf_measureText = "SSE-get-mask-count::explicit(faster): set-no-match to zero, where set(..) is used instead of load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_SET(buffer1[0+i], buffer1[1+i], buffer1[2+i], buffer1[3+i]);
	  VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_SET(buffer2[0+i], buffer2[1+i], buffer2[2+i], buffer2[3+i]);     
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &mask1[i], &mask2[i]);
	  //VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_maskCount);
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
    }    
    //! --------
    { //! Mask-count and 'loadu(..)'
      { //! A 'case' wherew we evlaaute the correctness wrt. our 'slow explcit amsk-implemntaiton':
	const char *stringOf_measureText = "SSE-get-mask-count::explicit(slow): loadu(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOADU(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOADU(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_mask_combined);
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      {    /*! Case: an explicit maks (with valeus set to eitehr "0" or "1"): include the count of itneresting vertices/valeus  */
	const char *stringOf_measureText = "SSE-get-mask-count::explicit(faster): set-no-match to zero: loadu(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOADU(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOADU(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &mask1[i], &mask2[i]);
	  //VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_maskCount);
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
    }    
    //! ---
    { //! Mask-count and 'load(..)'
      { //! A 'case' wherew we evlaaute the correctness wrt. our 'slow explcit amsk-implemntaiton':
	const char *stringOf_measureText = "SSE-get-mask-count::explicit(slow): load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_mask_combined);
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      {    /*! Case: an explicit maks (with valeus set to eitehr "0" or "1"): include the count of itneresting vertices/valeus  */
	const char *stringOf_measureText = "SSE-get-mask-count::explicit(faster): set-no-match to zero: load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &mask1[i], &mask2[i]);
	  //VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_maskCount);
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
    }    
    //! ---



    {   /*! Case: an explicit maks (with valeus set to eitehr "0" or "1")  */
      {	const char *stringOf_measureText = "SSE-only-mask-update::explicit(fast): set-no-match to zero: load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, mask1, mask2);
	  //VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &mask1[i], &mask2[i]);
	  //VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      //! ----------
      {	const char *stringOf_measureText = "SSE-only-mask-update::explicit(fast): set-no-match to zero: loadu(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOADU(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOADU(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, mask1, mask2);
	  //VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &mask1[i], &mask2[i]);
	  //VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
    }
    //! ------
    { //! SSE-macro-versions which we wrote before a stepp SSE-learinign-cruve:
      {	const char *stringOf_measureText = "SSE-only-mask-update::explicit(slow): set-no-match to zero: load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_localSum = VECTOR_FLOAT_ADD(vec_input_1, vec_input_2);
	  VECTOR_FLOAT_charMask_pair(vec_localSum, mask1, mask2, i);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_localSum); //VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      {	const char *stringOf_measureText = "SSE-only-mask-update::explicit(slower): set-no-match to zero: load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_localSum = VECTOR_FLOAT_ADD(vec_input_1, vec_input_2);
	  VECTOR_FLOAT_charMask_pair_old(vec_localSum, mask1, mask2, i);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_localSum); //VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      {	const char *stringOf_measureText = "SSE-only-mask-update::explicit(slower.v2): set-no-match to zero: load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_localSum = VECTOR_FLOAT_ADD(vec_input_1, vec_input_2);
	  VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(vec_localSum, &mask1[i], &mask2[i]);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_localSum); //VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      {	const char *stringOf_measureText = "SSE-only-mask-update::explicit(slower.v3): set-no-match to zero: load(..)";
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_localSum = VECTOR_FLOAT_ADD(vec_input_1, vec_input_2);
	VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_old(vec_localSum, &mask1[i], &mask2[i]);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_localSum); //VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }

    }

    //!
    //! De-allocate:
    free_1d_list_float(&listOf_values_float_1);
    free_1d_list_float(&listOf_values_float_2);
    free_1d_list_char(&listOf_values_char_in);
    free_1d_list_char(&listOf_values_char_out);
  }
}

static void evaluate_mask_char_twoVectors__implicit() {
  #undef arrOf_chunkSizes_size
#define arrOf_chunkSizes_size 1
  //! Note: we use teh VECTOR_FLOAT_ITER_SIZE to simplify writing our our code-examples.
  const uint sizeOf_innerBlocks = 64; //! ie, to be a muliple of "2" ... and which 'allows' the use of 'char' to comptue max-valeus for.
  const uint base_size = 0.5*sizeOf_innerBlocks * 20 * VECTOR_FLOAT_ITER_SIZE;
  const uint arrOf_chunkSizes[arrOf_chunkSizes_size] = {
    10 *kilo * base_size
    // 10*base_size,      100*base_size,     
    // kilo*base_size,    5*kilo*base_size, 
    // 10*kilo*base_size, 40*kilo*base_size,
							//80*kilo*base_size, 160*kilo*base_size,
							//300*kilo*base_size, 600*kilo*base_size,
  };

  
  //! --------------------------------------
  printf("************************************************************************************* # Test 1d-list wrt. horizontal sums, at %s:%d\n", __FILE__, __LINE__);
  //! ------------------------
  for(uint chunk_index = 0; chunk_index < arrOf_chunkSizes_size; chunk_index++) {
    const uint size_of_array = arrOf_chunkSizes[chunk_index];
    //! Allocate list:
    const t_float default_value_float = 1;     const t_float default_value_char = 1;
    t_float *listOf_values_float_1  = allocate_1d_list_float(size_of_array, default_value_float);
    t_float *listOf_values_float_2  = allocate_1d_list_float(size_of_array, default_value_float);
    char *listOf_values_char_in  = allocate_1d_list_char(size_of_array, default_value_char);
    char *listOf_values_char_out = allocate_1d_list_char(size_of_array, default_value_char);

    { //! Test the application/use of implciit masks:
      {	const char *stringOf_measureText = "naive-get-mask-count:implicit(fast)";
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	t_float sum = 0;
	const t_float * buffer1 = listOf_values_float_1;       const t_float * buffer2 = listOf_values_float_2;
	const char *mask1 = listOf_values_char_in;  const char *mask2 = listOf_values_char_out;
	for(uint i = 0; i < size_of_array; i += 1) {
	  if(isOf_interest(buffer1[i]) && isOf_interest(buffer2[i])) {if(buffer1[i] && buffer2[i]) { sum++; } }
	}
	assert(sum != 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sum, __FILE__, __LINE__);
      }
      { const char *stringOf_measureText = "naive-mask::implicit and use restrict key-word";
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	t_float sum = 0;
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	for(uint i = 0; i < size_of_array; i += 1) {
	  if(isOf_interest(buffer1[i]) && isOf_interest(buffer2[i])) {if(buffer1[i] && buffer2[i]) { sum++; } }
	}
	assert(sum != 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sum, __FILE__, __LINE__);
      }

      {	const char *stringOf_measureText = "SSE-get-mask-count:implicit(fast): loadu(..)";
	/*! Case: an implicit maks (with valeus set to T_FLOAT_MAX) and where we are interested in 'getting' the 'vertex-count' */
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOADU(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOADU(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCount(vec_input_1, vec_input_2);
	  //VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, mask1, mask2);
	  //VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &mask1[i], &mask2[i]);
	  //VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_maskCount); // VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
      {	const char *stringOf_measureText = "SSE-get-mask-count:implicit(fast): load(..)";
	/*! Case: an implicit maks (with valeus set to T_FLOAT_MAX) and where we are interested in 'getting' the 'vertex-count' */
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCount(vec_input_1, vec_input_2);
	  //VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, mask1, mask2);
	  //VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &mask1[i], &mask2[i]);
	  //VECTOR_FLOAT_TYPE vec_mask_combined;  VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask1[i]), VECTOR_FLOAT_convertFrom_CHAR_buffer(&mask2[i]), vec_mask_combined);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, vec_maskCount); // VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }

      {	const char *stringOf_measureText = "SSE-update-mask:implicit(fast): load(..)";
	/*! Case: an implciti maks (with valeus set to T_FLOAT_MAX)  */
	//! ------------------------------ start: input:
	//! Start tiem-emasuremetns:
	start_time_measurement();
	//! -----------
	const t_float *__restrict__ buffer1 = listOf_values_float_1;
	const t_float *__restrict__ buffer2 = listOf_values_float_2;
	const char *__restrict__ mask1 = listOf_values_char_in;
	const char *__restrict__ mask2 = listOf_values_char_out;
	//! ---
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);
	//! ---
	for(uint i = 0; i < size_of_array; i += VECTOR_FLOAT_ITER_SIZE) {
	  //const t_float buffer2[VECTOR_FLOAT_ITER_SIZE] = {-6, 2, 5, 3};
	  VECTOR_FLOAT_TYPE vec_input_1 = VECTOR_FLOAT_LOAD(&buffer1[i]); VECTOR_FLOAT_TYPE vec_input_2 = VECTOR_FLOAT_LOAD(&buffer2[i]);
	  // VECTOR_FLOAT_PRINT_wMessage("vec_input_1=", vec_input_1);       VECTOR_FLOAT_PRINT_wMessage("vec_input_2=", vec_input_2);
	  //! ------------------------------ end: input.
	  VECTOR_FLOAT_dataMask_implicit_updatePair(vec_input_1, vec_input_2);
	  vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_ADD(vec_input_1, vec_input_2));
	}
	const t_float sumOf_values = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	assert(sumOf_values > 0);
	//! Complete:
	__assertClass_generateResultsOf_timeMeasurements(stringOf_measureText, size_of_array, prev_result_ofTimeMEasurement);
	printf("sumOf_values=%f, at %s:%d\n", (t_float)sumOf_values, __FILE__, __LINE__);
      }
    }

    //!
    //! De-allocate:
    free_1d_list_float(&listOf_values_float_1);
    free_1d_list_float(&listOf_values_float_2);
    free_1d_list_char(&listOf_values_char_in);
    free_1d_list_char(&listOf_values_char_out);
  }
}

// TODO: consider to write a erpforamcne-dfif-test wrt. ... "_mm_stream_ps" operand ... and "It is a good idea to call _mm_mfence after you finished with _mm_stream_ps. This is needed for correctness, not for performance." ("http://stackoverflow.com/questions/9068246/why-does-mm-stream-ps-produce-l1-ll-cache-misses") <-- consider to ask frist JC to evlauate/assess the difference btwheen 'these'.

//! The main assert function.
void main(const int array_cnt, char **array) {
  uint cnt_Tests_evalauted = 0;
  const bool isTo_processAll = (array_cnt == 2); //! ie, as we for 'this case' assumes that only the exec-name and the measure-id is set in the tmerinal-user-bash-call, eg fo r





  //! Evlaute the cost of using SSE3 wrt. 'char-masks'
  if(isTo_processAll || (0 == strncmp("mask-explicit-oneVector", array[2], strlen("mash-explicit-oneVector"))) ) {
    evaluate_mask_char_oneVector();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("mask-explicit", array[2], strlen("mask-explicit"))) ) {
    //! Evlaute the cost of using SSE3 wrt. 'char-masks'
    evaluate_mask_char_twoVectors__explicit();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("mask-implicit", array[2], strlen("mask-implicit"))) ) {
    //! Evlaute the cost of using SSE3 wrt. 'char-masks'
    evaluate_mask_char_twoVectors__implicit();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("mask", array[2], strlen("mask"))) ) {
    //! Evlaute the cost of using SSE3 wrt. 'char-masks'
    evaluate_mask_char_twoVectors__explicit();
    evaluate_mask_char_twoVectors__implicit();
    cnt_Tests_evalauted++;
  }

  //! Evlaute the cost of using SSE3 wrt. dvision-by-zero cases:
  if(isTo_processAll || (0 == strncmp("divide-by-zero", array[2], strlen("divide-by-zero"))) ) {
    evaluate_mask_divByZero();
    cnt_Tests_evalauted++;
  }

  //! Extend "evaluate_store_VS_union_1d(..)" wrt. "_max", "_min" and 'realtive' add-functions.
  if(isTo_processAll || (0 == strncmp("min-max-dataTypes", array[2], strlen("min-max-dataTypes"))) ) {
    evaluate_store_VS_union_2d();
    cnt_Tests_evalauted++;
  }

  if(isTo_processAll || (0 == strncmp("simple-aritmetics-add", array[2], strlen("simple-aritmetics-add"))) ) {
    //! Test the overhead assicated to the "_mm_store_ps(..)" funciton.
    evaluate_store_VS_union_1d_typeARti__add();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("simple-aritmetics-multiply", array[2], strlen("simple-aritmetics-multiply"))) ) {
    //! Test the overhead assicated to the "_mm_store_ps(..)" funciton.
    evaluate_store_VS_union_1d_typeARti__mult();
    cnt_Tests_evalauted++;
  } else if(isTo_processAll || (0 == strncmp("simple-aritmetics", array[2], strlen("simple-aritmetics"))) ) {
    //! Then we 'glue' both into the rsame result-chunk:
    evaluate_store_VS_union_1d_typeARti__add();
    evaluate_store_VS_union_1d_typeARti__mult();
    cnt_Tests_evalauted++;
  }
  



  if(cnt_Tests_evalauted == 0) {
    fprintf(stderr, "!!\t No tests were evaluated, ie, please validate that your termnal-bash-inpout-parameters conforms to the input-configuraitons: for quesitons please cotnact the developer at [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
  }
}
