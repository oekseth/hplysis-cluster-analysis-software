  
  int array_sizes[6] = {32,64,128,256,512,1024};

  int m;
  ofstream results; //output the results
  results.open ("results.txt");

  for(int i = 0; i < 6; i++){
    for (int k = 0; k < 3; k ++){

      clock_t start = clock();
      m = array_sizes[i];

      int **A = new int*[m];
      int **B = new int*[m];
      int **product = new int*[m];

      for ( int i = 0 ; i < m ; i++)
	{
	  A[i] = new int[m];
	  B[i] = new int[m];
	  product[i] = new int[m];
	}

      // Initialize A[I][K]
      // I x K == row * inner
      for(int row = 0; row < m; row++) {
	for(int inner = 0; inner < m; inner++){
	  A[row][inner] = 5;
	}
      }


      for(int row = 0; row < m; row++) {
	for(int inner = 0; inner < m; inner++){
	  B[row][inner] = 3;
	  //cout << B[row][inner] << " ";
	}
	//cout << "\n";
      }
      if(true) {
	compute_regular(m, A, B, product);
      } else {
	strassen (A,B,product,m);
      }

      //Deallocate
      for ( int i = 0 ; i < m ; i++)
	{
	  delete[] A[i];
	  delete[] B[i];
	  delete[] product[i];
	}
      delete[] A;
      delete[] B;
      delete[] product;


      clock_t end = clock();
      double cpu_time = static_cast<double>(end - start)/CLOCKS_PER_SEC;

      fprintf(stderr, "time[%d][%d]=%f\n", m, m, cpu_time);
      results << cpu_time << endl;
    }
    results << m << endl;
  }

  results.close();
