#!/usr/bin/perl
package module_bm_java;
use Carp;
use strict;
use Time::HiRes qw(usleep ualarm gettimeofday tv_interval);

sub apply {
    my ($dim_base_size, $dim_iter, $cnt_call_each) = @_;
    #!
    #!
    #! Note: belwo cofnigurations are fectehd from "x_measure_scriptLanguages.pl".
    #!
    #!
    #!
    #! Note: path to different java-versiosn.
    my $prefix = "/usr/lib/jvm/";
    my $suffix = "bin/";
    my @arr_compiler = (
	#! Note: to install: sudo apt install openjdk-11-jdk openjdk-8-jdk
	$prefix . "java-11-openjdk-amd64/" . $suffix,
	$prefix . "java-8-openjdk-amd64/"  . $suffix,	
	);
    #!
    #! Note: when calling each java-verison, this implisa a redireation to the absolute path, eg, /usr/lib/jvm/java-11-openjdk-amd64/bin/java and /usr/lib/jvm/java-11-openjdk-amd64/bin/javac
    #!
    #! compile: javac tut__altLAng_simMetric.java
    #! Run: java -classpath . altLAng_simMetric
    my $file_name  = "tut__altLAng_simMetric.java";
    my $class_name = "altLAng_simMetric";
    my @arr_func = (
	"_3d_multAndPlus_dsList",  #! in each of the 3d-ops, one '+' and one '*' is performed; calculations reflects the ones performed in the cityblock-metric.
	"_3d_mult",  #! data is not accssed; two '+' operaitons are performed
	"_3d_multAndPlus_dsList_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; for every artimetic operaiton an if-branch-test is performed.
	"_3d_multAndPlus_dsList_callFunc_onVector_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; the funciton is called for each of the vectors, ie, instead of trhee for-loops, only two for-loops are performed inside teh main-body; for every artimetic operaiton an if-branch-test is performed.
	"_3d_multAndPlus_dsList_callFunc_2ifBranchInner", #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; each of the artimetic operaitosn are eprformed in a single funciotn; this reflects the strategy of using external libriares for computing similiarites (ie, a joiint-full strategy); for every artimetic operaiton an if-branch-test is performed.
	);
    #!
    #!
    #! -----------------------
    my $result_file_min = "res-x_tut_3_java_min.tsv";
    my $result_file_max = "res-x_tut_3_java_max.tsv";
    open(FILE_OUT_MIN, ">$result_file_min") or die("Unable to open the input-file $result_file_min\n");
    open(FILE_OUT_MAX, ">$result_file_max") or die("Unable to open the input-file $result_file_max\n");
    #!
    #! Add header:
    printf(FILE_OUT_MIN "#! Compiler \t Function \t Dimension\t Time\n");
    printf(FILE_OUT_MAX "#! Compiler \t Function \t Dimension\t Time\n");
    #--------------
    foreach my $comp (@arr_compiler) {
	my $cmd = $comp . 'javac ' . $file_name;
	printf("CMD\t " . $cmd . "\n ");
	system($cmd);
	
	foreach my $fun (@arr_func) {
	    for(my $i = 1; $i <= $dim_iter; $i++) {
		my $nrows = $dim_base_size * $i; #! eg, 64, 128, ...
		#! ---------
		my $t_min = undef;
		my $t_max = undef;
		for(my $iter = 0; $iter < $cnt_call_each; $iter++) {
		    my $t0 = [gettimeofday];
		    my $cmd = "time " . $comp . "java -classpath . $class_name $fun $nrows"; #! eg, java tut_main_matrixBM.py _3d_mult 1000
		    #my $cmd = "time $comp $file_name $fun $nrows"; #! eg, java tut_main_matrixBM.py _3d_mult 1000
		    printf("CMD\t " . $cmd . "\n ");
		    system($cmd);
		    my $duration_curr = tv_interval ($t0, [gettimeofday]);
		    if(!defined($t_min) || ($t_min > $duration_curr) ) {
			$t_min = $duration_curr;
		    }
		    if(!defined($t_max) || ($t_max < $duration_curr) ) {
			$t_max = $duration_curr;
		    }
		    #croak("...");
		}
		#!
		#! Update result-files:
		printf(FILE_OUT_MIN "%s\t%s\t%u\t%.2f\n", $comp, $fun, $nrows, $t_min);
		printf(FILE_OUT_MAX "%s\t%s\t%u\t%.2f\n", $comp, $fun, $nrows, $t_max);
	    }
	}
    }
    #!
    #! CLOSE:
    close(FILE_OUT_MIN);
    close(FILE_OUT_MAX);
}


#! ============================= EOF:
1;
