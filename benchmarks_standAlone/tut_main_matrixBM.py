from __future__ import print_function
"""
@remarks a permtuation of our "tut__altLAng_sciPy__listAccess.py"
@remarks depdencneis (possible an overkill):
-- sudo apt-get install python-scikits-learn python-sklearn python3-sklearn python-numpy python-numpy-dbg python3-numpy python3-numpy-dbg
"""
import argparse #! https://docs.python.org/3.3/library/argparse.html
#
import numpy as np
import time
from sklearn.metrics.pairwise import manhattan_distances
from sklearn.metrics import euclidean_distances #! which we use for simliarty-metrics
#from sklearn.metrics import cosine_similarity
from sklearn.metrics.pairwise import paired_distances
from scipy.spatial.distance import cosine, cityblock, minkowski, wminkowski
from sklearn.metrics.pairwise import paired_distances
from sklearn.metrics.pairwise import pairwise_distances
import sys

#! A genelrized funciton to symplify the 'dump' of values: 
def eprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    print(*args, file=sys.stderr, **kwargs)

#! -----------------------------------------------------    Functions supporting sim-emtric copmtuation:
#!
#! 
"""
        - from scikit-learn: ['cityblock', 'cosine', 'euclidean', 'l1', 'l2',
          'manhattan']

        - from scipy.spatial.distance: ['braycurtis', 'canberra', 'chebyshev',
          'correlation', 'dice', 'hamming', 'jaccard', 'kulsinski',
          'mahalanobis', 'matching', 'minkowski', 'rogerstanimoto',
          'russellrao', 'seuclidean', 'sokalmichener', 'sokalsneath',
          'sqeuclidean', 'yule']
"""
exec_time_pyIter_sim__sciPySpatial__cosine = []
exec_time_pyIter_sim__sciPySpatial__minkowski = []

currentPos = 0

def __getFor_pair(score1, score2):
    if( (score1 != None) and (score2 != None) ): #! then a 'mask' is Not set.
        return (score1 * score2)
    return 0

def __compute_row_seperateMetricCall(ncols, row_1, row_2):
    sum = 0
    for i in xrange(ncols):
        sum += __getFor_pair(row_1[i], row_2[i])
    return sum

def __compute_row(ncols, row_1, row_2):
    sum = 0
    for i in xrange(ncols):
        if( (row_1[i] != None) and (row_2[i] != None) ): #! then a 'mask' is Not set.
            sum += (row_1[i] * row_2[i])
    return sum
#! --------------------------------------


def _3d_plus_dsList(nrows, ncols):
    #a = time.clock() 
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    sum = 0.0
    #print "len(A)=", len(A_pyStruct)
    #print "len(A[0])=", len(A_pyStruct[0])
    for k in xrange(nrows):
        for l in xrange(nrows):
            for i in xrange(ncols):
                sum = sum + A_pyStruct[l][i] + B_pyStruct[k][i]
                b = time.clock()
    # exec_time_pyIter_sum_plus.append( b - a )
    print("sum=%.1f" % (sum));
            #; #
# ------------
def _3d_multAndPlus_dsList(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    sum = 0.0;
    for k in xrange(nrows):
        for l in xrange(nrows):
            for i in xrange(ncols):
                sum += (A_pyStruct[l][i] * B_pyStruct[k][i])
            #b = time.clock()
            #exec_time_pyIter_sum_mul.append( b - a )
    print("sum=%.1f" % (sum));
# ------------
def _3d_mult(nrows, ncols):
    sum = 0.0;
    for k in xrange(nrows):
        for l in xrange(nrows):
            for i in xrange(ncols):
                sum += (k * l)
    print("sum=%.1f" % (sum));
# ------------
def _3d_plus_dsList_2ifBranchInner(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    sum = 0.0;
    for k in xrange(nrows):
        for l in xrange(nrows):
            for i in xrange(ncols):
                if( (A_pyStruct[k][i] != None) and (B_pyStruct[l][i] != None) ): #! then a 'mask' is Not set.
                    sum += (A_pyStruct[k][i] + B_pyStruct[l][i])
    print("sum=%.1f" % (sum));
# ------------
def _3d_multAndPlus_dsList_2ifBranchInner(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    sum = 0.0;
    for k in xrange(nrows):
        for l in xrange(nrows):
            for i in xrange(ncols):
                if( (A_pyStruct[l][i] != None) and (B_pyStruct[k][i] != None) ): #! then a 'mask' is Not set.
                    sum += (A_pyStruct[l][i] * B_pyStruct[k][i])
    print("sum=%.1f" % (sum));
# ------------
def _3d_multAndPlus_dsList_callFunc_onVector_2ifBranchInner(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    sum = 0.0;
    for k in xrange(nrows):
        for l in xrange(nrows):
            sum += __compute_row(ncols, A_pyStruct[k], B_pyStruct[l])
    print("sum=%.1f" % (sum));
# ------------
def _3d_multAndPlus_dsList_callFunc_2ifBranchInner(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    sum = 0.0;
    for k in xrange(nrows):
        for l in xrange(nrows):
            sum += __compute_row_seperateMetricCall(ncols, A_pyStruct[k], B_pyStruct[l])
    print("sum=%.1f" % (sum));
# ------------
def _skLearn_euclidean_distances(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    # FIXME: ...     
    sum = 1.0; #euclidean_distances(A_pyStruct, B_pyStruct)
    print("sum=%.1f" % (sum));
# ------------
def _skLearn_manhattan_distances(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    m = manhattan_distances(A_pyStruct, B_pyStruct);
    sum = 0.0;
    #print(m);
    #print(type(m));
    for r in range(m.shape[0]):
        for c in range(m.shape[1]):
            #for r in range(10):
            sum += m[r][c];
            #print( m[r][c]);
    #m2 = np.asmatrix(m)
    #for r in m:
    #print type(r);
    #sum = np.asscalar(m);
    #m = manhattan_distances(A_pyStruct, B_pyStruct, sum_over_features=False);
    # print m;
    # sum = 1.0;
    # for r in m:        
    #     for c in r:                
    #         #print("..." + c);
    #         sum += float(c);
    #print r;
    #print("..." + row);
    #print type(row);
    print("sum=%.1s" % str(sum));
        #print("sum=%.1s" % (sum));
# ------------
def _skLearn_paired_cityblock(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    sum =  0.0
    m = paired_distances(A_pyStruct, B_pyStruct, metric="cityblock", n_jobs=1)
    #print(m);
    for r in range(m.shape[0]):
        #for c in range(m.shape[1]):
        #for r in range(10):
        sum += m[r];
        #sum += m[r][c];
            #print( m[r][c]);    
    print("sum=%.1f" % (sum));
# ------------
def _skLearn_paired_cosine(nrows, ncols):
    #! ----
    #! Construct sample-data:
    A = np.random.rand(nrows, ncols)
    B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
    A_pyStruct = A.tolist()
    B_pyStruct = B.tolist()
    #! ----
    #    sum = pairwise_distances(A_pyStruct, B_pyStruct, metric="cosine", n_jobs=1) #! ie, "scipy.spatial"
    #    print("sum=%.1f" % (sum));
    #def _skLearn_paired_(nrows, ncols):
    sum = 0.0;
    m = pairwise_distances(A_pyStruct, B_pyStruct, metric=cosine) #! ie, "scipy.spatial"    print(m);
    #print(m);
    for r in range(m.shape[0]):
        for c in range(m.shape[1]):
            #for r in range(10):
            sum += m[r][c];
            #print( m[r][c]);    
    print("sum=%.1f" % (sum));
# ------------
# def _skLearn_metric_minkowski(nrows, ncols):
# #def _skLearn_metric_minkowski(nrows, ncols, kwds):
#     #! ----
#     #! Construct sample-data:
#     A = np.random.rand(nrows, ncols)
#     B = A # np.random.randint(0,255,(sizeOfArray,sizeOfArray_out))
#     A_pyStruct = A.tolist()
#     B_pyStruct = B.tolist()
#     #! ----
#     sum = 0.0
#     kwds = {}
#     m = pairwise_distances(A_pyStruct, B_pyStruct, metric=minkowski); #, **kwds) #! ie, "scipy.spatial"
#     for r in range(m.shape[0]):
#         for c in range(m.shape[1]):
#             #for r in range(10):
#             sum += m[r][c];
#             print( m[r][c]);
#     print("sum=%.1f" % (sum));
# ------------

    
arr_funcs = [
    "all", #! ie, then apply all functions.
    #! ------------
    "_3d_plus_dsList", #! ie, three for-loops: in each of the 3d-ops, two '+' operaitons are performed, where a dataStructure=list is accessed
    "_3d_multAndPlus_dsList", #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric
    "_3d_mult", #! data is not accssed; two '+' operaitons are performed
    "_3d_plus_dsList_2ifBranchInner", #! ie, three for-loops: in each of the 3d-ops, two '+' operaitons are performed, where a dataStructure=list is accessed; for every artimetic operaiton an if-branch-test is performed.
    "_3d_multAndPlus_dsList_2ifBranchInner", #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; for every artimetic operaiton an if-branch-test is performed.
    "_3d_multAndPlus_dsList_callFunc_onVector_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; the funciton is called for each of the vectors, ie, instead of trhee for-loops, only two for-loops are performed inside teh main-body; for every artimetic operaiton an if-branch-test is performed.
    "_3d_multAndPlus_dsList_callFunc_2ifBranchInner",  #! in each of the 3d-ops, one '+' and one '*' is performed ... which is similar to the cityblock-metric; each of the artimetic operaitosn are eprformed in a single funciotn; this reflects the strategy of using external libriares for computing similiarites (ie, a joiint-full strategy); for every artimetic operaiton an if-branch-test is performed.
    "_skLearn_euclidean_distances",  #! call sci-kit Learn's euclidean-funciton
    "_skLearn_manhattan_distances", #! call sci-kit Learn's manhatten-funciton
    "_skLearn_paired_cityblock", #! uses the 'pairwise' sci-kit learn module to copmute cityblock (or: mahatten) dsitance
    "_skLearn_paired_cosine",  #! uses the 'pairwise' sci-kit learn module to copmute distance
    #"_skLearn_metric_minkowski",  #! uses the 'pairwise' sci-kit learn module to copmute Minkowski-distance
]

def help(nrows, ncols):
    print("Name of possible functions to call:");
    for i, val in enumerate(arr_funcs):
        print("\t%d)\t%s %s" % (i, val, nrows));

def all(nrows, ncols):
    print("Applies for *all* sim-metric pamplementaiton-patterns in Python::");
    for i, val in enumerate(arr_funcs):
        if(val != "all"):
            print("\t APPLY\t%d)\t%s %s" % (i, val, nrows));
            globals()[val](nrows, ncols); #! where 'globals' is used to access funciotns outside this funciton's scope.


#! -----------------------------------------------------    
#!
#! Handle inpput-arguments:
parser = argparse.ArgumentParser()
parser.add_argument('name', 
    help='name of pattern'
)
parser.add_argument('n', 
                    help='dimension of matrix to calculcate',
                    type=int,
)
# parser.add_argument('-g', '--greeting', 
#     default='Hello',
#     help='optional alternate greeting'
# )
args = parser.parse_args()
print("Starts: ops=\"%s\" with dim=%d\n" % (args.name, args.n));
#! -----------------------------------------------------    
# print("{greeting}, {name}!".format(
#     greeting=args.greeting,
#        name=args.name)
# )

if __name__ == "__main__":
    #if(args.name == "help"):            
    #!
    #! Apply logics:
    #! Note: for expalantion of how below 'call fucniotn by its string-name' works, see: https://stackoverflow.com/questions/3061/calling-a-function-of-a-module-by-using-its-name-a-string
    locals()[args.name](args.n, args.n)
    # globals()["myfunction"]()
    #locals()["myfunction"](args.n, args.n)
    #method_to_call = getattr(foo, args.name)
