/* #include "kt_math_matrix.h" */
/* #include "kt_mathMacros.h" */
/* #include "mask_api.h" */
/* #include "types_base.h" */
#include "types.h"

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file kt_math_matrix
   @brief provide functions for math-operations on matrices
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/
//#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"



//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate" where we comptue the 'min' valeu for each 'run', ie, an overehad wrt. summations.
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling__slowDirectUpdate(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_adjusted; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_transposed[col_id];
      for(uint j = 0; j < size_adjusted; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    uint int_j = 0; t_float scalar_result = VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))); 
	    //	    uint int_j = 0; t_float scalar_result = VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1[int_j]), VECTOR_FLOAT_LOAD(&row_2[int_j]))); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {	
	      // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOAD(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	      scalar_result += VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	      //	      scalar_result += VECTOR_FLOAT_storeAnd_horizontalSum(VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1[int_j]), VECTOR_FLOAT_LOAD(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += scalar_result;
	  }
	}
      }
    }
  }
}


//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE" with tiling
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE__tiling(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  //const bool isSymmetric = matrix_transposed
  for(uint row_id = 0; row_id < size_adjusted; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
    
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = size_adjusted;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_transposed[col_id];
      for(uint j = 0; j < size_adjusted; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOAD(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOAD(&row_1[int_j]), VECTOR_FLOAT_LOAD(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}


//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow" with tiling
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__tiling(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < size_adjusted; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_transposed[col_id];
      for(uint j = 0; j < size_adjusted; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    uint int_j = 0; t_float scalar_result = row_1[int_j] * row_2[int_j];
	    int_j++;
	    for(; int_j < SM; int_j++) {	
	      scalar_result += row_1[int_j] * row_2[int_j];
	    }
	    //! Update the partial result:
	    const uint global_row_id = row_id+int_row_id;
	    const uint global_col_id = col_id+int_col_id;
#if(0) // then we inlcude a set of itnernal-tests, internal tests whcih are normally not include,d ie, to avoid cløutteirng our perofmrance-comparsion-tests.
	    assert( (col_id + SM) <= size_adjusted);
	    assert(int_col_id < SM);
	    assert(global_row_id < size_adjusted); assert(global_col_id < size_adjusted);
#endif
	    //! Update:
	    result[global_row_id][global_col_id] += scalar_result;
	  }
	}
      }
    }
  }
}


//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow" with SSE
 void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow__SSE(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      const t_float *__restrict__ row_1 = matrix[row_id];
      const t_float *__restrict__ row_2 = matrix_transposed[row_id];
      uint j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[j]), VECTOR_FLOAT_LOADU(&row_2[j])); j += VECTOR_FLOAT_ITER_SIZE;
      for(; j < size_adjusted; j += VECTOR_FLOAT_ITER_SIZE) {
	vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[j]), VECTOR_FLOAT_LOADU(&row_2[j]))); 
      }
      result[row_id][col_id] = VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
    }
  }
}


//! Extends the "kt_func_multiplyMatrices_slow_transposedAsInput" with a "__restrict__" guineklien-keyword to the compiler
void kt_func_multiplyMatrices_slow_transposedAsInput__restrictedRow(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      const t_float *__restrict__ row_1 = matrix[row_id];
      const t_float *__restrict__ row_2 = matrix_transposed[row_id];
      uint j = 0; t_float scalar_result = row_1[j] * row_2[j]; j++;     
      for(; j < size_adjusted; j++) {
	scalar_result += row_1[j] * row_2[j];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}


//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow_transposedAsInput(t_float **matrix, t_float **matrix_transposed, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(matrix_transposed); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix_transposed[col_id][j]; j++;
      for(; j < size_adjusted; j++) {
	scalar_result += matrix[row_id][j] * matrix_transposed[col_id][j];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}

//! A non-optmized muliplication-matrix-strategy where we use an 'extra' trosnped matrix-input to get a 'partial' increase in quesseiocnt-time.
void kt_func_multiplyMatrices_slow(t_float **matrix, const uint size_adjusted, t_float **result) {
  assert(matrix); assert(result); assert(size_adjusted);
  for(uint row_id = 0; row_id < size_adjusted; row_id++) {
    for(uint col_id = 0; col_id < size_adjusted; col_id++) {
      uint j = 0; t_float scalar_result = matrix[row_id][j] * matrix[j][col_id]; j++;
      for(; j < size_adjusted; j++) {
	scalar_result += matrix[row_id][j] * matrix[j][col_id];
      }
      result[row_id][col_id] = scalar_result;
    }
  }
}

//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! Comptue inner distance-metric for euclid
void kt_func_metricInner_fast__fitsIntoSSE__euclid__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result); assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
#if(1 == 1) //! TODO: consider dropping this new-added 'partlay-symmetry' case ... included iot. simplify comparison with "http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9" (oekseth, 06. feb. 2017)
    const uint size_local = row_id;
#else
    const uint size_local = nrows;
#endif
    for(uint col_id = 0; col_id < size_local; col_id += SM) {
      //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      //assert((col_id+SM) <= ncols);
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id + int_row_id];
	  //const t_float *__restrict__ row_1_base = matrix[row_id];
	  //const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = matrix[col_id + int_col_id];
	    //const t_float *__restrict__ row_2_base = matrix[col_id + int_col_id];
	    /* const t_float *__restrict__ row_1 = row_1_base + j; */
	    /* const t_float *__restrict__ row_2 = row_2_base + j; */

	    //const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOAD(..)" instead of the "VECTOR_FLOAT_LOAD(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    //assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}

//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__float(t_float **matrix, t_float **matrix_2, const uint nrows, const uint ncols, t_float **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const t_float *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //    for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const t_float *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const t_float *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const t_float *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_FLOAT_LOADU(..)" instead of the "VECTOR_FLOAT_LOADU(..)" call:
	    uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))); 
	    //uint int_j = 0; VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_FLOAT_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_FLOAT_ITER_SIZE) {
	      vec_result = VECTOR_FLOAT_ADD(vec_result, VECTOR_FLOAT_abs(VECTOR_FLOAT_SUB(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_FLOAT_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
//! ------------------------------------------------------------------------
void kt_func_metricInner_fast__fitsIntoSSE__euclid__int16(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  assert( (nrows % SM) == 0);   assert( (ncols % SM) == 0); //! ie, as we expect the 'metric' to 'fit into' the row and columsn, ie, to avoid any 'list-overhead'.
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < nrows; col_id += SM) {
    //for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT16_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT16_ITER_SIZE) {
	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])));
	    }
	    //! Update the partial result:
	    // FIXME: drop [”elow] asserts.
	    assert((row_id+int_row_id) < nrows); 	    assert((col_id+int_col_id) < ncols);
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT16_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__int16(int16_t **matrix, int16_t **matrix_2, const uint nrows, const uint ncols, int16_t **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const int16_t *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const int16_t *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const int16_t *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const int16_t *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_INT16_LOADU(..)" instead of the "VECTOR_INT16_LOADU(..)" call:
	    uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))); 
	    //uint int_j = 0; VECTOR_INT16_TYPE vec_result = VECTOR_INT16_MUL(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j])); 
	    int_j += VECTOR_INT16_ITER_SIZE;
	    for(; int_j < SM; int_j += VECTOR_INT16_ITER_SIZE) {
	      vec_result = VECTOR_INT16_ADD(vec_result, VECTOR_INT16_abs(VECTOR_INT16_SUB(VECTOR_INT16_LOADU(&row_1[int_j]), VECTOR_INT16_LOADU(&row_2[int_j]))));
	    }
	    //! Update the partial result:
	    result[row_id+int_row_id][col_id+int_col_id] += VECTOR_INT16_storeAnd_horizontalSum(vec_result);
	  }
	}
      }
    }
  }
}
/* //! ------------------------------------------------------------------------ */
/* //! Comptue inner distance-metric for euclid */
/* void kt_func_metricInner_fast__fitsIntoSSE__euclid__char(char **matrix, const uint nrows, const uint ncols, char **result, const uint SM) { */

/* } */
//! Comptue inner distance-metric for cityblock
void kt_func_metricInner_fast__fitsIntoSSE__cityBlock__char(char **matrix, char **matrix_2, const uint nrows, const uint ncols, char **result, const uint SM) {
  assert(matrix); assert(matrix_2); assert(result);  assert(nrows); assert(nrows);
  for(uint row_id = 0; row_id < nrows; row_id += SM) {
    const char *__restrict__ row_1_base = matrix[row_id];
    for(uint col_id = 0; col_id < ncols; col_id += SM) {
      const char *__restrict__ row_2_base = matrix_2[col_id];
      for(uint j = 0; j < ncols; j += SM) {
	//!
	//! start the tiling-meausrements:
	for(uint int_row_id = 0; int_row_id < SM; int_row_id++) {
	  const char *__restrict__ row_1 = row_1_base + int_row_id;
	  for(uint int_col_id = 0; int_col_id < SM; int_col_id ++) {
	    const char *__restrict__ row_2 = row_2_base + int_col_id;
	    //! Iterate through the column:
	    // FIXME: try to idneityf a 2d-allocation-strategy where we may safely use the "VECTOR_CHAR_LOAD(..)" instead of the "VECTOR_CHAR_LOAD(..)" call:

/* #ifndef NDEBUG */
/* #warning TODO[JC]: what is the differnece between "__m64" and "__m128i" ... ... why is there 'such a difnferec'e? .... and how may this difference be tuilized/ and voercomed? ... and how may the "_mm_max_epu8(..)" be combined with "_mm_sub_epi8(..)"? (oekseth, 06.. otk. 2016). */
/* #endif */
	    assert(false); // FXIEM: include [”elow] when we have larned how to combine "__m64" and "__m128i" wrt. the SSe-function-logics.
	    /* uint int_j = 0; VECTOR_CHAR_TYPE_alt vec_result =  */
	    /* 		      //VECTOR_CHAR_abs */
	    /* 		      _mm_abs_epi8 */
	    /* 		      (VECTOR_CHAR_SUB(VECTOR_CHAR_LOAD_alt(&row_1[int_j]), VECTOR_CHAR_LOAD_alt(&row_2[int_j]))); */
	    /* //uint int_j = 0; VECTOR_CHARYPE vec_result = VECTOR_CHAR_MUL(VECTOR_CHAR_LOAD(&row_1[int_j]), VECTOR_CHAR_LOAD(&row_2[int_j]));  */
	    /* int_j += VECTOR_CHAR_ITER_SIZE; */
	    /* for(; int_j < SM; int_j += VECTOR_CHAR_ITER_SIZE) { */
	    /*   vec_result = VECTOR_CHAR_ADD(vec_result, VECTOR_CHAR_abs(VECTOR_CHAR_SUB(VECTOR_CHAR_LOAD(&row_1[int_j]), VECTOR_CHAR_LOAD(&row_2[int_j])))); */
	    /* } */
	    /* //! Update the partial result: */
	    /* result[row_id+int_row_id][col_id+int_col_id] += VECTOR_CHAR_storeAnd_horizontalSum(vec_result); */
	  }
	}
      }
    }
  }
}



#define __M__calledInsideFunction
//! ------------------
#undef __MiV__mainIsDecelared
#include "tut_sse_matrixMul_cmpApproaches.c"
#undef  __M__calledInsideFunction


//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------

/**
   @remarks 
   @remarks 
   @remarks compile: gcc ops_matrix.c -std=c99 -lm -std=gnu99 -mssse3
 **/
int main(const int array_cnt, char **array) {
  if( (array_cnt >= 2) && (0 == strncmp("small_basicCases__language",array[1], strlen("small_basicCases__language")) ) ) { //! then we elvauate a small subset, a subset designed to 'reflect' our "tut__altLAng_simMetric.java" java-measurement-appraoch. 
    tut_sse_matrixMul_cmpApproaches(); //! ie, the call
  } else  if( (array_cnt >= 2) && (0 == strncmp("matrixSmallSmaller",array[1], strlen("matrixSmallSmaller")) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"    
    { //!    (b) 'increase for cols':
#undef listOf_cols_size
#define listOf_rows_size 1 
      uint listOf_rows[listOf_rows_size] = {128};
#undef listOf_cols_size
#define listOf_cols_size 1
      uint listOf_cols[listOf_cols_size] = {128*10};
      const char *str_filePath = "matrixSmall_basicCases_language_synt_simMetricEval_smallSmall_case_b";
      //!
      //! Apply the test:
      const bool is_ok = measureStub__case__basicCases__language__setOf(str_filePath, listOf_rows, listOf_rows_size, listOf_cols, listOf_cols_size, /*useColVal_alsoForRow=*/false);
      assert(is_ok);
    }
  } else  if( (array_cnt >= 2) && (0 == strncmp("matrixSmall",array[1], strlen("matrixSmall")) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"    
    { //!    (b) 'increase for cols':
#undef listOf_cols_size
#define listOf_rows_size 1 
    const uint b = 128;
    uint listOf_rows[listOf_rows_size] = {128};
      listOf_rows[0] = b;
#undef listOf_cols_size
#define listOf_cols_size 2
       uint listOf_cols[listOf_cols_size];
      for(uint i = 0; i < listOf_cols_size; i++) {
	listOf_cols[i] = 128 + b*i*100;
      }
      const char *str_filePath = "matrixSmall_basicCases_language_synt_simMetricEval_small_case_b";
      //!
      //! Apply the test:
      const bool is_ok = measureStub__case__basicCases__language__setOf(str_filePath, listOf_rows, listOf_rows_size, listOf_cols, listOf_cols_size, /*useColVal_alsoForRow=*/false);
      assert(is_ok);
    }

  } else  if( (array_cnt >= 2) && (0 == strncmp("basicCases__language",array[1], strlen("basicCases__language")) ) ) { //! where "sCSF" is a shorthand for "sysntetic code-style funciton"    
    if(false) { //! then we use an udpated prodcued where we 'traverse' two lists, and then costnrcut one 'unfied' result-amtrix:
      const char *str_filePath = "basicCases__language__synt_simMetricEval";
#undef listOf_rows_size
#undef listOf_cols_size
#define listOf_rows_size 2
#define listOf_cols_size 10
      //      const uint listOf_rows_size = 2; const uint listOf_cols_size = 10;
      const uint b = 128;
      uint listOf_rows[listOf_rows_size];
      listOf_rows[0] = b*50*1;
      listOf_rows[1] = b*50*10;
      //      = {b*50*1, b*50*10};

      for(uint i = 1; i <= listOf_rows_size; i++) {listOf_rows[i-1] = b*50*i;}
      uint listOf_cols[listOf_cols_size];
      for(uint i = 1; i <= listOf_cols_size; i++) {listOf_cols[i-1] = b*50*i;}
      //!
      //! Apply the test:
      const bool is_ok = measureStub__case__basicCases__language__setOf(str_filePath, listOf_rows, listOf_rows_size, listOf_cols, listOf_cols_size, /*useColVal_alsoForRow=*/false);
      assert(is_ok);
    } else {      
      measureStub__case__basicCases__language(/*nrows=*/2560, /*ncols=*/256);
      //measureStub__case__basicCases__language(/*nrows=*/10*128, /*ncols=*/2*128);
      if(true) {
	//! Note: in [below] we comapre wrth the ruslts from \cite{cmp}="http://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0351-9"
	measureStub__case__basicCases__language(/*nrows=*/50*128, /*ncols=*/4*128); //! ie, [54675, 559]=MULTMYEL in [above] \cite{cmp}
	measureStub__case__basicCases__language(/*nrows=*/100*128, /*ncols=*/4*128); //! ie, [54675, 559]=MULTMYEL in [above] \cite{cmp}
	measureStub__case__basicCases__language(/*nrows=*/140*128, /*ncols=*/4*128); //! ie, TCGA in [above] \cite{cmp}
	measureStub__case__basicCases__language(/*nrows=*/200*128, /*ncols=*/4*128); //! ie, TCGA in [above] \cite{cmp}
	measureStub__case__basicCases__language(/*nrows=*/428*128, /*ncols=*/4*128); //! ie, [54675, 559]=MULTMYEL in [above] \cite{cmp}
       }
      //const uint nrows = 30*128; 	const uint ncols = 2*128;
      measureStub__case__basicCases__language(/*nrows=*/10*128, /*ncols=*/2*128);
      measureStub__case__basicCases__language(/*nrows=*/10*128, /*ncols=*/6*128);
      //! --
      measureStub__case__basicCases__language(/*nrows=*/20*128, /*ncols=*/2*128); 
      measureStub__case__basicCases__language(/*nrows=*/20*128, /*ncols=*/6*128);
      //! --
      measureStub__case__basicCases__language(/*nrows=*/30*128, /*ncols=*/2*128);
      measureStub__case__basicCases__language(/*nrows=*/30*128, /*ncols=*/6*128);
      //! --
      measureStub__case__basicCases__language(/*nrows=*/40*128, /*ncols=*/2*128);
      measureStub__case__basicCases__language(/*nrows=*/40*128, /*ncols=*/6*128);
      //! --
    }
    return true;
  } 
  return 1;
}
