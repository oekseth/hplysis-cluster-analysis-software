#ifndef measure_base_h
#define measure_base_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file measure_base
   @brief provide basic logics for measruign the time-cost of differnet operations, ie, the base-fucntion for such (oekseth, 06. sept. 2016).
   @author Ole Kristina Ekseth (oekseth).
 **/


#define configure_globalProject_NOT_INCLUDE_configuration 1 //! which is sued to avoid including C++ specific cofniguriaotn.
//#include "libs.h"

//#include "types_base.h"

typedef unsigned int uint;

#include "libs.h"
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h> // some systems require it
#include <sys/stat.h>
#include <sys/termios.h> // for winsize
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/times.h>
#include <stdlib.h>  // rand(), srand()
#include <time.h>    // time()
//#include "measure.h"
#include "def_memAlloc.h"

static const uint kilo = 1000;
static clock_t clock_time_start; static clock_t clock_time_end;
static struct tms tms_start;
static  struct tms tms_end;
/**
   @brief Start the measurement with regard to time.
   @remarks
   - Uses the global vairalbe in this file for this prupose.
   - Clears the cache before starting this measurement, causing a small slowdown in the outside performance, though not included in the estimates returned.
**/
static  void start_time_measurement() {
/* #ifndef NDEBUG */
/*   //! Clears the cache by allocating 100MB of memory- and the de-allocating it: */
/*   // TODO: Consier adding random access to it for further obfuscating of earlier memory accesses. */
/*   uint size = 1024*1024*100; */
/*   const char default_value_char = '\0'; const uint string_size = size; */
/*   char *string = allocate_1d_list_char(string_size, default_value_char); assert(string); memset(string, '\0', 1000); */
/*   //char *tmp = new char[size]; */
/*   assert(string); */
/*   free_1d_list_char(&string); string = NULL;  */
/*     //delete [] tmp; tmp = NULL; size = 0; */
/* #endif */
  //tms_start = tms();
  clock_time_start = times(&tms_start);
  // if(() == -1) // starting values
  //   err_sys("times error");
}
/* /\** */
/*    @brief Ends the measurement with regard to time. */
/*    @param <msg> If string is given, print the status information to stdout. */
/*    @param <user_time> The CPU time in seconds executing instructions of the calling process since the 'start_time_measurement()' method was called. */
/*    @param <system_time> The CPU time spent in the system while executing tasks since the 'start_time_measurement()' method was called. */
/*    @return the clock tics on the system since the 'start_time_measurement()' method was called. */
/* **\/ */
/* static float end_time_measurement(const char *msg, float &user_time, float &system_time, const float prev_time_inSeconds) { */
/*   clock_time_end = times(&tms_end); */
/*   long clktck = 0; clktck =  sysconf(_SC_CLK_TCK); */
/*   const clock_t t_real = clock_time_end - clock_time_start; */
/*   const float time_in_seconds = t_real/(float)clktck; */
/*   const float diff_user = tms_end.tms_utime - tms_start.tms_utime; */
/*   if(diff_user) user_time   = (diff_user)/((float)clktck); */
/*   const float diff_system = tms_end.tms_stime - tms_start.tms_stime; */
/*   system_time = diff_system/((float)clktck); */
/*   if(msg) { */
/*     // printf("time_in_seconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__); */
/*     if( (prev_time_inSeconds == FLT_MAX) || (time_in_seconds == 0) ) { */
/*       printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg); */
/*     } else { */
/*       //float diff = prev_time_inSeconds / time_in_seconds; */
/*       float diff = time_in_seconds / prev_time_inSeconds; */
/*       // printf("\tprev_time_inSeconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__); */
/*       printf("%f x \ttick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg); */
/*     } */
/*   } */
/*   //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg); */
/*   return (float)time_in_seconds; */
/* } */


/**
   @brief Ends the measurement with regard to time.
   @return the clock tics on the system since the 'start_time_measurement()' method was called.
**/
static float end_time_measurement(const char *msg, const float prev_time_inSeconds) {
  float user_time = 0, system_time=0;
  clock_time_end = times(&tms_end);
  long clktck = 0; clktck =  sysconf(_SC_CLK_TCK);
  const clock_t t_real = clock_time_end - clock_time_start;
  const float time_in_seconds = t_real/(float)clktck;
  const float diff_user = tms_end.tms_utime - tms_start.tms_utime;
  if(diff_user) user_time   = (diff_user)/((float)clktck);
  const float diff_system = tms_end.tms_stime - tms_start.tms_stime;
  system_time = diff_system/((float)clktck);
  if(msg) {
    // printf("time_in_seconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
    if( (prev_time_inSeconds == FLT_MAX) || (time_in_seconds == 0) ) {
      printf("tick=%.4fs cpu=%.4fs system=%.4f (%s).\n", time_in_seconds, user_time, system_time, msg);
    } else {
      //float diff = prev_time_inSeconds / time_in_seconds;
      float diff = time_in_seconds / prev_time_inSeconds;
      // printf("\tprev_time_inSeconds = %f, prev=%f, at %s:%d\n", time_in_seconds, prev_time_inSeconds, __FILE__, __LINE__);
      printf("%f x \ttick=%.4fs cpu=%.4fs system=%.4f (%s).\n", diff, time_in_seconds, user_time, system_time, msg);
    }
  }
  //    if(msg) printf("-\tUsed %.4f seconds %s.\n", time_in_seconds, msg);
  return (float)time_in_seconds;
  //return end_time_measurement(msg, user_time, system_time, prev_time_inSeconds);
}

//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements(const char *stringOf_measureText, const uint size_of_array, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    const char default_value_char = '\0'; const uint string_size = 2000;
    char *string = allocate_1d_list_char(string_size, default_value_char); assert(string); memset(string, '\0', 1000);
    sprintf(string, "size=%u \t tag:%s", size_of_array, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    free_1d_list_char(&string); string = NULL; 
    
    return time_in_seconds;
  } 
}

//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately(const char *stringOf_measureText, const uint nrows, const uint ncols, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    const char default_value_char = '\0'; const uint string_size = 2000;
    char *string = allocate_1d_list_char(string_size, default_value_char); assert(string); memset(string, '\0', 1000);
    sprintf(string, "size=%u \t nrows=%u tag:%s", ncols, nrows, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    free_1d_list_char(&string); string = NULL; 
    //delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}

//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__tiling(const char *stringOf_measureText, const uint nrows, const uint ncols, const uint SM, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    const char default_value_char = '\0'; const uint string_size = 2000;
    char *string = allocate_1d_list_char(string_size, default_value_char); assert(string); memset(string, '\0', 1000);
    sprintf(string, "size=%u \t nrows=%u tileSM=%u tag:%s", ncols, nrows, SM, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //time.print_formatted_result(string); 
    free_1d_list_char(&string); string = NULL; 
    //delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}
//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements__specify_rowSizeSeperately__cntDisjoint(const char *stringOf_measureText, const uint nrows, const uint ncols, const uint cnt_disjoint, const float prev_time_inSeconds) {
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    const char default_value_char = '\0'; const uint string_size = 2000;
    char *string = allocate_1d_list_char(string_size, default_value_char); assert(string); memset(string, '\0', 1000);
    sprintf(string, "size=%u \t nrows=%u cnt-disjoint=%u tag:%s", ncols, nrows, cnt_disjoint, stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //Time.print_formatted_result(string); 
    free_1d_list_char(&string); string = NULL; 
    //delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}

//! Ends the time measurement for the array:
static float __assertClass_generateResultsOf_timeMeasurements__kmeans__noIteration(const char *stringOf_measureText, const uint nrows, const uint ncols, const uint nclusters, const bool transpose) {
  const float prev_time_inSeconds = FLT_MAX;
  assert(stringOf_measureText);
  assert(strlen(stringOf_measureText));
  //! Get knowledge of the memory-intiaition/allocation pattern:
  {
    //! Provides an identficator for the string:
    const char default_value_char = '\0'; const uint string_size = 2000;
    char *string = allocate_1d_list_char(string_size, default_value_char); assert(string); memset(string, '\0', 1000);
    sprintf(string, "size=%u \t nrows=%u nclusters=%u transpose='%s' tag:%s", ncols, nrows, nclusters, 
	    (transpose) ? "yes" : "no",
	    stringOf_measureText); //, stringOf_isTo_use_continousSTripsOf_memory, stringOf_isTo_use_transpose, stringOf_isTo_sumDifferentArrays, CLS);  
    //! Prints the data:
    //! Complte the measurement:
    const float time_in_seconds = end_time_measurement(string, prev_time_inSeconds);
    //Time.print_formatted_result(string); 
    free_1d_list_char(&string); string = NULL; 
    //delete [] string; string = NULL; 
    
    return time_in_seconds;
  } 
}


/**
   @brief Ends the measurement with regard to time.
   @return the clock tics on the system since the 'start_time_measurement()' method was called.
**/
static float end_time_measurement__dims(const uint nrows, const uint ncols, const char *msg, const float prev_time_inSeconds) {
  assert(msg);
  assert(strlen(msg) < 1000);
  char msg_extensive[1200]; memset(msg_extensive, '\0', 1200);
  sprintf(msg_extensive, "dims[%u, %u]::%s", nrows, ncols, msg);
  //! 
  return end_time_measurement(msg_extensive, prev_time_inSeconds);
}



#endif
