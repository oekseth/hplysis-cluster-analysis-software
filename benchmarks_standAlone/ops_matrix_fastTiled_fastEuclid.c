
//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------
//! Comptue inner distance-metric for euclid: fast-default:
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__euclid__float
#define __MiF__ops_each VECTOR_FLOAT_MUL(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__2Add
#define __MiF__ops_each VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__3Add
#define __MiF__ops_each VECTOR_FLOAT_ADD(vec_default, VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])))
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__2Add_1Abs
#define __MiF__ops_each VECTOR_FLOAT_abs(VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])))
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__2Add_2Abs
#define __MiF__ops_each VECTOR_FLOAT_abs(VECTOR_FLOAT_ADD(VECTOR_FLOAT_abs(VECTOR_FLOAT_LOADU(&row_1[int_j])), VECTOR_FLOAT_LOADU(&row_2[int_j])))
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__1Add_1Max
#define __MiF__ops_each VECTOR_FLOAT_MAX(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j]))
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__1Add_2Max
#define __MiF__ops_each VECTOR_FLOAT_MAX(VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])), vec_default)
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__2Add_1Mul
#define __MiF__ops_each VECTOR_FLOAT_MUL(VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])), vec_default)
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__2Add_2Mul
#define __MiF__ops_each VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])), vec_default), vec_default)
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast kt_func_metricInner_fast__fitsIntoSSE__float__2Add_3Mul
#define __MiF__ops_each VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_MUL(VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOADU(&row_1[int_j]), VECTOR_FLOAT_LOADU(&row_2[int_j])), vec_default), vec_default), vec_default)
#include "ops_matrix_fastTiled.c"

