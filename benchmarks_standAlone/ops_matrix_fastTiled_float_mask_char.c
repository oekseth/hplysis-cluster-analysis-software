//! ------------------------------------------------------------------------
//! ------------------------------------------------------------------------ different masking strategies:
//!
//!
//! 
#define __funcName__fast float_mask_char_pair_implicit_removeHidden_getCount
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2 VECTOR_FLOAT_TYPE vec_maskCount = VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCount(vec_1, vec_2);
#define __MiF__ops_each vec_maskCount
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast float_mask_char_pair_implicit_removeHidden
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2  VECTOR_FLOAT_dataMask_implicit_updatePair(vec_1, vec_2); //! ie, seet to 'zero' the valeus which are set to T_FLOAT_MAX
#define __MiF__ops_each VECTOR_FLOAT_ADD(vec_1, vec_2)
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast float_mask_char_pair_replaceZeroFrom_nan
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(VECTOR_FLOAT_DIV(vec_1, vec_2))
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast float_mask_char_pair_replaceZeroFrom_inf
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each VECTOR_FLOAT_dataMask_replaceINF_by_Zero(VECTOR_FLOAT_DIV(vec_1, vec_2))
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast float_mask_char_pair_zeroFilter_case1_add
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(vec_1, vec_2, VECTOR_FLOAT_ADD)
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast float_mask_char_pair_zeroFilter_case1_div
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(vec_1, vec_2, VECTOR_FLOAT_DIV)
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast float_mask_char_pair_case2_div
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_1(vec_1, vec_2, VECTOR_FLOAT_DIV)
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast float_mask_char_pair_case3_div
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_2(vec_1, vec_2, VECTOR_FLOAT_DIV)
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast float_mask_char_pair_case0_div
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each VECTOR_FLOAT_omitZeros_fromComputation_DIV(vec_1, vec_2)
#include "ops_matrix_fastTiled.c"
#if(false)
/*
//!
//! 
#define __funcName__fast 
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each 
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast 
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each 
#include "ops_matrix_fastTiled.c"
//!
//! 
#define __funcName__fast 
#define __MiF__ops_each_preStep_1 VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_LOADU(&row_1[int_j]); VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_LOADU(&row_2[int_j]); 
#define __MiF__ops_each_preStep_2
#define __MiF__ops_each 
#include "ops_matrix_fastTiled.c"
*/
#endif
