print(__doc__)

import numpy as np

from sklearn.cluster import DBSCAN
from sklearn import cluster
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import NearestNeighbors
from sklearn.neighbors import KDTree
from sklearn import metrics
from sklearn.metrics.cluster import silhouette_score
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.metrics.pairwise import manhattan_distances
from sklearn.metrics import euclidean_distances #! which we use for simliarty-metrics
#from sklearn.metrics import cosine_similarity
from sklearn.metrics.pairwise import paired_distances
from scipy.spatial.distance import cosine, cityblock, minkowski, wminkowski
from sklearn.metrics.pairwise import paired_distances
from sklearn.metrics.pairwise import pairwise_distances
import sys


import time


# FIXME: compute .... using permtuation from .... "tmp.py"



#nrows = 1000*1000
#nrows = 1000
nrows = 1000*2
#nrows = 1000*10
#nrows = 750*10
#nrows = 750*40
ncols = 5
#ncols = 20
#ncols = 512
#ncols = 30000
#100 000 000 = 10^8 = (10^4)^2
# centers = [[1, 1, 1, 10], [-1, -1, -1, 20], [1, -1, -1, 30], [1, -1, 1, 40], [-1, -1, 1, -90]]
# #centers = [[1, 1, 1], [-1, -1, -1], [1, -1, 1]]
# #centers = [[1, 1], [-1, -1], [1, -1]]
# X, labels_true = make_blobs(n_samples=nrows, centers=centers, cluster_std=0.4,
#                             random_state=0)
X = np.random.rand(nrows, ncols)


# FIXME: tmeplte ... tut__altLAng_sciPy__listAccess.py

#print("len(X)=%d, len(X[0])=%d" % len(X), len(X[0]))
#X = StandardScaler().fit_transform(X)
# t0 = time.time()
# kdt = KDTree(X, leaf_size=12, metric='euclidean')
# #nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(X)
# #!
# #! Complete:
# #X = np.random.rand(nrows, ncols)
# t1 = time.time()
# print('Time(construct): %.2fs' % (t1 - t0)).lstrip('0')
# t0 = time.time()
# #!
# #!
# cnt_calls = 0
# for i in xrange(nrows):
#     dist, ind = kdt.query([X[i]], k=10)                
#     cnt_calls = cnt_calls+1

# t1 = time.time()
# print('Time(query): %.2fs' % (t1 - t0)).lstrip('0')
# print('Count(query-vertices): %d' % cnt_calls)
#!
#! DB-SCAN:
t0 = time.time()
true_k = 10
km = KMeans(n_clusters=true_k, 
#init='k-means++',
            max_iter=100, n_init=1)
#km = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=1)
#            verbose=opts.verbose)
#dbscan = cluster.DBSCAN(algorithm='kd_tree')
#dbscan = cluster.DBSCAN(eps=.2, metric='euclid', algorithm='kd_tree')
#dbscan = cluster.DBSCAN(eps=.2, metric='cosine', algorithm='kd_tree')
#dbscan = cluster.DBSCAN(eps=.2, metric='kulsinski', algorithm='kd_tree')
#dbscan = cluster.DBSCAN(eps=.2, metric='kulsinski', algorithm='brute')
#dbscan = cluster.DBSCAN(eps=.2, metric='canberra', algorithm='kd_tree')
#dbscan = cluster.DBSCAN(metric='canberra', algorithm='brute')
#dbscan = cluster.DBSCAN(eps=.2, algorithm='kd_tree')
#dbscan.fit(X)
km.fit(X)
t1 = time.time()
print('Time(k-means++): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
#print('Time(dbSCAN): %.2fs' % (t1 - t0)).lstrip('0')
metrics.silhouette_score(X, km.labels_, metric='euclidean') #, sample_size=1000)
t1 = time.time()
print('Time(Silhouette::Euclid): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()


closest_dist_sq = manhattan_distances(X, X)
t1 = time.time()
print('Time(metric::manhattan): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
closest_dist_sq = paired_distances(X, X, metric="cityblock", n_jobs=1)
t1 = time.time()
print('Time(metric::cityblock--paired): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
closest_dist_sq = pairwise_distances(X, X, metric="cosine", n_jobs=1) #! ie, "scipy.spatial"
t1 = time.time()
print('Time(metric::cosine--paired): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
if(False):
    closest_dist_sq = pairwise_distances(X, X, metric=cosine) #! ie, "scipy.spatial"
    t1 = time.time()
    print('Time(metric::cosine-paired--alt): %.2fs' % (t1 - t0)).lstrip('0')

#! ----------------------
metrics.silhouette_score(closest_dist_sq, km.labels_, metric='precomputed') #, sample_size=1000)
#      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))
t1 = time.time()
print('Time(Silhouette::sim-pre): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
#! ----------------------
metrics.silhouette_score(closest_dist_sq, km.labels_, metric='sqeuclidean') #, sample_size=1000)
#      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))
t1 = time.time()
print('Time(Silhouette::"sqeuclidean"): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
#! 
#! ----------------------
#! Note: for list of supported emtric see "externalLibs__scikit_learn/scikit-learn-master/sklearn/metrics/pairwise.py" 
metrics.silhouette_score(closest_dist_sq, km.labels_, metric='sokalsneath') #, sample_size=1000)
#      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))
t1 = time.time()
print('Time(Silhouette::"sokalsneath"): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
#! 
#! ----------------------
metrics.calinski_harabaz_score(closest_dist_sq, km.labels_) #, metric='precomputed') #, sample_size=1000)
#      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))
t1 = time.time()
print('Time(VRC): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()
#! 
#! ----------------------
#metrics.(closest_dist_sq, km.labels_, metric='precomputed') #, sample_size=1000)
#      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))
t1 = time.time()
#print('Time(Silhouette::sim-pre): %.2fs' % (t1 - t0)).lstrip('0')
t0 = time.time()

#metrics.silhouette_score(X, dbscan.labels_, metric='euclidean', sample_size=1000)

        # # Given that the actual labels are used, we can assume that S would be
        # # positive.
        # score_precomputed = silhouette_score(D, y, metric='precomputed')
        # assert_greater(score_precomputed, 0)
        # # Test without calculating D
        # score_euclidean = silhouette_score(X, y, metric='euclidean')
        # assert_almost_equal(score_precomputed, score_euclidean)




#    dbscan_brute_1 = cluster.DBSCAN(eps=.2, metric='euclidean', algorithm='brute')

#    dbscan_canberra_brute_1 = cluster.DBSCAN(eps=.2, metric='canberra', algorithm='brute')
