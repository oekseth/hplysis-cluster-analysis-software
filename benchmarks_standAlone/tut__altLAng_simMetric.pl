#!/usr/bin/perl 

use warnings;
use strict;
use POSIX;
use Carp;
use Time::HiRes qw( gettimeofday tv_interval);

=head
    @brief ivnestigate time-cost of a Perl-based comptaution of the Euclidioan simlairty-emtirc (oekseth, 06. mar. 2017)
=cut
my ($user0, $system0, $child_user0, $child_system0, $start_time_0);
sub start_measurement {
    ($user0, $system0, $child_user0, $child_system0) = times;
    $start_time_0 = [Time::HiRes::gettimeofday()];
    #$wallclock0 = Time::HiRes::tv_interval($start_time_0);
}

sub end_measurement {
    my ($user1__, $system1__, $child_user1__, $child_system1__) = times;
    my ($user1, $system1, $child_user1, $child_system1) =  ($user1__- $user0, $system1__ - $system0, $child_user1__ - $child_user0, $child_system1__ - $child_system0 );
    my $wallclock1 = Time::HiRes::tv_interval($start_time_0);
    return ($wallclock1, $user1, $system1);
}

sub __computePair {
    my($score_1, $score_2) = @_;
    if(defined($score_1) && defined($score_2)) {
	return ($score_1*$score_2);
    }
    return 0;
}
sub __callColFunc_pairFuncCall {
    my($ncols, $matrix, $row_in, $row_out) = @_;
    my $sum = 0;
    for(my $col = 0; $col < $ncols; $col++) {
	my $score_1 = $matrix->[$row_in][$col];
	my $score_2 = $matrix->[$row_out][$col];
	#! Sum:
	$sum += __computePair($score_1, $score_2);
    }
    #! @return
    return $sum;
}

sub __callColFunc {
    my($ncols, $matrix, $row_in, $row_out) = @_;
    my $sum = 0;
    for(my $col = 0; $col < $ncols; $col++) {
	my $score_1 = $matrix->[$row_in][$col];
	my $score_2 = $matrix->[$row_out][$col];
	#! Sum:
	if(defined($score_1) && defined($score_2)) {
	    $sum += ($score_1*$score_2);
	}
    }
    #! @return
    return $sum;
}

sub compute__simMatrix {
    my($nrows, $ncols) = @_;
    #! Intiate the matrix:
    my @matrix = ();        
    my @row_currMeasurement = ("matrix[" . $nrows . "," . $ncols . "]"); #! ie, the name of the rwo.
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	my @row = ();
	for(my $col = 0; $col < $ncols; $col++) {
	    push(@row, $col);
	}
	push(@matrix,\@row);
    }
    #!
    #! Comptue:
    start_measurement();
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	my $sum = 0;
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    for(my $col = 0; $col < $ncols; $col++) {
		my $score_1 = 1.0;
		my $score_2 = 2.1;
		$sum += ($score_1*$score_2);
	    }
	}
    }
    my ($wallclock1, $user1, $system1) = end_measurement();
    push(@row_currMeasurement, $wallclock1);
    #!
    #! 
    #! New case: 
    #!
    #! Comptue:
    start_measurement();
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	my $sum = 0;
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    for(my $col = 0; $col < $ncols; $col++) {
		my $score_1 = $matrix[$row_in][$col];
		my $score_2 = $matrix[$row_out][$col];
		#! Sum:
		$sum += ($score_1*$score_2);
	    }
	}
    }
    ($wallclock1, $user1, $system1) = end_measurement();
    push(@row_currMeasurement, $wallclock1);
    #!
    #! 
    #! New case: 
    #! Comptue:
    start_measurement();
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	my $sum = 0;
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    for(my $col = 0; $col < $ncols; $col++) {
		my $score_1 = $matrix[$row_in][$col];
		my $score_2 = $matrix[$row_out][$col];
		#! Sum:
		if(defined($score_1) && defined($score_2)) {
		    $sum += ($score_1*$score_2);
		}
	    }
	}
    }
    ($wallclock1, $user1, $system1) = end_measurement();
    push(@row_currMeasurement, $wallclock1);    
    #!
    #! 
    #! New case: 
    #! Comptue:
    start_measurement();
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	my $sum = 0;
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    $sum += __callColFunc($ncols, \@matrix, $row_in, $row_out);
	}
    }
     ($wallclock1, $user1, $system1) = end_measurement();
    push(@row_currMeasurement, $wallclock1);    
    #!
    #! 
    #! New case: 
    #! Comptue:
    start_measurement();
    for(my $row_in = 0; $row_in < $nrows; $row_in++) {
	my $sum = 0;
	for(my $row_out = 0; $row_out < $nrows; $row_out++) {
	    $sum += __callColFunc_pairFuncCall($ncols, \@matrix, $row_in, $row_out);
	}
    }
     ($wallclock1, $user1, $system1) = end_measurement();
    push(@row_currMeasurement, $wallclock1);    
    #! ***********************************************'
    #! @return:
    return @row_currMeasurement;
}


#! **********************************************'
#!
#! Generate a header: 
printf("%s\n", join("\t", ("row-name", "case::numeric::mult", "case::numeric::mult::data", "case::numeric::mult::data-and-mask", "case::numeric::mult::data-and-mask:colFunc", "case::numeric::mult::data-and-mask:colFunc:pairFunc")));
printf(STDERR "%s\n", join("\t", ("row-name", "case::numeric::mult", "case::numeric::mult::data", "case::numeric::mult::data-and-mask", "case::numeric::mult::data-and-mask:colFunc", "case::numeric::mult::data-and-mask:colFunc:pairFunc")));
#! ***********************************************'
for(my $case_id = 0; $case_id < 12; $case_id++) {
    my $nrows = 128*(1 + $case_id);
    my $ncols = $nrows; 
    #my $ncols = 128*(1 + $case_id);
    #! Comptue: 
    my @row = compute__simMatrix($nrows, $ncols);
    #! Write out:
    printf("%s\n", join("\t", @row));
    printf(STDERR "%s\n", join("\t", @row));
}
