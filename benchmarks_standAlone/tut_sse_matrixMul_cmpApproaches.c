// #include "hp_api_fileInputTight.h" //! ie, the simplifed API for hpLysis

#include "kt_matrix.c"
#include "measure_base.h"

//! Compute measure for: Euclid
#define correlation_macros__distanceMeasures__euclid(value1, value2) ({t_float term = value1 - value2; term = term*term; term;})

// #include "tut_sse_matrixMul_cmpApproaches__stub.c"

//#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
#include "measureStub__case__basicCases__language.c" //! the 'performance-evlauation-wrapeprs' for our appraoch: describes function "measureStub__case__basicCases__language(..)" (oekseth, 06. feb. 2017).
//#endif 


#ifndef __M__calledInsideFunction //! where the latter macro is used to simplify our 'generic' test-buidlint-strategy
/**
   @brief evalautes different time-cost optimizaiton-strategies wrt. computation of simlairty-metrics (oekseth, 06. mar. 2017).
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks 
   -- to buidl this tut-example (in the soruce-folder) call: g++ -I../src/  -O2  -mssse3   -L ../src/  tut_sse_matrixMul_cmpApproaches.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC; ./a.out 2>err.txt
**/
int main() 
#else
  static void tut_sse_matrixMul_cmpApproaches()
#endif
{

    const uint b = 128;
#undef listOf_cols_size
#define listOf_rows_size 1 
    uint listOf_rows[listOf_rows_size] = {128};
#define listOf_cols_size  10
    uint listOf_cols[listOf_cols_size];
    for(uint i = 1; i <= listOf_cols_size; i++) {listOf_cols[i-1] = b*i;}
    //if(false)
    { //!    (a) 'increse for both nrows and ncols':
      const char *str_filePath = "basicCases_language_synt_simMetricEval_small_case_a";
      //!
      //! Apply the test:
      const bool is_ok = measureStub__case__basicCases__language__setOf(str_filePath, listOf_rows, listOf_rows_size, listOf_cols, listOf_cols_size, /*useColVal_alsoForRow=*/true);
      assert(is_ok);
    }
    { //!    (b) 'increase for cols':
      listOf_rows[0] = b;
#undef listOf_cols_size
#define listOf_cols_size 10
       uint listOf_cols[listOf_cols_size];
      for(uint i = 0; i < listOf_cols_size; i++) {
	listOf_cols[i] = 128 + b*i*100;
      }
      const char *str_filePath = "basicCases_language_synt_simMetricEval_small_case_b";
      //!
      //! Apply the test:
      const bool is_ok = measureStub__case__basicCases__language__setOf(str_filePath, listOf_rows, listOf_rows_size, listOf_cols, listOf_cols_size, /*useColVal_alsoForRow=*/false);
      assert(is_ok);
    }
    //if(false)
    { //!    (c) 'increase for rows':
      const char *str_filePath = "basicCases_language_synt_simMetricEval_small_case_c_swapRowCols";
      //!
      //! Apply the test:
      const bool is_ok = measureStub__case__basicCases__language__setOf(str_filePath, listOf_cols, listOf_cols_size, listOf_rows, listOf_rows_size, /*useColVal_alsoForRow=*/false);
      assert(is_ok);
    }

  //!
  //! @return
}
//#endif
