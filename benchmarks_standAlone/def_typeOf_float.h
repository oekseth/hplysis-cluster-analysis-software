#ifndef def_typeOf_float_h
#define def_typeOf_float_h

/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */


#define VECTOR___const__PI 3.1415926535897932384626433832795
#define VECTOR___const__log2_inv 3.321928095 //! ie, 1/log(2), where the result is dervied using NTNUS standard scritic calculator "HP 30s" (oekseth, 06. des. 2016)

#if VECTOR_CONFIG_USE_FLOAT == 1
#define MiC__float_resultBuffer_empty {0, 0, 0, 0}
#else
#define MiC__float_resultBuffer_empty {0, 0}
#endif


//!
//! Handle different options wrt. "float" or "double"
#if VECTOR_CONFIG_USE_FLOAT == 1
//! Define the type, ie, to simplify our allcoations.
typedef float t_float;
//! Change the sign of a float (oekseth, 06. des. 2016).
// FIXME[JC]: may you try to write an imrpvoed code wr.t 'sign changing' (oekseth, 06. des. 2016).
#define mathLib_float_changeSign(scalar) ({scalar *-1;})
//! @return the logratihm for a 4-byte float:
#define mathLib_float_log(inp_scalar) ({logf((t_float)inp_scalar);})
//! Comptue the lgoarighm for both postivie and neagitve numbers, ie, by first 'taking' teh absoltue value (oekseth, 06. des. 2016).
#define mathLib_float_log_abs(inp_scalar) ({mathLib_float_log(mathLib_float_abs(inp_scalar));})
#define mathLib_float_log_2(inp_scalar) ({VECTOR___const__log2_inv*logf(inp_scalar);})
//! Comptue the lgoarighm for both postivie and neagitve numbers, ie, by first 'taking' teh absoltue value (oekseth, 06. des. 2016).
#define mathLib_float_log_2_abs(inp_scalar) ({VECTOR___const__log2_inv*mathLib_float_log(mathLib_float_abs(inp_scalar));})
//! @return the square-root for a 4-byte float:
#define mathLib_float_sqrt(scalar) ({sqrtf(scalar);})
#define mathLib_float_sqrt_abs(scalar) ({sqrtf(mathLib_float_abs(scalar));})
//! @return the arc-tangent for a 4-byte float:
#define mathLib_float_atan(scalar) ({atanf(scalar);})
//! @return the  for an 8-byte float:
#define mathLib_float_sin(scalar) ({sinf(scalar);})
//! @return the  for an 8-byte float:
#define mathLib_float_cos(scalar) ({cosf(scalar);})
//! @return the  for an 8-byte float:
#define mathLib_float_cosh(scalar) ({coshf(scalar);})
//! @return the  for an 8-byte float:
#define mathLib_float_tanh(scalar) ({tanhf(scalar);})
#define mathLib_float_asin(scalar) ({asinf(scalar);})
#define mathLib_float_acos(scalar) ({acosf(scalar);})
//#define mathLib_float_(scalar) ({f(scalar);})
#define mathLib_float_atan2(scalar1, scalar2) ({atan2f(scalar1, scalar2);})
#define mathLib_float_mod(v1, v2) ({fmodf(v1, v2);})
//! @return teh absoltue values for a 4-byte float:
#define mathLib_float_abs(scalar) ({fabsf(scalar);})
//! @return teh exponnent value for a 4-byte float:
#define mathLib_float_exp(scalar) ({expf(scalar);})
//! @return the 'power(..)' for a 4-byte float:
#define mathLib_float_pow(scalar, value_power) ({ \
      assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(scalar >= 0); /*ie, as 'otherwise' would result in "NAN" values (oekseth, 06. mar. 2017) */  \
      const t_float result_pow = powf((float)scalar, (float)value_power);	\
      /*printf("\t(in-macro)\t result=%f given 'scalar=%f, value_power=%f', at %s:%d\n", result_pow, (float)scalar, (float)value_power, __FILE__, __LINE__);*/ \
      result_pow;})
#define mathLib_float_pow_abs(scalar, value_power) ({mathLib_float_pow(mathLib_float_abs(scalar), value_power);}) //! where latter is used to 'handle' negative "scalar" values (oekseth, 06. mar. 2017)
//! @returnt the floor-value
#define mathLib_float_floor(scalar) ({floorf(scalar);})
//! @return the ceil-value
#define mathLib_float_ceil(scalar) ({ceilf(scalar);})


//! Udpate wrt. the itnrisitnistics:
#define VECTOR_FLOAT_ITER_SIZE 4
#define T_FLOAT_MIN  FLT_MIN
#define T_FLOAT_MIN_ABS  -1*FLT_MAX
#define T_FLOAT_MAX  FLT_MAX
//! --- data-access
#define VECTOR_FLOAT_TYPE __m128
//! ---- basic data-loading:
#define VECTOR_FLOAT_SET _mm_set_ps
#define VECTOR_FLOAT_SET1 _mm_set1_ps
#define VECTOR_FLOAT_SET_zero _mm_setzero_ps
#define VECTOR_FLOAT_SET1_SINGLE _mm_set_ss
#define VECTOR_FLOAT_LOAD _mm_load_ps
#define VECTOR_FLOAT_LOADU _mm_loadu_ps
//for(uint m = 0; m < 4; m++) {res[i][j+m] = mul1[j+m][i];} // <-- FIXME[jc]: may you consider writing an atlernative macro-def where we use the 'base-oeprations' ... ??
#define VECTOR_FLOAT_STORE _mm_store_ps
#define VECTOR_FLOAT_STOREU _mm_storeu_ps
//! --- simple aritmetics
#define VECTOR_FLOAT_MUL _mm_mul_ps
#define VECTOR_FLOAT_DIV _mm_div_ps
#define VECTOR_FLOAT_RCP _mm_rcp_ps //! which compute "1/value"


// FIXEM[tutorial]: seems like we hadd an odd issue where we had defined "VECTOR_FLOAT_SUB _mm_mul_ps" and "VECTOR_FLOAT_ADD _mm_mul_ps" instead of the correct "VECTOR_FLOAT_SUB _mm_sub_ps" and "VECTOR_FLOAT_SUB _mm_add_ps"
#define VECTOR_FLOAT_ADD _mm_add_ps
// FIXME: validate that the result of "VECTOR_FLOAT_ADD_SINGLE" correspdosnt ot he callers expecations.
// FXIME[jc]: is there a better 'fucntion to call' than [”elow]?
/* #define VECTOR_FLOAT_ADD_SINGLE(vec_result, scalar) ( _mm_add_ss(vec_result,  */
#define VECTOR_FLOAT_SUB _mm_sub_ps
// FIXME: validate that the result of "VECTOR_FLOAT_SUB_SINGLE" correspdosnt ot he callers expecations.
#define VECTOR_FLOAT_SUB_SINGLE _mm_sub_ss

#define VECTOR_FLOAT_SQRT _mm_sqrt_ps
#define VECTOR_FLOAT_SQRT_abs(vec) ({ _mm_sqrt_ps(VECTOR_FLOAT_abs(vec));})
//! --- boolean lgoics, logics which are used to 'consturct' if-else clauses
// TODO[bonus] investigate if "_mm_rcp_ps" may be used to comptue "1/value" aritmetics ... and accraucy ... and thereafter (a) updat eour perofmrance-emasurements ... and (b) consider updating our SSE-difstnac-metrics. (oekseth, 06. sept. 2016)
//#define VECTOR_FLOAT_AND _mm_and_ps
#define VECTOR_FLOAT_AND _mm_and_ps
#define VECTOR_FLOAT_ANDNOT _mm_andnot_ps
#define VECTOR_FLOAT_OR _mm_or_ps
#define VECTOR_FLOAT_XOR _mm_xor_ps
//! Logics for comparison:
#define VECTOR_FLOAT_CMPEQ _mm_cmpeq_ps
#define VECTOR_FLOAT_CMPLT _mm_cmplt_ps
#define VECTOR_FLOAT_CMPGT _mm_cmpgt_ps
#define VECTOR_FLOAT_CMPNEQ _mm_cmpneq_ps
//! --- comparisons
#define VECTOR_FLOAT_MIN _mm_min_ps
#define VECTOR_FLOAT_MAX _mm_max_ps
//! Nan-filtering
#define VECTOR_FLOAT_CMPORD _mm_cmpord_ps
#define VECTOR_FLOAT_CMPUNORD _mm_cmpunord_ps //! ie, 'neither' (oekseth, 06. setp. 2016)
//! --- conversions
#define VECTOR_FLOAT_convertFrom_epi32 _mm_cvtepi32_ps
#define VECTOR_FLOAT_convertFrom_epi16 _mm_cvtepi16_ps

#define VECTOR_FLOAT_boolean_scalar_isNAN(val) (isnan(val))/*  \ */

//! Revertes the roder of a vector.
#define VECTOR_FLOAT_revertOrder(vec) (_mm_shuffle_ps(vec, vec, _MM_SHUFFLE(0, 1, 2, 3)))
//! Convert a float to an 8-bit intrisinistic char-vector.
//! Note: demonstrate how an 32-bit floatign-point-array may be converted into a char-array
#define VECTOR_CHAR_convertFrom_float_vector(vec) (_mm_cvtps_pi8(vec))
//! Convert from an 8-bit "__m64" to an 32-bit "__mm128"
//! Note: demonstrate how an 8-bit mask-arary may be loaded into a flatoign-potin-array.
#define VECTOR_FLOAT_convertFrom_CHAR(vec) (_mm_cvtpi8_ps(vec))
#define VECTOR_FLOAT_convertFrom_CHAR_buffer__xmtCharOverFlow(mem_ref, startPos) (VECTOR_FLOAT_SET((t_float)mem_ref[startPos + 0], (t_float)mem_ref[startPos + 1], (t_float)mem_ref[startPos + 2], (t_float)mem_ref[startPos + 3])) //! where latter is sued as an alnteriate to "VECTOR_FLOAT_convertFrom_CHAR_buffer_withOverflow(..)" to ensure 'comaptiblity' witht he "VECTOR_FLOAT_ITER_SIZE", ie, to avoid 'access to un-asigned valeus' wrt. the char-buffer (When used in anttieraiotn with 'flaots', as expected wrt. this macro) (oekseth, 06. mar. 2017)
#define VECTOR_FLOAT_convertFrom_CHAR_buffer(mem_ref) (VECTOR_FLOAT_convertFrom_CHAR(VECTOR_CHAR_LOAD(mem_ref)))
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_INT(vec) (VECTOR_FLOAT_revertOrder(_mm_cvtepi32_ps(vec)))
// FIXME[jc]: validate correctness of [below] ... adn then compare with [above] "VECTOR_FLOAT_convertFrom_INT(..)" wrt. perfroamcne.
#define VECTOR_FLOAT_convertFrom_INT_old(vec) ({ \
  int buffer[VECTOR_FLOAT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec); \
  VECTOR_FLOAT_SET((t_float)buffer[0], (t_float)buffer[1], (t_float)buffer[2], (t_float)buffer[3]); \
})

#define VECTOR_FLOAT_converTo_int_32 _mm_cvttps_epi32 /* ie, convert from float to 32-bit int*/
#define VECTOR_FLOAT_converTo_int_16 _mm_cvttps_epi16 /* ie, convert from float to 32-bit int*/

// FIXME[jc]: validate correctness of [below]  <-- seems like the 'closest' optmial funciton is "__m128d _mm_cvtepu32_pd (__m128i a)" ... ie, only workign for 'dobule' .. ie, an overhead
/* #define VECTOR_FLOAT_convertFrom_UINT(vec) (VECTOR_FLOAT_revertOrder(_mm_cvtepu32_ps(vec))) */
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_UINT(vec) ({ \
  int buffer[VECTOR_FLOAT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec); \
  VECTOR_FLOAT_SET((t_float)buffer[0], (t_float)buffer[1], (t_float)buffer[2], (t_float)buffer[3]); \
})
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_UINT_data(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+0], (t_float)buffer[index+1], (t_float)buffer[index+2], (t_float)buffer[index+3]);})
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_UINT_buffer(buffer) ({VECTOR_FLOAT_SET((t_float)buffer[0], (t_float)buffer[1], (t_float)buffer[2], (t_float)buffer[3]);})
//! A fucntion which is used to handle diffenret data-type-inptus, eg, "short int"
#define VECTOR_FLOAT_convertFrom_data(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+0], (t_float)buffer[index+1], (t_float)buffer[index+2], (t_float)buffer[index+3]);})
//! A fucntion which is used to handle diffenret data-type-inptus, eg, "short int"
#define VECTOR_FLOAT_convertFrom_data_notInv(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+3], (t_float)buffer[index+2], (t_float)buffer[index+1], (t_float)buffer[index+0]);})


//#define VECTOR_FLOAT_convertFrom_INT _mm_castsi128_pd

// FIXME[jc]: seems like we do nto manage to compiel/'use the _mm_load_pd(..) ... ie, any suggestions 'for how getting this to work'?
// FIXME: wrt. [”elow] write a performance-test where we compare with a to-be-written "VECTOR_FLOAT_LOG_data(..)"
//! @return the log of teh function
#define VECTOR_FLOAT_LOG(vec_input) ({ \
    float buffer[VECTOR_FLOAT_ITER_SIZE]; \
    _mm_store_ps(buffer, vec_input); \
    _mm_set_ps((float)logf(buffer[0]), (float)logf(buffer[1]), (float)logf(buffer[2]), (float)logf(buffer[3]));	\
})

#define VECTOR_FLOAT_EXP(vec_input) ({ \
    float buffer[VECTOR_FLOAT_ITER_SIZE]; \
    _mm_store_ps(buffer, vec_input); \
    _mm_set_ps((float)expf(buffer[0]), (float)expf(buffer[1]), (float)expf(buffer[2]), (float)expf(buffer[3]));	\
})
//    _mm_set_ps((float)powf(buffer[0], power), (float)powf(buffer[1], power), (float)powf(buffer[2], power), (float)powf(buffer[3], power)); 
#define VECTOR_FLOAT_POWER(vec_input, power) ({ \
    float buffer[VECTOR_FLOAT_ITER_SIZE]; \
    _mm_store_ps(buffer, vec_input); \
    _mm_set_ps(powf(buffer[0], power), powf(buffer[1], power), powf(buffer[2], power), powf(buffer[3], power)); \
})
#define VECTOR_FLOAT_POWER_abs(vec_input, power) ({VECTOR_FLOAT_POWER(VECTOR_FLOAT_abs(vec_input), power);}) //! where latter is used to 'handle' negative "scalar" values (oekseth, 06. mar. 2017)
//! @return the power for a 'case' where the first arugment is a scalar while the second argument is an SSE-vector (oekseth, 06. oktj. 2016).
#define VECTOR_FLOAT_POWER_arg1Scalar_arg2Vec(scalar, vec_power) ({ \
    union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_power; \
    _mm_set_ps(powf(scalar, v.buffer[0]), powf(scalar, v.buffer[1]), powf(scalar, v.buffer[2]), powf(scalar, v.buffer[3])); })

//    _mm_set_pd(log(buffer[0]), log(buffer[1]);			
//#define VECTOR_FLOAT_LOG(vec_input) ({double buffer[VECTOR_FLOAT_ITER_SIZE]; _mm_store_pd(buffer, vec_input); _mm_set_pd(log(buffer[0]), log(buffer[1]))} 

//! ------------------------------
//! Sopecify semi-complex macros:

//! For the dobule data-type specify the row-vector:
//! Note: the number of arguments i [below] reflects VECTOR_FLOAT_ITER_SIZE
// FIXME[jc]: would it go faster if we used _mm_load1_ps(..) <-- ?? .... and is there any other magics/trickery? .... and what is the difference wrt. _mm_stream_ps(..) and "_mm_store_ss(..)" ??
// FIXME[jc]: ask jan-chrsitan wrt. the difference between "_mm_store_ps(..)" and "_mm_storeu_ps(..)" .... ie, the implciaton/'requiremnt' of "mem_addr must be aligned on a 16-byte boundary or a general-protection exception may be generated" ["https://software.intel.com/sites/landingpage/IntrinsicsGuide/#expand=10,13,3152,583,0,0,5126,5174&text=_mm_store"] ... and why the latter error is thrown in valgrind ... <--- any stratgies to 'oversecome this'?
#define VECTOR_FLOAT_setFrom_columns(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_ps(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed], matrix[rowPos_dynamic+2][columnPos_fixed], matrix[rowPos_dynamic+3][columnPos_fixed]))


//! Specify a vector wrt. the index-range.
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]?
#define VECTOR_FLOAT_setFrom_indexRange(index) (_mm_set_ps((t_float)(index+0), (t_float)(index+1), (t_float)(index+2), (t_float)(index+3)))

//! Specify a vector wrt. the scalar
#define VECTOR_FLOAT_setFrom_scalar(scalar) (_mm_set1_ps(scalar))

/* //! Intiates a 'fixed-sized' buffer. */
/* #define VECTOR_INIT_BUFFER_EMPTY(buff) () */

//! Use a macro to handle the nan and 0 valeus wrt. "div"
// FIXME[jc]: may you udpate [”elow] to 'handle' "nan" and "0" (ie, if neccessary for correctness)?
#define VECTOR_FLOAT_storeAnd_div(result, vec_div1, vec_div2) (_mm_store_ps(result, _mm_div_ps(vec_div1, vec_div2)))
// FIXME[jc]: may you udpate [”elow] to 'handle' "nan" and "0" (ie, if neccessary for correctness)?
#define VECTOR_FLOAT_storeAnd_div_self(result, vec_div2) (_mm_store_ps(result, _mm_div_ps(_mm_load_ps(result), vec_div2)))

//#define VECTOR_FLOAT_storeAnd_div_self(result, vec_div2) 
/* #define VECTOR_FLOAT_storeAnd_div_self(result, vec_div2) (_mm_store_ps(result, vec_div2)) */
/* #define VECTOR_FLOAT_storeAnd_div_self_data(result, vec_div2) (_mm_store_ps(result, vec_div2)) */

//! Use a macro to stanarize the operation of incrmeneting a varialbe.
// FIXME[jc]: may you try to idneitfy a faster macro than [”elow]?
#define VECTOR_FLOAT_storeAnd_add(result, vec_add1) (_mm_store_ps(result, _mm_add_ps(_mm_load_ps(result), vec_add1)))
#define VECTOR_FLOAT_storeAnd_add_data(result, data_2) (_mm_store_ps(result, _mm_add_ps(_mm_load_ps(result), _mm_load_ps(data_2))))
#define VECTOR_FLOAT_storeAnd_min(result, vec_add1) (_mm_store_ps(result, _mm_min_ps(_mm_load_ps(result), vec_add1)))
#define VECTOR_FLOAT_loadAnd_min_vec_memRef(vec_min, mem_ref) (_mm_min_ps(vec_min, _mm_load_ps(mem_ref) ) )
#define VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_sum, mem_ref) (_mm_add_ps(vec_sum, _mm_load_ps(mem_ref) ) )
#define VECTOR_FLOAT_loadAnd_mult_vec_memRef(vec_sum, mem_ref) (_mm_mul_ps(vec_sum, _mm_load_ps(mem_ref) ) )
#define VECTOR_FLOAT_storeAnd_max(result, vec_add1) (_mm_store_ps(result, _mm_max_ps(_mm_load_ps(result), vec_add1)))
#define VECTOR_FLOAT_storeAnd_max_data(result, mem_addr) (VECTOR_FLOAT_storeAnd_max(result, _mm_load_ps(mem_addr)))



//assert(false) // add somethnng ... ie, get the compielr to 'fail'. <-- first c'ompelte this' when jan-crhistain has valdiated for the 'dobule' case.

// FIXME[jc]: this 
// FIXME[ok]: this 'macro' is not inlcuded int eh 'mien-version' ... ie, udpate the latter.
//! Use a char-mask to filter 
//! Note: the rmul1_Ref and rmul2_ref may be 'called' using: __m128 term1  = _mm_loadu_ps(&rmul1[j2]); __m128 term2  = _mm_loadu_ps(&rmul2[j2]);  
//! Note: teh "_mm_shuffle_ps(..)" is called iot revert the order <-- FIXEM[JC]: si this neccessar ... ie, why does the intrisnticitcs 'revert the order'?
// FIXME[JC]: try to optimzie [below] code. <-- ask jan-crhistain for suggestions.
//#define VECTOR_FLOAT_charMask(rmul1_ref, rmul2_ref, mask1, mask2) ({
// __m128 term1  = _mm_loadu_ps(rmul1_ref); __m128 term2  = _mm_loadu_ps(&rmul2_ref); 

// FIXME: write perofrmance-test to compare "VECTOR_FLOAT_charMask_pair(..)" and "VECTOR_FLOAT_charMask_pair_old(..)"
// FIXME[jc]: when [below] is correct, then update for our 'dobule' 'macro'.
#define VECTOR_FLOAT_charMask_pair(vec_mul, rmask1, rmask2, mask_index) ( VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64(vec_mul, VECTOR_CHAR_LOAD(&rmask1[mask_index]), VECTOR_CHAR_LOAD(&rmask2[mask_index]))) 
//! A permutaiton of our "VECTOR_FLOAT_charMask_pair(..)"
#define VECTOR_FLOAT_charMask_pair_old(vec_mul, rmask1, rmask2, mask_index) ({		\
const __m128 vec_mask1 = _mm_set_ps((float)rmask1[mask_index + 0], (float)rmask1[mask_index + 1], (float)rmask1[mask_index + 2], (float)rmask1[mask_index + 3]); \
const __m128 vec_mask2 = _mm_set_ps((float)rmask2[mask_index + 0], (float)rmask2[mask_index + 1], (float)rmask2[mask_index + 2], (float)rmask2[mask_index + 3]); \
__m128 mask = _mm_and_ps(vec_mask1, vec_mask2); \
  mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); \
_mm_mul_ps(mask, vec_mul); }) //! where the latter returns the result


//! Make use of FLT_MAX in data-data-comparisons, ie, muliply the mask with the copmuted distance: FLT_MAX is used to comptue the AND mask.
// FIXME[JC]: validate correctness of [below] based on [ªbove]
// FIXME[jc]: ask jan-christian to vlaidate correctness of [”elow] ... where intail correctnes-stests are seen/found int ehs tart of our evaluate_cmpOf_two_vectors_wrt_intrisinitstics() in our "graphAlgorithms_timeMeasurements_syntetic.cxx"
// FIXME: validate that callers of 'this' mrembers to sghuffle the result ... ie, wrtie correctness-tests.
// FIXME: write a performance-tests for [”elow] macro-atneraitves (oekseth, 06. sept. 2016) <-- may may 'these' be of novel itneret?
#define VECTOR_FLOAT_dataMask_pair(vec_mul, vec_term1, vec_term2) ({ \
      const __m128 vec_empty_empty = _mm_set1_ps(FLT_MAX); 		  const __m128 vec_empty_ones = _mm_set1_ps(1); \
      __m128 vec_cmp_1 = _mm_cmplt_ps(vec_term1, vec_empty_empty); vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones); \
      __m128 vec_cmp_2 = _mm_cmplt_ps(vec_term2, vec_empty_empty); vec_cmp_2 = _mm_and_ps(vec_cmp_2, vec_empty_ones); \
      __m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2); mask = _mm_mul_ps(mask, vec_mul);  \
      _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); }) //! where the latter returns the result

// FIXME[jc]: may you validate correctness of [below]?
#define VECTOR_FLOAT_charMask_data1(data_ref, rmask1, mask_index, vec_mask) ({	\
      __m128 vec_term1 = _mm_load_ps(data_ref);				\
      vec_mask = _mm_set_ps((float)rmask1[mask_index + 0], (float)rmask1[mask_index + 1], (float)rmask1[mask_index + 2], (float)rmask1[mask_index + 3]); \
      _mm_mul_ps(vec_mask, vec_term1); }) //! where the latter returns the result
#define VECTOR_FLOAT_charMask_data1_vec(vec_term1, rmask1, mask_index, vec_mask) ({	\
      vec_mask = _mm_set_ps((float)rmask1[mask_index + 0], (float)rmask1[mask_index + 1], (float)rmask1[mask_index + 2], (float)rmask1[mask_index + 3]); \
      _mm_mul_ps(vec_mask, vec_term1); }) //! where the latter returns the result
// FIXME[jc]: may you validate correctness of [below]?
#define VECTOR_FLOAT_charMask_data1_implit(data_ref, rmask1, mask_index) ({	\
      const __m128 vec_term1 = _mm_load_ps(data_ref);				\
      const __m128 vec_mask = VECTOR_FLOAT_convertFrom_CHAR_buffer(&rmask1[mask_index]); \
      _mm_mul_ps(vec_mask, vec_term1); }) //! where the latter returns the result
// FIXME[jc]: may you validate correctness of [below]?
#define VECTOR_FLOAT_charMask_data1_implit_old(data_ref, rmask1, mask_index) ({	\
      const __m128 vec_term1 = _mm_load_ps(data_ref);				\
      const __m128 vec_mask = _mm_set_ps((float)rmask1[mask_index + 0], (float)rmask1[mask_index + 1], (float)rmask1[mask_index + 2], (float)rmask1[mask_index + 3]); \
      _mm_mul_ps(vec_mask, vec_term1); }) //! where the latter returns the result

//! Use a data-filter, where we set to empty elements with FLT_MAX, ie, apply the FLT_MAX mask to a signle value.
//! @param <data_ref> is the ANSI C data-strcuture to be used, ie, "&data[k][j]" for a "float **data" structure/topology.

// FIXME[jc]}: may you validate [below]? <-- in this context may you valdiate correctness of the _mm_shuffle_ps(..) funciton-call?
//mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3));	
#define VECTOR_FLOAT_dataMask_data1(data_ref, mask) ({	\
      __m128 vec_term1 = _mm_load_ps(data_ref); \
      __m128 vec_cmp_1 = _mm_cmplt_ps(vec_term1, _mm_set1_ps(FLT_MAX)); vec_cmp_1 = _mm_and_ps(vec_cmp_1, _mm_set1_ps(1)); \
      mask = vec_cmp_1; \
      __m128 vec_mul = _mm_mul_ps(mask, vec_term1); \
vec_mul; }) //! where the latter returns the result
#define VECTOR_FLOAT_dataMask_data1_vec(vec_term1, mask) ({	\
      __m128 vec_cmp_1 = _mm_cmplt_ps(vec_term1, _mm_set1_ps(FLT_MAX)); vec_cmp_1 = _mm_and_ps(vec_cmp_1, _mm_set1_ps(1)); \
      mask = vec_cmp_1; \
      __m128 vec_mul = _mm_mul_ps(mask, vec_term1); \
vec_mul; }) //! where the latter returns the result
// FIXME[jc]}: may you validate [below]? <-- in this context may you valdiate correctness of the _mm_shuffle_ps(..) funciton-call?
#define VECTOR_FLOAT_dataMask_data1_maskImplicit(data_ref) ({__m128 mask; \
      VECTOR_FLOAT_dataMask_data1(data_ref, mask);}) //! where the latter returns the result
 

/* //! Similar to "VECTOR_FLOAT_dataMask_pair(..)" with differnece that we use "NAN" instead of "FLT_MAX" */
/* #define VECTOR_FLOAT_dataMask_NAN(vec_term1) ({ \ */
/*       __m128 vec_cmp_1 = _mm_cmpneq_ps(vec_term1, _mm_set1_ps(NAN)); vec_cmp_1 = _mm_and_ps(vec_cmp_1, _mm_set1_ps(1)); \ */
/*       __m128 mask = vec_cmp_1; \ */
/*       __m128 vec_mul = _mm_mul_ps(mask, vec_term1); \ */
/* vec_mul; }) //! where the latter returns the result */


//! @return a set of booleans stating if teh vector is above zero, eg, for for a docntiaonal "cmask_tmp > 0" foudn in our "graphAlgorithms_kCluster.cxx" "cluster.c" library.
// FIXME[jc]}: may you validate [below]? <-- in this context may you valdiate correctness of the _mm_shuffle_ps(..) funciton-call?
// _mm_shuffle_ps(vec_cmp_1, vec_cmp_1, _MM_SHUFFLE(0, 1, 2, 3)); 
#define VECTOR_FLOAT_getBoolean_aboveZero_vec(vec_term1) ({ \
      __m128 vec_cmp_1 = _mm_cmpgt_ps(vec_term1, _mm_set1_ps(0)); vec_cmp_1 = _mm_and_ps(vec_cmp_1, _mm_set1_ps(1)); \
      vec_cmp_1;}) //! where the latter returns the result
#define VECTOR_FLOAT_getBoolean_aboveZero(data_ref) ({VECTOR_FLOAT_getBoolean_aboveZero_vec(_mm_load_ps(data_ref));  }) //! where the latter returns the result

//! @return a set of booleans stating if teh vector is above zero, eg, for for a docntiaonal "cmask_tmp > 0" foudn in our "graphAlgorithms_kCluster.cxx" "cluster.c" library.
// FIXME[jc]}: may you validate [below]? <-- in this context may you valdiate correctness of the _mm_shuffle_epi8(..) funciton-call?
#define VECTOR_CHAR_getBoolean_aboveZero(data_ref) ({ \
      __m128i vec_term1 = _mm_loadu_si128(data_ref); \
      const __m128i vec_empty_empty = _mm_set1_epi8(0); 		  const __m128i vec_empty_ones = _mm_set1_epi8(1); \
      __m128i vec_cmp_1 = _mm_cmpgt_epi8(vec_term1, vec_empty_empty); vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones); \
      _mm_shuffle_epi8(vec_cmp_1, vec_cmp_1);  }) //! where the latter returns the result


//! Print the cotnents of a vector to stdout
// FIXME[jc]: wrt. [”elow] ... what is the difference when compared to "float *p = (float *)&vec_result";
#define VECTOR_FLOAT_PRINT(vec_result) ({float result[4] = MiC__float_resultBuffer_empty; _mm_store_ps(result, vec_result); printf("["); for(uint i = 0; i < 4; i++) {printf("%f, ", (float)result[i]);} fprintf(stdout, "], at %s:%d\n", __FILE__, __LINE__); })
#define VECTOR_FLOAT_PRINT_wMessage(msg, vec_result) ({float result[4] = MiC__float_resultBuffer_empty; _mm_store_ps(result, vec_result); printf("%s [", msg); for(uint i = 0; i < 4; i++) {printf("%f, ", (float)result[i]);} fprintf(stdout, "], at %s:%d\n", __FILE__, __LINE__); })
// #define VECTOR_FLOAT_PRINT(vec_result) ({float result[4] = MiC__float_resultBuffer_empty; _mm_store_ps(result, vec_result); for(uint i = 0; i < 4; i++) {printf("vec[%u] = %f, ", i, result[i]);} fprintf(stdout, "at %s:%d\n", __FILE__, __LINE__); })




//! Update a resutl-vector based on two inptu-values.    
//! @remarks describe logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
//! @remarks we call the "mask = _mm_mul_ps(mask, mask);" iot. to 'handle' the "-1" signs.
//! @remarks we make use of "_mm_mul_ps" as a 'pre-step' before "_mm_cmpgt_pi8" as there does not exist any opators named "_mm_cmpne_pi8(..)"
// FIXME[aritcle]: describe a generalized applciaton wrt. [”elow] 'template'.
#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64_xmtShuffle(vec_mul, vec_term1, vec_term2) ({ \
      __m64 vec_cmp_1 = _mm_cmpgt_pi8(_mm_mul_su32(vec_term1, vec_term1), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(_mm_mul_su32(vec_term2, vec_term2), _mm_set1_pi8(0)); \
      __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	\
      mask = _mm_mul_ps(mask, mask);  \
      __m128 vec_result = _mm_mul_ps(mask, vec_mul); \
      vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \
      vec_result;}) //! ie, return


//! Update a resutl-vector based on two inptu-values.    
//! @remarks describe logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
//! @remarks we call the "mask = _mm_mul_ps(mask, mask);" iot. to 'handle' the "-1" signs.
// FIXME[aritcle]: describe a generalized applciaton wrt. [”elow] 'template'.
#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64(vec_mul, vec_term1, vec_term2) ({ \
  __m128 vec_result = VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64_xmtShuffle(vec_mul, vec_term1, vec_term2); \
      vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \
      vec_result;}) //! ie, return


//! Update a resutl-vector based on two inptu-values.    
//! @remarks describe logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
//! @remarks we call the "mask = _mm_mul_ps(mask, mask);" iot. to 'handle' the "-1" signs.
//! @remarks we make use of "_mm_mul_ps" as a 'pre-step' before "_mm_cmpgt_pi8" as there does not exist any opators named "_mm_cmpne_pi8(..)"
// FIXME[aritcle]: describe a generalized applciaton wrt. [”elow] 'template'.
//
//#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(vec_mul, char_term1, char_term2) (VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64_xmtShuffle(vec_mul, VECTOR_CHAR_LOAD(char_term1), VECTOR_CHAR_LOAD(char_term2)))
#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_resultIsInverted(vec_mul, char_term1, char_term2) ({ \
    __m64 vec_1 = VECTOR_CHAR_LOAD(char_term1); __m64 vec_2 = VECTOR_CHAR_LOAD(char_term2); \
    /*VECTOR_FLOAT_PRINT_wMessage("vec_1: ", VECTOR_FLOAT_convertFrom_CHAR(vec_1));*/ \
    /* VECTOR_FLOAT_PRINT_wMessage("vec_2: ", VECTOR_FLOAT_convertFrom_CHAR(vec_2));*/ \
    VECTOR_CHAR_TYPE mask_char = _mm_and_si64(vec_1, vec_2); \
    VECTOR_FLOAT_TYPE mask =     VECTOR_FLOAT_convertFrom_CHAR(mask_char); \
    mask = VECTOR_FLOAT_revertOrder(mask); \
    /*VECTOR_FLOAT_PRINT_wMessage("mask: ", mask); */ \
    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_mul, mask); \
    vec_result;}) //! ie, return

#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(vec_mul, char_term1, char_term2) ({VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_resultIsInverted(vec_mul, char_term1, char_term2));})

#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_old(vec_mul, char_term1, char_term2) ({ \
  __m64 vec_1 = VECTOR_CHAR_LOAD(char_term1); __m64 vec_2 = VECTOR_CHAR_LOAD(char_term2); \
  __m64 vec_cmp_1 = _mm_cmpgt_pi8(_mm_mul_su32(vec_1, vec_1), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(_mm_mul_su32(vec_2, vec_2), _mm_set1_pi8(0)); \
      __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	\
      mask = _mm_mul_ps(mask, mask);  \
      mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); \
      __m128 vec_result = _mm_mul_ps(mask, vec_mul); \
      vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \
      vec_result;}) //! ie, return

/* #define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(vec_mul, char_term1, char_term2) ({ \ */
/*       __m64 vec_cmp_1 = _mm_cmpgt_pi8(VECTOR_CHAR_LOAD(char_term1), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(VECTOR_CHAR_LOAD(char_term2), _mm_set1_pi8(0)); \ */
/*       __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	\ */
/*       mask = _mm_mul_ps(mask, mask);  \ */
/*       __m128 vec_result = _mm_mul_ps(mask, vec_mul); \ */
/*       vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \ */
/*       vec_result;}) //! ie, return */



//! Update a resutl-vector based on two inptu-values.    
//! @remarks describe logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
//! @remarks we call the "mask = _mm_mul_ps(mask, mask);" iot. to 'handle' the "-1" signs.
//! @remarks we make use of "_mm_mul_ps" as a 'pre-step' before "_mm_cmpgt_pi8" as there does not exist any opators named "_mm_cmpne_pi8(..)"
// FIXME[aritcle]: describe a generalized applciaton wrt. [”elow] 'template'.
// vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); 
#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0(vec_mul, vec_term1, vec_term2) ({ \
      __m64 vec_cmp_1 = _mm_cmpgt_pi8(_mm_cvtps_pi8(_mm_mul_ps(vec_term1, vec_term2)), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(_mm_cvtps_pi8(_mm_mul_ps(vec_term2, vec_term2)), _mm_set1_pi8(0)); \
      __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	\
      mask = _mm_mul_ps(mask, mask);  \
      __m128 vec_result = _mm_mul_ps(mask, vec_mul); \
      vec_result;}) //! ie, return



//! Call a function seperately for each value (of the indexes) in the input-vector.
//! @return the vector which is the result of the values mapped (ie, 'applied') to the function.
//! @remarks demonstrates the applicacbailbity of a "map" function wrt. macros.
// FIXME: wrt. [”elow] write a performacne-test where we vvalaute the time-cost of an 'itnermeidate' fuction-call 'in covnerison' ... eg, to describe ...??..
// FIXME: in [”elow] validate that it is always correct to use "VECTOR_FLOAT_revertOrder(..)" <-- first 'discovered' when writing correctnes-tests for our "mine.c" impelmetnation.
#define VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec, funcRef_float) ({ \
      union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec;				\
      VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_SET(funcRef_float(v.buffer[0]), funcRef_float(v.buffer[1]), funcRef_float(v.buffer[2]), funcRef_float(v.buffer[3]))); }) //! ie, return.



//! **************************************************************************
#else
//! Define the type, ie, to simplify our allcoations.
typedef double t_float;
//! Change the sign of a float (oekseth, 06. des. 2016).
// FIXME[JC]: may you try to write an imrpvoed code wr.t 'sign changing' (oekseth, 06. des. 2016).
#define mathLib_float_changeSign(scalar) ({scalar *-1;})
//! @return the lgoarithm for an 8-byte float:
#define mathLib_float_log(inp_scalar) ({log(inp_scalar);})
//! Comptue the lgoarighm for both postivie and neagitve numbers, ie, by first 'taking' teh absoltue value (oekseth, 06. des. 2016).
#define mathLib_float_log_abs(inp_scalar) ({mathLib_float_log(mathLib_float_abs(inp_scalar));})
//! @return the lgoarithm for an 8-byte float:
#define mathLib_float_2_log(inp_scalar) ({VECTOR___const__log2_inv*log(inp_scalar);})
//! @return the lgoarithm for an 8-byte float:
#define mathLib_float_log_2(inp_scalar) ({VECTOR___const__log2_inv*log(inp_scalar);})
//! Comptue the lgoarighm for both postivie and neagitve numbers, ie, by first 'taking' teh absoltue value (oekseth, 06. des. 2016).
#define mathLib_float_log_2_abs(inp_scalar) ({VECTOR___const__log2_inv*mathLib_float_log(mathLib_float_abs(inp_scalar));})
//! @return the arc-tangent for an 8-byte float:
#define mathLib_float_atan(scalar) ({atan(scalar);})
//! @return the  for an 8-byte float:
#define mathLib_float_sin(scalar) ({sin(scalar);})
//! @return the  for an 8-byte float:
#define mathLib_float_cos(scalar) ({cos(scalar);})
//! @return the  for an 8-byte float:
#define mathLib_float_cosh(scalar) ({cosh(scalar);})
//! @return the  for an 8-byte float:
#define mathLib_float_tanh(scalar) ({tanh(scalar);})
#define mathLib_float_asin(scalar) ({asin(scalar);})
#define mathLib_float_acos(scalar) ({acos(scalar);})
//#define mathLib_float_(scalar) ({(scalar);})
#define mathLib_float_atan2(scalar1, scalar2) ({atan2(scalar1, scalar2);})
//! @return the square-root for an 8-byte float:
#define mathLib_float_sqrt(scalar) ({sqrt(scalar);})
#define mathLib_float_sqrt_abs(scalar) ({sqrt(mathLib_float_abs(scalar));})
#define mathLib_float_mod(v1, v2) ({fmod(v1, v2);})
//! @return teh absoltue values for an 8-byte float:
#define mathLib_float_abs(scalar) ({fabs(scalar);})
//! @return teh exponnent value for an 8-byte float:
#define mathLib_float_exp(scalar) ({exp(scalar);})
//! @return the 'power(..)' for an 8-byte float:
#define mathLib_float_pow(inp_scalar, inp_power) ({ \
  assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(scalar >= 0); /*ie, as 'otherwise' would result in "NAN" values (oekseth, 06. mar. 2017) */  \
  pow(inp_scalar, inp_power);})
#define mathLib_float_pow_abs(scalar, value_power) ({mathLib_float_pow(mathLib_float_abs(scalar), value_power);}) //! where latter is used to 'handle' negative "scalar" values (oekseth, 06. mar. 2017)
//! @returnt the floor-value
#define mathLib_float_floor(scalar) ({floor(scalar);})
//! @return the ceil-value
#define mathLib_float_ceil(scalar) ({ceil(scalar);})

#define VECTOR_FLOAT_converTo_int_32 _mm_cvttpd_epi32 /* ie, convert from float to 32-bit int*/
#define VECTOR_FLOAT_converTo_int_16 _mm_cvttpd_epi16 /* ie, convert from float to 32-bit int*/

#define T_FLOAT_MIN  DBL_MIN
#define T_FLOAT_MAX  DBL_MAX
#define T_FLOAT_MIN_ABS  -1*DBL_MAX
//! Udpate wrt. the itnrisitnistics:
#define VECTOR_FLOAT_ITER_SIZE 2
//! --- data-access
#define VECTOR_FLOAT_TYPE __m128d
#define VECTOR_FLOAT_SET _mm_set_pd
#define VECTOR_FLOAT_SET1 _mm_set1_pd
#define VECTOR_FLOAT_SET_zero _mm_setzero_pd
#define VECTOR_FLOAT_SET1_SINGLE _mm_set_sd
#define VECTOR_FLOAT_LOAD _mm_load_pd
#define VECTOR_FLOAT_LOADU _mm_loadu_pd
#define VECTOR_FLOAT_STORE _mm_store_pd
//! --- simple aritmetics
#define VECTOR_FLOAT_MUL _mm_mul_pd
#define VECTOR_FLOAT_RCP _mm_rcp_pd //! which compute "1/value"
#define VECTOR_FLOAT_DIV _mm_div_pd
//! ---
#define VECTOR_FLOAT_ADD _mm_add_pd
// FIXME: validate that the result of "VECTOR_FLOAT_ADD_SINGLE" correspdosnt ot he callers expecations.
#define VECTOR_FLOAT_ADD_SINGLE _mm_add_sd
// FIXME: validate that the result of "VECTOR_FLOAT_SUB_SINGLE" correspdosnt ot he callers expecations.
#define VECTOR_FLOAT_SUB_SINGLE _mm_sub_sd
#define VECTOR_FLOAT_SUB _mm_sub_pd
//! --- boolean lgoics, logics which are used to 'consturct' if-else clauses
#define VECTOR_FLOAT_AND _mm_and_pd
#define VECTOR_FLOAT_AND _mm_and_pd
#define VECTOR_FLOAT_ANDNOT _mm_andnot_pd
#define VECTOR_FLOAT_OR _mm_or_pd
#define VECTOR_FLOAT_XOR _mm_xor_pd
//! Logics for comparison:
#define VECTOR_FLOAT_CMPEQ _mm_cmpeq_pd
#define VECTOR_FLOAT_CMPLT _mm_cmplt_pd
#define VECTOR_FLOAT_CMPGT _mm_cmpgt_pd
#define VECTOR_FLOAT_CMPNEQ _mm_cmpneq_pd
#define VECTOR_FLOAT_SQRT _mm_sqrt_pd
#define VECTOR_FLOAT_SQRT_abs(vec) ({ _mm_sqrt_pd(VECTOR_FLOAT_abs(vec));})
//! --- comparisons
#define VECTOR_FLOAT_MIN _mm_min_pd
#define VECTOR_FLOAT_MAX _mm_max_pd
//! Nan-filtering
#define VECTOR_FLOAT_CMPORD _mm_cmpord_pd
#define VECTOR_FLOAT_CMPUNORD _mm_cmpunord_pd //! ie, 'neither' (oekseth, 06. setp. 2016)
//
//! --- conversions
#define VECTOR_FLOAT_convertFrom_epi32 _mm_cvtepi32_pd


//! Revertes the roder of a vector.
#define VECTOR_FLOAT_revertOrder(vec) (_mm_shuffle_pd(vec, vec, _MM_SHUFFLE(0, 1)))
//! Convert a float to an 8-bit intrisinistic char-vector.
//! Note: demonstrate how an 32-bit floatign-point-array may be converted into a char-array
#define VECTOR_CHAR_convertFrom_float_vector(vec) (_mm_cvtpd_pi8(vec))
//! A fucntion which is used to handle diffenret data-type-inptus, eg, "short int"
#define VECTOR_FLOAT_convertFrom_data(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+0], (t_float)buffer[index+1]);})
#define VECTOR_FLOAT_convertFrom_data_notInv(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+1], (t_float)buffer[index+0]);})

#define VECTOR_FLOAT_boolean_scalar_isNAN(val) (isnan(val))/*  \ */
#define VECTOR_FLOAT_convertFrom_CHAR_buffer__xmtCharOverFlow(mem_ref) (VECTOR_FLOAT_SET(mem_ref[0], mem_ref[1])) //! where latter is sued as an alnteriate to "VECTOR_FLOAT_convertFrom_CHAR_buffer_withOverflow(..)" to ensure 'comaptiblity' witht he "VECTOR_FLOAT_ITER_SIZE", ie, to avoid 'access to un-asigned valeus' wrt. the char-buffer (When used in anttieraiotn with 'flaots', as expected wrt. this macro) (oekseth, 06. mar. 2017)
#define VECTOR_FLOAT_convertFrom_CHAR_buffer(mem_ref) (VECTOR_FLOAT_convertFrom_CHAR(VECTOR_CHAR_LOAD(mem_ref)))

  /* assert(sizeof(t_float) == sizeof(int));  /\* FIXME: remove this to ...??...  *\/ \ */
//float scalar = NAN;
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_INT(vec) ({ \
  int buffer[VECTOR_FLOAT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec); \
VECTOR_FLOAT_SET((t_float)buffer[0], (t_float)buffer[1]); \
})
//#define VECTOR_FLOAT_convertFrom_INT _mm_castsi128_pd

// FIXME[jc]: seems like we do nto manage to compiel/'use the _mm_load_pd(..) ... ie, any suggestions 'for how getting this to work'?
//! @return the log of teh function
#define VECTOR_FLOAT_LOG(vec_input) ({ \
    double buffer[VECTOR_FLOAT_ITER_SIZE]; \
    _mm_store_pd(buffer, vec_input); \
    _mm_set_pd((double)log(buffer[0]), (double)log(buffer[1]));	\
})
#define VECTOR_FLOAT_EXP(vec_input) ({ \
    double buffer[VECTOR_FLOAT_ITER_SIZE]; \
    _mm_store_pd(buffer, vec_input); \
    _mm_set_pd((double)exp(buffer[0]), (double)exp(buffer[1])); }) //! ie, return
#define VECTOR_FLOAT_POWER(vec_input, power) ({ \
    float buffer[VECTOR_FLOAT_ITER_SIZE]; \
    _mm_store_pd(buffer, vec_input); \
    _mm_set_pd((double)mathLib_float_pow(buffer[0], power), (float)mathLib_float_pow(buffer[1], power)); \
})
#define VECTOR_FLOAT_POWER_abs(vec_input, power) ({VECTOR_FLOAT_POWER(VECTOR_FLOAT_abs(vec_input), power);}) //! where latter is used to 'handle' negative "scalar" values (oekseth, 06. mar. 2017)
//! @return the power for a 'case' where the first arugment is a scalar while the second argument is an SSE-vector (oekseth, 06. oktj. 2016).
#define VECTOR_FLOAT_POWER_arg1Scalar_arg2Vec(scalar, vec_power) ({ \
    union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_power; \
    _mm_set_pd(mathLib_float_pow_abs(scalar, v.buffer[0]), mathLib_float_pow_abs(scalar, v.buffer[1])); })
//    _mm_set_pd(log(buffer[0]), log(buffer[1]);			
//#define VECTOR_FLOAT_LOG(vec_input) ({double buffer[VECTOR_FLOAT_ITER_SIZE]; _mm_store_pd(buffer, vec_input); _mm_set_pd(log(buffer[0]), log(buffer[1]))} 

//! ------------------------------
//! Sopecify semi-complex macros:

//! For the dobule data-type specify the row-vector:
//! Note: the number of arguments i [below] reflects VECTOR_FLOAT_ITER_SIZE
#define VECTOR_FLOAT_setFrom_columns(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_pd(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed]))

//! Specify a vector wrt. the index-range.
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]?
#define VECTOR_FLOAT_setFrom_indexRange(index) (_mm_set_pd((t_float)(index+0), (t_float)(index+1)))

//! Specify a vector wrt. the scalar
// FIXME: in [below] consider to instead use _mm_set1_pd(scalar)
#define VECTOR_FLOAT_setFrom_scalar(scalar) (_mm_set_pd(scalar, scalar))

/* //! Intiates a 'fixed-sized' buffer. */
/* #define VECTOR_INIT_BUFFER_EMPTY(buff) () */

//! Use a macro to handle the nan and 0 valeus wrt. "div"
// FIXME[jc]: may you udpate [”elow] to 'handle' "nan" and "0" (ie, if neccessary for correctness)?
#define VECTOR_FLOAT_storeAnd_div(result, vec_div1, vec_div2) (_mm_store_pd(result, _mm_div_pd(vec_div1, vec_div2)))
// FIXME[jc]: may you udpate [”elow] to 'handle' "nan" and "0" (ie, if neccessary for correctness)?
#define VECTOR_FLOAT_storeAnd_div_self(result, vec_div2) (_mm_store_pd(result, _mm_div_pd(_mm_load_pd(result), vec_div2)))

//! Convert from an 8-bit "__m64" to an 64-bit "__mm128d"
//! Note: demonstrate how an 8-bit mask-arary may be loaded into a flatoign-potin-array.
#define VECTOR_FLOAT_convertFrom_CHAR(vec) (_mm_cvtpi8_pd(vec))



//! Call a function seperately for each value (of the indexes) in the input-vector.
//! @return the vector which is the result of the values mapped (ie, 'applied') to the function.
//! @remarks demonstrates the applicacbailbity of a "map" function wrt. macros.
// FIXME: wrt. [”elow] write a performacne-test where we vvalaute the time-cost of an 'itnermeidate' fuction-call 'in covnerison' ... eg, to describe ...??..
#define VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec, funcRef_float) ({ \
      union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec;				\
      VECTOR_FLOAT_SET(funcRef_float(v.buffer[0]), funcRef_float(v.buffer[1])); }) //! ie, return.




// #define VECTOR_FLOAT_

//! **************************************************************************
#endif


//! @return the interation-size with offset:
#define macro__get_maxIntriLength_float(length) ( (length > VECTOR_FLOAT_ITER_SIZE) ? (length - VECTOR_FLOAT_ITER_SIZE) : 0  )



//! @return the valeu at the assicated index
// FIXME: try to update our examples and oru macros to use [beloł] .. ie, to not alwyas make use of the buffer[4] 'code-strategy'
union UNION_VECTOR_FLOAT_TYPE { VECTOR_FLOAT_TYPE sse; t_float buffer[VECTOR_FLOAT_ITER_SIZE]; } ;
#define VECTOR_FLOAT_getAtIndex(vec, index) ({ union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec; v.buffer[index];})


//! @retunr the absoltue value of the flaoting-poitn vector.
#define VECTOR_FLOAT_abs(vec) ({VECTOR_FLOAT_MAX(VECTOR_FLOAT_SUB(VECTOR_FLOAT_SET_zero(), vec), vec);})



//! @remarks translated from the work of "https://gist.github.com/Novum/1200562": Fast SSE pow for range [0, 1]; Adapted from C. Schlick with one more iteration each for exp(x) and ln(x); 8 muls, 5 adds, 1 rcp
#define VECTOR_FLOAT_fastpow_0_1(vec_input, power) ({ \
  const VECTOR_FLOAT_TYPE vec_power = VECTOR_FLOAT_SET1(power); \
    const VECTOR_FLOAT_TYPE vec_one = VECTOR_FLOAT_SET1(1.0f);  const VECTOR_FLOAT_TYPE vec_half = VECTOR_FLOAT_SET1(0.5f); \
    const VECTOR_FLOAT_TYPE a = VECTOR_FLOAT_SUB(vec_one, vec_power);			\
    const VECTOR_FLOAT_TYPE b = VECTOR_FLOAT_SUB(vec_input, vec_one);			\
    const VECTOR_FLOAT_TYPE aSq = VECTOR_FLOAT_MUL(a, a);			\
    const VECTOR_FLOAT_TYPE bSq = VECTOR_FLOAT_MUL(b, b);			\
    const VECTOR_FLOAT_TYPE c = VECTOR_FLOAT_MUL(vec_half, bSq);		\
    const VECTOR_FLOAT_TYPE d = VECTOR_FLOAT_SUB(b, c);			\
    const VECTOR_FLOAT_TYPE dSq = VECTOR_FLOAT_MUL(d, d);			\
    const VECTOR_FLOAT_TYPE e = VECTOR_FLOAT_MUL(aSq, dSq);			\
    const VECTOR_FLOAT_TYPE f = VECTOR_FLOAT_MUL(a, d);			\
    const VECTOR_FLOAT_TYPE g = VECTOR_FLOAT_MUL(vec_half, e);		\
    const VECTOR_FLOAT_TYPE h = VECTOR_FLOAT_ADD(vec_one, f);			\
    const VECTOR_FLOAT_TYPE i = VECTOR_FLOAT_ADD(h, g);			\
    const VECTOR_FLOAT_TYPE iRcp = VECTOR_FLOAT_RCP(i);				\
    const VECTOR_FLOAT_TYPE result = VECTOR_FLOAT_MUL(vec_input, iRcp);		\
  /*! @return teh result: */ \
    result;})

// FIXME[jc]: is there a better approach to avodi the "NAN" value (ie, than converting to-and-from "char")? <-- if so, then update functions which makes use of "_mm_cmpeq_pi8(..)". 
#define VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(vec) ({ \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_convertFrom_CHAR(VECTOR_CHAR_convertFrom_float_vector(vec))); \
  VECTOR_FLOAT_MUL(vec_result, VECTOR_FLOAT_SET1(__constant_float_charMin));}) //! ie, return.


// #define VECTOR_FLOAT_converTo_int_32 _mm_cvttpd_epi32 /* ie, convert from float to 32-bit int*/
// #define VECTOR_FLOAT_converTo_int_16 _mm_cvttpd_epi16 /* ie, convert from float to 32-bit int*/


/* //! A generic procedure to convert floating-point-numbers into valeus, eg, from "32-bit float"  to "16-bit int" */
/* // FIXME[jc]: given the applicaiton/usage of the "VECTOR_FLOAT_convertTo_dataPoiner(..)" funciton ... do you manage to imrpove/udpate 'this funciotn wrt. performance'? */
/* #define VECTOR_FLOAT_convertTo_dataPoiner(mem_ref_result, index, vec_input, typeOf_result) ({ \ */
/*       union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_input; 		\ */
/*       for(uint i = 0; i < VECTOR_FLOAT_ITER_SIZE; i++) { \ */
/* 	mem_ref_result[index+i] = (typeOf_result)v.buffer[i];	 \ */
/* }}) */


//! A generic procedure to convert floating-point-numbers into valeus, eg, from "32-bit float"  to "16-bit int"
// FIXME[jc]: given the applicaiton/usage of the "VECTOR_FLOAT_convertTo_dataPoiner(..)" funciton ... do you manage to imrpove/udpate 'this funciotn wrt. performance'?
#define VECTOR_FLOAT_convertTo_dataPoiner(mem_ref_result, index, vec_input, typeOf_result) ({ \
      union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_input; 		\
      for(uint i = 0; i < VECTOR_FLOAT_ITER_SIZE; i++) { \
	mem_ref_result[index+i] = (typeOf_result)v.buffer[i];	 \
	/* printf("[%u]=[%u] = %f = %d, at %s:%d\n", index+i, i, v.buffer[i], (int)mem_ref_result[index+i], __FILE__, __LINE__); */ \
}})

//! Comarpe teh two results, eg, for use case "are valeus insdie the range [min, max]?" (oekseth, 06. sept. 2016)
//! Note: interesting in how the NAs are hadnled, ie, wher ethe latter is the compelxity:
//! @rerutn a boolean vector sdesciginb if "vec_value" is inside "[vec_min, vec_max]".
#define VECTOR_FLOAT_insideRange_notRevertOrder_notInvertNan(vec_value, vec_min, vec_max) ({ \
  VECTOR_FLOAT_TYPE vec_gt = VECTOR_FLOAT_CMPGT(vec_value, vec_min); \
  /*VECTOR_FLOAT_PRINT_wMessage("result-gt", vec_gt);*/		     \
  VECTOR_FLOAT_TYPE vec_lt = VECTOR_FLOAT_CMPLT(vec_value, vec_max); \
  /* VECTOR_FLOAT_PRINT_wMessage("result-lt", vec_lt); */	     \
  VECTOR_FLOAT_TYPE vec_eq = VECTOR_FLOAT_AND(vec_gt, vec_lt);	     \
  /* VECTOR_FLOAT_PRINT_wMessage("result-eq", vec_eq); */	     \
  /*vec_eq = VECTOR_FLOAT_AND(vec_eq, VECTOR_FLOAT_SET1(1)); */	     \
  vec_eq;})
#define VECTOR_FLOAT_insideRange_notRevertOrder(vec_value, vec_min, vec_max) ({VECTOR_FLOAT_AND(VECTOR_FLOAT_insideRange_notRevertOrder_notInvertNan(vec_value, vec_min, vec_max), VECTOR_FLOAT_SET1(1));}) 
//! Comarpe teh two results, eg, for use case "are valeus insdie the range [min, max]?" (oekseth, 06. sept. 2016)
//! Note: interesting in how the NAs are hadnled, ie, wher ethe latter is the compelxity:
//! @rerutn a boolean vector sdesciginb if "vec_value" is inside "[vec_min, vec_max]".
#define VECTOR_FLOAT_insideRange(vec_value, vec_min, vec_max) ({VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_insideRange_notRevertOrder(vec_value, vec_min, vec_max));})


#define VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_combined) ({ \
    VECTOR_FLOAT_TYPE vec_mask_eq_1 = VECTOR_FLOAT_CMPEQ(vec_input_1, VECTOR_FLOAT_SET1(T_FLOAT_MAX)); \
    VECTOR_FLOAT_TYPE vec_mask_eq_2 = VECTOR_FLOAT_CMPEQ(vec_input_2, VECTOR_FLOAT_SET1(T_FLOAT_MAX)); \
    vec_mask_combined = VECTOR_FLOAT_CMPORD(vec_mask_eq_1, vec_mask_eq_2); \
    /*! Apply the filters, ie, update: */ \
    vec_input_1 = VECTOR_FLOAT_AND(vec_mask_combined, vec_input_1); \
    vec_input_2 = VECTOR_FLOAT_AND(vec_mask_combined, vec_input_2); \
    })

#define VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCount(vec_input_1, vec_input_2) ({ \
      VECTOR_FLOAT_TYPE vec_mask_combined; VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_combined); \
      vec_mask_combined = VECTOR_FLOAT_AND(vec_mask_combined, VECTOR_FLOAT_SET1(1)); \
      vec_mask_combined;})

#define VECTOR_FLOAT_dataMask_implicit_updatePair(vec_input_1, vec_input_2) ({ VECTOR_FLOAT_TYPE vec_mask_combined; VECTOR_FLOAT_dataMask_implicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_combined);})


//! A 'slow-perofrmance' pertmaution of our "VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(..)"
//! @remarks is written andd inludced (int he main-heade-rfile) in order to demonstrate ana latenrive (and ion many cotnects mroe obvious/intutive) appraoch for application of mask-proeprties. 
#define VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted__slow(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2, vec_mask_combined) ({ \
      /*! First combine the masks: */					\
      vec_mask_combined = VECTOR_FLOAT_AND(vec_mask_1, vec_mask_2); \
      /*! Apply the filters, ie, update: */				\
      vec_input_1 = VECTOR_FLOAT_revertOrder(vec_input_1); \
      vec_input_2 = VECTOR_FLOAT_revertOrder(vec_input_2); \
      vec_input_1 = VECTOR_FLOAT_MUL(vec_input_1, vec_mask_combined); \
      vec_input_2 = VECTOR_FLOAT_MUL(vec_input_2, vec_mask_combined); \
    })

#define VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2, vec_mask_combined) ({ \
      /*! First combine the masks: */					\
      vec_mask_combined = VECTOR_FLOAT_AND(vec_mask_1, vec_mask_2); \
      /*! Apply the filters, ie, update: */				\
      VECTOR_FLOAT_TYPE vec_mask_gtZero = VECTOR_FLOAT_CMPEQ(vec_mask_combined, VECTOR_FLOAT_SET1(1)); \
      vec_input_1 = VECTOR_FLOAT_revertOrder(vec_input_1); \
      vec_input_2 = VECTOR_FLOAT_revertOrder(vec_input_2); \
      vec_input_1 = VECTOR_FLOAT_AND(vec_input_1, vec_mask_gtZero); \
      vec_input_2 = VECTOR_FLOAT_AND(vec_input_2, vec_mask_gtZero); \
    })

//! @return a boolean float-table holding the count of interesting values
#define VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2) ({ \
      VECTOR_FLOAT_TYPE vec_mask_combined; VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2, vec_mask_combined); \
      vec_mask_combined = VECTOR_FLOAT_AND(vec_mask_combined, VECTOR_FLOAT_SET1(1)); \
      vec_mask_combined;})
//! Update vec_input_1 and vec_input_2 based on the char-masks (set to eitehr "1" or "0") (oekseth, 06. sept. 2016).
//! @return a boolean float-table holding the count of interesting values
#define VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, char_mask_1, char_mask_2) ({ \
      VECTOR_FLOAT_TYPE vec_mask_1 = VECTOR_FLOAT_convertFrom_CHAR_buffer(char_mask_1); \
      VECTOR_FLOAT_TYPE vec_mask_2 = VECTOR_FLOAT_convertFrom_CHAR_buffer(char_mask_2); \
      VECTOR_FLOAT_TYPE vec_mask_combined; VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2, vec_mask_combined); \
      vec_mask_combined = VECTOR_FLOAT_AND(vec_mask_combined, VECTOR_FLOAT_SET1(1)); \
      vec_mask_combined;})
#define VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef_atIndex(vec_input_1, vec_input_2, char_mask_1, char_mask_2, index_charTable) ({ \
      VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCount_fromCharMaskRef(vec_input_1, vec_input_2, &char_mask_1[index_charTable], &char_mask_2[index_charTable]);})


#define VECTOR_FLOAT_dataMask_explicit_updatePair(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2) ({ VECTOR_FLOAT_TYPE vec_mask_combined; VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_combined);})
//! Update vec_input_1 and vec_input_2 based on the char-masks (set to eitehr "1" or "0") (oekseth, 06. sept. 2016).
#define VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, char_mask_1, char_mask_2) ({ \
  VECTOR_FLOAT_TYPE vec_mask_1 = VECTOR_FLOAT_convertFrom_CHAR_buffer(char_mask_1); \
  VECTOR_FLOAT_TYPE vec_mask_2 = VECTOR_FLOAT_convertFrom_CHAR_buffer(char_mask_2); \
  VECTOR_FLOAT_TYPE vec_mask_combined; VECTOR_FLOAT_dataMask_explicit_updatePair_and_getMaskCountInverted(vec_input_1, vec_input_2, vec_mask_1, vec_mask_2, vec_mask_combined);})
#define VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef_atIndex(vec_input_1, vec_input_2, char_mask_1, char_mask_2, index_charTable) ({ \
      VECTOR_FLOAT_dataMask_explicit_updatePair_fromCharMaskRef(vec_input_1, vec_input_2, &char_mask_1[index_charTable], &char_mask_2[index_charTable]);})
//! @brief @return an updated result-vector: a  vector replacing NAN values with '0': for general archictectures we expect a latency of "4": whiole the "CMP" has a latency of "3" the "AND" operator has a latency of "1"; is an optmization when compared to muliplcation with a ltency of 5
#define VECTOR_FLOAT_dataMask_replaceNAN_by_Zero(vec_result) ({ \
  VECTOR_FLOAT_TYPE vec_cmpMIN = VECTOR_FLOAT_CMPGT(vec_result, VECTOR_FLOAT_SET1(T_FLOAT_MIN_ABS)); \
  VECTOR_FLOAT_AND(vec_result, vec_cmpMIN);}) //! ie, return the updated vecotr with "0"'is instead of NAN's.

//! @return a vector where the INFINITY valeus are replaced by "0" (oekseth, 06. nvo. 2016).
#define VECTOR_FLOAT_dataMask_replaceINF_by_Zero(vec_result) ({ \
    VECTOR_FLOAT_TYPE vec_cmpMIN = VECTOR_FLOAT_CMPLT(vec_result, VECTOR_FLOAT_SET1(T_FLOAT_MAX)); \
    VECTOR_FLOAT_AND(vec_result, vec_cmpMIN); }) //! ie, return.

//! Set replce valeus assicated to a 'maks' with an 'empty value (eg, "T_FLOAT_MAX") (oekseth, 06. sept. 2016).
#define VECTOR_FLOAT_func_maskTransform_explcitToImplicit_notRevertOrder(vec_input, vec_mask, vec_spec_emptyValue) ({ \
      VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_input, vec_mask);		\
      /* VECTOR_FLOAT_PRINT_wMessage("result-1", vec_result); */	\
      /*Invert the mask:*/ \
      VECTOR_FLOAT_TYPE vec_maskInverted = VECTOR_FLOAT_ANDNOT(vec_mask, VECTOR_FLOAT_SET1(1)); \
      /*VECTOR_FLOAT_PRINT_wMessage("vec-mask-inverted", vec_maskInverted); */ \
      vec_maskInverted = VECTOR_FLOAT_MUL(vec_maskInverted, vec_spec_emptyValue); \
      /*vec_result = VECTOR_FLOAT_XOR(vec_result, vec_spec_emptyValue);*/ \
      vec_result = VECTOR_FLOAT_ADD(vec_result, vec_maskInverted);	\
      /*VECTOR_FLOAT_PRINT_wMessage("result-2", vec_result);*/		\
      vec_result;}) //! ie, return

//! Set replce valeus assicated to a 'maks' with an 'empty value (eg, "T_FLOAT_MAX") (oekseth, 06. sept. 2016).
#define VECTOR_FLOAT_func_maskTransform_explcitToImplicit(vec_input, vec_mask, vec_spec_emptyValue) ({VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_func_maskTransform_explcitToImplicit_notRevertOrder(vec_input, vec_mask, vec_spec_emptyValue));})


//! replace implict masks with a new mask-value (oekseth, 06. sept. 2016).
#define VECTOR_FLOAT_func_maskTransform_implicitMask_notRevertOrder(vec_input, vec_specEmptyValue_inInput, vec_spec_emptyValue) ({ \
	VECTOR_FLOAT_TYPE vec_mask = VECTOR_FLOAT_CMPNEQ(vec_input, vec_specEmptyValue_inInput); \
	if(false) {VECTOR_FLOAT_PRINT_wMessage("vec-mask: ", vec_mask);} \
	vec_mask = VECTOR_FLOAT_MAX(vec_mask, VECTOR_FLOAT_SET1(-1)); \
	/*vec_mask = _mm_sub_ps(_mm_set1_ps(0.0), vec_mask); */ \
	if(false) {VECTOR_FLOAT_PRINT_wMessage("vec-mask: ", vec_mask);} \
	/*! Invert the sign: flip the bits: */ \
	vec_mask = VECTOR_FLOAT_XOR(vec_mask, VECTOR_FLOAT_SET1(-0.0)); \
	/*! Comptue the result: */ \
	VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_func_maskTransform_explcitToImplicit_notRevertOrder(vec_input, vec_mask, vec_spec_emptyValue); \
	/*if(false) {VECTOR_FLOAT_PRINT_wMessage("vec-mask: ", vec_mask);} */ \
	vec_result;})

#define VECTOR_FLOAT_func_maskTransform_implicitMask(vec_input, vec_specEmptyValue_inInput, vec_spec_emptyValue) ({ VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_func_maskTransform_implicitMask_notRevertOrder(vec_input, vec_specEmptyValue_inInput, vec_spec_emptyValue));})

//! Handle the "nan" case arising from a "0/value" and "value/0" case, ie, by replacing each "nan" with "0"
//! Note: idea is to use/apply an explicit mask-proeprty: if(mask[i]) { /* then some action, ie, a set of instructions */ } 
//! Note: if this code-block we examplify how 'boolean type-covnerison' from float may be used to ...??..
//! Note: conseptually we in [”elow] macro compute "div-result - mask*nan". However, as "nan-nan = nan", the latter approach is meaningless. To overcome the latter we therefore first replace all '0's with 'an rbitrary value', adn then 'clear all vectors witht eh arbitrary value' after the comptuation.
//! Note: in this procedure the followin steps are taken/followed: step(1) Replace '0' with a different 'arbirary' value; step(2) Compute the div-result; step(3) Remove the values which were inferred to be '0' .... ie, step(1) identify the vector-items with "value == 0", ie, a 'boolean mask'; step(2) replace all occurences of "0" with 'a different value' (eg, "xor"); step(3) compute "div"; step(4) multiply the result with the 'boolean mask'.
// FIXME[tutorial] ... sems like [”elow] exmaple is missing from both stack-voerflow posts and course-slides (wrt. presentaitons held at universities around the wolrd) ... 
#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(vec_term1, vec_term2, funcRef_float) ({ \
  VECTOR_FLOAT_TYPE vec_cmpNonZero_1 = VECTOR_FLOAT_CMPEQ(vec_term1, VECTOR_FLOAT_SET1(0)); \
  /* VECTOR_FLOAT_PRINT_wMessage("vec non-zero-1", vec_cmpNonZero_1); */ \
  VECTOR_FLOAT_TYPE vec_cmpNonZero_2 = VECTOR_FLOAT_CMPEQ(vec_term2, VECTOR_FLOAT_SET1(0)); \
  /* VECTOR_FLOAT_PRINT_wMessage("vec non-zero-2", vec_cmpNonZero_1); */ \
  /*! Step(3): Find all values which are zero: */ \
  VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_OR(vec_cmpNonZero_1, vec_cmpNonZero_2); \
  /* VECTOR_FLOAT_PRINT_wMessage("vec-notZero", mask); */ \
  /*! Note: we need to use the 'ant-not' operatnd to 'clear' the NAN-valeus: if the latter had nott bene the case/issue, then we instead coudl have 'ivnerted' above lgocis.*/ \
  mask = VECTOR_FLOAT_ANDNOT(mask, VECTOR_FLOAT_SET1(1)); \
  /*! Step(4): 'revert' the zeros: */ \
  /* VECTOR_FLOAT_PRINT_wMessage("vec-notZero and 1: ", vec_notZero); */ \
  /* Comptue the scores: */ \
  VECTOR_FLOAT_TYPE vec_result = funcRef_float(vec_term1, vec_term2); \
  VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
  vec_result = vec_subtract; /*VECTOR_FLOAT_SUB(vec_result, vec_subtract);*/ \
  VECTOR_FLOAT_revertOrder(vec_result);}) //! ie, return.
//! Similar to "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(..)", with difference that we here only evaluate for one input-vector.
//	VECTOR_FLOAT_TYPE vec_result = 
//
//#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(vec_term1) ({ 
#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(vec_term1, funcRef_float_1d) ({ \
  VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_convertFrom_CHAR(_mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), VECTOR_CHAR_SET1(0))); \
  mask = VECTOR_FLOAT_MUL(mask, mask); \
  const VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, mask); \
  VECTOR_FLOAT_TYPE vec_result = funcRef_float_1d(vec_altValue_1);	\
  vec_result = VECTOR_FLOAT_revertOrder(vec_result);			\
  const VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
  vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract); }) //! ie, return.


#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_1(vec_term1, vec_term2, funcRef_float) ({ \
      const __m64 vec_cmp_1 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), _mm_set1_pi8(0)); \
      const __m64 vec_cmp_2 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term2), _mm_set1_pi8(0)); \
      VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_convertFrom_CHAR(_mm_or_si64(vec_cmp_1, vec_cmp_2)); mask = _mm_mul_ps(mask, mask); \
	VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, mask); \
	VECTOR_FLOAT_TYPE vec_altValue_2 = VECTOR_FLOAT_ADD(vec_term2, mask); \
	VECTOR_FLOAT_TYPE vec_result = funcRef_float(vec_altValue_1, vec_altValue_2); \
	VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
 	vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract); \
	VECTOR_FLOAT_revertOrder(vec_result);}) //! ie, return.

//! Note: in contraost to "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(..)" below 'introduces an unnceccesary ovrheead' wrt. "VECTOR_FLOAT_MUL(mask, VECTOR_FLOAT_SET1(FLT_MAX))" (where the latter was foudn in our first correct/wroking version fo thsi macro-function) (oekseth, 06. june 2016).
#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_2(vec_term1, vec_term2, funcRef_float) ({ \
	const __m64 vec_cmp_1 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), _mm_set1_pi8(0)); \
	const __m64 vec_cmp_2 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term2), _mm_set1_pi8(0)); \
	VECTOR_FLOAT_TYPE mask = _mm_cvtpi8_ps(_mm_or_si64(vec_cmp_1, vec_cmp_2)); mask = _mm_mul_ps(mask, mask); \
	VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, VECTOR_FLOAT_MUL(mask, VECTOR_FLOAT_SET1(FLT_MAX))); \
	VECTOR_FLOAT_TYPE vec_altValue_2 = VECTOR_FLOAT_ADD(vec_term2, VECTOR_FLOAT_MUL(mask, VECTOR_FLOAT_SET1(FLT_MAX))); \
	VECTOR_FLOAT_TYPE vec_result = funcRef_float(vec_altValue_1, vec_altValue_2); \
	VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
 	vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract); \
	VECTOR_FLOAT_revertOrder(vec_result);}) //! ie, return.


#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d_old_1(vec_term1, funcRef_float_1d) ({ \
  VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_convertFrom_CHAR(_mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), VECTOR_CHAR_SET1(0))); \
  mask = VECTOR_FLOAT_MUL(mask, mask); \
  const VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, mask); \
  VECTOR_FLOAT_TYPE vec_result = funcRef_float_1d(vec_altValue_1);	\
  vec_result = VECTOR_FLOAT_revertOrder(vec_result);			\
  const VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
  vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract); }) //! ie, return.



//! Divide the two numbers, seeting 'divide by zero' case to '0'.
#define VECTOR_FLOAT_omitZeros_fromComputation_DIV(vec_term1, vec_term2) ({ \
  /*! Merge the input-terms: */ \
  VECTOR_FLOAT_TYPE mask = vec_term2; \
  mask = VECTOR_FLOAT_CMPNEQ(mask, VECTOR_FLOAT_SET1(0));	\
  /*! Clear the NAN signs: */					\
  mask = VECTOR_FLOAT_AND(mask, VECTOR_FLOAT_SET1(1));		\
  /*! In order to avoid nan-cases each '0' valeu need to be set to 1: */ \
  VECTOR_FLOAT_TYPE mask_inv = VECTOR_FLOAT_ANDNOT(mask, VECTOR_FLOAT_SET1(1)); \
  /*! Increment 'zero-valeus': */					\
  VECTOR_FLOAT_TYPE vec_term2_wMask = VECTOR_FLOAT_ADD(vec_term2, mask_inv); \
  /*! Set to 'zero' the valeus which are to be empty: */		\
  VECTOR_FLOAT_TYPE vec_1_mask = VECTOR_FLOAT_AND(VECTOR_FLOAT_SET1(1), mask); \
  /*! Apply the mask to term2: */					\
  VECTOR_FLOAT_TYPE vec_divBy_term2 = VECTOR_FLOAT_DIV(vec_1_mask, vec_term2_wMask); \
  /*VECTOR_FLOAT_PRINT_wMessage("1/term2: ", vec_divBy_term2);	*/	\
  /*! Multiply the tersult with term1: */				\
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_term1, vec_divBy_term2); \
  vec_result; })

#define VECTOR_FLOAT_omitZeros_fromComputation_DIV_old_2(vec_term1, vec_term2) ({VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(vec_term1, vec_term2, VECTOR_FLOAT_DIV);})

//! COmpute shannons etnropy:
// FIXME[jc]: validate our [”elow] "VECTOR_FLOAT_convertFrom_INT(..)" 'proceudre'
#define VECTOR_MATH_shannonEntropy_float(result_tmp, vec_divResult) ({ \
  VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_MUL(vec_divResult, VECTOR_FLOAT_LOG(vec_divResult)); \
  VECTOR_FLOAT_STORE(result_tmp, VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOAD(result_tmp), vec_mul)); \
})
#define VECTOR_MATH_shannonEntropy_float_divInputPair(result_tmp, vec_div_head, vec_div_tail) ({ \
  VECTOR_FLOAT_TYPE vec_divResult = VECTOR_FLOAT_omitZeros_fromComputation_DIV(vec_div_head, vec_div_tail); \
  VECTOR_MATH_shannonEntropy_float(result_tmp, vec_divResult); })


//! @return the for-loop-size to be sued.
#define VECTOR_FLOAT_maxLenght_forLoop(size) ( (size >= VECTOR_FLOAT_ITER_SIZE) ? size - VECTOR_FLOAT_ITER_SIZE : 0 )

//! Copmute "result = result/data_2" for two "float *" memory-references
#define VECTOR_FLOAT_storeAnd_div_self_data(result, data_2) (VECTOR_FLOAT_storeAnd_div_self(result, VECTOR_FLOAT_LOAD(data_2)))



//! Transpsoes a given array.
//! @remarks optmized to store a value without loading a cache line, ie, use intrinsic functions:
//! @remarks for an 'innline' example of matrix-transpsotion see our "graphAlgorithms_distance::compute_transposedMatrix_float(..)"
// FIXME[JC]: may you both validate correctness of [”elow] ... and describe applications of [below] wrt. 'gluing' into software-applicaitonis?
#define VECTOR_FLOAT_transposeMatrix_forColumn(matrix_res, matrix_input, index_row, col_size) ({ \
  const uint size_innerOptimized = (col_size > VECTOR_FLOAT_ITER_SIZE) ? col_size - VECTOR_FLOAT_ITER_SIZE : 0; \
  uint j = 0; \
  if(size_innerOptimized > 0) { \
    for (; j < size_innerOptimized; j += VECTOR_FLOAT_ITER_SIZE) {	\
      VECTOR_FLOAT_STORE(&matrix_res[index_row][j], VECTOR_FLOAT_setFrom_columns(matrix_input, j, index_row)); \
     } \
  } \
  for (; j < col_size; j += 1) { \
    matrix_res[index_row][j] = matrix_input[j][index_row];			\
  } \
})




//! Convert from a CHAR-data-array to a FLOAT vector.
#define VECTOR_FLOAT_convertFrom_CHAR_data(arrOf_values, index) (VECTOR_FLOAT_convertFrom_CHAR(*(__m64*)&arrOf_values[index]))





//! Idneitfy the min-value in a flaot-vector.
// FIXME[jc]: si there a mroe effective instrinsitctc-appraoch tahn [”elow]?
#define VECTOR_FLOAT_storeAnd_horizontalMax(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = MiC__float_resultBuffer_empty; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = T_FLOAT_MIN_ABS; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(result[m] > scalar_result) {scalar_result = result[m];} } \
  scalar_result; }) //! ie, return the result.
//! Idneitfy the min-value in a flaot-vector.
// FIXME[jc]: si there a mroe effective instrinsitctc-appraoch tahn [”elow]?
#define VECTOR_FLOAT_storeAnd_horizontalMin(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = MiC__float_resultBuffer_empty; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = T_FLOAT_MAX; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(result[m] < scalar_result) {scalar_result = result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalMin_alt1(vec) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec; \
  t_float scalar_result = T_FLOAT_MAX; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(v.buffer[m] < scalar_result) {scalar_result = v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.
//! Idneitfy the max-value in a flaot-vector.
// FIXME[jc]: si there a mroe effective instrinsitctc-appraoch tahn [”elow]? 
// FIXME[time]: write a performacne-test to comapre [”elow] with an 'altertive' union-of operation-call.
//! Note[article]: ... an interesting bug which we made when suign this funcitonw as to intiate a "VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);", though only set a signular vlaue using our "VECTOR_FLOAT_SUB_SINGLE", ie, where the result is that values 'less than zero' is ignored, ie, errnous reeulsts. This 'example-note' therefore (ie, among otehrs) illsutrates the bug-case where 'intiation to 0' results in result-errors.
/* printf("scalar_result=%f == %f, at %s:%d\n", scalar_result, T_FLOAT_MIN_ABS, __FILE__, __LINE__);  */
/*   printf("cmp: %.10f VS %.10f, at %s:%d\n", result[m], scalar_result, __FILE__, __LINE__);  */
/* assert(T_FLOAT_MIN_ABS != 0);  assert(scalar_result < -1);  */
#define VECTOR_FLOAT_storeAnd_horizontalMax(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = MiC__float_resultBuffer_empty; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = T_FLOAT_MIN_ABS;  for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(result[m]  > scalar_result) {scalar_result = result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalMax_alt(vec_add1) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_add1;  \
  t_float scalar_result = T_FLOAT_MIN_ABS;  for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(v.buffer[m] > scalar_result) {scalar_result = v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.

//! Idneitfy the min-value in a flaot-vector.
// FIXME: write a perforamnce-test where we compare [”elow] 'approach' with suign the "UNION_VECTOR_FLOAT_TYPE" 'appraoch'.
// FIXME[jc]: si there a mroe effective instrinsitctc-appraoch tahn [”elow]?
#define VECTOR_FLOAT_storeAnd_horizontalSum(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = MiC__float_resultBuffer_empty; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result += result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec; \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result += v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalMult(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = MiC__float_resultBuffer_empty; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result *= result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalMult_alt1(vec) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec; \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result *= v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalSum_subtract(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = MiC__float_resultBuffer_empty; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result -= result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalSum_subtract_alt(vec_add1) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_add1; \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result -= v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.

//! @return the mini-distance for an array
//! Note: we assume that ( (endPosPlussOne - start_pos) % VECTOR_FLOAT_ITER_SIZE) == 0, ie, to recue the ovrehead wrt. our appraoch.
// FIXME[tutorial]: ... consider to write an errnous version of [”elow] funciton where we dorp the "= VECTOR_FLOAT_SET1(T_FLOAT_MAX)" assignmetn .. and then ask users/studnets to fidn the soruce of the eror ... ie, as (a) the eror descries a previous cocnrete/real bug (iin oru software) and (b) 'states' the default intaition to '0' for vectors. <-- validate that the latter assum,ption laways hold
#define  VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arrOf_distance, start_pos, endPosPlussOne) ({ \
      VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_SET1(T_FLOAT_MAX);	\
  for(uint m = start_pos; m < endPosPlussOne; m+= VECTOR_FLOAT_ITER_SIZE) { \
    vec_min = VECTOR_FLOAT_loadAnd_min_vec_memRef(vec_min, &arrOf_distance[m]); \
  } \
VECTOR_FLOAT_storeAnd_horizontalMin(vec_min);}) //! ie, return.


//! @return the mini-distance for an array
//! Note: we assume that ( (endPosPlussOne - start_pos) % VECTOR_FLOAT_ITER_SIZE) == 0, ie, to recue the ovrehead wrt. our appraoch.
#define  VECTOR_FLOAT_MATH_find_sumOfDistance_ofArray_alignedInPerfectChunks(arrOf_distance, start_pos, endPosPlussOne) ({ \
      VECTOR_FLOAT_TYPE vec_sum = VECTOR_FLOAT_SET1(0);			\
  for(uint m = start_pos; m < endPosPlussOne; m+= VECTOR_FLOAT_ITER_SIZE) { \
    vec_sum = VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_sum, &arrOf_distance[m]); \
  } \
VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);}) //! ie, return.

//! Identify the min-value for each column-range:
//! @remarks Logics to compute a 'split' set of min-values for "arr[ ... part1 ... | ... part2 ... | .. (etc) ...]", and store the result at "arr_result[row_index ... row_index+VECTOR_FLOAT_ITER_SIZE]" (oekseth, 06. juni 2016).
//! @remarks an example-applicaiton is to comptue 'tiles' of min-values, eg, as seen in our otpimized/new algorithm for comptuation of min-valeus in hierarhcicaal clustering.
//! @remarks teh min-oepration is handled by the [”elow] "VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks" macro, ie, a call to this 'funciton-macro' results in a list-traversal for each of the 'sub-chunks'.
#if VECTOR_CONFIG_USE_FLOAT == 1 
//  printf("scalar_1=%f, at %s:%d\n", scalar_1, __FILE__, __LINE__);	
#define VECTOR_FLOAT_minDistance_eachTile(arr_result, row_index, arr_input, arr_input_pos_start_memRef, tile_size) ({ \
  const float scalar_0 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 0, *arr_input_pos_start_memRef + tile_size); \
  const float scalar_1 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 1*tile_size, *arr_input_pos_start_memRef + 2*tile_size); \
  const float scalar_2 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 2*tile_size, *arr_input_pos_start_memRef + 3*tile_size); \
  const float scalar_3 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 3*tile_size, *arr_input_pos_start_memRef + 4*tile_size); \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(scalar_0, scalar_1, scalar_2, scalar_3); \
  VECTOR_FLOAT_PRINT(vec_result); \
  VECTOR_FLOAT_STORE(&(arr_result[row_index]), vec_result); \
  *arr_input_pos_start_memRef += (uint)VECTOR_FLOAT_ITER_SIZE*tile_size; \
})						\

#elif VECTOR_CONFIG_USE_FLOAT == 0 
#define VECTOR_UNROLL_minDistance(arr_result, row_index, arr_input, arr_input_pos_start_memRef, tile_size) ({ \
  VECTOR_FLOAT_STORE(&(arr_result[row_index]), \
		     VECTOR_FLOAT_SET( \
				      VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 0, *arr_input_pos_start_memRef + tile_size) \
				      )); \
  *arr_input_pos_start_memRef += (uint)VECTOR_FLOAT_ITER_SIZE*tile_size; \
})
#else 
#error "Won't work, ie, need to add specific support for this case. An example-case (for this erorr) is when testing differnet hardware-configurations."  
#endif 


//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics.
//! @remarks Logics: if(arr_current[bucket_id] < arr_best_value(bucket_id)) {arr_best_value[bucket_id] = score; arr_best_index[bucket_id] = row_id;}
//! @remarks in this appraoch we: Step(1): identify the masks wrt. the 'local' and the 'global' data-set; step(2) hadnle the NAN values; step(3) clear the valeus which are not of interest; step(4) combine the updated 'local' and 'global' index; step(5) return the min-values of the input-scores/distances
#define VECTOR_FLOAT_minDistance_rememberSource_vec(vec_input, vec_global, vec_input_index, vec_global_index) ({ \
__m128 local_mask = _mm_cmpgt_ps(vec_input, vec_global);  __m128 global_mask = _mm_cmplt_ps(vec_input, vec_global); \
 local_mask = VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(local_mask); \
 global_mask = VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(global_mask); \
 vec_input_index = VECTOR_FLOAT_MUL(vec_input_index, local_mask); \
 vec_global_index = VECTOR_FLOAT_MUL(vec_global_index, global_mask); \
 vec_global_index = VECTOR_FLOAT_ADD(vec_input_index, vec_global_index); \
 VECTOR_FLOAT_MIN(vec_input, vec_global); }) //! @return the min-value of vec_input and vec_global

//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics.
#define VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input(vec_input, vec_global, vec_global_index, idOf_input) ({ \
  VECTOR_FLOAT_TYPE vec_input_index = VECTOR_FLOAT_SET1(idOf_input); \
  VECTOR_FLOAT_minDistance_rememberSource_vec(vec_input, vec_global, vec_input_index, vec_global_index);})
//! Extends our "VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input(..)" to make use of memory-references:
#define VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input_data(memRef_result_score, memRef_result_index, data_local_score, data_global_score, data_global_index, idOf_input) ({ \
  const VECTOR_FLOAT_TYPE vec_input  = VECTOR_FLOAT_LOAD(data_local_score); const VECTOR_FLOAT_TYPE vec_global = VECTOR_FLOAT_LOAD(data_global_score); \
  VECTOR_FLOAT_TYPE vec_global_index = VECTOR_FLOAT_LOAD(data_global_index); \
  VECTOR_FLOAT_TYPE vec_result_min = VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input(vec_input, vec_global, vec_global_index, idOf_input); \
  VECTOR_FLOAT_STORE(memRef_result_score, vec_result_min); VECTOR_FLOAT_STORE(memRef_result_index, vec_global_index); })
//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics.
#define VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_range(vec_input, vec_global, vec_global_index, idOf_input) { \
  VECTOR_FLOAT_TYPE vec_input_index = VECTOR_FLOAT_setFrom_indexRange(idOf_input); \
VECTOR_FLOAT_minDistance_rememberSource_vec(vec_input, vec_global, vec_input_index, vec_global_index);})


// TODO: consider to supprot/write macros for getting the mantiussa of lfoats wrt. SSE3, eg, as described at "https://www.kvraudio.com/forum/viewtopic.php?t=314213" (oekseth, 06. june 2016)


/** ----------------- functions currenlty not fond in the mine-py-folder <-- FIXME[JC]: may you have a look upon these ... and then update? ---- **/


// FIXME: figure out how the following may be used in our operaitons: ...  __m128i packed01 = _mm_packss_epi32(dwords0, dwords1); __m12i8 packedbytes=_mm_packus_epi16(packed01, packed23); // SSE2

//! @return the max-value in a horizontal vector:
/* #define VECTOR_FLOAT_horizontal_max(vec_value) ({ \   */
/* /\*   __m128 max1 = _mm_shuffle_ps(x, _MM_SHUFFLE(0,0,3,2)); *\/ */
/* /\*   __m128 max2 = _mm_max_ps(x,max1); *\/ */
/* /\*   __m128 max3 = _mm_shuffle_ps(max2, _MM_SHUFFLE(0,0,0,1)); *\/ */
/* /\*   __m128 max4 = _mm_max_ps(max2,max3); *\/ */
/* /\* // FIXME: validate correnctess of this funciton. *\/ */
/* /\*   return _mm_cvtsi32_ss(max4); //! docu: Copy the lower 32-bit integer in a to dst. *\/ */
/* }); */


//! ************************************************************************************
#endif //! EOF.
