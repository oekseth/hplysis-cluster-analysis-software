#include "config_nonRank_manyToMany.h"


//! Initaites the config_nonRank_manyToMany_t to empty.
void setTo_empty___config_nonRank_manyToMany(config_nonRank_manyToMany_t *self) {
  //  self->index1 = 0; 
  self->nrows = 0;
  self->ncols = 0;
  self->data1 = NULL; 
  self->data2 = NULL;
  self->mask1 = NULL; 
  self->mask2 = NULL;
  self->weight = NULL;
  self->typeOf_metric = e_kt_correlationFunction_undef;
  self->typeOf_metric_correlation = e_typeOf_metric_correlation_pearson_undef;
  self->config_allAgainstAll = NULL;
  self->resultMatrix = NULL;
  self->obj_resultsFromTiling = NULL;
  // self->arrOf_result = NULL;
}

//! @brief intiate an object of type config_nonRank_manyToMany_t
//! @return an intiated object (ie, which is set to default empty values) wrt. the config_nonRank_manyToMany_t structure-type.
config_nonRank_manyToMany_t get_init__config_nonRank_manyToMany() {
  config_nonRank_manyToMany_t self; setTo_empty___config_nonRank_manyToMany(&self);
  return self;
}

//! Initiate the config_nonRank_manyToMany_t object with the complete lsit of specificaitons.
void init__config_nonRank_manyToMany(config_nonRank_manyToMany_t *self, const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, s_allAgainstAll_config_t *config_allAgainstAll, s_kt_computeTile_subResults_t *obj_resultsFromTiling) {
  //self->index1 = index1; 
  assert(data1); assert(data2);

  self->nrows = nrows;
  self->ncols = ncols;
  self->data1 = data1; 
  self->data2 = data2;
  self->mask1 = mask1; 
  self->mask2 = mask2;
  self->weight = weight;;
  self->typeOf_metric = typeOf_metric;
  self->typeOf_metric_correlation = typeOf_metric_correlation;
  self->config_allAgainstAll = config_allAgainstAll;
  self->resultMatrix = resultMatrix;
  self->obj_resultsFromTiling = obj_resultsFromTiling;
/* #if(TEMPLATE_local_distanceConfiguration_typeOf_distanceUpdate != 0) //! ie, e_template_correlation_tile_typeOf_distanceUpdate_scalar) */
  /* if(typeOf_metric == e_kt_correlationFunction_groupOf_absoluteDifference_Sorensen) { */
  /*   assert(obj_resultsFromTiling); */
  /*   assert(self->obj_resultsFromTiling); */
  /* } */
  //#endif


  assert(self->data1);
  assert(self->data1 == data1);
  assert(self->data2);
  assert(self->data2 == data2);
  //self->arrOf_result = arrOf_result;
}

