#include "config_nonRank_each.h"

//! Initaites the config_nonRank_each_t to empty.
void setTo_empty___config_nonRank_each(config_nonRank_each_t *self) {
  self->ncols = 0;
  self->rmul1 = NULL;
  self->rmul2 = NULL;
  self->weight = NULL;
  self->mask1 = NULL;
  self->mask2 = NULL;
  self->config = get_init_struct_s_allAgainstAll_config();  
}

//! @brief intiate an object of type config_nonRank_each_t
//! @return an intiated object (ie, which is set to default empty values) wrt. the config_nonRank_each_t structure-type.
config_nonRank_each_t get_init__config_nonRank_each() {
  config_nonRank_each_t self; setTo_empty___config_nonRank_each(&self);
  return self;
}

//! Initiate the config_nonRank_each_t object with the complete lsit of specificaitons.
void init__config_nonRank_each(config_nonRank_each_t *self, const uint ncols, const t_float *__restrict__ rmul1, const t_float *__restrict__ rmul2, const t_float *__restrict__ weight, char *__restrict__ mask1, char *__restrict__ mask2, const s_allAgainstAll_config_t config) {
  self->ncols = ncols;
  self->rmul1 = rmul1;
  self->rmul2 = rmul2;
  self->weight = weight;
  self->mask1 = mask1;
  self->mask2 = mask1;
  self->config = config;
}
