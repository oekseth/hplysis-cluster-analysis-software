/**
   @struct s_kt_matrix
   @brief a wrapper-object used to add elements to the input-matrix (oekseth, 06. okt. 2016).
   @remarks for examples of use-cases please see our test-ocde in our "measure_terminal.c" (oekseth, 06. nov 2016).
 **/
typedef struct s_kt_matrix {
  t_float **matrix;
  t_float *weight;
  uint ncols; uint nrows;  
  //! ----
  uint *mapOf_rowIds; //! which is used for cases such as 'storing the clsuter-mebership of aparitcula row-vertedx' (oekseth, 06. nvo. 2016).
  uint mapOf_rowIds_biggestIndexInserted; //! which is used during a 'push' operation.
  //! ----
  char **nameOf_rows; char **nameOf_columns; bool nameOf__allocationScheme__allocatedSeperatelyInEach;
  bool isMemoryOwnerOf_attribute_nameOf; //! whihc wehn used is 'used' to avoid 'the strucutre' from de-allocating the "nameOf_columns" and "nameOf_rows" attributes
  //! ----
  FILE *stream_out;
  bool isTo_useMatrixFormat;
  bool isTo_inResult_isTo_useJavaScript_syntax;
  bool isTo_inResult_isTo_useJSON_syntax;
  const char *stringOf_header;
  bool include_stringIdentifers_in_matrix;
  bool isTo_writeOut_stringHeaders;
  //uint cnt_matrices_exported;
  uint cnt_dataObjects_written;  uint cnt_dataObjects_toGenerate; 

  //! ----
  //s_dataStruct_matrix_dense_t data_obj; 
} s_kt_matrix_t;


//! Initates an object

//! SEt the s_kt_matrix_t object to empty.
void setTo_empty__s_kt_matrix_t(s_kt_matrix_t *self) {
  assert(self);
  self->nrows = 0; self->ncols = 0; self->weight = NULL; self->matrix = NULL;
  //! ----
  self->mapOf_rowIds = NULL;
  self->mapOf_rowIds_biggestIndexInserted = 0;
  //! ----
  self->nameOf_rows = NULL; self->nameOf_columns = NULL; self->nameOf__allocationScheme__allocatedSeperatelyInEach = false;
  //! ----
  self->stream_out = NULL;
  self->isTo_useMatrixFormat  = false;
  self->isTo_inResult_isTo_useJavaScript_syntax  = false;
  self->isTo_inResult_isTo_useJSON_syntax  = false;
  self->stringOf_header  = NULL;
  self->include_stringIdentifers_in_matrix  = false;
  self->isTo_writeOut_stringHeaders  = false;
  self->cnt_dataObjects_toGenerate = 0;
  self->cnt_dataObjects_written = 0;
  self->isMemoryOwnerOf_attribute_nameOf = true; //! whihc wehn used is 'used' to avoid 'the strucutre' from de-allocating the "nameOf_columns" and "nameOf_rows" attributes
}

//! @return an nitaited amtrix with No memory-allcoatiosn:
s_kt_matrix_t setToEmptyAndReturn__s_kt_matrix_t() {
  s_kt_matrix_t self; setTo_empty__s_kt_matrix_t(&self); 
  return self;
}


//! Initates an object
void init__s_kt_matrix(s_kt_matrix_t *self, const uint nrows, const uint ncols, const bool isTo_allocateWeightColumns) {
  assert(self); assert(nrows > 0); assert(ncols > 0);
  //! Default allocations:
  setTo_empty__s_kt_matrix_t(self);

  //! Then the 'operation':
  const t_float default_value_float = 0;
  self->matrix = allocate_2d_list_float(nrows, ncols, default_value_float); 
  //! Set each element to 'no-of-itnerest':
  for(uint row_id = 0; row_id < nrows; row_id++) {
    for(uint col_id = 0; col_id < ncols; col_id++) {
      self->matrix[row_id][col_id] = T_FLOAT_MAX; //! ie, 'mark' the cell as 'not of itnerest'
    }
  }
  if(isTo_allocateWeightColumns) {
    const t_float def_weight = 1; //! ie, as we then assume taht 'all vertices are of interest'.
    self->weight = allocate_1d_list_float(ncols, def_weight);
  } else {self->weight = NULL;}
  self->nrows = nrows; self->ncols = ncols; 
}


//void init__s_kt_matrix(s_kt_matrix_t *self, const uint nrows, const uint ncols, const bool isTo_allocateWeightColumns) ;
//! Initates an object
//! @returnt he new-allcoated object:
static s_kt_matrix_t initAndReturn__s_kt_matrix(const uint nrows, const uint ncols) {
  s_kt_matrix_t self; setTo_empty__s_kt_matrix_t(&self);
  init__s_kt_matrix(&self, nrows, ncols, /*isTo_allocateWeightColumns=*/false);
  return self;
}

/**
   @brief specify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos> is the id of the internla column or row to udpdate
   @param <stringTo_add> is the string to copy into this strucutre
   @param <addFor_column> which if faset to false (or '0') implies that we insert into the row-set.
   @remarks we expect the index_pos to be less than "nrows" for "addFor_column==0", and index_pos to be less than ncols for "addFor_column==1";   
 **/
void set_stringConst__s_kt_matrix(s_kt_matrix_t *self, const uint index_pos, const char *stringTo_add, const bool addFor_column) {
  assert(self); 
  char **arr_local = NULL;
  if(!stringTo_add || !strlen(stringTo_add)) {
    fprintf(stderr, "!!\t String=\"%s\" seems empty. In brief we regard the latter as an inocnsistnecy, ie, please udpate your intaiton-funciton. For questions please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringTo_add, __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  if(addFor_column == false) {
    if(index_pos >= self->nrows) {
      fprintf(stderr, "!!\t For string=\"%s\" you have requested a row-string-inseriton at index=%u >= %u=nrows. In brief we regard the latter as an inocnsistnecy, ie, please udpate your intaiton-funciton. For questions please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringTo_add, index_pos, self->nrows, __FUNCTION__, __FILE__, __LINE__);
      return;
    }
    if(self->nameOf_rows == NULL) {
      self->nameOf_rows =  (char**)alloc_generic_type_2d(char, self->nameOf_rows, self->nrows);
      for(uint i = 0; i < self->nrows; i++) {
	self->nameOf_rows[i] = NULL; //! ie, intiate.
      }
      //self->nameOf_rows =  (char**)alloc_generic_type_2d(char, &(self->nameOf_rows), self->nrows);
      self->nameOf__allocationScheme__allocatedSeperatelyInEach = true;
    }
    //! Specify the data-structre to update:
    arr_local = self->nameOf_rows;
  } else {
    if(index_pos >= self->ncols) {
      fprintf(stderr, "!!\t For string=\"%s\" you have requested a row-string-inseriton at index=%u >= %u=ncols. In brief we regard the latter as an inocnsistnecy, ie, please udpate your intaiton-funciton. For questions please contact [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringTo_add, index_pos, self->ncols, __FUNCTION__, __FILE__, __LINE__);
      assert(false); // TODO: cosnider removing this.
      return;
    }
    if(self->nameOf_columns == NULL) {
      self->nameOf_columns = (char**)alloc_generic_type_2d(char, self->nameOf_columns, self->ncols);
      for(uint i = 0; i < self->ncols; i++) {
	self->nameOf_columns[i] = NULL; //! ie, intiate.
      }
      //	self->nameOf_columns = (char**)alloc_generic_type_2d(char, &(self->nameOf_columns), self->ncols);
      self->nameOf__allocationScheme__allocatedSeperatelyInEach = true;
    }
    //! Specify the data-structre to update:
    arr_local = self->nameOf_columns;
  }
  //! ------------------
  //!
  //! Allocate memory and copy-paste teh strucutre:
  const uint str_len = strlen(stringTo_add) + 1;
  assert(str_len > 0);
  const char emppty_char = 0;
  char *new_string = allocate_1d_list_char(str_len, emppty_char);
  memcpy(new_string, stringTo_add, str_len-1);
  //! Udpate our 'global' struycutre with the new string:
  assert(arr_local);
  arr_local[index_pos] = new_string;
}

/**
   @brief specify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos> is the id of the internla column or row to udpdate
   @param <stringTo_add> is the string to copy into this strucutre
   @param <addFor_column> which if faset to false (or '0') implies that we insert into the row-set.
   @remarks we expect the index_pos to be less than "nrows" for "addFor_column==0", and index_pos to be less than ncols for "addFor_column==1";   
 **/
static void set_string__s_kt_matrix(s_kt_matrix_t *self, const uint index_pos, char *stringTo_add, const bool addFor_column) { set_stringConst__s_kt_matrix(self, index_pos, stringTo_add, addFor_column);}

//! De-allocate sthe nameOf_columns attribute (fi the latter differns from the "nameOf_rows" attribute) (oekseth, 06. des. 2016).
//! @remarks a use-case where we 'replace' teh "nameOf_columns" attribute with a 'driect' poitner-reference to "nameOf_rows" after the correlaiton-matrix is comptued (oekseth, 06. des. 2016).
void free_arrOf_names_columns__s_kt_matrix(s_kt_matrix_t *self) {
  assert(self);
  if(self->isMemoryOwnerOf_attribute_nameOf == true) {
    const bool rows_equals_cols = (self->nameOf_columns == self->nameOf_rows);
    if(self->nameOf_columns) {
      if(rows_equals_cols == false) {
	if(self->nameOf__allocationScheme__allocatedSeperatelyInEach == false) {
	  free_2d_list_char(&(self->nameOf_columns), self->ncols); 
	} else { //! then we de-allcoate seperately for each 'chunk':
	  for(uint i = 0; i < self->ncols; i++) {
	    if(self->nameOf_columns[i]) {
	      free_1d_list_char(&(self->nameOf_columns[i]));
	    }
	  }
	  free_generic_type_2d(self->nameOf_columns); 
	}
      } //! otherwise we assuem the nameOf_columns was implictly freed wrt. [ªbove] "nameOf_rows"
    } //! else we asusme a differetn object will 'de-allcoate this objhect'.
    self->nameOf_columns = NULL;
  }
}


//! De-allcoate the s_kt_matrix_t object.
void free__s_kt_matrix(s_kt_matrix_t *self) {
  assert(self);
  //! ---
  if(self->matrix) {free_2d_list_float(&(self->matrix), self->nrows); self->matrix = NULL;}
  if(self->weight) {free_1d_list_float(&(self->weight)); self->weight = NULL;}
  if(self->mapOf_rowIds) {free_1d_list_uint(&(self->mapOf_rowIds)); self->mapOf_rowIds = NULL;}
  //! ---

  if(self->isMemoryOwnerOf_attribute_nameOf == true) {  
    free_arrOf_names_columns__s_kt_matrix(self); //! ie, first de-allcoate the columns: this 'order' given the often-seen-use-case where we 'replace' teh "nameOf_columns" attribute with a 'driect' poitner-reference to "nameOf_rows" after the correlaiton-matrix is comptued (oekseth, 06. des. 2016).
    if(self->nameOf_rows) {
      if(self->nameOf__allocationScheme__allocatedSeperatelyInEach == false) {
	free_2d_list_char(&(self->nameOf_rows), self->nrows); 
      } else { //! then we de-allcoate seperately for each 'chunk':
	for(uint i = 0; i < self->nrows; i++) {
	  if(self->nameOf_rows[i]) {
	    free_1d_list_char(&(self->nameOf_rows[i]));
	  }
	}
	free_generic_type_2d(self->nameOf_rows); 
      }
      self->nameOf_rows = NULL;
    }
  } else { //! then we asusme a different object will 'de-allocate tehse':
    self->nameOf_rows = NULL;
    self->nameOf_columns = NULL;
  }
  //! ---
  self->nrows = 0;   self->ncols = 0;
  if(self->stream_out != NULL) {
    if(
       (self->stream_out != stderr) &&
       (self->stream_out != stdout)
       ) {
      fclose(self->stream_out); self->stream_out = NULL;
    }
  }
}
//__attribute__((always_inline)) 
void get_string__s_kt_matrix(const s_kt_matrix_t *self, const uint index_pos, const bool addFor_column, char **stringOf_result) {
  assert(self);
  *stringOf_result = NULL; //! ie, to 'init' the variable.
  if(addFor_column == false) {
    //assert(self->nameOf_rows);
    //assert(index_pos < self->nrows);
    if( (index_pos < self->nrows) && self->nameOf_rows) {*stringOf_result = self->nameOf_rows[index_pos];}
    //printf("\t\trow[%u]=\"%s\", at %s:%d\n", index_pos, *stringOf_result, __FILE__, __LINE__);
  } else {
    //assert(self->nameOf_columns);
    //assert(index_pos < self->ncols);
    if( (index_pos < self->ncols) && self->nameOf_columns) {*stringOf_result = self->nameOf_columns[index_pos];}
  }
  // printf("\t\trow[%u]=\"%s\", at %s:%d\n", index_pos, *stringOf_result, __FILE__, __LINE__);
}

//! @return the assicated string:
//static inline 
//__attribute__((always_inline)) 
const char *getAndReturn_string__s_kt_matrix(const s_kt_matrix_t *self, const uint index_pos, const bool addFor_column) {
  assert(self);
  char *stringOf_result = NULL;
  get_string__s_kt_matrix(self, index_pos, addFor_column, &stringOf_result);
  return stringOf_result;
  // printf("\t\trow[%u]=\"%s\", at %s:%d\n", index_pos, *stringOf_result, __FILE__, __LINE__);
}

/**
   @file parse_main.h
   @struct s_dataStruct_matrix_dense
   @brief provide lgocis for parsing files
   @author Ole Kristian Ekseth (oekseth)
 **/
typedef struct s_dataStruct_matrix_dense {
  //! Note: if ["elow] data-structure is updated, then rembmer to also update our "mine.pyx" (eosekth, 06. sept. 2016).
  char **nameOf_rows; char **nameOf_columns;
  t_float **matrixOf_data;
  uint cnt_columns; uint cnt_rows;
  uint cnt_columns_allocated; uint cnt_rows_allocated;
  uint _word_max_length;
  const char *stringOf_defaultRowPrefix;
  char *__allocated_stringOf_defaultRowPrefix;
} s_dataStruct_matrix_dense_t;



//! Intiate (ie, set to empty) the "s_dataStruct_matrix_dense_t" object wrt. the memory-refrences (and not the object-reference itself).
void init_s_dataStruct_matrix_dense_t_setToEmpty(s_dataStruct_matrix_dense_t *obj) {
  // printf("sets-to-empty, at %s:%d\n", __FILE__, __LINE__);
  obj->nameOf_rows = NULL; obj->nameOf_columns = NULL; obj->matrixOf_data = NULL;
  obj->cnt_columns = 0; obj->cnt_rows = 0;
  obj->cnt_columns_allocated = 0; obj->cnt_rows_allocated = 0;
  obj->_word_max_length = 0;
  obj->stringOf_defaultRowPrefix = "row";
  obj->__allocated_stringOf_defaultRowPrefix = NULL;
  //  printf("\n\nreset-stringOf_defaultRowPrefix, at %s:%d\n", __FILE__, __LINE__);
}

//! @return true if "obj" is set to empty, eg, through a call to our "init_s_dataStruct_matrix_dense_t_setToEmpty(obj)".
bool is_setTo_empty(const s_dataStruct_matrix_dense_t *obj) {
  assert(obj);
  if(obj->nameOf_columns != NULL) {return false;}
  if(obj->nameOf_rows != NULL) {return false;}
  if(obj->matrixOf_data != NULL) {return false;}
  if(obj->cnt_columns != 0) {return false;}
  if(obj->cnt_rows != 0) {return false;}
  
  // printf("\n\nobj-is-reset,. at %s:%d\n", __FILE__, __LINE__);

  //! At this exeuction-point we asusme the object is set to empty:
  return true;
}

/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
   @param <word_max_length> which if set iodentifies the max-word-length in the data: if word_max_length is greater than zero we allocate the "nameOf_columns" and "nameOf_rows" object-attributes.
 **/
void init_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj,  uint cnt_rows,  uint cnt_columns,  uint _word_max_length){
//void init_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns, const uint _word_max_length){
  assert(obj);
  //! First itnaitet he object:
  assert(obj->nameOf_rows == NULL);
  //  init_s_dataStruct_matrix_dense_t_setToEmpty(obj);
  uint word_max_length = _word_max_length;

  if(word_max_length < 1000) {
    word_max_length = 1000; //! ie, our default assumpiotn    
  }
  assert(word_max_length != UINT_MAX);

  //! Update the counts:
  obj->cnt_rows = cnt_rows; obj->cnt_columns = cnt_columns;
  //! Then allcoate the data, ie, if the lengts are set.
  const uint new_word_size = word_max_length + 1;     const char default_value_alloc = '\0';
  if(cnt_rows && word_max_length) {
    assert(obj->nameOf_rows == NULL);
    obj->nameOf_rows = allocate_2d_list_char(cnt_rows, new_word_size, default_value_alloc);
    obj->cnt_rows_allocated = cnt_rows;
    assert(obj->nameOf_rows != NULL);
  }
  if(cnt_columns && word_max_length) {
    // printf("allocates for %u columns, at %s:%d\n", cnt_columns, __FILE__, __LINE__);
    // printf("allocates for column-names-cnt=%u, w/max-word-size=%u, at %s:%d\n", cnt_columns, new_word_size, __FILE__, __LINE__);
    assert(obj->nameOf_columns == NULL);
    obj->nameOf_columns = allocate_2d_list_char(cnt_columns, new_word_size, default_value_alloc);
    obj->cnt_columns_allocated = cnt_columns;
  } else {
    // printf("not allocated for column-names, at %s:%d\n", __FILE__, __LINE__);
  }
  if(cnt_rows && cnt_columns) {
    const t_float default_value_float = 0;
    obj->matrixOf_data = allocate_2d_list_float(cnt_rows, cnt_columns, default_value_float);
    for(uint i = 0; i < cnt_rows; i++) {
      for(uint k = 0; k < cnt_columns; k++) {obj->matrixOf_data[i][k] = T_FLOAT_MAX;}} //! ie, set the vlaue epxlcitly to 'empty' (oekseth, 06. feb. 2017).
  }
  obj->_word_max_length = word_max_length;
  obj->stringOf_defaultRowPrefix = "row";
  obj->__allocated_stringOf_defaultRowPrefix = NULL;
  // printf("reset-stringOf_defaultRowPrefix, at %s:%d\n", __FILE__, __LINE__);
}



/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_useJavaScript_syntax> which if set to true implies that we makes use of a 'java-script' wrappers from the values.
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const bool include_stringIdentifers_in_matrix, const char _isTo_writeOut_stringHeaders) {
  //! What we expect:
  assert(obj);   assert(stream);
  assert(obj->matrixOf_data); assert(obj->cnt_columns); assert(obj->cnt_rows);
  
  if(stringOf_header && strlen(stringOf_header)) {
    fprintf(stream, "\n#! %s\n", stringOf_header); //! where "#! " is used to avoid the string from wrongly being parsed as a data-row.
  }

  
  const char seperator_columns = '\t';

  if(_isTo_writeOut_stringHeaders == false) {nameOf_columns = NULL;}
  const bool isTo_writeOut_stringHeaders = true;

  //! Eitehr use the input-arguemnts or the 'implict' name-mappings found in "obj":
  char **nameOf_cols_local = (nameOf_columns != NULL) ? nameOf_columns : obj->nameOf_columns;
  char **nameOf_rows_local = (nameOf_rows != NULL) ? nameOf_rows : obj->nameOf_rows;
  //!
  //! Idnietyf columns to ignore:
  char empty_0 = 1; //! ie, amrk all columsn as of interest.
  char *mapOf_colsToUse = allocate_1d_list_char(obj->cnt_columns, empty_0);
  assert(mapOf_colsToUse);
  uint cntTotal__cols = 0;
  for(uint y = 0; y < obj->cnt_columns; y++) {
    long long int cnt_ofInterest = 0;
    for(uint i = 0; i < obj->cnt_rows; i++) {
      const t_float score = obj->matrixOf_data[i][y];  
      cnt_ofInterest += isOf_interest(score);
    }
    const bool isTo_use = (cnt_ofInterest > 0); //!, where where '0' implies 'false'.
    mapOf_colsToUse[y] = isTo_use;
    cntTotal__cols += isTo_use;
  }


  // printf("## isTo_writeOut_stringHeaders=%d, at %s:%d\n", isTo_writeOut_stringHeaders, __FILE__, __LINE__);

  if(isTo_writeOut_stringHeaders) {
  //if(nameOf_cols_local != NULL) {
    //! Write out the 'ehader', a 'header' expected by our "parse_main.c"
    fprintf(stream, "#! row-name:\t");
    //! Write out the column-rows:
    if(nameOf_cols_local != NULL) {
      uint y_currPos = 0;       bool newLineAdded = false;
      for(uint y = 0; y < obj->cnt_columns; y++) {
	if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
	const char *string_local = nameOf_cols_local[y];
	if(!string_local || !strlen(string_local)) {
	  string_local = "-"; //! ie, an empty-sign (oekseth, 06. des. 2016).
	}
	fprintf(stream, "%s", string_local); 
	//if((y +1) != obj->cnt_columns) {fputc(seperator_columns, stream);} else {fputc('\n', stream);}
	if((y_currPos +1) != cntTotal__cols) {fputc(seperator_columns, stream);} else {fputc('\n', stream); newLineAdded = true;}
	y_currPos++;
      }     
      if(newLineAdded == false) {fputc('\n', stream); newLineAdded = true;}
    } else {
      uint y_currPos = 0;       bool newLineAdded = false;
      for(uint y = 0; y < obj->cnt_columns; y++) {
	if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
	fprintf(stream, "col-%u", y);
	if((y_currPos +1) != cntTotal__cols) {fputc(seperator_columns, stream);} else {fputc('\n', stream); newLineAdded = true;}
	y_currPos++;
      }
      if(newLineAdded == false) {fputc('\n', stream); newLineAdded = true;}
    }
  }

  if(nameOf_rows_local && (include_stringIdentifers_in_matrix == false)) {
    if(isTo_writeOut_stringHeaders) {
      //! Then write out the row-identifers:
      fprintf(stream, "#! legend-id-rows:\t");
      //! Write out the column-rows:
      for(uint y = 0; y < obj->cnt_rows; y++) {
	fprintf(stream, "%s\t", nameOf_rows_local[y]); 
	if((y +1) != obj->cnt_rows) {fputc(seperator_columns, stream);} else {fputc('\n', stream);}
      }
    }
    //! Avoid teh string-variables from being pritned 'as-is' in the matrix:
    nameOf_rows_local = NULL;     nameOf_cols_local = NULL;
  }


  //! Write out the tab-formatted matrix:
  for(uint i = 0; i < obj->cnt_rows; i++) {
    uint cnt_rowIsOf_interst = 0;
    for(uint y = 0; y < obj->cnt_columns; y++) {
      if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
      if(obj->matrixOf_data[i][y] != T_FLOAT_MAX) {cnt_rowIsOf_interst++;}
    }
    if(cnt_rowIsOf_interst == 0) {continue;} //! ie, as the row is then not of interest.
    assert(obj->matrixOf_data[i]);
    if(nameOf_rows_local) { fprintf(stream, "%s\t", nameOf_rows_local[i]); }
    else if(include_stringIdentifers_in_matrix) {fprintf(stream, "row-%u\t", i); }
    //! Write out the scores:
    uint y_currPos = 0;
    for(uint y = 0; y < obj->cnt_columns; y++) {
      if(mapOf_colsToUse[y] == 0) {continue;} //! ie, as we then assuemt ehc olumn is Not of interest (oekseth, 06. june. 2017).
      t_float score = obj->matrixOf_data[i][y];
      //if(score == T_FLOAT_MAX) {score = 0;} //! ie, as we then assuem that  '0' implies 'emptyness' <-- TDOO[JC]: may you JC valdiate correcteness/implcaitosn wrt. this asusmption-strategy? (oekseth, 06. des. 2016).
      if(score != T_FLOAT_MAX) {
	fprintf(stream, "%f", score);
      } else {
	fputc('-', stream);
      }
      if((y_currPos +1) != cntTotal__cols) {fputc(seperator_columns, stream);} else {fputc('\n', stream);}
      y_currPos++;
    }
  }
  if(mapOf_colsToUse) {free_1d_list_char(&mapOf_colsToUse); mapOf_colsToUse = NULL;}
}


//! An extneisve configruation to the file-export-routine.
void export_config_extensive_s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, const bool isTo_useMatrixFormat, const bool isTo_inResult_isTo_useJavaScript_syntax, const bool isTo_inResult_isTo_useJSON_syntax, const char *stringOf_header, const bool include_stringIdentifers_in_matrix, uint cnt_dataObjects_toGenerate) {
  if( (cnt_dataObjects_toGenerate == 0) || (cnt_dataObjects_toGenerate == UINT_MAX)) {
    cnt_dataObjects_toGenerate = 0;}
  self->cnt_dataObjects_toGenerate = cnt_dataObjects_toGenerate; //! ie, update.
  self->cnt_dataObjects_written = 0; //! ie, reset.
  
  //! 
  //! Update the filter-pointer:
  if(stringOf_resultFile != NULL) {
    if(self->stream_out != NULL) {
      if(
	 (self->stream_out != stderr) &&
	 (self->stream_out != stdout)
	 ) {
	fclose(self->stream_out); self->stream_out = NULL;
      }
    }
  }
  //! Idnetify the file-descrptor:
  FILE *stream_out = self->stream_out;
  if(stream_out == NULL) {stream_out = stdout;}
  char str_tmp[1000] = {'\0'};
  if(stringOf_resultFile && strlen(stringOf_resultFile)) {

    fprintf(stderr, "#! exportFile: \"%s\", at %s:%d\n",stringOf_resultFile, __FILE__, __LINE__);  //! among other used at "x_measure_all.pl"
    stream_out = fopen(stringOf_resultFile, "w");
    // printf("given stringOf_resultFile=\"%s\" sets the stream-out variable, at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
    if (stream_out == NULL) {
      assert(false);
      /* struct tms tms_start = tms();  clock_t clock_time_start = times(&tms_start); */
      /* sprintf(str_tmp, "%p_%lld.brief.tsv", self, (long long int)clock_time_start); //! ie, build a unique string by combining the memory-address and the unix-time. */
      /* stream_out = fopen(str_tmp, "w"); */
      /* if (stream_out != NULL) {       */
      /* 	fprintf(stderr, "!!\t Seems like your input-file-name \"%s\" was all to long: we have now shorted the file-name into \"%s\", and included the 'original-fiel-name' as a '#! fileName: ' tag in your result-file. For quesitons please cotnact the devleoper at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_resultFile, str_tmp, __FUNCTION__, __FILE__, __LINE__); */
      /* 	//! Add the file-name as a 'tag': */
      /* 	fprintf(stream_out, "#! fileName: \t%s\n", str_tmp); */
      /* } */
      /* stringOf_resultFile = str_tmp; */
    }
    if (stream_out == NULL) {      
      fprintf(stderr, "!!\t Unale to open the file \"%s\" for writing: please validate that the directory (assicated to the file) both exists and is writale: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
      return;
    }
  } else {self->stream_out = stdout;}
  // printf("sets the stream-out variable, at %s:%d\n", __FILE__, __LINE__);
  self->stream_out = stream_out;
  assert(self->stream_out);
  //! ------------------------
  //!
  //! Udpate generic proerpteis:
  self->isTo_useMatrixFormat  = isTo_useMatrixFormat;
  self->isTo_inResult_isTo_useJavaScript_syntax  = isTo_inResult_isTo_useJavaScript_syntax;
  // printf("isTo_inResult_isTo_useJSON_syntax=%u, at %s:%d\n", isTo_inResult_isTo_useJSON_syntax, __FILE__, __LINE__);
  self->isTo_inResult_isTo_useJSON_syntax  = isTo_inResult_isTo_useJSON_syntax; 
  self->stringOf_header  = stringOf_header;
  self->include_stringIdentifers_in_matrix  = include_stringIdentifers_in_matrix;
  self->isTo_writeOut_stringHeaders  = (stringOf_header != NULL); //isTo_writeOut_stringHeaders;  
}

//! Cofnigure the file-export-routine:
void export_config__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile) {
  // printf("Configures the exprot-rotuine given stringOf_resultFile=\"%s\", at %s:%d\n", stringOf_resultFile, __FILE__, __LINE__);
  assert(self);
  export_config_extensive_s_kt_matrix_t(self, stringOf_resultFile, 
					/*isTo_useMatrixFormat=*/true, 
					/*isTo_inResult_isTo_useJavaScript_syntax=*/false,
					/*isTo_inResult_isTo_useJSON_syntax=*/false,
					/*stringOf_header=*/NULL,
					/*include_stringIdentifers_in_matrix=*/false,
					/*cnt_dataobjects_togenerate=*/1);
}


/**
   @brief write out the obj to the stream as set of relations.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stringOf_predicate> which if set is used as predicate when writing out the data, eg, to simplify interpreation of the relationships.
   @param <stream> is the poitner to the restul-stream.
   @param <isTo_useJavaScript_syntax> which if set to true implies that we makes use of a java-script syntax.
   @param <isTo_useMatrixFormat> which if set to false impleis that we exprot using a relation-centered format.
 **/
void export_dataSet_s_dataStruct_matrix_dense_relations__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_header, const char *stringOf_predicate, FILE *stream, const bool isTo_useJavaScript_syntax, const bool isTo_useMatrixFormat) {
  if( (self->nrows == 0) || (self->ncols == 0) 
      //|| (self->nameOf_rows == NULL)       || (self->nameOf_columns == NULL)
      || (self->matrix == NULL)
      ) {
    fprintf(stderr, "!!\t The object was not intliased as expected, ie, please update your call: for quesitons please cotnact the develoepr at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }
  assert(self); assert(self->matrix); 
  /* assert(self->nameOf_rows); assert(strlen(self->nameOf_rows[0])); */
  /* assert(self->nameOf_columns); assert(strlen(self->nameOf_columns[0])); */
  //!
  //! Intiate a wrapper-object:
  s_dataStruct_matrix_dense_t data_obj; // = self->data_obj;
  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
  data_obj.cnt_rows = self->nrows;
  data_obj.cnt_columns = self->ncols;
  data_obj.matrixOf_data = self->matrix;
  data_obj.nameOf_rows = self->nameOf_rows;
  data_obj.nameOf_columns = self->nameOf_columns;

  //! 
  //! 
  export_dataSet_s_dataStruct_matrix_dense_matrix_tab(&data_obj, self->nameOf_rows, self->nameOf_columns, stringOf_header, stream,  /*include_stringIdentifers_in_matrix=*/true, /*isTo_writeOut_stringHeaders=*/true);

  //! Reset the input-object:
  //  init_s_dataStruct_matrix_dense_t_setToEmpty(&data_obj); //! ie, set the object-variables to empty.
}


//! A fucntion where we use the file-out-configuratiosn in "self" and the matrix-data in data_obj to udpate the result-file.
void export__dataObj_seperateFromFileObj__s_kt_matrix_t(s_kt_matrix_t *self, s_kt_matrix_t *data_obj, const char *stringOf_header) {
  assert(self);
  //! ------------------------------------------------
  //! Get access to the configuraitons:
  FILE *stream_out = self->stream_out; assert(stream_out);
  //printf("\t stream_out=%p, at %s:%d\n", stream_out, __FILE__, __LINE__);
  const bool isTo_inResult_isTo_useJavaScript_syntax = self->isTo_inResult_isTo_useJavaScript_syntax;
  //printf("isTo_inResult_isTo_useJSON_syntax=%u, at %s:%d\n", isTo_inResult_isTo_useJSON_syntax, __FILE__, __LINE__);
  const bool isTo_inResult_isTo_useJSON_syntax = self->isTo_inResult_isTo_useJSON_syntax; 
  const bool isTo_useMatrixFormat = (self->isTo_useMatrixFormat || isTo_inResult_isTo_useJSON_syntax || isTo_inResult_isTo_useJavaScript_syntax);
  //const char *stringOf_header; // = self->stringOf_header;
  const bool include_stringIdentifers_in_matrix = self->include_stringIdentifers_in_matrix;
  const bool isTo_writeOut_stringHeaders = (stringOf_header != NULL); //self->isTo_writeOut_stringHeaders;
  const bool isLast = ( (self->cnt_dataObjects_toGenerate + 1) == self->cnt_dataObjects_written);
  //! ------------------------------------------------
  if (stream_out == NULL) {
    fprintf(stderr, "!!\t The result-file was not opened: did you remeber to call the \"export_config__s_kt_matrix_t(..)\" before 'this fucntion-call' was made?: if the latter does not help, then please contact the developer at [oekseth@gmail.com]. Observation at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);
    return;
  }  

  
  //! -----------------------
  //!
  //! Handle specific cases wrt. formats:
  /* if(self->isTo_inResult_isTo_useJSON_syntax && (self->cnt_dataObjects_written > 0) ) { */
  /*   //! Then add a speratro for the JSON-data-set: */
  /*   fprintf(stream_out, ",\n"); */
  /* } */
  
  const bool isLast_timeThisObjectIsCalled = ((1 + self->cnt_dataObjects_written) == self->cnt_dataObjects_toGenerate);

  //! -----------------------

      //printf("at %s:%d\n", __FILE__, __LINE__);
      export_dataSet_s_dataStruct_matrix_dense_relations__s_kt_matrix_t(data_obj, stringOf_header, stringOf_header, stream_out, isTo_inResult_isTo_useJavaScript_syntax, isTo_useMatrixFormat);
      //export_dataSet_s_dataStruct_matrix_dense_matrix_tab_JSON__s_kt_matrix_t( data_obj,  stringOf_header, stream_out, (bool)include_stringIdentifers_in_matrix, /*isTo_writeOut_stringHeaders=*/(self->cnt_dataObjects_written == 0), isLast, stringOf_header);

  //! -----------------------
  //!
  //! Compelte:
  self->cnt_dataObjects_written++; //! ie, udpate the count.
  
}

//! Wirte out the matrix to the format specified through the set fo inptu-aprameters (oekseth, 06. nov. 2016).
void export__s_kt_matrix_t(s_kt_matrix_t *self) {
  assert(self); 
  //printf("\t stream_out=%p, at %s:%d\n", self->stream_out, __FILE__, __LINE__);
  export__dataObj_seperateFromFileObj__s_kt_matrix_t(self, self, self->stringOf_header);
}

//! An explcit approach to clsoe the file-handler.
//! @rmekars an explampel-applciaiton conserns the sue-case where one is interested in 'using the result-fiel before the compelte proeprties of this object is freeed'.
void closeFileHandler__s_kt_matrix_t(s_kt_matrix_t *self) {
  assert(self);
  if(self->stream_out != NULL) {
    if(
       (self->stream_out != stderr) &&
       (self->stream_out != stdout)
       ) {
      fclose(self->stream_out); self->stream_out = NULL;
    }
  }
}

/**
   @struct s_kt_longFilesHandler
   @brief provide logics to support the sue of long file-names, ie, file-naems which includes a 'bunch' of emta-ifnromation.
   @author Ole Kristian Ekseth (oekseth, 06. des. 2016).
**/
typedef struct s_kt_longFilesHandler {
  FILE *stream_out;
  bool stream_out__isAllocated;
  long long unsigned int currentCounter;
  const char *result_directory;
} s_kt_longFilesHandler_t;


/**
   @brief a signlce call to exprot the result-file (oekseth, 06. des. 2016).
   @param <self> is the object to 'hold' the export-file
   @param <stringOf_resultFile> which is the anem of the result-file.
   @param <fileHandler> optional: may be set for cases wehre the file-naem 'repsr ents' a complex string of meta-file-proerpteis.
   @return true upon success.
 **/
bool export__singleCall__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler) {
  assert(self);
  //!
  //! Teh call:
  char *string_alloc = NULL;
  /* if(fileHandler != NULL) { */
  /*   stringOf_resultFile = string_alloc = allocateDenseFileName__s_kt_longFilesHandler_t(fileHandler, stringOf_resultFile, /\*prefix=*\/"matrix."); */
  /*   assert(stringOf_resultFile); assert(strlen(stringOf_resultFile)); */
  /* } */
  //!
  //! Itnaite the file:
  export_config__s_kt_matrix_t(self, stringOf_resultFile);
  if(self->stream_out != NULL) {
    export__s_kt_matrix_t(self);
    //!
    //! Close:
    closeFileHandler__s_kt_matrix_t(self);
  } else {
    fprintf(stderr, "!!\t Unable to open the file=\"%s\", ie, please investigate: observation at [%s]:%s:%d\n", stringOf_resultFile, __FUNCTION__, __FILE__, __LINE__);
    return false; //! ie, as we were Not able to open the result-fiel for writringg, eg, due to 'erronosu direcotyr-path' or 'not enough space on disk to wrte'
  }

  if(stringOf_resultFile && strlen(stringOf_resultFile)) {
    /* if(false) { //! then we alos write out a PPM image: */
    /*   const uint str_local_size = strlen(stringOf_resultFile) + 100; */
    /*   const char def_0 = 0; */
    /*   char *str_local = allocate_1d_list_char(str_local_size, def_0); */
    /*   sprintf(str_local, "%s.image.ppm", stringOf_resultFile); */
    /*   ppm_image_t *picture = exportTo_image ( self->matrix, self->nrows, self->ncols, self->nameOf_rows, self->nameOf_columns); */
    /*   ppm_image_write ( str_local, picture ); */
    /*   ppm_image_finalize ( picture ); */
    /*   free_1d_list_char(&str_local); str_local = NULL; */
    /* } */
  } //! else we asusme this is Not of itnerest, ie, to avoid 'minixnxg' the sdtou-stream with an image.
  if(fileHandler != NULL) {
    assert(string_alloc);
    free_1d_list_char(&string_alloc); string_alloc = NULL;
  }  
  //! ---------------
  return true; //! ie, as we asusme the oerpation was a success at this exeuciton-point
}

/**
   @brief copies a trasnposed version of a given matrix 'into this' (oekseth, 06. des. 2016).
   @param <self> the object to both itnaite and update
   @param <superset> is the 'input' to copy data from
   @param <isTo_updateNames> which if set to true impleis that we also copy the subset of the naems 'into the new object.
   @return true if the operaion was a success.
   @remarks is among other used when evlauating dsijoitn sub-regions in a matrix, for which our "kt_forest_findDisjoint" struuct-relogics idneitfy disjoitn-regions.
 **/
bool init__copy_transposed__s_kt_matrix(s_kt_matrix_t *self, const s_kt_matrix_t *superset, const bool isTo_updateNames) {
  assert(self); assert(superset);
  assert(self != superset); //! ie, as we otherwise will ahve a cyclic update, ie, both potiness and 'coudl be infinte for some un-expected case' (ie, based o our programming-exper4encE).
  setTo_empty__s_kt_matrix_t(self); //! ie, intiate.
  //!
  //! Apply the logics:
  //#define __localConfig__copyWeights 1
#define __localConfig__useRanks 0
#include "kt_matrix__stub__buildSubset__useRanks__transposed.c"

  //!
  //! At this exueciotn-point we asusme the operaiotn was a succcess:
  return true;  
}

/**
   @brief a signlce call to exprot the result-file (oekseth, 06. des. 2016).
   @param <self> is the object to 'hold' the export-file
   @param <stringOf_resultFile> which is the anem of the result-file.
   @param <fileHandler> optional: may be set for cases wehre the file-naem 'repsr ents' a complex string of meta-file-proerpteis.
   @return true upon success.
 **/
bool export__singleCall_firstTranspose__s_kt_matrix_t(s_kt_matrix_t *self, const char *stringOf_resultFile, s_kt_longFilesHandler_t *fileHandler) {
  s_kt_matrix_t mat_t = setToEmptyAndReturn__s_kt_matrix_t();
  bool is_ok = init__copy_transposed__s_kt_matrix(&mat_t, self, /*isTo_updateNames=*/true);
  assert(is_ok);
  is_ok = export__singleCall__s_kt_matrix_t(&mat_t, stringOf_resultFile, fileHandler);
  free__s_kt_matrix(&mat_t);
  return is_ok;
}
