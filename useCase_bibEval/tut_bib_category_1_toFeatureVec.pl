#!/usr/bin/perl
use bib_categories;
use Carp;
use strict;

=head
    @brief provide a simple exempel to demostnrate how our "bib_categories" module maty be sued to classify differnet fields/topcis of reseharc ... 
    @remarks
    -- strength: titles are the terms which are often searhced for during reseharc. The challenge with key-words is that they are often tuend towards a given reviewer-ardueinec,e ie, instead of the large/btorad community of itnerestign resehar-appraoches. In thsi work we seek to address the latter by using a "newtons emthod" ot itertively/manully idneitfy gorups to titles based on an tial reading of 600+ reseehar-cariltces. 
    -- weakenss:  
=cut

#! 
#! Initialize: 
my $obj_bib = hpLysis::curation::bib_categories::init__bib_categories();

#! 
#! Configure:
my $bib_conf = hpLysis::curation::bib_categories::initConf__get_groupVector_forString();



#! 
#! Start evaluation: 
my @arrOf_strings_1 = (
    "The International Race of Top Supercomputers and Its Implications",
    "The Norwegian University of Science and Technology, met.no buy new supercomputer", 
    "Mining big data: current status, and forecast to the future",
    "l-dbscan: A fast hybrid density based clustering method",
    "k-means++: The advantages of careful seeding",
    "X-means: Extending K-means with Efficient Estimation of the Number of Clusters.",
    "Genetic growth differentiation in guinea pigs",
    "Homology-directed repair of DNA nicks via pathways distinct from canonical double-strand break repair",
    "The ERATO Systems Biology Workbench: enabling interaction and exchange between software tools for computational biology",
     "Key cost drivers of pharmaceutical clinical trials in the United States",
     "Fact Sheet: New Drug Development Process",
     "Usability testing of mobile ICT for clinical settings: Methodological and practical challenges",
"Pathways with PathWhiz",
"SABIO-RK—database for biochemical reaction kinetics",
"Tough mining"
    );

# #! 
# #! Start evaluation: 
 my @arrOf_strings = (
     "Artificial Intelligence a moden appraoch"
    );
#     );
#! 
#! The call:
foreach my $title (@arrOf_strings) {
    hpLysis::curation::bib_categories::get_groupVector_forString($obj_bib, $title, $bib_conf);
}
