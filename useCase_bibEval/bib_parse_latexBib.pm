#!/usr/bin/perl 
package hpLysis::curation::bib_parse_latexBib;
use strict;
use Carp;
use bib_categories;
#use IO::CaptureOutput;
$Carp::Verbose = 1;
use warnings "all";
use warnings::register;

=head
    @brief written in order to simplify artilce-generation from our 'native' HTML format (oekseth, 06. apr. 2016).

# TODO: ... updae ... java-script ... when a user hoovers over an entry then update a/the pargraph with the latex entry (ie, a HTML-ified version of the latter, using the fields) .... where a javascript-table is accessed 'during this operaiton' .... OR ... two tables ... hwere fiedst tiable is a summary/overfiew wr.t the observiatons ..... <-- dropped.
# TODO: PubMed ... request keywords and citaiton-list for all of the artilces .... 
# TODO: 
# TODO: 

=cut

#! @return an tinlzied veirosn of the bib-catoeries (oekseth, 06. otk. 2017).
sub init__bib_parsing {
    #! 
#! 
#! Initialize: 
    my $obj_bib          = hpLysis::curation::bib_categories::init__bib_categories();
    my $obj_bib_journals = hpLysis::curation::bib_categories::init__bib_categories();
    my $obj_bib_journals_merged = hpLysis::curation::bib_categories::init__bib_categories();
    #! 
    #! 
    my $self = {
	obj_bib => $obj_bib, 	
	obj_bib_journals => $obj_bib_journals, 	
	obj_bib_journals_merged => $obj_bib_journals_merged, 	
	hashOf_keys => {},
    };
    return $self;
}

sub  __get_arrOf_bibKeys {
    my $arrOf_bibKeys = [
	"year",
	"meta: category-count", 
	"journal",
	"title",
	"author",
	"volume ",
	"number",
	"pages",
	"publisher",
	"booktitle ",
	#! ---------------
	"url",
	"howpublished",
	"institution",
	"organization ",
	"month",
	];
    return $arrOf_bibKeys;
}

sub __init_bibEntry {
    my ($cit_key) = @_;
    #! 
    #! Get arr of bib-keys
    my $arrOf_bibKeys = __get_arrOf_bibKeys();
    my $obj_hash = {};
    foreach my $key (@{$arrOf_bibKeys}) {
	$obj_hash->{$key} = "";
    }
    if(defined($cit_key)) {
	$obj_hash->{key} = $cit_key;
    }
    $obj_hash->{"year"} = 0; #! ie, def. ini
    return $obj_hash;
}

#! Provide/ add logics to support 'inclusion' of bib-tex-ciation ... ie, parse the bib-text-entries:
#! 
#! Example: "citations_useCase_mineDB.bib"
sub parse {
    my ($self, $arr_bib_file, $resultPrefix, $conf_bib) = @_;
    #! 
    #!     
    my $name_features_tsv = $resultPrefix  . "_" . "features.tsv";
    my $name_features_html = $resultPrefix . "_" . "features.html";
    my $name_bib_html = $resultPrefix . "_" . "bib.html";
    my $code_location = sprintf("%s:%d", __FILE__, __LINE__);
    open(FILE_RESULT_FEATURES_TSV, ">$name_features_tsv") or die("Unable to construct open file-pointer for file=\"$name_features_tsv\", at $code_location\n");
    $code_location = sprintf("%s:%d", __FILE__, __LINE__);
    open(FILE_RESULT_FEATURES_HTML, ">$name_features_html") or die("Unable to construct open file-pointer for file=\"$name_features_html\", at $code_location\n");
    $code_location = sprintf("%s:%d", __FILE__, __LINE__);
    open(FILE_RESULT_BIB_HTML, ">$name_bib_html") or die("Unable to construct open file-pointer for file=\"$name_bib_html\", at $code_location\n");
    #! 
    #! Get arr of bib-keys
    my $arrOf_bibKeys = __get_arrOf_bibKeys();
    #! 
    #! 
    { #! Headers for fiels:
	{ #! Main-bib header: 
	    printf(FILE_RESULT_BIB_HTML  qq( <table> <thead> <tr> ));
	    foreach my $key (@{$arrOf_bibKeys}) {
		printf(FILE_RESULT_BIB_HTML "<th colspan=\"1\"> %s</th> ", $key);
	    }
	    printf(FILE_RESULT_BIB_HTML  qq(     </tr> 
  </thead>
  <tbody>
));		   
	}
	#! ------------------
	{ #! Main-feature header: 

	    printf(FILE_RESULT_FEATURES_TSV "\n");
	    printf(FILE_RESULT_FEATURES_HTML  qq( <table> ));
	}
	#! ------------------
    }
    #! 
    #! Deleate resposinbel for writing out files to the "bib_categories.pm" module: 
    $conf_bib->{file_pointer} = \*FILE_RESULT_FEATURES_TSV;
    $conf_bib->{file_pointer_html} = \*FILE_RESULT_FEATURES_HTML;
    $conf_bib->{isTo_writeOut} = 1;
    #! 
    #! 
    my $obj_curr = __init_bibEntry();
    my %hashOf_citations;
    my %hashOf_journals;
    foreach my $bib_file (@{$arr_bib_file}) {
	#! Read through the bibtex-ctiations:
	#my $bib_file = "slides_citation.bib";
#! Iterate through the file:
	open (INPUT, $bib_file) || die { print "Was unable to open the file \"$bib_file\", ie, might be that the latter fiel is either not readable, or actually does not exists (eg, please check the correctness of your callers path).\n"};    
	# my $file_name_result = "debug_mappingFile.txt";
	# my $FILE_RESULT = undef;
	# my $code_location = sprintf("%s:%d", __FILE__, __LINE__);
	# open($FILE_RESULT, ">$file_name_result") or die("Unable to construct open file-pointer for file=\"$file_name_result\", at $code_location\n");	       
	my $curr_url = undef;
	while (my $line = <INPUT>) {
	    chomp($line);
	    if($line =~/^\s*$/) {next;} #! ie, as we then assume 'the row is empty'
#	    } elsif($line =~ /(.+?)/) {
	    if($line =~ /^%!\s*.cite.*\=.*\"(.+)\"$/) {
#	    if($line =~ /^%!\s*.cite(.+)$/) {
#	    if($line =~ /^%! (.+)$/) {
#    } elsif($line =~ /^%.*cite.+(htt.+?)/) {
#	    } elsif($line =~ /^%.*cite.+\=\"(htt.+?)\"/) {
		$curr_url = $1;
		#printf("\n\n(set-url)\t %s\t at %s:%d\n", $curr_url, __FILE__, __LINE__);
	    } elsif($line =~/^\%/) {next;} #! ie, as we then assuem the file/dataset is not of interest.
	    
	    #! 
	    #! Adjsut the inptu-line:
	    $line =~ s/\s+/ /g; #! ie, to repalce muliople 'balnsk'w ith a single balnk

	    if($line =~ /^\@/) { #! Then (a) write out rpevious, and (b) initaite. 
		my ($cit_key) = ($line =~ /^\@[a-z]+\{(.+?),/i);
		#! 
		#! Add previous colected URL to the 'set':
		my $title = $obj_curr->{title};
		if(defined($title) && length($title)) {
		    my $key = $obj_curr->{key};
		    if(!defined($hashOf_citations{$key})) { #! then the entry is Not alaredy known:
			my $obj_title = hpLysis::curation::bib_categories::get_groupVector_forString($self->{obj_bib}, $title, $conf_bib, $obj_curr->{key}, $obj_curr->{year});
			my $cnt_cat = $obj_title->{foundIn_cntCat};
			$obj_curr->{"meta: category-count"} = $cnt_cat;
			$hashOf_citations{$key} = $obj_curr; #! ie, store the key.
			#! 
			#! Update the meafeature-ctor for the givne journal:
			my $name = $obj_curr->{"journal"};
			if(defined($name) && length($name)) {
			    $name = lc($name);
			    for(my $cat_index = 0; $cat_index < scalar(@{$obj_title->{featureRow}}); $cat_index++) {
				my $score = $obj_title->{featureRow}->[$cat_index];
				if(scalar(@{$hashOf_journals{$name}->{features}}) == 0) {
				    push(@{$hashOf_journals{$name}->{features}}, 0); #! ie, intailize.
				}
				$hashOf_journals{$name}->{features}->[$cat_index] += $score;
			    }
			}
		    }
		}
		#! 
		#! 
		$obj_curr = __init_bibEntry($cit_key);
		if(defined($curr_url) && length($curr_url)) {
		    $obj_curr->{url} = $curr_url;
		}
		$curr_url = undef;
		#! 
		#! 
		if($line =~ /^\@.+\{(.+?),\s*\%.+\"(.+?)\".*/i) {		    
		    my $cit_key = $1; my $url = $2;
		    #$hashOf_citations{$url} = $cit_key;
		    if(defined($url) && length($url)) {
			$obj_curr->{url} = $url;
		    }
		    #printf($FILE_RESULT "%s --> %s\n", $cit_key, $url);
		} # else {
		#     printf("!!\t investigate why the folloiwn line does not have an explict URL-HTML reference: line: %s\n", $line);
		# }
	    } else {
		my($key, $value) = ($line =~/^\s*([a-z]+)\s*\=\s*(.+)$/i);
		if(defined($key) && length($key)) {
		    $key = lc($key);
		    if(defined($obj_curr->{$key})) {
			#! Remove all sepvial symbols, eg,  {{ .. }}, before inseritng the stirng ... 
			$value =~s/[\{\}\n]//g;
			if($value =~/^(.+),\s*$/) {$value = $1;} #! eg, to remove a trialing ",".
			if($value =~/^\"(.+)\"\s*$/) {$value = $1;} #! eg, for "...".
			# printf("set-key: $key\n"); 
			if( ($value ne ",") && length($value)) {
			    $obj_curr->{$key} = $value;
			}
			if($key eq "journal") { #! then udpate the jorunal-count:
			    $value = lc($value);
			    if(!defined($hashOf_journals{$value})) {
				$hashOf_journals{$value} = {count => 0, features => []};
			    }
			    $hashOf_journals{$value}->{count}++;
			}
		    }
		}
		#! 
		#! Test if we are 'in the middle' of an evalaution-lopp.
		# TODO: cosndier adding supproit for mlti-line tags.
	    }
	    
	}
	close(INPUT);
    }
    #! 
    #! 
    { #1 Handle the last inserted bib-entry:
	#! 
	#! Add previous colected URL to the 'set':
	my $title = $obj_curr->{title};
	if(defined($title) && length($title)) {
	    my $key = $obj_curr->{key};
	    if(!defined($hashOf_citations{$key})) { #! then the entry is Not alaredy known:
		my $obj_title = hpLysis::curation::bib_categories::get_groupVector_forString($self->{obj_bib}, $title, $conf_bib, $obj_curr->{key}, $obj_curr->{year});
		#my $obj_title = hpLysis::curation::bib_categories::get_groupVector_forString($self->{obj_bib}, $title, $conf_bib, $obj_curr->{key});
		#! 
		#! 
		my $cnt_cat = $obj_title->{foundIn_cntCat};
		$obj_curr->{"meta: category-count"} = $cnt_cat;
		$hashOf_citations{$key} = $obj_curr; #! ie, store the key.
	    }
	}
    }
    { #! Sort the entries by 'year' and then write out the result of the bib-entries which we have elvauted. 
	my $hash = \%hashOf_citations;
	my $sort_key = "year";	
	foreach my $key_out (sort { $hash->{$b}{$sort_key} <=> $hash->{$a}{$sort_key} } keys %{$hash}) {
	    my $obj_art = $hash->{$key_out};
	    printf("year:%s\t entropy:%u\t %s\n", 
		   $obj_art->{"year"}, 
		   $obj_art->{"meta: category-count"},
		   $obj_art->{"title"});
#	foreach my $key_out (sort { $hash->{$b}{$sort_key} <=> $hash->{$a}{$sort_key} } keys %{$hash->{$key}}) {
	    ; 
	    
	    printf(FILE_RESULT_BIB_HTML  qq(     \n<tr> )); 
	    foreach my $key (@{$arrOf_bibKeys}) {
		printf(FILE_RESULT_BIB_HTML "<td colspan=\"1\"> %s</td> ", $obj_art->{$key});
	    }
	    printf(FILE_RESULT_BIB_HTML  qq(     </tr> \n)); 
	}
    }    
    #! ------------------
    { #! Main-feature header: 	
	#   printf(FILE_RESULT_FEATURES_TSV "\n");
	printf(FILE_RESULT_FEATURES_HTML  qq( </tbody> </table> ));
	printf(FILE_RESULT_BIB_HTML  qq( </tbody> </table> ));
    }
    close(FILE_RESULT_FEATURES_TSV);
    close(FILE_RESULT_FEATURES_HTML);
    close(FILE_RESULT_BIB_HTML);
    #! 
    #! Write otut hte meta-files:
    hpLysis::curation::bib_categories::close_andWriteOut($self->{obj_bib}, $resultPrefix . "_meta");
    { #! Writ eout attirbutes of the jounrals, sorted by the number of related artilces.
	my $result_jorun = $resultPrefix . "_journ_";
	my $name_features_tsv = $result_jorun  . "_" . "features.tsv";
	my $name_features_html = $result_jorun . "_" . "features.html";
	my $code_location = sprintf("%s:%d", __FILE__, __LINE__);
	open(FILE_RESULT_FEATURES_TSV, ">$name_features_tsv") or die("Unable to construct open file-pointer for file=\"$name_features_tsv\", at $code_location\n");
	$code_location = sprintf("%s:%d", __FILE__, __LINE__);
	open(FILE_RESULT_FEATURES_HTML, ">$name_features_html") or die("Unable to construct open file-pointer for file=\"$name_features_html\", at $code_location\n");

	#! 
	#! Deleate resposinbel for writing out files to the "bib_categories.pm" module: 
	my $conf_bib_journ = hpLysis::curation::bib_categories::initConf__get_groupVector_forString();
	$conf_bib_journ->{file_pointer} = \*FILE_RESULT_FEATURES_TSV;
	$conf_bib_journ->{file_pointer_html} = \*FILE_RESULT_FEATURES_HTML;
	$conf_bib_journ->{isTo_writeOut} = 1;
	#! 
	#! Write out for each: 
	my $hash = \%hashOf_journals;
	foreach my $key_out (sort { $hash->{$b}->{count} <=> $hash->{$a}->{count} } keys %{$hash}) {
	    my $journ_count = $hashOf_journals{$key_out}->{count};
	    my $title = "count: $journ_count . $key_out";
	    my $obj_title = hpLysis::curation::bib_categories::get_groupVector_forString($self->{obj_bib_journals}, $title, $conf_bib_journ, $title);
	    # 			    $hashOf_journals{$value}->{features}++;
	    #! 
	    #! 
	    #my $cnt_cat = $obj_title->{foundIn_cntCat};	    
	}
	#! Writ eout summary: 
	hpLysis::curation::bib_categories::close_andWriteOut($self->{obj_bib_journals_merged}, $result_jorun . "_meta");
	#! Close local file-hanlders: 
	close(FILE_RESULT_FEATURES_TSV);
	close(FILE_RESULT_FEATURES_HTML);
    }
    #! -------------------------------------
    #! 
    #! Summary colelced from the ariltces (rather thant he journal name,s).
    { #! Writ eout attirbutes of the jounrals, sorted by the number of related artilces.
	my $result_jorun = $resultPrefix . "_journ_merged_";
	my $name_features_tsv = $result_jorun  . "_" . "features_.tsv";
	my $name_features_html = $result_jorun . "_" . "features.html";
	my $code_location = sprintf("%s:%d", __FILE__, __LINE__);
	open(FILE_RESULT_FEATURES_TSV, ">$name_features_tsv") or die("Unable to construct open file-pointer for file=\"$name_features_tsv\", at $code_location\n");
	$code_location = sprintf("%s:%d", __FILE__, __LINE__);
	open(FILE_RESULT_FEATURES_HTML, ">$name_features_html") or die("Unable to construct open file-pointer for file=\"$name_features_html\", at $code_location\n");

	#! 
	#! Deleate resposinbel for writing out files to the "bib_categories.pm" module: 
	my $conf_bib_journ = hpLysis::curation::bib_categories::initConf__get_groupVector_forString();
	$conf_bib_journ->{file_pointer} = \*FILE_RESULT_FEATURES_TSV;
	$conf_bib_journ->{file_pointer_html} = \*FILE_RESULT_FEATURES_HTML;
	$conf_bib_journ->{isTo_writeOut} = 1;
	#! 
	#! Write out for each: 
	my $hash = \%hashOf_journals;
	foreach my $key_out (sort { $hash->{$b}->{count} <=> $hash->{$a}->{count} } keys %{$hash}) {
	    my $title = $key_out;
	    my $journ_count = $hashOf_journals{$key_out}->{count};
	    my $cit_key = "count: $journ_count . $key_out";
	    #my $obj_title = hpLysis::curation::bib_categories::get_groupVector_forString($self->{obj_bib_journals}, $title, $conf_bib_journ, $title);
	    hpLysis::curation::bib_categories::writeOut_featureVector($self->{obj_bib_journals_merged}, $title, $conf_bib_journ, $cit_key, $hashOf_journals{$key_out}->{features});
	    # 			    $hashOf_journals{$value}->{features}++;
	    #! 
	    #! 
	    #my $cnt_cat = $obj_title->{foundIn_cntCat};	    
	}
	#! Writ eout summary: 
	hpLysis::curation::bib_categories::close_andWriteOut($self->{obj_bib_journals}, $result_jorun . "_meta");
	#! Close local file-hanlders: 
	close(FILE_RESULT_FEATURES_TSV);
	close(FILE_RESULT_FEATURES_HTML);
    }
}

=head

=cut

1;
