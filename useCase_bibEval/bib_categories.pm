#!/usr/bin/perl
package hpLysis::curation::bib_categories;
use Carp;
use strict;
#use format_globalIDs;



#! 
#!  @return a meta-list of keywrod-classes inferred from a manual isnpeciton of approx. 600 reseharc-arilteces (oesketh, 06. otk. 2017)
sub __get_bib_categories {
    
    #! 
    #! Organise the groups
    my $groupToIndex = {
	"survey"=>0, 
	"network"=>1,
	"model"=>2, 
	"database"=>3, 
	"biology"=>4, 
	"quality"=>5, 
	"engineering"=>6, 
	"software"=>7, 
	# ----------------------
	"network(challenges+tasks)"=>8, 
	"network(eval)"=>9, 
	"network(datasets)"=>10, 
	"semantic"=>11, 
	"literature"=>12, 
	"cluster"=>13, 
	"similarity"=>14, 
	"mine strategy"=>15, 
	"database(engine)"=>16, 
	"similarity(change)"=>17, 
	"similarity(network)" => 18,
	"performance"=>19, 
	"method(computer)"=>20, 
	"integration"=>21, 
	"processing"=>22, 
	"processing(tacit)"=>23,
	"engineering(site)"=> 24,
	"engineering(testing)" => 25, 
	"emotional" => 26,
    };
    


    return ({	
	survey => ["review", "survey", "evaluation", "comparison", "Comparative", "overview", "analysis", "guide", "study","future directions","current trends"], 

	network => ["network", "system", "analysis", "graph, group", "database networking", "organisation", "data", "dataset", "semantic", "provenance", "accumulation", "big data","big data", "linked data", "graph", "dataset", "social network", "system", "web", "ontology", "resource","knowledge","information", "informatica", "high throughput", "feature", "information sources", "archive"], 
	"network(datasets)" => ["time series","ontology","news stories","expression","synthesis","Facebook","silicon cell","patient data","art", "BioPax", "blog", "social", "economic"], 
	"network(eval)" => ["network", "benchmark","workload","Performance characterization"],
	
	"network(challenges+tasks)" => ["network", "interaction", "association", "integration", "integrating", " accumulation", "Representing", "transforming", "tinkering", "Targeting", "retrieval", "integrating", "standardization", "paramter", "discovery", "discovering","hypothese", "regulate", "processes", "data integration","High dimensional","mechanisms","alignment", "bridge", "unification", "sharing", "standard", "design principles","resources","Deciphering","consistent",
					"data integration","disparate sources"	    ],
	    
	    "model" => ["model", "modelling","network reconstruction","modular","structure","grid", "partition", "topological", "classification", "recognition", "pattern", "motif", "Quantitative", "representation", "analysis", "interpretation","variance","variation","differential","model based", "structure", "relatedness", "function", "functional", "functional","association", "ontology", "Ontological", "lexical", "centralitie", "organization", "mechanism", "map", "Circuit diagrams","Systematic Discovery",
			"Predicting", "forecasting", "forecast", 
			"multivariate data"],
	
	
	"database" => ["database", "knowledgebase","database engine", "rdbms", "vldb", "bdms","information system","storing","Retrieval","record","semantic","registry","exchange","collection","registries","biobanks","diseases","diseasome","disease","Social Connections","social network", "collection", "RDF","linked data","knowledge systems"],
	
	"semantic"  => ["semantic", "transitive", "chain link", "Entscheidungsproblem", "studia logica", "ontology", "ontologie", "language", "querie", "rule", "query", "reasoner", "reasoning", "bio ontologies","triple stores","knowledge base", "annotation", "taxonomy", "integration", "processes", "OBO", "OWL", "description logic","Classification","EL","natural language","RDF","SPAR QL","SPARQL"],
	
	"literature" => ["litterature", "semantic", "concept", "document", "concept discovery"],
	
	"biology" => ["biology", "biological", "biol", "nature", "the lancet", 
		      "proteome","proteomic", "genomic", "genome", "genet",  "genetic", "interactome", "evolution", 
		      "interactomic", "molecular", "life science", "bioinformatic", "bioinformatician", "bioscience", "biomedical", "biochemical", "ecology","pharmacological","pharmaceutical", "biotechnology", "biosystems",  "neuroscience", "bioengineering", "psychiatry", "psychology", "biotechnol", "veterinary", "pathology", 
		      "functional", "homology", "cell","cell cycle","metabolic", 
		      "transposable","immune systems","protein folding","epigenetic", "eugenics", "eicosanoid",
		      "inflammation","Chronic","Influence",  "heredity", 
		      "sequencing","co expressed","microarray", "regulate", 
		      "pathway", "sequence", "gene product", "chromosome","molecular","macromolecule", 
		      "peptide",  "nucleic", "DNA","RNA", "gene", "protein", "cell type", "transcription", " Clinical", "Biomolecular", "brain", 
		      "tumor","tumours","cancer","Infection","Alcoholism","obesity","rheumatoid","arthritis","Alzheimer's", "fertility", 
		      "chemical", 
		      "GO","Gene Ontology",
		      "drug", "medicine","healthcare", "medical", "health", 
		      "clinical", "trial",
		      "Mendelian", 
		      "mice","human","Escherichia","yeast","Herpesviral", "virus","viruses", "macrophage", "guinea", "plant"    ],
	
	
	"cluster" => [
	    "cluster", "clustering","Modularity","community","Superfamilies","structure","Organization", "centrality", "familie", 
	    "small world", "ungrouped data", "Community structure",			      
	    "connection","shortest", 
	    ],
	    
# --------------------------------
	    "similarity" => ["distance","relatedness","proximity","Alignments","network analysis", "distinct", "unique",
			     "mutual information","Minimum information","entropy","information content","diversity","diverse","divergence","variability", 
			     "estimator", "estimation", "frequencies","information content","contingency","outlier detection", "co training", "probability","statistics","t test","Bonferroni","statistical","Least squares","Discriminative","Regression","analysis","distribution","bibliometric","variance", "growth"],
	    
	    "similarity(network)" => ["implication", "relation", "correlation", "implication", "causation"],
	    #! ---- 
	    "mine strategy" => ["empirical", "graphical", "recognition", "understand", "disambiguate", "unsupervised", "Classification", "learning","feature learning","feature selection","Discretization","analysis","k means","image filtering","hierarchical","agglomerative","nearest neighbor", 
				"data mining", "sensing", "artificial intelligence", "artif intell", "outlier mining","text mining","Semantic mining", "Automatic Extraction", 
				"mathematical", "mathematik", "mathematic", "principal component","iterative","Machine learning","bayesian","multiple testing","manhattan","euclid","shannon","false discovery","over representation","Boltzmann Gibbs"],
	    #! ---- 
	    "database(engine)" => ["sparse table","data structure","tree","heap","forest","Cache efficient","Database Backend"], 
	    #! ---- 
	    "similarity(change)" => [
		"random", "mutation", "variation", "evolution", "change", "surprise","adaptive","evolve","fluctuation","dynamics","Percolation","fragility","Irregular", 
		"identification","Sensitivity", "annotation"], 
	    "performance" => ["high performance","load balancing","cloud","Massively","Architectures","big data","large","complex","large scale", "high throughput", "memory", "memorization", "compression", "time", "quickly", "accurately", "high performance","high dimensional","computation","Computational","algorithms","measures","parallel","Multicore","Amdahl's law","Optimize","optimization","Optimising","optimized", "efficient","speedup","Accelerating","fast","heuristic","manipulating","GPU","FPGA","rapid computation"], 
	    #! ---- 
	    "method(computer)" => ["algorithms","computer","k means","spectral", "kd tree","k d tree", "density", "ieee", 
				   "similaritie", "distance", "similarity", "separation", "correlation", 
				   "eigenvector", 
				   "techniques","quadratic programming", 
				   "Cache efficient","compiler",  "cache", 
				   "correlation", "measure", " method", "criteria", "calculation"],
	    
	    "software" => ["software", "soft", "library", "computer", "supercomputer", "computation", "Automated", "framework", "open source","workbench","open source", "platform", "Architecture", "Application", "computational", "computing","comput",  "web based","web accessible", "tool", "Tooling","web service","web based","web server","web browser", "repository", "application", "API", "interactive", "visualization","HTML","HTML5","implementation","graphical","package","hardware","search engine","computer program"], 
			  # "application=[""]", "
# --------------------------------
			  
	    "integration" => ["data", "normalization", "procedure", "empirical"],
	    
	    "processing(tacit)" => ["buy", "advantage", "cost"],
	    "processing" => ["profiling", "process", "prediction", "diagnosis", "predicting", "forcasting",  "extraction", "quantization", "Measure", "design", "analysis", "analyzing", "search", "Systematic", "finding", "mine", "mining", "experiment", "management", "algorithm", "application", " effort", "navigation", "curation", "seeding", "interoperable", "management", "development", "exchange", "experimentalist", "sharing","combining","Comparing","workflow","integrate", "integration", "capturing","application", " supporting", " partitioning", "framework", "exploration", "   evolve", "evaluation", "method", "interoperability"],
	    
#! NotE: below term-cateogyr cotnains antonyms", "eg", "wrt.t eh "robus" key-word. The inclsuion/use of antonyms is based on the observat that \textit{if there s a researhc interest for 'rboust' clustering", "then there is a perccpetion that clustering often is 'sloppy'}. ... 
	    "quality" => ["Unintended", "inaccruate", "ambiguity", "confusion", "quality", "Robust", "robustness","fragility", "assessment", "stability", "sensititty", "interoperability", "bias", "for chance", "issue", "validation", "validity", "validating", "problem", "multiple truths"],

	    
	    
	    "engineering" => ["research", "science", "management", "Development", "gauging",  "engine", "comprehensive", "comparing", "promote", "innovative","Detecting","Predicting","significant","race", "problem","Improving"],
	    #! Note: "engineering(site)" is used to 'grab' partiularies to use-case
	    "engineering(site)" => ["university", "factory", "company", "stock marked", "united state", "intel", "pfam"],
	    "engineering(testing)" => ["clinical", "trial", "testing", "usability", "methodological"],
	    "emotional" => ["top", "tradeoff", "Effectively","Efficiently", "limitation", "Predicting", "forecast", "significant", "Improving", "advantage", "race", "key",  "cost",  "driver", "problem", "fast", "hybrid", "big", "challenge", "abuse"],
	    
    }
	    , $groupToIndex);
}



sub __addToCat {
    my ($wordsTo_features, $key, $word_1) = @_;
#    my ($wordsTo_features, $key, $word_1, $word_2) = @_;
    if(!defined($word_1)) {return;}
    $word_1 = lc($word_1); #! ie, to reduce searhc ocmplexity.
    #! 
    #! 
    if(!defined($wordsTo_features->{$word_1})) {
	$wordsTo_features->{$word_1} = [];
    }
    #! 
    #! 
    my $arr_prev = $wordsTo_features->{$word_1};
    my $is_found = 0;
    for(my $i = 0; $i < scalar(@{$arr_prev}); $i++) {
	if($is_found == 0) {
	    if(lc($key) eq lc($arr_prev->[$i])) {$is_found = 1;}
	}
    }
    if($is_found == 0) {
	# printf(STDERR "\t\t add-word\t%s\t%s\t, at %s:%d\n", $word_1, $key, __FILE__, __LINE__);
	push(@{$wordsTo_features->{$word_1}}, $key); #! ie, then add the key to the set. 
    }
    # if(defined($word_2) && length($word_2)) {
    # if(!defined($wordsTo_features->{$word_1}->{$word_2})) {
    # 	$wordsTo_features->{$word_1} = {};
    # }
}

#! @return an tinlzied veirosn of the bib-catoeries (oekseth, 06. otk. 2017).
sub init__bib_categories {
    #! 
    #! 
    #! 
    my ($hash_cat, $groupToIndex) = __get_bib_categories();
    my $self = {
	hash_ => $hash_cat,
	wordsTo_features => {},
	groupToIndex => $groupToIndex, 	
	groupToIndex_count => 0, 
	count_localCalls => 0, #! which is use dot remember the numbe rof calls to this function.
	resultStat => {
	    constant__yearOffset => 1960, #! ie, to 'merge' all entrices which have a publsiahble-year before 1980.
	    #constant__yearOffset => 1980, #! ie, to 'merge' all entrices which have a publsiahble-year before 1980.
	    map_cat_count => [], #! which is the number of occurence observed for a given artilce-title.
	    map_cat_count_numberEach => [], #! which is the number of occurence observed for a given artilce ... ie, where a title/aritlce may have muliple 'members' for the sme cateogyr. 
	    mat_extCat_x_cat => {}, #! eg, "{journal-id}{title-cateogry} = \d+"
	},
    };
    #!     
    my @featureRow_strings;
    { #! Find the max-count:
	foreach my $key (keys(%{$self->{groupToIndex}})) {
	    my $index = $groupToIndex->{$key};
	    if($index > $self->{groupToIndex_count}) {
		$self->{groupToIndex_count} = $index + 1;
	    }
	}
	for(my $i = 0; $i < $self->{groupToIndex_count};  $i++) {
	    push(@featureRow_strings, undef);
	    #! 
	    #! 
	    push(@{$self->{resultStat}->{map_cat_count}}, 0); 
	    push(@{$self->{resultStat}->{map_cat_count_numberEach}}, []); 
	    push(@{$self->{resultStat}->{map_cat_year_numberEach}}, []); 
	}
	foreach my $key (keys(%{$self->{groupToIndex}})) {
	    my $index = $groupToIndex->{$key};
	    $featureRow_strings[$index] = $key;
	    #printf("[$index] = \"$key\", at %s:%d\n", __FILE__, __LINE__);
	}
    }
    $self->{featureRow_strings} = \@featureRow_strings;
    #! 
    #! Cosntruct a hash of category--word relationships.
    foreach my $key (keys(%{$hash_cat})) {
	my $arr = $hash_cat->{$key};
	#my $word_prev = undef;
	for(my $i = 0; $i < scalar(@{$arr}); $i++) {
	    my $word_1 = $arr->[$i];
	    #! 
	    #! Add to category:
	    __addToCat($self->{wordsTo_features}, $key, $word_1);
	    #__addToCat($self->{wordsTo_features}, $key, $word_prev, $word_1);
	    # __addToCat($self->{wordsTo_features}, $word_prev);
	    #! 
	    #! 
	    #$word_prev = $word_1;
	}
    }
    #! 
    #! 
    return $self;
}

sub __writeOut__file {
    my($self, $name_features_tsv, $arr_2d, $offset_colHead) = @_;
    my $code_location = sprintf("%s:%d", __FILE__, __LINE__);
    open(FILE_RESULT_FEATURES_TSV, ">$name_features_tsv") or die("Unable to construct open file-pointer for file=\"$name_features_tsv\", at $code_location\n");
    {
	my $max_size_outer = 0;
	for(my $cat_index = 0; $cat_index < scalar(@{$arr_2d}); $cat_index++) {
	    my $arr = $arr_2d->[$cat_index];
	    my $arr_size = scalar(@{$arr});
	    if($max_size_outer < $arr_size) { $max_size_outer = $arr_size;}
	}
	printf(FILE_RESULT_FEATURES_TSV "#! Category\t ");
	for(my $i = 0; $i <= $max_size_outer; $i++) {	    
	    printf(FILE_RESULT_FEATURES_TSV "%s\t", $i + $offset_colHead); #! ie, the count.
	}
	printf(FILE_RESULT_FEATURES_TSV "\n");
	for(my $cat_index = 0; $cat_index < scalar(@{$arr_2d}); $cat_index++) {
	    my $arr = $arr_2d->[$cat_index];
	    my $arr_size = scalar(@{$arr});
	    printf(FILE_RESULT_FEATURES_TSV "%s\t", $self->{featureRow_strings}->[$cat_index]);
	    for(my $i = 0; $i < $arr_size; $i++) {	    
		printf(FILE_RESULT_FEATURES_TSV "%u\t", $arr->[$i]); #! ie, the count.
	    }
	    printf(FILE_RESULT_FEATURES_TSV "\n");
	}
    }
    close(FILE_RESULT_FEATURES_TSV);
}

sub close_andWriteOut {
    my ($self, $resultPrefix) = @_;
    {
	my $name_features_tsv = $resultPrefix  . "_" . "cat_x_countEachDocu" . "features.tsv";
	    my $arr_2d = $self->{resultStat}->{map_cat_count_numberEach};
	__writeOut__file($self, $name_features_tsv, $arr_2d, 0);
    }
   {
	my $name_features_tsv = $resultPrefix  . "_" . "cat_x_countYear" . "features.tsv";
	my $arr_2d = $self->{resultStat}->{map_cat_year_numberEach};
	my $year_offset = $self->{resultStat}->{constant__yearOffset};
	__writeOut__file($self, $name_features_tsv, $arr_2d, $year_offset);
    }
}

#! Intlise the cofniguraiton-option for the title-parser: 
sub initConf__get_groupVector_forString {
    return {
	isTo_highlight_entriesWithoutMatch => 1, #! which is used to simplify de-bugging.
	isTo_writeOut => 1,
	formatWriteOut => "tsv", #! OR: HTML,
	file_pointer => undef, 
	file_pointer_html => undef, 
	entry_title => undef,
    };
}

#! Construct a feautre-vector for the given string: 
sub writeOut_featureVector {
    my ($self, $title, $conf, $cit_key, $features, $year) = @_;
    if(!defined($conf)) {$conf = initConf__get_groupVector_forString();}
    #! 
    my @featureRow = @{$features};
    { #! Updat ethe 'max-count' for each cat-index:
	for(my $cat_index = 0; $cat_index < scalar(@featureRow); $cat_index++) {
	    my $count = $featureRow[$cat_index];
	    if($count > 0) {
		my $arr = $self->{resultStat}->{map_cat_count_numberEach}->[$cat_index];
		my $arr_size = scalar(@{$arr});
		if($count >= $arr_size) { #! then itnaitte: 
		    for(my $i = $arr_size; $i < $count; $i++) {
			push(@{$arr}, 0);
		    }
		}
		#! 
		#! Add 'this title' to the entropy-count: 
		$self->{resultStat}->{map_cat_count_numberEach}->[$cat_index]->[$count]++;
		#!
		if(defined($year) && length($year)) { #! then rpvodie detials of the year-attributes ... 
		    #! 
		    #! 
		    my $year_offset = $self->{resultStat}->{constant__yearOffset};
		    my $count = $year - $year_offset;
		    if($count < 0) {$count = 0;} 
		    my $arr = $self->{resultStat}->{map_cat_year_numberEach}->[$cat_index];
		    my $arr_size = scalar(@{$arr});
		    if($count >= $arr_size) { #! then itnaitte: 
			for(my $i = $arr_size; $i < $count; $i++) {
			    push(@{$arr}, 0);
			}
		    }		
		    #! 
		    #! Add 'this title' to the entropy-count: 
		    $self->{resultStat}->{map_cat_year_numberEach}->[$cat_index]->[$count]++;
		}
	    }
	}	
    }
    
    if($conf->{isTo_writeOut} == 0) {
	$self->{count_localCalls}++;
	return;}
    #! 
    #! 
    my $entry_title = $conf->{entry_title};
    #! 
    #! Row-header: 
    if(defined($cit_key) && length($cit_key)) {$entry_title = $cit_key;} #! ie, hte override the defulat.
    if(!defined($entry_title) || !length($entry_title)) {$entry_title = sprintf("[%u]", $self->{count_localCalls}); }
    
    #! 
    #! 
    if(lc($conf->{formatWriteOut}) eq "tsv" ) {
	my $file_pointer = $conf->{file_pointer};	
	if(!defined($file_pointer)) {		    
	    $file_pointer = \*STDOUT;
	}
	if($self->{count_localCalls} == 0) {
	    #! Main-feature header: 
	    printf({$file_pointer} "#! %s", "ID");
	    foreach my $key (@{$self->{featureRow_strings}}) {
		printf({$file_pointer} "%s\t", $key);
	    }
	    printf({$file_pointer} "\n");
	}
	#! 
	if(defined($file_pointer)) {		
	    printf({$file_pointer} "%s\t", $entry_title);
	} else { printf(STDOUT "%s\t", $entry_title); }
	#! 
	#! Fields: 
	for(my $i = 0; $i < scalar(@featureRow); $i++) {
	    my $count = $featureRow[$i];
	    if(defined($file_pointer)) {		
		printf({$file_pointer} "%u", $count);
	    } else { printf(STDOUT "%u", $count); }
	    if(($i+1) != (scalar(@featureRow))) {
		#! 
		if(defined($file_pointer)) {		
		    printf({$file_pointer} "\t");
		} else { printf(STDOUT "\t"); }
	    } else {
		#! 
		if(defined($file_pointer)) {		
		    printf({$file_pointer} "\n");
		} else { printf(STDOUT "\n"); }
	    }
	}
    }
    #! 
    #! HTML:
    if(defined($conf->{file_pointer_html})) {
	my $file_pointer = $conf->{file_pointer_html};	    
	if($self->{count_localCalls} == 0) {
	    #! Main-feature header: 
	    printf({$file_pointer} "\t<thead>\n<tr>\t");
	    printf({$file_pointer} "<th colspan=\"1\"> %s</th> ", "ID");
	    foreach my $key (@{$self->{featureRow_strings}}) {
		printf({$file_pointer} "<th colspan=\"1\"> %s</th> ", $key);
		#printf(FILE_RESULT_FEATURES_TSV "%s\t", $key);
	    }
	    printf({$file_pointer} "\t </tr>  </thead>  <tbody>");
#		printf({$file_pointer} "\n");
#		printf(FILE_RESULT_FEATURES_TSV "\n");		
	}
	#! 
	printf({$file_pointer} "\t<tr>\n\t\t");
	printf({$file_pointer} "<td>%s</td>\t", $entry_title);
	#! 
	#! Fields: 
	for(my $i = 0; $i < scalar(@featureRow); $i++) {
	    my $count = $featureRow[$i];
	    printf({$file_pointer} "<td>%u</td>\t", $count);
	}
	printf({$file_pointer} "\t</tr>\n");
    }
    $self->{count_localCalls}++;
}

#! Construct a feautre-vector for the given string: 
sub get_groupVector_forString {
    my ($self, $title, $conf, $cit_key, $year) = @_;
    if(!defined($conf)) {$conf = initConf__get_groupVector_forString();}
    #! 
    #! 
    #! 
    #! Remove all sepvial symbols, eg,  {{ .. }}, before inseritng the stirng ... 
    $title =~s/[:.\+,\{\}\(\)\[\]\n]//g;
    $title =~s/[\-]/ /g;
    #! 
    #! 
    my $hash_cat = $self->{hash_cat};
    # my @arrOf_cat;
    #my %hash_cat;
    #!
    #!
    my @featureRow; #my @featureRow_strings;
    { 
	for(my $i = 0; $i < $self->{groupToIndex_count};  $i++) {
	    push(@featureRow, 0);
	    #push(@featureRow_strings, undef);
	}
	# foreach my $key (keys(%{$self->{groupToIndex}})) {
	#     my $index = $groupToIndex->{$key};
	#     $featureRow_strings[$index] = $key;
	# }
    }
    #! 
    #! 
    my @arrOf_words = split(/\s+/, $title); my $cnt_matches_cat = 0;
    for(my $i = 0; $i < scalar(@arrOf_words); $i++) {
	my $word_1 = $arrOf_words[$i];
	if(defined($word_1) && lc($word_1)) {
	    $word_1 = lc($word_1);
	    my $count_local = 0;
	    #!
	    #! Evalute a stences both in a 'normal-raw-mode' and where all ["-", "/"] are replaced by a space (ie, a " "). Thsi strategy is deisng to avoid/address isses wr.t the fleiciblity of the english language/grammar:
	    for(my $isTo_removeSuffix = 0; $isTo_removeSuffix < 2; $isTo_removeSuffix++) {
		my $word_alt = $word_1;
		if($isTo_removeSuffix == 1) {
		    ($word_alt) = ($word_1 =~/^(.+)s$/);
		}
		if(!defined($word_alt)) {next; } #continue;} #! ie, as the case does not need to be elvuated.
		$word_1 = $word_alt;
		#! 
		#! Add to category:
		my $arr_loc = $self->{wordsTo_features}->{$word_1};	    
		if(defined($arr_loc)) {
		    foreach my $cat (@{$arr_loc}) {
			my $cat_index = $self->{groupToIndex}->{$cat};
			$featureRow[$cat_index]++;
			#$featureRow[$self->{groupToIndex}->{$cat}]++;
			$cnt_matches_cat++;
			$count_local++;
			$self->{resultStat}->{map_cat_count}->[$cat_index]++;
			# if(!defined($hash_cat{$cat})) {
			# 	$hash_cat{$cat} = 1;
			# } else {
			# 	$hash_cat{$cat}++;
			# }
		    }
		}
		if($i > 0) {
		    my $search_word = lc($arrOf_words[$i-1]) . " " . $word_1;
		    #printf("search: \"%s\", at %s:%d\n", $search_word, __FILE__, __LINE__);
		    $arr_loc = $self->{wordsTo_features}->{$search_word };  #! ie, then explroe the applcaiblity of the repvisus word ... ie, 2--look-ahead. 
		    if(defined($arr_loc)) {
			foreach my $cat (@{$arr_loc}) {
			    my $cat_index = $self->{groupToIndex}->{$cat};
			    $featureRow[$cat_index]++;
			    $cnt_matches_cat++;
			    $count_local++;
			    $self->{resultStat}->{map_cat_count}->[$cat_index]++;
#			    $self->{resultStat}->{map_cat_count_numberEach}->[$cat_index]++;
			    # if(!defined($hash_cat{$cat})) {
			    #     $hash_cat{$cat} = 1;
			    # } else {
			    #     $hash_cat{$cat}++;
			    # }
			}
		    }
		}
	    }
	    printf("\t\t %s\t%u\t\t at %s:%d\n", $word_1, $count_local, __FILE__, __LINE__);
	}
    }
    if($conf->{isTo_highlight_entriesWithoutMatch} && ($cnt_matches_cat == 0) ) {
	printf(STDERR "(no-matches)\t %s\t\t #! %s:%d\n", $title, __FILE__, __LINE__);
    } else {
	; #printf(STDERR "(matches::%u)\t  %s\t\t #! %s:%d\n",  $cnt_matches_cat,  $title,__FILE__, __LINE__);
    }
    
    #! 
    #! Update meta-proeprites:
    writeOut_featureVector($self, $title, $conf, $cit_key, \@featureRow, $year);
    
    #! 
    #!     
    return { 
	#hash => \%hash_cat,
	foundIn_cntCat => $cnt_matches_cat,
	featureRow => \@featureRow,
	featureRow_strings => $self->{featureRow_strings}, 
    };
#    return \@arrOf_cat;
}

1;
