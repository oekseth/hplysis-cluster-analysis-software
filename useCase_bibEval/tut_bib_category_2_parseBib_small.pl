#!/usr/bin/perl
use bib_categories;
use bib_parse_latexBib;
use Carp;
use strict;


my $obj_parse = hpLysis::curation::bib_parse_latexBib::init__bib_parsing();

#! 
#! Configure:
my $bib_conf = hpLysis::curation::bib_categories::initConf__get_groupVector_forString();

#!
#! Inptu files:
my @arrOf_file = (
    "bib_mineing.bib", 
    "citations_useCase_mineDB.bib"
    #"bib_test.bib",
    );
#! PArse: 
 hpLysis::curation::bib_parse_latexBib::parse($obj_parse, \@arrOf_file, "test_result", $bib_conf);
