#ifndef libs_h
#define libs_h
/**
   @file
   @brief Specifies commonly used libraries.
   @ingroup common
   @author Ole Kristian Ekseth (oekseth)
   @date 29.12.2011 by oekseth (initial)
**/ 
#ifdef __cplusplus
extern "C"
#endif
/* #ifdef __cplusplus */
/* extern "C" { */
/* #endif */

/* #ifndef configure_globalProject_NOT_INCLUDE_configuration //! eg, to be used when integraitn/combinging "ANSI C" libraryes (oekseth, 06. setp. 2016) */
/* #include "../configure.h" */
/* #endif */
#ifdef USE_MPI
#include <mpi.h>
#endif
#include <stdarg.h>
#ifdef __cplusplus
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#endif
#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <math.h>

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/types.h>
#ifdef __cplusplus
//#include <cmph.h>
#include <queue> // a FIFO to retrieve the file locations
#include <stack>
#include <map> // the c++ hash map to use
//#include "tbb.h"
#include <list>
// Degubber
//#include "libfence.a"
using namespace std;
//#include "error_handling.h"
#endif

/* #ifdef __cplusplus */
/* } */
/* #endif */


#endif //! EOF
