#ifndef kt_list_1d_h
#define kt_list_1d_h

/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */

/**
   @file kt_list_1d
   @brief provide a generic list-itnerface to 1d-lists (oekseth, 06. mar. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. mar. 2017).
   @remarks used to simplify API and access in/through different programming-languates
**/

//#include "kt_matrix_cmpCluster.h"
//#include "type_2d_float_nonCmp_uint.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
// #include "kt_matrix_fileReadTuning.h" //! which is used to facilatie/provuide support for file-reading

//! Note: [below] is sinpred by: "https://stackoverflow.com/questions/26562491/define-a-preprocessor-macro-swapt-x-y"
#define MF__SWAP_int(a, b) { (a) ^= (b); (b) ^= (a); (a) ^= (b); } //! where latter use X-OR logics: only supported for "int", "uint", "char" (or any oter non-complex data-type).
//! A mcro for compled data-tyeps such as "flaot" and "struct{..}":
//#define MF__SWAP_complexType(type_t, a, b) { type_t __swap_temp;  __swap_temp = b;     b = a; a = __swap_temp; }
#define MF__SWAP_complexType(type_t, a, b) { type_t __swap_temp;  __swap_temp = (b);     (b) = (a); (a) = __swap_temp; }

/**
   @struct s_kt_list_1d_float
   @brief provide a wrapper to accesss a list-strucutre (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_float {
  t_float *list; uint list_size;
  uint current_pos;
} s_kt_list_1d_float_t;
//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_float_t init__s_kt_list_1d_float_t(uint list_size);
//! initalises a  list-object with size "list_size"
static void init__ref_s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint list_size) {
  assert(self); *self = init__s_kt_list_1d_float_t(list_size);
}
//! @return an 'empty verison' of our s_kt_list_1d_float_t object
s_kt_list_1d_float_t setToEmpty__s_kt_list_1d_float_t();
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float valueTo_set);
//! Increment the score-count assicatred to "index" (oekseth, 06. apr. 2017)
void increment_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float valueTo_set);
//! De-allcoates the s_kt_list_1d_float_t object.
void free__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self);
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, const t_float valueTo_set);
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self);
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self);
//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_float_t(const s_kt_list_1d_float_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  uint biggest_index_plussOne = 0;
  if(self->list_size != 0) {
    for(uint i = 0; i < self->list_size; i++) {
      if(self->list[i] != T_FLOAT_MAX) {biggest_index_plussOne = i+1;}
    }
  }
  //printf("float::size=%u/%u, at %s:%d\n", biggest_index_plussOne, self->list_size, __FILE__, __LINE__);
  *scalar_result = biggest_index_plussOne;
  //  *scalar_result = self->list_size;
}



//! ***************************************************************************
//! ***************************************************************************

/**
   @struct s_kt_list_1d_uint
   @brief provide a wrapper to accesss a list-strucutre (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_uint {
  uint *list; uint list_size;
  uint current_pos; //! where latter is only updated if the the 'pushs(..)' funciton is used.
} s_kt_list_1d_uint_t;
//! Itnaites a s_ktType_kvPair_t object (oekseth, 06. mar. 2017)
//#define MF__init__s_ktType_kvPair() ({s_ktType_kvPair_t rel; rel;}) //! ie, return
#define MF__init__s_ktType_uint() ({uint rel; rel = UINT_MAX;  rel;}) //! ie, return
#define MF__initVal__s_ktType_uint(v1) ({uint rel; rel = v1;  rel;}) //! ie, return
#define MF__initFromRow__s_ktType_uint(row, row_size) ({assert(row); assert(row_size >= 2); uint rel; rel = (uint)row[0]; rel;}) //! ie, return
//! @return true if the uint is empty (eosekth, 06. mar. 2017)
#define MF__isEmpty__s_ktType_uint(rel) ({bool is_empty = ((rel == UINT_MAX) ); is_empty;}) //! ie, wehre we do Not expcltuly evlauate the dsitance/score proerpty.
//! Allocate a new set of uint objects.
#define MF__allocate__s_ktType_uint(size) ({assert(size > 0); uint *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(uint, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
#define MF__allocate__s_ktType_s_kt_list_1d_uint_t(size) ({assert(size > 0); s_kt_list_1d_uint_t *arrOf_rels = NULL; arrOf_rels = alloc_generic_type_1d_xmtMemset(s_kt_list_1d_uint_t, arrOf_rels, size); assert(arrOf_rels); arrOf_rels;}) //! ie, return
//! De-allocates a set of s_ktType_uint objects.
#define MF__free__s_ktType_uint(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return
#define MF__free__s_kt_list_1d_uint_t(arrOf_rels) ({assert(arrOf_rels); free_generic_type_1d(arrOf_rels); arrOf_rels = NULL;}) //! ie, return

//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_uint_t init__s_kt_list_1d_uint_t(uint list_size);
static s_kt_list_1d_uint_t setToEmpty__s_kt_list_1d_uint_t() {
  return init__s_kt_list_1d_uint_t(0);
}
//! initalises a  list-object with size "list_size"
static void init__ref_s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint list_size) {
  assert(self); *self = init__s_kt_list_1d_uint_t(list_size);
}
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint valueTo_set);
//! Increment the score-count assicatred to "index" (oekseth, 06. apr. 2017)
void increment_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint valueTo_set);
//! Push the valeu to teh list, and reutnr the inserrted index (oekseth, 06. jun. 2017).
static uint push__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint valueTo_set) {
  assert(self);
  const uint index = self->current_pos; self->current_pos++;
  set_scalar__s_kt_list_1d_uint_t(self, index, valueTo_set);
  return index;
}
//! Pop the entry, and treturn the value/key (oekseth, 06. jun. 2017).
static uint pop__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self) {
  assert(self);
  const uint index = self->current_pos; 
  if(index > 0) {self->current_pos--; return self->list[index - 1];}
  else {return UINT_MAX;} //! ie, as the value was then Not found. 
}
//! De-allcoates the s_kt_list_1d_uint_t object.
void free__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self);

//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_uint_t(const s_kt_list_1d_uint_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  uint biggest_index_plussOne = 0;
  if(self->list_size != 0) {
    for(uint i = 0; i < self->list_size; i++) {
      if(self->list[i] != UINT_MAX) {biggest_index_plussOne = i+1;}
    }
  }
  *scalar_result = biggest_index_plussOne;
  //*scalar_result = self->list_size;
}


//! Convert a matrix to the "s_kt_list_1d_uint_t" object.
//! @remakrs we use a macor to avodi a cyclic compiler-dependcy duing linking (eosketh, 06. mar. 2017).
#define MF__floatToUint__matrix__atRow(row_id, mat_local, list_result) ({assert(row_id < mat_local.nrows); assert(mat_local.nrows > 0); assert(list_result.list_size > 0); /*copy: */for(uint i = 0; i < mat_local.ncols; i++) {list_result.list[i] = (uint)mat_local.matrix[row_id][i];}})



//! ***************************************************************************
//! ***************************************************************************

/**
   @struct s_kt_list_1d_uint_pointer
   @brief provide a wrapper to accesss a list-strucutre (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_uint_node {
  uint key; s_kt_list_1d_uint_node *link_parent;
} s_kt_list_1d_uint_node_t;
//! Set the node:
static s_kt_list_1d_uint_node_t *setNode__s_kt_list_1d_uint_node_t(const uint key, s_kt_list_1d_uint_node_t *link_parent) {
  s_kt_list_1d_uint_node_t *self = (s_kt_list_1d_uint_node_t*)malloc(sizeof(s_kt_list_1d_uint_node_t));
  self->key = key; self->link_parent = link_parent;
  return self;
}
//! De-allcoates the s_kt_list_1d_uint_pointer_t object.
static void free__s_kt_list_1d_uint_node_t(s_kt_list_1d_uint_node_t *self) {
  assert(self);
  if(self->link_parent) {
    assert(self->link_parent != self);
    free__s_kt_list_1d_uint_node_t(self->link_parent); 
  }
  free(self);  
}

/**
   @struct s_kt_list_1d_uint_pointer
   @brief provide a wrapper to accesss a list-strucutre (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_1d_uint_pointer {
  //s_kt_list_1d_uint_node_t *root;
  s_kt_list_1d_uint_node_t *lastInserted; //! ie, the 'current-pos'.
} s_kt_list_1d_uint_pointer_t;
//! @return an itnainted verison:
static s_kt_list_1d_uint_pointer_t setToEmpty__s_kt_list_1d_uint_pointer_t() {
  s_kt_list_1d_uint_pointer_t self; 
  self.lastInserted = NULL;
  return self;
}
//! Push the valeu to teh list, and reutnr the inserrted index (oekseth, 06. jun. 2017).
static void push__s_kt_list_1d_uint_pointer_t(s_kt_list_1d_uint_pointer_t *self, uint valueTo_set) {
  assert(self);
  s_kt_list_1d_uint_node_t *new_node = setNode__s_kt_list_1d_uint_node_t(valueTo_set, self->lastInserted);
  self->lastInserted = new_node;
  //if(self->root == NULL) {
  /*   assert(self->lastInserted != NULL); */    
}

//! De-allcoates the s_kt_list_1d_uint_pointer_t object.
static void free__s_kt_list_1d_uint_pointer_t(s_kt_list_1d_uint_pointer_t *self) {
  assert(self);
  if(self->lastInserted != NULL) {
    free__s_kt_list_1d_uint_node_t(self->lastInserted); 
    self->lastInserted = NULL;
  }
}

#endif //! EOF
