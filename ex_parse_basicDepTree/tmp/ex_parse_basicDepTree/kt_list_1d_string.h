#ifndef kt_list_1d_string_h
#define kt_list_1d_string_h
/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */


/**
   @file kt_list_1d_string
   @brief a list of strings, and asiscated functions   
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks is sued to generalize the insertion and extraciton of string-names, eg, for wrapper-languess such as Perl and Java.
**/

//#include "kt_matrix_cmpCluster.h"
//#include "type_2d_float_nonCmp_uint.h"
#include "configure_cCluster.h"
//#include "list_uint.h"
#include "def_intri.h"
// #include "kt_swig_retValObj.h"

//! @reutrn a 'chomed stirng'.
#define MF__kt_string__Chomp(list) ({ const uint list_size = strlen(list); assert(list_size > 0); \
  const char *list_xmtWhitePreTail = list; \
  /*! Update for the: prefix: */ \
  bool isWhite = true; uint start_pos = 0; for(uint i = 0; (i < (list_size-1)) && isWhite; i++) {if(isblank(list[i])) {start_pos = (i+1);} else {isWhite = false;}} \
  /*! Update for the: suffix: */ \
  isWhite = true; for(uint i = 0; (i < (list_size-1)) && isWhite; i++) {const uint pos_local = (list_size-i)-1; assert(pos_local < list_size); if(isblank(list[pos_local])) {list[pos_local] = '\0';} else {isWhite = false;}} \
  assert(start_pos < list_size); \
  &(list_xmtWhitePreTail[start_pos]);})


/**
   @struct s_kt_list_1d_string
   @brief a list of strings, and asiscated functions   
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks is sued to generalize the insertion and extraciton of string-names, eg, for wrapper-languess such as Perl and Java.
**/
typedef struct s_kt_list_1d_string {
  uint nrows;
  char **nameOf_rows;
  uint biggestIndex_inserted_plussOne; //! which is used in combination our "get_count__s_kt_list_1d_string_t"
} s_kt_list_1d_string_t;

//! Updat ehte sclaar_Result with the number of elements in the "self" object.
//! @remarks tis fucntion si among others needed by our SWIG-routiens to 'correctly' fetch the number of tranversal to be sued when 'collecting' data from 'this object'.
static void get_count__s_kt_list_1d_string_t(const s_kt_list_1d_string_t *self, uint *scalar_result) {
  assert(self); assert(scalar_result);
  uint curr_pos = 0;
  if(self->nrows > 0) {
    for(uint i = 0; i < self->nrows; i++) {
      const char *str = self->nameOf_rows[i];
      if(str && strlen(str)) {curr_pos = i + 1;}
    }
  }
  *scalar_result = curr_pos;
  //*scalar_result = self->biggestIndex_inserted_plussOne;
  assert(*scalar_result <= self->nrows);
}

//! Inititates an s_kt_list_1d_string_t object to empty (oekseth, 06. apr. 2017)
void setToEmpty__s_kt_list_1d_string_t(s_kt_list_1d_string_t *self);
//! @returns an intiated s_kt_list_1d_string_t object (oekseth, 06. apr. 2017)
static s_kt_list_1d_string_t setToEmpty_andReturn__s_kt_list_1d_string_t() {
  //assert(self);
  s_kt_list_1d_string_t self;
  setToEmpty__s_kt_list_1d_string_t(&self);
  return self;
}
//! @return a new-tianted s_kt_list_1d_string_t object (oekseth, 06. apr. 2017).
void init__s_kt_list_1d_string_t( s_kt_list_1d_string_t *self, const uint nrows);

//! @return a new-tianted s_kt_list_1d_string_t object (oekseth, 06. apr. 2017).
static s_kt_list_1d_string_t init_andReturn__s_kt_list_1d_string_t(const uint nrows) {
  s_kt_list_1d_string_t self;
  init__s_kt_list_1d_string_t(&self, nrows);
  return self;
}

//! De-allcoate the s_kt_list_1d_string_t object.
void free__s_kt_list_1d_string(s_kt_list_1d_string_t *self);

//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 3 columns.
s_kt_list_1d_string_t initFromFile__s_kt_list_1d_string_t(const char *input_file);

/**
   @brief specify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos> is the id of the internla column or row to udpdate
   @param <stringTo_add> is the string to copy into this strucutre
 **/
void set_stringConst__s_kt_list_1d_string(s_kt_list_1d_string_t *self, const uint index_pos, const char *stringTo_add);
//! Constuct a string from a pttenr, and then isner thtre new-constructed string (oekseth, 06. arp. 2017).
static void set_stringConst__concat__s_kt_list_1d_string(s_kt_list_1d_string_t *self, const uint index_pos, const char *prefix, const int score) {
  allocOnStack__char__sprintf(2000, str_local, prefix, score); //! ie, intate a new variable "str_local". 
  assert(index_pos < self->nrows);
  //! Then apply the logics.
  set_stringConst__s_kt_list_1d_string(self, index_pos, str_local);
}
//! Perform a  lienar searhc for "str_search" and insert if "insertIf_notFound = true" (oekseth, 06. jul. 2017).
//! @returnt he index which the string was inserted at: if not inserted (and not found) then UINT_MAX is returned. 
uint get_indexOf_string_slowSearch__s_kt_list_1d_string_t(s_kt_list_1d_string_t *self, const char *str_search, const bool insertIf_notFound);


/**
   @brief specify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos> is the id of the internla column or row to udpdate
   @param <stringTo_add> is the string to copy into this strucutre
 **/
static void set_string__s_kt_list_1d_string(s_kt_list_1d_string_t *self, const uint index_pos, char *stringTo_add) { 
  //  printf("sets-string[%u]=\"%s\", at %s:%d\n", index_pos, stringTo_add, __FILE__, __LINE__);
  set_stringConst__s_kt_list_1d_string(self, index_pos, stringTo_add);}


/**
   @brief idnetify the row- or column-name (oekseth, 06. nov. 2016)
   @param <self> is the object to update
   @param <index_pos>
   @param <stringOf_result> is the string to get a memory-pointer to.
 **/
static 


//! Identify the index for the given string (ie, if the stirng is found) (oekseht, 06. des. 2016)
//! @return row_id if foudn: otherwise UINT_MAX is retunred (ie, as we then assume the string is Not found).
//! @remarks this fucntion is Not set to 'inline' as we asusme there will be now signficnat perfomrance-costs assicated to thsi funciton (ie, due to the relative low number of access which we expect).
uint getIndexOf__atRow__s_kt_list_1d_string(const s_kt_list_1d_string_t *self, const char *stringOf_value);

#endif //! EOF
