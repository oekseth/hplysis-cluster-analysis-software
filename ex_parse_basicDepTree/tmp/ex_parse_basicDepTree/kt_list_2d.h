#ifndef kt_list_2d_h
#define kt_list_2d_h

/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */

/**
   @file kt_list_2d
   @brief provide a generic list-itnerface to 2d-lists (oekseth, 06. mar. 2017)
   @author Ole Kristian Ekseth (oekseth, 06. aug. 2017).
   @remarks used to simplify API and access in/through different programming-languates
**/

#include "kt_list_1d.h"


/**
   @struct s_kt_list_2d_uint
   @brief provide a wrapper to accesss a list-strucutre of "uint" elements (oekseth, 06. mar. 2017).
 **/
typedef struct s_kt_list_2d_uint {
  s_kt_list_1d_uint_t *list; uint list_size; uint current_pos;
} s_kt_list_2d_uint_t;
//! @return an itnailted list-object with size "list_size"
s_kt_list_2d_uint_t init__s_kt_list_2d_uint_t(uint cnt_rows, uint list_size);
//! @return an 'empty verison' of our s_kt_list_2d_uint_t object
s_kt_list_2d_uint_t setToEmpty__s_kt_list_2d_uint_t();
//! intiates a data-set from a psarse matrix
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
s_kt_list_2d_uint_t initFrom_sparseMatrix__s_kt_list_2d_uint_t(const uint row_id, const uint nrows, const uint ncols, t_float **matrix) ;
//! @return an itnailted list-object when data is parsed from a list-input-file.
//! @remarks we expect the inptu to consist of only 2 columns.
//s_kt_list_2d_uint_t initFromFile__s_kt_list_2d_uint_t(const uint row_id, const char *input_file);
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
//! @return true if a value is set.
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
bool get_scalar__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, uint index, uint *scalar_result);
//! Programatilly set a given value: extend list if "index >= self->list_size"
//! @remarks used to simplify 'biding' to ohtter programming-languages, eg, to "Perl" through a SWIG language-binding.
void set_scalar__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, uint index, uint valueTo_set);
//! De-allcoates the s_kt_list_2d_uint_t object.
void free__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self);
//! Push a list-item to the set. (oekseth, 06. jul. 2017).
void push__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, const uint valueTo_set);
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id, const uint valueTo_set);
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id);
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id);
//! ***************************************************************************
//! ***************************************************************************

//! Sort the data-set (oekseth, 06. jul. 2017).
void sort__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint row_id);
//! Udpat ethe object-lsit size (oesketh, 06. aug. 2017).
//! @remarks is used to enable a 'comrepssion' of meory-usage, ie, to reduce rednant emmroys-aprce when memory-overla is of interest, eg, used as a psot-ranknig-stpe
void setTo_size__s_kt_list_2d_uint_t(s_kt_list_2d_uint_t *self, const uint list_size);


// ***************************************************************************
// ***********************'***************************************************

#endif //! EOF
