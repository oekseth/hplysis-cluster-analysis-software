#include <stdlib.h> //! to support use of "exit(2)" command. 
#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <signal.h>
#include <sys/time.h>
//! Handle erorrs to simplify fixing of issues such as 'why usb is noit possible to open'
#include <stdio.h>
#include <errno.h>
#include <string.h>
extern int errno ;
#define str_usb "/dev/bus/usb/002/017"
// "/dev/ttyUSB0"


#if 0 //! startComment ---------------------------------
int set_serial_attribs (uart_conn* conn, const speed_t speed, const int parity)
{
	// originally by user wallyk on stackoverflow.com (CC-BY-SA 2.5)
	// http://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c/6947758#6947758
	PRINT_POS(set_serial_attribs);
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (conn->read_fd, &tty) != 0)
	{
			printf ("error %d from tcgetattr (set_serial_attribs)\n", errno);
			return -1;
	}
	else if (tcgetattr (conn->write_fd, &tty) != 0)
	{
			printf ("error %d from tcgetattr (set_serial_attribs)\n", errno);
			return -1;
	}
	cfsetospeed (&tty, speed);
	cfsetispeed (&tty, speed);
	
	cfmakeraw(&tty);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
	// disable IGNBRK for mismatched speed tests; otherwise receive break
	// as \000 chars
	tty.c_iflag &= ~IGNBRK;         // ignore break signal
	tty.c_lflag = 0;                // no signaling chars, no echo,
									// no canonical processing
	tty.c_oflag = 0;                // no remapping, no delays
	tty.c_cc[VMIN]  = 0;            // read doesn't block
	tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

	tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
									// enable reading
	tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	//tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr (conn->read_fd, TCSANOW, &tty) != 0)
	{
			printf ("error %d from tcsetattr setting attributes to read_fd\n", errno);
			return -1;
	}
	else if (tcsetattr (conn->write_fd, TCSANOW, &tty) != 0)
	{
			printf ("error %d from tcsetattr setting attributes to write_fd\n", errno);
			return -1;
	}
	return 0;
}

void set_blocking (uart_conn* conn, const int should_block)
{
	// originally by user wallyk on stackoverflow.com (CC-BY-SA 2.5)
	// http://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c/6947758#6947758
	PRINT_POS(set_blocking);
	struct termios tty;
	memset (&tty, 0, sizeof tty);
	if (tcgetattr (conn->read_fd, &tty) != 0)
	{
			printf ("error %d from tggetattr (set_blocking)\n", errno);
			return;
	}

	tty.c_cc[VMIN]  = should_block ? 1 : 0;
	tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

	if (tcsetattr (conn->read_fd, TCSANOW, &tty) != 0)
			printf ("error %d setting term attributes\n", errno);
}


void driver_init(const char* read_portname, const char* write_portname, uart_conn* out_conn){
	
	PRINT_POS(driver_init);
	out_conn->read_fd = open (read_portname, O_RDWR | O_SYNC | O_NOCTTY);
	if (out_conn->read_fd < 0)
	{
		printf ("error opening %s\n", read_portname);
		return;
	}
	if (strcmp(read_portname, write_portname)){
		out_conn->write_fd = open (write_portname, O_RDWR | O_SYNC | O_NOCTTY);
		if (out_conn->write_fd < 0)
		{
			printf ("error opening %s\n", write_portname);
			return;
		}
	}
	else{
		out_conn->write_fd = out_conn->read_fd;
	}
	
	

	set_serial_attribs (out_conn, UART_SPEED, 0);  // set speed to defined speed, 8n1 (no parity)
	set_blocking (out_conn, 0);                // set no blocking
	
	
}




return_code driver_check_for_msg(uart_conn* conn, uart_msg_struct* out){
	PRINT_POS(driver_check_for_msg);
	uint8_t read_byte;
	uint16_t crc16;
	static int state = 0;
	//static int msg_ready = 0;
	static uart_msg_struct buffer = {
		.payload = NULL
	};
	//buffer.payload = (uint8_t*)pdcp_malloc(1); // to have something to realloc()
	//static int payload_size;
	int read_count = 0;
	
	if (out == NULL){
		return BAD_POINTER;
	}
	
	if (state!=MESSAGE_READY)
		read_count = read(conn->read_fd, &read_byte, 1);
	
	while (read_count > 0 && state != MESSAGE_READY){
		PRINT_VAR(state, d);
		switch(state){
			case 0:
				if(read_byte == 0xFE)
					state = 1;
				break;
			case 1:
				if(read_byte == 0x19)
					state = 2;
				else
					state = 0;
				break;
			case 2:
				buffer.msg_id = read_byte;
				state++;
				break;
			case 3:
				buffer.msg_id += (uint16_t)read_byte << 8;
				state++;
				break;
			case 4:
				buffer.ref_id = read_byte;
				if(buffer.ref_id != out->ref_id)
					state = 0;
				else 
					state++;
				break;
			case 5:
				state++;
				break;
			case 6:
				buffer.payload_size = (read_byte == 0) ? read_byte : read_byte-2;
				
				state++;
				break;
			case 7:
				buffer.payload_size += (uint16_t)read_byte << 8;
				state++;
				if (buffer.payload_size == 0){
					state = MESSAGE_READY;
				}
				else{
					buffer.payload = (uint8_t*)pdcp_malloc(buffer.payload_size * sizeof(uint8_t));
				}
				break;
			default: // state >= 8:				
				if(state - 8 == buffer.payload_size){
					PRINTF_MSG("crc16 1st byte\n");
					crc16 = read_byte;
					state++;
				}
				else if(state - 8 == (buffer.payload_size + 1)){
					
					PRINTF_MSG("crc16 2nd byte\n");
					
					crc16 += read_byte << 8;
					state = MESSAGE_READY;
				}
				else{
					
					PRINTF_MSG("payload %d\n", state-8);
					
					buffer.payload[state-8] = read_byte;
					state++;
				}
				break;
				
		}
		
		if (state!=MESSAGE_READY)
			read_count = read(conn->read_fd, &read_byte, 1);
			
			
	}
	
	PRINT_POS_AUTO();
	
	if (state == MESSAGE_READY){
		if (out->payload_size < buffer.payload_size){
			conn->payload_size_ready = buffer.payload_size;
			conn->ref_id_ready = buffer.ref_id;
			conn->message_is_ready = 1;
			PRINT_POS_AUTO();
			return TOO_SMALL;
		
		}
		else{
			conn->message_is_ready = 0;
			state = 0;
			out->msg_id = buffer.msg_id;
			out->ref_id = buffer.ref_id;
			out->payload_size = buffer.payload_size;
			memcpy((void*)(out->payload), (void*)(buffer.payload), buffer.payload_size*sizeof(uint8_t));
			pdcp_free(buffer.payload);
			buffer.payload = NULL;
			
			PRINT_POS_AUTO();
			return OUT_VAR_FILLED;

		}
	}

	PRINT_POS_AUTO();
	return NO_ACTION;
	
}

return_code driver_blocking_read(uart_conn* conn, uart_msg_struct* out){
	PRINT_POS(driver_blocking_read);
	
	// uint8_t* dummy_payload = NULL;
	// uint8_t* payload_backup = NULL;
	
	// int done = 0;
	
	int loop_count = 0;
	int timeout = 500;
	
	return_code ret = NO_ACTION;
	do{
		ret = driver_check_for_msg(conn, out);
		
		// if ((ret == BAD_POINTER || ret == TOO_SMALL) && conn->ref_id_ready != ref_id){
			// payload_backup = out->payload;
			// dummy_payload = pdcp_malloc(conn->payload_size_ready * sizeof(uint8_t));
			// out->payload = dummy_payload;
		// }
		// else if(ret == OUT_VAR_FILLED && dummy_payload != NULL){
			// out
		// }
		
		
		PRINTF_VAR("ret: %d\n", ret);
		
		loop_count++;
		
		if (loop_count == 10)
			timeout = 500000;
		else if (loop_count == 11)
			break;
		usleep(timeout);
	} while(ret == NO_ACTION);
	
	if  (ret != OUT_VAR_FILLED){
		printf("read failed: ret: %d\n", ret);
		//assert (ret == OUT_VAR_FILLED);
	}
	return ret;
}
#endif //! endComment ---------------------------------

int main()
{
  short portfd=-1;
  int n,f,len;
  alarm(2);

  struct termios oldtio,newtio;
  char buf[256];
  char *s = NULL; //"";


  
#if defined(O_NDELAY) && defined(F_SETFL)
  printf("open-case:1\n");  
  portfd = open(str_usb, O_RDWR|O_NDELAY);
  if (portfd >= 0){
    /* Cancel the O_NDELAY flag. */
    printf("port openend\n");
    n = fcntl(portfd, F_GETFL, 0);
    (void) fcntl(portfd, F_SETFL, n & ~O_NDELAY);
  }
#else
  printf("open-case:2\n");
  portfd = open(str_usb, O_RDWR);
#endif
  if (portfd < 0) {
    printf("(afer-open)\t cannot open %s. Sorry.\n", str_usb);
    int errnum;
    errnum = errno;
    fprintf(stderr, "Value of errno: %d\n", errno);
    perror("Error printed by perror");
    fprintf(stderr, "Error opening file: %s\n", strerror( errnum ));
    //!
    // exit(2);
  }

  if(false) {
    // open port...
    // save existing attributes
    tcgetattr(portfd,&oldtio);
    
    // set attributes - these flags may change for your device
    /** //! differnet baudrates [src:"info termios"]
       B0
       B50
       B75
       B110
       B134
       B150
       B200
       B300
       B600
       B1200
       B1800
       B2400
       B4800
       B9600
       B19200
       B38400
       B57600
       B115200
       B230400
    **/
#define BAUDRATE B38400
    //#define BAUDRATE B9600
    memset(&newtio, 0x00, sizeof(newtio));
    newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR | ICRNL;
    newtio.c_oflag = 0;


    tcflush(portfd, TCIFLUSH);
    tcsetattr(portfd,TCSANOW,&newtio);
    
    //reset attributes
    tcsetattr(portfd,TCSANOW,&oldtio);
  }
  if (portfd >= 0)
    {
      int iter = 0;
      if(s != NULL) {
	if (len == 0) len = strlen(s);
      }
      for(f = 0; f < len && f <100; f++) {
	if(s != NULL) {
	  buf[f] = *s++ | 0x80;
	}
      }
      write(portfd, buf, f);
      printf("Do write\n");
      while(portfd>=0){
	for(int f = 0; f < 256; f++) {
	  printf("[%d][%d]=%d\n", iter, f, buf[f]);
	  //printf("[%d][%d]='%x'='%d'\n", iter, f, buf[f], buf[f]);
	}
	//for(int k = 0; k < 
	// printf("%s\n",buf);
	iter += 1;
      }
    }

  alarm(0);
  signal(SIGALRM, SIG_IGN);
  if (portfd < 0) {
    printf("cannot open %s. Sorry.\n", str_usb);
    int errnum;
    errnum = errno;
    fprintf(stderr, "Value of errno: %d\n", errno);
    perror("Error printed by perror");
    fprintf(stderr, "Error opening file: %s\n", strerror( errnum ));
  }
}
