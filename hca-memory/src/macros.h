#ifndef macros_h
#define macros_h
//! return the name of the variable as seen in the calls to it when reading this comment.
#define macro_getVarName(varname) #varname  

//! A short-hand to avoid getting the full path of a file.
//! @remarks used to reduce the clutter in some of the (informative) "printf(...)" and "fprintf(...)" calls.
#define __FNAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#endif
