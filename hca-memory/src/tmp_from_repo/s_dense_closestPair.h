#ifndef s_dense_closestPair_h
#define s_dense_closestPair_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hca-mem-data-anlysis. If not, see <http://www.gnu.org/licenses/>.
 */



/* #ifndef configIsCalledFromExternal_software */
#include "def_intri.h"
#include "types.h"
#include "dense_closestPair_base.h"
/* #endif //! else we assuem the caller uses a 'linker' to include 'these' (oekseth, 06. july 2016). */

//! The __cplusplus macro macro was included in this document by oekseth to make it possible to link to KnittingTools C++-library.
#ifdef __cplusplus
extern "C"{
#endif 
 

  /**
   @struct s_dense_closestPair_t
   @brief optimizes the identifciation of shortest-pair distance for distance-matrices which are iterativly updated (oekseth, 06. juni 2016).

   @remarks In hierarchical clustering-algorithms a major lag/bottleneck is the identification of shortest paths/pairs (SP): for the cluster-algorithms of "psl", "pml" and "pal" the SP is computed for every iteration. To our surprise none of the popular hierarchical-cluster-algorithms provide efficient algorithms for computation of SP: the latter explains why  hierarchical clustering-algorithms (HCAs) provide/have a poor performance (ie, a cubic time-complexity).

   What we have observed, is that there is a sequence of matrix-updates which provides the potential/opportunity to reduce the time-complexity of HCAs from cubic time-complexity to quadratic time-complexity. 

 In this work we demonstrates an algorithm which translates/reduce hierarchical-clustering-algorithms with a cubic time-complexity into quadratic-time-complexity (ie, from "n*n*n" into "n*n", where "n" describes/represents the number of elements in a matrix). In brief, our new algorithm provides/presents a fast approach for the identification of SPs for distance-matrices which are iteratively updated. 
   
   Given the large number of applications wrt. our new algorithm, we in this work will outline both the algorithm, execution-times for different data-sets and different HCAs, and provide details of different optimization-strategies wrt. our ANSI C implementation. From the latter we demonstrate/identify how our approach enables domain-experts to identify novel features in their: through an increase in performance and ease in application/use complex features may be used to derive clusters of related entities.


   # Introduction
   In clustering of large data-sets we have observed/identified a need to reduce the execution-time of hierarchical-clustering-algorithms. An example-application is the identification of ...??... 

   

   # Implementation

   Idea is to store the 'min-value' for each index in a new "arrOf minValueAtRow" and "arrOf minValueAtRow columnIndex", arrays which are then updated every time the distance-matrix changes. Before introducing the algorithm, we in below identify/list key-observations/properties wrt. the HCAs:
   step(a) each iteration: a naive SP with time-complexity "n*n" is called for the distance-matrix;
   step(b) each iteration: approx. two rows and two columns are updated (in the distance-matrix) for every iteration;
   step(c) each iteration: the distance-matrix-size is reduced by a factor of '1' (for each iteration);
   From the latter list/points/steps we observe that a 'memory' wrt. the updated rows and shortest-paths provides a potential/opportunity to reduce the SP time-complexity: if we for each row-update remember adjusts/remembers the 'global' shortest-path-distance, there is no need to visits/investigate the complete set of elements (in the distance matrix for every HCA-iteration). 


   ... In our "s_dense_closestPair_init(..)" funciton the logic we apply illustrate the 'base framework' wrt. our approach:
   implementation(a) initiation of min-buckets: separate each row into a set of buckets: when a sub-part of a row is updated, we only need to update the buckets;
   implementation(b) initiation of min-columns-buckets: find the min-score for each 'bucket-row-tile-column': when a bucket is updated, we only need to iterate (ie, update) the 'chunk-columns' in order to have/identify the min-distance-score in the complete matrix;
   implementation(c) update row: ...??..
   implementation(d) update column: ...??...
   implementation(e) identify min-pair: ....??...

   .... Our implementation is based on eh following set of properties/observations wrt. HCAs .... 

   ... 
   
   # Results:
   ... The structure (and associated functions) are called (eg, from the HCA) every time the distance-matrix changes. From a theoretical time-approximation we expects the execution-time to decrease by a factor of "n" for the functions where the SP computation/call/funcion is the largest time-consumer. In order to empirically validate our approach we use performance-test wrt. the data-distributions described at "http://www.knittingtools.org/gui_lib_mine_cmp.cgi". 
   
   # Summary:
   
   ... From the time-measurements of ...??.... we observe that our new algorithm (and the associated implementation) results in significant performance-improvement for hierarchical clustering-algorithms. In this work we have demonstrated how our new algorithm (and associated implementation) boosts the performance of: "psl", "pml" and "pal" cluster-algorithms. In brief, our algorithm provides logic for identifying the shortest paths/pairs in dense matrices: in contrast to a naive/'default' implementation with "n*n" time-complexity (where "n" describes/represents the number of elements in a matrix), our novel algorithm/implementation reduces the time-complexity to "n".
   ... Given the large number of 'low-level' back-compatible software we implement our code in ANSI C language-compatible 'style'. Our new algorithm (and library) may therefore be included/incorporated into a large number of existing software-implementations. A use-case wrt. the latter is seen in our \citet{lib_cCluster}, where our new SP-algorithm for HCAs significantly boosts the performance (of the HCA computation).



   @author Ole Kristian Ekseth (oekseth).
  **/

  typedef struct s_dense_closestPair {
    // FIXME: in [below] may we instead 'store' shortest-path-distance to muliple rows 'in one tile'?
    uint nrows; uint ncols; //! where each vertex is assicated to one row: we store speerately for each row iot. (a) simplify the logics wrt. our intristnictict optmizaiton, and (b) as the update-sqeuence (Wrt. shrotest-paths) are 'seperately done' for both rows and columns (ie, where one 'complete row' is udpated insead of only single cells wrt. the 'muliple rows updated for one column case').
    //#ifndef NDEBUG
    uint _nrows_threshold; uint _ncols_threshold; //! whcih are used to 'infer' if we are to udpate teh 'boundaries' wrt. columns
    t_float *_arrOf_tmp_scalarRowValues; //! whcih is used to update teh 'bounardy-columns' 
    //#endif
    uint cnt_bucketsEachRows; //! whihc is used to optimize the 
    uint _bucket_size; //! which is used itneranlyy in the object for cosnistency wrt. cnt_bucketsEachRows;
    t_float **matrixOf_locallyTiled_minPairs; //! which stores wrt. [row-id][bucket-tile]
    //#if (config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 1)
    uint **matrixOf_locallyTiled_minPairs_columnPos; //! which stores wrt. [row-id][bucket-tile] wrt. the column-pos, ie, to 'aovid' the need to iterate through matrices to find the column-value.
    //#endif
    t_float **distmatrix;
    //! Configure-variables to test/evlauate differnet strategies to icnrease the perofrmance.
    bool config_useIntrisintitcs;

    //! -------------------------
    //! Optimization: remember min-values of each 'column-bucket':
    // FIXME: cosnider to add support for [below] .... ie, to avoid iterating through every row to find the min-value.
    // FIXME: test the perfomrance-effects of the "isTo_use_fast_minTileOptimization" option.
    bool isTo_use_fast_minTileOptimization;
    //! Note: the "lfoat" data-type is used for botht he index and the row to simplify the intricitnict-logics wr. the iteration.
    t_float *mapOf_minTiles_value; t_float *mapOf_minTiles_value_atRowIndex; //! where we store for [tile] = {min-value, row-id}. 
#if (config_reWrite_infer_onlyAtOnePoint == 1) //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
    uint *mapOf_endPosOf_columnTiles_toUpdate; //! where each index refers to the 'last-column-pos+1' for each bucket to update: used as an appraoch to reduce the exeuction-time wrt. our "__forAll_update_globalMin_atBucketIndex(..)" function.
# endif
# if(config_useLog_main==1)
    s_log_hca_t obj_log; //! ie, then we make use of the log-object.
# endif
  } s_dense_closestPair_t;


  //! @return true if the inserted value may irmpvoe the score
#define __macro__minValueIsImproved(self, row_id, bucket_id, currentValue) ({assert(self); assert(self->matrixOf_locallyTiled_minPairs_columnPos); const t_float old_minValue = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]; (currentValue <= old_minValue);}) //! ie, returnt h result of the comparison.
  
  //! Investigate if the 'old min-value' no longer hodls, ie, we ened to tierate t'rhoguht eh compelte chunk':
#if(config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos == 1)
#define __macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id, column_pos) ({ \
      assert(self->matrixOf_locallyTiled_minPairs_columnPos);  (self->matrixOf_locallyTiled_minPairs_columnPos[row_id][bucket_id] == column_pos);})
#else
#define __macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id, column_pos) ({ \
  const t_float old_value = arrOf_previouslyUsedValues[row_id]; \
  const t_float old_minValue = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id];  \
  if(false) {printf("row=%u, w/score(old-bucket-min, before-update, current)=(%f, %f, %f), for bucket=%u, column_pos=%u, at %s:%d\n", row_id, old_minValue, old_value, currentValue, bucket_id, column_pos, __FILE__, __LINE__);} \
  (old_minValue == old_value); }) //! ie, return
#endif

  //! @return true if we should udpate the column wrt. this
#define __macro_isTo_updateColumn_forBucketTile(self, row_id, bucket_id, column_pos) ({__macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id, column_pos) || __macro__minValueIsImproved(self, row_id, bucket_id, self->distmatrix[row_id][column_pos]);})

  /**
     @brief re-intalize the time-emasurements for the enum_id in quesiton.
     @param <self> the object to hold the s_log_hca_t object.
     @param <enum_id> the log-type to update
  **/
  void update_log_startMeasurement(s_dense_closestPair_t *self, const e_log_hca_typeOf_t enum_id);
  /**
     @brief update the "s_log_hca_t" object with updated time-measurements (oekseth, 06. july 2016).
     @param <self> the object to hold the s_log_hca_t object.
     @param <enum_id> the log-type to update
     @param <cnt> if not set to "0" nor "UINT_MAX" then wwe use this valeu to update the assciated s_log_hca_t structure     
  **/
  void update_log_endMeasurement(s_dense_closestPair_t *self, const e_log_hca_typeOf_t enum_id, const uint cnt);  
  //! @return the column-bucket-id assicated to column_pos for the self object
  uint get_columnBucketId(const s_dense_closestPair_t *self, const uint column_pos);
  //! @return the first-column-pos to investigate for bucket_id
  uint get_columnBucket_startPosOf_bucket(const s_dense_closestPair_t *self, const uint bucket_id);
  //! Evaluate if the object is conssitenten.
  //! @remarks this funciton is assicated with a sicitnifcant time-overhead. The fucntion si therefore only active if the compiler-macro-param "config_debugSlowdown_useExtensive_correctnessTesting" is set to "1". Otherwise the function is 'a dummy one', ie, an empty call only cosnsitning of pointless asserts (oekseth, 06. july 2016).
  bool classWideTests_runTime(s_dense_closestPair_t *self, const uint nrows_threshold, const uint ncols_threshold, const char *caller_file, const int caller_line, const bool test_distancematrix);
  
  
  //! A function to intiate the s_dense_closestPair object.
  void s_dense_closestPair_init(s_dense_closestPair_t *self, const uint nrows, const uint ncols, uint cnt_bucketsEachRows, t_float **distmatrix, const bool config_useIntrisintitcs, const bool isTo_use_fast_minTileOptimization);
  //! De-allcoate the locally reserved memory.
  void s_dense_closestPair_free(s_dense_closestPair_t *self);
  
  //! Update the shortest-paht-positions wrt. an updated row
  void s_dense_closestPair_updateAt_row(s_dense_closestPair_t *self, const uint row_id, const uint column_startPos, const uint column_endPosPlussOne, const uint row_endPosPlussOne);
  //! If a column-tile has has an improved/shroter distance, then consider to update 'local-min(row)(column-tile)' and 'global-min(column-tile)':
  //! @return true if the locla-column-min has changed, ie, for whcih we (at some exec-point) need to update the lcoal-min-value.
  bool update_minValues_for_columnBucket_setOptimal_atColumnPos(s_dense_closestPair_t *self, const uint row_id, const uint bucket_id, const uint column_pos, const t_float currentValue);
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
  //! Update for row-column pairs where the 'earlier column-local-min' no lnger holds, ie, for whcih we need to idneityf a different/new lcola-column-min
  void s_dense_closestPair_updateAt_column_notAt_columnSelfPos(s_dense_closestPair_t *self, const uint input_column_pos, const uint row_startPos, const uint row_endPosPlussOne, const uint column_endPosPlussOne, const t_float *arrOf_previouslyUsedValues, const uint *mapOf_columnPos_toUse, const uint *arrOf_condensed_rowId_toUse, const uint arrOf_condensed_rowId_toUse_currPos, const uint cnt_alreadyInserted_forColumnAtSelfPos);
#endif
  /**
   @brief Update the shortest-paht-positions wrt. an updated column
   @remarks procedure:
   -- each row-tile: Update the min-bucket-tiles for each row: we expect one signualr valeus at each row to be updated: if a 'row-assicated tile' 'is at the best row-tile-index and has changed', then we need to update all values 'for the given tile'.
   -- the global tile: iterate through the best-performing tiles and update.

  **/
  void s_dense_closestPair_updateAt_column(s_dense_closestPair_t *self, const uint column_pos, const uint row_startPos, const uint row_endPosPlussOne, const uint column_endPosPlussOne, const t_float *arrOf_previouslyUsedValues, const uint *mapOf_columnPos_toUse, const uint *arrOf_condensed_rowId_toUse, const uint arrOf_condensed_rowId_toUse_currPos);

  //! @return the closest pair, ie, update scalar_index1, scalar_index2
  t_float s_dense_closestPair_getClosestPair(s_dense_closestPair_t *self, uint *scalar_index1, uint *scalar_index2, const uint nrows_threshold, const uint ncols_threshold);


  //! Write out the min-scores for the "self" object.
  void writeOut_MinScores_humanized(FILE *stream, const s_dense_closestPair_t *self);

  /* //! Correctness-tests and performance-tests for our "s_dense_closestPair" ANSI C class (for idneitfying shortest paths). */
  /* void assert_s_dense_closestPair(); */


#ifdef __cplusplus
}
#endif

#endif //! EOF
