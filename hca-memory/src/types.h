#ifndef types_h
#define types_h
/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of orthAgogue.
 *
 * orthAgogue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * orthAgogue is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with orthAgogue
. If not, see <http://www.gnu.org/licenses/>.
 */
/**
  @file
  @brief Defines commonly used types in the project.
  @ingroup common
  @author Ole Kristian Ekseth (oekseth)
  @date 21.12.2010 by Ole Kristian Ekseth (init)
  @date 28.12.2011 by oekseth (clean-up)
 */
/**  @ingroup common **/
/** \{ **/
#include "libs.h"
#include "macros.h"
//! Depricated, but some methods still uses it holding the memmory address.
typedef long int mem_loc; 

//! May have maximum 65.535 'indexes': Halving the size, without posing constraints, e.g. for the number of iummediate ancestors.
typedef unsigned short int lowint;
//! A synonym to the c-standard-macro.
static const lowint LOWINT_MAX = USHRT_MAX;
//typedef unsigned short int overlap_t; //! Defines the maximum size of the overlap.

//! For convenience, giving the system easier job casting error if value is negative.
typedef unsigned int uint; 

//! For convenience, giving the system easier job casting error if value is negative.
typedef unsigned char uchar; 

//! Handling memory references: set to either 34b or 64b, dependening on the archictecture of the system.
typedef unsigned long int uli;
/**
  @brief Defines the maximum size of the overlap.
  @todo Changed to 'short int' as valgrind complains on 'usigned short int'.
  @remarks If changed from other type, MPI structures using this must be updated appropriatly, as the complainer does not have means detecting such inconsistencies!.
*/
typedef uint overlap_t; 

//! Holding the internal address of a data in a file, requires a a large number. 
typedef unsigned long long int loint; 
//typedef long long unsigned int loint; 
//! Holding the internal address of a data in a file, requires a a large number. 
typedef long int lint; 

#include <stdlib.h>  /* The standard C libraries */
/* #include <map> */
/* #include <algorithm>  */
/* #include<string.h> */
/* #include <cstring> */
/* #include <string> */
/* using namespace std; */
//#include <wchar.h>

/* typedef std::pair<std::string, uint> string_number_pair_t; */
/* struct key_comparer */
/* { */
/*   bool operator()(std::string a, std::string b) const */
/*   { */
/*     return strcasecmp(a.c_str(), b.c_str()) < 0; */
/*   } */
/* }; */


/** \} **/

#endif
