#ifndef configure_hcaMem_h
#define configure_hcaMem_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hca-mem-data-anlysis. If not, see <http://www.gnu.org/licenses/>.
 */


#include "types.h"
#include "def_intri.h"


/**
   @file configure_hcaMem
   @brief provide compiler-logics and 'live-lgoics' to configure/change the flow of exeuctions in the software-process.
   @author Ole Kristian Ekseth (oekseth, 06. july 2016).
   @remarks 
   - the 'configuraiton-swithces' whitch we provide is sued to both chagne the type of lgocis wrt. inferences (ie, the degree of correctenss) and wrt. exeuction-time-optmilaity, ie, may be used to introspect upon the feasiblity of 'such software' wrt. computer-hardware-logics.
   - the compiler-swithces which we provide only represents a sub of psosbile optmziaiton-strategies. An exmaplf of an optmziation-case which we currently do not handle conserns the case where 'the lcoal min is still the local min', ie, to include a 'second-bestScore-value-table' for each column .... ie, wrt. oru "assert_local(debug_cnt_minValueHasCahnged > 0);" in oru "s_dense_closestPair.c". We do not optmize wrt. this case as 'the case' does not seem to happend all to frequently.
 **/


#define __internal_config_useFast  1 //! which is set to "0" if we are to evalaute the 'effect' of not use our improved HCA-mem algorithm (oekseth, 06. july 2016).

//! ***************************************************************
#ifndef config_typeOf_function_shortestPaths_useOptimal
#define config_typeOf_function_shortestPaths_useOptimal __internal_config_useFast //! ie, the 'main-swithch' in this configuraiton-file
#endif
#ifndef config_debugSlowdown_useExtensive_correctnessTesting
#define config_debugSlowdown_useExtensive_correctnessTesting 0 //__internal_config_useFast //! which if set to "1" is expecteded to result in a considerabl slwo-down: used to test/assert correctness of the code 'during run-time'.
#endif
//! ***************************************************************


#ifndef config_FixedNumberOf_bucketsFor_columns
//! Note[article]: when "config_FixedNumberOf_bucketsFor_columns" is compared to "config_reWrite_initFunction_identifyBcuekt_notUsingExtraForLoop" we observe that the compiler is unalbe to idneitfyAchiceve the same for-loop-rewiritng as our 'manaul strategy optmziaiton-strategy', an observation which may be expalined by the fact/observaiton of ...??... <-- fixme[jc]: any expalatniosn 'for this'?
#define config_FixedNumberOf_bucketsFor_columns 0 //! then the latter describes the fixed number of buckets to be used wrt. the column-tiles.
#endif
#ifndef config_FixedNumberOf_bucketsFor_rows
#define config_FixedNumberOf_bucketsFor_rows 10 //! then the latter describes the fixed number of buckets to be used wrt. the row-tiles.
#endif
#ifndef config_reWrite_initFunction_identifyBcuekt_notUsingExtraForLoop
#define config_reWrite_initFunction_identifyBcuekt_notUsingExtraForLoop 1 //! which if set to "1" implies that we do not seperate the for-lop into 'for(bucket = 0; ...)", ie, where we instead use a "for(column_id=0; ...)": this optmizaiton is ude to the fwer number of bracnh-statements, ie, exmplifies a (revelivatly speaking) signficnat optmizaiton-factor wrt. 'standrized' code-re-wrtiging
#endif


//! ***************************************************************
#ifndef config_useRowChunks
#define config_useRowChunks 0
#endif
#ifndef config_useRowChunks_update_globalMin_atBucketIndex
#define config_useRowChunks_update_globalMin_atBucketIndex 0 //! which if set to "1" implies that we do not need to traverse through every row in our "__forAll_update_globalMin_atBucketIndex/(..)"  function.
#endif
#if(config_useRowChunks == 0)
#if(config_useRowChunks_update_globalMin_atBucketIndex == 1)
#error "Seems like there is an inconsistency, ie, please update yoru compiler-macro-variable-settings"
#endif
#endif
//! ***************************************************************


#ifndef config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos
#define config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos __internal_config_useFast //! Note: the exeuction-time seems to be un-related to the use of this option. However, for 'real-time' deubggign this option is of specific interest, ie, as the latter simplfieis 'atuoamtic' validation of crorectness.
#endif
#ifndef config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos
//! Note(1): [below] parameter reduces the time-cost from 0.8s to 0.6s for a "sinus" distribution and from 1.3s to 0.94s fro a "random" distribution (whean measured for "cnt_rows=6000" wrt. "Pairwise Maximum- (complete-) Linking (PML) cluster"). 
//! Note(2): we did not expect this code-mdoficaiton/re-write to hace 'such a signicnat time-effect', ie, as we through/expecte  dthe compiler to be able to hadnle/idneitfy 'teh case': the [”elow] macro-parameter code-example-case/strategy may therefore be applied to a dider range of software-optmixzaiton, eg, ...??.. ... ie, similar to the 'case' of our "config_reWrite_mainHcaLoop_notUpdatePointless_memoryCells" compiler-macro-param.
#define config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos __internal_config_useFast
#endif

#ifndef config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall
#define config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall __internal_config_useFast //! ie, for "1" we identify the min-value in the 'main-HCA-loop and thereafter 'make the update-call'.
#endif
#ifndef config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns 
//! Note: for "sinus" data-distrubiotns we amnaged to get a cosndierablly exeuction-tiem-reductionw r.t this 'atlerantive: indicates the need/importance to use a 'dense list of row-valeus to iterate through': to investigate every row muliple times has a (realivly speaking) high time-cost, ie, for which the use of a 'dense' version reduces the time-compelxity.
//! Ntoe(2): through this option we 'mark' buckets which are to be updated, and then call a 'permtuation' of our "s_dense_closestPair_updateAt_column(..)" where we iterate through a 'condensed arrya of interesting row-ids'. idea: only a subset of the scores are changed wrt. teh "max" property, ie, ....??.... <-- in a new 'max' funciton in our "s_dense_closestPair.c", insert 'row-pos which are updated' in a 'condensed' array ... and then iterate through this array. <--- wrt. "non-max" copy-operations of columns for (is == js) we use the 'old' function-call....??... 
#define config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns 1 //__internal_config_useFast //! where "1" implies that we use a dense array when udpating 
#endif

#ifndef config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal
//! Note: wrt. "random" data-dsitrubitons this 'appraoch' managed to reduce the teim-compelxtiy of teh latter ...time-compelxity nwo 'moved' to our hca-algorithm
#define config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal 1 //__internal_config_useFast
#endif

#if(config_typeOf_temporaryDataStruct_rememberBestTiles_columnPos == 0)
#if(config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos == 1)
#error "Seems like there is an inconsistency, ie, please update yoru compiler-macro-variable-settings"
#endif
#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
#error "Seems like there is an inconsistency, ie, please update yoru compiler-macro-variable-settings"
#endif
#endif
#if(config_reWrite_spAlg_insteadOfTmpTable_useKnowledgeOf_columnMinPos == 0)
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
//! Note: teh [”elow] is incldued iot. simplify oru code-writign, ie, to try to reduce the 'messyness'w rt. differnt impelmetnation-strategies, ie, vadliation of correctness wr.t the latter.
#error "Seems like there is an inconsistency, ie, please update yoru compiler-macro-variable-settings" 
#endif
#endif
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 0)
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal == 1) //! ie, as this 'extends' teh "config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns"
#error "Seems like there is an inconsistency, ie, please update yoru compiler-macro-variable-settings" 
#endif
#endif


#ifndef config_inputData_useDefaultMatrix_type
//! Note: se "config_inputData_useDefaultMatrix_type" to "0" iot. enbalbe the termnal-gui-arg-inputs, ie, to use 'real' data-inptus.
#define config_inputData_useDefaultMatrix_type 1 //! which if set to a differnt valeu than '1' implies that we do not 'allow' for input-options to be set, eg, iot. explore a set of pre-configured otpions
//#define config_inputData_useDefaultMatrix_type -1 //! which if set to '-1' implies we only 'compute' a dummy data-set, ie, to ivnestigate the cost of 'inferring data'.
#endif

#ifndef config_reWrite_infer_onlyAtOnePoint
//! Note: [”elow] function is assciated to the use of our "__forAll_update_globalMin_atBucketIndex(..)" function.
#define config_reWrite_infer_onlyAtOnePoint __internal_config_useFast //! which if set implies that we 'colelct' the diffnernet 'optmziaiton-tasks, ie, to avodi muliple redundant calls.
#endif

#ifndef config_reWrite_mainHcaLoop_useMemCpy //! a parpaemter which for the "sinus" data-distrubtion has an in-signficant effect wrt. teh exeuction-time.
#define config_reWrite_mainHcaLoop_useMemCpy __internal_config_useFast
#endif

#ifndef config_reWrite_mainHcaLoop_notUpdatePointless_memoryCells
//! Note(1): [below] parameter reduces the time-cost of HCA w/a factor of 1.2x (whean measured for "sinus" data-distrubition and "cnt_rows=6000" for "Pairwise Maximum- (complete-) Linking (PML) cluster"). <-- Note: fro a random distribution there is hardly any difference betweent eh "0" option and the "1" option.
//! Note(2): we did not expect this code-mdoficaiton/re-write to hace 'such a signicnat time-effect', ie, as we through/expecte  dthe compiler to be able to hadnle/idneitfy 'teh case': the [”elow] macro-parameter code-example-case/strategy may therefore be applied to a dider range of software-optmixzaiton, eg, ...??..
#define config_reWrite_mainHcaLoop_notUpdatePointless_memoryCells __internal_config_useFast //! which recues the number of pointless udpate-sequences wrt. copying of valeus.
#endif

#ifndef config_useLog_main
//! Note: using "config_useLog_main" resutls in a sligth performance-decrement.
#define config_useLog_main 1 //__internal_config_useFast //! then we make use of the log-function.
#endif
#ifndef config_useLog_main_signiOfSameBucket
#define config_useLog_main_signiOfSameBucket 0 //! then we extend the log-function to evlauate the signficnace of vertices being in the same bucket.
#endif


//#define assert_local(value) ({;})
#define assert_local(value) ({assert(value);})


/* //! Simplify the configuration of our optmized "mine" software-implementaiton. */
/* #include <stdlib.h> */
/* #include <stdio.h>  */
/* #include <string.h> */
/* #include <math.h> */
/* #include <float.h> */
/* #include <assert.h>   */

/* typedef char bool; */

#endif //! EOF 
