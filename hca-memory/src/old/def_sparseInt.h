#ifndef def_sparseInt_h
#define def_sparseInt_h

/**
   @file def_sparseInt
   @brief provide macro defintionts and macro-funcitons for efficnt utilizaiton of low number-ranges
   @author Ole Kristian Ekseth (oekseth)
   @remarks expected to be inlcuded in the tail of our "def_intri.h"
 **/

// FIXME: add support for other data-types than [”elow] "int"

//! Define the type, ie, to simplify our allcoations.


#ifndef SPARSEVEC_CONFIG_DATATYPE
//! Then we use a default configuration wrt. the float-data-type to use (oekseth, 06. june 2016)
// FIXME: if we choose to use oterh 'valeus' than 0|1 for "VECTOR_CONFIG_USE_FLOAT" ... then update the macros which use the "VECTOR_CONFIG_USE_FLOAT" .. to an if-else macro-clause
#define SPARSEVEC_CONFIG_DATATYPE 1
#endif

#if SPARSEVEC_CONFIG_DATATYPE == 0

typedef int t_sparseVec;
#define SPARSEVEC_ITER_SIZE 4
#define SPARSEVEC_TYPE __m128i

//! ---- basic data-loading: 
//#define SPARSEVEC_LOAD VECTOR_INT_LOAD
#define SPARSEVEC_LOAD(mem_ref) (VECTOR_INT_LOAD(mem_ref))
#define SPARSEVEC_LOADU(mem_ref) (VECTOR_INT_LOADU(mem_ref))
#define SPARSEVEC_SET1 _mm_set1_epi32
//! --- simple aritmetics
#define SPARSEVEC_MUL _mm_mul_epi32
#define SPARSEVEC_ADD _mm_add_epi32
#define SPARSEVEC_SUB _mm_sub_epi32
#define SPARSEVEC_DIV _mm_div_epi32
//! --- simple 'modifications':
#define SPARSEVEC_ABS _mm_abs_epi32
//! Convert an int to a float:
#define SPARSEVEC_convertTo_float(vec) (VECTOR_FLOAT_convertFrom_epi32(vec)) //! ie, makse use of our macro from our "def_intri.h"

//! @return the interation-size with offset:
#define SPARSEVEC__get_maxIntriLength(length) ( (length > SPARSEVEC_ITER_SIZE) ? (length - SPARSEVEC_ITER_SIZE) : 0  )

#else //! thenw e use a "t_float" data-tyep specified in oru "def_intri.h"

typedef t_float t_sparseVec;
#define SPARSEVEC_ITER_SIZE VECTOR_FLOAT_ITER_SIZE
#define SPARSEVEC_TYPE VECTOR_FLOAT_TYPE

//! ---- basic data-loading: 
//#define SPARSEVEC_LOAD VECTOR_INT_LOAD
#define SPARSEVEC_LOAD(mem_ref) (VECTOR_FLOAT_LOAD(mem_ref))
#define SPARSEVEC_LOADU(mem_ref) (VECTOR_FLOAT_LOADU(mem_ref))
#define SPARSEVEC_SET1 VECTOR_FLOAT_SET1
//! --- simple aritmetics
#define SPARSEVEC_MUL VECTOR_FLOAT_MUL
#define SPARSEVEC_ADD VECTOR_FLOAT_ADD
#define SPARSEVEC_SUB VECTOR_FLOAT_SUB
#define SPARSEVEC_DIV VECTOR_FLOAT_DIV
//! --- simple 'modifications':
// FIXME[JC]: may you improve [”elow]?
#define SPARSEVEC_ABS(vec) (VECTOR_FLOAT_MAX(vec, VECTOR_FLOAT_MUL(vec, VECTOR_FLOAT_SET1(-1))))
//! Convert an int to a float:
#define SPARSEVEC_convertTo_float(vec) ({vec;}) //! ie, a 'wrapper'.

//! @return the interation-size with offset:
#define SPARSEVEC__get_maxIntriLength macro__get_maxIntriLength_float

#endif


//! End of file: ------------------------------------------------------------------------
#endif //! ie, EOF
