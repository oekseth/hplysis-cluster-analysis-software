#ifndef def_intri_h
#define def_intri_h

/**
   @file def_intri
   @brief describes and generalises a set of intrisnitistc SIMD macros.
   @author Ole Kristian Ekseth (oekseth)
   @remarks in the set of macro-fucntiosn which we use, we oberve the following categories/clases (of tasks):
   - ...?? 
   - masks: use either implict masks (eg, FLT_MAX) or 'direct' masks (eg, "0" stored in a spserte mask-array, or "0" in the dta-array itself);
   - ...?? 

   ... a challenge in rpgoramming wrt. intrisnitistc is/cosnerns our goal to construct programs which may be used on semi-new devices, ie, to avoid the pre-codniton/requriement of ...??...

   ... wrt. 'the goal to learn intrisnitistc' ... it seems like a mjaor chalelnge is to correctly itnerpret the warnigns given by teh compiler, eg, wrt: float data1[4]  = {0, 1, 2, 3};; __m128 vec_term1 = VECTOR_FLOAT_LOAD(data1);; VECTOR_FLOAT_TYPE mask = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float(vec_term1), VECTOR_FLOAT_SET1(0)); 
   ... a different challegne (in learning to efficently program/apply/use intrisnitistc) is the 'reversion' of the storage-order. An exmaple of the latter is seen wrt. "vec_result = VECTOR_FLOAT_revertOrder(vec_result);" ub our "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(vec_term1, VECTOR_FLOAT_LOG)" VS the lgocis in our "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(..)" (wrt. the "VECTOR_FLOAT_revertOrder(..)" call).

   .. conceptual challenge when using intel SSE itnruscriotns is/conserns the diffnere ce in programming from 'explict if-claues' to 'no-branch-statements' (a case which si explcit true for marchine-hardware which only support versions less than SSE3): instead of computing if(...) { ... } else { ...}, both resutls are comptued, and then 'merged togheter'. The conspetual complexity (wrt. the latter) is foudn/seen wrt. ...??.. 


   .... we have observed that the use of SSE intrisnticts may improve the efficency/perofmrance of non-SSE code ... and exmaple is the use of muliplciaotns instead of divisons ... where the diccutlg/challenging handling of "NAN" values in SSE encoruage the use of muliplcatiosn: an improtant side-effect is that muliplciatiosn has a higher perofmrnace than divison, from which a 'similar-written' non-SSE code results in a better/faster performance.

 **/





#define isOf_interest(value) ({ (value != FLT_MAX) && (value != FLT_MIN) ;} )


#define __constant_float_charMin  (float)((float)1.0 / SCHAR_MIN)


#include <math.h>
#include <xmmintrin.h>
#include <immintrin.h>
#include <xmmintrin.h> 
#include <pmmintrin.h>
#include <emmintrin.h>
#include <x86intrin.h> //! Note: for eahder-fiels see "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/"
#ifdef __SSE4_1__
#include <smmintrin.h>
#endif



#ifndef VECTOR_CONFIG_USE_FLOAT
//! Then we use a default configuration wrt. the float-data-type to use (oekseth, 06. june 2016)
// FIXME: if we choose to use oterh 'valeus' than 0|1 for "VECTOR_CONFIG_USE_FLOAT" ... then update the macros which use the "VECTOR_CONFIG_USE_FLOAT" .. to an if-else macro-clause
#define VECTOR_CONFIG_USE_FLOAT 1
#endif




// FIXME[jc]: consider to include the 'defts' from  "/home/klatremus/poset_src/externalLibs/libpll-1.0.11/src/parsimony.c" in 'this' <-- may you describe how the different compiel-specs are set (indcued [”elow])?

/* #define ULINT_SIZE 64 */
/* #define INTS_PER_VECTOR 8 */
/* #define LONG_INTS_PER_VECTOR 4 */
/* #define INT_TYPE __m256d */
/* #define CAST double* */
/* #define SET_ALL_BITS_ONE (__m256d)_mm256_set_epi32(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF) */
/* #define SET_ALL_BITS_ZERO (__m256d)_mm256_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000) */
/* #define VECTOR_LOAD _mm256_load_pd */
/* #define VECTOR_BIT_AND _mm256_and_pd */
/* #define VECTOR_BIT_OR  _mm256_or_pd */
/* #define VECTOR_STORE  _mm256_store_pd */
/* #define VECTOR_AND_NOT _mm256_andnot_pd */
 
/* #elif (defined(__SSE3)) */

/* #include <xmmintrin.h>  */
/* #include <pmmintrin.h> */
  
/* #define INTS_PER_VECTOR 4 */
/* #ifdef __i386__ */
/* #define ULINT_SIZE 32 */
/* #define LONG_INTS_PER_VECTOR 4 */
/* #else */
/* #define ULINT_SIZE 64 */
/* #define LONG_INTS_PER_VECTOR 2 */
/* #endif */
/* #define INT_TYPE __m128i */
/* #define CAST __m128i* */
/* #define SET_ALL_BITS_ONE _mm_set_epi32(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF) */
/* #define SET_ALL_BITS_ZERO _mm_set_epi32(0x00000000, 0x00000000, 0x00000000, 0x00000000) */
/* #define VECTOR_LOAD _mm_load_si128 */
/* #define VECTOR_BIT_AND _mm_and_si128 */
/* #define VECTOR_BIT_OR  _mm_or_si128 */
/* #define VECTOR_STORE  _mm_store_si128 */
/* #define VECTOR_AND_NOT _mm_andnot_si128 */

/* #endif */
 

/**
   FIXME[jc]: ... wrt. coding-schemed ... may you itrnospect upon .... how we may beautfiy the vector-code through void-pointers ... 
   ... in the grammer we often observe a 'pattern': 
   <code-block> ::= <for-loop-wrapper> + <logical-block> + <tail-operations>
   <logical-block> ::= "expansion of variables into 'load', 'set', 'aritmetics' and 'store'"
   <tail-operations> ::= "scalar-max" | "scalar-min" | ...       
 **/
 
#define VECTOR_CHAR_ITER_SIZE 16
// FIXME: may we use 'the fact' that VECTOR_CHAR_TYPE and VECTOR_INT_TYPE is the 'same'? ... eg, when 'covnerting' from differnet vector-formats??
#define VECTOR_CHAR_TYPE __m64

#define VECTOR_CHAR_SET1 _mm_set1_pi8
//#define VECTOR_CHAR_TYPE __m128i
// FIXME[JC]: is it a better stratgy/approach to laod a 'transoposed' row than [”elow]?
/* char, char, char, char,  */
/* char, char, char, char,  */
/* char, char, char, char,  */
/* char, char, char, char)’ */
// , matrix[rowPos_dynamic+16][columnPos_fixed], matrix[rowPos_dynamic+17][columnPos_fixed], matrix[rowPos_dynamic+18][columnPos_fixed], matrix[rowPos_dynamic+19][columnPos_fixed], matrix[rowPos_dynamic+20][columnPos_fixed], matrix[rowPos_dynamic+21][columnPos_fixed], matrix[rowPos_dynamic+22][columnPos_fixed], matrix[rowPos_dynamic+23][columnPos_fixed], matrix[rowPos_dynamic+24][columnPos_fixed], matrix[rowPos_dynamic+25][columnPos_fixed], matrix[rowPos_dynamic+26][columnPos_fixed], matrix[rowPos_dynamic+27][columnPos_fixed], matrix[rowPos_dynamic+28][columnPos_fixed], matrix[rowPos_dynamic+29][columnPos_fixed], matrix[rowPos_dynamic+30][columnPos_fixed], matrix[rowPos_dynamic+31][columnPos_fixed]
#define VECTOR_CHAR_setFrom_rows(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_epi8(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed], matrix[rowPos_dynamic+2][columnPos_fixed], matrix[rowPos_dynamic+3][columnPos_fixed], matrix[rowPos_dynamic+4][columnPos_fixed], matrix[rowPos_dynamic+5][columnPos_fixed], matrix[rowPos_dynamic+6][columnPos_fixed], matrix[rowPos_dynamic+7][columnPos_fixed], matrix[rowPos_dynamic+8][columnPos_fixed], matrix[rowPos_dynamic+9][columnPos_fixed], matrix[rowPos_dynamic+10][columnPos_fixed], matrix[rowPos_dynamic+11][columnPos_fixed], matrix[rowPos_dynamic+12][columnPos_fixed], matrix[rowPos_dynamic+13][columnPos_fixed], matrix[rowPos_dynamic+14][columnPos_fixed], matrix[rowPos_dynamic+15][columnPos_fixed]))

//! Load a chunk of 16 (ie, "4 * 4Byte") 8-bit organised memory:
#define VECTOR_CHAR_set_data(arrOf_values, rowPos_dynamic) ({*(__m64*)&arrOf_values[rowPos_dynamic]})
#define VECTOR_CHAR_LOAD(arr) (*(__m64*)arr)

//! Note an example-applciaiton is for: "mask[i] = (array[i] > 0) ? 1 : 0;"
// FIXME[jc]: is there a better 'way' to convert a 'float' to a char'?
// FIXME: compare [”elow] "VECTOR_CHAR_convertFrom_float_old(..)" to [ªbove] "VECTOR_CHAR_convertFrom_float(..)"
#define VECTOR_CHAR_convertFrom_float_old(arrOf_values, rowPos_dynamic) ( _mm_set_epi8((char)arrOf_values[rowPos_dynamic+0], (char)arrOf_values[rowPos_dynamic+1], (char)arrOf_values[rowPos_dynamic+2], (char)arrOf_values[rowPos_dynamic+3], (char)arrOf_values[rowPos_dynamic+4], (char)arrOf_values[rowPos_dynamic+5], (char)arrOf_values[rowPos_dynamic+6], (char)arrOf_values[rowPos_dynamic+7], (char)arrOf_values[rowPos_dynamic+8], (char)arrOf_values[rowPos_dynamic+9], (char)arrOf_values[rowPos_dynamic+10], (char)arrOf_values[rowPos_dynamic+11], (char)arrOf_values[rowPos_dynamic+12], (char)arrOf_values[rowPos_dynamic+13], (char)arrOf_values[rowPos_dynamic+14], (char)arrOf_values[rowPos_dynamic+15]))
// FIXME[jc]: is there a better 'way' to convert a 'float' to a char'?
#define VECTOR_CHAR_convertFrom_uint(arrOf_values, rowPos_dynamic) ( _mm_set_epi8((char)arrOf_values[rowPos_dynamic+0], (char)arrOf_values[rowPos_dynamic+1], (char)arrOf_values[rowPos_dynamic+2], (char)arrOf_values[rowPos_dynamic+3], (char)arrOf_values[rowPos_dynamic+4], (char)arrOf_values[rowPos_dynamic+5], (char)arrOf_values[rowPos_dynamic+6], (char)arrOf_values[rowPos_dynamic+7], (char)arrOf_values[rowPos_dynamic+8], (char)arrOf_values[rowPos_dynamic+9], (char)arrOf_values[rowPos_dynamic+10], (char)arrOf_values[rowPos_dynamic+11], (char)arrOf_values[rowPos_dynamic+12], (char)arrOf_values[rowPos_dynamic+13], (char)arrOf_values[rowPos_dynamic+14], (char)arrOf_values[rowPos_dynamic+15]))

/* #define VECTOR_CHAR_convertFrom_float_toBoolean(arrOf_values, rowPos_dynamic) ({ \ */
/*   __m128 vec_float = VECTOR_FLOAT_getBoolean_aboveZero_vec(VECTOR_FLOAT_setFrom_columns_array(arrOf_values, rowPos_dynamic */
/* }) */

// FIXME[jc]: validae correctness of [”elow] _mm_loadu_si128(..) and _mm_storeu_si128(..)
#define VECTOR_CHAR_storeAnd_add(result, vec_add1) (_mm_storeu_si128((__m128i*)result, _mm_add_epi8(_mm_loadu_si128((__m128i*)result), vec_add1)))

#include "def_typeOf_int.h" //! which includes defintions for "int" and "short int"

//!
//! Handle different options wrt. UINT:
#define VECTOR_UINT_ITER_SIZE 4
// FIXME[jc]: may you suggest a better type?
#define VECTOR_UINT_TYPE __m128i
//! Specify a vector wrt. the index-range. 
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]? <-- may we use "_mm_loadu_si32(..)" ??
#define VECTOR_UINT_setFrom_indexRange(index) (_mm_set_epi32((uint)(index+0), (uint)(index+1), (uint)(index+2), (uint)(index+3)))
//! Load a chunk of memory:
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]?
#define VECTOR_UINT_set_data(arrOf, index) (_mm_set_epi32((uint)(arrOf[index+0]), (uint)(arrOf[index+1]), (uint)(arrOf[index+2]), (uint)(arrOf[index+3])))
//! For the dobule data-type specify the row-vector:
//! Note: the number of arguments i [below] reflects VECTOR_FLOAT_ITER_SIZE
#define VECTOR_UINT_setFrom_columns(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_epi32(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed], matrix[rowPos_dynamic+2][columnPos_fixed], matrix[rowPos_dynamic+3][columnPos_fixed]))

//! --- simple aritmetics
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_UINT_MUL _mm_mul_epu32
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_UINT_ADD _mm_add_epi32
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_UINT_SUB _mm_sub_epi32
// FXIME[JC]: mauy you validate correctness of [”elow]?
#define VECTOR_UINT_DIV _mm_div_epi32

// FIXME[jc]: validate correctness of [below]
#define VECTOR_UINT_LOAD(mem_ref) ( _mm_loadu_si128((__m128i*)mem_ref) )
// FIXME[jc]: validate correctness of [below]
#define VECTOR_UINT_STORE(mem_ref, vector) (_mm_store_si128((__m128i*)mem_ref, vector) )

//! Print the cotnents of a vector to stdout
// FIXME[jc]: wrt. [”elow] ... what is the difference when compared to "float *p = (float *)&vec_result";
#define VECTOR_UINT_PRINT(vec_result) ({uint result[4] = {0, 0, 0, 0}; VECTOR_UINT_STORE(result, vec_result); for(uint i = 0; i < 4; i++) {printf("vec[%u] = %u, ", i, result[i]);} fprintf(stdout, "at %s:%d\n", __FILE__, __LINE__); })


//!
//! Handle different options wrt. "float" or "double"
#if VECTOR_CONFIG_USE_FLOAT == 1
//! Define the type, ie, to simplify our allcoations.
typedef float t_float;
#define mathLib_float_abs(scalar) ({fabsf(scalar);})
//! Udpate wrt. the itnrisitnistics:
#define VECTOR_FLOAT_ITER_SIZE 4
#define T_FLOAT_MIN  FLT_MIN
#define T_FLOAT_MIN_ABS  -1*FLT_MAX
#define T_FLOAT_MAX  FLT_MAX
//! --- data-access
#define VECTOR_FLOAT_TYPE __m128
//! ---- basic data-loading:
#define VECTOR_FLOAT_SET _mm_set_ps
#define VECTOR_FLOAT_SET1 _mm_set1_ps
#define VECTOR_FLOAT_SET1_SINGLE _mm_set_ss
#define VECTOR_FLOAT_LOAD _mm_load_ps
#define VECTOR_FLOAT_LOADU _mm_loadu_ps
//for(uint m = 0; m < 4; m++) {res[i][j+m] = mul1[j+m][i];} // <-- FIXME[jc]: may you consider writing an atlernative macro-def where we use the 'base-oeprations' ... ??
#define VECTOR_FLOAT_STORE _mm_store_ps
//! --- simple aritmetics
#define VECTOR_FLOAT_MUL _mm_mul_ps
// FIXEM[tutorial]: seems like we hadd an odd issue where we had defined "VECTOR_FLOAT_SUB _mm_mul_ps" and "VECTOR_FLOAT_ADD _mm_mul_ps" instead of the correct "VECTOR_FLOAT_SUB _mm_sub_ps" and "VECTOR_FLOAT_SUB _mm_add_ps"
#define VECTOR_FLOAT_ADD _mm_add_ps
// FIXME: validate that the result of "VECTOR_FLOAT_ADD_SINGLE" correspdosnt ot he callers expecations.
// FXIME[jc]: is there a better 'fucntion to call' than [”elow]?
/* #define VECTOR_FLOAT_ADD_SINGLE(vec_result, scalar) ( _mm_add_ss(vec_result,  */
#define VECTOR_FLOAT_SUB _mm_sub_ps
// FIXME: validate that the result of "VECTOR_FLOAT_SUB_SINGLE" correspdosnt ot he callers expecations.
#define VECTOR_FLOAT_SUB_SINGLE _mm_sub_ss
#define VECTOR_FLOAT_DIV _mm_div_ps
//! --- boolean lgoics, logics which are used to 'consturct' if-else clauses
#define VECTOR_FLOAT_AND _mm_and_ps
#define VECTOR_FLOAT_AND _mm_and_ps
#define VECTOR_FLOAT_ANDNOT _mm_andnot_ps
#define VECTOR_FLOAT_OR _mm_or_ps
#define VECTOR_FLOAT_XOR _mm_xor_ps
//! Logics for comparison:
#define VECTOR_FLOAT_CMPEQ _mm_cmpeq_ps
#define VECTOR_FLOAT_CMPNEQ _mm_cmpneq_ps
//! --- comparisons
#define VECTOR_FLOAT_MIN _mm_min_ps
#define VECTOR_FLOAT_MAX _mm_max_ps
//! --- conversions
#define VECTOR_FLOAT_convertFrom_epi32 _mm_cvtepi32_ps
#define VECTOR_FLOAT_convertFrom_epi16 _mm_cvtepi16_ps

#define VECTOR_FLOAT_boolean_scalar_isNAN(val) (isnan(val))/*  \ */

//! Revertes the roder of a vector.
#define VECTOR_FLOAT_revertOrder(vec) (_mm_shuffle_ps(vec, vec, _MM_SHUFFLE(0, 1, 2, 3)))
//! Convert a float to an 8-bit intrisinistic char-vector.
//! Note: demonstrate how an 32-bit floatign-point-array may be converted into a char-array
#define VECTOR_CHAR_convertFrom_float_vector(vec) (_mm_cvtps_pi8(vec))
//! Convert from an 8-bit "__m64" to an 32-bit "__mm128"
//! Note: demonstrate how an 8-bit mask-arary may be loaded into a flatoign-potin-array.
#define VECTOR_FLOAT_convertFrom_CHAR(vec) (_mm_cvtpi8_ps(vec))
#define VECTOR_FLOAT_convertFrom_CHAR_buffer(mem_ref) (VECTOR_FLOAT_convertFrom_CHAR(VECTOR_CHAR_LOAD(mem_ref)))
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_INT(vec) (VECTOR_FLOAT_revertOrder(_mm_cvtepi32_ps(vec)))
// FIXME[jc]: validate correctness of [below] ... adn then compare with [above] "VECTOR_FLOAT_convertFrom_INT(..)" wrt. perfroamcne.
#define VECTOR_FLOAT_convertFrom_INT_old(vec) ({ \
  int buffer[VECTOR_FLOAT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec); \
  VECTOR_FLOAT_SET((t_float)buffer[0], (t_float)buffer[1], (t_float)buffer[2], (t_float)buffer[3]); \
})

#define VECTOR_FLOAT_converTo_int_32 _mm_cvttps_epi32 /* ie, convert from float to 32-bit int*/
#define VECTOR_FLOAT_converTo_int_16 _mm_cvttps_epi16 /* ie, convert from float to 32-bit int*/

// FIXME[jc]: validate correctness of [below]  <-- seems like the 'closest' optmial funciton is "__m128d _mm_cvtepu32_pd (__m128i a)" ... ie, only workign for 'dobule' .. ie, an overhead
/* #define VECTOR_FLOAT_convertFrom_UINT(vec) (VECTOR_FLOAT_revertOrder(_mm_cvtepu32_ps(vec))) */
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_UINT(vec) ({ \
  int buffer[VECTOR_FLOAT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec); \
  VECTOR_FLOAT_SET((t_float)buffer[0], (t_float)buffer[1], (t_float)buffer[2], (t_float)buffer[3]); \
})
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_UINT_data(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+0], (t_float)buffer[index+1], (t_float)buffer[index+2], (t_float)buffer[index+3]);})
//! A fucntion which is used to handle diffenret data-type-inptus, eg, "short int"
#define VECTOR_FLOAT_convertFrom_data(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+0], (t_float)buffer[index+1], (t_float)buffer[index+2], (t_float)buffer[index+3]);})
//! A fucntion which is used to handle diffenret data-type-inptus, eg, "short int"
#define VECTOR_FLOAT_convertFrom_data_notInv(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+3], (t_float)buffer[index+2], (t_float)buffer[index+1], (t_float)buffer[index+0]);})


//#define VECTOR_FLOAT_convertFrom_INT _mm_castsi128_pd

// FIXME[jc]: seems like we do nto manage to compiel/'use the _mm_load_pd(..) ... ie, any suggestions 'for how getting this to work'?
// FIXME: wrt. [”elow] write a performance-test where we compare with a to-be-written "VECTOR_FLOAT_LOG_data(..)"
//! @return the log of teh function
#define VECTOR_FLOAT_LOG(vec_input) ({ \
    float buffer[VECTOR_FLOAT_ITER_SIZE]; \
    _mm_store_ps(buffer, vec_input); \
    _mm_set_ps((float)log(buffer[0]), (float)log(buffer[1]), (float)log(buffer[2]), (float)log(buffer[3]));	\
})
//    _mm_set_pd(log(buffer[0]), log(buffer[1]);			
//#define VECTOR_FLOAT_LOG(vec_input) ({double buffer[VECTOR_FLOAT_ITER_SIZE]; _mm_store_pd(buffer, vec_input); _mm_set_pd(log(buffer[0]), log(buffer[1]))} 

//! ------------------------------
//! Sopecify semi-complex macros:

//! For the dobule data-type specify the row-vector:
//! Note: the number of arguments i [below] reflects VECTOR_FLOAT_ITER_SIZE
// FIXME[jc]: would it go faster if we used _mm_load1_ps(..) <-- ?? .... and is there any other magics/trickery? .... and what is the difference wrt. _mm_stream_ps(..) and "_mm_store_ss(..)" ??
// FIXME[jc]: ask jan-chrsitan wrt. the difference between "_mm_store_ps(..)" and "_mm_storeu_ps(..)" .... ie, the implciaton/'requiremnt' of "mem_addr must be aligned on a 16-byte boundary or a general-protection exception may be generated" ["https://software.intel.com/sites/landingpage/IntrinsicsGuide/#expand=10,13,3152,583,0,0,5126,5174&text=_mm_store"] ... and why the latter error is thrown in valgrind ... <--- any stratgies to 'oversecome this'?
#define VECTOR_FLOAT_setFrom_columns(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_ps(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed], matrix[rowPos_dynamic+2][columnPos_fixed], matrix[rowPos_dynamic+3][columnPos_fixed]))


//! Specify a vector wrt. the index-range.
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]?
#define VECTOR_FLOAT_setFrom_indexRange(index) (_mm_set_ps((t_float)(index+0), (t_float)(index+1), (t_float)(index+2), (t_float)(index+3)))

//! Specify a vector wrt. the scalar
#define VECTOR_FLOAT_setFrom_scalar(scalar) (_mm_set1_ps(scalar))

/* //! Intiates a 'fixed-sized' buffer. */
/* #define VECTOR_INIT_BUFFER_EMPTY(buff) () */

//! Use a macro to handle the nan and 0 valeus wrt. "div"
// FIXME[jc]: may you udpate [”elow] to 'handle' "nan" and "0" (ie, if neccessary for correctness)?
#define VECTOR_FLOAT_storeAnd_div(result, vec_div1, vec_div2) (_mm_store_ps(result, _mm_div_ps(vec_div1, vec_div2)))
// FIXME[jc]: may you udpate [”elow] to 'handle' "nan" and "0" (ie, if neccessary for correctness)?
#define VECTOR_FLOAT_storeAnd_div_self(result, vec_div2) (_mm_store_ps(result, _mm_div_ps(_mm_load_ps(result), vec_div2)))

//#define VECTOR_FLOAT_storeAnd_div_self(result, vec_div2) 
/* #define VECTOR_FLOAT_storeAnd_div_self(result, vec_div2) (_mm_store_ps(result, vec_div2)) */
/* #define VECTOR_FLOAT_storeAnd_div_self_data(result, vec_div2) (_mm_store_ps(result, vec_div2)) */

//! Use a macro to stanarize the operation of incrmeneting a varialbe.
// FIXME[jc]: may you try to idneitfy a faster macro than [”elow]?
#define VECTOR_FLOAT_storeAnd_add(result, vec_add1) (_mm_store_ps(result, _mm_add_ps(_mm_load_ps(result), vec_add1)))
#define VECTOR_FLOAT_storeAnd_add_data(result, data_2) (_mm_store_ps(result, _mm_add_ps(_mm_load_ps(result), _mm_load_ps(data_2))))
#define VECTOR_FLOAT_storeAnd_min(result, vec_add1) (_mm_store_ps(result, _mm_min_ps(_mm_load_ps(result), vec_add1)))
#define VECTOR_FLOAT_loadAnd_min_vec_memRef(vec_min, mem_ref) (_mm_min_ps(vec_min, _mm_load_ps(mem_ref) ) )
#define VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_sum, mem_ref) (_mm_add_ps(vec_sum, _mm_load_ps(mem_ref) ) )
#define VECTOR_FLOAT_loadAnd_mult_vec_memRef(vec_sum, mem_ref) (_mm_mul_ps(vec_sum, _mm_load_ps(mem_ref) ) )
#define VECTOR_FLOAT_storeAnd_max(result, vec_add1) (_mm_store_ps(result, _mm_max_ps(_mm_load_ps(result), vec_add1)))
#define VECTOR_FLOAT_storeAnd_max_data(result, mem_addr) (VECTOR_FLOAT_storeAnd_max(result, _mm_load_ps(mem_addr)))



//assert(false) // add somethnng ... ie, get the compielr to 'fail'. <-- first c'ompelte this' when jan-crhistain has valdiated for the 'dobule' case.

// FIXME[jc]: this 
// FIXME[ok]: this 'macro' is not inlcuded int eh 'mien-version' ... ie, udpate the latter.
//! Use a char-mask to filter 
//! Note: the rmul1_Ref and rmul2_ref may be 'called' using: __m128 term1  = _mm_loadu_ps(&rmul1[j2]); __m128 term2  = _mm_loadu_ps(&rmul2[j2]);  
//! Note: teh "_mm_shuffle_ps(..)" is called iot revert the order <-- FIXEM[JC]: si this neccessar ... ie, why does the intrisnticitcs 'revert the order'?
// FIXME[JC]: try to optimzie [below] code. <-- ask jan-crhistain for suggestions.
//#define VECTOR_FLOAT_charMask(rmul1_ref, rmul2_ref, mask1, mask2) ({
// __m128 term1  = _mm_loadu_ps(rmul1_ref); __m128 term2  = _mm_loadu_ps(&rmul2_ref); 

// FIXME: write perofrmance-test to compare "VECTOR_FLOAT_charMask_pair(..)" and "VECTOR_FLOAT_charMask_pair_old(..)"
// FIXME[jc]: when [below] is correct, then update for our 'dobule' 'macro'.
#define VECTOR_FLOAT_charMask_pair(vec_mul, rmask1, rmask2, mask_index) ( VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64(vec_mul, VECTOR_CHAR_LOAD(&rmask1[mask_index]), VECTOR_CHAR_LOAD(&rmask2[mask_index]))) 
//! A permutaiton of our "VECTOR_FLOAT_charMask_pair(..)"
#define VECTOR_FLOAT_charMask_pair_old(vec_mul, rmask1, rmask2, mask_index) ({		\
const __m128 vec_mask1 = _mm_set_ps((float)rmask1[mask_index + 0], (float)rmask1[mask_index + 1], (float)rmask1[mask_index + 2], (float)rmask1[mask_index + 3]); \
const __m128 vec_mask2 = _mm_set_ps((float)rmask2[mask_index + 0], (float)rmask2[mask_index + 1], (float)rmask2[mask_index + 2], (float)rmask2[mask_index + 3]); \
__m128 mask = _mm_and_ps(vec_mask1, vec_mask2); \
  mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); \
_mm_mul_ps(mask, vec_mul); }) //! where the latter returns the result


//! Make use of FLT_MAX in data-data-comparisons, ie, muliply the mask with the copmuted distance: FLT_MAX is used to comptue the AND mask.
// FIXME[JC]: validate correctness of [below] based on [ªbove]
// FIXME[jc]: ask jan-christian to vlaidate correctness of [”elow] ... where intail correctnes-stests are seen/found int ehs tart of our evaluate_cmpOf_two_vectors_wrt_intrisinitstics() in our "graphAlgorithms_timeMeasurements_syntetic.cxx"
// FIXME: validate that callers of 'this' mrembers to sghuffle the result ... ie, wrtie correctness-tests.
#define VECTOR_FLOAT_dataMask_pair(vec_mul, vec_term1, vec_term2) ({ \
      const __m128 vec_empty_empty = _mm_set1_ps(FLT_MAX); 		  const __m128 vec_empty_ones = _mm_set1_ps(1); \
      __m128 vec_cmp_1 = _mm_cmplt_ps(vec_term1, vec_empty_empty); vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones); \
      __m128 vec_cmp_2 = _mm_cmplt_ps(vec_term2, vec_empty_empty); vec_cmp_2 = _mm_and_ps(vec_cmp_2, vec_empty_ones); \
      __m128 mask = _mm_and_ps(vec_cmp_1, vec_cmp_2); mask = _mm_mul_ps(mask, vec_mul);  \
      _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); }) //! where the latter returns the result

// FIXME[jc]: may you validate correctness of [below]?
#define VECTOR_FLOAT_charMask_data1(data_ref, rmask1, mask_index, vec_mask) ({	\
      __m128 vec_term1 = _mm_load_ps(data_ref);				\
      vec_mask = _mm_set_ps((float)rmask1[mask_index + 0], (float)rmask1[mask_index + 1], (float)rmask1[mask_index + 2], (float)rmask1[mask_index + 3]); \
      _mm_mul_ps(vec_mask, vec_term1); }) //! where the latter returns the result
// FIXME[jc]: may you validate correctness of [below]?
#define VECTOR_FLOAT_charMask_data1_implit(data_ref, rmask1, mask_index) ({	\
      const __m128 vec_term1 = _mm_load_ps(data_ref);				\
      const __m128 vec_mask = VECTOR_FLOAT_convertFrom_CHAR_buffer(&rmask1[mask_index]); \
      _mm_mul_ps(vec_mask, vec_term1); }) //! where the latter returns the result
// FIXME[jc]: may you validate correctness of [below]?
#define VECTOR_FLOAT_charMask_data1_implit_old(data_ref, rmask1, mask_index) ({	\
      const __m128 vec_term1 = _mm_load_ps(data_ref);				\
      const __m128 vec_mask = _mm_set_ps((float)rmask1[mask_index + 0], (float)rmask1[mask_index + 1], (float)rmask1[mask_index + 2], (float)rmask1[mask_index + 3]); \
      _mm_mul_ps(vec_mask, vec_term1); }) //! where the latter returns the result

//! Use a data-filter, where we set to empty elements with FLT_MAX, ie, apply the FLT_MAX mask to a signle value.
//! @param <data_ref> is the ANSI C data-strcuture to be used, ie, "&data[k][j]" for a "float **data" structure/topology.

// FIXME[jc]}: may you validate [below]? <-- in this context may you valdiate correctness of the _mm_shuffle_ps(..) funciton-call?
//mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3));	
#define VECTOR_FLOAT_dataMask_data1(data_ref, mask) ({	\
      __m128 vec_term1 = _mm_load_ps(data_ref); \
      __m128 vec_cmp_1 = _mm_cmplt_ps(vec_term1, _mm_set1_ps(FLT_MAX)); vec_cmp_1 = _mm_and_ps(vec_cmp_1, _mm_set1_ps(1)); \
      mask = vec_cmp_1; \
      __m128 vec_mul = _mm_mul_ps(mask, vec_term1); \
vec_mul; }) //! where the latter returns the result
// FIXME[jc]}: may you validate [below]? <-- in this context may you valdiate correctness of the _mm_shuffle_ps(..) funciton-call?
#define VECTOR_FLOAT_dataMask_data1_maskImplicit(data_ref) ({__m128 mask; \
      VECTOR_FLOAT_dataMask_data1(data_ref, mask);}) //! where the latter returns the result
 

/* //! Similar to "VECTOR_FLOAT_dataMask_pair(..)" with differnece that we use "NAN" instead of "FLT_MAX" */
/* #define VECTOR_FLOAT_dataMask_NAN(vec_term1) ({ \ */
/*       __m128 vec_cmp_1 = _mm_cmpneq_ps(vec_term1, _mm_set1_ps(NAN)); vec_cmp_1 = _mm_and_ps(vec_cmp_1, _mm_set1_ps(1)); \ */
/*       __m128 mask = vec_cmp_1; \ */
/*       __m128 vec_mul = _mm_mul_ps(mask, vec_term1); \ */
/* vec_mul; }) //! where the latter returns the result */


//! @return a set of booleans stating if teh vector is above zero, eg, for for a docntiaonal "cmask_tmp > 0" foudn in our "graphAlgorithms_kCluster.cxx" "cluster.c" library.
// FIXME[jc]}: may you validate [below]? <-- in this context may you valdiate correctness of the _mm_shuffle_ps(..) funciton-call?
// _mm_shuffle_ps(vec_cmp_1, vec_cmp_1, _MM_SHUFFLE(0, 1, 2, 3)); 
#define VECTOR_FLOAT_getBoolean_aboveZero_vec(vec_term1) ({ \
      __m128 vec_cmp_1 = _mm_cmpgt_ps(vec_term1, _mm_set1_ps(0)); vec_cmp_1 = _mm_and_ps(vec_cmp_1, _mm_set1_ps(1)); \
      vec_cmp_1;}) //! where the latter returns the result
#define VECTOR_FLOAT_getBoolean_aboveZero(data_ref) ({VECTOR_FLOAT_getBoolean_aboveZero_vec(_mm_load_ps(data_ref));  }) //! where the latter returns the result

//! @return a set of booleans stating if teh vector is above zero, eg, for for a docntiaonal "cmask_tmp > 0" foudn in our "graphAlgorithms_kCluster.cxx" "cluster.c" library.
// FIXME[jc]}: may you validate [below]? <-- in this context may you valdiate correctness of the _mm_shuffle_epi8(..) funciton-call?
#define VECTOR_CHAR_getBoolean_aboveZero(data_ref) ({ \
      __m128i vec_term1 = _mm_loadu_si128(data_ref); \
      const __m128i vec_empty_empty = _mm_set1_epi8(0); 		  const __m128i vec_empty_ones = _mm_set1_epi8(1); \
      __m128i vec_cmp_1 = _mm_cmpgt_epi8(vec_term1, vec_empty_empty); vec_cmp_1 = _mm_and_ps(vec_cmp_1, vec_empty_ones); \
      _mm_shuffle_epi8(vec_cmp_1, vec_cmp_1);  }) //! where the latter returns the result


//! Print the cotnents of a vector to stdout
// FIXME[jc]: wrt. [”elow] ... what is the difference when compared to "float *p = (float *)&vec_result";
#define VECTOR_FLOAT_PRINT(vec_result) ({float result[4] = {0, 0, 0, 0}; _mm_store_ps(result, vec_result); printf("["); for(uint i = 0; i < 4; i++) {printf("%f, ", (float)result[i]);} fprintf(stdout, "], at %s:%d\n", __FILE__, __LINE__); })
#define VECTOR_FLOAT_PRINT_wMessage(msg, vec_result) ({float result[4] = {0, 0, 0, 0}; _mm_store_ps(result, vec_result); printf("%s [", msg); for(uint i = 0; i < 4; i++) {printf("%f, ", (float)result[i]);} fprintf(stdout, "], at %s:%d\n", __FILE__, __LINE__); })
// #define VECTOR_FLOAT_PRINT(vec_result) ({float result[4] = {0, 0, 0, 0}; _mm_store_ps(result, vec_result); for(uint i = 0; i < 4; i++) {printf("vec[%u] = %f, ", i, result[i]);} fprintf(stdout, "at %s:%d\n", __FILE__, __LINE__); })




//! Update a resutl-vector based on two inptu-values.    
//! @remarks describe logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
//! @remarks we call the "mask = _mm_mul_ps(mask, mask);" iot. to 'handle' the "-1" signs.
//! @remarks we make use of "_mm_mul_ps" as a 'pre-step' before "_mm_cmpgt_pi8" as there does not exist any opators named "_mm_cmpne_pi8(..)"
// FIXME[aritcle]: describe a generalized applciaton wrt. [”elow] 'template'.
#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64_xmtShuffle(vec_mul, vec_term1, vec_term2) ({ \
      __m64 vec_cmp_1 = _mm_cmpgt_pi8(_mm_mul_su32(vec_term1, vec_term1), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(_mm_mul_su32(vec_term2, vec_term2), _mm_set1_pi8(0)); \
      __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	\
      mask = _mm_mul_ps(mask, mask);  \
      __m128 vec_result = _mm_mul_ps(mask, vec_mul); \
      vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \
      vec_result;}) //! ie, return


//! Update a resutl-vector based on two inptu-values.    
//! @remarks describe logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
//! @remarks we call the "mask = _mm_mul_ps(mask, mask);" iot. to 'handle' the "-1" signs.
// FIXME[aritcle]: describe a generalized applciaton wrt. [”elow] 'template'.
#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64(vec_mul, vec_term1, vec_term2) ({ \
  __m128 vec_result = VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64_xmtShuffle(vec_mul, vec_term1, vec_term2); \
      vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \
      vec_result;}) //! ie, return


//! Update a resutl-vector based on two inptu-values.    
//! @remarks describe logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
//! @remarks we call the "mask = _mm_mul_ps(mask, mask);" iot. to 'handle' the "-1" signs.
//! @remarks we make use of "_mm_mul_ps" as a 'pre-step' before "_mm_cmpgt_pi8" as there does not exist any opators named "_mm_cmpne_pi8(..)"
// FIXME[aritcle]: describe a generalized applciaton wrt. [”elow] 'template'.
//
//#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(vec_mul, char_term1, char_term2) (VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_m64_m64_xmtShuffle(vec_mul, VECTOR_CHAR_LOAD(char_term1), VECTOR_CHAR_LOAD(char_term2)))
#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_resultIsInverted(vec_mul, char_term1, char_term2) ({ \
    __m64 vec_1 = VECTOR_CHAR_LOAD(char_term1); __m64 vec_2 = VECTOR_CHAR_LOAD(char_term2); \
    /*VECTOR_FLOAT_PRINT_wMessage("vec_1: ", VECTOR_FLOAT_convertFrom_CHAR(vec_1));*/ \
    /* VECTOR_FLOAT_PRINT_wMessage("vec_2: ", VECTOR_FLOAT_convertFrom_CHAR(vec_2));*/ \
    VECTOR_CHAR_TYPE mask_char = _mm_and_si64(vec_1, vec_2); \
    VECTOR_FLOAT_TYPE mask =     VECTOR_FLOAT_convertFrom_CHAR(mask_char); \
    mask = VECTOR_FLOAT_revertOrder(mask); \
    /*VECTOR_FLOAT_PRINT_wMessage("mask: ", mask); */ \
    VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_mul, mask); \
    vec_result;}) //! ie, return

#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(vec_mul, char_term1, char_term2) ({VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_resultIsInverted(vec_mul, char_term1, char_term2));})

#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData_old(vec_mul, char_term1, char_term2) ({ \
  __m64 vec_1 = VECTOR_CHAR_LOAD(char_term1); __m64 vec_2 = VECTOR_CHAR_LOAD(char_term2); \
  __m64 vec_cmp_1 = _mm_cmpgt_pi8(_mm_mul_su32(vec_1, vec_1), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(_mm_mul_su32(vec_2, vec_2), _mm_set1_pi8(0)); \
      __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	\
      mask = _mm_mul_ps(mask, mask);  \
      mask = _mm_shuffle_ps(mask, mask, _MM_SHUFFLE(0, 1, 2, 3)); \
      __m128 vec_result = _mm_mul_ps(mask, vec_mul); \
      vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \
      vec_result;}) //! ie, return

/* #define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0_wArgs_float_charData_charData(vec_mul, char_term1, char_term2) ({ \ */
/*       __m64 vec_cmp_1 = _mm_cmpgt_pi8(VECTOR_CHAR_LOAD(char_term1), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(VECTOR_CHAR_LOAD(char_term2), _mm_set1_pi8(0)); \ */
/*       __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	\ */
/*       mask = _mm_mul_ps(mask, mask);  \ */
/*       __m128 vec_result = _mm_mul_ps(mask, vec_mul); \ */
/*       vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \ */
/*       vec_result;}) //! ie, return */



//! Update a resutl-vector based on two inptu-values.    
//! @remarks describe logics for: "if( (a[i] != 0) && (b[i] != 0) ) { /* then do somethign*/ }":
//! @remarks we call the "mask = _mm_mul_ps(mask, mask);" iot. to 'handle' the "-1" signs.
//! @remarks we make use of "_mm_mul_ps" as a 'pre-step' before "_mm_cmpgt_pi8" as there does not exist any opators named "_mm_cmpne_pi8(..)"
// FIXME[aritcle]: describe a generalized applciaton wrt. [”elow] 'template'.
// vec_result = _mm_shuffle_ps(vec_result, vec_result, _MM_SHUFFLE(0, 1, 2, 3)); 
#define VECTOR_FLOAT_vectorCombine_setTo_0_ifAtLeastOneIs_0(vec_mul, vec_term1, vec_term2) ({ \
      __m64 vec_cmp_1 = _mm_cmpgt_pi8(_mm_cvtps_pi8(_mm_mul_ps(vec_term1, vec_term2)), _mm_set1_pi8(0)); __m64 vec_cmp_2 = _mm_cmpgt_pi8(_mm_cvtps_pi8(_mm_mul_ps(vec_term2, vec_term2)), _mm_set1_pi8(0)); \
      __m128 mask = _mm_cvtpi8_ps(_mm_and_si64(vec_cmp_1, vec_cmp_2));	\
      mask = _mm_mul_ps(mask, mask);  \
      __m128 vec_result = _mm_mul_ps(mask, vec_mul); \
      vec_result;}) //! ie, return



//! Call a function seperately for each value (of the indexes) in the input-vector.
//! @return the vector which is the result of the values mapped (ie, 'applied') to the function.
//! @remarks demonstrates the applicacbailbity of a "map" function wrt. macros.
// FIXME: wrt. [”elow] write a performacne-test where we vvalaute the time-cost of an 'itnermeidate' fuction-call 'in covnerison' ... eg, to describe ...??..
// FIXME: in [”elow] validate that it is always correct to use "VECTOR_FLOAT_revertOrder(..)" <-- first 'discovered' when writing correctnes-tests for our "mine.c" impelmetnation.
#define VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec, funcRef_float) ({ \
      union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec;				\
      VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_SET(funcRef_float(v.buffer[0]), funcRef_float(v.buffer[1]), funcRef_float(v.buffer[2]), funcRef_float(v.buffer[3]))); }) //! ie, return.



//! **************************************************************************
#else
//! Define the type, ie, to simplify our allcoations.
typedef double t_float;
#define mathLib_float_abs(scalar) ({fabs(scalar);})

#define T_FLOAT_MIN  DBL_MIN
#define T_FLOAT_MAX  DBL_MAX
#define T_FLOAT_MIN_ABS  -1*DBL_MAX
//! Udpate wrt. the itnrisitnistics:
#define VECTOR_FLOAT_ITER_SIZE 2
//! --- data-access
#define VECTOR_FLOAT_TYPE __m128d
#define VECTOR_FLOAT_SET _mm_set_pd
#define VECTOR_FLOAT_SET1 _mm_set1_pd
#define VECTOR_FLOAT_SET1_SINGLE _mm_set_sd
#define VECTOR_FLOAT_LOAD _mm_load_pd
#define VECTOR_FLOAT_LOADU _mm_loadu_pd
#define VECTOR_FLOAT_STORE _mm_store_pd
//! --- simple aritmetics
#define VECTOR_FLOAT_MUL _mm_mul_pd
#define VECTOR_FLOAT_ADD _mm_add_pd
// FIXME: validate that the result of "VECTOR_FLOAT_ADD_SINGLE" correspdosnt ot he callers expecations.
#define VECTOR_FLOAT_ADD_SINGLE _mm_add_sd
// FIXME: validate that the result of "VECTOR_FLOAT_SUB_SINGLE" correspdosnt ot he callers expecations.
#define VECTOR_FLOAT_SUB_SINGLE _mm_sub_sd
#define VECTOR_FLOAT_SUB _mm_sub_pd
#define VECTOR_FLOAT_DIV _mm_div_pd
//! --- comparisons
#define VECTOR_FLOAT_MIN _mm_min_pd
#define VECTOR_FLOAT_MAX _mm_max_pd
//! --- conversions
#define VECTOR_FLOAT_convertFrom_epi32 _mm_cvtepi32_pd

//! Revertes the roder of a vector.
#define VECTOR_FLOAT_revertOrder(vec) (_mm_shuffle_pd(vec, vec, _MM_SHUFFLE(0, 1)))
//! Convert a float to an 8-bit intrisinistic char-vector.
//! Note: demonstrate how an 32-bit floatign-point-array may be converted into a char-array
#define VECTOR_CHAR_convertFrom_float_vector(vec) (_mm_cvtpd_pi8(vec))
//! A fucntion which is used to handle diffenret data-type-inptus, eg, "short int"
#define VECTOR_FLOAT_convertFrom_data(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+0], (t_float)buffer[index+1]);})
#define VECTOR_FLOAT_convertFrom_data_notInv(buffer, index) ({VECTOR_FLOAT_SET((t_float)buffer[index+1], (t_float)buffer[index+0]);})

#define VECTOR_FLOAT_boolean_scalar_isNAN(val) (isnan(val))/*  \ */
  /* assert(sizeof(t_float) == sizeof(int));  /\* FIXME: remove this to ...??...  *\/ \ */
//float scalar = NAN;
// FIXME[jc]: validate correctness of [below]
#define VECTOR_FLOAT_convertFrom_INT(vec) ({ \
  int buffer[VECTOR_FLOAT_ITER_SIZE]; VECTOR_INT_STORE(buffer, vec); \
VECTOR_FLOAT_SET((t_float)buffer[0], (t_float)buffer[1]); \
})
//#define VECTOR_FLOAT_convertFrom_INT _mm_castsi128_pd

// FIXME[jc]: seems like we do nto manage to compiel/'use the _mm_load_pd(..) ... ie, any suggestions 'for how getting this to work'?
//! @return the log of teh function
#define VECTOR_FLOAT_LOG(vec_input) ({ \
    double buffer[VECTOR_FLOAT_ITER_SIZE]; \
    _mm_store_pd(buffer, vec_input); \
    _mm_set_pd((double)log(buffer[0]), (double)log(buffer[1]));	\
})
//    _mm_set_pd(log(buffer[0]), log(buffer[1]);			
//#define VECTOR_FLOAT_LOG(vec_input) ({double buffer[VECTOR_FLOAT_ITER_SIZE]; _mm_store_pd(buffer, vec_input); _mm_set_pd(log(buffer[0]), log(buffer[1]))} 

//! ------------------------------
//! Sopecify semi-complex macros:

//! For the dobule data-type specify the row-vector:
//! Note: the number of arguments i [below] reflects VECTOR_FLOAT_ITER_SIZE
#define VECTOR_FLOAT_setFrom_columns(matrix, rowPos_dynamic, columnPos_fixed) (_mm_set_pd(matrix[rowPos_dynamic+0][columnPos_fixed], matrix[rowPos_dynamic+1][columnPos_fixed]))

//! Specify a vector wrt. the index-range.
// FIXME[jc]: is there any 'itnernal and faster fucntion' wrt. [”elow]?
#define VECTOR_FLOAT_setFrom_indexRange(index) (_mm_set_pd((t_float)(index+0), (t_float)(index+1)))

//! Specify a vector wrt. the scalar
// FIXME: in [below] consider to instead use _mm_set1_pd(scalar)
#define VECTOR_FLOAT_setFrom_scalar(scalar) (_mm_set_pd(scalar, scalar))

/* //! Intiates a 'fixed-sized' buffer. */
/* #define VECTOR_INIT_BUFFER_EMPTY(buff) () */

//! Use a macro to handle the nan and 0 valeus wrt. "div"
// FIXME[jc]: may you udpate [”elow] to 'handle' "nan" and "0" (ie, if neccessary for correctness)?
#define VECTOR_FLOAT_storeAnd_div(result, vec_div1, vec_div2) (_mm_store_pd(result, _mm_div_pd(vec_div1, vec_div2)))
// FIXME[jc]: may you udpate [”elow] to 'handle' "nan" and "0" (ie, if neccessary for correctness)?
#define VECTOR_FLOAT_storeAnd_div_self(result, vec_div2) (_mm_store_pd(result, _mm_div_pd(_mm_load_pd(result), vec_div2)))

//! Convert from an 8-bit "__m64" to an 64-bit "__mm128d"
//! Note: demonstrate how an 8-bit mask-arary may be loaded into a flatoign-potin-array.
#define VECTOR_FLOAT_convertFrom_CHAR(vec) (_mm_cvtpi8_pd(vec))



//! Call a function seperately for each value (of the indexes) in the input-vector.
//! @return the vector which is the result of the values mapped (ie, 'applied') to the function.
//! @remarks demonstrates the applicacbailbity of a "map" function wrt. macros.
// FIXME: wrt. [”elow] write a performacne-test where we vvalaute the time-cost of an 'itnermeidate' fuction-call 'in covnerison' ... eg, to describe ...??..
#define VECTOR_FLOAT_SET_fromFunc_vectorValueAsIndex(vec, funcRef_float) ({ \
      union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec;				\
      VECTOR_FLOAT_SET(funcRef_float(v.buffer[0]), funcRef_float(v.buffer[1])); }) //! ie, return.




// #define VECTOR_FLOAT_

//! **************************************************************************
#endif


//! @return the interation-size with offset:
#define macro__get_maxIntriLength_float(length) ( (length > VECTOR_FLOAT_ITER_SIZE) ? (length - VECTOR_FLOAT_ITER_SIZE) : 0  )
//! @return the interation-size with offset:
#define macro__get_maxIntriLength_uint(length) ( (length > VECTOR_UINT_ITER_SIZE) ? (length - VECTOR_UINT_ITER_SIZE) : 0  )

//! @return the valeu at the assicated index
// FIXME: try to update our examples and oru macros to use [beloł] .. ie, to not alwyas make use of the buffer[4] 'code-strategy'
union UNION_VECTOR_FLOAT_TYPE { VECTOR_FLOAT_TYPE sse; t_float buffer[VECTOR_FLOAT_ITER_SIZE]; } ;
#define VECTOR_FLOAT_getAtIndex(vec, index) ({ union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec; v.buffer[index];})



// FIXME[jc]: is there a better approach to avodi the "NAN" value (ie, than converting to-and-from "char")? <-- if so, then update functions which makes use of "_mm_cmpeq_pi8(..)". 
#define VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(vec) ({ \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_revertOrder(VECTOR_FLOAT_convertFrom_CHAR(VECTOR_CHAR_convertFrom_float_vector(vec))); \
  VECTOR_FLOAT_MUL(vec_result, VECTOR_FLOAT_SET1(__constant_float_charMin));}) //! ie, return.


// #define VECTOR_FLOAT_converTo_int_32 _mm_cvttpd_epi32 /* ie, convert from float to 32-bit int*/
// #define VECTOR_FLOAT_converTo_int_16 _mm_cvttpd_epi16 /* ie, convert from float to 32-bit int*/


/* //! A generic procedure to convert floating-point-numbers into valeus, eg, from "32-bit float"  to "16-bit int" */
/* // FIXME[jc]: given the applicaiton/usage of the "VECTOR_FLOAT_convertTo_dataPoiner(..)" funciton ... do you manage to imrpove/udpate 'this funciotn wrt. performance'? */
/* #define VECTOR_FLOAT_convertTo_dataPoiner(mem_ref_result, index, vec_input, typeOf_result) ({ \ */
/*       union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_input; 		\ */
/*       for(uint i = 0; i < VECTOR_FLOAT_ITER_SIZE; i++) { \ */
/* 	mem_ref_result[index+i] = (typeOf_result)v.buffer[i];	 \ */
/* }}) */


//! A generic procedure to convert floating-point-numbers into valeus, eg, from "32-bit float"  to "16-bit int"
// FIXME[jc]: given the applicaiton/usage of the "VECTOR_FLOAT_convertTo_dataPoiner(..)" funciton ... do you manage to imrpove/udpate 'this funciotn wrt. performance'?
#define VECTOR_FLOAT_convertTo_dataPoiner(mem_ref_result, index, vec_input, typeOf_result) ({ \
      union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_input; 		\
      for(uint i = 0; i < VECTOR_FLOAT_ITER_SIZE; i++) { \
	mem_ref_result[index+i] = (typeOf_result)v.buffer[i];	 \
	/* printf("[%u]=[%u] = %f = %d, at %s:%d\n", index+i, i, v.buffer[i], (int)mem_ref_result[index+i], __FILE__, __LINE__); */ \
}})


//! Handle the "nan" case arising from a "0/value" and "value/0" case, ie, by replacing each "nan" with "0"
//! Note: idea is to use/apply an explicit mask-proeprty: if(mask[i]) { /* then some action, ie, a set of instructions */ } 
//! Note: if this code-block we examplify how 'boolean type-covnerison' from float may be used to ...??..
//! Note: conseptually we in [”elow] macro compute "div-result - mask*nan". However, as "nan-nan = nan", the latter approach is meaningless. To overcome the latter we therefore first replace all '0's with 'an rbitrary value', adn then 'clear all vectors witht eh arbitrary value' after the comptuation.
//! Note: in this procedure the followin steps are taken/followed: step(1) Replace '0' with a different 'arbirary' value; step(2) Compute the div-result; step(3) Remove the values which were inferred to be '0' .... ie, step(1) identify the vector-items with "value == 0", ie, a 'boolean mask'; step(2) replace all occurences of "0" with 'a different value' (eg, "xor"); step(3) compute "div"; step(4) multiply the result with the 'boolean mask'.
// FIXME[tutorial] ... sems like [”elow] exmaple is missing from both stack-voerflow posts and course-slides (wrt. presentaitons held at universities around the wolrd) ... 
#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(vec_term1, vec_term2, funcRef_float) ({ \
  VECTOR_FLOAT_TYPE vec_cmpNonZero_1 = VECTOR_FLOAT_CMPEQ(vec_term1, VECTOR_FLOAT_SET1(0)); \
  /* VECTOR_FLOAT_PRINT_wMessage("vec non-zero-1", vec_cmpNonZero_1); */ \
  VECTOR_FLOAT_TYPE vec_cmpNonZero_2 = VECTOR_FLOAT_CMPEQ(vec_term2, VECTOR_FLOAT_SET1(0)); \
  /* VECTOR_FLOAT_PRINT_wMessage("vec non-zero-2", vec_cmpNonZero_1); */ \
  /*! Step(3): Find all values which are zero: */ \
  VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_OR(vec_cmpNonZero_1, vec_cmpNonZero_2); \
  /* VECTOR_FLOAT_PRINT_wMessage("vec-notZero", mask); */ \
  /*! Note: we need to use the 'ant-not' operatnd to 'clear' the NAN-valeus: if the latter had nott bene the case/issue, then we instead coudl have 'ivnerted' above lgocis.*/ \
  mask = VECTOR_FLOAT_ANDNOT(mask, VECTOR_FLOAT_SET1(1)); \
  /*! Step(4): 'revert' the zeros: */ \
  /* VECTOR_FLOAT_PRINT_wMessage("vec-notZero and 1: ", vec_notZero); */ \
  /* Comptue the scores: */ \
  VECTOR_FLOAT_TYPE vec_result = funcRef_float(vec_term1, vec_term2); \
  VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
  vec_result = vec_subtract; /*VECTOR_FLOAT_SUB(vec_result, vec_subtract);*/ \
  VECTOR_FLOAT_revertOrder(vec_result);}) //! ie, return.
//! Similar to "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(..)", with difference that we here only evaluate for one input-vector.
//	VECTOR_FLOAT_TYPE vec_result = 
//
//#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(vec_term1) ({ 
#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d(vec_term1, funcRef_float_1d) ({ \
  VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_convertFrom_CHAR(_mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), VECTOR_CHAR_SET1(0))); \
  mask = VECTOR_FLOAT_MUL(mask, mask); \
  const VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, mask); \
  VECTOR_FLOAT_TYPE vec_result = funcRef_float_1d(vec_altValue_1);	\
  vec_result = VECTOR_FLOAT_revertOrder(vec_result);			\
  const VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
  vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract); }) //! ie, return.


#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_1(vec_term1, vec_term2, funcRef_float) ({ \
      const __m64 vec_cmp_1 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), _mm_set1_pi8(0)); \
      const __m64 vec_cmp_2 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term2), _mm_set1_pi8(0)); \
      VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_convertFrom_CHAR(_mm_or_si64(vec_cmp_1, vec_cmp_2)); mask = _mm_mul_ps(mask, mask); \
	VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, mask); \
	VECTOR_FLOAT_TYPE vec_altValue_2 = VECTOR_FLOAT_ADD(vec_term2, mask); \
	VECTOR_FLOAT_TYPE vec_result = funcRef_float(vec_altValue_1, vec_altValue_2); \
	VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
 	vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract); \
	VECTOR_FLOAT_revertOrder(vec_result);}) //! ie, return.

//! Note: in contraost to "VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(..)" below 'introduces an unnceccesary ovrheead' wrt. "VECTOR_FLOAT_MUL(mask, VECTOR_FLOAT_SET1(FLT_MAX))" (where the latter was foudn in our first correct/wroking version fo thsi macro-function) (oekseth, 06. june 2016).
#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_old_2(vec_term1, vec_term2, funcRef_float) ({ \
	const __m64 vec_cmp_1 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), _mm_set1_pi8(0)); \
	const __m64 vec_cmp_2 = _mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term2), _mm_set1_pi8(0)); \
	VECTOR_FLOAT_TYPE mask = _mm_cvtpi8_ps(_mm_or_si64(vec_cmp_1, vec_cmp_2)); mask = _mm_mul_ps(mask, mask); \
	VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, VECTOR_FLOAT_MUL(mask, VECTOR_FLOAT_SET1(FLT_MAX))); \
	VECTOR_FLOAT_TYPE vec_altValue_2 = VECTOR_FLOAT_ADD(vec_term2, VECTOR_FLOAT_MUL(mask, VECTOR_FLOAT_SET1(FLT_MAX))); \
	VECTOR_FLOAT_TYPE vec_result = funcRef_float(vec_altValue_1, vec_altValue_2); \
	VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
 	vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract); \
	VECTOR_FLOAT_revertOrder(vec_result);}) //! ie, return.


#define VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand_1d_old_1(vec_term1, funcRef_float_1d) ({ \
  VECTOR_FLOAT_TYPE mask = VECTOR_FLOAT_convertFrom_CHAR(_mm_cmpeq_pi8(VECTOR_CHAR_convertFrom_float_vector(vec_term1), VECTOR_CHAR_SET1(0))); \
  mask = VECTOR_FLOAT_MUL(mask, mask); \
  const VECTOR_FLOAT_TYPE vec_altValue_1 = VECTOR_FLOAT_ADD(vec_term1, mask); \
  VECTOR_FLOAT_TYPE vec_result = funcRef_float_1d(vec_altValue_1);	\
  vec_result = VECTOR_FLOAT_revertOrder(vec_result);			\
  const VECTOR_FLOAT_TYPE vec_subtract  = VECTOR_FLOAT_MUL(mask, vec_result); \
  vec_result = VECTOR_FLOAT_SUB(vec_result, vec_subtract); }) //! ie, return.



//! Divide the two numbers, seeting 'divide by zero' case to '0'.
#define VECTOR_FLOAT_omitZeros_fromComputation_DIV(vec_term1, vec_term2) ({ \
  /*! Merge the input-terms: */ \
  VECTOR_FLOAT_TYPE mask = vec_term2; \
  mask = VECTOR_FLOAT_CMPNEQ(mask, VECTOR_FLOAT_SET1(0));	\
  /*! Clear the NAN signs: */					\
  mask = VECTOR_FLOAT_AND(mask, VECTOR_FLOAT_SET1(1));		\
  /*! In order to avoid nan-cases each '0' valeu need to be set to 1: */ \
  VECTOR_FLOAT_TYPE mask_inv = VECTOR_FLOAT_ANDNOT(mask, VECTOR_FLOAT_SET1(1)); \
  /*! Increment 'zero-valeus': */					\
  VECTOR_FLOAT_TYPE vec_term2_wMask = VECTOR_FLOAT_ADD(vec_term2, mask_inv); \
  /*! Set to 'zero' the valeus which are to be empty: */		\
  VECTOR_FLOAT_TYPE vec_1_mask = VECTOR_FLOAT_AND(VECTOR_FLOAT_SET1(1), mask); \
  /*! Apply the mask to term2: */					\
  VECTOR_FLOAT_TYPE vec_divBy_term2 = VECTOR_FLOAT_DIV(vec_1_mask, vec_term2_wMask); \
  /*VECTOR_FLOAT_PRINT_wMessage("1/term2: ", vec_divBy_term2);	*/	\
  /*! Multiply the tersult with term1: */				\
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_term1, vec_divBy_term2); \
  vec_result; })

#define VECTOR_FLOAT_omitZeros_fromComputation_DIV_old_2(vec_term1, vec_term2) ({VECTOR_FLOAT_omitZeros_fromComputation_useDefinedOperand(vec_term1, vec_term2, VECTOR_FLOAT_DIV);})

//! COmpute shannons etnropy:
// FIXME[jc]: validate our [”elow] "VECTOR_FLOAT_convertFrom_INT(..)" 'proceudre'
#define VECTOR_MATH_shannonEntropy_float(result_tmp, vec_divResult) ({ \
  VECTOR_FLOAT_TYPE vec_mul = VECTOR_FLOAT_MUL(vec_divResult, VECTOR_FLOAT_LOG(vec_divResult)); \
  VECTOR_FLOAT_STORE(result_tmp, VECTOR_FLOAT_ADD(VECTOR_FLOAT_LOAD(result_tmp), vec_mul)); \
})
#define VECTOR_MATH_shannonEntropy_float_divInputPair(result_tmp, vec_div_head, vec_div_tail) ({ \
  VECTOR_FLOAT_TYPE vec_divResult = VECTOR_FLOAT_omitZeros_fromComputation_DIV(vec_div_head, vec_div_tail); \
  VECTOR_MATH_shannonEntropy_float(result_tmp, vec_divResult); })


//! @return the for-loop-size to be sued.
#define VECTOR_FLOAT_maxLenght_forLoop(size) ( (size >= VECTOR_FLOAT_ITER_SIZE) ? size - VECTOR_FLOAT_ITER_SIZE : 0 )
//! @return the for-loop-size to be sued.
#define VECTOR_CHAR_maxLenght_forLoop(size) ( (size >= VECTOR_CHAR_ITER_SIZE) ? size - VECTOR_CHAR_ITER_SIZE : 0 )
//! @return the for-loop-size to be sued.
#define VECTOR_UINT_maxLenght_forLoop(size) ( (size >= VECTOR_UINT_ITER_SIZE) ? size - VECTOR_UINT_ITER_SIZE : 0 )

/* // FIXME[jc]: may you imrpove speed of [”elow] funciton? */
/* #define VECTOR_FLOAT_MEMSET(arr, size, value) ({ */

/* }) */

/** // FIXME[jc] may you support [”elow] new-added 'operations'?
    

 **/

//! Copmute "result = result/data_2" for two "float *" memory-references
#define VECTOR_FLOAT_storeAnd_div_self_data(result, data_2) (VECTOR_FLOAT_storeAnd_div_self(result, VECTOR_FLOAT_LOAD(data_2)))



//! Transpsoes a given array.
//! @remarks optmized to store a value without loading a cache line, ie, use intrinsic functions:
//! @remarks for an 'innline' example of matrix-transpsotion see our "graphAlgorithms_distance::compute_transposedMatrix_float(..)"
// FIXME[JC]: may you both validate correctness of [”elow] ... and describe applications of [below] wrt. 'gluing' into software-applicaitonis?
#define VECTOR_FLOAT_transposeMatrix_forColumn(matrix_res, matrix_input, index_row, col_size) ({ \
  const uint size_innerOptimized = (col_size > VECTOR_FLOAT_ITER_SIZE) ? col_size - VECTOR_FLOAT_ITER_SIZE : 0; \
  uint j = 0; \
  if(size_innerOptimized > 0) { \
    for (; j < size_innerOptimized; j += VECTOR_FLOAT_ITER_SIZE) {	\
      VECTOR_FLOAT_STORE(&matrix_res[index_row][j], VECTOR_FLOAT_setFrom_columns(matrix_input, j, index_row)); \
     } \
  } \
  for (; j < col_size; j += 1) { \
    matrix_res[index_row][j] = matrix_input[j][index_row];			\
  } \
})




//! Convert from a CHAR-data-array to a FLOAT vector.
#define VECTOR_FLOAT_convertFrom_CHAR_data(arrOf_values, index) (VECTOR_FLOAT_convertFrom_CHAR(*(__m64*)&arrOf_values[index]))


//! Print the cotnents of a vector to stdout
// FIXME[jc]: may you try to idnetify a mroe effective method/approach than [”elow] instruciton/funciton?
#define VECTOR_CHAR_PRINT(vec_result) (VECTOR_FLOAT_PRINT(VECTOR_FLOAT_convertFrom_CHAR(vec_result)))

//! Convert a float to an 8-bit intrisinistic char-vector.
//! Note: demonstrate how an 32-bit floatign-point-array may be converted into a char-array
#define VECTOR_CHAR_convertFrom_float(arrOf_values, rowPos_dynamic) (VECTOR_CHAR_convertFrom_float_vector(VECTOR_FLOAT_LOAD(&arrOf_values[rowPos_dynamic])))




//! Idneitfy the min-value in a flaot-vector.
// FIXME[jc]: si there a mroe effective instrinsitctc-appraoch tahn [”elow]?
#define VECTOR_FLOAT_storeAnd_horizontalMin(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = T_FLOAT_MAX; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(result[m] < scalar_result) {scalar_result = result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalMin_alt1(vec) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec; \
  t_float scalar_result = T_FLOAT_MAX; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(v.buffer[m] < scalar_result) {scalar_result = v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.
//! Idneitfy the max-value in a flaot-vector.
// FIXME[jc]: si there a mroe effective instrinsitctc-appraoch tahn [”elow]? 
// FIXME[time]: write a performacne-test to comapre [”elow] with an 'altertive' union-of operation-call.
//! Note[article]: ... an interesting bug which we made when suign this funcitonw as to intiate a "VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET1(0);", though only set a signular vlaue using our "VECTOR_FLOAT_SUB_SINGLE", ie, where the result is that values 'less than zero' is ignored, ie, errnous reeulsts. This 'example-note' therefore (ie, among otehrs) illsutrates the bug-case where 'intiation to 0' results in result-errors.
/* printf("scalar_result=%f == %f, at %s:%d\n", scalar_result, T_FLOAT_MIN_ABS, __FILE__, __LINE__);  */
/*   printf("cmp: %.10f VS %.10f, at %s:%d\n", result[m], scalar_result, __FILE__, __LINE__);  */
/* assert(T_FLOAT_MIN_ABS != 0);  assert(scalar_result < -1);  */
#define VECTOR_FLOAT_storeAnd_horizontalMax(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = T_FLOAT_MIN_ABS;  for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(result[m]  > scalar_result) {scalar_result = result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalMax_alt(vec_add1) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_add1;  \
  t_float scalar_result = T_FLOAT_MIN_ABS;  for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { if(v.buffer[m] > scalar_result) {scalar_result = v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.

//! Idneitfy the min-value in a flaot-vector.
// FIXME: write a perforamnce-test where we compare [”elow] 'approach' with suign the "UNION_VECTOR_FLOAT_TYPE" 'appraoch'.
// FIXME[jc]: si there a mroe effective instrinsitctc-appraoch tahn [”elow]?
#define VECTOR_FLOAT_storeAnd_horizontalSum(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result += result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalSum_alt1(vec) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec; \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result += v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalMult(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result *= result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalMult_alt1(vec) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec; \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result *= v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalSum_subtract(vec_add1) ({t_float result[VECTOR_FLOAT_ITER_SIZE] = {0, 0, 0, 0}; VECTOR_FLOAT_STORE(result, vec_add1); \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result -= result[m];} } \
  scalar_result; }) //! ie, return the result.
#define VECTOR_FLOAT_storeAnd_horizontalSum_subtract_alt(vec_add1) ({union UNION_VECTOR_FLOAT_TYPE v; v.sse = vec_add1; \
  t_float scalar_result = 0; for(uint m = 0; m < VECTOR_FLOAT_ITER_SIZE; m++) { {scalar_result -= v.buffer[m];} } \
  scalar_result; }) //! ie, return the result.

//! @return the mini-distance for an array
//! Note: we assume that ( (endPosPlussOne - start_pos) % VECTOR_FLOAT_ITER_SIZE) == 0, ie, to recue the ovrehead wrt. our appraoch.
// FIXME[tutorial]: ... consider to write an errnous version of [”elow] funciton where we dorp the "= VECTOR_FLOAT_SET1(T_FLOAT_MAX)" assignmetn .. and then ask users/studnets to fidn the soruce of the eror ... ie, as (a) the eror descries a previous cocnrete/real bug (iin oru software) and (b) 'states' the default intaition to '0' for vectors. <-- validate that the latter assum,ption laways hold
#define  VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arrOf_distance, start_pos, endPosPlussOne) ({ \
      VECTOR_FLOAT_TYPE vec_min = VECTOR_FLOAT_SET1(T_FLOAT_MAX);	\
  for(uint m = start_pos; m < endPosPlussOne; m+= VECTOR_FLOAT_ITER_SIZE) { \
    vec_min = VECTOR_FLOAT_loadAnd_min_vec_memRef(vec_min, &arrOf_distance[m]); \
  } \
VECTOR_FLOAT_storeAnd_horizontalMin(vec_min);}) //! ie, return.


//! @return the mini-distance for an array
//! Note: we assume that ( (endPosPlussOne - start_pos) % VECTOR_FLOAT_ITER_SIZE) == 0, ie, to recue the ovrehead wrt. our appraoch.
#define  VECTOR_FLOAT_MATH_find_sumOfDistance_ofArray_alignedInPerfectChunks(arrOf_distance, start_pos, endPosPlussOne) ({ \
      VECTOR_FLOAT_TYPE vec_sum = VECTOR_FLOAT_SET1(0);			\
  for(uint m = start_pos; m < endPosPlussOne; m+= VECTOR_FLOAT_ITER_SIZE) { \
    vec_sum = VECTOR_FLOAT_loadAnd_add_vec_memRef(vec_sum, &arrOf_distance[m]); \
  } \
VECTOR_FLOAT_storeAnd_horizontalSum(vec_sum);}) //! ie, return.

//! Identify the min-value for each column-range:
//! @remarks Logics to compute a 'split' set of min-values for "arr[ ... part1 ... | ... part2 ... | .. (etc) ...]", and store the result at "arr_result[row_index ... row_index+VECTOR_FLOAT_ITER_SIZE]" (oekseth, 06. juni 2016).
//! @remarks an example-applicaiton is to comptue 'tiles' of min-values, eg, as seen in our otpimized/new algorithm for comptuation of min-valeus in hierarhcicaal clustering.
//! @remarks teh min-oepration is handled by the [”elow] "VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks" macro, ie, a call to this 'funciton-macro' results in a list-traversal for each of the 'sub-chunks'.
#if VECTOR_CONFIG_USE_FLOAT == 1 
//  printf("scalar_1=%f, at %s:%d\n", scalar_1, __FILE__, __LINE__);	
#define VECTOR_FLOAT_minDistance_eachTile(arr_result, row_index, arr_input, arr_input_pos_start_memRef, tile_size) ({ \
  const float scalar_0 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 0, *arr_input_pos_start_memRef + tile_size); \
  const float scalar_1 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 1*tile_size, *arr_input_pos_start_memRef + 2*tile_size); \
  const float scalar_2 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 2*tile_size, *arr_input_pos_start_memRef + 3*tile_size); \
  const float scalar_3 = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 3*tile_size, *arr_input_pos_start_memRef + 4*tile_size); \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_SET(scalar_0, scalar_1, scalar_2, scalar_3); \
  VECTOR_FLOAT_PRINT(vec_result); \
  VECTOR_FLOAT_STORE(&(arr_result[row_index]), vec_result); \
  *arr_input_pos_start_memRef += (uint)VECTOR_FLOAT_ITER_SIZE*tile_size; \
})						\

#elif VECTOR_CONFIG_USE_FLOAT == 0 
#define VECTOR_UNROLL_minDistance(arr_result, row_index, arr_input, arr_input_pos_start_memRef, tile_size) ({ \
  VECTOR_FLOAT_STORE(&(arr_result[row_index]), \
		     VECTOR_FLOAT_SET( \
				      VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(arr_input, *arr_input_pos_start_memRef + 0, *arr_input_pos_start_memRef + tile_size) \
				      )); \
  *arr_input_pos_start_memRef += (uint)VECTOR_FLOAT_ITER_SIZE*tile_size; \
})
#else 
#error "Won't work, ie, need to add specific support for this case. An example-case (for this erorr) is when testing differnet hardware-configurations."  
#endif 


//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics.
//! @remarks Logics: if(arr_current[bucket_id] < arr_best_value(bucket_id)) {arr_best_value[bucket_id] = score; arr_best_index[bucket_id] = row_id;}
//! @remarks in this appraoch we: Step(1): identify the masks wrt. the 'local' and the 'global' data-set; step(2) hadnle the NAN values; step(3) clear the valeus which are not of interest; step(4) combine the updated 'local' and 'global' index; step(5) return the min-values of the input-scores/distances
#define VECTOR_FLOAT_minDistance_rememberSource_vec(vec_input, vec_global, vec_input_index, vec_global_index) ({ \
__m128 local_mask = _mm_cmpgt_ps(vec_input, vec_global);  __m128 global_mask = _mm_cmplt_ps(vec_input, vec_global); \
 local_mask = VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(local_mask); \
 global_mask = VECTOR_FLOAT_convertFrom_CHAR_minExtreme_to_boolean(global_mask); \
 vec_input_index = VECTOR_FLOAT_MUL(vec_input_index, local_mask); \
 vec_global_index = VECTOR_FLOAT_MUL(vec_global_index, global_mask); \
 vec_global_index = VECTOR_FLOAT_ADD(vec_input_index, vec_global_index); \
 VECTOR_FLOAT_MIN(vec_input, vec_global); }) //! @return the min-value of vec_input and vec_global

//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics.
#define VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input(vec_input, vec_global, vec_global_index, idOf_input) ({ \
  VECTOR_FLOAT_TYPE vec_input_index = VECTOR_FLOAT_SET1(idOf_input); \
  VECTOR_FLOAT_minDistance_rememberSource_vec(vec_input, vec_global, vec_input_index, vec_global_index);})
//! Extends our "VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input(..)" to make use of memory-references:
#define VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input_data(memRef_result_score, memRef_result_index, data_local_score, data_global_score, data_global_index, idOf_input) ({ \
  const VECTOR_FLOAT_TYPE vec_input  = VECTOR_FLOAT_LOAD(data_local_score); const VECTOR_FLOAT_TYPE vec_global = VECTOR_FLOAT_LOAD(data_global_score); \
  VECTOR_FLOAT_TYPE vec_global_index = VECTOR_FLOAT_LOAD(data_global_index); \
  VECTOR_FLOAT_TYPE vec_result_min = VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_input(vec_input, vec_global, vec_global_index, idOf_input); \
  VECTOR_FLOAT_STORE(memRef_result_score, vec_result_min); VECTOR_FLOAT_STORE(memRef_result_index, vec_global_index); })
//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics.
#define VECTOR_FLOAT_minDistance_rememberSource_vec_use_idOf_range(vec_input, vec_global, vec_global_index, idOf_input) { \
  VECTOR_FLOAT_TYPE vec_input_index = VECTOR_FLOAT_setFrom_indexRange(idOf_input); \
VECTOR_FLOAT_minDistance_rememberSource_vec(vec_input, vec_global, vec_input_index, vec_global_index);})


// TODO: consider to supprot/write macros for getting the mantiussa of lfoats wrt. SSE3, eg, as described at "https://www.kvraudio.com/forum/viewtopic.php?t=314213" (oekseth, 06. june 2016)


/** ----------------- functions currenlty not fond in the mine-py-folder <-- FIXME[JC]: may you have a look upon these ... and then update? ---- **/


// FIXME: figure out how the following may be used in our operaitons: ...  __m128i packed01 = _mm_packss_epi32(dwords0, dwords1); __m12i8 packedbytes=_mm_packus_epi16(packed01, packed23); // SSE2

//! @return the max-value in a horizontal vector:
/* #define VECTOR_FLOAT_horizontal_max(vec_value) ({ \   */
/* /\*   __m128 max1 = _mm_shuffle_ps(x, _MM_SHUFFLE(0,0,3,2)); *\/ */
/* /\*   __m128 max2 = _mm_max_ps(x,max1); *\/ */
/* /\*   __m128 max3 = _mm_shuffle_ps(max2, _MM_SHUFFLE(0,0,0,1)); *\/ */
/* /\*   __m128 max4 = _mm_max_ps(max2,max3); *\/ */
/* /\* // FIXME: validate correnctess of this funciton. *\/ */
/* /\*   return _mm_cvtsi32_ss(max4); //! docu: Copy the lower 32-bit integer in a to dst. *\/ */
/* }); */

#if 0 == 1
/* definition to expand macro then apply to pragma message */
#define VALUE_TO_STRING(x) #x
#define VALUE(x) VALUE_TO_STRING(x)
#define VAR_NAME_VALUE(var) #var "="  VALUE(var)


/* Some example here */
#pragma message(VAR_NAME_VALUE(VECTOR_CONFIG_USE_FLOAT))
#pragma message(VAR_NAME_VALUE(macroConfig_use_posixMemAlign)) 
#endif



// TODO: cosnider to implement for rpef-"http://stackoverflow.com/questions/19494114/parallel-prefix-cumulative-sum-with-sse": xisum

// FIXME[JC]: may to propose/describe macros to merge/unify procedusres in opur "def_memAlloc.h" with 'those in this "def_intri.h" file, eg, wrt. "_mm_store_ps(..)" VS "_mm_storeu_ps(..)" ... ??
#include "def_sparseInt.h"
#include "def_memAlloc.h" //! which need to be included after the defniont of "t_float", ie, to aovid incosnsitencies between the memory-allcoations and the 'defualt cofniguraitons'.

//! end: file def_intri ---------------------------------------------------------
#endif
