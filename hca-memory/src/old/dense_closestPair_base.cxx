#include "dense_closestPair_base.h"

#ifdef NDEBUG
static 
#endif
//! Update the glboally tiled minbucket-tiles for each of the rows:
void  __forAll_update_globalMin_atBucketIndex(float *mapOf_minTiles_value, float *mapOf_minTiles_value_atRowIndex, float **matrixOf_locallyTiled_minPairs, const uint bucket_id, const uint row_endPosPlussOne, const bool config_useIntrisintitcs) {

  // FIXME: write correctness-tests 'for this' ... where we validate that each row 'is has a known min-distance'.

  //! Note: in order to get correct valeus for the complete set, we first need to itnalize teh vlaues:
  mapOf_minTiles_value[bucket_id] = FLT_MAX;   mapOf_minTiles_value_atRowIndex[bucket_id] = FLT_MAX;

  //! Note: in [below] we choose to start iteration seperately for each bucket, ie, to test the effect of 'this' optmizaiton.
  uint row_updateIndex = 0;
  // const uint row_endPosPlussOne_intri = macro__get_maxIntriLength_float(row_endPosPlussOne);
  // if(config_useIntrisintitcs) {
  //   for(; row_updateIndex < row_endPosPlussOne_intri; row_updateIndex += VECTOR_FLOAT_ITER_SIZE) {
  //     assert(false); // FIXME: icnldue [below] funciton wehn we have validated correcntess of our "VECTOR_FLOAT_minDistance_eachTile(..)"
  //     __intri_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs[row_updateIndex], bucket_id, /*row-id=*/row_updateIndex);
  //   }
  // } 
  { //! Then hadnle the 'rmaineder':
    for(; row_updateIndex < row_endPosPlussOne; row_updateIndex++) {
      //! Note: in [”elow] we update "mapOf_minTiles_value" and "mapOf_minTiles_value_atRowIndex" if "matrixOf_locallyTiled_minPairs[bucket_id]" has a 'better' distance.
      const uint row_id = row_updateIndex;
      const t_float min_distance = matrixOf_locallyTiled_minPairs[row_id][bucket_id]; 
      if(min_distance <= mapOf_minTiles_value[bucket_id]) {		
	mapOf_minTiles_value[bucket_id] = min_distance;		
	mapOf_minTiles_value_atRowIndex[bucket_id] = row_id;		
      }
      //__slow_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs[row_updateIndex], bucket_id, /*row-id=*/row_updateIndex);
    }
  }
}


#ifdef NDEBUG
static 
#endif
//! Identify teh shortest paths for each bucket
void __init_minPairs_eachBucket(t_float **distmatrix, const uint nrows, const uint cnt_bucketsEachRows, t_float **matrixOf_locallyTiled_minPairs, const uint bucket_size) {
    
  //! For each row find the min-valeu in each 'row-tile-chunk':
  for(uint i = 0; i < nrows; i++) {
    uint bucket_id = 0; uint column_pos = 0;
    // if(config_useIntrisintitcs) {
    // 	const uint cntBuckets_intri = macro__get_maxIntriLength_float(cnt_bucketsEachRows-1);
    // 	if(cntBuckets_intri > 0) {
    // 	  for(; bucket_id < cntBuckets_intri; bucket_id += VECTOR_FLOAT_ITER_SIZE) {
    // 	    //! Identify the min-value for each column-range:
    // 	    VECTOR_FLOAT_minDistance_eachTile(matrixOf_locallyTiled_minPairs[i], bucket_id, distmatrix[i], &column_pos, bucket_size);
    // 	  }
    // 	}
    // }
    if(true) { //! then we make use of an non-SSE-version:
      for(; bucket_id < cnt_bucketsEachRows; bucket_id++) {	
	const uint endPosPlussOne = column_pos + bucket_size;
	uint m = column_pos;
	t_float min_value = distmatrix[i][m]; m++; //! ie, an 'absoltue max' value	
	for(; m < endPosPlussOne; m++) { 
	  if(distmatrix[i][m] < min_value) {min_value = distmatrix[i][m];}
	}
	matrixOf_locallyTiled_minPairs[i][bucket_id] = min_value; column_pos += bucket_size;
	if(false) {printf("[%u][bucket=%u] \t min=%f, at %s:%d\n", i, bucket_id, min_value, __FILE__, __LINE__);}
      }
    } else {
      assert((bucket_size % VECTOR_FLOAT_ITER_SIZE) == 0); // ie, as [”elow] will otherwise resutl in an error.
      for(; bucket_id < cnt_bucketsEachRows-1; bucket_id++) {	
	matrixOf_locallyTiled_minPairs[i][bucket_id] = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(distmatrix[i], column_pos + 0, column_pos + bucket_size); column_pos += bucket_size;
      }
      //! Then the last bucket:
      assert(bucket_id == (cnt_bucketsEachRows-1));
      assert((column_pos + bucket_size) == cnt_bucketsEachRows); //! ie, otherwise write a new funciton to handle the 'un-covered tail'.
      assert((bucket_size % VECTOR_FLOAT_ITER_SIZE) == 0); // ie, as [”elow] will otherwise resutl in an error.
      matrixOf_locallyTiled_minPairs[i][bucket_id] = VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(distmatrix[i], column_pos + 0, column_pos + bucket_size);
    }
  }
}


#ifdef NDEBUG
static 
#endif
//! Identify the min-values for the 'global' column:
void __init_minColumns_eachBucket(const uint nrows, const uint cnt_bucketsEachRows, t_float *mapOf_minTiles_value, t_float *mapOf_minTiles_value_atRowIndex, t_float **matrixOf_locallyTiled_minPairs) {

  //! Traverse through each of the rows:
  for(uint i = 0; i < nrows; i++) {
    uint bucket_id = 0; 
    // if(config_useIntrisintitcs) {
    // 	const uint cntBuckets_intri = macro__get_maxIntriLength_float(cnt_bucketsEachRows-1);
    // 	if(cntBuckets_intri > 0) {
    // 	  for(; bucket_id < cntBuckets_intri; bucket_id += VECTOR_FLOAT_ITER_SIZE) {	   
    // 	    //! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics:
    // 	    __intri_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs[i], bucket_id, /*row-id=*/i);
    // 	  }
    // 	}
    // }
    //! Then a non-intrisnictic code:
    for(; bucket_id < cnt_bucketsEachRows; bucket_id++) {	
      const uint row_id = i;
      const t_float min_distance = matrixOf_locallyTiled_minPairs[row_id][bucket_id]; 
      // FIXME[time] .... consider to compare the exeuciton-time of "<=" and "<" ... and if we 'udpate [”elow] '' then aslo update our correctness-tests at/in our "assert_dense_closestPair.cxx"
      if(min_distance <= mapOf_minTiles_value[bucket_id]) {		
	mapOf_minTiles_value[bucket_id] = min_distance;		
	mapOf_minTiles_value_atRowIndex[bucket_id] = row_id;		
      }
      if(false) {printf("min[bucket=%u] \t min=%f, row_id=%u, at %s:%d\n", bucket_id, mapOf_minTiles_value[bucket_id], (uint)mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
      //__slow_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs[i], bucket_id, /*row-id=*/i);
    }
  }
}

