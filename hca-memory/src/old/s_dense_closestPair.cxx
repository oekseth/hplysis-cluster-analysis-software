#include "s_dense_closestPair.h"

#include "dense_closestPair_base.h"




/**
   @brief A function to intiate the s_dense_closestPair object.
**/
void s_dense_closestPair_init(s_dense_closestPair_t *self, const uint nrows, const uint ncols, const uint cnt_bucketsEachRows, t_float **distmatrix, const bool config_useIntrisintitcs, const bool isTo_use_fast_minTileOptimization) {
  //! What we expect:
  assert(self); assert(nrows); assert(cnt_bucketsEachRows);  assert(distmatrix);

  //! Intialise the containers.
  self->nrows = nrows; self->ncols = ncols; self->cnt_bucketsEachRows = cnt_bucketsEachRows; self->distmatrix = distmatrix; self->config_useIntrisintitcs = config_useIntrisintitcs; self->isTo_use_fast_minTileOptimization = isTo_use_fast_minTileOptimization;
  //#ifndef NDEBUG
  self->_nrows_threshold = nrows;
  self->_ncols_threshold = ncols;
  //#endif
  //! Allcoate memory:
  // FIXME: try to identify the 'otpmial' number of buckets for each row.
  const t_float default_value_float = /*default-value=*/0;
  self->matrixOf_locallyTiled_minPairs = allocate_2d_list_float(nrows, cnt_bucketsEachRows, default_value_float);
  self->_arrOf_tmp_scalarRowValues = allocate_1d_list_float(nrows, default_value_float);
  assert(self->_arrOf_tmp_scalarRowValues != NULL);

  self->mapOf_minTiles_value = NULL; self->mapOf_minTiles_value_atRowIndex = NULL;
  if(self->isTo_use_fast_minTileOptimization) { //! then we allcoate memory an intlaize:
    self->mapOf_minTiles_value =allocate_1d_list_float(cnt_bucketsEachRows, default_value_float); //! ie, as we are interested in the min-value for every row wrt. a given tile-bucket.
    self->mapOf_minTiles_value_atRowIndex = allocate_1d_list_float(cnt_bucketsEachRows, default_value_float); //! ie, as we are interested in the min-value for every row wrt. a given tile-bucket.
    //! Intalise the arrays using a max-distance-value:
    // FIXME: write a macro wrt. [below] which sets teh valeus ... and compare the performance-difference.
    for(uint i = 0; i < cnt_bucketsEachRows; i++) {self->mapOf_minTiles_value[i] = T_FLOAT_MAX; self->mapOf_minTiles_value_atRowIndex[i] = T_FLOAT_MAX;}
  }

  
  { //! Identify teh shortest paths for each bucket
    self->_bucket_size = self->ncols / self->cnt_bucketsEachRows;
    //const uint bucket_size = self->_bucket_size;

    if(true) {printf("Given column-range=[%u, %u-1] and bucket_size=%u, we evalaute buckets in range=[%u, %u-1], at %s:%d\n", 0, ncols, self->_bucket_size, 0, self->_bucket_size-1, __FILE__, __LINE__);}
    
    if( config_useIntrisintitcs && (self->_bucket_size % VECTOR_FLOAT_ITER_SIZE) != 0) { //! ie, to simplify our code, ie, reuce the algorithmic overhead.
      fprintf(stderr, "!!\t The input was not specified as expected: the bucket-size=%u = %u/%u was not 'divisable' by '%u', ie, a (minor) slwodown may incure: if the latter is acceptable, then please request a code-update of the (below assicated \"VECTOR_FLOAT_MATH_find_minDistance_ofArray_alignedInPerfectChunks(..)\" function) code, ie, for the latter please cotneact the develoepra t [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", self->_bucket_size, self->ncols, self->cnt_bucketsEachRows, (uint)VECTOR_FLOAT_ITER_SIZE, __FUNCTION__, __FILE__, __LINE__);
    }

    //! Identify teh shortest paths for each bucket
    __init_minPairs_eachBucket(distmatrix, nrows, self->cnt_bucketsEachRows,    self->matrixOf_locallyTiled_minPairs, self->_bucket_size);
  }

  if(self->isTo_use_fast_minTileOptimization) { 
    //! Then identify the min-values for the 'global' column:
    __init_minColumns_eachBucket(nrows, self->cnt_bucketsEachRows, self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs);
  }

  //! What we expect:
  assert(self); assert(self->nrows); assert(self->cnt_bucketsEachRows); assert(self->matrixOf_locallyTiled_minPairs); assert(self->distmatrix);
}

//! De-allcoate the locally reserved memory.
void s_dense_closestPair_free(s_dense_closestPair_t *self) {
  free_2d_list_float(&self->matrixOf_locallyTiled_minPairs);
  if(self->matrixOf_locallyTiled_minPairs) {
    free_1d_list_float(&self->mapOf_minTiles_value);
    free_1d_list_float(&self->mapOf_minTiles_value_atRowIndex);
  }
  assert(self->_arrOf_tmp_scalarRowValues); 
  // printf("Free-poitner, at %s:%d\n", __FILE__, __LINE__);
  free(self->_arrOf_tmp_scalarRowValues); 
  //  free(&(self->_arrOf_tmp_scalarRowValues)); 
  // printf("ok: Free-poitner, at %s:%d\n", __FILE__, __LINE__);
  //assert(self->_arrOf_tmp_scalarRowValues == NULL); //! ie, wtha we expect
  self->_arrOf_tmp_scalarRowValues = NULL;
}

//! Update the shortest-paht-positions wrt. an updated row
void s_dense_closestPair_updateAt_row(s_dense_closestPair_t *self, const uint row_id, const uint column_startPos, const uint column_endPosPlussOne, const uint row_endPosPlussOne) {
   //! What we expect (generic):
  assert(self); assert(self->nrows); assert(self->cnt_bucketsEachRows); assert(self->matrixOf_locallyTiled_minPairs); assert(self->distmatrix);
  //! Specific:
  assert(row_id < self->nrows); assert(column_startPos <= column_endPosPlussOne); assert(row_endPosPlussOne <= self->nrows); assert(column_endPosPlussOne <= self->ncols);
  assert(self->_bucket_size > 0); assert(self->_bucket_size != UINT_MAX);

  //! Identify the bucket-range:
  uint bucket_id = column_startPos / self->_bucket_size; const uint bucket_end = column_endPosPlussOne / self->_bucket_size;  
  if(false) {printf("Given column-range=[%u, %u-1] and bucket_size=%u, we evalaute buckets in range=[%u, %u], at %s:%d\n", column_startPos, column_endPosPlussOne, self->_bucket_size, bucket_id, bucket_end, __FILE__, __LINE__);}


  uint column_pos = bucket_id * self->_bucket_size; //! ie, the start-pos of the current row-chunk-id.
  assert(column_pos <= column_startPos); //! ie, as we otehriwse might have a bug.


  if(self->config_useIntrisintitcs) {
    assert(false); // FIXME: validate correcntess of this funciton ... ie, 'move' this funciton into sub-parts which may esily be verfied wrt. assert-tests.
    assert(false); // FIXME: write differnet systnetic perofrmance-tests wrt. this funciton ... eg, to dineitfy 'trheashodls' wrt. SSE-optmalizaitons.
    const uint cntBuckets_intri_size = macro__get_maxIntriLength_float((bucket_end - bucket_id) - 1);
    if(cntBuckets_intri_size > 0) {
      const uint cntBuckets_intri = bucket_id + cntBuckets_intri_size;

      assert(false); // FIXME: valdiate correctness of the vec_ref_mask values <-- try to set different values, and ivnesttigate the result.

      //! Identify the min-tiles seperately for each "row[ ... chunk-tile-0 ... chunk-tile-1 ... chunk-tile-(etc) ... ]".
      for(; bucket_id < cntBuckets_intri; bucket_id += VECTOR_FLOAT_ITER_SIZE) {
	//! Identify the min-value for each column-range:
	VECTOR_FLOAT_minDistance_eachTile(self->matrixOf_locallyTiled_minPairs[row_id], bucket_id, self->distmatrix[row_id], &column_pos, self->_bucket_size);


	if(self->isTo_use_fast_minTileOptimization) { //! Identify the min-values for the 'global' column:

	  assert(false); // FIXME: move this out into a seperate function, adn then validate correctness 'of this'.

	  //! Update the min-tile-values:
	  VECTOR_FLOAT_TYPE vec_needToIterate;
	  
	  const uint bucket_id_local_end = bucket_id + VECTOR_FLOAT_ITER_SIZE;
	  for(uint bucket_id_local = bucket_id; bucket_id_local < bucket_id_local_end; bucket_id_local++) {
	    const uint cnt_needTo_iterateThroughAllRows =  __slow_update_globalMin_scoreAndIndex_forBucket(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs[row_id], bucket_id_local, /*row-id=*/row_id);
	    if(cnt_needTo_iterateThroughAllRows > 0) { //! Then 'handle' the last bucket, suign non-itrnsitnictic iteration:
	      // FIXME: write a performnce-time-test wrt. the "_mm_castps_si128(..)" when compared to using "(uint)" casts ... ie, as an example of the 'explict optmizaitons' which may be performned .. and similar wr.t the use of "0xfff0" VS FLT_MAX
	      
	      //! Then we update all of the vlaues:
	      for(uint row_updateIndex = 0; row_updateIndex < row_endPosPlussOne; row_updateIndex++) {

		assert(false); // FIXME: make a modfied call wr.t [”elow]

		// __intri_update_globalMin_scoreAndIndex_forBucket_xmtIndexSelfTest(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs[row_updateIndex], bucket_id, /*row-id=*/row_updateIndex);
	      }
	    }
	  }
	}

	// FIXME: remove [”elow] when we know that [”elow] conditon always hold:
	assert(column_pos <= column_endPosPlussOne);
      }
    }    
    assert(false); // FIXME: validate taht all of the [ªbove] 'non-itnricticntict idnex-cases' ahs been covered/ahndled.
  }
  {

    // FIXME: why does [”elow] use "bucket_end + 1" ?? <-- udpated, though need to validdate correctness 'of not doing this'.
    for(; bucket_id < bucket_end; bucket_id++) {
      //! Idnetify the start- and stop-conditions:
      //! Note: the column-pos is iteratilvy updated in the for-loop, ie, the column-pos is always up-to-date.
      uint i_end = column_pos + self->_bucket_size;
      if(i_end > column_endPosPlussOne) {i_end = column_endPosPlussOne;} //! ie, to avodi iteration 'past' the threshold.
      const uint old_minValue = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id];
      
      //! Reset the vlaue, through remember the old/previous value:
      self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = self->distmatrix[row_id][column_pos]; column_pos++;
      //! Then find the min-values in the 'row-tile-chunk':
      for(; column_pos < i_end; column_pos++) {
	const t_float currentValue = self->distmatrix[row_id][column_pos];
	if(self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] > currentValue) {
	  self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = currentValue; //! ie, as 'currentValue' is then 'closer'.
	}
      }
      if(true) {printf("[%u][bucket=%u] \t min=%f, at %s:%d\n", row_id, bucket_id, /*min_value=*/self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], __FILE__, __LINE__);}

      if( old_minValue != self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]) { //! then the min-value has changed:
	//! Then we udpate the min-row-index for each column-tile:  


	if(self->isTo_use_fast_minTileOptimization) { //! Identify the min-values for the 'global' column:
	  //! Update the global min wrt. score and index-mappings for a column-tile usign intrisinistics:
	  //! Note: in [”elow] we compare 'global optmium' VS 'local optmium': "matrixOf_locallyTiled_minPairs[bucket_id]" VS "mapOf_minTiles_value[bucket_id]". In this comparsin an 'optmizaiton' (which we makes use of) is that if 'thr row with improved global score' si the same as 'the earlier row witht he improved global score' then we do no perofmr [”elow] 'call wrt. glboal idnetificiaotn of min-values':
	  const bool needTo_iterateThroughAllRows = __slow_update_globalMin_scoreAndIndex_forBucket(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs[row_id], bucket_id, /*row-id=*/row_id);
	  if(needTo_iterateThroughAllRows) { //! Then 'handle' the last bucket, suign non-itrnsitnictic iteration:
	    //! Then we need to iterate through all of the 'tiled rows', ie, for the bucket in question:

	    //! Note: we do Not test/evaluate an optmizaiton-option wrt. the 'time-optimum-effect' of 'storing the bucket-ids-to-update in a dense list'. The latter due to (a) the 'already in-place order of the bucket_ids', (b) overhead wrt. memory-accesses and (c) the 'arelady approximate order' of the memory-accesses wrt. [”elow] 'option'.

	    // FIXME: add a new temproary "arrOf_rowsTo_update" table, ie, to avoid 'muliple calls from updating the same min-value wrt. the "isTo_use_fast_minTileOptimization" case ... for which we may reduce the time-complexity (of the row-udpate-funcion-call) by a factor of "|rows|*|chunks|".
	    
	    //! Update the glboally tiled mincolumn-tile for each of the rows:
	    //! Note: in [below] we iterate compare each 'local best-fit': "mapOf_minTiles_value[bucket_id]" VS "matrixOf_locallyTiled_minPairs[...][bucket_id]":
	    __forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, row_endPosPlussOne, self->config_useIntrisintitcs);
	    if(true) {printf("min[bucket=%u] \t min=%f, row_id=%u, at %s:%d\n", bucket_id, self->mapOf_minTiles_value[bucket_id], (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
	  }
	}
      }
    }
  }
  
  //! What we expect:
  if(false) {printf("column_pos=%u, column_endPosPlussOne=%u, at %s:%d\n", column_pos, column_endPosPlussOne, __FILE__, __LINE__);}
  assert(column_pos == column_endPosPlussOne); //! ie, as we otherwise might have a bug.     
}


/**
   @brief Update the shortest-paht-positions wrt. an updated column
   @remarks procedure:
   -- each row-tile: Update the min-column-tile for each row: we expect one signualr valeus at each row to be updated: if a 'row-assicated tile' 'is at the best row-tile-index and has changed', then we need to update all values 'for the given tile'.
   -- the global tile: iterate through the best-performing tiles and update.

**/
void s_dense_closestPair_updateAt_column(s_dense_closestPair_t *self, const uint column_pos, const uint row_startPos, const uint row_endPosPlussOne, const uint column_endPosPlussOne, const t_float *arrOf_previouslyUsedValues) {
  //! What we expect (generic):
  assert(self); assert(self->nrows); assert(self->cnt_bucketsEachRows); assert(self->matrixOf_locallyTiled_minPairs); assert(self->distmatrix);
  //! Specific:
  assert(column_pos < self->ncols); assert(row_startPos <= row_endPosPlussOne); assert(row_startPos < self->nrows); assert(column_endPosPlussOne <= self->ncols);
  assert(arrOf_previouslyUsedValues); // assert(matrixWith_newValues_fixedColumnPos > 0); assert(matrixWith_newValues_fixedColumnPos < self->ncols);
  
  //! Identify the bucket-range:
  uint bucket_id = column_pos / self->_bucket_size; 
  const uint column_pos_startOf_block = bucket_id * self->_bucket_size; //! ie, to 'adjust' the the beginng of the tile.
  uint row_id = row_startPos;

  uint cnt_minTiles_updated_inConsistent = 0;


  //! Update the min-column-tile for each row:
  if(self->config_useIntrisintitcs) {
    const uint row_endPosPlussOne_intri = macro__get_maxIntriLength_float(row_endPosPlussOne);
    if(row_endPosPlussOne_intri > 0) {
      for(; row_id < row_endPosPlussOne_intri; row_id += VECTOR_FLOAT_ITER_SIZE) {
	
	assert(false); // FIXME: add something <-- cosndierfirst writing this when we have validated for the 'ealrier writtien' itnristnicts wr.t if-clauses.     
	assert(false); // FIXME: remember to udpate the "cnt_minTiles_updated_inConsistent" attribute 
      }
      
      assert(false); // FIXME: validate taht all of the [ªbove] 'non-itnricticntict idnex-cases' ahs been covered/ahndled.
    }
  } 
  

  { //! Then a non-intrisinistic block:
    for(; row_id < row_endPosPlussOne; row_id++) {
      const t_float old_minValue = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]; 
      const t_float old_value = arrOf_previouslyUsedValues[row_id]; //[matrixWith_newValues_fixedColumnPos];
      const t_float currentValue = self->distmatrix[row_id][column_pos];

      //! Compare the old/previous min-value to the 'value before the distance-matrix was udpated': 
      if(old_minValue == old_value) { //! then we need to update the min-valeus for the 'tile':
	if(currentValue <= old_minValue) { //! then the case is simple, ie, we do not need to idnevistate/idneitfy any specific/new valeus:
	  //! If a column-tile has has an improved/shroter distance, then consider to update 'local-min(row)(column-tile)' and 'global-min(column-tile)':
	  self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = currentValue;
	  if(self->mapOf_minTiles_value) {
	    if(self->mapOf_minTiles_value[bucket_id] > currentValue) {
	      self->mapOf_minTiles_value[bucket_id] = currentValue;
	      self->mapOf_minTiles_value_atRowIndex[bucket_id] = row_id;
	    }
	  }
	  if(false) {printf("[%u][bucket=%u] \t min=%f, at %s:%d\n", row_id, bucket_id, /*min_value=*/self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], __FILE__, __LINE__);}
	} else { //! then we need to traverse through the min-tile-set, ie, as it they may be a change in the min-values:	  
	  //! if the previous min-value is no longer valid/descritpive, then identify a new min-value (for the given row-column-tile), ie, "( min-value(row) == old-value(row)(column) ) < new-value(row)(column) < old-value(row)(other-columns)";
	  //! Note(1): this funciton si 'investgiated' for the case of: "( min-value(row) == old-value(row)(column) ) < new-value(row)(column) < old-value(row)(other-columns)"
	  //! Note(2): this if-case-block examplifes an exeuciton-time-performance consideration/challenge, ie, which conserns the case where the updated value is 'along' the min-positon:
	  //! Then iterate 'through the remainder':
	  uint column_pos_tmp = column_pos_startOf_block; //! ie, teh first/start index in a given column-tile:
	  const uint i_end = column_pos_startOf_block + self->_bucket_size;
	  uint debug_cnt_minValueHasCahnged = 0;
	  for(; column_pos_tmp < i_end; column_pos_tmp++) {
	    const t_float currentValue = self->distmatrix[row_id][column_pos_tmp];
	    if(self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] > currentValue) {
	      self->matrixOf_locallyTiled_minPairs[row_id][bucket_id] = currentValue;
	      debug_cnt_minValueHasCahnged++;
	    }
	  }
	  if(false) {printf("[%u][bucket=%u] \t min=%f, at %s:%d\n", row_id, bucket_id, /*min_value=*/self->matrixOf_locallyTiled_minPairs[row_id][bucket_id], __FILE__, __LINE__);}
	  assert(debug_cnt_minValueHasCahnged > 0); //! ie, as the for-loop-iteratioopn otehrwise is pointless.

	  //! Then 'make it clear' in the post-row-processing that we need to update the global-min wrt. the tiles
	  //! Note: in [”elow] there is no poin in first investigating the "self->mapOf_minTiles_value_atRowIndex[bucket_id]" value, ie, as we do not know the 'second-best' valeu.
	  cnt_minTiles_updated_inConsistent++; //! which is used as a a 'boolean mark'.
	}
      }
      // //! Update the value:
      // self->distmatrix[row_id][column_pos] = currentValue;
    }
  }

  if(cnt_minTiles_updated_inConsistent > 0) { //! Then we evaluate/investgiate wrt. the min-values for all of the rows, ie, a possible time-compelxity-increase of |nrows|.
    //! Iterate through the best-performing tiles and update.
    //! Note: update the glboally tiled mincolumn-tile for each of the rows:
    __forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, row_endPosPlussOne, self->config_useIntrisintitcs);
    if(false) {printf("min[bucket=%u] \t min=%f, row_id=%u, at %s:%d\n", bucket_id, self->mapOf_minTiles_value[bucket_id], (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
  } //! otehrwise we asusem that none of the valeus 'represent' a min-tile-case:
}


//! @return the closest pair, ie, update scalar_index1, scalar_index2
t_float s_dense_closestPair_getClosestPair(s_dense_closestPair_t *self, uint *scalar_index1, uint *scalar_index2, const uint nrows_threshold, const uint ncols_threshold) {
  //! What we expect:
  assert(self); assert(self->nrows); assert(self->cnt_bucketsEachRows); assert(self->matrixOf_locallyTiled_minPairs); assert(self->distmatrix);
  //! Specific:
  assert(nrows_threshold <= self->nrows); assert(ncols_threshold <= self->ncols);
  //! Set the 'return indexes' to empty:
  *scalar_index1 = UINT_MAX; *scalar_index2 = UINT_MAX;
  

  //! 
  //! 'Handle' the case where the next matrix-boundary 'is inside an exsiting boundary': iterate through both through the boundayr-columns and boundary-row:
  //! Note: in [”elow] provide/apply logics to find the 'next-best min-value', ie, to (a) update the last column-tile for each row, (b) identify the min-value for each column-tile (ie, for the complete set of rows).
  if(nrows_threshold != self->_nrows_threshold) { //! then 'remove' max-scores assicated to the last row
    if(self->isTo_use_fast_minTileOptimization) { //! Identify the min-values for the 'global' column:
      assert(self->_nrows_threshold <= self->nrows);
      assert(nrows_threshold < self->_nrows_threshold); //! ie, our gneeral expectaiotns wrt. use.
      assert(nrows_threshold != 0);
      const uint row_id = self->_nrows_threshold - 1; //! ie, the presumed last row-index.
      assert(row_id == nrows_threshold); //! whihch fi not hodl impleis that we need to use a for-loop in [”elow]
      
      //! Investigate if teh Iterate through the row and udpate:
      uint bucket_id = ncols_threshold / self->_bucket_size; const uint bucket_end = ncols_threshold / self->_bucket_size;  
      for(; bucket_id < bucket_end; bucket_id++) {
	//! Then we need to idnify a new 'min-value' for the column-column-tile, ie, first investgiate the if the row 'is the best-fit row-index':
	if(row_id == (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id]) { //! then we need to dinetify a new min-value for the column-tile:
	  __forAll_update_globalMin_atBucketIndex(self->mapOf_minTiles_value, self->mapOf_minTiles_value_atRowIndex, self->matrixOf_locallyTiled_minPairs, bucket_id, nrows_threshold, self->config_useIntrisintitcs);
	  if(true) {printf("min[bucket=%u] \t min=%f, row_id=%u, at %s:%d\n", bucket_id, self->mapOf_minTiles_value[bucket_id], (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id], __FILE__, __LINE__);}
	}          
      }
    } else {
      assert(self->mapOf_minTiles_value == NULL); //! ie, whta we expect.
    }
    //! Update ehte object 'for the next run':
    self->_nrows_threshold = nrows_threshold;
  }
  if(ncols_threshold != self->_ncols_threshold) { //! then 'remove' max-scores assicated to the last column:
    assert(ncols_threshold < self->_ncols_threshold); //! ie, our gneeral expectaiotns wrt. use.
    assert(self->_ncols_threshold <= self->ncols);
    assert(ncols_threshold != 0);
    assert(self->_ncols_threshold == (1 + ncols_threshold)); //! ie, otherwise update [”elow] "prev_bucket_id".
    const uint column_pos = ncols_threshold;
    assert(column_pos < self->ncols); //! ie, givne [ªbove] set of epxtctiaotns/assertions.

    //! 
    //! Iterate through the last column of each row and udpate:
    const uint bucket_id = (ncols_threshold -1 ) / self->_bucket_size;  //! ie, as we update the last column
    assert(bucket_id != self->_bucket_size);
    const uint prev_bucket_id = ncols_threshold / self->_bucket_size;  //! ie, as we update the last column
    
    if(bucket_id == prev_bucket_id) { //! then there might be errnous indexes in the buckets:
      assert(self->_arrOf_tmp_scalarRowValues);
      //! Frist 'collect' the old values:
      for(uint row_id = 0; row_id < self->_nrows_threshold; row_id++) {
	self->_arrOf_tmp_scalarRowValues[row_id] = self->distmatrix[row_id][column_pos];
      }
      //! Then update the column-tiels based on [ªbove]:
      //! Note: in [”elow] we iterate through each bucket and test if a bucket 'has row-index less than the givne contraint'
      s_dense_closestPair_updateAt_column(self, column_pos, /*row_startPos=*/0, /*row_endPosPlussOne=*/nrows_threshold, /*column_endPosPlussOne=*/ncols_threshold, /*arrOf_previouslyUsedValues=*/self->_arrOf_tmp_scalarRowValues);
    } //! else we assume the 'old' buckets will enver be accessed, ie, no need to update them.

    //! Update ehte object 'for the next run':
    self->_ncols_threshold = ncols_threshold;
  }



  t_float retVal_minValue = T_FLOAT_MAX; t_float bucket_id_foundAt = T_FLOAT_MAX;

  const uint bucket_size = (ncols_threshold ) / self->_bucket_size;  //! ie, as we update the last column
  assert(bucket_size <= self->cnt_bucketsEachRows); //! ie, as otherwise we migth have a bug.

 
  if(self->isTo_use_fast_minTileOptimization) { //! Identify the min-values for the 'global' column:
    //! Iterate through the buckets:
    uint bucket_id = 0; 
    //! Then we use a 'naivistic' appraoch:      
    for(; bucket_id < bucket_size; bucket_id++) {
      assert(self->mapOf_minTiles_value_atRowIndex[bucket_id] < ncols_threshold); //! ie, givne our above update-procedure/calls.
      if(retVal_minValue > self->mapOf_minTiles_value[bucket_id]) {
	retVal_minValue = self->mapOf_minTiles_value[bucket_id];
	//! Then update the index-refrences:
	*scalar_index1 = (uint)self->mapOf_minTiles_value_atRowIndex[bucket_id]; 
	bucket_id_foundAt = bucket_id;
      }
    }
    assert(retVal_minValue != T_FLOAT_MAX); assert(bucket_id_foundAt != T_FLOAT_MAX); //! ie, as we otehrwise might have a bug.          
  } else { //! then we need to evlauate "|nrows|*|column-tile|":
    //const uint row_endPosPlussOne_intri = macro__get_maxIntriLength_float(nrows_threshold);
    //const uint cntBuckets_intri = (self->config_useIntrisintitcs) ? macro__get_maxIntriLength_float(self->cnt_bucketsEachRows) : 0;
    //! Iterate through the buckets:    
    for(uint row_id = 0; row_id < nrows_threshold; row_id++) {
      uint bucket_id = 0; 
      for(; bucket_id < bucket_size; bucket_id++) {
	if(retVal_minValue < self->matrixOf_locallyTiled_minPairs[row_id][bucket_id]) {
	  retVal_minValue = self->matrixOf_locallyTiled_minPairs[row_id][bucket_id];
	  //! Then update the index-refrences:
	  *scalar_index1 = row_id;    bucket_id_foundAt = bucket_id;	    
	}
      }
    }     
  } 
  
  //!
  //! Our last step (in finding the min-values): identify the 'outer index':
  assert(*scalar_index1 != UINT_MAX); assert(retVal_minValue != T_FLOAT_MAX); assert(bucket_id_foundAt != T_FLOAT_MAX); //! ie, as we otehrwise might have a bug.      
  {//! Find the 'column-pos' with the value in question, ie, iterate through one signle tile assicated to one defined row:
    //! Note: below call is the 'key-point' in our optmizaiton, ie, to not store the column-pos in our index-update.
    uint column_pos = bucket_id_foundAt * self->_bucket_size;
    const uint end_pos = column_pos + self->_bucket_size; 
    //    const uint end_pos = (end_pos <= ncols_threshold) ? column_pos + self->_bucket_size : ncols_threshold;
    for(; column_pos < end_pos; column_pos++) {
      if(self->distmatrix[*scalar_index1][column_pos] == retVal_minValue) {
	// FIXME: test the perofrmance of 'instead strotign the min-valeu for "*scalar_index2" in a seperate table ... which we update when updating "matrixOf_locallyTiled_minPairs"
	*scalar_index2 = column_pos;
      }
    }
  }
  //! we expect to have found both indexes:
  assert(*scalar_index1 != UINT_MAX);   assert(*scalar_index2 != UINT_MAX);
  if(true) {printf("#\t (min-value)\5 %f\t at (%u, %u), at %s:%d\n", retVal_minValue, *scalar_index1, *scalar_index2, __FILE__, __LINE__);}

  //! @return the shortest distance:
  return retVal_minValue;
}





