#ifndef def_typeOf_int_h
#define def_typeOf_int_h

/**
   @file def_typeOf_int.h
   @brief includes defintions for "int" and "short int" (oekseth, 06. june 2016)
 **/



//!
//! Handle different options wrt. INT:
#define VECTOR_INT_ITER_SIZE 4
#define VECTOR_INT_ITER_SIZE_short 8
// FIXME[jc]: may you suggest a better type?
#define VECTOR_INT_TYPE __m128i
#define VECTOR_INT_TYPE_short __m128i
//! --- data-access
#define VECTOR_INT_SET _mm_set_epi32
#define VECTOR_INT_SET_short _mm_set_epi16
#define VECTOR_INT_SET1 _mm_set1_epi32
#define VECTOR_INT_SET1_short _mm_set1_epi16
// FIXME[jc]: .... wrt. writing a tutroial ... observe taht I'm having difficulty to 'automatically conceive' the difference between "load" and "set" ... ie, cosnider to higlight this 'cocneptual/prectiacal differenc'.
// FIXME[jc]: validate correctness of [below]
#define VECTOR_INT_STORE(mem_ref, vector) _mm_store_si128((__m128i*)mem_ref, vector)
#define VECTOR_INT_STORE_short(mem_ref, vector) _mm_store_si128((__m128i*)mem_ref, vector)
// FIXME[jc]: below seems wrogn ... ie, how to we load an 8*4=32 bit int into a SIMD vector?
#define VECTOR_INT_LOAD(mem_ref) (_mm_load_si128((__m128i const*)mem_ref))
#define VECTOR_INT_LOAD_alt1(mem_ref) (VECTOR_INT_SET(*(mem_ref+0), *(mem_ref+1), *(mem_ref+2), *(mem_ref+3)))
#define VECTOR_INT_LOADU(mem_ref) (VECTOR_INT_SET(*(mem_ref+0), *(mem_ref+1), *(mem_ref+2), *(mem_ref+3)))
#define VECTOR_INT_LOAD_short VECTOR_INT_LOAD
#define VECTOR_INT_LOADU_short VECTOR_INT_LOADU
//! --- simple aritmetics
#define VECTOR_INT_ADD _mm_add_epi32
#define VECTOR_INT_SUB _mm_sub_epi32
#define VECTOR_INT_ADD_short _mm_add_epi16
#define VECTOR_INT_SUB_short _mm_sub_epi16

// FIXME[jc]: are there better 'æappraoches' than 'to first covnert to float'?
#define VECTOR_INT_MUL(vec1, vec2) ({ \
  VECTOR_FLOAT_TYPE vec_1_int = VECTOR_FLOAT_convertFrom_INT(vec1); \
  VECTOR_FLOAT_TYPE vec_2_int = VECTOR_FLOAT_convertFrom_INT(vec2); \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_MUL(vec_1_int, vec_2_int); \
  VECTOR_FLOAT_converTo_int_32(vec_result);})
// FIXME[jc]: are there better 'æappraoches' than 'to first covnert to float'?
#if VECTOR_FLOAT_ITER_SIZE == 4
//!--------------------------------------------------------
#define VECTOR_INT_DIV(vec1, vec2) ({ \
  VECTOR_FLOAT_TYPE vec_1_int = VECTOR_FLOAT_convertFrom_epi32(vec1); \
  VECTOR_FLOAT_TYPE vec_2_int = VECTOR_FLOAT_convertFrom_epi32(vec2); \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_DIV(vec_1_int, vec_2_int); \
  VECTOR_FLOAT_converTo_int_32(vec_result);}
/* #define VECTOR_INT_DIV_notInverseResult(vec1, vec2) ({\ */
/*       VECTOR_INT_TYPE vec_result = VECTOR_INT_DIV(vec1, vec2); \ */
/*       vec_result = VECTOR_INT_revertOrder(vec_result);	       \ */
/*       vec_result;}) */
//!--------------------------------------------------------
#else //! then we need to iteratethrough each of the chunks:
//!--------------------------------------------------------
#define VECTOR_INT_DIV_notInverseResult(vec1, vec2) ({ \
  /*! Get access to the 'bare-bone' data: */ \
  const uint cnt_iterations = VECTOR_INT_ITER_SIZE / VECTOR_FLOAT_ITER_SIZE; \
  union UNION_VECTOR_INT_TYPE d_input_1; d_input_1.sse = vec1;  union UNION_VECTOR_INT_TYPE d_input_2; d_input_2.sse = vec2; 	\
  int buffer[VECTOR_INT_ITER_SIZE]; \
  for(uint iter = 0; iter < cnt_iterations; iter++) {\
  /* printf("iter=%u, start-index=%u, at %s:%d\n", iter, iter*VECTOR_FLOAT_ITER_SIZE, __FILE__, __LINE__); */ \
/*! Load the data into float-vectors: */				\
    VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_convertFrom_data_notInv(d_input_1.buffer, /*index=*/iter * VECTOR_FLOAT_ITER_SIZE); \
    VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_convertFrom_data_notInv(d_input_2.buffer, /*index=*/iter * VECTOR_FLOAT_ITER_SIZE); \
  /*! Compute the div: */ \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_DIV(vec_1, vec_2); \
  /*! Store the result: */ \
  VECTOR_FLOAT_convertTo_dataPoiner(buffer, /*index=*/iter*VECTOR_FLOAT_ITER_SIZE, vec_result, /*typeOf_result=*/int); \
  } \
  /*! Convert the buffer into a vector, and then return: */					\
  VECTOR_INT_SET(buffer[3], buffer[2], buffer[1], buffer[0];})
  //VECTOR_INT_LOADU(buffer); })
#define VECTOR_INT_DIV(vec1, vec2) (VECTOR_INT_revertOrder(VECTOR_INT_DIV_short_notInverseResult(vec1, vec2)))
//!--------------------------------------------------------
#endif

#define VECTOR_INT_MUL_short _mm_mullo_epi16
// FIXME[jc]: are there better 'æappraoches' than 'to first covnert to float'?

#define VECTOR_INT_DIV_short_notInverseResult(vec1, vec2) ({ \
  /*! Get access to the 'bare-bone' data: */ \
  const uint cnt_iterations = VECTOR_INT_ITER_SIZE_short / VECTOR_FLOAT_ITER_SIZE; \
  union UNION_VECTOR_INT_TYPE_short d_input_1; d_input_1.sse = vec1;  union UNION_VECTOR_INT_TYPE_short d_input_2; d_input_2.sse = vec2; 	\
  short int buffer[VECTOR_INT_ITER_SIZE_short]; \
  for(uint iter = 0; iter < cnt_iterations; iter++) {\
  /* printf("iter=%u, start-index=%u, at %s:%d\n", iter, iter*VECTOR_FLOAT_ITER_SIZE, __FILE__, __LINE__); */ \
/*! Load the data into float-vectors: */				\
    VECTOR_FLOAT_TYPE vec_1 = VECTOR_FLOAT_convertFrom_data_notInv(d_input_1.buffer, /*index=*/iter * VECTOR_FLOAT_ITER_SIZE); \
    VECTOR_FLOAT_TYPE vec_2 = VECTOR_FLOAT_convertFrom_data_notInv(d_input_2.buffer, /*index=*/iter * VECTOR_FLOAT_ITER_SIZE); \
  /*! Compute the div: */ \
  VECTOR_FLOAT_TYPE vec_result = VECTOR_FLOAT_DIV(vec_1, vec_2); \
  /* VECTOR_FLOAT_PRINT(vec_result); */ \
  /*! Store the result: */ \
  VECTOR_FLOAT_convertTo_dataPoiner(buffer, /*index=*/iter*VECTOR_FLOAT_ITER_SIZE, vec_result, /*typeOf_result=*/short int); \
  } \
  /*! Convert the buffer into a vector, and then return: */					\
  VECTOR_INT_SET_short(buffer[7], buffer[6], buffer[5], buffer[4], buffer[3], buffer[2], buffer[1], buffer[0]); /*VECTOR_INT_LOADU_short(buffer); */})
  //  VECTOR_INT_SET_short(buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7]); /*VECTOR_INT_LOADU_short(buffer); */})

#define VECTOR_INT_DIV_short(vec1, vec2) ({				\
  VECTOR_INT_TYPE_short vec_result = VECTOR_INT_DIV_short_notInverseResult(vec1, vec2); \
  vec_result = VECTOR_INT_revertOrder_short(vec_result); \
  vec_result;})





//! --- comparisons
// TODO: cosnider to add 'specifc' support for max-functiosn in SSE3 ... ie, fit eh copmtuer-hardware 'of the complied computer' suupports 'this'.
#define VECTOR_INT_MAX(vec_1, vec_2) ({ union UNION_VECTOR_INT_TYPE d_cmp; d_cmp.sse = vec_1; union UNION_VECTOR_INT_TYPE d_input; d_input.sse = vec_2; int buffer_result[VECTOR_INT_ITER_SIZE]; \
  for(uint i = 0; i < VECTOR_INT_ITER_SIZE; i++) {			\
    if(d_cmp.buffer[i] < d_input.buffer[i]) {				\
      buffer_result[i] = d_input.buffer[i];				\
      /*printf("updates at index=%u, given %u < %u at %s:%d\n", i, d_cmp.buffer[i], d_input.buffer[i], __FILE__, __LINE__);*/ \
    } else {buffer_result[i] = d_cmp.buffer[i];}			\
  }									\
  VECTOR_INT_TYPE vec_result = VECTOR_INT_SET(buffer_result[0], buffer_result[1], buffer_result[2], buffer_result[3]);  \
  vec_result; })


#define VECTOR_INT_MAX_short _mm_max_epi16
#define VECTOR_INT_MIN_short _mm_min_epi16

/* #define VECTOR_INT_loadAnd_min_vec_memRef(vec_min, mem_ref) (_mm_min_(vec_min, _mm_loadu_ps(mem_ref) ) ) */

//! Artimetic operaitons combined with memory-load for: "int"
#define VECTOR_INT_loadAnd_add_vec_memRef(vec_sum, mem_ref) (VECTOR_INT_ADD(vec_sum, VECTOR_INT_LOAD(mem_ref) ) )
#define VECTOR_INT_loadAnd_mult_vec_memRef(vec_sum, mem_ref) (VECTOR_INT_MUL(vec_sum, VECTOR_INT_LOAD(mem_ref) ) )

//! Aritmetic operaitons combined with memory-load for: "short int"
#define VECTOR_INT_loadAnd_add_vec_memRef_short(vec_sum, mem_ref) (VECTOR_INT_ADD_short(vec_sum, VECTOR_INT_LOAD_short(mem_ref) ) )
#define VECTOR_INT_loadAnd_mult_vec_memRef_short(vec_sum, mem_ref) (VECTOR_FLOAT_MUL_short(vec_sum, VECTOR_INT_LOAD_short(mem_ref) ) )



#define VECTOR_INT_PRINT(vec_result) ({int result[VECTOR_INT_ITER_SIZE] = {0, 0, 0, 0}; VECTOR_INT_STORE(result, vec_result); printf("["); for(uint i = 0; i < 4; i++) {printf("%d, ", result[i]);} fprintf(stdout, "], at %s:%d\n", __FILE__, __LINE__); })
#define VECTOR_INT_PRINT_short(vec_result) ({short int result[VECTOR_INT_ITER_SIZE_short] = {0}; VECTOR_INT_STORE_short(result, vec_result); printf("["); for(uint i = 0; i < VECTOR_INT_ITER_SIZE_short; i++) {printf("%d, ", (int)result[i]);} fprintf(stdout, "], at %s:%d\n", __FILE__, __LINE__); })



//! @return the sum of values, wrt. "int".
#define VECTOR_INT_storeAnd_horizontalSum(vec_add1) ({int result[VECTOR_INT_ITER_SIZE] = {0, 0, 0, 0}; VECTOR_INT_STORE(result, vec_add1); \
  int scalar_result = 0; for(uint m = 0; m < VECTOR_INT_ITER_SIZE; m++) { {scalar_result += result[m];} } \
  scalar_result; }) //! ie, return the result.
//! @return the sum of values, wrt. "short int".
#define VECTOR_INT_storeAnd_horizontalSum_short(vec_add1) ({short int result[VECTOR_INT_ITER_SIZE_short]; VECTOR_INT_STORE_short(result, vec_add1); \
  short int scalar_result = 0; for(uint m = 0; m < VECTOR_INT_ITER_SIZE_short; m++) { {scalar_result += result[m];} } \
  scalar_result; }) //! ie, return the result.

//! @return the sum of values, wrt. "int".
#define VECTOR_INT_storeAnd_horizontalMax(vec_add1) ({int result[VECTOR_INT_ITER_SIZE] = {0, 0, 0, 0}; VECTOR_INT_STORE(result, vec_add1); \
      int scalar_result = INT_MIN; for(uint m = 0; m < VECTOR_INT_ITER_SIZE; m++) { if(scalar_result < result[m]) {scalar_result += result[m];} } \
  scalar_result; }) //! ie, return the result.
//! @return the sum of values, wrt. "short int".
#define VECTOR_INT_storeAnd_horizontalMax_short(vec_add1) ({short int result[VECTOR_INT_ITER_SIZE_short]; VECTOR_INT_STORE_short(result, vec_add1); \
      short int scalar_result = SHRT_MIN; for(uint m = 0; m < VECTOR_INT_ITER_SIZE_short; m++) { if(scalar_result < result[m]) {scalar_result += result[m];} } \
  scalar_result; }) //! ie, return the result.


//! Revertes the roder of a vector.
#define VECTOR_INT_revertOrder(vec) ({_mm_shuffle_epi32(vec, _MM_SHUFFLE(0, 1, 2, 3));})
//#define VECTOR_INT_revertOrder_short(vec) (_mm_shufflelo_epi16(vec,  _MM_SHUFFLE(0, 1, 2, 3, 4, 5, 6, 7)))
// FIXME[jc]: ... is there any itnernal function which may go faster ... eg, to use 'left-shift' and 'right-shift'? ... eg, to combine 'shuffle' with "mm_unpacklo_epi16" ... ??
#define VECTOR_INT_revertOrder_short(vec_result) ({\
    vec_result = _mm_shufflelo_epi16(vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \
    vec_result = _mm_shufflehi_epi16(vec_result, _MM_SHUFFLE(0, 1, 2, 3)); \
    /*! Copy the results: */ \
    union UNION_VECTOR_INT_TYPE_short d_cpy; d_cpy.sse = vec_result; \
    /*! 'Swap' the two 'partiont's:*/ \
    VECTOR_INT_SET_short(d_cpy.buffer[4+0],d_cpy.buffer[4+1],d_cpy.buffer[4+2],d_cpy.buffer[4+3], \
			 d_cpy.buffer[0+0],d_cpy.buffer[0+1],d_cpy.buffer[0+2],d_cpy.buffer[0+3]);})

union UNION_VECTOR_INT_TYPE { VECTOR_INT_TYPE sse; int buffer[VECTOR_INT_ITER_SIZE]; } ;
union UNION_VECTOR_INT_TYPE_short { VECTOR_INT_TYPE_short sse; short int buffer[VECTOR_INT_ITER_SIZE_short]; } ;

//! ********************************************************************** End Of File (EOF)
#endif //! EOF
