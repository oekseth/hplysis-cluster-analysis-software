#ifndef s_dataStruct_matrix_dense_h
#define s_dataStruct_matrix_dense_h
/**
   @file s_dataStruct_matrix_dense
   @brief provide data-structure and logics for accessing elements in a dense matrix.
   @author Ole Kristian Ekseth (oekseth)
 **/

#include "configure_optimization.h" //! Whcih specifies the optmizaitons and macro-configurations
#include "def_intri.h"


/**
   @file parse_main.h
   @struct s_dataStruct_matrix_dense
   @brief provide lgocis for parsing files
   @author Ole Kristian Ekseth (oekseth)
 **/
typedef struct s_dataStruct_matrix_dense {
  char **nameOf_rows; char **nameOf_columns;
  t_float **matrixOf_data;
  uint cnt_columns; uint cnt_rows;
} s_dataStruct_matrix_dense_t;


/**
   @enum s_dataStruct_matrix_dense_typeOf_sampleFunction
   @brief identify the type of pre-defined data-sets to use
 **/
typedef enum s_dataStruct_matrix_dense_typeOf_sampleFunction {
  s_dataStruct_matrix_dense_typeOf_sampleFunction_flat,
  //s_dataStruct_matrix_dense_typeOf_sampleFunction_linear,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_equal,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_b,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_linear_differentCoeff_a,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus,
  //s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_undef
} s_dataStruct_matrix_dense_typeOf_sampleFunction_t;


/**
   @enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise
   @brief extends enum "s_dataStruct_matrix_dense_typeOf_sampleFunction" wrt. functions 'holding' noise-values.
 **/
typedef enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise {
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_different_ax,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_curved,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_and_axx,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_ax_inverse_x,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_circle,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_sinusoid_curved,

  //s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_undef
} s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t;

/**
   @enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel
   @brief identifies the nosie-level in the "s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise" enum
 **/
typedef enum s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel {
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_low,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_medium,
  //s_dataStruct_matrix_dense_typeOf_sampleFunction_sinus_and_linear,
  s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_undef
} s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t;




//! Intiate (ie, set to empty) the "s_dataStruct_matrix_dense_t" object wrt. the memory-refrences (and not the object-reference itself).
void init_s_dataStruct_matrix_dense_t_setToEmpty(s_dataStruct_matrix_dense_t *obj);

//! @return true if "obj" is set to empty, eg, through a call to our "init_s_dataStruct_matrix_dense_t_setToEmpty(obj)".
bool is_setTo_empty(const s_dataStruct_matrix_dense_t *obj);
/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
   @param <word_max_length> which if set iodentifies the max-word-length in the data: if word_max_length is greater than zero we allocate the "nameOf_columns" and "nameOf_rows" object-attributes.
 **/
void init_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns, const uint word_max_length); 
/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object, and intiate the column-headers and row-headers to 'sample-texts'.
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
 **/
void init_s_dataStruct_matrix_dense_t_isTo_constructSampleHeaders_rows_columns(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns);
//! De-allocat the "s_dataStruct_matrix_dense_t" object wrt. the memory-refrences (and not the object-reference itself).
void free_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t *obj);



/**
   @brief intiate the the "s_dataStruct_matrix_dense_t" object
   @param <obj> is the object ot initate.
   @param <cnt_rows> is the number of rows in the data-set.
   @param <cnt_columns> is the number of columns in the data-set.
   @param <word_max_length> which if set iodentifies the max-word-length in the data: if word_max_length is greater than zero we allocate the "nameOf_columns" and "nameOf_rows" object-attributes.
 **/
//s_dataStruct_matrix_dense_t *
void init_listOf_s_dataStruct_matrix_dense_t(s_dataStruct_matrix_dense_t **arrOf_1d, const uint list_size, const uint cnt_rows, const uint cnt_columns, const uint word_max_length); 
//! De-allocates a list of "s_dataStruct_matrix_dense_t" objects.
void free_listOf_s_dataStruct_matrix_dense_t(const uint list_size, s_dataStruct_matrix_dense_t *arrOf_1d);


//! Build a sample-data-set using random values.
void constructSample_data_random(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns);
//! Build a sample-data-set using uniform values, valeus which are gneerated using the algorithm implemneted in the "cluster.c" library.
void constructSample_data_uniform(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns);
/**
   @brief build a sample-data-set using bionomial values,
   // @param <config_cnt_trials> is the max-value to be used: is the number of trials to be used in comptuation/'drawing' of a number from the bionomial distribution.
   @param <config_probability> the likelyhood/probality of a single event: expected to be in range [0, 0.5].
   @remakrs the values are generated using the algorithm implemneted in the "cluster.c" library.
**/
void constructSample_data_bionmial(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns, const t_float config_probability);
//void constructSample_data_bionmial(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns, const uint config_cnt_trials, const t_float config_probability);
//! Build a sample-data-set using different functions 'to construct the data'.
void constructSample_data_by_function(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns, const s_dataStruct_matrix_dense_typeOf_sampleFunction_t typeOf_function);
//! Build a sample-data-set using different functions 'to construct the data'.
void constructSample_data_by_function_noise(s_dataStruct_matrix_dense_t *obj, const uint cnt_rows, const uint cnt_columns, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noise_t typeOf_function, const s_dataStruct_matrix_dense_typeOf_sampleFunction_w_noiseLevel_t typeOf_noise);
/* /\** */
/*    @brief write out the obj to the stream as a dense matrix. */
/*    @param <obj> is the data-set which contains the set of scores */
/*    @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows. */
/*    @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns. */
/*    @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream */
/*    @param <stream> is the poitner to the restul-stream. */
/*    @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix. */
/*    @param <stringOf_measureId> which is used to 'name' the Java-script matrix-varaible (whcih is 'written out'). */
/*  **\/ */
/* void export_dataSet_s_dataStruct_matrix_dense_matrix_tab(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const bool include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders); */
/**
   @brief write out the obj to the stream as a dense matrix.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream: used to 'name' the Java-script matrix-varaible (whcih is 'written out').
   @param <stream> is the poitner to the restul-stream.
   @param <include_stringIdentifers_in_matrix> which if set implies that the string-identifers are included in the matrix.
   @param <isTo_writeOut_stringHeaders> which if set to true implies that we write out the string-ehaders.
   @param <stringOf_measureId> 
 **/
void export_dataSet_s_dataStruct_matrix_dense_matrix_tab_javaScript(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, FILE *stream, const char include_stringIdentifers_in_matrix, const char isTo_writeOut_stringHeaders) ;
/**
   @brief write out the obj to the stream as set of relations.
   @param <obj> is the data-set which contains the set of scores
   @param <nameOf_rows> if set, then we use this mapping-table is expected to hold the string-mappigns for the rows.
   @param <nameOf_columns> if set, then we use this mapping-table is expected to hold the string-mappigns for the columns.
   @param <stringOf_header> if set, then we 'introduce' the generated matrix with the the stringOf_header identifer, eg, to be used to seperate muliple output-matrices in the same stream
   @param <stringOf_predicate> which if set is used as predicate when writing out the data, eg, to simplify interpreation of the relationships.
   @param <stream> is the poitner to the restul-stream.
   @param <isTo_useJavaScript_syntax> which if set to true implies that we makes use of a java-script syntax.
 **/
void export_dataSet_s_dataStruct_matrix_dense_relations(const s_dataStruct_matrix_dense_t *obj, char **nameOf_rows, char **nameOf_columns, const char *stringOf_header, const char *stringOf_predicate, FILE *stream, const bool isTo_useJavaScript_syntax);


#endif //! EOF
