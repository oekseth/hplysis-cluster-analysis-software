#ifndef s_dense_closestPair_h
#define s_dense_closestPair_h

#include "def_intri.h"
#include "libs.h"




/**
   @struct s_dense_closestPair_t
   @brief optimizes the identifciation of shortest-pair distance for distance-matrices which are iterativly updated (oekseth, 06. juni 2016).

   @remarks In hierarchical clustering-algorithms a major lag/bottleneck is the identification of shortest paths/pairs (SP): for the cluster-algorithms of "psl", "pml" and "pal" the SP is computed for every iteration. To our surprise none of the popular hierarchical-cluster-algorithms provide efficient algorithms for computation of SP: the latter explains why  hierarchical clustering-algorithms (HCAs) provide/have a poor performance (ie, a cubic time-complexity).

   What we have observed, is that there is a sequence of matrix-updates which provides the potential/opportunity to reduce the time-complexity of HCAs from cubic time-complexity to quadratic time-complexity. 

 In this work we demonstrates an algorithm which translates/reduce hierarchical-clustering-algorithms with a cubic time-complexity into quadratic-time-complexity (ie, from "n*n*n" into "n*n", where "n" describes/represents the number of elements in a matrix). In brief, our new algorithm provides/presents a fast approach for the identification of SPs for distance-matrices which are iteratively updated. 
   
   Given the large number of applications wrt. our new algorithm, we in this work will outline both the algorithm, execution-times for different data-sets and different HCAs, and provide details of different optimization-strategies wrt. our ANSI C implementation. From the latter we demonstrate/identify how our approach enables domain-experts to identify novel features in their: through an increase in performance and ease in application/use complex features may be used to derive clusters of related entities.


   # Introduction
   In clustering of large data-sets we have observed/identified a need to reduce the execution-time of hierarchical-clustering-algorithms. An example-application is the identification of ...??... 

   

   # Implementation

   Idea is to store the 'min-value' for each index in a new "arrOf minValueAtRow" and "arrOf minValueAtRow columnIndex", arrays which are then updated every time the distance-matrix changes. Before introducing the algorithm, we in below identify/list key-observations/properties wrt. the HCAs:
   step(a) each iteration: a naive SP with time-complexity "n*n" is called for the distance-matrix;
   step(b) each iteration: approx. two rows and two columns are updated (in the distance-matrix) for every iteration;
   step(c) each iteration: the distance-matrix-size is reduced by a factor of '1' (for each iteration);
   From the latter list/points/steps we observe that a 'memory' wrt. the updated rows and shortest-paths provides a potential/opportunity to reduce the SP time-complexity: if we for each row-update remember adjusts/remembers the 'global' shortest-path-distance, there is no need to visits/investigate the complete set of elements (in the distance matrix for every HCA-iteration). 


   ... In our "s_dense_closestPair_init(..)" funciton the logic we apply illustrate the 'base framework' wrt. our approach:
   implementation(a) initiation of min-buckets: separate each row into a set of buckets: when a sub-part of a row is updated, we only need to update the buckets;
   implementation(b) initiation of min-columns-buckets: find the min-score for each 'bucket-row-tile-column': when a bucket is updated, we only need to iterate (ie, update) the 'chunk-columns' in order to have/identify the min-distance-score in the complete matrix;
   implementation(c) update row: ...??..
   implementation(d) update column: ...??...
   implementation(e) identify min-pair: ....??...

   .... Our implementation is based on eh following set of properties/observations wrt. HCAs .... 

   ... 
   
   # Results:
   ... The structure (and associated functions) are called (eg, from the HCA) every time the distance-matrix changes. From a theoretical time-approximation we expects the execution-time to decrease by a factor of "n" for the functions where the SP computation/call/funcion is the largest time-consumer. In order to empirically validate our approach we use performance-test wrt. the data-distributions described at "http://www.knittingtools.org/gui_lib_mine_cmp.cgi". 
   
   # Summary:
   
   ... From the time-measurements of ...??.... we observe that our new algorithm (and the associated implementation) results in significant performance-improvement for hierarchical clustering-algorithms. In this work we have demonstrated how our new algorithm (and associated implementation) boosts the performance of: "psl", "pml" and "pal" cluster-algorithms. In brief, our algorithm provides logic for identifying the shortest paths/pairs in dense matrices: in contrast to a naive/'default' implementation with "n*n" time-complexity (where "n" describes/represents the number of elements in a matrix), our novel algorithm/implementation reduces the time-complexity to "n".
   ... Given the large number of 'low-level' back-compatible software we implement our code in ANSI C language-compatible 'style'. Our new algorithm (and library) may therefore be included/incorporated into a large number of existing software-implementations. A use-case wrt. the latter is seen in our \citet{lib_cCluster}, where our new SP-algorithm for HCAs significantly boosts the performance (of the HCA computation).



   @author Ole Kristian Ekseth (oekseth).
**/
typedef struct s_dense_closestPair {
  // FIXME: in [below] may we instead 'store' shortest-path-distance to muliple rows 'in one tile'?
  uint nrows; uint ncols; //! where each vertex is assicated to one row: we store speerately for each row iot. (a) simplify the logics wrt. our intristnictict optmizaiton, and (b) as the update-sqeuence (Wrt. shrotest-paths) are 'seperately done' for both rows and columns (ie, where one 'complete row' is udpated insead of only single cells wrt. the 'muliple rows updated for one column case').
  //#ifndef NDEBUG
  uint _nrows_threshold; uint _ncols_threshold; //! whcih are used to 'infer' if we are to udpate teh 'boundaries' wrt. columns
  t_float *_arrOf_tmp_scalarRowValues; //! whcih is used to update teh 'bounardy-columns' 
  //#endif
  uint cnt_bucketsEachRows; //! whihc is used to optimize the 
  uint _bucket_size; //! which is used itneranlyy in the object for cosnistency wrt. cnt_bucketsEachRows;
  t_float **matrixOf_locallyTiled_minPairs; //! which stores wrt. [row-id][bucket-tile]
  t_float **distmatrix;
  //! Configure-variables to test/evlauate differnet strategies to icnrease the perofrmance.
  bool config_useIntrisintitcs;

  //! -------------------------
  //! Optimization: remember min-values of each 'column-bucket':
  // FIXME: cosnider to add support for [below] .... ie, to avoid iterating through every row to find the min-value.
  // FIXME: test the perfomrance-effects of the "isTo_use_fast_minTileOptimization" option.
  bool isTo_use_fast_minTileOptimization;
  //! Note: the "lfoat" data-type is used for botht he index and the row to simplify the intricitnict-logics wr. the iteration.
  t_float *mapOf_minTiles_value; t_float *mapOf_minTiles_value_atRowIndex; //! where we store for [tile] = {min-value, row-id}. 
} s_dense_closestPair_t;


//! A function to intiate the s_dense_closestPair object.
void s_dense_closestPair_init(s_dense_closestPair_t *self, const uint nrows, const uint ncols, const uint cnt_bucketsEachRows, t_float **distmatrix, const bool config_useIntrisintitcs, const bool isTo_use_fast_minTileOptimization);
//! De-allcoate the locally reserved memory.
void s_dense_closestPair_free(s_dense_closestPair_t *self);

//! Update the shortest-paht-positions wrt. an updated row
void s_dense_closestPair_updateAt_row(s_dense_closestPair_t *self, const uint row_id, const uint column_startPos, const uint column_endPosPlussOne, const uint row_endPosPlussOne); 
/**
   @brief Update the shortest-paht-positions wrt. an updated column
   @remarks procedure:
   -- each row-tile: Update the min-bucket-tiles for each row: we expect one signualr valeus at each row to be updated: if a 'row-assicated tile' 'is at the best row-tile-index and has changed', then we need to update all values 'for the given tile'.
   -- the global tile: iterate through the best-performing tiles and update.

**/
void s_dense_closestPair_updateAt_column(s_dense_closestPair_t *self, const uint column_pos, const uint row_startPos, const uint row_endPosPlussOne, const uint column_endPosPlussOne, const t_float *arrOf_previouslyUsedValues);

//! @return the closest pair, ie, update scalar_index1, scalar_index2
t_float s_dense_closestPair_getClosestPair(s_dense_closestPair_t *self, uint *scalar_index1, uint *scalar_index2, const uint nrows_threshold, const uint ncols_threshold);

//! Correctness-tests and performance-tests for our "s_dense_closestPair" ANSI C class (for idneitfying shortest paths).
void assert_s_dense_closestPair();
#endif
