#ifndef dense_closestPair_base_h
#define dense_closestPair_base_h

#include "def_intri.h"
#include "libs.h"


//! Update the global min wrt. score and index-mappings for a bucket-tile usign intrisinistics:
#define __slow_update_globalMin_scoreAndIndex_forBucket(mapOf_minTiles_value, mapOf_minTiles_value_atRowIndex, matrixOf_locallyTiled_minPairs, bucket_id, row_id) ( { \
  const t_float min_distance = matrixOf_locallyTiled_minPairs[bucket_id]; \
  bool retVal_needToIterateThroughAllTiles = false; \
  if(min_distance <= mapOf_minTiles_value[bucket_id]) { \
    mapOf_minTiles_value[bucket_id] = min_distance; \
    mapOf_minTiles_value_atRowIndex[bucket_id] = row_id; \
  } else { \
  if(mapOf_minTiles_value_atRowIndex[bucket_id] == row_id) \
    retVal_needToIterateThroughAllTiles = true;		   \
  } \
  retVal_needToIterateThroughAllTiles; }) //! ie, return the result


#ifndef NDEBUG
void  __forAll_update_globalMin_atBucketIndex(float *mapOf_minTiles_value, float *mapOf_minTiles_value_atRowIndex, float **matrixOf_locallyTiled_minPairs, const uint bucket_id, const uint row_endPosPlussOne, const bool config_useIntrisintitcs);
//! Identify teh shortest paths for each bucket
void __init_minPairs_eachBucket(t_float **distmatrix, const uint nrows, const uint cnt_bucketsEachRows, t_float **matrixOf_locallyTiled_minPairs, const uint bucket_size);
//! Identify the min-values for the 'global' column:
void __init_minColumns_eachBucket(const uint nrows, const uint cnt_bucketsEachRows, t_float *mapOf_minTiles_value, t_float *mapOf_minTiles_value_atRowIndex, t_float **matrixOf_locallyTiled_minPairs);
#endif




#endif //! EOF
