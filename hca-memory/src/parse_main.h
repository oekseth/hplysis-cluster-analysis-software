#ifndef cha_cluster__config__distanceFunctionIsDefined //! then we assume the distance-funciton is defined (oekseth, 06. okt. 2016)
#ifndef parse_main_h
#define parse_main_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hca-mem-data-anlysis. If not, see <http://www.gnu.org/licenses/>.
 */


#include "configure_hcaMem.h" //! Whcih specifies the optmizaitons and macro-configurations
#include "def_intri.h"
#include "s_dataStruct_matrix_dense.h"

#ifdef __cplusplus
extern "C"{
#endif 


/**
   @brief test existence of file.
   @param <path_to_file> is the location of the file in question.
   @param <verify_file_is_regular> in order to investigate if the file has the protection which we expect.
   @param <sizeOf_file> is set to the size of the identified file: may be set though a false value is returned.
   @return true if the file exist.
**/
bool file_exists(const char *path_to_file, const bool verify_file_is_regular, uint *sizeOf_file);

#ifndef NDEBUG //! then we include a set of funcitons, ie, to simplify correcness-testing.
//! Insert a value into a cell, a calue which is either a flaot-number or a string.
void __update_cell(s_dataStruct_matrix_dense_t *obj, const uint word_max_length, const uint row_id, const uint cnt_tabs, const char *word_buffer, const uint pos_in_column_0_prev);
//! Get proeprties assiated to a givne file:
void __get_facts_forFile(FILE *file, const char column_seperator, uint *cnt_rows, uint *cnt_columns, uint *word_max_length);
#endif

//! Load a stream into an "s_dataStruct_matrix_dense_t" object.
//! @return 1 if the operation was a success.
bool parse_matrix_fromStream_tab_usingInitatedObject(s_dataStruct_matrix_dense_t *obj, FILE *file, const uint word_max_length);

//! Load a stream into an "s_dataStruct_matrix_dense_t" object.
//! @return 1 if the operation was a success.
bool parse_matrix_fromStream_tab(s_dataStruct_matrix_dense_t *obj, FILE *file);

//! Load the contents of a file into an "s_dataStruct_matrix_dense_t" object:
//! @return 1 if the operation was a success.
bool parse_matrix_fromFile_tab(s_dataStruct_matrix_dense_t *obj, const char *file_name) ;

/**
   @brief load a data-set into the data_objh, either using input-data or a value-range-distribution defined by our "stringOf_sampleData_type" param.
   @param <data_obj> the object to hold the new-cosntructed data-set: if set to a string-value, the other function-arpameters are ginored.
   @param <input_file> which if set ideitnfied the input-file to use
   @param <cnt_rows> the number of rows to be cosnturcted/inferred.
   @param <cnt_columns> the number of features/columns to be cosnturcted/inferred.
   @param <readData_fromStream> which if set implies that the data-'foudn' in "stream" is used to buidl the data-matrix: has prioerty over the stringOf_sampleData_type funciton-parameter.
   @param <stringOf_sampleData_type> identified the type of funciton (eg, 'random' or 'uniform') for which we are to infer value-ranges.
   @return true on success.
 **/
bool build_dataset_fromInputOrSample(s_dataStruct_matrix_dense_t *data_obj, const char *input_file, const uint cnt_rows, const uint cnt_columns, const bool readData_fromStream, const char *stringOf_sampleData_type);
/* //! A function to validate correctness of the lgocis in this c-file. */
/* void parse_main_assert(); */

#ifdef __cplusplus
}
#endif


#endif //! EOF
#endif //! endif("cha_cluster__config__distanceFunctionIsDefined")
