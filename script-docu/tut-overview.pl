use strict;
use Carp;
use warnings;


my @arr = (
# --------------------
# FIXME: replatce: "X" with "synethtic: disjoint clusters"
# FIXME: fields: optioanl: ... { features => ''    }
# .....................
    { group => 'Execution-Time', brief => 'time-cost of strategies for entropy-calculation', tut =>'tut__altLAng_entropy.c', data => 'syntetic', metrics => 'time(entropy-metrics)'},
{ group => 'Execution-Time and establish correlation:', brief => 'the cost of arithmetics in entropy-calculation', tut =>'tut_entropy_x_log.c', data => 'synthetic', features => '100,000,000 -- 2,000,000,000', metrics => 'artimetric operations: generate binomial numbers, generate random numbers, compute log, compute approxciatme log (for numbers, and addiional cdost of randiszed nnumbers), '},  #! comapres the cost of differne taritmehtiod operations.
{ group => 'Execution-Time and controllable learning', brief => 'compare the cost, and accuracy, when calculating the metrics of Shannon, MillerMadow, Jeffreys, ChaoShen, Shrink', tut =>'tut_entropy_x_time.c', data => 'synthetic', features => 100, metrics => 'time + accuracy: Shannon, MillerMadow, Jeffreys, ChaoShen, Shrink'}, #! ie, compare texecuion-time, and accurayc, of entropy-metrics
#
    #
{ group => 'Pareto Boundary', brief => 'effects of dimensinality on Euclidean K-means', tut =>'tut__aux__dataFiles__differentDimensions.c', data => 'Synthetic feature-curves', metrics => 'Euclidean, K-Means, dimensinality of data'}, #! Note: used in: { group => '', brief => '', tut =>'tut__aux__dataFiles__differentDimensions_wrapper.c', data => '', metrics => ''},
{ group => undef, brief => '', tut =>'tut__aux__dataFiles__FCPS.c', data => 'artifical shapes', metrics => 'Euclidean, K-Means, dimensinality of data'},
{ group => undef, brief => '', tut =>'tut__aux__dataFiles__mathFunctions__noise.c', data => 'Synthetic feature-curves w/noise', metrics => 'tut__aux__dataFiles__mathFunctions__noise.c'},
{ group => undef, brief => '', tut =>'tut__aux__dataFiles__realKT.c', data => '64 real-life data from the UCI repository', metrics => '*'}, #! ie, not explictly defined in the script. 
{ group => undef, brief => '', tut =>'tut__aux__dataFiles__realMine.c', data => '11 real-life data from UCI and measurements of guinea pigs \\citep{wright1936genetics}', metrics => 'Pearson::squared, k-means'},
{ group => undef, brief => '', tut =>'tut__aux__dataFiles__realMine_large.c', data => '25 real-life data from UCI and measurements of guinea pigs \\citep{wright1936genetics}', metrics => 'Pearson::squared, k-means'},
{ group => undef, brief => '', tut =>'tut__aux__dataFiles__syn.c', data => '6 synthetic data: permutations of feature-curves', metrics => '*'},
{ group => undef, brief => '', tut =>'tut__aux__dataFiles__Vilje.c', data => 'Communicaiton latency in supercomputer=Vilje at NTNU', metrics => '*'},
{ group => 'Pareto Boundary', brief => 'sensitivity of similarity-metrics when using different types of randomization-generators', tut =>'tut-bias-1-histogram-sensitvity-variance-in-randomness.c', data => 'random:(uniform, modulo, multinomial)', features => '200--4000', metrics => 'similarity=(Chebychev, Euclidean, Cityblock, Pearson::squared, Kendall::Kumar-Hassebrock, MINE)'},

{ group => 'Pareto Boundary', brief => 'how clustering-algorithms handles combinations of data from 17+ different data-distributions', tut =>'tut-bias-2-artiFicial-exportExtremeTestCases.c', data => 'artificial distributions=(student-t, bernoulli, binomial, cauchy, Chi-squared, discrete, exponential, extreme-value, Fisher-f, Gamma, Geometric, Log-normal, Negiatve binomial, Normal, Pieceiwise constant, Poission, Uniform (integer), Unform (real-values), Weibull)', metrics => 'normalization=(none, average::relative, relative::STD::abs, relative::STD, rank:relative); algorithms=(K-Means w/parameters=(fixed, dynamic), K-Medoid w/parameters=(fixed, dynamic), K-Rank w/parameters=(fixed, dynamic), mini-batch, hp-cluster w/parameters=(fixed, dynamic), DBSCAN w/parameters=(fixed, dynamic), MST w/parameters=(fixed, dynamic), HCA w/linkage=(single, max, average, centroid))'}, #! distribtiison deinfed in: "kt_distributionTypesC.h"
{ group => 'Pareto Boundary', brief => 'effects of dimensinality on 20+ clustering-algorithms and 30+ CCMs', tut =>'tut-bias-3-histogram-sensitvity-variance-in-randomness-multipleDistributions.c', data => 'artificial distributions=(student-t, bernoulli, binomial, cauchy, Chi-squared, discrete, exponential, extreme-value, Fisher-f, Gamma, Geometric, Log-normal, Negiatve binomial, Normal, Pieceiwise constant, Poission, Uniform (integer), Unform (real-values), Weibull)', metrics => 'similarity=(Chebychev, Euclidean, Cityblock, Pearson::squared, Spearman, Kendall::Kumar-Hassebrock), normalization=(none, average::relative, relative::STD::abs, relative::STD, rank:relative); algorithms=(K-Means w/parameters=(fixed, dynamic), K-Medoid w/parameters=(fixed, dynamic), K-Rank w/parameters=(fixed, dynamic), mini-batch, hp-cluster w/parameters=(fixed, dynamic), DBSCAN w/parameters=(fixed, dynamic), MST w/parameters=(fixed, dynamic), HCA w/linkage=(single, max, average, centroid))'},
{ group => undef, brief => 'Application: apply 300+ similarity-metrics to the same data, and visually compare the results (through exported results)', tut =>'tut_buildSimilarityMatrix.c', data => '', metrics => ''}, #! "examplifies how to build a file holding the simliarity-evlauation-metircs of all the simlarity-metrics supported by hpLysis  (oekseth, 06. jan. 2017).   ... input: applicaiton of muliple metrics: how to loop through different simliarity-metrics in order to evlauate large number of different agreemenet-evlauation-strategies....  -- result: how to merge the complete set of resutl-sets into one file: among others examplfie the 'opening' of a speicifc reuslt-file, an 'open d fiel' which is sued as 'result-out-channel' to a alrge number of dfiferent corr-emtrics-results."
{ group => 'Ability of generalization', brief => 'apply 300+ similarity-metrics to the same sparse data, and visually compare the results (through exported results)', tut =>'tut_buildSimilarityMatrix__testSparse.c', data => 'syntetic, hard-coded', features => '4', metrics => 'simliarity-metrics: 300+'},
{ group => 'Pareto Boundary', brief => 'sensitvity of CCM=F-measure to permutations of syntetic data', tut =>'tut_ccm_10_goldVSgold_fMeasure.c', data => 'synethtic: disjoint clusters, same number of clusters in each partition', features => '100', metrics => 'CCM=f-measure'},
{ group => 'Pareto Boundary', brief => 'sensitvity of CCM=SSE to permutations of syntetic data', tut =>'tut_ccm_14_matrixVSgold_SSE.c', data => 'synethtic: disjoint clusters', features => 100, metrics => 'CCM: 30+'},
{ group => 'Pareto Boundary', brief => 'sensitvity of 30+ CCMs to permutations of syntetic data', tut =>'tut_ccm_11_compareMatrixBased.c', data => 'synethtic: disjoint clusters, increasing number of clusters in each partition', features => '1000', metrics => 'CCM: 30+'},
{ group => 'Pareto Boundary', brief => 'prediction-difference between permutations of K-means', tut =>'tut_ccm_12_nestedApplication__centroid_simMetric_ccm.c', data => 'synthetic function',  features => '100', metrics => 'k-Means w/center=(average, rank, medoid), similarity-metric=MINE'}, #! where data is generted based on: "obj_1.matrix[row_id][col_id] = (row_id + 1)*0.3*(col_id + 1);"
{ group => 'Pareto Boundary', brief => 'sensitivity to Ground Truth explored through six artificial time-series', tut =>'tut_ccm_13_categorizeData.c', data => '6 synthetic data: permutations of feature-curves', features => 10, metrics => 'k-means, similiarty-metrics=300+'}, #! note: data: defined in "tut__aux__dataFiles__syn.c"
{ group => 'Pareto Boundary', brief => 'test growth-rate (linear?) of 30+ CCMs', tut =>'tut_ccm_15_useCase_singleMatrix_differentPatterns.c', data => 'synethtic: linearly changing scores', features => 8, metrics => 'similiarty-metrics: 300+, CCM(dynamic-algorithm-configuraiton): 30+, clustering-algorith: HCA::single'},
{ group => 'Pareto Boundary', brief => 'sensitivty of 30+ CCMs when confiugred through six different assumptison fo groudn-truth, and similairty defined through Euclid, Pearson, Spearman, Kendall/Kmar-Hassebrock', tut =>'tut_ccm_16_compareMatrixAndGold_twoVectors.c', data => 'synethtic: disjoint clusters (6 different permutations)', features => 1000, metrics => 'similiarty-metrics: euclid, Pearson, Spearman, Kendall/Kmar-Hassebrock; CCM: 30+'},
{ group => 'Ability of generalization', brief => 'Analyze 20 different data-permutations through the clustering-algorithms of hp-cluster, DBSCAN, K-means, mini-batch, HCA-permutations, MST, and the similarity-metrics of Minkowski, city-block, squared-Clark, squared-Pearson, product-moment, Canberra, Hellinger, downward-mutability, inner-product, squared-Euclid, Jensen-Shannon, Spearman, Kendall/Dice, MINE', tut =>'tut_ccm_17_ccmAlgDataSyn.c', data => 'synethtic: disjoint clusters (20 different permutations)', features => '100--1000', metrics => 'clsutering algorithms: hp-cluster, DBSCAN, K-means, mini-batch, HCA-permutations, MST; similiarty-metrics: minkoswksi, cityblock, squared-clark, squared-pearson, product-moment, Canberra, Hellinger, downward-mutaiblity, innter-proeuct, squared-Euclid, Jensen-Shannon, Spearman, Kenndal/Dice, MINE'}, #! where k-means and mini-batch are explreod  w/different iteration-counts (eg, niter=10,100,1000).
 { group => undef, brief => '', tut =>'tut_ccm_18_twoMatrices_x_goldCCM.c', data => '', features => undef, metrics => ''}, # ie, seems like only a parital/incompelte tut.
    #
    #
    #
{ group => 'Algorithm', brief => 'apply CCM VRC', tut =>'tut_ccm_1_matrixVSgold_VRC.c', data => 'X', features => 100, metrics => 'CCM: VRC'},
{ group => 'Algorithm', brief => 'apply CCM Silhouette', tut =>'tut_ccm_2_matrixVSgold_Silhouette.c', data => 'X', features => 100, metrics => 'CCM: Silhouette'},
{ group => 'Algorithm', brief => 'apply CCM Davies-Bouldin', tut =>'tut_ccm_3_matrixVSgold_DavisBouldin.c', data => 'X', features => 100, metrics => 'CCM: Davies-Bouldin'},
{ group => 'Algorithm', brief => 'apply CCM Dunn Index', tut =>'tut_ccm_4_matrixVSgold_Dunn.c', data => 'X', features => 100, metrics => 'CCM: Dunn\'s Index'},
{ group => 'Algorithm', brief => 'apply CCM Rands Index (permutation)', tut =>'tut_ccm_5_goldVSgold_RandsIndex_alt1.c', data => 'X', features => 100, metrics => 'Rand\'s Index'},
{ group => 'Algorithm', brief => 'apply CCM Rands Index', tut =>'tut_ccm_6_goldVSgold_RandsIndex_alt2.c', data => 'X', features => 100, metrics => 'Rand\'s Index (permutation)'},
{ group => 'Algorithm', brief => 'apply CCM ARI', tut =>'tut_ccm_7_goldVSgold_ARI.c', data => 'X', features => 100, metrics => 'CCM: ARI'},
{ group => 'Algorithm', brief => 'apply CCM Chi-Squared', tut =>'tut_ccm_8_goldVSgold_chiSquared.c', data => 'X', features => 100, metrics => 'CCM: Chi-Squared'},
{ group => 'Algorithm', brief => 'apply CCM Fowlkes-Mallows', tut =>'tut_ccm_9_goldVSgold_FowlesMallows.c', data => 'X', metrics => 'CCM: Fowlkes-Mallows'},
#
#
{ group => undef, brief => '', tut =>'tut_clust_dynamicK_1_hca_Silhoette.c', data => 'Real-life: Holzinger', features => undef, metrics => 'cluster-algorithm: HCA w/centroid=(single, centroid, average), k-means, SOM, hp-cluster, MST + HCA; CCM(internal): Silhouette'},
{ group => 'Algorithm', brief => 'apply a HCA + Silhouette clustering-algorithm for dynamic parameter-estimation', tut =>'tut_clust_dynamicK_1_hca_Silhoette.c', data => 'Real-life: Holzinger', features => undef, metrics => 'cluster-algorithm: HCA w/centroid=single; CCM(internal): Silhouette'},
{ group => 'Algorithm', brief => 'apply a K-Means + Silhouette clustering-algorithm for dynamic parameter-estimation', tut =>'tut_clust_dynamicK_2_kMeans_Silhoette.c', data => 'Real-life: Holzinger', features => undef, metrics => 'cluster-algorithm: K-means; CCM(internal): Silhouette'},
{ group => 'Algorithm', brief => 'apply a Minibatch + Dunns Index clustering-algorithm for dynamic parameter-estimation', tut =>'tut_clust_dynamicK_3_miniBatch_Dunn.c', data => 'Real-life: Holzinger', features => undef, metrics => 'cluster-algorithm: minbatch; CCM(internal): Dunn\'s Index'},
    { group => 'Algorithm', brief => 'apply an MST + HCA + VRC clustering-algorithm for dynamic parameter-estimation', tut =>'tut_clust_dynamicK_4_kruskal_VRC.c', data => 'Real-life: Holzinger', features => undef, metrics => 'cluster-algorithm: MST + HCA; CCM(internal): VRC'},
    { group => 'Pareto Boundary', brief => 'Relationship between clustering-algorithms (eg, the HCA-based SLINK) versus the 30+ CCMs', tut =>'tut_clust_dynamicK_5_hca_forResult_mulitpleCCMs.c', data => 'Real-life: Holzinger', features => undef, metrics => 'cluster-algorithm: HCA-single; CCM: 30+'},
{ group => 'Pareto Boundary versus reliable predictions', brief => 'How the performance of the HCA+Silhouette clustering-algorithm is influenced by changes in feature-curves', tut =>'tut_clust_dynamicK_6_find_nCluster_multipleData_Euclid.c', data => 'Synthetic feature-curves w/noise', features => undef, metrics => 'cluster-algorithm: HCA-single; CCM: Silhouette; similiarty-metric: Euclid'}, #! Note: data: tut__aux__dataFiles__mathFunctions__noise.c
{ group => 'Well-controlled NN architectures', brief => 'The relationship between multiple data versus an algorithm\'s configuration', tut =>'tut_clust_dynamicK_7_find_nCluster_multipleData_allSim_xmtPreFiltering.c', data => '*', features => undef, metrics => 'similarity-metircs: 300+; CCMs: Silhouette, VRC, Davis-Bouldin, Dunn\s Index; clustering-algorithms: 20+'}, #! Note:t eh data povided as input is optioanl ... eg, the seu of data in "tut__aux__dataFiles__mathFunctions__noise.c"
{ group => 'Algorithm', brief => 'apply the algorithm of HCA-single', tut =>'tut_clust-from-folder-1-hca.c', data => '*', features => undef, metrics => 'clustering-algorithm: HCA-single'},
{ group => 'Algorithm', brief => 'apply the algorithm of mini-batch', tut =>'tut_clust_kMeans__miniBatch.c', data => 'Real-life: Holzinger', features => undef, metrics => 'clustering-algorithm: mini-batch'},
{ group => 'Algorithm', brief => 'apply the algorithm of K-means++', tut =>'tut_clust_kMeans__plusspluss_1.c', data => 'Real-life: Holzinger', features => undef, metrics => 'clustering-algorithm: K-means++ (inside)'},
{ group => 'Algorithm', brief => 'apply the algorithm of K-means, where the SSE-convergence-step is changed to reflect the selection-step in K-means++', tut =>'tut_clust_kMeans__plusspluss_2.c', data => 'Real-life: Holzinger', features => undef, metrics => 'clustering-algorithm: K-means++ (inside K-means)'},
{ group => 'Algorithm', brief => 'apply the algorithm of MCL', tut =>'tut_clust_mcl.c', data => 'Real-life: Holzinger', features => undef, metrics => 'clustering-algorithm: hp-cluster + MCL'},
{ group => 'Algorithm', brief => 'apply the algorithm of MST + HCA', tut =>'tut_clust_mst_kruskal_1_hca.c', data => 'Real-life: Holzinger', features => undef, metrics => 'clustering-algorithm: MST + HCA'},
{ group => 'Algorithm', brief => 'apply the algorithm of MST', tut =>'tut_clust_mst_kruskal_2_kmeans.c', data => 'Real-life: Holzinger', features => undef, metrics => 'clustering-algorithm: MST'},
{ group => 'Algorithm', brief => 'apply the algorithm of MST + Silhouette', tut =>'tut_clust_mst_kruskal_3_ccm_Silhouette.c', data => 'Real-life: Holzinger', features => undef, metrics => 'clustering-algorithm: MST; CCM: Silhouette'},
{ group => 'Algorithm', brief => 'apply the algorithm of MST + Dunn\'s Index', tut =>'tut_clust_mst_kruskal_4_ccm_Dunn.c', data => 'Real-life: Holzinger', features => undef, metrics => 'clustering-algorithm: MST; CCM: Dunn\'s Index'},
    #
    #
{ group => 'Algorithm', brief => 'apply the algorithm of hp-cluster', tut =>'tut_disjointCluster.c', data => 'synthetic', features => 4, metrics => 'clustering-algorithm: hp-cluster'}, #! "examplify: how to use threshold-mask-variables to idneitfiy signicant regions, sigifant regiosn which are 'extracted' using disjtoitnf-roest-clsutering. The stnregth (and wkenass) of this appraoch cosnerns the direct relatioship between prior assumptiosn and clsuter-results: for disjoitn-clsutering to produce emanignful and correct results the 'remval' of in-signicnat cells need to be correct, a c'rorectness' which depends on acucrate prior assumptions wrt. the data.  ... we examplfiy: (a) how to comptue disjoint-clusters clsuters; (b) how to use the internal export-rotuiens for data-export; (c) how to programtailally access the cluster-resutls: from the latter we obseve how the 'same' generaiotn-and-exprotr-otuiens in k-means clsutering ("tut_kCluster.c") may be sued for disjtoint-forest-clustering, ie, where differnece si fodun wrt. the 'need' to specifiy mask-trheshold-fitlers. "
{ group => 'Algorithm', brief => 'apply the algorithm of hp-cluster, for data-case where no clusters should be found', tut =>'tut_disjointCluster__pointless.c', data => 'synthetic', features => 4, metrics => 'clustering-algorithm: hp-cluster'},
{ group => 'Algorithm', brief => 'apply the algorithm of hp-cluster + k-means', tut =>'tut_disjointKMeansCluster.c', data => 'synthetic', features => 10, metrics => 'clustering-algorithm; k-means + hp-cluster'},
#
#
#
 #   
{ group => 'Data Access', brief => 'compare 1d-lists with B-trees; search for pairs of key-value', tut =>'tut_db_bTree_simpleExample_1__keyValue.c', data => 'synthetic: random', features => 100*1000, metrics => 'data-structures: 1d-lists, B-trees; data-operations: random-insert, random-search'}, #! "demonstrates how a bo build and access a b-tree w/elements = 'key-->value' access-pattern"
{ group => 'Data Access', brief => 'compare 1d-lists with B-trees; search for pairs of triplets', tut =>'tut_db_bTree_simpleExample_2__rel.c', data => 'synthetic: random', features => 100*1000, metrics => 'data-structures: 1d-lists, B-trees; data-operations: random-insert, random-search'}, #! "demonstrates how a bo build and access a b-tree w/elements = 's_db_rel_t' access-pattern"
{ group => 'Data Access', brief => 'compare 2d-lists with B-trees; search for pairs of triplets; results used in \\cite{ekseth2018new} to identify a fast data-engine', tut =>'tut_db_bTree_simpleExample_3__2dList.c', data => 'synthetic: random', features => '100,000 -- 100,000,000', metrics => 'data-structure: 2d-sparse-list; data-operation: search'}, #! "demonstrates how a to build and access a data-structre using KNittingTools optmized data-access-patterns .. .examplify differnet use-case-patterns when using KnttingTools optmized data-access-pattern (where we use our "db_ds_directMapping.h"). A use-case conserns the case where we for each head 'store' 20 differnet relations: our 'simplicaiton' is that we 'only need' to access the 20 relations (in each of our iteraitons); ... "
{ group => 'Data Access', brief => 'compare 2d-lists with B-trees; randomly search for pairs of key-values and triplets', tut =>'tut_db_eval_dataAccess_2_bTree__randomSearch.c', data => 'synthetic: random', features => '100,000 -- 100,000,000', metrics => 'data-structure: B-trees, 2d-sparse-list; data-operation: insert, search'}, #! "demonstrates how a bo build and access a b-tree w/elements = ['key-->value', s_db_rel_t(...)]."
{ group => 'Data Access', brief => 'compare 1d-lists with B-trees; search for pairs of triplets; test the effects of strategies for data-normalization', tut =>'tut_db_useCase__1_normalizeData.c', data => 'synthetic: random', features => '800,000,000', metrics => ''}, #! "examplfies how to load a set of relations and synonysm from files int our "db_ds_directMapping.h" data-structue(s). ... Idea: examplifes a normlization-strategy for data-sets: produce a mapping-table based on a forest of synonyms, and then (a) write out this mapping-table, (b) noramlizes an input-matrix, and (c) examplfies how latter may be used to update+access our "db_ds_directMapping.h" (eg, wrt. b-tree-insertiosn) .... this mapping-table  ... Take 'as input' two different matrices: (a) 'synonym-relations' and (b) 'ordinary' SKB-relations ... ... API: we use our "get_centralityVertex_inDisjointforest__s_kt_forest_findDisjoint(..)" ("kt_forest_findDisjoint.h") to 'build' a mapping-table (or: a synonym-normalizaiton-list), ie, for which our nromalizaton-appraoch becomes stragith-forward." .... compares "1d-list-traversal VS poitner-based list-travesal"
{ group => 'Data Access', brief => 'evaluates the time to load triplet-data into memory', tut =>'tut_db_useCase__2_normalizeData.c', data => 'synthetic: random', features => undef, metrics => 'time(read file into memory)'}, #! "examplfies how logics in "tut_db_useCase__1_normalizeData.c" may be sued for a real-life data-set."
{ group => 'Data Access', brief => 'the ability of MySQL to handle the database-queries listed in \\cite{ekseth2018new}', tut =>'tut_external__mySQL.c', data => 'synthetic: random', features => '1000--100,000,000', metrics => 'data-structure: MySQL, data-operation: insert, search'},
    #
    # Note: below tuts exemplify lgocis for hashing of bibliograpic records
{ group => 'Data Access', brief => 'the time-cost of hashing bibliographic entities', tut =>'tut_hash_1_duplicateKeys.c', data => 'real-life: subset from \\cite{aalberg2008looking}', features => '2--13', metrics => ''}, #! Note: hash-metic: use an nitnerally nimplemntned hash-emtric, thoguh the cosruce-code deos nto state which .... the 'features' refers there to the strlen(wrds which ar hashed).
{ group => 'Data Access', brief => 'the time-cost of hashing bibliographic entities: use real-life data identified from \\cite{aalberg2008looking}', tut =>'tut_hash_2_wordsFromStrings.c', data => 'real-life: subset from \\cite{aalberg2008looking}', features => '2--..', metrics => ''}, #! a permutaiton of "tut_hash_1_duplicateKeys.c", where data si loaded into files.
{ group => 'Establish correlation', brief => 'test applicability of 300+ pariwise simlairty-metircs in classification of imputated text-strings, ie, the use of string-simliarty-metrics (StSM)', tut =>'tut_string_sim_1_singlePair.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'compare the Levenshtein-distance to the effects of 10+ metrics for data-normalization, ie, the use of string-simliarty-metrics (StSM)', tut =>'tut_string_sim_2_stringArray.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
#{ group => undef, brief => '', tut =>'tut__stub__config__inputData.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Algorithm for String-similarity', brief => 'Apply metric=Levenshtein, calculcate Shannons entropy, export result as histograms', tut =>'tut_parse_9_fromSparseString_split_multipleTailsEachRow_sparseSimEachMatrix.c', data => 'Data from Prof. Aalberg', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! Note: the Levenshtein-distnace is comptued through: s_kt_list_2d_kvPair_t obj_sparse = setOf__compute__vertex__kt_sim_string(&obj_sim, enum_id, &config_filter, &obj_str_2d, row_id, &strObj_1); ... applies entropy (const e_kt_entropy_genericType_t enum_entropy = e_kt_entropy_genericType_ML_Shannon)
#
#
#
 #   
{ group => 'Algorithm', brief => 'hp-cluster w/dynamic parameter-idnetifcation', tut =>'tut_disjointCluster_2_algComparisonSingleAlg.c', data => 'syntetic', features => 10, metrics => 'CCM: Silhouette, clustering-algorithm: hp-cluster w/dynamic parameter-idnetifcation'},
{ group => 'Pareto Boundary and establish correlation', brief => 'evaluate the effects of an algorithmic co-variance-pre step (eg, through Euclidean and Pearson) to the accuracy of clustering-algorithms (eg, K-means and hp-cluster); assess the dependence through 30+ CCMs', tut =>'tut_disjointCluster_3_algComparisonSingleMatrix.c', data => 'synetic: discrete, euclidean covaraince, perason-covariance, random-noise (permtuations)', features => 8, metrics => 'similarity-metrics: euclid, cityblock, Clart, Pearson, product-moment, Canberra, Hellinger, downward-mulaity, innter-prouct, squared-euclid, Jensen-Shannon, Spearman, Kendall-Dice, MINE; cluster-algorithm: hp-cluster; CCM: 30+'}, #! where the large number of simliarty-metircs are used in "s_hp_exportMatrix_t"
{ group => 'Pareto Boundary versus controllable learning', brief => 'evaluate the effects of an algorithmic co-variance-pre step (eg, through Euclidean and Pearson) to the accuracy of clustering-algorithms (eg, K-means and hp-cluster); assess the dependence through 30+ CCMs; explores a large number of algorithms, and differences in ground-truth', tut =>'tut_disjointCluster_4_differentAlgDifferentSyntheticDataSets.c', data => 'synetic: discrete, euclidean covaraince, perason-covariance, random-noise (permtuations)', features => '10--1280', metrics => 'similarity-metrics: euclid, cityblock, Clart, Pearson, product-moment, Canberra, Hellinger, downward-mulaity, innter-prouct, squared-euclid, Jensen-Shannon, Spearman, Kendall-Dice, MINE; cluster-algorithm: hp-cluster, DBSCAN, K-means, mini-batch, HCA w/centroids=(single, centroid); CCM: 30+'}, #! where the large number of simliarty-metircs are used in "s_hp_exportMatrix_t" 
#
#
#
{ group => 'Algorithm', brief => 'HCA-centroid analysed for hard-coded data', tut =>'tut_hca.c', data => 'synthetic', features => 6, metrics => 'cluster-algorithm: HCA-centroid'},
{ group => 'Algorithm', brief => 'HCA-max + Silhouette, Rand\'s Index, ARI, Fowlkes-Mallows and Mirkin; the algorithm-ensemble is sued to analyze the IRIS data', tut =>'tut_helloWorld.c', data => 'IRIS', features => 4, metrics => 'cluster-algorithm: HCA-max; CCM: Silhouette, Rand\'s Index, ARI, Fowlkes-Mallows, Mirkin; similarity-metrics: ; '},
{ group => 'Algorithm', brief => 'HCA-max with an Kendall-Dice covariance pre-step', tut =>'tut_hp_api_fileInputTight_10_exportClusterResult.c', data => 'synthetic', features => 9, metrics => 'cluster-algorithm: HCA-max; CCM: ; similarity-metrics (prestep): Kendall-Dice; similarity-metrics(inside): Euclid; '}, #! "examplify how our "hp_api_fileInputTight.h" may be combined with our "kt_clusterResult_condensed.h" to consturct a consnsed set of cluster-attriubtes, where latter may be used in our "hpLysisVisu.js" wrt. data-visauzliaitons. "
{ group => 'Algorithm', brief => 'the accuracy of the Silhouette CCM to different data', tut =>'tut_hp_api_fileInputTight_1_ccm_1_matrixHyp.c', data => 'Real-life: Milk', features => undef, metrics => 'cluster-algorithm: ; CCM: Silhouette; similarity-metrics: ; '},
{ group => 'Algorithm', brief => 'the accuracy of the Silhouette CCM + Shannon-Jeffreys entropy-metric to different data', tut =>'tut_hp_api_fileInputTight_2_ccm_2_matrixHyp_sparseInput.c', data => 'Real-life: Life Cycle Savings', features => undef, metrics => 'cluster-algorithm: ; CCM: Silhouette; similarity-metrics: Shannon-Jeffreys; '},
{ group => 'Algorithm', brief => 'the accuracy of the Rand\'s Index CCM to different data', tut =>'tut_hp_api_fileInputTight_3_ccm_3_hypHyp.c', data => 'Real-life: Cavendish', features => undef, metrics => 'cluster-algorithm: ; CCM: Rand\'s Index; similarity-metrics: ; '},
{ group => 'Reliable predictions', brief => 'feasibility of the Euclidean similarity to analyze a data-set describing changes in the metabolic rate (ie, the energy production in living systems)', tut =>'tut_hp_api_fileInputTight_4_dist_1_Euclid.c', data => 'Real-life: Metabolic Rate', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: Euclid; '},
{ group => 'Reliable predictions', brief => 'feasibility of the Cityblock similarity-metric to predict a rabbit\'s body-temperature', tut =>'tut_hp_api_fileInputTight_5_dist_2_CityBlock.c', data => 'Real-life: rabbit temperature', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: cityblock; '},
{ group => 'Reliable predictions', brief => 'feasibility of the Product-moment metric to predict a weed\'s length', tut =>'tut_hp_api_fileInputTight_6_dist_3_sparse_Pearson.c', data => 'Real-life: weed length', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics:  product-moment; '},
{ group => 'Reliable predictions', brief => 'Application: feasibility of the HCA-max algorithm to classify the IRIS-data', tut =>'tut_hp_api_fileInputTight_7_clustAlg_1_hca.c', data => 'Real-life: IRIS', features => 4, metrics => 'cluster-algorithm: HCA-max; CCM: ; similarity-metrics:; '},
# { group => '', brief => '', tut =>'tut_hp_api_fileInputTight_8_clustAlg_2_dynamicK_hca.c', data => 'Real-life: IRIS', features => undef, metrics => 'cluster-algorithm: HCA-max; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'feasibility of the k-medoid algorithm to classify the IRIS-data', tut =>'tut_hp_api_fileInputTight_9_clustAlg_3_sparse_kMeans.c', data => 'Real-life: IRIS', features => undef, metrics => 'cluster-algorithm: ; CCM: k-medoid; similarity-metrics: ; '},
    #
#!

    #!
    #! Note: below:  Note: calls: "initAndReturn__s_hp_clusterFileCollection_traverseSpec_t(); traverse__s_hp_clusterFileCollection_traverseSpec()" in "hp_clusterFileCollection.c" ... "provide logics to analyse a subset of the simlairty-metrics supported by hpLyssis Knitting-Tools sim-metric suit ... a generic stategy to idenitfy teh type of cluster-algorithms to use. ... provide lgocis to evlauate the perofmrance (eg, internal cosnistency and acucrayc) of a large number of simlairty-metric, clsuter-algorithms, data-sets and clsuter-comparison-metics (CCms) (oekseth, 06. feb. 2017)." 
{ group => 'Consolidation of what is learnt to identify the similarity-metric to be used', brief => "Evaluate for one feature-curve", tut =>'tut_inputFile_1_small__gold.c', data => 'synthetic: one feature-curve', features => 10, metrics => 'cluster-algorithm: K-means; CCM: 30+; similarity-metrics: 300+; '}, #!
{ group => 'Consolidation of what is learnt to identify the similarity-metric to be used', brief => "Evaluate for multiple feature-curves and real-life data", tut =>'tut_inputFile_2.c', data => 'synthetic: six feature-curve; real-life: 16 (eg, IRIS)', features => undef, metrics => 'cluster-algorithm: K-means; CCM: 30+; similarity-metrics: 300+; '}, #! "examplify the use of a 'advanced inptu-file specificaiton' through the "s_kt_matrix_fileReadTuning_t" into hpLysis-clustering, ie, in combiantion with the "traverse__s_hp_clusterFileCollection_traverseSpec(..)"   funciton in our "hp_clusterFileCollection.h""
{ group => 'Consolidation of what is learnt to identify the similarity-metric to be used', brief =>  "Evaluate for 17 feature-curves", tut =>'tut_inputFile_3.c', data => 'synthetic: 17 feature-curves', features => undef, metrics => 'cluster-algorithm: K-means; CCM: 30+; similarity-metrics: 300+; '},
{ group => 'Consolidation of what is learnt to identify the similarity-metric to be used', brief =>  "Evaluate for 25 real-life data", tut =>'tut_inputFile_4.c', data => 'real-life: 25 (eg, from UCI and the Guinea pig data)', features => undef, metrics => 'cluster-algorithm: K-means; CCM: 30+; similarity-metrics: 300+; '},
{ group => 'Consolidation of what is learnt to identify the similarity-metric to be used', brief =>  "Evaluate for 64 real-life data", tut =>'tut_inputFile_5.c', data => 'real-life: 64 (eg, from UCI)', features => 500, metrics => 'cluster-algorithm: K-means; CCM: 30+; similarity-metrics: 300+; '},
{ group => 'Consolidation of what is learnt to identify the similarity-metric to be used', brief =>  "Evaluate for a data describing the communication-latency in a super-computer", tut =>'tut_inputFile_6_vilje.c', data => 'real-life: Vilje communication matrix', features => undef, metrics => 'cluster-algorithm: K-means; CCM: 30+; similarity-metrics: 300+; '},
{ group => 'Consolidation of what is learnt to identify the similarity-metric to be used', brief =>  "Evaluate for 4000 syntetic data of random noise ", tut =>'tut_inputFile_7.c', data => '', features => undef, metrics => 'cluster-algorithm: K-means; CCM: 30+; similarity-metrics: 300+; '},
{ group => 'Consolidation of what is learnt to identify the similarity-metric to be used', brief =>  "Evaluate for 10 synthetically generated geometric shapes", tut =>'tut_inputFile_8.c', data => '', features => undef, metrics => 'cluster-algorithm: K-means; CCM: 30+; similarity-metrics: 300+; '},
    #
    #
    # Note: below uses: "hp_evalHypothesis_algOnData.h" //! which is used to provide a lsit of itnerest ing simlairty-emtics to investgiate 'in a paritcular investigoant of clsuter-algorithms and influence of sim-emtrics'.
{ group => 'Controllable learning', brief => 'explore the effects of randomized functions', tut =>'tut_hypothesis_whyResultDiffers_1.c', data => '6 synthetic data: permutations of feature-curve', features => undef, metrics => 'cluster-algorithm: 20+; CCM: ; similarity-metrics: 300+; randomness: srand, greedy-permutations, modulo, binomial'}, #! where 'greedy-permutations' refers to permtuations used in k-means++ and sci-kit-learn ... data:"tut__aux__dataFiles__syn.c"     
{ group => 'Controllable learning', brief => 'measure the effects of randomized functions, for which 6 synthetic and 14 real-life data are used in the evaluation', tut =>'tut_inputFile_9__algCmp__syntAndRealLife.c', data => '', features => undef, metrics => 'cluster-algorithm: 20+; CCM: ; similarity-metrics: 300+; randomness: srand, greedy-permutations, modulo, binomial'}, #! where 'greedy-permutations' refers to permtuations used in k-means++ and sci-kit-learn ... data:"tut__aux__dataFiles__syn.c"     
{ group => 'Execution-Time versus reliable predictions', brief => 'Explore the relationship between similarity-metrics versus clustering-algorithms, for 20+ different data-sets (real-life, and time-series)', tut =>'tut_inputFile.c', data => '', features => undef, metrics => 'cluster-algorithm: 20+; CCM: ; similarity-metrics: 300+; randomness: srand, greedy-permutations, modulo, binomial'}, #! "we have chosen to 'palce' the clustierng-algorithm 'outsid ethe otehr loops' as the clustering-algorithm is (in reseharc-ltiterature) oftne the mina-focus (ie, in cotnrast to our observaiotns indaiting that the silmairty-emtric is of even higher improtance wrt. the clsuter-reuslts)."
    #
    #
    #    
{ group => 'Base-algorithm', brief => 'Examplifies how to use k-means clustering to comptue results', tut =>'tut_som.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Base-algorithm', brief => 'Examplifies how to use k-means clustering to comptue results', tut =>'tut_kCluster.c', data => 'synthetic', features => 5, metrics => 'cluster-algorithm: K-means; CCM: ; similarity-metrics: ; '},
{ group => 'Algorithm', brief => 'KD-tree + hp-cluster + dynamic paremter-identifcaiton through CCM=Silhouette', tut =>'tut_kd_0_cluster.c', data => 'Real-life: IRIS', features => 4, metrics => 'cluster-algorithm: KD-tree, hp-cluster, dynamic paremter-identifcaiton; CCM: Silhouette; similarity-metrics: ; '},
{ group => 'Ability of generalization', brief => 'Use of many simliarty-metircs in multi-metric learning', tut =>'tut_kd_1_cluster_multiple_simMetrics.c', data => '100+ real-life, 10+ synetic data', features => undef, metrics => 'cluster-algorithm: DBSCAN, hp-cluster, MCL; CCM: 13+; similarity-metrics: ; '}, #! Note: data: tut__aux__dataFiles__syn.c, tut__aux__dataFiles__FCPS.c, tut__aux__dataFiles__realMine.c, tut__aux__dataFiles__realKT.c ... ande examplfeis support of reading GEO-data ... "For this test-case to work you need to have downlaoded the GEO-data-sets, eg, the " ../../mine-data-analysis/src/sample_data/GSE19753_series_matrix.txt" which is pased through KnittingTools "../../mine-data-analysis/src/perl_buildSamples/parse_geo.pl" GEO-patser."
{ group => 'Algorithm for Digital forensics', brief => 'classify data based on CCM=SSE and algorithm=KD-tree', tut =>'tut_kd_1_matrix_sse.c', data => 'real-life: Wine with missing/imputated data', features => undef, metrics => 'cluster-algorithm: KD-tree; CCM: SSE; similarity-metrics: ; '},
{ group => 'Execution-Time and Data-Structure', brief => 'effects of algorithm-permutations in building of KD-trees', tut =>'tut_kd_2_performanceEvaluate.c', data => 'Synetic: s-rand', features => 5, metrics => 'cluster-algorithm: hp-cluster, DBSCAN, KD-tree; CCM: ; similarity-metrics: ; '}, #! Note: times the effects of different stratgeies
{ group => 'Establish correlation', brief => 'KD-tree versus effects of dynamic parameter-identification', tut =>'tut_kd_3_data_simMetric.c', data => '100+ real-life and sytentic data', features => undef, metrics => 'cluster-algorithm: DBSCAN, hp-cluster, KD-tree, HCA; CCM: 13+; similarity-metrics: ; '}, #! "compute [data-id][sim-id] = max(ccmScore(ccm-id, cluster-result)) wrt. sim-id, gold-ccm, matrix-ccm (oekseth, 06. jul. 2017)." ... identify the best simalrity-emtrics to be sued for a given data-set. ... data: tut__aux__dataFiles__realMine.c , tut__aux__dataFiles__FCPS.c , tut__aux__dataFiles__realKT.c , tut__aux__dataFiles__realMine.c , tut__aux__dataFiles__FCPS.c , tut__aux__dataFiles__realKT.c , tests/data/kt_mine/fish_growth.tsv , ../../mine-data-analysis/src/result_GSE19743.tsv , data/local_downloaded/iris.data.hpLysis.tsv , 
# { group => undef, brief => '', tut =>'tut_kd_3_data_simMetric__configFileRead.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
#{ group => undef, brief => '', tut =>'tut_eval_3_kdTree.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},   
{ group => 'Exploit correlation', brief => 'How HCA may be used as a pre-step to irmpvoe the resutl-accuracy', tut =>'tut_kMeans_hcaPreStep.c', data => 'synthetic: feature-curves', features => '20--400', metrics => 'cluster-algorithm: K-means, HCA-centroid; CCM: ; similarity-metrics: ; '},
    #
    #
    #

    
    
{ group => 'Data-structure', brief => 'Access 1d-scalar items', tut =>'tut_list_0_pairFloat.c', data => 'synthetic: linear', features => 3, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "describe basic lgoics wrt. our "kt_list_1d.h" API. ... demosntrate insert and 'reading' from our "s_kt_list_1d_pairFloat_t", where we use a 'covnert-into-2d' through our "convertFrom__s_kt_list_1d_pairFloat__s_kt_set_2dsparse_t(..)" (defined in our "kt_set_2dsparse.h")." 
{ group => 'Data-structure', brief =>  'Calculate sparse-Euclid', tut =>'tut_list_1_pairFloat_sim.c', data => 'synthetic: linear', features => 3, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "how to use a list of paris as input to a sparse data-set comptuation; extends our "tut_list_0_pairFloat.c" with a 'call' to oru "kt_sparse_sim.h"."
{ group => 'Execution-Time and well-controlled NN-architectures', brief => 'Explore the time-cost of temporary files', tut =>'tut_list_2_pairFloat_readFromFile_sim.c', data => 'synthetic: linear', features => 3, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "how to export (to a file) and import (from a file) a list of pairs/relations, and then use latter as input to a sparse simlairty-copmtaution."
{ group => 'Data-structure', brief => 'build a max-heap', tut =>'tut_list_3_heap_uint.c', data => 'synthetic: linear', features => 1000, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "demosntraes the max-heap-proerpty"
{ group => 'Data-structure', brief => 'Access a max-heap', tut =>'tut_list_4_heap_keyValue.c', data => 'synthetic: linear', features => 1000, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},

{ group => 'Data-structure', brief => 'Access a feature-matrix', tut =>'tut_matrix_0_initAccessAndFree.c', data => 'synthetic: linear', features => 3, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Execution-Time and well-controlled NN-architectures', brief => 'Explore the time-cost of temporary files', tut =>'tut_matrix_1_toAndFromFile_values.c', data => 'synthetic: linear', features => 100, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => undef, brief => '', tut =>'tut_matrix_2_toAndFromFile_values__explicitWriting.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "evlauates the reading of a file which has been 'amnally' cosnturcted: otherise simliar to our "tut_matrix_1_toAndFromFile_values.c"."
{ group => undef, brief => '', tut =>'tut_matrix_3_toAndFromFile_valuesAndStrings.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
    #
    #
    #

{ group => 'Well-controlled NN-architectures', brief => 'Benchmark basic strategies for calculcating pairwise similarity', tut =>'tut_measure_dbScan_auxFunctions.c', data => '*', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! Note: handle terminatl-input-args fo meausre perofmrance of the hp-clsuter algorithm
    #
    #
{ group => 'Well-controlled NN-architectures', brief => 'Calculate Euclidean distance for a small matrix', tut =>'tut_mine.c', data => '', features => 5, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: Euclid; '}, #! " simplitic example to how corrleating amtrices using hpLyssi impelmantion fot eh MINE correlation-metric  (oekseth, 06. jan. 2017).   " ... data: "measure_kt_matrix_cmpCluster__stub__loadSampleData.c"
{ group => 'Well-controlled NN-architectures', brief => 'explore the effects of data-normlization', tut =>'tut_norm_1_differentNormStrategies.c', data => 'one real-life', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; normalizaiton: 10+'}, #! "examplify different data-nroamzioant-strategies and its implciaotn on kd-tree-clsutering"
{ group => 'Algorithm for Digital Forensics', brief => 'construct an algorithm from a metric for data-normalization, use of hp-cluster for data-partitioning, improve specificity through dynamic parameter-evaluation, where CCM=Silhouette is used', tut =>'tut_norm_2_cluster_kd_dbScan.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: Silhouette; similarity-metrics: Euclid; normalizaiton: 10+'},
{ group => 'Algorithm for Digital Forensics', brief => 'apply AI to identify a fast strategy for classification; for this task, we investigate the effects of data-normalization across five algorithms, six cluster-algorithms, 10+ data-normalization-strategies, six metrics for similarity', tut =>'tut_norm_3_cluster_kd_dbScan_80DataSets.c', data => '', features => undef, metrics => 'cluster-algorithm: hp-cluster, HCA-single, Kruskal-Dynamic, Random; CCM: Silhouette, Dunns, SSE, VNND, VRC, Davis-Bouldin, ; similarity-metrics: Euclid, cityblock, Canberra, Pearson, Kendall-CoVariance, MINE; '},

    #
    #
    #    
{ group => undef, brief => '', tut =>'tut_parse_1_matrix_dense.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "examplfiy the writign and reading to/from a file using logics in our "kt_matrix.h" API."
{ group => 'Well-controlled NN-architectures, Execution-Time and Sparse Data-structure', brief => 'time to read a sparse 1d scalar data-set', tut =>'tut_parse_2_sparseList_headTailScore.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Algorithm', brief => 'hp-cluster to partition sparse data', tut =>'tut_parse_3_fromSparseStrings_dbScan.c',  data => 'synthetic', features => undef, metrics => 'cluster-algorithm: hp-cluster; CCM: ; similarity-metrics: ; '},
{ group => 'Well-controlled NN-architectures, Execution-Time and Data Structure', brief => 'time to read a sparse 1d-triplet set of data', tut =>'tut_parse_4_sparseList_headTail.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Well-controlled NN-architectures, Execution-Time and Data-structure', brief => 'time to read a sparse 1d-pair set of data', tut =>'tut_parse_5_sparseList_score1Score2.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
#{ group => undef, brief => '', tut =>'tut_parse_6_sparseList_headScore.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Well-controlled NN-architectures, Execution-Time and Data-structure', brief => 'time to read, and hash, a list of strings, for real-life data (version=1)', tut =>'tut_parse_7_fromSparseString_split.c', data => 'Data from Prof. Aalberg', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "demonstrates a parsing-task where we laod data-rows into a string-1d-list, and then use logics in our "kt_hash_string.h" to split a string-set into (head, tail) parts."
{ group => 'Well-controlled NN-architectures, Execution-Time and Data-structure', brief => 'time to read, and hash, a list of strings, for real-life data (version=2)', tut =>'tut_parse_8_fromSparseString_split_multipleTailsEachRow.c', data => 'Data from Prof. Aalberg', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "demonstrates a parsing-task where we laod data-rows into a string-1d-list, and then use logics in our "kt_hash_string.h" to split a string-set into (head, tail) parts."



{ group => 'Execution time and exploit correlation', brief => 'explore permtautions of Kendall\'s Tau and Spearman', tut =>'tut_perf_metric_slowVS_fast_rankBased.c', data => 'synthetic: srand', features => '64--640', metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
#{ group => undef, brief => '', tut =>'tut_printFacts_simMetrics_base.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},


{ group => 'Controllable learning', brief => 'the relationship between randomization-algorithms, CCMs, and similarity-metrics', tut =>'tut_random_difference_1.c', data => 'synthetic: random', features => 40, metrics => 'cluster-algorithm: ; CCM: 30+; similarity-metrics: MINE, Euclid; random: default, srand,m kemans++-pemrtautions, binomial; '}, #! "examplifeis appliciaotn of two CCMs, mulipel clsuter-algorithms and and muliple data-sets. ... examplfies different strategies for randomc cosntruction of numbers: in this example we demonstates/inveestigates the seperatebility between different 'randomized data-sets': we comapre each 'ranomdized vector' to 's' samples (for the 'same' randomized funciotn) and to 'm' other randomziation-ralgorithms. The result (of our comparison) is a simliarty-matrix between the 'm' different randomzied functions. The result is evaluated through applicaiotn of CCMs: we comptue the simalrity-matrics wrt.t eh simlarities, and thereafter evaluate the simliarty-matrx 'towards' our null-hypothesisie (ie, with out 'godl-standard-assumption'): to investigate/test that "the randomizaiton-type results 'ends in' clearly distinct distrubiotns", an assunption/hypothesis we expect will Not hold. In order to further invesitgate the latter we have uploaded the results to a web-based GUI at ...??..  
    #!
    #!
    #! Note: below uses logics in "kt_semanticSim.h":
{ group => 'Execution time and controllable learning', brief => 'effects of different software-patterns for calculating semantic similarity', tut =>'tut_sem_cmpAlg_reachComputation.c', data => 'synthetic: linear', features => '5--100', metrics => 'semantic simliarity: Resnik; cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "evalaute executiont-time-effect on/through different algorithms. ... we evlauate effects of differnet closure-algorithms.    -- comparison(base): This subroutine is based on a method described in "Transitive Closure Algorithms Based on Graph Traversal" by Yannis Ioannidis, Raghu Ramakrishnan, and Linda Winger, ACM Transactions on Database Systems, Vol. 18, No. 3, September 1993, Pages: 512 - 576. It uses a simplified version of their "DAG_DFTC" algorithm." .... uses test-logics in "tut_sem_cmpAlg_reachComputation__stub__startMeasure.c"
{ group => undef, brief => '', tut =>'tut_sem_cmpAlg_reachComputation__stub__startMeasure.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, 
    #
    #
    #
{ group => 'Metric-space for controllable learning', brief => 'artifically construct new simliarty-metrics, and explore their feasability', tut =>'tut_simEval_1_syntheticData_syntheticFunctions.c', data => 'artifical', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
    


    #
    #
    #    
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Euclid', tut =>'tut_sim_1_vec_Euclid.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! "demonstrates how a similartiy-metirc may be used to describe the simlairty between two vectors. "
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Shannon\'s Entropy', tut =>'tut_sim_2_vec_Shannon.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through MINE', tut =>'tut_sim_3_vec_MINE.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Chebychev', tut =>'tut_sim_4_vec_chebychev.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Gower', tut =>'tut_sim_5_oneToMany_hideValues_Gower.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through squared-chord', tut =>'tut_sim_6_oneToMany_squaredChord.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Lorentzian', tut =>'tut_sim_7_oneToMany_Lortentizan.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Wave-Hedges', tut =>'tut_sim_8_oneToMany_WaweHedges.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Kendall', tut =>'tut_sim_9_manyMany_Kendall_coVariance.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through sample-covariance', tut =>'tut_sim_10_manyMany_sampleCoVariance.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Cosine', tut =>'tut_sim_11_manyMany_max_cosine.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Neyman', tut =>'tut_sim_12_manyMany_min_Neyman.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Dice', tut =>'tut_sim_13_manyMany_min_Dice.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Matusita', tut =>'tut_sim_14_manyMany_Matusita__setInputInLoop.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through Euclid-permutation', tut =>'tut_sim_15_manMany_Euclid__selectBestInsideMetricComp.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through all the 300+ similarity metrics', tut =>'tut_sim_16_manyMany_singleInput_allMetrics_ccmEval.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate vector similarity through 10+ metrics for pairwise similarity, and 30+ CCMs', tut =>'tut_sim_17_manyMany_singleInput_listOfGolds_x_CCMs.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},    
    #
    #
    # ----------------------------------------------------------------------------
    #
    #
    #    
    #
    #
    #
{ group => 'Establish correlation', brief => 'Calculate sparse vector similarity through Euclid', tut =>'tut_sparseSim_1_vec_Euclid.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate sparse vector similarity through Shannon', tut =>'tut_sparseSim_2_oneToMany_Shannon.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate sparse vector similarity through Canberra', tut =>'tut_sparseSim_3_manyToMany_densePostSelfComparison_Canberra.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate sparse vector similarity through metric=intersection; post-analyse through algorithm=HCA', tut =>'tut_sparseSim_4_fromFile_intersection__postOps_hca.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate sparse vector similarity through Gower', tut =>'tut_sparseSim_5_para_oneToMany_Gower.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Calculate sparse vector similarity through Topsoe', tut =>'tut_sparseSim_6_para_manyMany_denseResult_Topsoe.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation', brief => 'Application: analyse Apache web-server traffic through sparse pairwise similarity-metrics', tut =>'tut_sparseSim_7_realLife_apacheLog_compareMultipleFiles.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
    #
    #
    #
{ group => 'Establish correlation versus Execution-time', brief => 'explore different implementation strategies for matrix multiplication', tut =>'tut_sse_matrixMul_cmpApproaches.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => undef, brief => '', tut =>'tut_sse_matrixMul_cmpApproaches__stub.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation versus Execution-time', brief => 'invesitgate the effects of different strategies for SSE2 hardware-parallel instructions', tut =>'tut_sse_matrixMult_lowLevel.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},

    #
    #
    #
{ group => 'Establish correlation', brief => 'Classification:  compute $[data-id][sim-id] = max(ccmScore(ccmId, clusterResult))$', tut =>'tut_time_1_data_syntetic_wellDefinedClusters.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation in execution time', brief => 'identify the execution-time of different algorithms across a 100+ real-life and synthetic data.', tut =>'tut_time_2_clustAlg_syntAndReal.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation in execution time', brief => 'use artificial data to measure the performance of CCMs', tut =>'tut_time_3_ccm_synt.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation in execution time', brief => 'use randomized data to measure the performance of CCMs', tut =>'tut_time_4_ccm_rand.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Establish correlation in Digital Forensics', brief => 'use CCM=SSE in combination with KD-tree and data-normalization', tut =>'tut_time_5_ccm_kdTree_sse.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => undef, brief => '', tut =>'tut_time_6_ccm_kdTree_differentEnumStrategies.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => undef, brief => '', tut =>'tut_wrapper_emd.c', data => 'syntetic 1d-features', metrics => '"earth mover distance" (EMD)'},
 { group => undef, brief => '', tut =>'tut_wrapper_histogram.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! an empty wrapper-function
 { group => undef, brief => '', tut =>'tut_wrapper_image_segment.c', data => 'RGB-parsing', metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},  #! Tests correctness of RGB-image-parsing (eg, from txt-formatteded image-data).  
 { group => undef, brief => '', tut =>'tut-aux-matrixToPPM.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! a conversion-script: turns an hpLysis data-tsv-matrix into a PPM-file.
# FIXME: -------------- pre: understand, exaplain, adn enumerte (into a Perl-strucutre) the tuts for image clasificaiotn); run each of them (ie, if the files are found).
    # -----------
{ group => 'Controllable learning', brief => 'Exemplifies how Shannon\'s entropy may be computed for a data-set with linear increase in counts.', tut =>'tut_entropy_1_shannon.c', data => 'synthetic: 1d-list of numbers', metrics => 'Shannon entropy'}, 
{ group => 'Controllable learning', brief => 'Exemplifies how to compute Renyi\'s entropy-score separately for each row in a matrix.', tut =>'tut_entropy_2_matrix_Renyi.c', data => 'synthetic: 1d-list of numbers', metrics => 'entropy: 20--40 different metrics'}, #! approx 19--20 different entropy-metrics; if scaling is applied, approx 40 different entropy-metrics are explroed
# FIXME: useful to create histogram of images in a videostram? possible area of applicaitons?
# FIXME: pre: training-data for Digital Forensics?
    # FIXME: "hp_image_segment.c" and "":
    # FIXME: ... how does below relate to the artiical data-ensambles in ""?
    # FIXME: make the "config_useSyntDataSets" optional dynamic? <-- re-factor "tut_entropy_4_imageCmp.c" with "hp_frameWork_CCM_boundaries.h" into fiding extreme seperaitons in ground-truths (eg, wrt. CCM-cofnirutations, where "tut_entropy_3_rankAcrossFiles.c" explores the effect of entropy-congiruraitons)? .... how can the resulting files (eg, metric-reccomendtioatn) be made more itnituvie (to users)? .... Cosntruct a config-result-header-file? ... Possible to add a semi-manual iteraitve loop (eg, where users apply the recommended cofnigurion to auto-classify a given data)? ... how to desing such a framwork?
{ group => 'Algorithm for Digital Forensics', brief => 'Identify an AI-methdology to compute classifications in $O(n)$ time, a method used used to identify the 1d-entropy algorithm in \\cite{ekseth2020entropy1dalg}', tut =>'tut_entropy_3_rankAcrossFiles.c', data => 'real-life image-data', metrics => 'entropy, rgbToScalar, histogram', metrics_detailed => 'rgbToScalar:[min, max, sum, Hue, Saturation, multiply(Hue, Saturation), max(Hue, Saturaion)/minimum(Hue, Saturaion); histograms:[count-Bins:[10, 10, 1000], average-on-bins:[10, 100, 1000], sum:[10, 100, 1000]], combineInputData-max/min-:[data(0), entropy:[best, worst]]; e_kt_normType_t; s_kt_correlationMetric_t; s_tut_entropy_bestMetric_t;  '}, #! data: "vegas_som_inputData/vegas_tm5_1994_lrg_export_downSamplefrac0.01_cubic.tsv",
{ group => 'Establish correlation in Digital Forensics', brief => 'compare the 1d-algorithm in \\citep{ekseth2020entropy1dalg} to the establishment (ie, variations of baseline-algorithms); for this task, explore the relationships between entropy-metrics, matrix-conversions, and transformations of 2d-data into scalar numbers', tut =>'tut_entropy_4_imageCmp.c', data => '', features => undef, metrics => 'matrix-to-scalar conversions; '}, #! Note: used API in "tut_entropy_3_rankAcrossFiles.c" (a file which among others provides/defined hard-coded iamge-paths; file-opaths defined in "tut3_arr_fileNamesInput"; fiels laoded by calling "load_dataFiles_applyRGB_merge_toMatrix(...)".
# ----------
{ group => 'Algorithm for Digital Forensics', brief => 'describes an algorithm for threshold-based identificaiton of trajectories in data', tut =>'tut_sintef_1_helloWorld.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
    # ---------------
    # ---------------
    # ---------------    
# FIXME: update below "tut_image_1_helloWorld.c" when we have completed eval in our "tut_image_3_exploreImpactOfConfigurations_simMetric.c"    
{ group => 'Algorithm for Image Analysis', brief => 'explore different methdologies for the identification of disjoint regions', tut =>'tut_image_1_helloWorld.c', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},
{ group => 'Algorithm for Image Analysis', brief => 'investigate the effects modifying strategies for blurring, and clustering, of images', tut =>'tut_image_2_exploreImpactOfConfigurations_blur.c', data => 'synethtic cluster-shapes', metrics => 'blurring, image-normalisation'}, #! ie, iterates across different settings, and provide lgocis for validitng loading of images.
{ group => 'Algorithm for Image Analysis', brief => 'identify the effects of K-means for the task of clustering RGB points in images', tut =>'tut_image_3_exploreImpactOfConfigurations_simMetric.c', data => '', features => undef, metrics => 'all the sim-metrics, '},
{ group => undef, brief => '', tut =>'', data => '', features => undef, metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '},    
{ group => undef, brief => '', tut =>'tut_image_6_validate_parsing.c', data => 'syntetic', metrics => 'cluster-algorithm: ; CCM: ; similarity-metrics: ; '}, #! exemplifes strategies for image-parsing, and simplfied lgocis to validat coissiten in export-lgocis with import-logics    
    );


sub aux_identifyUnique_groups {
    my %hash;
    foreach my $o (@arr) {
	#printf("...");
	if(!defined($o->{group})) {next;} #! ie, as we then ingore this entry
	if(!defined($hash{$o->{group}})) {
	    $hash{$o->{group}} = 1;
	} else {
	    $hash{$o->{group}} += 1;
	}
    }
    #!
    #!
    my $cnt = 0;
    #! Prnt out a Perl-sctuctre of the order
    printf("#! Note: Identified %d groups (tuts: %d)\n", scalar(keys(%hash)), scalar(@arr));
    foreach my $key (keys(%hash)) {
	printf("\"%s\" => $cnt, #! occurences: %s\n", $key, $hash{$key});
	$cnt++;
    }
}

sub tutSummary_to_LatexSection {
    my ($result_file) = @_;
    my $code_location = sprintf("%s:%d", __FILE__, __LINE__);
    open(FILE_RESULT, ">$result_file") or die("Unable to construct open file-pointer for file=\"$result_file\", at $code_location\n");
    printf(FILE_RESULT "%% Note: this file was generated at %s:%d\n\n\n", __FILE__, __LINE__);

    my $hash = {
	#"Execution-Time" => 0, #! occurences: 1
"Base-algorithm" => 1, #! occurences: 2
"Algorithm" => 2, #! occurences: 34
"Algorithm for Image Analysis" => 3, #! occurences: 3
"Algorithm for Digital forensics" => 4, #! occurences: 1
"Algorithm for Digital Forensics" => 5, #! occurences: 4
"Algorithm for String-similarity" => 6, #! occurences: 1
#
#! Note: Identified 35 groups
"Pareto Boundary" => 7, #! occurences: 12
"Pareto Boundary versus reliable predictions" => 8, #! occurences: 1
"Pareto Boundary and establish correlation" => 9, #! occurences: 1
#
"Well-controlled NN architectures" => 10, #! occurences: 1
"Well-controlled NN-architectures" => 11, #! occurences: 3
"Well-controlled NN-architectures, Execution-Time and Data-structure" => 12, #! occurences: 3
#
"Well-controlled NN-architectures, Execution-Time and Sparse Data-structure" => 13, #! occurences: 1
"Well-controlled NN-architectures, Execution-Time and Data Structure" => 14, #! occurences: 1
#
"Data-structure" => 15, #! occurences: 5
"Data Access" => 16, #! occurences: 9
#
"Execution-Time" => 17, #! occurences: 1
"Execution-Time and Data-Structure" => 18, #! occurences: 1
"Execution-Time versus reliable predictions" => 19, #! occurences: 1    
"Execution time and exploit correlation" => 20, #! occurences: 1
"Execution-Time and well-controlled NN-architectures" => 21, #! occurences: 2
"Execution time and controllable learning" => 22, #! occurences: 1
"Execution-Time and controllable learning" => 23, #! occurences: 1
"Execution-Time and establish correlation:" => 24, #! occurences: 1
#
#
"Pareto Boundary versus controllable learning" => 25, #! occurences: 1
"Establish correlation" => 26, #! occurences: 29
"Establish correlation versus Execution-time" => 27, #! occurences: 2
"Establish correlation in execution time" => 28, #! occurences: 3
"Establish correlation in Digital Forensics" => 29, #! occurences: 2
#
#
"Exploit correlation" => 30, #! occurences: 1
"Reliable predictions" => 31, #! occurences: 4
"Ability of generalization" => 32, #! occurences: 3
#
"Controllable learning" => 33, #! occurences: 5
"Metric-space for controllable learning" => 34, #! occurences: 1
"Consolidation of what is learnt to identify the similarity-metric to be used" => 35, #! occurences: 8
    };
    #!
    #! Construct a list to hold each item:
    my @arr_strings;
    foreach my $k (keys(%{$hash})) {
	push(@arr_strings, {group => '', arr => []});
    }
    foreach my $k (keys(%{$hash})) {
	my $index = $hash->{$k};
	$arr_strings[$index]->{group} = $k; #! ie, a recirpocal map.
    }
    printf("#! Note: Identified %d groups (tuts: %d)\n", scalar(keys(%{$hash})), scalar(@arr));
    #!
    #! Add each entry to a given array:
    foreach my $o (@arr) {
	#printf("...");
	if(!defined($o->{group})) {next;} #! ie, as we then ingore this entry
	my $index = $hash->{$o->{group}};
	if(!defined($index)) {
	    printf("!!\t Group unkown: \"%s\"\t\t(skips)\n", $o->{group});
	} else {
	    my $base_url = 'https://bitbucket.org/oekseth/hplysis-cluster-analysis-software/src/master/src/';
	    #! Src: https://stackoverflow.com/questions/6508613/perl-how-can-i-capitalize-a-regex-match
	    #! Case 1): ensure that a captial-letter starts after '\{' and after '\!\s*'
	    #!  Case 1.a): <- an example of a string to fix: \index{software ! knowledge Discovery.} => \index{Software ! knowledge Discovery.}
	    $o->{brief} =~ s/^([a-z])/\U$1/g;
	    my $word_1 = '\href{' . $base_url . $o->{tut} . '}{' . $o->{brief} . '}';
	    push(@{$arr_strings[$index]->{arr}}, $word_1);
	}
    }
    #! ------------------------------------------
    #! 
    #! Cosntruct a hash of category--word relationships.
    foreach my $obj (@arr_strings) {
	# printf("ref:%s\n", ref($obj));
	my $arr = $obj->{arr};
	#my $word_prev = undef;
	my @arr = ();
	#!
	#! Handle emppty spots:
	my $index_lastWithContent = 0; #! ie, to handle cases where 'empty term-spots are used'.
	for(my $i = 0; $i < scalar(@{$arr}); $i++) {
	    my $word_1 = $arr->[$i];
	    #$word_1 =~ s/\\d/(number)/g; #! ie, make a regex more reabile to non-regex-software-delvopers
	    #$word_1 =~ s/\\+/(one or more entries)/g; #! ie, make a regex more reabile to non-regex-software-delvopers
	    if($word_1 !~/^\s*$/) {
		$index_lastWithContent = $i;
	    }
	}
	for(my $i = 0; $i < scalar(@{$arr}); $i++) {
	    my $word_1 = $arr->[$i];
	    if($index_lastWithContent != $i) {
	    #if($i != scalar(@{$arr})-1) {
		if($i < $index_lastWithContent) {
		    push(@arr, '\item \textit{' . $word_1 . '};' . "\n"),
		}
	    } else { 
		push(@arr, '\item \textit{' . $word_1 . '}.' . "\n"),
	    }
	}
	my $str_syn = join(" ", @arr);
	#! 
	#! Add to category:
	my $key = $obj->{group};
	my $str = qq(\\subsection{Class: \\textit{$key}}
\\begin{enumerate}
$str_syn
\\end{enumerate}
);	
	printf(FILE_RESULT $str . "\n");
    }
    #!
    close(FILE_RESULT);    
}

#!
#! Note: below is used to generate a crude appraoch for the order of printing to be used
#aux_identifyUnique_groups();

#!
#! Construct a LAtex-rpesrntaiton:
tutSummary_to_LatexSection("tmp.tex");

