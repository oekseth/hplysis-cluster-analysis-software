#ifndef hc_minHeap_h
#define hc_minHeap_h

#include "types_base.h"
#include "def_typeOf_float.h"
//#include "def_memAlloc.h"

/**
   @struct
   @brief an in-array min-heap implemtantion. Based on the work described (among ohters) in the "fastclsuter" clsuter-pacakge (oesketh, 06. nov. 2017).
   @remarks to reduce exeuciotn-tiem we assume that all valeus are of interest, therefore if usnure of tthe latte rhten please validat your iniptu-data before calling this funciton.
 **/
typedef struct s_hc_heap_min {
  t_float *list_scores; // const A;
  uli list_size;  uli list_currentPos;
  //uli size;
  uli *list_forward;
  uli *list_reverse;
} s_hc_heap_min_t;

//! set the "s_hc_heap_min_t" object toe empty:
#define MiF__setToEmpty__s_hc_heap_min_t() ({s_hc_heap_min_t self; self.list_scores = NULL; self.list_size = 0; self.list_currentPos = 0; self.list_forward = NULL; self.list_reverse = NULL; self;})

//! @returnt he score in the tree at the given index
#define MiF__scoreInTreeAtIndex(i) ({ assert(self); assert(self->list_size); assert(self->list_scores); assert(i < self->list_size); assert(self->list_forward); /*return=*/self->list_scores[self->list_forward[i]];})

//! Swap the index-pairs.
#define heap_swap_s_hc_heap_min(self, i, j) ({ assert(self); assert(i < self->list_size); assert(j < self->list_size); \
      uli tmp = self->list_forward[i]; self->list_forward[i] = self->list_forward[j]; self->list_forward[j] = tmp; self->list_reverse[self->list_forward[i]] = i; self->list_reverse[self->list_forward[j]] = j; })

/**
   @brief intiates a min-heap (eosekth, 06. nov. 2017).
   @param <list_scores> a 1d-list of matrix-scores
   @param <list_size> is the number of elemetns in the list_scores list
   @return the itnated object
   @remarks in this call we first allcote seperate lists for 'forward' and 'backward' lsits, and then apply the heapfiyoepraito, hence obht memroy-cosntumpiton and exeuciton-tiem si assotied/boudn to this function.
**/
s_hc_heap_min_t init__s_hc_heap_min(t_float *list_scores, const uli list_size);  
//! De-allcoate the "s_hc_heap_min_t" object (oesekth, 06. nov. 2017).
void free__s_hc_heap_min_t(s_hc_heap_min_t *self);
//! Remove the miimal elemnt fromt eh heap.
void heap_pop_s_hc_heap_min(s_hc_heap_min_t *self, t_float *score_min, uli *index_min);
//! Remove elemnt at a given index. 
void remove_s_hc_heap_min(s_hc_heap_min_t *self, uli index);
//! Override a given valeu at a tivggnve postion, and then erneust that the heap is consistent.
void replace_s_hc_heap_min(s_hc_heap_min_t *self, const uli index_old, const uli index_new, const t_float val);
//! Modify the vlaeu at a gvien psotion. 
void update_s_hc_heap_min(s_hc_heap_min_t *self, const uli index, const t_float val);


#endif //! EOF
