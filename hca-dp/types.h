#ifndef types_h
#define types_h
/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of orthAgogue.
 *
 * orthAgogue is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * orthAgogue is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with orthAgogue
. If not, see <http://www.gnu.org/licenses/>.
 */
/**
  @file
  @brief Defines commonly used types in the project.
  @ingroup common
  @author Ole Kristian Ekseth (oekseth)
  @date 21.12.2010 by Ole Kristian Ekseth (init)
  @date 28.12.2011 by oekseth (clean-up)
 */
/**  @ingroup common **/
/** \{ **/
#include "libs.h"
#include "macros.h"
//! Depricated, but some methods still uses it holding the memmory address.
typedef long int mem_loc; 

#include "types_base.h"

//#ifdef __cplusplus
//! A synonym to the c-standard-macro.
static const lowint LOWINT_MAX = USHRT_MAX;
//#endif
//typedef unsigned short int overlap_t; //! Defines the maximum size of the overlap.


#include <stdlib.h>  /* The standard C libraries */


#ifdef __cplusplus
#include <map>
#include <algorithm> 
#include<string.h>
#include <cstring>
#include <string>
using namespace std;
//#include <wchar.h>

typedef std::pair<std::string, uint> string_number_pair_t;
struct key_comparer
{
  bool operator()(std::string a, std::string b) const
  {
    return strcasecmp(a.c_str(), b.c_str()) < 0;
  }
};
#endif

/** \} **/

#endif
