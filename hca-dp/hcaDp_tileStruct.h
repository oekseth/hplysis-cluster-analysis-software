#ifndef hcaDp_tileStruct_h
#define hcaDp_tileStruct_h


#include "libs.h"
#include "def_typeOf_float.h"
#include "def_memAlloc.h"

/**
   @struct 
   @brief describe a given pair of vertices. 
 **/
typedef struct s_hcaDp_pair {
  //! The 'locaiton' in the input-matrix.
  uint row_id; uint col_id;
  t_float score;
} s_hcaDp_pair_t;

//! @reutnr an empty veriosn of "s_hcaDp_pair_t"
#define setToEmpty__s_hcaDp_pair_t() ({s_hcaDp_pair_t self; self.row_id = UINT_MAX; self.col_id = UINT_MAX; self.score = T_FLOAT_MAX; self;})

// FIXME: ... write a list-wrapper for the "s_hcaDp_pair_t" structure ....  or altenrtively used a 'fixed upper size'
// FIXME: ... 

/**
   @struct 
   @brief identify the proeprteis stored for each tile (oekseth, 06. nov. 2017).
 **/
typedef struct s_hcaDp_tileStruct {
  //! The 'locaiton' in the input-matrix.
  s_hcaDp_pair_t best_pair; 
  //! Properties wrt. closest pair.
  //t_float best_score;
  // uint best_score_index_x;   uint best_score_index_y;
  //! 
  //! The update-seuence needs to start when "best_score" changes (ie, when either "best_score_index_x" or "best_score_index_y" are ...??.... 
  // FIXME: ... 
  // FIXME: ... the size of the 'pairs to update' mya be large (ie, $n^2 |tiles(x)|*|tiles(y)$) .... to address this issue .... 
  // FIXME: ... to simply dealy update of a tile is (for many use-cases) pointless .... ie, as the tasks may 'anywho be need to permed ... which is due to the proerpty of ....??.... --TODO: ivneistgat ehte latter asusmption ... which if wrong implies that a large number of updates in HCA-caclucaitonis are wasted/redundant .... an observaiton used to ....??.... 
  // FIXME: ... try idneitficaing cases which may never need to be evaluated .... ie, prudning .... eg, wrt. using K-D-tree as a trheshold wrt. vertices to insert in the "pairsTo_update" list. 
  // FIXME: ... 
  // FIXME: ... 
  // FIXME: ... compare [ªbofe] to ... "s_dense_closestPair.h" ("hca-memory/src/old") ... 
  // FIXME: ... compare [ªbofe] to ... 
  // FIXME: ... 
  s_hcaDp_pair_t *pairsTo_update; uint pairsTo_update_size; uint pairsTo_update_currentPos; //! where latter variable 'avodi' the need to enlarge the list for every inseriton.
} s_hcaDp_tileStruct_t;

//! Intiate the 2d-lsit of amtrix:
#define MiF__allocList__2d_s_hcaDp_tileStruct_t(mat_rows, mat_cols) ({s_hcaDp_tileStruct_t obj_empty = setToEmpty__s_hcaDp_tileStruct_t(); s_hcaDp_tileStruct_t **mat = NULL;  \
      mat = alloc_generic_type_2d__wrapper(s_hcaDp_tileStruct_t, mat_rows, mat_cols, obj_empty); assert(mat); mat;}) //! ie, return the allcoated matrix.
#define MiF__free__2d_s_hcaDp_tileStruct_t(mat, mat_size) ({free_generic_type_2d__wrapper(s_hcaDp_tileStruct_t, mat, mat_size); }) //! ie, return the allcoated matrix.


//! Intiate the 1d-lsit of amtrix:
#define MiF__allocList__1d_s_hcaDp_tileStruct_t(list_size) ({s_hcaDp_tileStruct_t obj_empty = setToEmpty__s_hcaDp_tileStruct_t(); s_hcaDp_tileStruct_t *list = NULL;  \
      list = alloc_generic_type_1d(s_hcaDp_tileStruct_t, list, list_size, obj_empty); assert(list); list;}) //! ie, return the allcoated listrix.
#define MiF__free__1d_s_hcaDp_tileStruct_t(list) ({free_generic_type_1d(list); }) //! ie, return the allcoated matrix.

//s_hcaDp_tileStruct_t init__s_hcaDp_tileStruct_t() 
//! @reutnr a n itnation object of type "s_hcaDp_tileStruct_t"
s_hcaDp_tileStruct_t setToEmpty__s_hcaDp_tileStruct_t();

/**
   @enum
   @brief describes the set of supproted searhc-strategyes.
 **/
typedef enum e_hcaDp_matrix_strategy {
  e_hcaDp_matrix_strategy_tile,
  //! Support: remembering of best rows, thereby reduicng number of tiels which need to be ivnestigated 
  e_hcaDp_matrix_strategy_tile_andMinMax_row,
  //! Support: dealyed update:  first investigate tile if .... 
  //e_hcaDp_matrix_strategy_tile_andMinMax_row,
  //! Support: 
  e_hcaDp_matrix_strategy_undef
} e_hcaDp_matrix_strategy_t;

/**
   @enum 
   @brief provide support for different symemtric proerpteis wrt. the matrix, ie, where some adjcency-amtrices (though Not neccesary all) provides/has a symemtric data-proerpty ... 
 **/
typedef enum e_hcaDp_matrix_symmetry {
  e_hcaDp_matrix_symmetry_rowTile_lessThan_colTile, //! ie, to use 'lower triangular-half' of the matrix. 
  e_hcaDp_matrix_symmetry_rowTile_gtThan_colTile,//! ie, to use 'upper triangular-half' of the matrix. 
  e_hcaDp_matrix_symmetry_undef //! ie, do Not apply symemtric-prop during comtpatuiosn. 
} e_hcaDp_matrix_symmetry_t;

/**
   @struct 
   @brief identify the proeprteis stored for each tile (oekseth, 06. nov. 2017).
 **/
typedef struct s_hcaDp_matrix {
  //! 
  //! 
  //! Note: to reduce meomory-consumption and exeuction-time we provide containers for "list_tiles_row" and "list_tiles_col"
  s_hcaDp_tileStruct_t *list_tiles_row; uint list_tiles_row_size;
  s_hcaDp_tileStruct_t *list_tiles_col; uint list_tiles_col_size;
  //! Hold summary-data fro each element: 
  s_hcaDp_tileStruct_t **list_tiles_matrix; uint list_tiles_matrix_nrow;
  uint cnt_eachBucket;
  
  // FIXME: cosnider provdiing support for sparse-data-sets .... eg, by assuming correctness of 'splitting' the celsl into equally-squared tiles (where each tile store a sparse lsit of entites) .... a wekaness of the latter appraoch cosenrns how it ....??..
  t_float **mat_input; uint nrows; uint ncols; uint nrows_input;
  e_hcaDp_matrix_strategy_t alg;
  e_hcaDp_matrix_symmetry_t type_symmetric;

  //!
  //! The globally best scores: 
  s_hcaDp_pair_t globalBest_pair; 
  //t_float globalBest_score;
  
  // FIXME: how to support "PAL" clustering ... where challenge is that the score may both be increasing AND decreasing ... eg, for the cases where count=[1, 2]: score=[0, 4] => (4/3)=1.3 (increase ... and de-crease if inverted) .... however, the score may never ...??..
  // FIXME: write a simple tut based on the "PAL" cluster-exemple .... if-loops ... initate update of a tile if: (a) score is less ... OR (b) updated row along criitcal axis .... 

  // FIXME: new-alg ... where we take a feature-matrix as niput instead of an 'ordianry' matrix ... ie, simliar to the HCA-algorithm of "pairwise centroid cluster" (pcl)

  // FIXME: 
} s_hcaDp_matrix_t;

//! A function to intiate the s_hcaDp_matrix_t object.
s_hcaDp_matrix_t s_hcaDp_matrix_t_init(const uint nrows, const uint ncols, const uint cnt_bucketsEachRows, t_float **distmatrix, e_hcaDp_matrix_strategy_t alg, e_hcaDp_matrix_symmetry_t type_symmetric);
//! De-allcoate the locally reserved memory.
void s_hcaDp_matrix_t_free(s_hcaDp_matrix_t *self);

//! Idneitfy the min-element and then remvoes it (hence udpatign the structure) (oesketh, 06. nov. 2017).
t_float popMin__s_hcaDp_matrix_t_free(s_hcaDp_matrix_t *self, uint *row_id, uint *col_id);

//! Redcues the nubmer of row-counts in the data-set. 
void decrementRowCounter__s_hcaDp_matrix_t(s_hcaDp_matrix_t *self);

static void test_case() {
#if(0 == 1)
    uint is = 1;     uint js = 0;
    //! Idenitfy the shortest distance between two vertices:
    result[nelements-n].distance = __local__find_minPair(nrows, ncols, distmatrix, &is, &js); //! ie, identify the pair which has the lowest weight.
    /* Save result */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];


    for(uint j = 0; j < js; j++) { //! ie, [js][0...js] , which for all js>0 will result in updates.
      distmatrix[js][j] = distmatrix[is][j]*number[is] 
                        + distmatrix[js][j]*number[js];
      distmatrix[js][j] /= sum;

      //! 
      //! Update the score for the iven vertex: 
      assert(false); // FIXME: add support for [below]:
      MF_hcaOp__updateScore_atCell(j, js); //, (distmatrix[js][j]));
    }


    //! Note: in [below] the 'iteration-threshold is set to "is":
    for(uint j = js+1; j < is; j++) { //! ie, [js...is][js] , which implies that condtion (js < is) will not be evalauted/applied/updated.
      distmatrix[j][js] = distmatrix[is][j]*number[is]
                        + distmatrix[j][js]*number[js];
      distmatrix[j][js] /= sum;

      //! 
      //! Update the score for the iven vertex: 
      assert(false); // FIXME: add support for [below]:
      MF_hcaOp__updateScore_atCell(j, js); //, (distmatrix[j][js]));
    }
    //! Note: in [below] the 'iteration-threshold is set to "n":
    for(uint j = is+1; j < n; j++) { //! ie, [is...n][js] , which implies that 
      distmatrix[j][js] = distmatrix[j][is]*number[is]
                        + distmatrix[j][js]*number[js];
      distmatrix[j][js] /= sum;


      //! 
      //! Update the score for the iven vertex: 
      assert(false); // FIXME: add support for [below]:
      MF_hcaOp__updateScore_atCell(j, js); //, (distmatrix[j][js]));
    }

    //! Copy:
    for(uint j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
    for(uint j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];



    //! 
    //! Update the score for the iven vertex: 
    assert(false); // FIXME: add support for [below]:
    MF_hcaOp__updateScore_atRow(is, /*row-size=*/is); 


    //! 
    //! Update the score for the iven vertex: 
    assert(false); // FIXME: add support for [below]:
    MF_hcaOp__updateScore_atColumn(is, /*column-start=*/(is+1), /*column-end=*/(n-1)); 

#endif
}

//! Update the shortest-paht-positions wrt. an updated row
void s_hcaDp_matrix_t_updateAt_row(s_hcaDp_matrix_t *self, const uint row_id, const uint column_startPos, const uint column_endPosPlussOne, const uint row_endPosPlussOne); 
/**
   @brief Update the shortest-paht-positions wrt. an updated column
   @remarks procedure:
   -- each row-tile: Update the min-bucket-tiles for each row: we expect one signualr valeus at each row to be updated: if a 'row-assicated tile' 'is at the best row-tile-index and has changed', then we need to update all values 'for the given tile'.
   -- the global tile: iterate through the best-performing tiles and update.

**/
void s_hcaDp_matrix_t_updateAt_column(s_hcaDp_matrix_t *self, const uint column_pos, const uint row_startPos, const uint row_endPosPlussOne, const uint column_endPosPlussOne, const t_float *arrOf_previouslyUsedValues);

//! @return the closest pair, ie, update scalar_index1, scalar_index2
t_float s_hcaDp_matrix_t_getClosestPair(s_hcaDp_matrix_t *self, uint *scalar_index1, uint *scalar_index2, const uint nrows_threshold, const uint ncols_threshold);

//! Correctness-tests and performance-tests for our "s_hcaDp_matrix_t" ANSI C class (for idneitfying shortest paths).
void assert_s_hcaDp_matrix_t();


#endif //! EOF
