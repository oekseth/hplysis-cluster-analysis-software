#include "hcaDp_tileStruct.h"
#include "macros.h"

// #define __MiF__rowIsInteresting
//! Reset the taile score.
#define __MiF__resetTileScore(self) ({self.best_pair = setToEmpty__s_hcaDp_pair_t();  })
//! Describe short-cuts: 
#define cellScore self->mat_input[row_id][col_id]
#define tile self->list_tiles_matrix[bucket_row][bucket_col] //! which is used to simplfiy coding and eace work o compiler
#define tile_row self->list_tiles_row[bucket_row]

#define __MiF__ignoreTile() ({ bool isTo_ignore = false;	\
      if(self->type_symmetric == e_hcaDp_matrix_symmetry_rowTile_lessThan_colTile) {isTo_ignore = (bucket_row > bucket_col);} \
      if(self->type_symmetric == e_hcaDp_matrix_symmetry_rowTile_gtThan_colTile)   {isTo_ignore = (bucket_row < bucket_col);} \
      isTo_ignore;}) //! ie, return.

#define __MiF__scoreIsWorse(s1, s2) (s1 > s2) //! where latter is used as a generion-funciton, eg, to simplfiy handling of a 'max-proerpty' where T_FLOAT_MAx then needs to be considered/eluated.
//! 
//! Update the best-tile-property for "tile": 
#define __MiF__setTile_fromMatrix__forCell(row_id, col_id) ({ \
      assert(bucket_row < self->list_tiles_matrix_nrow);       \
      assert(bucket_col < self->list_tiles_matrix_nrow);		\
      /* printf("bucket[%u][%u], at %s:%d\n", bucket_row, bucket_col, __FILE__, __LINE__);*/ \
      if(__MiF__scoreIsWorse(tile.best_pair.score,  cellScore)) { /*! then an imrpvoed score is idnetifed: */ \
	tile.best_pair.score = cellScore;				\
	/*! Update indexes: */						\
	/* printf("bcuket(%u)(%u)\t %f , for [%u][%u], at %s:%d\n", bucket_row, bucket_col, tile.best_score,  row_id, col_id, __FILE__, __LINE__); */ \
	tile.best_pair.row_id = row_id; tile.best_pair.col_id = col_id; \
      } else {								\
	/* printf("bcuket(%u)(%u)\t %f < %f, for [%u][%u], at %s:%d\n", bucket_row, bucket_col, tile.best_score,  cellScore, row_id, col_id, __FILE__, __LINE__); */ \
      } })

    //! Idneitfy the best score for the givne sub-matrix: 
#define __MiF__setTile_fromMatrix__forTile() ({ \
  for(uint row_id = row_start; row_id < row_end; row_id++) { \
  for(uint col_id = col_start; col_id < col_end; col_id++) { \
    __MiF__setTile_fromMatrix__forCell(row_id, col_id); } } })

//! Idneitfy te best row-tile and 'global tile'.
#define __MiF__setBest_rowAndGlobal() ({ \
      for(uint bucket_row = 0; bucket_row < self->list_tiles_matrix_nrow; bucket_row++) { \
	for(uint bucket_col = 0; bucket_col < self->list_tiles_matrix_nrow; bucket_col++) { \
	  const t_float score_tile = tile_row.best_pair.score; \
	  if(__MiF__scoreIsWorse(score_tile,  tile.best_pair.score)) { \
	    /*! Then udpate the best score */			       \
	    tile_row.best_pair  = tile.best_pair; \
	  } \
	} \
	/*! Investigate global: */ \
	if(self->globalBest_pair.score > tile_row.best_pair.score) { \
	  /*! Then udpate the best score: */ \
	  self->globalBest_pair  = tile_row.best_pair; \
	} }})

      //! Idneitfy te best  'global tile'.
#define __MiF__setBest_global() ({ \
      for(uint bucket_row = 0; bucket_row < self->list_tiles_matrix_nrow; bucket_row++) { \
	for(uint bucket_col = 0; bucket_col < self->list_tiles_matrix_nrow; bucket_col++) { \
	  const t_float score_tile = self->globalBest_pair.score; \
	  if(__MiF__scoreIsWorse(score_tile,  tile.best_pair.score)) { \
	    /*! Then udpate the best score */  \
	    self->globalBest_pair  = tile.best_pair; \
	  } } } } )

//s_hcaDp_tileStruct_t init__s_hcaDp_tileStruct_t() 
//! @reutnr a n itnation object of type "s_hcaDp_tileStruct_t"
s_hcaDp_tileStruct_t setToEmpty__s_hcaDp_tileStruct_t() {
  s_hcaDp_tileStruct_t self;
  __MiF__resetTileScore(self); //! ie, Reset the taile score.
  self.pairsTo_update = NULL;
  self.pairsTo_update_size = 0;
  self.pairsTo_update_currentPos = 0;
  /* self.best_score_index_x = UINT_MAX; */
  /* self.best_score_index_x = UINT_MAX; */
  //
  return self;
}

#define __MiF__getBucketId(row_id, cnt_eachBucket, cnt_buckets) ( { const uint bucket_id = (row_id > 0) ? (uint)((t_float)row_id / (t_float)cnt_eachBucket) : 0; bucket_id;}) //macro_min(bucket_id, (cnt_buckets - 1)); } )

//! A function to intiate the s_hcaDp_matrix_t object.
s_hcaDp_matrix_t s_hcaDp_matrix_t_init(const uint nrows, const uint ncols, const uint cnt_bucketsEachRows, t_float **distmatrix, e_hcaDp_matrix_strategy_t alg, e_hcaDp_matrix_symmetry_t type_symmetric) {
  if(alg == e_hcaDp_matrix_strategy_undef) {alg = e_hcaDp_matrix_strategy_tile;}
  s_hcaDp_matrix_t self_base;
  s_hcaDp_matrix_t *self = &self_base;
  self->list_tiles_row = NULL;
  self->list_tiles_row_size = 0;
  self->list_tiles_col = NULL;
  self->list_tiles_col_size = 0;
  self->list_tiles_matrix = NULL;
  self->list_tiles_matrix_nrow = cnt_bucketsEachRows;
  if( (self->list_tiles_matrix_nrow == 0)  || (self->list_tiles_matrix_nrow > nrows) ) {
    self->list_tiles_matrix_nrow = nrows / 10;
    if( (self->list_tiles_matrix_nrow == 0)  || (self->list_tiles_matrix_nrow > nrows) ) {
      self->list_tiles_matrix_nrow = 4;; //! ie, defualt asusmption
    }
  }
  self->cnt_eachBucket = UINT_MAX;
  //!
  self->mat_input = distmatrix;
  self->nrows = nrows;  self->nrows_input = nrows;
  self->ncols = ncols;
  //! 
  self->alg = alg;
  self->type_symmetric = type_symmetric;
  //!
  //!
  self->globalBest_pair = setToEmpty__s_hcaDp_pair_t();
  //self->globalBest_score = T_FLOAT_MAX;
  //!
  //!
  if(self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) { //! then we allocate for 'rwo':
    //!
    self->list_tiles_row = MiF__allocList__1d_s_hcaDp_tileStruct_t((self->list_tiles_matrix_nrow)); //! ie, the number of tiels whciht the rwos are spread along in the x-axis. 
    //printf("cnt-tiles=%u, at %s:%d\n", self->list_tiles_matrix_nrow, __FILE__, __LINE__);
  }

  if( (self->alg == e_hcaDp_matrix_strategy_tile)  ||
      (self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) 
      )
    {
    //!
    self->list_tiles_matrix = MiF__allocList__2d_s_hcaDp_tileStruct_t(
								     (self->list_tiles_matrix_nrow), 
								     (self->list_tiles_matrix_nrow));
    //printf("cnt-buekcetS=%u, at %s:%d\n", self->list_tiles_matrix_nrow, __FILE__, __LINE__);
    uint cnt_eachBucket = (self->nrows / self->list_tiles_matrix_nrow + 1);
    if( (cnt_eachBucket * self->list_tiles_matrix_nrow) > nrows) {
      cnt_eachBucket--;
    }
    assert(cnt_eachBucket < self->nrows);
    assert(cnt_eachBucket != 0);
    self->cnt_eachBucket = cnt_eachBucket;
    //! 
    //! idnetify the min--max scores in each matrix. 
    uint bucket_row = 0; uint bucket_row_cnt = 0;
    for(uint row_id = 0; row_id < nrows; row_id++) {
      if(bucket_row_cnt >= cnt_eachBucket) {	  
	bucket_row_cnt = 0;  	bucket_row++;
	assert(bucket_row < self->list_tiles_matrix_nrow);
	//printf("\t\t at %s:%d\n", __FILE__, __LINE__);
      } bucket_row_cnt++;
      uint bucket_col = 0; uint bucket_col_cnt = 0;



      assert(row_id >= (bucket_row * cnt_eachBucket));
      assert(bucket_row == __MiF__getBucketId(row_id, cnt_eachBucket, (self->list_tiles_matrix_nrow)));
#if( 0 == 1)
      printf("[%u] w/bucket(%u), (row_id/cnt_eachBucket)=%u, bucket(each)=%u, at %s:%d\n",
	     row_id,
	     bucket_row, 
	     __MiF__getBucketId(row_id, cnt_eachBucket, (self->list_tiles_matrix_nrow)), 
	     cnt_eachBucket,
	     __FILE__, __LINE__);
#endif
      // for(uint col_id = 0; col_id < (nrows - (1 + row_id)); col_id++) {
      for(uint col_id = 0; (col_id < nrows) && !__MiF__ignoreTile(); col_id++) { 
	//! Ivnestigate bucket-membership: 
	if(bucket_col_cnt >= cnt_eachBucket) {	bucket_col++;  bucket_col_cnt = 0;  }
	assert(bucket_row < self->list_tiles_matrix_nrow); assert(bucket_col < self->list_tiles_matrix_nrow);
	//! The range were we expect each vertex to be inside:
	assert(col_id >= (bucket_col * cnt_eachBucket));
	assert(bucket_col == __MiF__getBucketId(col_id, cnt_eachBucket, (self->list_tiles_matrix_nrow)));
	/* printf("row(%u)(%u)\t bucket[%u][%u], at %s:%d\n", row_id, col_id, bucket_row, bucket_col, __FILE__, __LINE__);  */
	//! 
	//! Update the best-tile-property for "tile": 
	__MiF__setTile_fromMatrix__forCell(row_id, col_id);
	//!
	bucket_col_cnt++;
      }
    }
    // printf("cnt-tiles=%u, at %s:%d\n", self->list_tiles_matrix_nrow, __FILE__, __LINE__);
    if(self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) { //! then we allocate for 'rwo':
      //!
      //! Set the glboally best scores:
      __MiF__setBest_rowAndGlobal();  
    } else { //! Idneitfy te best  'global tile'.
      __MiF__setBest_global();  
    }

#ifndef NDEBUG
    //! 
    //! Then validte correctness of the [above] intiatonaion-function: 
    { //! Iterate through the buckets:
      for(uint bucket_row = 0; bucket_row < self->list_tiles_matrix_nrow; bucket_row++) {
	for(uint bucket_col = 0; (bucket_col < self->list_tiles_matrix_nrow) && !__MiF__ignoreTile(); bucket_col++) {
	  //#define tile self->list_tiles_matrix[bucket_row][bucket_col] //! which is used to simplfiy coding and eace work o compiler
	  //! 
	  //! Get range of coordinates, and validte:
	  const uint row_start = bucket_row * cnt_eachBucket;
	  const uint col_start = bucket_col * cnt_eachBucket;
	  // printf("rows=[%u, %u], at %s:%d\n", row_start, col_start, __FILE__, __LINE__);
	  assert(row_start < self->nrows);
	  assert(col_start < self->nrows);
	  assert(bucket_row == __MiF__getBucketId(row_start, cnt_eachBucket, (self->list_tiles_matrix_nrow)));
	  assert(bucket_col == __MiF__getBucketId(col_start, cnt_eachBucket, (self->list_tiles_matrix_nrow)));
	  assert(bucket_row < self->list_tiles_matrix_nrow);
	  assert(bucket_col < self->list_tiles_matrix_nrow);
	  const uint row_end = macro_min((row_start + cnt_eachBucket), nrows);
	  const uint col_end = macro_min((col_start + cnt_eachBucket), ncols);	  
	  { //! Validte taht the row-range is in the same bucket-range: 
	    if(row_end > 1) {
	      const uint row_endMinusOne = row_end - 1;
	      const uint bucket_end_row = __MiF__getBucketId(row_endMinusOne, (self->cnt_eachBucket), (self->list_tiles_matrix_nrow));
	      //	      const uint bucket_end_row = __MiF__getBucketId((row_end-1), cnt_bucketsEachRows);
	      // printf("compare rows=([%u, %u] in buckets=(%u, %u), cnt_eachBucket=%u, at %s:%d\n", row_start, row_endMinusOne, bucket_row, bucket_end_row, cnt_eachBucket, __FILE__, __LINE__);
	      assert(bucket_end_row == bucket_row);
	    }
	    if(col_end > 1) {
	      const uint col_endMinusOne = col_end - 1;
	      uint bucket_end_col = __MiF__getBucketId(col_endMinusOne, (self->cnt_eachBucket), (self->list_tiles_matrix_nrow));
	      //if(bucket_end_col
	      // printf("compare cols=([%u, %u] in buckets=(%u, %u), cnt_eachBucket=%u, at %s:%d\n", col_start, col_endMinusOne, bucket_col, bucket_end_col, cnt_eachBucket, __FILE__, __LINE__);
	      assert(bucket_end_col < self->list_tiles_matrix_nrow);
	      assert(bucket_end_col == bucket_col);
	    }
	  }

	  //! 
	  //! validate the min-idnex is acutally the min-index: 
	  // t_float score_min = T_FLOAT_MAX;
	  bool minIndex_isFound = false;
	  for(uint row_id = row_start; row_id < row_end; row_id++) {
	    for(uint col_id = col_start; col_id < col_end; col_id++) {
	      //const t_float score = self->mat_input[row_id][col_id];	      
	      // printf("bcuket(%u)(%u)\t %f <= %f=[%u][%u], at %s:%d\n", bucket_row, bucket_col, tile.best_score, score, row_id, col_id, __FILE__, __LINE__);
	      assert(tile.best_pair.score <= cellScore);
	      if(tile.best_pair.score == cellScore) {
		if( 
		   (tile.best_pair.row_id == row_id) && 
		   (tile.best_pair.col_id == col_id) ) {minIndex_isFound = true;} //! ie, as there may be muliple indces with the same score.
	      }
	    }
	  }
	  //! Comapre: 
	  assert(minIndex_isFound == true);

	  //! 
	  //! Validte taht the glboal-min is correctly set: 
	  assert(self->globalBest_pair.score <= tile.best_pair.score);
	  //! 
	  //! Validte taht the glboal-max is correctly set: 
	  //! Note: if min(row(1)) + min(row(2)) > max(..) of remaining rows, then the columsn does Not need to investigated:
	  // FIXME: evlauate the beneift of this heuristc ... which is used when ivnestigating rows and columns.

	  //! 
	  //! Validte taht an udpate is Not appleid, ie, as the isnerted elemtn is above the max-trheshold fior all isnerted vertices:


	}
      }
    }
#endif
  } else {
    assert(false); //! ie, as we then need adding supprot for this.
  }

  //!
  //! @return
  return self_base;
}

//! De-allcoate the locally reserved memory.
void s_hcaDp_matrix_t_free(s_hcaDp_matrix_t *self) {
  assert(self);
  if(self->list_tiles_matrix) {
    MiF__free__2d_s_hcaDp_tileStruct_t(&(self->list_tiles_matrix), self->nrows);
    self->list_tiles_matrix = NULL;
  }
  if(self->list_tiles_row) {
    MiF__free__1d_s_hcaDp_tileStruct_t(&(self->list_tiles_row));
    self->list_tiles_row = NULL;
  }

  assert(false); // FIXME: compelte .... 
}


//! Idneitfy the min-element and then remvoes it (hence udpatign the structure) (oesketh, 06. nov. 2017).
t_float popMin__s_hcaDp_matrix_t_free(s_hcaDp_matrix_t *self, uint *row_id, uint *col_id) {
  assert(self);
  //!
  //! Get facts:
  const t_float score_min = self->globalBest_pair.score;
  *row_id = self->globalBest_pair.row_id;
  *col_id = self->globalBest_pair.col_id;
  //!
  //! Then udpate the bcuket-id-structure: 
  // FIXME: ensure that we 'drop' investgating buckets when we pass a 'size-threshold' for buckets ... consider usign different sizes for the buckets ...
  uint bucket_row = __MiF__getBucketId(*row_id, self->cnt_eachBucket, (self->list_tiles_matrix_nrow));
  uint bucket_col = __MiF__getBucketId(*col_id, self->cnt_eachBucket, (self->list_tiles_matrix_nrow));
  { //! Use thte 'correct symmetric' tile: 
    if(self->type_symmetric == e_hcaDp_matrix_symmetry_rowTile_lessThan_colTile) {
      if(__MiF__ignoreTile()) { //! then we 'swap':
	const uint old_id = bucket_row; bucket_row = bucket_col; bucket_col = old_id;
      }
    } else if(self->type_symmetric == e_hcaDp_matrix_symmetry_rowTile_gtThan_colTile) {
      if(__MiF__ignoreTile()) { //! then we 'swap':
	const uint old_id = bucket_row; bucket_row = bucket_col; bucket_col = old_id;
      }
    }
  }
  //! --------------------------------------------------------------------------------------------------------
  //!
  //!
  { //! Investigate scores in the given tile:
    const uint cnt_eachBucket = self->cnt_eachBucket;  const uint nrows = self->nrows; const uint ncols = nrows;
    //! 
    //! Remvoe the vlaues from the input-matrix:
    self->mat_input[*row_id][*col_id] = T_FLOAT_MAX;
    //! 
    //! Get the row-range to investgiate: 
    const uint row_start = bucket_row * cnt_eachBucket;
    const uint col_start = bucket_col * cnt_eachBucket;	  
    //uint row_start = __MiF__getRange_rowStart(bucket_row, self->cnt_eachBucket);
    uint row_end = macro_min((row_start + cnt_eachBucket), nrows);    
    uint col_end = macro_min((col_start + cnt_eachBucket), ncols);    
    //#define tile self->list_tiles_matrix[bucket_row][bucket_col] //! which is used to simplfiy coding and eace work o compiler
    //#if( 0 == 1)
    //!
    //! Intiate: 
    __MiF__resetTileScore(tile); //! ie, Reset the taile score.
    //!
    //! Idneitfy the best score for the givne sub-matrix: 
    if( (row_start < row_end) && (col_start < col_end) ) { 
      __MiF__setTile_fromMatrix__forTile();
    } //! else we assuemt eh 'row' have decreased to such an extend taht the tile is No longer of interest to investigate. 
    // #undef tile
  }
  //! --------------------------------------------------------------------------------------------------------
  //!
  //!
  if(self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) { //! then we allocate for 'rwo':
    //! Update the global-min of the data-strcutre:    
    //!
    //! Note: we need to search through all of the column-tiles to veirfy that the row-id is the best.
    tile_row.best_pair  = setToEmpty__s_hcaDp_pair_t(); //! ie, reset for each column-tile: 
    //! 
    //! The iterate through the olumn-tiles:
    for(uint bucket_col = 0; (bucket_col < self->list_tiles_matrix_nrow) && !__MiF__ignoreTile(); bucket_col++) {
      if(__MiF__scoreIsWorse(tile_row.best_pair.score,  tile.best_pair.score)) { //! then better socre:
	tile_row.best_pair  = tile.best_pair;
      }
    }
    //! Then use [ªbove] as ain itnal sugesiton wrt. 'best':
    self->globalBest_pair  = tile_row.best_pair; 
  } else {
    self->globalBest_pair  = setToEmpty__s_hcaDp_pair_t(); //! ie, as the 'best' is then Not overwritten.
  }
  //! --------------------------------------------------------------------------------------------------------
  //! 
  //! 
  //! 
  //! 
#if(1 == 1) //! Note: [below] coce-chunk is asostied with less than 0.3 per-cent of teh exeuciton-time, ie, no need to optmzie [”elow]
  //! 
  //! Reset:
  if(self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) { //! then we allocate for 'rwo':
    //! Update the global-min of the data-strcutre: 
    for(uint bucket_row = 0; bucket_row < self->list_tiles_matrix_nrow; bucket_row++) {
      //! 
      //! Investigate global:
      if(self->globalBest_pair.score > tile_row.best_pair.score) {
	//!
	//! Investigate all of the cases:	
	for(uint bucket_col = 0; (bucket_col < self->list_tiles_matrix_nrow) && !__MiF__ignoreTile(); bucket_col++) {
	  if(tile_row.best_pair.score > tile.best_pair.score) { //! then better socre:
	    tile_row.best_pair  = tile.best_pair;
	  }
	}
	//! Then udpate the best score
	//! Note: by 'def' self.globalBest_pair is to prvoide a better/improved update. 
	self->globalBest_pair  = tile_row.best_pair;
      }
    }
  } else { //! then ivnestigate all of the tiles:
    //! Update the global-min of the data-strcutre: 
    __MiF__setBest_global();
  }
#else //! then we test the time-cost of [ªbove]
  //#define tile self->list_tiles_matrix[bucket_row][bucket_col] //! which is used to simplfiy coding and eace work o compiler
  self->globalBest_pair.score = tile.best_pair.score;
  self->globalBest_pair  = tile.best_pair;  
#endif  

  //!
  //! Return:
  return score_min;
}

/**
   @brief Reduces the nubmer of row-counts in the data-set. 
   @remarks algorithm:
   -- worst(1.a): iterate through vertices d(i, k) in [last][*] and then update the min-score
   -- worst(1.b): iterate through tiles [last][*] and then update rowTile[last] with the min-score.
   -- best(2.a.1): row(last) != best(tile[last][*]), ie, no updates needed. 
   -- best(2.a.2): tile(last) otuside updated search-scope: find globalMin(rowTile[*]). 
**/
void decrementRowCounter__s_hcaDp_matrix_t(s_hcaDp_matrix_t *self) {
  assert(self); 
  if(self->nrows > 0) {
    //! 
    //! Fetch proeprteis of the evaluation-space: 
    const uint row_id = self->nrows - 1; //! ie, the last row-id, a row-id which is now removed.
    const uint bucket_row = __MiF__getBucketId(row_id, self->cnt_eachBucket, (self->list_tiles_matrix_nrow));
    const uint cnt_eachBucket = self->cnt_eachBucket;  const uint nrows = self->nrows; const uint ncols = nrows;
    const uint row_start = bucket_row * cnt_eachBucket;
    //! 
    //! 
    t_float prevScore_global = self->globalBest_pair.score; //! which is used to 'idneitfy' cases where the 'local tile' actually has received a better score
    //! 
    //!  
    bool needTo_findNewGlobalMin = false; 
    { //! Case: iterate through vertices d(i, k) in [last][*] and then update the min-score
      if( (self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) 
	  //&& (tile_row.best_pair.col_id >= ncols) 
	  ) { //! then we need to udpate the row-proerpty with a new improved' column-score. 
	if( (tile_row.best_pair.row_id >= nrows) || (tile_row.best_pair.col_id >= ncols) ) { //! then teh best-pair has changed. 
	  tile_row.best_pair = setToEmpty__s_hcaDp_pair_t(); //! ie, reset. 
	}
      }
      //! 
      //! Iterate through each of the column-tiles: 
      const t_float prevScore_tileRow = tile_row.best_pair.score;
      //! 
      //! Evaluate: best(2.a.2): tile(last) otuside updated search-scope: find globalMin(rowTile[*]). 
      if(row_start != row_id) { //! then 'this row' is Not the last elemnt int he tile, ie, decrement the number of tiles, and then udpate the global summary:

	for(uint bucket_col = 0; (bucket_col < self->list_tiles_matrix_nrow) && !__MiF__ignoreTile(); bucket_col++) { \
	  const uint col_start = bucket_col * cnt_eachBucket;
	  uint row_end = macro_min((row_start + cnt_eachBucket), nrows);    
	  uint col_end = macro_min((col_start + cnt_eachBucket), ncols);    
	  //!
	  const bool tile_isUpdated = (tile.best_pair.row_id == (self->nrows -1)); //! ie, as we then need update the tile-content. 
	  if( (tile.best_pair.row_id >= nrows) || (tile.best_pair.col_id >= ncols) ) { //! then teh best-pair has changed. 
	    tile.best_pair = setToEmpty__s_hcaDp_pair_t(); //! ie, reset. 
	  }
	  bool tileScore_hasChanged = false;
	  if(tile_isUpdated
	     //! We 'also' need to consider the case where a user has updated the score with a new value which may be an improvement ...??...
	   // FIXME: make use of [”elow] .... eg, when updating 'rows' and 'columsn' wieht new scores ... 
	     || (tile.best_pair.row_id == UINT_MAX) //! which then would indicate/signal that a given rowtile has been locally udpated (ie, for whicht eh incossnteicy precludes/motviates a new evlauaiton for/of the row-tile). 
	     ) {
	    const t_float prevScore_tile = tile.best_pair.score;
	    //! Apply:
	    __MiF__setTile_fromMatrix__forTile();
	    //! Update: 
	    tileScore_hasChanged = (prevScore_tile != tile.best_pair.score);
	  }
	  if(self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) { //! then we update for the 'best-row': 
	    if(__MiF__scoreIsWorse(tile_row.best_pair.score,  tile.best_pair.score)) { //! then better socre:
	      tile_row.best_pair  = tile.best_pair;
	    }
	  }
	  if(tileScore_hasChanged) {
	    needTo_findNewGlobalMin = true; //! ie, as the min-row then has changed. 
	  }
	}
	if(self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) { //! then we update for the 'best-row': 
	  if(prevScore_tileRow == tile_row.best_pair.score) {
	    needTo_findNewGlobalMin = false; //! ie, as the min-score may then Not be improved. 
	  }
	}
      } else {
	needTo_findNewGlobalMin = true;
      }
      //! ---------------------------------------
      if(needTo_findNewGlobalMin 
	 && (self->globalBest_pair.row_id >= row_start) //! ie, for which the 'best row-id' has then changed. 
	 && (__MiF__scoreIsWorse(self->globalBest_pair.score, prevScore_global) ) //! which impleis that the udpated score (if any) has Not improved.
	 ) { //! 
	//! 
	//! Reset the global:
	self->globalBest_pair = setToEmpty__s_hcaDp_pair_t();
	//! 
	//! Update: 
	//if(__MiF__scoreIsWorse(tile_row.best_pair.score, tile.best_pair.score)) { //! then better socre:
	if(self->alg == e_hcaDp_matrix_strategy_tile_andMinMax_row) { //! then we update for the 'best-row': 
	  for(uint bucket_row = 0; bucket_row < self->list_tiles_matrix_nrow; bucket_row++) { \
	    if(self->globalBest_pair.score > tile_row.best_pair.score) {	
	      /*! Then udpate the best score: */	       
	      self->globalBest_pair  = tile_row.best_pair;	
	    }
	  }
	} else { //! then we evlauate for all of teh tiles: 
	  __MiF__setBest_global();
	}
      }
    }
    //! ---------------------------------------
    //! ---------------------------------------



    //!
    //! Reduce counters:
    (self->nrows)--;
    if(row_start == row_id) {
      (self->list_tiles_matrix_nrow)--;
    }
    //!
    //! Reduce counters:
    (self->nrows)--;
    assert(false);
    return;
  } 
}

//!
//! Re-set short-cuts:
#undef tile
#undef tile_row
#undef cellScore
