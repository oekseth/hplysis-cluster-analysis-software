#include "hcaDp_tileStruct.h"
#include "macros.h"
#include "hc_minHeap.h"

#if( 0 == 1) 
#define MiF__pairTo_triangularIndex(r_,c_) ( ((2*nrows -3 -(r_)) * (r_)>>1) + (c_)-1)    
  //#define MiF__pairTo_triangularIndex(r_,c_) ( ((2*nrows -3 -(r_)) * (r_)>>1) + (c_)-1)    


  /* { */
  /*   // const uint nrows = 10; */
  /*   //#define D_(r_,c_) ( ((2*nrows -3 -(r_)) * (r_)>>1) + (c_)-1)     */
  /*   uint pos = 1; */
  /*   const uint n_t = (nrows*nrows); */
  /*   for(uint i = 0; i < nrows; i++) { */
  /*     printf("row[%u] = [", i); */
  /*     //for(uint k = 0; k < (nrows - (1 + i)); k++) { */
  /*     for(uint k = 0; k < nrows; k++) { */
  /* 	//if( (i != k) && (i != 0) )  */
  /* 	  { */
  /* 	  printf("%u\t", n_t % pos); */
  /* 	  pos++; */
  /* 	  //printf("%u\t",  */
  /* 	  //printf("%u\t", MiF__pairTo_triangularIndex((i+0), k)); */
  /* 	} */
  /* 	//printf("\t (2, 1)=%u, at %s:%d\n", D_(2, 1), __FILE__, __LINE__); */
  /*     } */
  /*     printf("\n"); */
  /*   } */
  /* } */
  if(false) { //! Note: in [below] we exemplfiy how ...??..
    //! The below strategy is amogn others used in the "hc_minHeap" implemtatnion to improve exeuciont-time. 
    
    lint size = 3000;
    lint index = (index=(size>>1)); //! which for n=1000 => index=500;
    for( ; index>0; ) {
      index--;
      printf("%u\t", (uint)index);
    }
    printf(" at %s:%d\n", __FILE__, __LINE__);
  }
  if(array_cnt > 1) {
    case_id = (uint)atoi(array[1]);
  }
  if(array_cnt > 2) {
    typeOps = (e_hcaOp_type_t)atoi(array[2]);
    if(typeOps >= e_hcaOp_type_undef) {
      fprintf(stderr, "!!(abort)\t no ops specifed for index=%d, at %s:%d\n", atoi(array[2]), __FILE__, __LINE__);
      return -1;
    }
  }
#endif


#if HAVE_VISIBILITY
// FIXMe[tut]:e valiate the time-difference wrt. [below] "GCC visibility" .. compile-option <-- unable to see an time-exectuion-beneift wrt. [below] otpion when eamsuring exec-time ... 
#pragma GCC visibility push(hidden)
#endif

typedef struct {int left; int right; t_float distance;} s_hcDp_node;

t_float find_closest_pair(uint n, t_float** distmatrix, uint* ip, uint* jp) { 
  uint i, j;
  t_float temp;
  t_float distance = distmatrix[1][0];
  *ip = 1;
  *jp = 0;
  for (i = 1; i < n; i++)
  { for (j = 0; j < i; j++)
    { temp = distmatrix[i][j];
      if (temp<distance)
      { distance = temp;
        *ip = i;
        *jp = j;
      }
    }
  }
  return distance;
}

//clusterid = __init_nodeList(nelements);
static s_hcDp_node *__init_nodeList(uint nelements) {
  s_hcDp_node *result = (s_hcDp_node*)malloc((nelements)*sizeof(s_hcDp_node));
  //  result = (s_hcDp_node*)malloc((nelements-1)*sizeof(s_hcDp_node));
  if (!result)
    { //free(result);
      return NULL;
    }
  for(uint i = 0; i < nelements; i++) {
    result[i].left = INT_MAX;
    result[i].right = INT_MAX;
    result[i].distance = 0; //T_FLOAT_MAX;
  }
  return result;
}



static s_hcDp_node* __pml__(int nelements, t_float** distmatrix) {
int j;
  int n;
  int* clusterid;
  s_hcDp_node* result;
  clusterid = (int*)malloc(nelements*sizeof(int));
  if(!clusterid) return NULL;
  //! Initate the result: 
  result = __init_nodeList(nelements);
  //! 
  //! 
  /* Setup a list specifying to which cluster a gene belongs */
  for (j = 0; j < nelements; j++) clusterid[j] = j;
  //! 
  //! 
  //! 
  //! 
  for (n = nelements; n > 1; n--) { 
    uint is = 1;     uint js = 0;
    result[nelements-n].distance = find_closest_pair(n, distmatrix, &is, &js);

    /* Fix the distances */
    for (j = 0; j < js; j++) //! ie, symmetric-prop. 
      distmatrix[js][j] = macro_max(distmatrix[is][j], distmatrix[js][j]);
    for (j = js+1; j < is; j++)
      distmatrix[j][js] = macro_max(distmatrix[is][j], distmatrix[j][js]);
    for (j = is+1; j < n; j++)
      distmatrix[j][js] = macro_max(distmatrix[j][is], distmatrix[j][js]);

    for (j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
    for (j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];

    /* Update clusterids */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];
    clusterid[js] = n-nelements-1;
    clusterid[is] = clusterid[n-1];
  }
  free(clusterid);

  return result;
}

static s_hcDp_node* __pml__tile(int nelements, t_float** distmatrix) {
int j;
  int n;
  int* clusterid;
  s_hcDp_node* result;
  clusterid = (int*)malloc(nelements*sizeof(int));
  if(!clusterid) return NULL;
  //! Initate the result: 
  result = __init_nodeList(nelements);
  //! 
  //! 
  /* Setup a list specifying to which cluster a gene belongs */
  for (j = 0; j < nelements; j++) clusterid[j] = j;

  const uint nrows = ncols = nelements;
  //! 
  //! 
  const t_float default_value_float = 0;
  t_float *arrOf_tmp_tranposeOld_1 = allocate_1d_list_float(/*size=*/ncols, default_value_float);
  t_float *arrOf_tmp_tranposeOld_2 = allocate_1d_list_float(/*size=*/ncols, default_value_float);

  //! 
  //! 
  //! 
  //! 
  for (n = nelements; n > 1; n--) { 
    uint is = 1;     uint js = 0;
    if(false) {
      result[nelements-n].distance = find_closest_pair(n, distmatrix, &is, &js);
      
      /* Fix the distances */
      for (j = 0; j < js; j++) //! ie, symmetric-prop. 
	distmatrix[js][j] = macro_max(distmatrix[is][j], distmatrix[js][j]);
      for (j = js+1; j < is; j++)
	distmatrix[j][js] = macro_max(distmatrix[is][j], distmatrix[j][js]);
      for (j = is+1; j < n; j++)
	distmatrix[j][js] = macro_max(distmatrix[j][is], distmatrix[j][js]);
      
      for (j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
      for (j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];
    } else {
      const uint inode = nelements-n;
      const uint local_matrixSize = n; //! ie, to simplify our writing.
      result[nelements-n].distance = find_closest_pair(n, distmatrix, &is, &js);
      assert_local(is < nrows); assert_local(js < ncols);


      const uint columnUpdate_pos_start = min(is, js) + 1; //! ie, .....??.... 
      const uint columnUpdate_pos_end   = n;
      //!
      //!
      const uint bucket_id_js = get_columnBucketId(&s_obj_closestPair, js); //! ie, to ge thte signicance of valeus in the same bucket.
      const uint bucket_id_is = get_columnBucketId(&s_obj_closestPair, is); //! ie, to ge thte signicance of valeus in the same bucket.
      
      uint arrOf_condensed_rowId_toUse_is_currPos = 0;  uint arrOf_condensed_rowId_toUse_js_currPos = 0;
      uint *mapOf_columnPos_toUse_is_local = mapOf_columnPos_toUse_is; 
      uint *arrOf_condensed_rowId_toUse_is_local = arrOf_condensed_rowId_toUse_is; 
      uint *arrOf_condensed_rowId_toUse_is_local_currPos_scalar = &arrOf_condensed_rowId_toUse_is_currPos; 
      uint cnt_alreadyInserted_forColumnAtSelfPos_is = 0; uint cnt_alreadyInserted_forColumnAtSelfPos_js = 0;
      if( (is == js)
	  || (bucket_id_is == bucket_id_js) 
	) {
	//printf(" set (is, js) indexes to be in the same tile, at %s:%d\n", __FILE__, __LINE__);
	mapOf_columnPos_toUse_is_local = mapOf_columnPos_toUse_js; //! ie, to simplify the udpate-procedure.
	arrOf_condensed_rowId_toUse_is_local = arrOf_condensed_rowId_toUse_js; //! ie, to simplify the udpate-procedure.
	arrOf_condensed_rowId_toUse_is_local_currPos_scalar = &arrOf_condensed_rowId_toUse_js_currPos; //! ie, to simplify the udpate-procedure.
	assert(arrOf_condensed_rowId_toUse_is_local_currPos_scalar == &arrOf_condensed_rowId_toUse_js_currPos);
      }

    // printf("--------- test: %f, at %s:%d\n", distmatrix[1][0], __FILE__, __LINE__);

    { 
      for(uint j = 0; j < n-1; j++) {
	// if(false) {printf("oldValue[%u][%u]=%f, at %s:%d\n", j, js, distmatrix[j][js], __FILE__, __LINE__);}
	arrOf_tmp_tranposeOld_1[j] = distmatrix[j][js];  //! which is sued in our s_dense_closestPair_updateAt_column(..)"
      }
      if(is != js) { //! then copy the old values:
	for(uint j = 0; j < n-1; j++) { 
	  if(false) {printf("oldValue[%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__);}
	  arrOf_tmp_tranposeOld_2[j] = distmatrix[j][is];
	} //! which is sued in our s_dense_closestPair_updateAt_column(..)"
      }
      // if(false) {printf("(update-matrix) for values [%u][%u....%u], at %s:%d\n", js, 0, js, __FILE__, __LINE__);}
      for(uint j = 0; j < js; j++) {
	distmatrix[js][j] = max(distmatrix[is][j], distmatrix[js][j]);
      }
      
      if(true) { // #if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	for(uint j = js+1; j < is; j++) {	  
	  if(distmatrix[j][js] < distmatrix[is][j]) {
	    
	    assert(false); // FIXME["hcaDp_main.c"::pml]: ... update only if above ...??... trheshold 
	    assert(false); // FIXME["hcaDp_main.c"::pml]: ... add supprot for this .... 
	    assert(false); // FIXME["hcaDp_main.c"::pml]: ...

	  /*   distmatrix[j][js] = distmatrix[is][j];	   */
	  /*   const t_float currentValue = distmatrix[j][js]; */
	  /*   mapOf_columnPos_toUse_js[j] = js; */
	  /*   if(js == is) {mapOf_updateTimePoint[j] = n;} //! ie, to enure that a row-id is evlauated only once. */
	  /*   if(true) { */
	  /*     const uint row_id = j;  */
	  /*     if(true) { */
	  /* 	// FIXME: validate correctness of [”elow] ... and add seperate tables for "arrOf_condensed_rowId_toUse_js" and "arrOf_condensed_rowId_toUse_js_currPos" if the tables are differnet */
	  /* 	s_dense_closestPair_t *self = &s_obj_closestPair; //! ie, to satsifiy the specific grammar of the 'ANSI C macro' sytnax; */
	  /* 	if(__macro__minValueIsImproved(self, row_id, bucket_id_js, currentValue)) { */
	  /* 	  const uint column_pos = js; */
	  /* 	  cnt_alreadyInserted_forColumnAtSelfPos_js += (uint)update_minValues_for_columnBucket_setOptimal_atColumnPos(self, row_id, bucket_id_js, column_pos, currentValue);	     */
	  /* 	} else if(__macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id_js, js)) { */
	  /* 	  arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest. */
	  /* 	} //! else we assuem the column is not of interest. */
	  /*     } else { */
	  /* 	arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest. */
	  /*     } */
	  /*   } */
	  /* } else { */
	  /*   mapOf_columnPos_toUse_js[j] = UINT_MAX; //! ie, as the valeu then did not imrpvoe. */
	  }
	}	
      } else {
	for(uint j = js+1; j < is; j++) {
	  distmatrix[j][js] = max(distmatrix[is][j], distmatrix[j][js]);
	}
      }
      //printf("--------- test: %f, at %s:%d\n", distmatrix[1][0], __FILE__, __LINE__);
      //! Note: in [below] if "js > is", then the 'updated rows' may change <-- could we 'move this' before the row-update <-- consider using a new amcro-arpam 'to thest time correctess-impact'. <-- seems like the  "js !> is" is an example where the 'sorted proepty' may/will not hold.


      //#if(config_reWrite_mainHcaLoop_notUpdatePointless_memoryCells == 1) 
      if(is != js) //! otehrwise the valeus will anyghow be overwritten
	//#endif      
	{
	  // FIXME: update [”elow] wr.t 'case(1)' in our SP-optmizaiton.
	  if(true) { // #if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	    for(uint j = is+1; j < n; j++) {
	      if(distmatrix[j][js] < distmatrix[j][is]) {
		distmatrix[j][js] = distmatrix[j][is];	  
		const t_float currentValue = distmatrix[j][js];
		mapOf_columnPos_toUse_js[j] = js;
		if(js == is) {mapOf_updateTimePoint[j] = n;} //! ie, to enure that a row-id is evlauated only once.
		if(true) { // #if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
		  const uint row_id = j; 
		  if(true) { // #if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal == 1)
		    // FIXME: validate correctness of [”elow] ... and add seperate tables for "arrOf_condensed_rowId_toUse_js" and "arrOf_condensed_rowId_toUse_js_currPos" if the tables are differnet
		    s_dense_closestPair_t *self = &s_obj_closestPair; //! ie, to satsifiy the specific grammar of the 'ANSI C macro' sytnax;
		    if(__macro__minValueIsImproved(self, row_id, bucket_id_js, currentValue)) {
		      const uint column_pos = js;
		      cnt_alreadyInserted_forColumnAtSelfPos_js += (uint)update_minValues_for_columnBucket_setOptimal_atColumnPos(self, row_id, bucket_id_js, column_pos, currentValue);	      
		    } else if(__macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id_js, js)) {
		      arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest.
		    } //! else we assuem the column is not of interest.
		    /* if(__macro_isTo_updateColumn_forBucketTile(self, row_id, bucket_id_js, js)) { */
		    /* 	arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest. */
		    /* } //! else we assuem the column is not of interest. */
		  } else {
		    arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest.
		  }
		}
	      } else {
		mapOf_columnPos_toUse_js[j] = UINT_MAX; //! ie, as the valeu then did not imrpvoe.
	      }
	    }
	  } else {
	    for(uint j = is+1; j < n; j++) {
	      distmatrix[j][js] = max(distmatrix[j][is], distmatrix[j][js]);
	      // printf("[row=%u][%u]=%f, and where local_matrixSize=%u, at %s:%d\n", j, js, distmatrix[j][js], local_matrixSize, __FILE__, __LINE__);
	    }
	  }
	}
      //#if(config_reWrite_mainHcaLoop_notUpdatePointless_memoryCells == 1) 
      else { //! then we anyhow need to update for the last value
	// TODO: consider dropping this, ie, as the 'last valeu' is not expected to be used.
	//! Note: [”elow] operation is expected to have an ins-igficnat time-cost.
	const uint j = n-1;
	distmatrix[j][js] = max(distmatrix[j][is], distmatrix[j][js]);
      }    
      //! Copy:
      // FIXME: wrt. [”elow] cosnider to include a 'copy' funciton in our "s_dense_closestPair(..)" <-- may reduce the time-cost by a factor of "|rows|*|rows|" <-- experiemtn witht eh latter .... make use of 'itnernal cegbug-aprams to hyptoese the exueciton-tiem-effect ... write correctness-tests ... using a macro-apram ... and then updte our article.
      // if(false) {printf("(update-matrix) for values [%u][%u....%u], at %s:%d\n", is, 0, is, __FILE__, __LINE__);}
      // FIXME: include [”elow]


      const uint endPos_rowUpdate_is = is;
#if(config_reWrite_mainHcaLoop_useMemCpy == 1)      
      if(is > 0) {
	if(is != (n-1)) {
	  const uint j = 0;
	  memcpy(&distmatrix[is][j], &distmatrix[n-1][j], sizeof(t_float)*endPos_rowUpdate_is);
	}
      } else 
#endif
	{
	  for(uint j = 0; j < endPos_rowUpdate_is; j++) { 
	    distmatrix[is][j] = distmatrix[n-1][j]; 
	    // printf("[row_id=%u][%u]=%f, at %s:%d\n", is, j, distmatrix[is][j], __FILE__, __LINE__);
	  }
	}
    

      // FIXME: update [”elow] wr.t 'case(2)' in our SP-optmizaiton.
      const uint endPos_coluUpdate_is = n-1;
      const uint n_minusOne = n-1;
      // FIXME: valdiate that [”elow] if-caluse is reflected in ª[bveo] wrt. the "distmatrix" udpates
      //if(n_minusOne != is) 

      // FIXME: in [”elow] only iterate through each 'column-bukcet' wrt. "distmatrix[n_minusOne][j]" ... and compare each bucket to the to-be-added row-buckets .. eg, wrt. "[1...100][1...100]"
      for(uint j = is+1; j < endPos_coluUpdate_is; j++) {
	//distmatrix[j][is] = 1;
	
	// printf("odl-value=%f, at %s:%d\n", distmatrix[j][is], __FILE__, __LINE__);

	distmatrix[j][is] = distmatrix[n_minusOne][j];
	const t_float currentValue = distmatrix[j][is];

	// printf("(update)\t [%u][%u]=%f, js=[%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], js, distmatrix[j][js], __FILE__, __LINE__);

#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	// FIXME: validate correctness of [”elow] if-calsue:
	if((is != js) || (distmatrix[j][is] <= distmatrix[j][js]) ) {
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1)
	  //printf("\t (update)\t [%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__);
	  
	  //! Note: our "mapOf_updateTimePoint" is used to handle the case wrt. mulipel updates: we use a 'boolean exailiy-table' where the [j] = 'time-point in euxeicont' (eg, "n") ... ie, to 'seprate' [”elow] from earlier udpates ... without the need for a 'compelte re-iteration'.

	  // assert(false); // FIXME: thereafter update [below] .... seems like [below] does not proeprtly 'handle' the case where 'the udpated valie was the best chosie, though no longer is'.

	  //! Investigate 
	  //if(true) {
	  if( (is != js) || (mapOf_updateTimePoint[j] != n) ) { //! then we assuem that "j" has nto been already marked as 'of interest'.
	    //printf("(update---)\t [%u][%u]=%f, and index-optmal-at-bucket=%u, at %s:%d\n", j, is, distmatrix[j][is], (uint)s_obj_closestPair.matrixOf_locallyTiled_minPairs_columnPos[is][bucket_id_is], __FILE__, __LINE__);
	    // FIXME: ... below may results in non-order wrt. row-ids ... try to use a differnet aprpaoch than [”elow]
	    const uint row_id = j; 	    
#if(config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns_inMainLoop_testIfColumnIsOptimal == 1)
	    // FIXME: validate correctness of [”elow] ... and add seperate tables for "arrOf_condensed_rowId_toUse_js" and "arrOf_condensed_rowId_toUse_js_currPos" if the tables are differnet
	    s_dense_closestPair_t *self = &s_obj_closestPair; //! ie, to satsifiy the specific grammar of the 'ANSI C macro' sytnax;
	    if(__macro__minValueIsImproved(self, row_id, bucket_id_is, currentValue)) {
	      const uint column_pos = is;
	      cnt_alreadyInserted_forColumnAtSelfPos_is += (uint)update_minValues_for_columnBucket_setOptimal_atColumnPos(self, row_id, bucket_id_is, column_pos, currentValue);	      
	    } else if(__macro_minValue_forColumnBucket_noLongerHolds(self, row_id, bucket_id_is, js)) {
	      arrOf_condensed_rowId_toUse_is_local[(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar)++] = row_id; //! ie, as we then assuem teh row is of itnerest. */
	      //arrOf_condensed_rowId_toUse_js[arrOf_condensed_rowId_toUse_js_currPos++] = row_id; //! ie, as we then assuem teh row is of itnerest.
	    } //! else we assuem the column is not of interest.
	    /* if(__macro_isTo_updateColumn_forBucketTile(self, row_id, bucket_id_is, is)) { */
	      
	    /*   //printf("(update****)\t [%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__); */
	    /*   arrOf_condensed_rowId_toUse_is_local[(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar)++] = row_id; //! ie, as we then assuem teh row is of itnerest. */
	    /* } //! else we assuem the column is not of interest. */
#else
	    //printf("(update****)\t [%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__);
	    arrOf_condensed_rowId_toUse_is_local[(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar)++] = row_id; //! ie, as we then assuem teh row is of itnerest.
#endif
	  } //! else we asusme the row is already added.
#endif
	  mapOf_columnPos_toUse_is_local[j] = is; 
	} else {
	  //! Then we assume that the valeu is unchanged, ie, eitehr "[j] = js" or "[j] = UINT_MAX"
	}
#endif	//distmatrix[is][j] = distmatrix[n-1][j]; // FIXME: remove and incldue [ªbove]
	//distmatrix[is][j] = distmatrix[n_minusOne][j]; // FIXME: remove and incldue [ªbove]
	// printf("[row_id=%u][%u]=%f, at %s:%d\n", j, is, distmatrix[j][is], __FILE__, __LINE__);

	//mapOf_columnPos_toUse_js[j] = is; // FIXME: remove
      }
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
      //! Update the log-function, starting 'at a new time-point':
      update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific_cpy, /*cnt=*/1);
#endif
    }

    //printf("--------- test: %f, at %s:%d\n", distmatrix[1][0], __FILE__, __LINE__);

    //! Update the log-function, starting 'at a new time-point':
#if(config_useLog_main_signiOfSameBucket == 1)
    update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific, /*cnt=*/(bucket_id_is == bucket_id_js)); 
#else
#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_hcaSpecific, /*cnt=*/1); 
#endif
#endif

#if (config_typeOf_function_shortestPaths_useOptimal == 1)
    //! Update the log-function, starting 'at a new time-point':
    update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_dataUpdateCalls); 
    //! Then update the closest-distance matrix:
    if(local_matrixSize > 0) { //! then we udpate 'for the next iteration':	
      // printf("... at %s:%d\n", __FILE__, __LINE__);
      
      //if(js != (local_matrixSize-1)) 



      const uint columnUpdate_pos_start = min(is, js) + 1; //! ie, givent eh for-loop-updates of "j = js+1" and "j = is+1" for "distmatrix[j][js]".
      const uint columnUpdate_pos_end   = n-1;

#if(config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
      if(bucket_id_is == bucket_id_js) {
	//! Then our "arrOf_condensed_rowId_toUse_js" dientifes the 'best-fitting' column-positions:
	if( (is != (local_matrixSize-1)) || (js != (local_matrixSize-1)) ) {
	  assert_local( (is+1) == columnUpdate_pos_start); //! ie, as othwsie would be indicative of an in-cosnsitent code-update.
	  // printf("--------------------- at %s:%d\n", __FILE__, __LINE__);
	  assert(*arrOf_condensed_rowId_toUse_is_local_currPos_scalar == arrOf_condensed_rowId_toUse_js_currPos);
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	  s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/columnUpdate_pos_start, columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos, (cnt_alreadyInserted_forColumnAtSelfPos_is + cnt_alreadyInserted_forColumnAtSelfPos_js));
#else	  
	  s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/columnUpdate_pos_start, columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos);
#endif
	} //! otherwise this 'case' whill be implcialy handled when we call our "s_dense_closestPair_getClosestPair(..)"
      } else
#endif
	//! Provide logics for updating our "s_obj_closestPair" obly once, ie, if the buckets 'are the same':
	{
	  //! Note[time]: in [”elow] block if [abov€] data-set never changes the time-cost of below is 1.2x (when compare dot the overall exueciotn-time).
	  //! Update wrt. the 'vertically updated column':
	  // const uint row_endPos = min(local_matrixSize-1, nnodes-inode); //! ie, .....??....
	  if(is != js) {
	    //assert_local(columnUpdate_pos_start < columnUpdate_pos_end);
	    assert(mapOf_columnPos_toUse_is_local == mapOf_columnPos_toUse_is); //! ie, as we then assume taht 'udapting one implies udpating "is" and not "js'.
	    assert(mapOf_columnPos_toUse_is_local != mapOf_columnPos_toUse_js); //! ie, as we then assume taht 'udapting one implies udpating "is" and not "js'.

	  
	    /* 	  //! The operation: */
	    if(js != (local_matrixSize-1)) {
	      // printf("js=%u, ncols=%u, ... at %s:%d\n", js, ncols, __FILE__, __LINE__);
	      assert_local(js != n);
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	      s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/js, /*row-start-pos=*/columnUpdate_pos_start, /*row_endPos+1=*/columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos, cnt_alreadyInserted_forColumnAtSelfPos_js);
#else
	      s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/js, /*row-start-pos=*/columnUpdate_pos_start, /*row_endPos+1=*/columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos);
#endif
	    } //! otherwise this 'case' whill be implcialy handled when we call our "s_dense_closestPair_getClosestPair(..)"	  
	    if(is != (local_matrixSize-1)) {
	      //! Update the log-function, starting 'at a new time-point':
	      update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections); 
	      // printf("... at %s:%d\n", __FILE__, __LINE__);
	      const uint row_start_pos = is+1; const uint row_endPosPlussOne = n-1;
	      if(row_start_pos < row_endPosPlussOne) {
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
		s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/row_start_pos, /*row_endPos+1=*/row_endPosPlussOne, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_2, mapOf_columnPos_toUse_is_local, arrOf_condensed_rowId_toUse_is, arrOf_condensed_rowId_toUse_is_currPos, cnt_alreadyInserted_forColumnAtSelfPos_is);
#else
		s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/row_start_pos, /*row_endPos+1=*/row_endPosPlussOne, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_2, mapOf_columnPos_toUse_is_local, arrOf_condensed_rowId_toUse_is, arrOf_condensed_rowId_toUse_is_currPos);
#endif
	      }
	      //! Update the log-function, starting 'at a new time-point':
	      update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections, /*counut=*/(row_endPosPlussOne-row_start_pos)); 
	    } //! otherwise this 'case' whill be implcialy handled when we call our "s_dense_closestPair_getClosestPair(..)"
	  } else {
	    // printf("... at %s:%d\n", __FILE__, __LINE__);
	    if(is != (local_matrixSize-1)) {
	      assert_local( (is+1) == columnUpdate_pos_start); //! ie, as othwsie would be indicative of an in-cosnsitent code-update.
	      assert(mapOf_columnPos_toUse_is_local == mapOf_columnPos_toUse_is); //! ie, as we then assume taht 'udapting one implies udpating the second'.
#if (config_reWrite_spAlg_useDenseArrWhenUpdating_spColumns == 1) && (config_reWrite_spAlg_for_x_y_sameColumnBucket_useOneUpdateCall == 1)
	  s_dense_closestPair_updateAt_column_notAt_columnSelfPos(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/columnUpdate_pos_start, columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_js, arrOf_condensed_rowId_toUse_js_currPos, (cnt_alreadyInserted_forColumnAtSelfPos_is + cnt_alreadyInserted_forColumnAtSelfPos_js));
#else	  
	      s_dense_closestPair_updateAt_column(&s_obj_closestPair, /*column-id=*/is, /*row-start-pos=*/columnUpdate_pos_start, columnUpdate_pos_end, /*column-end-pos-plussOne=*/(local_matrixSize-1), /*data-tmp=*/arrOf_tmp_tranposeOld_1, mapOf_columnPos_toUse_js, arrOf_condensed_rowId_toUse_is_local, *arrOf_condensed_rowId_toUse_is_local_currPos_scalar);
#endif
	    } //! otherwise this 'case' whill be implcialy handled when we call our "s_dense_closestPair_getClosestPair(..)"
	  }
	} //! else the value will otherwise be ingored 'due to the incremental reduction in the column-size'.
	


      //if(false) // FIXME: remove
      {
      	//! Udpate wrt. the two 'hroizontally updated rows':
      	//! Note: we update for index-ranges: js=[0, js] and is=[0 ... is]:
      	if(is != js) {
      	  if(js != 0) {
	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/js, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/js, /*row-end-pos-plussOne=*/(local_matrixSize-1));
      	  }
      	  if(is != 0) {
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_startMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections); 
	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/is, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	    //! Update the log-function, starting 'at a new time-point':
	    update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_numberOf_updates_copyContinousSections, /*counut=*/is); 
      	  }
      	} else { //! then we update for only the 'copied region':
	  if(is != 0) {
	    // FIXME: icnldue:
	    s_dense_closestPair_updateAt_row(&s_obj_closestPair, /*row-id=*/is, /*column-start-pos=*/0, /*column-end-pos-plussOne=*/is, /*row-end-pos-plussOne=*/(local_matrixSize-1));
	  }
      	}
      } //! else the value will otherwise be ingored 'due to the incremental reduction in the column-size'.
      //! Evaluate if the object is conssitenten:
      // assert_local(classWideTests_runTime(&s_obj_closestPair, local_matrixSize-1, local_matrixSize-1, __FILE__, __LINE__, /*test_distancematrix=*/true)); //! ie, validate cosnistnecy if 'requeste'd through the compiler-macro-aprams.

      //! Update the log-function, starting 'at a new time-point':
      update_log_endMeasurement(&s_obj_closestPair, /*enum-id=*/e_log_hca_typeOf_all_dataUpdateCalls, /*cnt=*/1); 
    }
#endif    
      

      /* Fix the distances */
      for (j = 0; j < js; j++) //! ie, symmetric-prop. 
	distmatrix[js][j] = macro_max(distmatrix[is][j], distmatrix[js][j]);
      for (j = js+1; j < is; j++)
	distmatrix[j][js] = macro_max(distmatrix[is][j], distmatrix[j][js]);
      for (j = is+1; j < n; j++)
	distmatrix[j][js] = macro_max(distmatrix[j][is], distmatrix[j][js]);
      
      for (j = 0; j < is; j++) distmatrix[is][j] = distmatrix[n-1][j];
      for (j = is+1; j < n-1; j++) distmatrix[j][is] = distmatrix[n-1][j];
    }
    /* Update clusterids */
    result[nelements-n].left = clusterid[is];
    result[nelements-n].right = clusterid[js];
    clusterid[js] = n-nelements-1;
    clusterid[is] = clusterid[n-1];
  }
  free(clusterid);

  return result;
}

typedef enum e_hcaOp_type {
  e_hcaOp_type_brute,
  e_hcaOp_type_minHeap,
  e_hcaOp_type_tile,
  e_hcaOp_type_undef,
} e_hcaOp_type_t;
static const char *str_type[e_hcaOp_type_undef+1] = {
  "brute",
  "min-heap",
  "tile",
  "<undef>",
  // "",
};

/**
   @remarks

   ... in the measurements of data-structures ... ... write new sample-test-struct ... tut ... synethtic ... find the 'm' best entries in a data-set (ie, a min-heap without complexity of updates) ...  compare [naive, 'the propsoed appraoch', 'min-heap'] ... for 'the propseod appraoch' evaluate different combaitnos of tile-block-sizes ... 

... the 'iled' algorithm ... in the emausrement-setup ... code ... provide support for storing matrices usinga  'tiled' memory-allocation-routine ... ie, to try imrpvoign meory-ache-utliziaotn ... 

   -- Kruskal--MST: a large differnece betwene min-ehap and defualt-impl ... 
   -- .... : for a squared-matrix evlauation the time to use/apply a min-heap-based defualt impemtatnio is aprpox. the same as a brute search using a squared-linear-decreaisng adjcency-matrix.

   ... the compelxtiy in the clsuter-algorithm proposed by ...??..
   ...mtoviates/urges the need for an algorithm impemtantionw hich is both cocneptaul simple/trival to impmetantion and which is fast ... 

... symemtric amtrix-cases: .... not all adjcency-atrmices are symemtric ... some are symmetric ... for these cases ... 
... symmetric proerpties in tiling-alg .... traits/feautres which are observed wrt. improtance of the latter ... 
-- "e_hcaDp_matrix_symmetry_rowTile_lessThan_colTile" provides/gives a perofmrance which is condeirable sliglty worst than a non-symmetric-tiling-aprpoahc ... which is due to ....??....
 **/
int main(const int array_cnt, char **array) {
  assert(array);
  assert(array_cnt >= 1);

  uint nrows = 5;
  e_hcaOp_type_t typeOps = e_hcaOp_type_minHeap;
  uint case_id = 1;   
  if(true) {
    //nrows = 80;
    nrows = 6000;
    //nrows = 4000;
    //    nrows = 2000;
    //typeOps = e_hcaOp_type_tile;
    typeOps = e_hcaOp_type_brute;
    //case_id = 2;
    case_id = 0;
    //case_id = 1;
    //case_id = UINT_MAX;
  } 
  //! ----------------------------
  //! 
  { //! Set arguments:
    if(array_cnt > 1) {
      case_id = (uint)atoi(array[1]);
    }
    if(array_cnt > 2) {
      typeOps = (e_hcaOp_type_t)atoi(array[2]);
      if(typeOps >= e_hcaOp_type_undef) {
	fprintf(stderr, "!!(abort)\t no ops specifed for index=%d, at %s:%d\n", atoi(array[2]), __FILE__, __LINE__);
	return -1;
      }
    }
    //  uint nrows = 3000;
    if(array_cnt > 3) {
      nrows = (uint)atoi(array[3]);
    }
  }
  //! ----------------------------
  //! 
  printf("(info)\t ops(case)=%u, ops(ds)='%s', nrows=%u, at %s:%d\n", case_id, str_type[typeOps], nrows, __FILE__, __LINE__);
  //!
  //!
  const t_float empty_0 = 0;
  t_float **matrix = allocate_2d_list_float(nrows, nrows, empty_0); 

  //! 
  //! Set random scores: 
  for(uint i = 0; i < nrows; i++) {
    for(uint k = 0; k < nrows; k++) {
      const t_float rand_score = rand();
      if(rand_score != 0) {
	//matrix[i][k] = rand_score;
	matrix[i][k] = (RAND_MAX / rand_score);
	//	matrix[i][k] = (T_FLOAT_MAX / rand_score);
	// printf("rand_score=%f, at %s:%d\n", matrix[i][k], __FILE__, __LINE__);
      }
    }
  }
  s_hcDp_node *list_node = NULL;
  //! ----------------------------------------------
  //!
  //! 
  if( (case_id == 1) || (case_id == 2) ) {
    //! Case: find the 'm' best entries in a data-set (ie, a min-heap without complexity of updates) ...  compare [naive, 'the propsoed appraoch', 'min-heap'] ... for 'the propseod appraoch' evaluate different combaitnos of tile-block-sizes ... 
    //! Initate the result: 
    list_node = __init_nodeList(nrows);
    //!
    //! Iitnaitle for arlgorithm-case where 'this' is of interest:
    s_hc_heap_min_t obj_minHeap = MiF__setToEmpty__s_hc_heap_min_t(); //! ie, set thte oebject toe mpty: 
    uli *map_indexTo_row = NULL; uli *map_indexTo_col = NULL;
    t_float *listTo_1d = NULL; //! which is used when we are itnerested in only the 'upper-triangle' of a given matrix. 
    uli listTo_1d_size = 0;
    //!
    s_hcaDp_matrix_t obj_tile;
    //!
    if(typeOps == e_hcaOp_type_tile) {
      //uint cnt_bucketsEachRows = 100;
      //uint cnt_bucketsEachRows = 60;
      uint cnt_bucketsEachRows = 200;
      //uint cnt_bucketsEachRows = 400;
      //      uint cnt_bucketsEachRows = 10;
      //uint cnt_bucketsEachRows = 20;
      //      uint cnt_bucketsEachRows = 2;
      e_hcaDp_matrix_strategy_t tile_alg 
	= e_hcaDp_matrix_strategy_tile_andMinMax_row;      
      // = e_hcaDp_matrix_strategy_tile;
      e_hcaDp_matrix_symmetry_t type_symmetric = 
	//e_hcaDp_matrix_symmetry_rowTile_lessThan_colTile; //! ie, do Not use a summetric proeprty.
	e_hcaDp_matrix_symmetry_rowTile_gtThan_colTile; //! ie, do Not use a summetric proeprty.
      //e_hcaDp_matrix_symmetry_undef; //! ie, do Not use a summetric proeprty.
      //! 
      //! 
      obj_tile = s_hcaDp_matrix_t_init(nrows, /*ncols=*/nrows, cnt_bucketsEachRows, matrix, tile_alg, type_symmetric);
    } else if(typeOps == e_hcaOp_type_minHeap) { //! then tinite it.
      //!
      //! Cover the 2d-upper-triangle-amtrix to the lower halft:
      //! step(1): allocate:
      listTo_1d_size = (nrows * nrows); // *0.5;
      //      listTo_1d_size = (nrows * nrows)*0.5;
      assert(listTo_1d_size > 0);
      t_float empty_0 = 0;
      map_indexTo_row = allocate_1d_list_uli(listTo_1d_size, empty_0);
      map_indexTo_col = allocate_1d_list_uli(listTo_1d_size, empty_0);
      listTo_1d = allocate_1d_list_float(listTo_1d_size, empty_0);
      //! 
      //! 
      //! step(2): intailize:      

#define __MiF__get_index(r_, c_) ({ uli pos = (r_ * nrows) + c_; pos;})
      //#define __MiF__get_index(r_, c_) ({ uli pos = (r_ * nrows*0.5) + c_; pos;})
      
      { //! Iterate trhoguh the matrix, isnerting only the upper-part fo the matrix:      
	uli pos = 0;
	for(uint row_id = 0; row_id < nrows; row_id++) {
	  for(uint col_id = 0; col_id < nrows; col_id++) { //(nrows - (1 + row_id)); col_id++) {
	  //	  for(uint col_id = 0; col_id < (nrows - (1 + row_id)); col_id++) {
	    //!
	    // FIXME: valdiate [”elow];
	    //if(row_id != col_id) 
	    {
	      // printf("%u\t", MiF__pairTo_triangularIndex(row_id, col_id));
	      // uli pos_local = MiF__pairTo_triangularIndex(row_id, col_id);
	      // uint pos_local = ( (2*nrows - 3 - row_id) * row_id>>1) +  col_id - 1;
	      // assert(pos_local == pos);
	      // assert(pos_local < listTo_1d_size);
	      //!
	      //! Insert: 
	      const uli pos_local = __MiF__get_index(row_id, col_id);
	      assert(pos_local < listTo_1d_size);
	      listTo_1d[pos_local] = matrix[row_id][col_id];
	      map_indexTo_row[pos_local] = row_id;
	      map_indexTo_col[pos_local] = col_id;
	      // printf("[%u]\t (%u, %u)=%f\n", (uint)pos_local, (uint)map_indexTo_row[pos_local], (uint)map_indexTo_col[pos_local], listTo_1d[pos_local]);
	      //! Increment: 
	      if(pos < pos_local) {pos = pos_local;}
	      //pos++;
	    }
	  }
	}
	pos++;
	assert(pos <= listTo_1d_size);      
	//!
	//! step(3): insert: 
	printf("pos=%lu, at %s:%d\n", pos, __FILE__, __LINE__);
	obj_minHeap = init__s_hc_heap_min(listTo_1d, /*list-size=*/pos);
      }
    }
    //printf("\n\n\n");
    //! 
    //! Iterate and then 'mark as to be ingored':
    for(uint i = 0; i < (nrows-1); i++) {
      uint is = 1;     uint js = 0; t_float distance = T_FLOAT_MAX;

      const uint nrows_rest = nrows - i;

      if(typeOps == e_hcaOp_type_brute) {
	if(case_id == 2) { //! then aw e'also' remove all valeus alognt he 'last' row and column: 
	  distance = find_closest_pair((nrows-i), matrix, &is, &js); //! ie, then use an icnrementally-lower-search-space
	} else {
	  distance = find_closest_pair(nrows, matrix, &is, &js);
	}
	//! 
	//! Mark the cell as un-intereintg, ie, to be skipepd from enxt elvauaiton:
	matrix[is][js] = T_FLOAT_MAX; //! ie, where latter impleist that the vertex will never be of interest. 
	if(case_id == 2) { //! then aw e'also' remove all valeus alognt he 'last' row and column: 
	  for(uint row_id = 0; row_id < nrows_rest; row_id++) {
	    const uint col_id = ( nrows - (1 + i));;
	    matrix[row_id][col_id] = T_FLOAT_MAX;
	  }
	  for(uint col_id = 0; col_id < nrows_rest; col_id++) {
	    const uint row_id = ( nrows - (1 + i));;
	    matrix[row_id][col_id] = T_FLOAT_MAX;
	  }
	}
      } else if(typeOps == e_hcaOp_type_tile) {	
	//! Idneitfy the min-element and then remvoes it (hence udpatign the structure) (oesketh, 06. nov. 2017):
	distance = popMin__s_hcaDp_matrix_t_free(&obj_tile, &is, &js);
	//!
	//! Incrmeentally remvoe: 
	if(case_id == 2) { //! then aw e'also' remove all valeus alognt he 'last' row and column: 
	  decrementRowCounter__s_hcaDp_matrix_t(&obj_tile);
	}
      } else if(typeOps == e_hcaOp_type_minHeap) {	
	uli index_local = 0;
	heap_pop_s_hc_heap_min(&obj_minHeap, &distance, &index_local);
	assert(index_local < listTo_1d_size);
	is = (uint)map_indexTo_row[index_local];
	js = (uint)map_indexTo_col[index_local];
	//! then mark the 'ndoe' as removed:
	map_indexTo_col[index_local] = (uli)UINT_MAX; //! which is sued as a 'removed-tag'.
	//!
	//! Incrmeentally remvoe: 
	if(case_id == 2) { //! then aw e'also' remove all valeus alognt he 'last' row and column: 
	  for(uint row_id = 0; row_id < nrows_rest; row_id++) {
	    const uint col_id = ( nrows - (1 + i));;
	    //const uint col_id = ( nrows - i);
	    const uli pos = __MiF__get_index(row_id, col_id);
	    if(pos < obj_minHeap.list_size) {
	      if(map_indexTo_col[pos] < obj_minHeap.list_size) {	      
		if(obj_minHeap.list_currentPos >= 1) { //! then tere are more vertices to remove.
		  map_indexTo_col[pos] = (uli)UINT_MAX; //! which is sued as a 'removed-tag'.
		  remove_s_hc_heap_min(&obj_minHeap, pos);
		}
	      }
	    }
	  }
	  const uint row_id = ( nrows - (1 + i));;
	  //const uint row_id = (nrows - 1);
	  for(uint col_id = 0; col_id < nrows_rest; col_id++) { //(nrows - (1 + row_id)); col_id++) {
	  //for(uint col_id = 0; col_id < (nrows - (1 + row_id)); col_id++) {
	  //for(uint col_id = 0; col_id < ncols; col_id++) {
	    const uli pos = __MiF__get_index(row_id, col_id);
	    if(pos < obj_minHeap.list_size) {
	      if(map_indexTo_col[pos] < obj_minHeap.list_size) {	      
		if(obj_minHeap.list_currentPos >= 1) { //! then tere are more vertices to remove.
		  map_indexTo_col[pos] = (uli)UINT_MAX; //! which is sued as a 'removed-tag'.
		  remove_s_hc_heap_min(&obj_minHeap, pos);
		}
	      }
	    } //! else we asusemt eh idnex has alreayd beeen remvoed.
	  }
	}
	//printf("[%u]\t (%u, %u)=%f, index_local=%u\n", i, is, js, distance, (uint)index_local);
      }

      //printf("[%u]\t (%u, %u)=%f\n", i, is, js, distance);
      //! Update:
      const uint pos = nrows_rest - 1;
      list_node[pos].distance = distance; 
      list_node[pos].left = is; 
      list_node[pos].left = js;;
    }
    //!
    //! De-allcoate:
    free__s_hc_heap_min_t(&obj_minHeap);
    if(listTo_1d) { //! then we de-allcoate the itnenral list. 
      assert(listTo_1d_size > 0);
      free_1d_list_float(&listTo_1d); //, listTo_1d_size);
      listTo_1d = NULL;
      listTo_1d_size = 0;
      assert(map_indexTo_row);
      free_1d_list_uli(&map_indexTo_row);
      free_1d_list_uli(&map_indexTo_col);
    }
  } else  if(case_id == UINT_MAX) { //! then time the cost: 
    //! 
    //! Then we evaluate using a 'radnom searhc', ie, a 'reference' wrt. exeucion-time-cost
    for(uint i = 0; i < (nrows-1); i++) {
      uint is = rand() % (nrows - 1);
      uint js = rand() % (nrows - 1);
      assert(is < nrows);
      assert(js < nrows);
      
      //printf("[%u]\t (%u, %u)=%f\n", i, is, js, distance);
      //! Update:
      const t_float distance = matrix[is][js];
      const uint pos = i; 
      list_node[pos].distance = distance; 
      list_node[pos].left = is; 
      list_node[pos].left = js;;
    }
  } else  if(case_id == 0) { //! then time the cost: 
    //! 
    //! Apply:
    list_node = __pml__(nrows, matrix);
    assert(list_node);    
  } else {
    fprintf(stderr, "!!\t No task spefieid for case_id=%u, at %s:%d\n", case_id, __FILE__, __LINE__);
  }
  //! ----------------------------------------------
  //!
  //! 
  if(list_node) { //! then avodi the compilioer-optziaotn form 'forgetting' the appleid strategy.
    t_float score_sum = 0;
    for(uint i = 0; i < (nrows-1); i++) {
      score_sum += list_node[i].distance;
    }
    //! Where latter otpion is sued to avodi the optizonat-compiler-stategi to 'drop' the mian-cotmpatuion-part
    printf("(info)\t sum-of-distance=%f, at %s:%d\n", score_sum, __FILE__, __LINE__);
    //!
    //! De-allcoate:
    free(list_node); list_node = NULL;
  }
  //!
  //! De-allcoate:
  free_2d_list_float(&matrix, nrows);      matrix = NULL;



    assert(false); // FIXME["hcaDp_tileStruct.c"]: "..PML-alg-support" ... xplain different permtautiosn in our 'old-macro-varialbe-cetnered' strategy .... and what issues/cosndieroants we seek to address ... different time-cost-isseus wrt. update-seuqnces ... eg, wrt. 'transpose' column-update-calls.
    assert(false); // FIXME["hcaDp_tileStruct.c"]: "..PML-alg-support" ... add support for the option ... join rows .... ... default ... use the max-hca-alg-requreimetn as tempatle-basis .... 
    assert(false); // FIXME["hcaDp_tileStruct.c"]: complete [below] ...logics ... 
    assert(false); // FIXME["HCADP_TILESTRUCT.C"]: add support for new option ... join rows .... ... where the 'join-operation' is Not performed if ( (min(row(1)) + min(row(2))) >  max(all-rows) ) ... where latter min--max properties are defined/incldued in the 'init' function ... 

    assert(false); // FIXME["HCADP_TILESTRUCT.C"]: artilce ... use the opriaotn-strategies to descirbe/higlight the inerseicont between an algorithm versus its implemtatnions .... 

  
  return 1;
}
