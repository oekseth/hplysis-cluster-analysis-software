#include "hc_minHeap.h"
#include "def_memAlloc.h"

//! Update when "new value" <= "old value"
static void update_leq_ (s_hc_heap_min_t *self, uli i)  {
  assert(self);   assert(self->list_reverse); assert(self->list_forward);
  assert(self);
  assert(i < self->list_size);
  for (uli j = 0; (i>0) && 
	 ( MiF__scoreInTreeAtIndex(i) < MiF__scoreInTreeAtIndex(j=/*previus-tree-level=*/(i-1)>>1) ); 
       /*swap=*/i=j) {
    //! 
    //! Swap the index-pairs.
    heap_swap_s_hc_heap_min(self, i,j);
  }
}
//! Update when "new value" >= "old value"
static void update_geq_ (s_hc_heap_min_t *self, uli i)  {
  assert(self);   assert(self->list_reverse); assert(self->list_forward);
  assert(self);
  assert(i < self->list_size);
  //! 
  for (uli j = 0; /*next-level=*/(j=2*i+1) < self->list_currentPos; /*swap=*/i=j) {
    if ( MiF__scoreInTreeAtIndex(j) >= MiF__scoreInTreeAtIndex(i) ) {
      ++j;
      if( (j >= self->list_currentPos) || (MiF__scoreInTreeAtIndex(j) >= MiF__scoreInTreeAtIndex(i)) ) break;
    } else if ( (j+1 < self->list_currentPos) && MiF__scoreInTreeAtIndex(j+1) < MiF__scoreInTreeAtIndex(j) ) ++j;
    //! 
    //! Swap the index-pairs.
    heap_swap_s_hc_heap_min(self, i, j);
  }
}



//! Update when "new value" <= "old value"
static void update_leq(s_hc_heap_min_t *self, const uli index, const t_float val ) {
  assert(self);   assert(self->list_reverse); assert(self->list_forward);
  // Use this when the new value is not more than the old value.
  self->list_scores[index] = val;
  update_leq_(self, self->list_reverse[index]);
}

//! Update when "new value" >= "old value"
static void update_geq (s_hc_heap_min_t *self, const uli index, const t_float val ) {
  assert(self);   assert(self->list_reverse); assert(self->list_forward);
  // Use this when the new value is not less than the old value.
  self->list_scores[index] = val;
  update_geq_(self, self->list_reverse[index]);
}



//! @brief intiates a min-heap (eosekth, 06. nov. 2017).
s_hc_heap_min_t init__s_hc_heap_min(t_float *list_scores, const uli list_size) {
  s_hc_heap_min_t self;
  self.list_scores = list_scores;
  self.list_size = list_size;
  self.list_currentPos = list_size - 1;
  //! 
  //! Allocate:
  if(list_size != 0) {
    uli default_value = 0;
    self.list_forward = allocate_1d_list_uli(list_size, default_value);
    self.list_reverse = allocate_1d_list_uli(list_size, default_value);
    //! 
    //! Set defualt vlaues:
    for(uli i = 0; i < self.list_size; i++) {
      self.list_forward[i] = self.list_reverse[i] = i;
    }
    //! 
    { //! Then start the heapify-process. 
      //! Note: arrange the 'reverse' and 'forward' indces so that 
      //!    "H[i] = H[F[i]]" (ie, where "F" maps a 'real-world' index to the tree-position) satisfies: 
      //!    "H[i] < H[2*i + 1]" (ie, where "i" is a parent of tis children), and
      //!    "H[i] < H[2*i + 2]" (ie, hence the binary-heap-strategy which is used). 
      // Complexity: Θ(size)
      // Reference: Cormen, Leiserson, Rivest, Stein, Introduction to Algorithms,
      // 3rd ed., 2009, Section 6.3 “Building a heap”
      lint index = 0; //! which is used to 'allow' numbers < 0.
      lint size = (lint)self.list_size;
	for (index=(size>>1); index>0; ) {
	  index--;
	  update_geq_(&self, index);
	}
    }
    //heapify_s_hc_heap_min(&self);
  } else {
    self.list_reverse  = self.list_forward = NULL;
  }
  //! 
  //! @return
  return self;
}

//! De-allcoate the "s_hc_heap_min_t" object (oesekth, 06. nov. 2017).
void free__s_hc_heap_min_t(s_hc_heap_min_t *self) {
  assert(self);
  if(self->list_size) {
    assert(self->list_forward != NULL);
    assert(self->list_reverse != NULL);    
    //! 
    //! De-allocate:
    free_1d_list_uli(&(self->list_forward));
    free_1d_list_uli(&(self->list_reverse));    
    self->list_forward = NULL;
    self->list_reverse = NULL;
  } else {
    assert(self->list_forward == NULL);
    assert(self->list_reverse == NULL);
  }
  //! Clear the size-varialbes:
  self->list_size = 0;   self->list_currentPos = 0;
  self->list_scores = NULL;
  assert(self->list_forward == NULL);
  assert(self->list_reverse == NULL);  
}

/* inline t_index argmin() const { */
/*   // Return the minimal element. */
/*   return I[0]; */
/* } */

//! Remove the miimal elemnt fromt eh heap.
void heap_pop_s_hc_heap_min(s_hc_heap_min_t *self, t_float *score_min, uli *index_min) {
  assert(self);
  assert(self->list_currentPos >= 1); //!  ie, as we othewise woudl be removing an empty elemnt, hence causing an errror. 
  --(self->list_currentPos);
  //! 
  //! 
  *score_min =  MiF__scoreInTreeAtIndex(0); //! ie, self->list_scores[self->list_forward[0]];
  *index_min = self->list_forward[0];
  //! 
  //! 
  const uint index_f = self->list_currentPos;
  assert(index_f < self->list_size);
  self->list_forward[0] = self->list_forward[index_f];
  self->list_reverse[self->list_forward[0]] = 0;
  update_geq_(self, 0);
}

//! Remove elemnt at a given index. 
void remove_s_hc_heap_min(s_hc_heap_min_t *self, uli index) {
  assert(self);
  assert(self->list_currentPos >= 1); //!  ie, as we othewise woudl be removing an empty elemnt, hence causing an errror.
  --(self->list_currentPos);
  assert(self->list_currentPos < self->list_size);
  assert(index < self->list_size);
  self->list_reverse[self->list_forward[self->list_currentPos]] = self->list_reverse[index];
  const uint index_r = self->list_reverse[index];
  assert(index_r < self->list_size);
  self->list_forward[index_r] = self->list_forward[self->list_currentPos];
  if ( MiF__scoreInTreeAtIndex(self->list_currentPos)<=self->list_scores[index] ) {
    update_leq_(self, self->list_reverse[index]);
  }
  else {
    update_geq_(self, self->list_reverse[index]);
  }
}

//! Override a given valeu at a tivggnve postion, and then erneust that the heap is consistent.
void replace_s_hc_heap_min(s_hc_heap_min_t *self, const uli index_old, const uli index_new, const t_float val) {
  assert(self);   assert(self->list_reverse); assert(self->list_forward);
  self->list_reverse[index_new] = self->list_reverse[index_old];
  self->list_forward[self->list_reverse[index_new]] = index_new;
  if (val<=self->list_scores[index_old])
    update_leq(self, index_new, val);
  else
    update_geq(self, index_new, val);
}

//! Modify the vlaeu at a gvien psotion. 
void update_s_hc_heap_min(s_hc_heap_min_t *self, const uli index, const t_float val )  {
  assert(self);   assert(self->list_reverse); assert(self->list_forward);
  if (val<=self->list_scores[index])
    update_leq(self, index, val);
  else
    update_geq(self, index, val);
}
