#ifndef hp_image_segment_h
#define hp_image_segment_h
/*
 * Copyright 2012 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the poset library.
 *
 * the poset library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * the poset library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the poset library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "kt_matrix.h"

/**
   @brief hold information for a given image pixel (oekseth, 06. feb. 2018).
 **/
typedef struct s_hp_image_segment {
  uint pos_row;
  uint pos_col;
  unsigned char RGB[3]; //! ie, "Red", "Green", and "Blue".
} s_hp_image_segment_t;

//! @returen an 'empty' veriosn of the "s_hp_image_segment_t" object (oekseth, 06. feb. 2018).
s_hp_image_segment_t init__s_hp_image_segment_t();

//! Extract the RGB value from the text-string of: "9,0: (138,138,146)  #8A8A92  rgb(138,138,146)"
//! @return the identifed object.
s_hp_image_segment_t extractFrom_imageMagicText_RGB__s_hp_image_segment_t(const char *row_input);

#endif //! EOF
