--------------------------------------------------
         R E A D M E  
---------------------------------------------------


### What is this repository for? ###

* Summary: A machine-learning library created for fast and accurate cluster-analysis of complex relationships: important features concerns high accuracy of predictions and minimization of execution-time (ie, where the latter is enabled without sacrificing the accuracy of the predictions);  
* Version: 1.0
* Examples: "tut_helloWorld.c" and "www.knittingTools.org/gui_lib_hpLysis.cgi";

### Frequently Asked Questions (FAQ): 
* why developed? -- When analysing our data-sets, we realized that there did not exist any software-tools which were fast nor expressive for machine-learning.
* how is it simple to use? -- Due to its breath and with: (a) dedicated user-interfaces, (b) configuraiton metrics which are intuitive to understand (eg, similarity of Euclid versus Kendall), (c) fast (ie, users does not need to manually reduce their data-set before the anlaysis-process);
* why is it faster and more accurate than other tools? -- low-level implementations and support for conceptually challenging optimization algorithms. 



### How do I get set up? ###
* Summary of set up: go to "src/" and call "bash install.bash" in your terminal.
* Configuration: see the configuration-alternatives and examples listed for "ls src/tut_\*" (or: "src/tut/\*") and/or when calling "./x_hpLysis" (in your terminal).
* Dependencies: [none].
* Database configuration [none]. 
* How to use(C++): g++ -I../src/  -O2  -mssse3   -L ../src/  tut_helloWorld.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC -o x_tut
* How to use(C): gcc -std=c99 -I../src/ -O2  -mssse3   -L../src/  tut_helloWorld.c  -l lib_x_hpLysis  -Wl,-rpath=. -fPIC
* How to use(JavaScript): see "knittingTools.org/hpLysisVisu/tut_hellowWorld.html"; 

How to run tests: 

* specific: call the "./assert\*" and "./x_measure_\*" executables (constructed during the build-process).
* detailed focus: "perl perf/convertToJs_generic.pl"
* algorithm-comparison: "perl perf/ex_tut_all.pl"

* 100+ simplified tutorial-examples ("tut_\*"), eg, with respect to:
tut_buildSimilarityMatrix.c              tut_disjointCluster.c             tut_som.c      tut_sim_2_vec_Shannon.c
tut_clust_kMeans\_\_plusspluss_1.c         tut_disjointKMeansCluster.c       tut_kCluster.c                                  tut_sim_4_vec_chebychev.c
tut_ccm_1_matrixVSgold_VRC.c             tut_hca.c                         tut_kMeans_hcaPreStep.c                         tut_sim_5_oneToMany_Gower.c
tut_ccm_2_matrixVSgold_Silhouette.c      tut_inputFile_1_small\_\_gold.c     tut_mine.c                                      tut_sim_6_oneToMany_squaredChord.c
tut_ccm_3_matrixVSgold_DavisBouldin.c    tut_inputFile_2.c                 tut_random_difference_1.c                       tut_sim_7_oneToMany_Lortentizan.c
tut_ccm_9_goldVSgold_FowlesMallows.c     tut_inputFile_8.c                 tut_sim_1_vec_Euclid.c


### Who do I talk to? ###
* user-interface, algorithms, HPC, code: Ole Kristian Ekseth [oekseth@gmail.com];
* application: Ole Vegard Solberg (SINTEF);
* quality assurance: Prof. Svein Olaf Hvasshovd [sophus@ntnu.no];
* web-interface, see "www.knittingTools.org/gui_lib_hpLysis.cgi".

### From where may I use it? ###
* in software: Bash, Java, Perl, Python, R, C, C++
* direct use (large data-sets): from the terminal-API: "x_hp";
* direct use (interactive): www.knittingTools.org/gui_lib_hpLysis.cgi
* entropy: direct use (large data-sets): from the terminal-API: "x_hp_entropy", a bash-API which may be programmatically accessed through cal to  "hp_api_entropy.h" ;

### How may I use it?

The hpLysis provides a number of different interfaces. However, before using the software you need to install it. In below we exemplify the installaion process for Linux.

```
#!bash

         hg clone https://bitbucket.org/oekseth/hplysis-cluster-analysis-software/;
         cd <base-folder>/src/;
         bash install.bash;
         cd ..; #! ie, to seperate the hpLysis source code from your own working files.
         #!
         #! Compile a tutotrial, ie, a recepie later to be used for your own data mining scripts (eg, for segmentation of images):
         g++ -Isrc/  -g -O0  -mssse3   -L src/  src/tut_helloWorld.c  -l src/lib_x_hpLysis -o tut_test -Wl,-rpath=. -fPIC; #! ie, compile the "src/tut_helloWorld.c" tutorial (tut) use-case. 
```

Bash interface. In below we examplify the Bash interface of "x_hp", an interface which is easy to use, though limited in its functionality (when compared to the programming APIs). Motivation is to answer the question:
* how may I quickly analyse my data-sets?

```
#!bash

#! 
#! Note[pre]: Install the software by calling  "bash install.bash", and then use the simplified terminal--bash API to address use-cases such as: 
#! 

#! 
#! Use-case: compute similarity for different pairwise metrics:
#! Step(1): identify candidate metrics: 
x_hp -ops=distance
#! Step(2.a): metric: Euclid
x_hp -ops=e_kt_correlationFunction_groupOf_minkowski_euclid -file1=data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv 
#! Step(2.b): metric: Kendall-Cosine
x_hp -ops=e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine -file1=data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv
#! Step(2.c): Metric: MINE-Mic ("")
x_hp -ops=e_kt_correlationFunction_groupOf_MINE_mic -file1=data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv 

#! 
#! Use-case: cluster-analysis, ie, to identiy categories of entites.
#! Note: to analyse correctness of the identified cluster partition,
#!       the support for "Cluster Comparison Metrics (CCMs)" may be used, as described in later examples: 
#! Step(1): identify the default cluster-algorithms which may be applied: 
x_hp -ops=clustAlg
#! Step(2.a): compute for: "Hierarchical Cluster Algorithms (HCA)" max:
x_hp -ops=algorithm::HCA::max -file1=data/local_downloaded/iris.data.hpLysis.tsv 
#! Step(2.b): compute for: DB-SCAN(kd), where paramters are identifed through application of the default CCM-matrix:
x_hp -ops=algorithm::disjoint::kdTree::CCM -file1=data/local_downloaded/iris.data.hpLysis.tsv -nclusters=4 
#! Step(2.c): compute for: k-means-avg
x_hp -ops=algorithm::k-means::avg -file1=data/local_downloaded/iris.data.hpLysis.tsv -nclusters=4 
#! Step(2.d): compute for: k-means-avg, where clusters are idneitfed through a pre-HCA step
x_hp -ops=algorithm::k-means::avg -file1=data/local_downloaded/iris.data.hpLysis.tsv 


#! 
#! Use-case: accuracy of hypothesis (CCM:matrix-based). 
#! Assumption: features are split in four (-nclusters=4) parts:
#! Step(1): identify a CCM:matrix metric to use (eg, "Silhouette"): 
./x_hp -ops=ccm_matrix
#! Step(2): compute: 
./x_hp -ops=Silhouette-Index -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -nclusters=4
#! Step(3): repeat the above steps for all files in a directory "data/local_downloaded/*.tsv":
find data/local_downloaded/*.tsv  -exec ./tut_hp_api_fileInputTight_15_bash_1_ccm_matrix.bash "{}" \;

#! 
#! Use-case: Use-case(sparse-data): accuracy of hypothesis (CCM:matrix-based). Assumption: features are split in four (-nclusters=4) parts 
#! Step(1): identify a CCM-matrix to use: 
./x_hp -ops=ccm_matrix
#! Step(2): compute: 
./x_hp -ops=Dunns-Index -file1=data/fromTutExamples/tut_hp_api_fileInputTight_2_ccm_2_matrixHyp_sparseInput_case1.tsv -inputIsSparse=1 

#! 
#! Use-case: Test the similarity between different use-cases (CCM:gold based).
#! Note: to simplify our test-setup we assume that the features in the data describe boolean hypothesis. 
#!       However, for real-world use-cases the features should be without decimals, 
#!       as these are ingored during the applicaiton of gold-data clustering (examplfied in this use-case).
#! Step(1): identify a CCM:gold to use (eg, "Rands"): 
./x_hp -ops=ccm_goldXgold 
#! Step(2): compute: 
./x_hp -ops=Rands -nclusters=4 -file1=data/local_downloaded/airquality.csv.hpLysis.tsv
#! Step(3): repeat the above steps for all files in a directory "data/local_downloaded/*.tsv":
find data/local_downloaded/*.tsv  -exec ./tut_hp_api_fileInputTight_16_bash_2_ccm_gold.bash "{}" \;

#!
#! Use case: Normalize input data, eg, as a pre-step before clustering:
./x_hp_norm  -ops=row-Relative-Avg -file1=data/anthony_missingData/fire1.csv.hpLysis.tsv -out=tmp_0_.tsv -transpose=0

```

### Ansi-C and C++: how do I get started (ie, where do I find the main-function-logic)? ###
* basic usage: "hp_api_fileInputTight.h": a simplified API to our hpLysis cluster-analysis-software;
* "hpLysis_API.h": for cluster-analysis: supports the combination of algorithms, such as HCA in combination with the "Silhouette" CCM to compute the best-scoring k-means cluster;
* "hp_distance.h": for computation of similarity-metrics using 'dense' input-matrices, eg, for "Euclid", "MINE", "Kendall, "Shannon", etc.;
* "kt_sparse_sim.h": similarity-computation using sparse data-sets (eg, 2d-lists of triplets);
* "hp_ccm.h": evaluation of hypothesis and accuracy of cluster-predictions, where "CCM" is an acronym for "Cluster Comparison Metrics" (CCMs), such as "Silhouette", "Rands Index", and "Davis-Bouldins Index (DBI)"; 
* "hp_entropy.h": interface 20+ metrics for high-performance computation of accurate entropy-metrics; 
* "kt_sim_string.h": interface 5+ metrics for string-similarity, eg, "LCS(..)", "Levenshtein",  "Jaro-Winkler", etc.. 

To simplify usage of above we have written 100+ tutorials (exemplifying the application of the hpLysis software), eg, covering topics such as:

* similarity: "how similar are the features in my data-set?": eg, "tut_sim_10_manyMany_sampleCoVariance.c" for a basic introduction;
* CCM: "what hypothesis fits best to my feature-set?": eg, "tut_ccm_1_matrixVSgold_VRC.c" for an introduction, while "tut_ccm_11_compareMatrixBased.c" for a medium-complexity use-case-example;
* cluster-algorithm: "what is the ideal number of clusters in my feature-set?": eg, "tut_cluster.c" for an introduction to k-means clustering, and "tut_clust_dynamicK_1_hca_Silhoette.c" for an introduction to how combining HCA-algorithms with CCMs for cluster-count-identification.


### "Bash", "Perl", "Python", "Java", and "R": how do I compute my results? ###
* "bash": (release-testing now taking place: update forthcoming.)
* "Perl": (release-testing now taking place: update forthcoming.)
* "Python": (release-testing now taking place: update forthcoming.)
* "Java":  (release-testing now taking place: update forthcoming.)
* "R": (release-testing now taking place: update forthcoming.)

The above cross-language support is motivated by the high performance of the hpLysis software: to allow users to integrate the hpLysis high-performance approach into existing (and new) use-task-tailored software (eg, written an Java and Python). Therefore, to simplify the integration with/into programs written in other/alternative programming-languages we provide/enable high-level language support (for a restricted/limited set of hpLysis features, using our "hp_api_fileInputTight.h" as a template, in the programming-language of): "Perl", "Python", "Java", and "R". For each of the latter 'simplified API-access-points we include a number of different use-cases exemplifying the application of the latter APIs


### What is the scope of hpLysis cluster-analysis software? ###
The "hpLysis cluster-analysis software" is an approach to facilitate large-scale cluster-analysis of complex relationships in minimal time. In brief, the hpLysis high-performance cluster-data-analysis software library (hpLysis) significantly outperforms other cluster-analysis software, both wrt: 

* (a) execution-time performance: usability, algorithms, efficent implementations; 
* (b) number of supported similarity-metrics: more than 320+ simliarty-metrics integrated inot more than 16 popular algorithms;
* (c) weighting and masking of multi-dimensional parameter-spaces (eg, for complex relationships with missing information and differences in feature-weighting). 


Analytical capability. hpLysis enables accurate analysis of large data-sets (eg, for thousands of unique features) in minimal execution-time. The strength of the hpLysis optimized algorithms concerns: (a) general applicability (ie, may be used as-is in your research), (b) high performance (both wrt. accuracy and execution-time), and (c) well-documented (eg, exemplified through the 80+ tutorials provided with the source-code). From a time-cost perspective the hpLysis analytical machine-learning software enables energy-savings of more than 200x for large-scale data-analysis. When compared to alternative software for HPC data-analyses the following key-aspects are striking: (a) expressiveness (eg, similarity-metrics, cluster-algorithms and convergence-criteria/CCMs), (b) execution-time-optimizations (eg, reaches the maximum-optimization-threshold for bottleneck-similarity-matrix computations), (c) availability (free-of-charge for non-commercial usage). 

Scope. In our hpLysis HPC machine-learning approach we have focused our efforts on combining metrics for:

* vertex-similarity (ie, similarity-metrics, such as "Euclid", "Shannon", "MINE"), 
* cluster-assignment (ie, cluster-algorithms aush as "Agglomerate HCA", "k-means" and "DB-scan"), and 
* cluster-similarity (ie, CCMs such as "Silhouette", eg, to weight accuracy of different hypothesis). 

To evaluate our approach we have investigate the cluster-results of more than 1000 unique data-sets using more than 1000,000 different algorithm-permutations. Our results clearly indicates that accurate cluster-predictions need to incorporate the latter aspects, both wrt. execution-time performance and wrt. result-accuracy. (Details of the latter is now under peer-review: an updated will be presented.) An aspect (of the latter) concerns the importance of accurate and descriptive similarity-metrics in cluster-analysis (a perspective which is often ignored in research): in our "hpLysis_api.h" more than 320+ similarity-metrics are combined with more than 16+ cluster-algorithms and 20+ CCMs to facilitate accurate identification of vertex-based similarity, cluster-evaluation and hypothesis-testing. In our empirical evaluation (of the latter assertments wrt. result-quality and execution-time) we observe a large deviation/difference in results produced by the same clustering-algorithm (through with different similarity-metrics). To exemplify the latter, when we evaluate cluster-results using different CCMs, we clearly observe that the correct choice of similarity-metrics is (for many important real-life cases) of utmost importance. 

#### Application: where is the use-case for evaluation of simliarty and clustering? ####
In our "tut_helloWorld.c" we demonstrate an extensive application of generic logic found in our hpLysis cluster-analysis software. For some the latter tutorial-use-case may seem overwhelming. (If latter is the case, we suggest first reading "tut_hp_api_fileInputTight_1_ccm_1_matrixHyp.c".) In brief, hpLysis is designed to efficiently identify patterns in data. Therefore, we believe an introduction-example should cover these aspects: to relate similarity to predicted clusters through application of well-defined metrics. In this "Hello world" "tut_helloWorld.c" the latter is exemplified: we describe how to: step(a) identify the similarity of the rows in a wild-card input-file (eg, the "IRIS" data-set); step(b) separately hypothesized partitions through application of two different cluster-algorithms; step(3) evaluate accuracy and similarity of cluster-results VS computed similarity-matrix. For the latter "step(3)" we use hpLysis support for "Vluster Comparison Metrics (CCMs)": to first separately assess the consistency of each cluster-prediction, and thereafter to compare 'directly' the cluster-prediction-results using multiple "gold x gold" CCMs. 


### What algorithms are supported? ###
Supports 16+ different algorithms: in brief the complete set of popular machine-learning algorithms for un-supervised clustering; for details see our "hpLysis_api.h"; benchmarking across 1000+ synthetic and 80+ real-life data-sets indicates that hpLysis out-performs all popular software-tools (for un-supervised clustering). In below we list a subset of the algorithms which may be computed from our "hpLysis_api.h":

* "k-means": an optional "k-means++" initialization-step (or alt. init-steps such as: "HCA", "mini-batch", SOM, etc.), and thereafter a user-choice of k-means-permutations such as: average, median, and medoid  (and for each of the latter different random-assign-algorithms may be used, eg, wrt, "kmeans++") (algorithm-count=6++);
* "k-means-disjoint": first a disjoint-forest-pre-step to identify the 'k' clusters to evaluate, an optional "k-means++" initialization-step, and thereafter: average, median, and medoid (algorithm-count=6++);
* "k-means-mini-batch": an algorithm which randomly selects centroids, ie, in contrast to the k-means-algorithm which randomly selects cluster-memberships for all vertices (algorithm-count=1);
* "disjoint-forest": similar to the DB-SCAN algorithm, ie, first apply a similarity-metric, remove relationships/cells/pairs using a pre-defined similarity-value-threshold, and then identify the vertices which are related 'after the applied similarity-threshold' (algorithm-count=1);
* "Self Organising Maps (SOMs)": combine a data-learning-phase with a cluster-assignment into an (n, m) grid of clusters, ie, where cluster-topology is kept (algorithm-count=1);
* "Hierarchical Cluster Algorithms (HCA)": both hierarchical trees and "k-means"-cuts: Single-Link, Max, Average, Centroid (algorithm-count=4);
* "Kruskal": both hierarchical trees and "k-means"-cuts; use Kruskal's algorithm for computation of Minimum Spanning Trees (MST) to identify cluster-subsets, an approach where we support the permutations of: "kruskal-HCA", "kruskal-k-means" (where max-cluster-count is defined by the user), "kruskal-k-means-CCM" (which extends "kruskal-k-means" with application of a user-defined Cluster Comparison-Metric to evaluate if a merging of two clusters will improve cluster-prediction-accuracy) (algorithm-count=3);
* "MCL": the Markow Chain cluster-algorithm which is asserted (in numerous artilces) to provide high prediction-accuracy; in practise MCL is a permtuation of eigen-vector-centrality, DB-SCAN and k-means-mini-batch; in contrast to alternative appraoches we provide support for the 320+ simliarity-meitrcs supported in hpLysis;

### What similarity-metrics are supported? ###
hpLysis supports the 320+ most popular similarity-metrics used in research. 
Key characteristics of our approach concerns: (a) low execution-time (when computing for a similarity-metric); (b) large number of supported similarity-metrics (more than 320+ different supported metrics), (c) support for 'hiding' of values/scores in similarity-metrics computations (ie, to avoid 'unknown features' from cluttering the accuracy of your computations).  
Example-cases concerns: (a) as 'part' of an hpLysis-supported algorithm (configuring the "s_hpLysis_api_t" object defined in our "hpLysis_api.h"); (b) as integral part of your own work (eg, through our "hp_distance.h" API). In brief the similarity-metrics consists of the following 'parts':

* general cases: approx. 100+ 'count-based' similarity-metrics;
* rank-based cases: different permutations of Kendall's algorithm;
* specific algorithm: the MINE algorithm for information-measures;


The above similarity-metrics may further be tuned/modified through hpLysis configuration-steps, such as:

* rank-based modification: pre-compute the ranks before applying the similarity-metric (where "Spearman" is an example of the latter);
* binary-based modification: translate a floating-point vector into an 'either-or' vector (where "Hamming distance" is an example of the latter);
* weights: use a specific/given weight-table as a modification (eg, "Weighed Euclid");

In computation of similarity-metrics we manage a performance-increase of more than 200x-10,000x (when compared to popular libraries for similarity-metric computations). Examples of optimization-strategies applied in hpLysis concerns:

* SSE: to use assembly-level optimization to improve performance;
* memory-access: to reduce execution-time-cost through optimization-strategies such as "tiling", "restricted pointers", ordered memory-access, etc.;
* parallelism: to make use of the multi-core architectures (on many off-the-shelf computer-hardware): enabled through the "optmize_parallel_2dLoop_useFast" macro-option in our "configure_optimization.h" 

An example of a simplistic parallelism-strategy is seen in for our "kt_sparse_sim.h": in our "template_sparseSimMetric__nonOptimized.c" we apply the following procedure: step(a) partition data-sets into ('b1' < nrows_1, 'b2' < nrows_2) blocks, step(b) build a mapping-list wrt. 'these blocks', step(c) apply the parallelism (where "export OMP_NUM_THREADS=1" may be used to explictly set the code to use on computer-thread) , step(d) 'for each [...row1...][...row2...] set compute similarity-metric and post-process the results.

Use-case. To exemplify usage of above simliarty-metrics, we in the approx. 14 "tut_sim_\*.c" use-cases provide a number of straight-forward applications of our "hp_distance.h". (If our generic optimization-strategy sounds of interest, then we suggest reading our "kt_sparse_sim.c", where the latter provide an intuitive introduction to our compiler-optimized approach for fast computation of similarity-metrics.) In our tutorial-use-cases ("tut_\*.c") we demonstrates the general applicability of our approach: to compute for both sparse 'graph' data-sets (through our "kt_sparse_sim.h"), and feature-matrices and adjacency-matrices (eg, through our "hp_distance.h"). 

Advanced configuration. In application of similarity-metrics a large number of 'ambiguity-interpretations' exits: the latter refers to cases where the correct distance-computation-approach depends upon tacit interpretations and knowledge of input-data. An example of 'tacit knowledge-handling' concerns the issue of computing distance-scores when no-matches (ie, 'masked value') are frequent: to correctly 'handle cases' where only a small subset of the elements matches. An example-use-case concerns the case where "|v=pathway(i)|=100 and |v=pathway(i) cap y=protein(k)|=5" (ie, where we compute the distance between two vertices using two sparse lists of scores). In the latter use-case, if we omit/discard the 'no-matches' from evaluation, the implication is that 'a protein with only 5 common vertices (to a v=pathway with 100 vertices)' may be inferred to more similar to 'v' than 'an x=protein with 70 common vertices'. In order to address the latter interpretation-cases we in our "kt_sparse_sim.h" provide multiple configuration-options (for the latter). To properly handle/designate the latter cases, we provide interpretation-options to: solve(a) distance\*1 (ie, to assume that only intersections are to be considered: latter is the default approach for our matrix-based mask-approach); solve(b): distance\*max(|v|, |y|); solve(c) distance*(sum(v)\*sum(y)) (where we use the squared sum of possible distance-candidates to evaluate/update the distance-score); solve(d) distance\*(sum(v)+sum(y)) (where we use the 'sum of scores' to evaluate the distance-similarity-score). (For details wrt. the latter please see our "e_kt_sparse_sim_config__howToHandleNoMatches_t" in our "kt_sparse_sim_config.h").   

### What are the types of supported data normalization strategies? ###
There are 25 different data normalization strateiges (which are supported). These types may be summarized into:

* row-Relative-Avg:      Median=false, Absolute=false,  STD=false, adjust-by-max=true,  rows=true,   extreme=false
* row-Relative-Avg-Abs:  Median=false, Absolute=true,   STD=false, adjust-by-max=true,  rows=true,   extreme=false
* row-Relative-Rank:     Median=true,  Absolute=false,  STD=false, adjust-by-max=true,  rows=true,   extreme=false
* row-Relative-Rank-Abs: Median=true,  Absolute=true,   STD=false, adjust-by-max=true,  rows=true,   extreme=false
* row-Relative-STD:      Median=false, Absolute=false,  STD=true,  adjust-by-max=true,  rows=true,   extreme=false
* row-Relative-STD-ABS:  Median=false, Absolute=true,   STD=true,  adjust-by-max=true,  rows=true,   extreme=false
* row-Avg:               Median=false, Absolute=false,  STD=false, adjust-by-max=false, rows=true,   extreme=false
* row-Avg-Abs:           Median=false, Absolute=true,   STD=false, adjust-by-max=false, rows=true,   extreme=false
* column-Avg:            Median=false, Absolute=false,  STD=false, adjust-by-max=false, rows=false,  extreme=false
* row-Max-Relative:      Median=false, Absolute=false,  STD=false, adjust-by-max=false, rows=true,   extreme=true
* row-Min-Relative:      Median=false, Absolute=false,  STD=false, adjust-by-max=false, rows=true,   extreme=true
* row-STD:               Median=false, Absolute=false,  STD=true,  adjust-by-max=false, rows=true,   extreme=false


### What Cluster-Comparison-Metrics (CCMs) are supported? ###
To begin with, the "Cluster-Comparison-Metrics" (CCMs) provides opportunity for:

* hypothesis: test and measure how similarly a cluster-result is to your own hypothesis;
* cluster-convergence: fine-tune an algorithm with your data-set using a specific CCM as a convergence/stop criteria;
* data-similarity: measure the similarity of different data-sets by using the CCM-scores as a 'reference-point';

Given the importance of CCM in data-interpretations, we would expect there to be a large number of CCMs in use. The CCMs supported by hpLysis are:

* VRC;
* Sum of Squares (SSE), ie, the CCM-appraoch used in k-means, k-rank and k-medoid cluster-algorithms;
* Silhouette;
* DavisBouldin;
* Dunn;
* Rands Index;
* Rands Adjusted Index: provide two different interpretations used in research-litterature;
* chi-squared;
* Fowles-Mallows;
* Mirkin;
* Wallace: is exactly similiar to "Fowles-Mallows";
* Jaccard;
* Minkowski;
* PR: provide two different interpretations used in research-litterature;
* partition-difference;
* f-Measure;
* maximum-match-measure;
* vanDogen;
* Strehl & Hosh: a measure of mututal information;
* Fred & Jain: a measure of mututal information;
* Variation of Information;

The above listed CCMs are only a small subset: while our "hp_ccm.h" provide a simplistic CCM-interface, our "kt_matrix_cmpCluster.h" provide extensive options to improve accuracy of established CCMs. To examplify usage of above, we in the 10+ "tut_ccm_*.c" use-cases provide a number of straight-forward applications of our "hp_ccm.h", eg, wrt.:

* "tut_ccm_1_matrixVSgold_VRC.c": use the VRC matrix-based CCM to describe how simliar a matrix is to an hypothesis;
* "tut_ccm_7_goldVSgold_ARI.c": apply "Rands Adjusted Index" (ARI) to assess similarity between two hypothesis;
* "tut_ccm_12_nestedApplication_\_centroid_simMetric_ccm.c": a use-case: evaluate 'relative goodness' of an hypothesis.

In above "tut_ccm_*.c" examples the first exemplified use-cases describes simplified application of CCM-based cluster-evaluation. In contrast, the "tut_ccm_12_nestedApplication_\_centroid_simMetric_ccm.c" demonstrates how users may use hpLysis to construct their 'own' CCMs (eg, to capture specific traits in their analysed data-set). In the latter use-case, 4 steps are applied: step(1) combine (1.a) a feature-matrix with (1.b) an hypothesis (to investigate); step(2) use centroid-identification to measure distance from each feature-row to each of the 'center-points'; step(3) identity similarity between the feature-rows wrt. their 'relative closeness' to each of the hypothesis-center-points; step(4) compute all of the matrix-based CCMs (supported by hpLysis): describes similarity between the hypothesis-center-points VS the user-defined hypothesis. 

(For a brief discussion and visualization of differences in CCM-scores (for a syntetic cluster-result-set with a linear decrease in syntetic-cluster-accuracies), see our "knittingTools.org/data\_\_hpLysis/ccm_deviationFromIdeal\_\_gradualIncreaseInClusterCnt.html".)

### What Entropy-Metrics are supported? ###
To begin with application of entropy is used to describe deviations in data-sets. The application of entropy-metrics provides opportunity for:

* hypothesis: test and measure how similarly a distribution of frequency-counts are when comapred to different hypothesis;
* deviation: fine-tune an algorithm with your data-set using a specific entropy-metric as a convergence/stop criteria;
* data-similarity: a high-perofmrance appraoch to approximate clusters of entropies, eg, through a ranking of entropy-scores. 

Given the importance of entropy in data-interpretations, we would expect there to be a large number of CCMs in use. The entropy-metrics supported by hpLysis are:

* Shannon (ML);
* Miller-Madow (MM)
* Chao-Shen (CS); 
* Dirichlet-Jeffreys;
* Dirichlet-Laplace;
* Dirichlet-SG;
* Dirichlet-min-max;
* Shrink
* Simpson;
* Simpson-generic;
* Gini;
* Hulbert;
* HCDT;
* Hill;
* Renyi;
* Patil \& Taillie v1 (PT1);
* Patil \& Taillie v1 (PT2);
* Aczél \& Daróczy (AD);
* Herfindahl-Hirschman Index (HHI). 

Use-case: compute entropy from the bash-call
```
#!bash

#! 
#! Note[pre]: Install the software by calling  "bash install.bash", and then use the simplified terminal--bash API to address use-cases such as: 
#! 

#! Compute ML Shannon, and write out the average entropy of the rows (as defined in "kt_entropy.h"): 
/x_hp_entropy -metric=ML -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1
#! Compute ML Shannon, and write for each row:
./x_hp_entropy -metric=ML -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1 
#! Compute MM Miller Madow, and write out the average entropy of the rows
./x_hp_entropy -metric=MM -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1" 
#! 
./x_hp_entropy -metric=Jeffreys -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1", "Compute Dirichlet Jeffreys (Jeffreys), and write out the average entropy of the rows");
#! Compute seperately for all entropy-metrics using default configurations:
./x_hp_entropy -metric=all -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1
#! Compute seperately for all entropy-metrics using default configurations, and store the details in the result-files: 
./x_hp_entropy -metric=all -file1=data/local_downloaded/11_milk.csv.hpLysis.tsv -writeOutAverage=1 -out=entropy_result.tsv

```

### What String-Similarity-Metrics are supported? ###
In our "kt_sim_string.h" we provide support for five base-similarity-metrics and two value-adjustment-strategies, ie, 10 metrics in total. The string-similarity-metrics are used to compare similarity for cases where gaps (between strings) does not need to indicate strong dis-similarity. However for some use-cases 'gaps' indicates clear difference in strings, ie, for which our 320+ pairwise-similarity-metrics (eg, Hamming-distance may be correctly applied). In our tut-examples (eg, "tut_string_sim_2_stringArray.c" and "tut_string_sim_1_singlePair.c") we exemplify the broad usage (of our string-based comparison-strategy). 


### I have large sets of 'out-of-memory' high-dimensional data: how do I proceed?

To address your needs, which are often an important issue in mining of big data, we have constructed a new software which apply your filter-criteria during the computation of similarity, ie, rather than after all the pairwise similarities have been computed. To support different use-cases we provide support for:

*  bash: the "x_hp_sim_dense" exemplified in below use-cases
*  "hp_api_sim_dense.c": provide a conceptual simple interface for high-dimensional data-analysis with minimal memory consumption; 
*  "apply__2d__storeSparse__hp_distance(..)" ("hp_distance.h"): an advanced interface which compute sparse similarities through application of dense strategies. 

To explore the application of the advanced API support we suggest an introspection into the different tutorials (covering this topic), such as the "tut_sim_15_manMany_Euclid__selectBestInsideMetricComp.c"  use-case.


Use-cases: introduction examples which illustrate how accurate similarity-metrics may be used to analyse high-dimensional data-sets.

```
#!bash

#! 
#! Note[pre]: Install the software by calling  "bash install.bash", and then use the simplified terminal--bash API to address use-cases such as: 
#! Note[exemples]: [below] exemples is copy-pasted from a call to: "x_hp_sim_dense -ex"

#! Use-case: list options and metrics
x_hp_sim_dense #! where this call list the input paramters to use.
x_hp_sim_dense -ops=distance #! where this call list the metrics.
#! --------------------------------------------------------------
#! Applications: 
#! ---
#! Use-case: similarity of features for metric=Euclid
x_hp_sim_dense -ops=e_kt_correlationFunction_groupOf_minkowski_euclid -file1=data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv -filterRankMin=1 -filterRankMax=10 
#! Use-case: write out results into 'triplets'.
x_hp_sim_dense -ops=e_kt_correlationFunction_groupOf_minkowski_euclid -file1=data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv -transpose=1 -filterRankMin=1 -filterRankMax=10 
#! Use-case: correlate and then write out results to a seperate file
x_hp_sim_dense -ops=e_kt_correlationFunction_groupOf_minkowski_euclid -file1=data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv -out=result.tsv -transpose=1 -filterRankMin=1 -filterRankMax=10 
#! Use-case: correlate, and then export cluster results to "clusters.tsv" while the sparse colelction of densely inferred similarities to "result.tsv"
x_hp_sim_dense -ops=e_kt_correlationFunction_groupOf_minkowski_euclid -file1=data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv -out=result.tsv -outCluster=clusters.tsv -transpose=1 -filterRankMin=1 -filterRankMax=10 
#! Use-case: infer similarity of features for metric=Kendall-Cosine
x_hp_sim_dense -ops=e_kt_correlationFunction_groupOf_rank_kendall_sampleCorrelation_or_BrownianCoVariance_or_Cosine -file1=data/local_downloaded/61_MetabolicRate.csv.hpLysis.tsv -transpose=1 -filterRankMin=1 -filterRankMax=10 
#! --------------------------------------------------------------
```


## Structure of the API:

Below captures the core division of the code:

* types: 
* lists:
* correlation:
* cluster:
* api:
* tut: 

## Generic descriptions

### Why hpLysis: application of high-performance data-analysis to overcome major bottlenecks ###

The development of the "hpLysis cluster-analysis software" stems from the observation that there did not exist any software to analyse data-sets produced by Knitting-Tools knitting of biological entities. In the process to identify accurate software, the "Cluster c" cluster-analysis software was observed to perform the best accuracy of predictions. However, the software suffers from a large number of drawbacks:

* (a) performance: a significant performance-overhead prevented the application of large feature-spaces and vertex-spaces, ie, as the computation-time significantly exceeded reasonable time-thresholds;
* (b) expressively: the few number of distance-correlation-metrics caused our data-analysis to be skewed towards a set of un-explainable assumptions: to overcome this issue we have implemented more than 240 unique correlation-metrics, correlation-metrics which are guaranteed to have the best possible software-implementation (both for off-the-shelf computers and for dedicated HPC-computers);
* (c) usability: an important aspect of cluster-analysis is to relate the domain-experience (of domain-experts) to cluster-analysis software: while the cluster-c has a user-interface, the user-interface does neither allow nor encourage the comparison of different experimental cluster-results, ie, no support for learning. In contrast our web-interface is specifically designed to overcome these issues;

In order to ensure back-compatibility the "hpLysis cluster-analysis software" use the same algorithm-implementation and API as the "cluster c" implementation, ie, both result-accuracy and performance are increased to a significant degree: for important cases we observe that the hpLysis software outperforms the "cluster.c" library with a factor of more than 10,000,000x, ie, hence the importance of our appraoch in data-analysis. 

### Why the name of "hpLysis"
From above we observe the descriptiveness of the "hpLysis cluster-analysis software" name:

* "hp": the significant speed-up enabled by the "hpLysis cluster-analysis software" relates directly to both "horse power" and High Performance Computing (HPC), ie, a new standard for efficient dissemination of data-connectivity;
* "lysis" (http://www.dictionary.com/browse/lysis): from a dissemination of input-data we realize data-connectivity, eg, through application of data-filtering, correlation-analysis and clustering;



### Cite the "hpLysis" software
We have now multiple articles under peer review covering different aspects of the "hpLysis" software. What we suggest, is to cite the hpLysis-onto article which captures your application. To exemplify a subset of our article using the hpLysis-onto software:

* high-performance implementations: papers describing different strategies to optimize the performance of cluster-algorithms, metrics for similarity, and metrics for cluster convergence and entropy; 
* unsupervised prediction accuracy: papers which identify strategies to translate poor-performing cluster algorithms into best-performers; 
* supervised prediction accuracy: papers which intersect accuracy of unsupervised predictions with manually identifed clusters, thereby assessing best-practises in mining of drug-related knowledge; 
* big-data mining: papers which address issues in quality and performance of 'finding the needles' in complex heterogeneous collections of big-data; 
* visualization: papers outlining how application of the above described strategies enable user-interactive data-mining. 

For details of above please contact the developers of the "hpLysis" software. 
