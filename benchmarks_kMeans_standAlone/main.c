#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stddef.h>
#include <time.h>
#include <math.h>

#include "kt_matrix_base.h"
#include "kt_list_1d.h"
typedef t_float (*metricType)(uint, t_float*, t_float*);


typedef struct s_config_kMeans {
  uint k_cluster;
  uint iterations;
  metricType metric;
} s_config_kMeans_t;

void lloyd_clustering(const s_config_kMeans_t *self, const s_kt_matrix_base_t *mat,
		      //uint n, uint d,
		      //uint k_cluster, unsigned int iterations,
		      s_kt_matrix_base_t *map_clusterCenters_init, //! of dim: |clusters|x|features|
		      s_kt_list_1d_uint_t *map_result_rowToCluster //! of dim: |rows|
		      //uint *assignments
		      //double *clusterCenters, uint *assignments
		      //, metricType metric
		      ) {       

  
        
        
  double CONVERGENCE = 0.0001;
  unsigned int iteration_c = 0;    
        
  // Allocate memory to keep count of how many data points in each cluster
  uint *map_cntInEachCluster = allocate_1d_list_uint(self->k_cluster, 0);  
  //uint *map_cntInEachCluster  = (uint*)malloc(k * sizeof(uint)); 
  // ALCHECK(map_cntInEachCluster, "map_cntInEachCluster");          
    
  // Allocate memory for cluster center locations.
  //double *clusterCentersNext = (double*)malloc(d * k * sizeof(double));
  t_float **map_clusterCenters = allocate_2d_list_float(self->k_cluster, mat->ncols, 0);
  if(map_clusterCenters_init && map_clusterCenters_init->nrows) {
    //! Valdiate cocnistency:
    assert(map_clusterCenters_init->nrows == self->k_cluster);
    assert(map_clusterCenters_init->ncols == mat->ncols);
    //!
    //! Apply a slow copy-operaiton:
    for(uint k_i = 0; k_i < self->k_cluster; k_i++) {
      for(uint f_i = 0; f_i < mat->ncols; f_i++) {
	map_clusterCenters[k_i][f_i] = map_clusterCenters_init->matrix[k_i][f_i];
      }
    }
  }
  //t_float **map_clusterCenters = allocate_2d_list_float(mat->ncols, self->k_cluster, 0);
  // cmask_tmp = allocate_2d_list_float(nclusters, ndata, 1);

  // ALCHECK(clusterCentersNext, "clusterCentersNext");        
        
  t_float sumPointsToCenters = T_FLOAT_MAX, prevSumPointsToCenters = 0, difference = T_FLOAT_MAX;

  //#if false  

  // While less than given iteration count or converged.
  while (iteration_c < self->iterations && difference > CONVERGENCE) {                
    prevSumPointsToCenters = sumPointsToCenters;       
    sumPointsToCenters = 0;        
            
    // Clear center counters
    uint dataPointNr;
            
    for(uint k_i = 0; k_i < self->k_cluster; k_i++) {
    //for (dataPointNr = 0; dataPointNr < k; ++dataPointNr) {                
      map_cntInEachCluster[dataPointNr] = 0;               
      //uint centerStart = dataPointNr*d, dimensionNr;
      for(uint f_i = 0; f_i < mat->ncols; f_i++) {
	// for (dimensionNr = 0; dimensionNr < d; ++dimensionNr) {
	map_clusterCenters[k_i][f_i] = 0.0;
	//clusterCentersNext[centerStart+dimensionNr] = 0;
      }
    }                                  
            
    // Step 1 - Find assignments for each cluster center.   
    // Iterate over all points
    //for(uint k_i = 0; k_i < self->k_cluster; k_i++) {
    for(uint r_i = 0; r_i < mat->nrows; r_i++) {
    //for (dataPointNr = 0; dataPointNr < n; ++dataPointNr){              
      double minimumDistance = DBL_MAX;              
      uint centerNr, dimensionNr;
      // Find closest center. 
      for(uint k_i = 0; k_i < self->k_cluster; k_i++) {
      // for (centerNr = 0; centerNr < k; ++centerNr) {                 
	double currentDistance = 0; 
	//assert(false); // FIXME: incldue below!
	// FIXME: write permtautions of below:
	currentDistance = self->metric(mat->ncols, mat->matrix[r_i], map_clusterCenters[k_i]); // points + dataPointNr*d, clusterCenters + centerNr*d);
	//currentDistance = metric(d, points + dataPointNr*d, clusterCenters + centerNr*d);
	// If current center is closer than previously found closest.
	if (currentDistance < minimumDistance) {                      
	  minimumDistance = currentDistance;
	  assert(r_i < map_result_rowToCluster->list_size);
	  map_result_rowToCluster->list[r_i] = k_i;
	  //assignments[dataPointNr] = centerNr;                    
	}                                               
      }                
      // Add to count and to cluster.
      assert(r_i < map_result_rowToCluster->list_size);
      const uint k_assigned = map_result_rowToCluster->list[r_i];
      //const uint k_assigned = assignments[r_i];
      //uint assignedCluster = assignments[dataPointNr], clusterStart = assignedCluster*d, dataStart = dataPointNr*d;
      for(uint f_i = 0; f_i < mat->ncols; f_i++) {
      //for (dimensionNr = 0; dimensionNr < d; ++dimensionNr) {
	map_clusterCenters[k_assigned][f_i] = mat->matrix[r_i][f_i];
	//clusterCentersNext[clusterStart+dimensionNr] += points[dataStart+dimensionNr];
      }
      // Add to count
      map_cntInEachCluster[k_assigned]++;
                
      // Add to distance (E value) to check for convergence.
      sumPointsToCenters += minimumDistance;
    }

    //!
    //!  Vaclucate the mean-vecotr for each cluster:
    
    // Step 2 - Find new cluster centers based on new assignment
    // uint centerNr;
    for(uint k_i = 0; k_i < self->k_cluster; k_i++) {
    //for (centerNr = 0; centerNr < k; ++centerNr) {
      //uint clusterStart = centerNr*d, dimensionNr;
      // Fail safe. If no point in cluster, leave cluster at its own location.
      for(uint f_i = 0; f_i < mat->ncols; f_i++) {
      //for (dimensionNr = 0; dimensionNr < d; ++dimensionNr) {
	if (map_cntInEachCluster[k_i]) {
	  if( (map_clusterCenters[k_i][f_i] != 0) && (map_cntInEachCluster[k_i] != 0) ) {
	    map_clusterCenters[k_i][f_i] = map_clusterCenters[k_i][f_i] / map_cntInEachCluster[k_i];
	  }
	  //clusterCenters[clusterStart+dimensionNr] = clusterCentersNext[clusterStart+dimensionNr] / map_cntInEachCluster[centerNr];
	/* } else { */
	/*   clusterCenters[clusterStart+dimensionNr] = clusterCenters[clusterStart+dimensionNr]; */
	}
      }
    }
            
    // Check the E value difference comparing to last iteration
    difference = fabs(prevSumPointsToCenters - sumPointsToCenters);

#if DEBUG == 0
    fprintf(stdout, "Iteration %d, E-value %lf\n", iteration_c, sqrt(sumPointsToCenters));
#endif
    iteration_c++;
  }
    
  fprintf(stdout, "Lloyd stopped after %d iterations\n", iteration_c);

  //#endif // #if false    
  
  // Free memory.
  free_1d_list_uint(&(map_cntInEachCluster)); //, self->k_cluster);
  free_2d_list_float(&(map_clusterCenters), self->k_cluster); //, mat->ncols);
  //ALFREE(clusterCentersNext)
}

//
//
//
//
void elkan_clustering(double *points, uint n, uint d, uint k, int self->iterations, double *clusterCenters, uint *assignments, metricType metric) {
    
 // allocate memory for lower, upper bounds and center to center distances
    double *lowerBound = calloc(n * k, sizeof(double));
    ALCHECK(lowerBound, "lowerBound");
    
    // centerToCenter is half of the distance, because we only need to use half.
    double *centerToCenter = malloc((k * k ) * sizeof(double));
    ALCHECK(centerToCenter, "centerToCenter");
    
    // n upper bound
    double *upperBound = malloc(n * sizeof(double));
    ALCHECK(upperBound, "upperBound");
    
    // Allocate memory to keep count of how many data points in each cluster
    uint *map_cntInEachCluster = calloc(k, sizeof(uint));
    ALCHECK(map_cntInEachCluster, "map_cntInEachCluster");
    
    // Allocate memory for cluster center locations
    // center minimums s(c)
    double *centerMinimums = malloc(k * sizeof(double));
    ALCHECK(centerMinimums, "centerMinimums");
    
    // For next cluster centers
    double *clusterCentersNext = malloc(k * d * sizeof(double));
    ALCHECK(clusterCentersNext, "clusterCentersNext");
    
    double *distMoved = malloc(k * sizeof(double));
    ALCHECK(distMoved, "distMoved");
    
    uint i, ii, j, c1, dimNr;
    int reassignments = 1;
  
    double difference = 1000, CONVERGENCE = 0.0001;
    // Assign eachpoint to its closest initial center c(x) = argmin(c)d(x,c), using Lemma 1
    // To avoid redundant distance calculations
    
    int iterationNr = 0;
    
    //uint dataPointNr;
    //for (dataPointNr = 0; dataPointNr < n; ++dataPointNr){
    for(uint r_i = 0; r_i < mat->nrows; r_i++) {
      //for(uint k_i = 0; k_i < self->k_cluster; k_i++) {    
      uint centerNr;
      for(uint k_i = 0; k_i < self->k_cluster; k_i++) {
	//for (centerNr = 0; centerNr < k; ++centerNr) {
	lowerBound[r_i][k_i] = 0;    
	//lowerBound[dataPointNr*k+centerNr] = 0;    
      }
        
      assignments[dataPointNr] = 0;
      upperBound[dataPointNr] = DBL_MAX;
        
      // Add to count
      map_cntInEachCluster[assignments[r_i]]++;        
    }                
    
    
    // While not converged and not enough self->iterations.
    while (iterationNr < self->iterations && difference > CONVERGENCE && reassignments > 0) {
      // Set change and reassignments count to 0.
      difference = 0;
      reassignments = 0;
    
      // Find center to center distances in the beginning of each iteration.
      for(uint k_i = 0; k_i < self->k_cluster; k_i++) {
        //for (i = 0; i < k; ++i) {
	centerMinimums[k_i] = DBL_MAX;
	for(uint k_j = 0; k_j < self->k_cluster; k_j++) {
	  //for (j = 0; j < k; ++j) {
	  centerToCenter[k_i][k_j]  = 0;
	  //centerToCenter[i*k+j] = 0;
            
	  if (k_i != k_j) {
	    centerToCenter[k_i][k_j] = self->metric(mat->ncols, clusterCenters[k_i], clusterCenters[k_j]) / 2;
	    //centerToCenter[i*k+j] = self->metric(d, clusterCenters + i*d, clusterCenters + j*d) / 2;
	    // We need to find minimum distance for each center.
	    if (centerToCenter[k_i][k_j] < centerMinimums[k_i]) {
	      centerMinimums[k_i] = centerToCenter[k_i][k_j];
	    }
	  }
                
	}
      }

      
      // Iterate over all points
      for(uint r_i = 0; r_i < mat->nrows; r_i++) {
        //for (i = 0; i < n; ++i) {
	// If upper bound for I is smaller than half of its center's distance to another center, then no change.
	if (upperBound[r_i] <= centerMinimums[assignments[r_i]]) {
	  continue;
	}
	// We need to calculate upper bound once for each iteration.
	int needCalculation = 1;
	for(uint k_j = 0; k_j < self->k_cluster; k_j++) {
	  //for (j = 0; j < k; ++j) {
	  if (k_j == assignments[k_i]) continue;
                
	  double zValue = MAX( lowerBound[r_i][k_j], centerToCenter[/*row=*/assignments[r_i]][/*cluster=*/k_j] );
	  if (upperBound[i] <= zValue) {
	    continue;
	  }
                
	  // Update upper bound.
	  if (needCalculation) {
	    upperBound[i] = 0;
	    //for (dimNr = 0; dimNr < d; ++dimNr) {
	    for(uint f_i = 0; f_i < mat->ncols; f_i++) {
	      upperBound[r_i] += pow(mat->matrix[r_i][f_i] - clusterCenters[/*row=*/assignments[r_i]][f_i], 2);
	      // points[i*d+dimNr] - clusterCenters[assignments[i]*d+dimNr], 2);
	    }
	    upperBound[r_i] = sqrt(upperBound[r_i]);
	    needCalculation = 0;
	    if (upperBound[r_i] <= zValue) {
	      continue;
	    }

	  }
	  // Get distance.
	  lowerBound[r_i][k_j] = self->metric(mat->ncols, mat->matrix[r_i], clusterCenters[k_j]);
	  //lowerBound[i*k+j] = self->metric(d, points + i*d, clusterCenters + j*d);
               
	  // If lower bound is smaller than upper bound, then we need to change. Update upper bound as well
	  if (lowerBound[r_i][k_j] < upperBound[r_i]) {
	    //if (lowerBound[i*k+j] < upperBound[i]) {
	    map_cntInEachCluster[assignments[r_i]]--;
	    map_cntInEachCluster[k_j]++;
	    assignments[r_i] = j;
	    reassignments++;
	    upperBound[r_i] = lowerBound[r_i][k_j];
                   
	  }
	}
      }
        
      // Change accumulators to 0.
      for(uint k_j = 0; k_j < self->k_cluster; k_j++) {
	for(uint f_i = 0; f_i < mat->ncols; f_i++) {
	  //for (i = 0; i < k*d; i++) {
	  clusterCentersNext[k_i][f_i] = 0;
        }
      }
      
      // For each center we find all the points which belong to it and add up their locations
      for(uint r_i = 0; r_i < mat->nrows; r_i++) {
	//for (dataPointNr = 0; dataPointNr < n; ++dataPointNr) {
	uint k_assigned = assignments[r_i];
	for(uint f_i = 0; f_i < mat->ncols; f_i++) {
	  //for (dimNr = 0; dimNr < d; ++dimNr) {
	  clusterCentersNext[k_assigned][f_i] += mat->matrix[r_i][f_i];
	  //clusterCentersNext[whichCluster*d+dimNr] += points[dataPointNr*d+dimNr];
	}            
      }
      // division by cluster size, find average in each dimension.
      for(uint k_j = 0; k_j < self->k_cluster; k_j++) {
	//for (c1 = 0; c1 < k; ++c1) {
	//for (ii = 0; ii < d; ++ii) {
	for(uint f_i = 0; f_i < mat->ncols; f_i++) {
	  clusterCentersNext[k_j][f_i] = clusterCenters[k_j][f_i];
	  if(map_cntInEachCluster[k_j] != 0) {
	    const t_float diff = clusterCentersNext[k_j][f_i] / map_cntInEachCluster[k_j];
	    clusterCentersNext[k_j][f_i] = diff;
	    //clusterCentersNext[k_j][f_i] = map_cntInEachCluster[c1] ? (clusterCentersNext[c1*d+ii] / map_cntInEachCluster[c1]) : clusterCenters[c1*d+ii];
	  }
	}
	//! Identify teh diffrence between old versus new cluster:
	distMoved[k_j] = self->metric(mat->ncols, clusterCentersNext[k_j], clusterCenters[k_j]);
	difference += distMoved[k_j];
      }
      
      // Update upper bounds by distance moved by its corresponding center
      for(uint r_i = 0; r_i < mat->nrows; r_i++) {
        //for (i = 0; i < n; ++i) {
	upperBound[r_i] += distMoved[assignments[r_i]];
	// Decrease lower bound by distance moved for the center and point pair.
	for(uint k_j = 0; k_j < self->k_cluster; k_j++) {
	  //            for (j = 0; j < k; ++j)
	  lowerBound[r_i][k_j] -= distMoved[k_j];
	}
      }
      
      // 7. replace each center c by m(c)
      memcpy(clusterCenters, clusterCentersNext, sizeof(t_float) * self->k_cluster * mat->ncols);
      
#if DEBUG == 0
      fprintf(stderr, "Iteration nr %d, difference: %lf, reassignments: %d\n", iterationNr, difference, reassignments);
#endif
      iterationNr++;   
    }
            
    
    fprintf(stdout, "Elkan stopped after %d self->iterations\n", iterationNr);
    
    ALFREE(lowerBound);
    ALFREE(centerToCenter);
    ALFREE(upperBound);
    ALFREE(map_cntInEachCluster);
    ALFREE(centerMinimums);
    ALFREE(clusterCentersNext);
    ALFREE(distMoved);
}
    
//
//
//

//
	# if 0
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "hamerly.h"
#include "commonmacros.h"

void hamerly_clustering(double *points, uint n, uint d, uint k, int self->iterations, double *clusterCenters, uint *assignments, self->metricType self->metric) {
    
    //Allocate memory for:
    
    // n upper bounds
    double *upperBounds = malloc(sizeof(double)*n);
    ALCHECK(upperBounds, "upperBounds");
    
    // n lower bounds
    double *lowerBounds = malloc(sizeof(double)*n);
    ALCHECK(lowerBounds, "lowerBounds");

    // Distance to nearest other cluster for each cluster. 
    double *lowestClusterDistances = malloc(sizeof(double)*k);
    ALCHECK(lowestClusterDistances, "lowestClusterDistances");
    
    // to keep track how much distance did each cluster center move.
    double *centerDistMoved = malloc(sizeof(double)*k);
    ALCHECK(centerDistMoved, "centerDistMoved");
    
    // assignments count
    uint  *map_cntInEachCluster = malloc(sizeof(uint)*k);
    ALCHECK(map_cntInEachCluster, "map_cntInEachCluster");
    
    // For following iteration centers
    double *nextClusterCenters = malloc(sizeof(double)*k*d);
    ALCHECK(nextClusterCenters, "nextClusterCenters");
    
    uint i, ii, j, dimensionNr, centerNr;
    
    // Initialize void bounds and assign each point to center 0.
    for (i = 0; i < n; ++i) {
        assignments[i] = 0;
        upperBounds[i] = DBL_MAX;
        lowerBounds[i] = 0;
    }
    
    map_cntInEachCluster[0] = n;
    for (centerNr = 1; centerNr < k; ++centerNr) map_cntInEachCluster[centerNr] = 0;
    uint reassignments = 1;
    int iterationCount = 0;
    
    // While not converged and not enough self->iterations
    while (iterationCount < self->iterations && reassignments > 0) { 
    
        reassignments = 0;
        // Find minimum center to center distance between each two centers i != ii
        for (i = 0; i < k; i++) {
            double minDist = DBL_MAX;
            for (ii = 0; ii < k; ii++) {
                if (i != ii) {
                    // Calculate center to center distance
                    double currentDist = 0;
                    for (dimensionNr = 0; dimensionNr < d; dimensionNr++) currentDist += pow(clusterCenters[i*d+dimensionNr] - clusterCenters[ii*d+dimensionNr], 2);
                    currentDist = sqrt(currentDist) / 2;
                    if (currentDist < minDist) {minDist = currentDist; lowestClusterDistances[i] = currentDist;}
                }
            }
        }  
        
        // Next cluster centers reset
        for (i = 0; i < k*d; ++i) nextClusterCenters[i] = 0;
        // for all points
        for (i = 0; i < n; ++i) {
            // z = max(l(i), s(a(i)))
            double zValue = MAX(lowerBounds[i], lowestClusterDistances[assignments[i]]);
            // If upper bound is smaller than zVal then it is not possible that any other center is closer.
            if (upperBounds[i] <= zValue) continue;
            
             // Recalculate upper bound
            upperBounds[i] = self->metric(d, points + i*d, clusterCenters + assignments[i]*d);
            if (upperBounds[i] <= zValue) continue;
            
            // Find c(j) and c(j'), the two closest centers to x(i), as well as the distances to each
            uint firstClosestCenter = 0;
            double distToFirstCenter = DBL_MAX;
            double distToSecondCenter = DBL_MAX;
            // calculate distance to each center, find two closest centers.
            
            // Iterate over all centers and find first and second closest distances.
            for (centerNr = 0; centerNr < k; centerNr++) {
                double currentDist = self->metric(d, points + i*d, clusterCenters + centerNr*d);
                // Find distace between the point and the center.
                if (currentDist < distToFirstCenter) {
                    distToSecondCenter = distToFirstCenter;
                    distToFirstCenter = currentDist;
                    firstClosestCenter = centerNr;
                } else if (currentDist < distToSecondCenter) {
                    distToSecondCenter = currentDist;
                }               
            
            
            }
            // If closest center is not currently assigned, then reassign. Update bounds.
            if (firstClosestCenter != assignments[i]) {
                upperBounds[i] = distToFirstCenter;
                map_cntInEachCluster[assignments[i]]--;
                assignments[i] = firstClosestCenter;
                map_cntInEachCluster[firstClosestCenter]++;
                reassignments++;
            }
            lowerBounds[i] = distToSecondCenter;
            
            
        }
        
        // Accumulate and calculate mean for each center.
        for (i = 0; i < n; i++) {
            for (j = 0; j < d; j++) {
            nextClusterCenters[assignments[i]*d+j] += points[i*d+j];
            }
        }
        
        // check for distance moved and move. Find max moved.
        double maxMoved = 0;
        for (centerNr = 0; centerNr < k; ++centerNr) {
            double currentDist = 0;
            if (map_cntInEachCluster[centerNr] > 0) {
                for (dimensionNr = 0; dimensionNr < d; dimensionNr++) {
                    nextClusterCenters[centerNr*d+dimensionNr] = map_cntInEachCluster[centerNr] ? 
                    (nextClusterCenters[centerNr*d+dimensionNr] / map_cntInEachCluster[centerNr]) : clusterCenters[centerNr*d+dimensionNr];
                }
                currentDist = self->metric(d, clusterCenters + centerNr*d, nextClusterCenters + centerNr*d);
            }
            centerDistMoved[centerNr] = currentDist;
            if (currentDist > maxMoved) maxMoved = currentDist;
            
        }
        // for all points update upper and lower distance bounds
        for (i = 0; i < n; ++i) {
            upperBounds[i] += centerDistMoved[assignments[i]];
            lowerBounds[i] -= maxMoved;
        }
        
        memcpy(clusterCenters, nextClusterCenters, sizeof(double) * k * d);
        
        #if DEBUG == 0
        printf("Hamerly - Iteration %d, maxmoved %lf, reassigned %d\n", iterationCount, maxMoved, reassignments);
        #endif
        iterationCount++;
    
    }
    
    printf("Hamerly stopped after %d self->iterations\n", iterationCount);
    
    ALFREE(upperBounds)
    ALFREE(lowerBounds)
    ALFREE(lowestClusterDistances)
    ALFREE(centerDistMoved)
    ALFREE(map_cntInEachCluster)
    ALFREE(nextClusterCenters)
}





void macqueen_clustering(double *points, uint n, uint d, uint  k, int self->iterations, double *clusterCenters, uint *assignments, self->metricType self->metric) {
    

        // Allocate memory for cluster sizes. How many objects in a cluster.
        uint *map_cntInEachCluster = calloc(k, sizeof(uint));
        ALCHECK(map_cntInEachCluster, "map_cntInEachCluster");
        
        
        
        int iteration_c = 0, changed = 1;
        uint i, centerNr, dim;
        // First iteration is different. For each point find closest center and assign it to it.. Move centers and 
        // start usual macqueen. Each time point changes the cluster in belongs to, change both centers (previous and new).
        for (i = 0; i < n; ++i) {
            double lowest_distance = DBL_MAX;
            for (centerNr = 0; centerNr < k; ++centerNr) {
                // calculate distance
                double distance = self->metric(d, points + i*d, clusterCenters + centerNr*d);
                if (distance < lowest_distance) {
                    assignments[i] = centerNr;
                    lowest_distance = distance;
                }
            }
        }
        
        // Move centers to the means.
        for (i = 0; i < k*d; ++i) clusterCenters[i] = 0;
        
        for (i = 0; i < n; ++i) {
            map_cntInEachCluster[assignments[i]] += 1;
            for (dim = 0; dim < d; ++dim) {
                clusterCenters[assignments[i]*d+dim] += points[i*d+dim];
            }
        }
        
        // Divide each center's points sum by count of points in its cluster.
        for (centerNr = 0; centerNr < k; ++centerNr) {
            for (dim = 0; dim < d; ++dim) {
                if (map_cntInEachCluster[centerNr]) {
                    clusterCenters[centerNr*d+dim] = clusterCenters[centerNr*d+dim] / map_cntInEachCluster[centerNr];
                } else {
                    clusterCenters[centerNr*d+dim] = clusterCenters[centerNr*d+dim];
                    }
            }
        }
        
        // Every iteration check if point has a new closest center. If so then move point and update centers.
        while (iteration_c < self->iterations && changed > 0) {
            changed = 0;
            // For each point
            for (i = 0; i < n; ++i) {
                // find distance to current center and compare to other centers.

                double currentDist = self->metric(d, points + i*d, clusterCenters + assignments[i]*d);
                
                uint nearestCenter = assignments[i];
                
                // find nearest center.
                for (centerNr = 0; centerNr < k; ++centerNr) {
                    if (centerNr != assignments[i]) {
                        
                        double newDist = self->metric(d, points + i*d, clusterCenters + centerNr*d);
                        if (newDist < currentDist) {
                            currentDist = newDist;
                            nearestCenter = centerNr;
                        }
                    }
                }
                // If this is true, then there is a new nearest center. Move centroids.
                if (nearestCenter != assignments[i]) {
                    // Assign point to the other cluster and move center.
                    for (dim = 0; dim < d; ++dim) {
                        // Subtracted from previous center.
                        // Check if any points assigned, if not, then don't move.
                        if (map_cntInEachCluster[assignments[i]] - 1 > 0) {
                            clusterCenters[assignments[i]*d+dim] *= map_cntInEachCluster[assignments[i]];
                            clusterCenters[assignments[i]*d+dim] -= points[i*d+dim];
                            clusterCenters[assignments[i]*d+dim] /= (map_cntInEachCluster[assignments[i]] - 1);
                        }
                        
                        // Add to new centroid.
                        clusterCenters[nearestCenter*d+dim] *= map_cntInEachCluster[nearestCenter];
                        clusterCenters[nearestCenter*d+dim] += points[i*d+dim];
                        clusterCenters[nearestCenter*d+dim] /= (map_cntInEachCluster[nearestCenter] + 1);
                        
                        // Change sizes and assignment
                        map_cntInEachCluster[assignments[i]] -= 1;
                        map_cntInEachCluster[nearestCenter] += 1;
                        assignments[i] = nearestCenter;
                        changed += 1;
                        
                    }
                }
                
                
            }

            
         // Set accumulators to 0
         for (centerNr = 0; centerNr < k; ++centerNr) {
             if (map_cntInEachCluster[centerNr]) {
                for (dim = 0; dim < d; ++dim) clusterCenters[centerNr*d+dim] = 0;	
             }
         }
          		
         // Add all up
         for (i = 0; i < n; ++i) {		
               for (dim = 0; dim < d; ++dim) {		
                   clusterCenters[assignments[i]*d+dim] += points[i*d+dim];		
                }		
          }		
             		
            // Divide each center's points sum by count of points in its cluster.		
          for (centerNr = 0; centerNr < k; ++centerNr) {		
                for (dim = 0; dim < d; ++dim) {		
                    if (map_cntInEachCluster[centerNr]) {		
                         clusterCenters[centerNr*d+dim] = clusterCenters[centerNr*d+dim] / map_cntInEachCluster[centerNr];		
                    } 
                 }		
             }            
            iteration_c++;
        }
        printf("MacQueen stopped after %d self->iterations\n", iteration_c);
            
            
        ALFREE(map_cntInEachCluster);
}



void hartigan_clustering(double *points, uint n, uint d, uint k, int self->iterations, double *clusterCenters, uint *assignments, self->metricType self->metric) {

        // Allocate memory for:
        
        // How many points are in each luster
        uint *map_cntInEachCluster = calloc(k, sizeof(uint));
        ALCHECK(map_cntInEachCluster, "map_cntInEachCluster");
        
        
        // What is the first closest center for each point
        uint *firstClosestCenters =  malloc(sizeof(uint)*n);
        ALCHECK(firstClosestCenters, "firstClosestCenters");
        
        // What is the second closest center for each point
        uint *secondClosestCenters = malloc(sizeof(uint)*n);
        ALCHECK(secondClosestCenters, "secondClosestcCnters");
        
        
        // Which centers are in the liveset. 1 if live, 0 otherwise.
        int *isLiveSet = malloc(sizeof(int)*k);
        ALCHECK(isLiveSet, "isLiveSet");
        
        // What centers are made live in quicktransfer set.
        int *qtranLiveSet = malloc(sizeof(int)*k);
        ALCHECK(qtranLiveSet, "qtranLiveSet");
        
        // Swap memory for live sets.
        int *nextLiveSet = malloc(sizeof(int)*k);
        ALCHECK(nextLiveSet, "nextLiveSet");
        
        
        // What are R1 values for each point.
        double *R1_values = malloc(sizeof(double)*n);
        ALCHECK(R1_values, "R1_values");
        
        // To remember previous clusters.
        double *prevClusterCenters = malloc(sizeof(double) * k * d);
        ALCHECK(prevClusterCenters, "prevClusterCenters");
        
        memcpy(prevClusterCenters, clusterCenters, sizeof(double) * k * d);
        
        uint i, centerNr, dim;

        int iteration_c = 0;

        
        // For each point, find closest and second closest centers.
        for (i = 0; i < n; ++i) {
            double fClosestDist = DBL_MAX;
            double sClosestDist = DBL_MAX;
            uint fClosest = -1;
            uint sClosest = -1;
            
            // Iterate over all centers
            for (centerNr = 0; centerNr < k; ++centerNr) {
                double distPointToCenter = self->metric(d, points + i*d, clusterCenters + centerNr*d);
               
                // Find distance from point to center.

                // Compare found distance to previously found distances.
                if (distPointToCenter < fClosestDist) {
                    // If newly found center is closer then previously found closest center, then we have a new closest center.
                    sClosestDist = fClosestDist;
                    sClosest = fClosest;
                    fClosestDist = distPointToCenter;
                    fClosest = centerNr;
                } else if (distPointToCenter < sClosestDist) {
                    // If newly found center is closer than 2nd previous center, then we got new 2nd previous center.
                    sClosestDist = distPointToCenter;
                    sClosest = centerNr;
                }
            }
            // After iterating over all set dists and assignments.
            firstClosestCenters[i] = fClosest;
            secondClosestCenters[i]= sClosest;
            assignments[i] = fClosest;
            map_cntInEachCluster[fClosest]++;
            
        }
        
        // Step 2. Update cluster centers to be averages of points contained within them.
        // All accumulators to 0.0
        for (i = 0; i < k*d; ++i) {
            clusterCenters[i] = 0.0;
        }
        
        // Find mean
        for (i = 0; i < n; ++i) {
            for (dim = 0; dim < d; ++dim) {
                clusterCenters[assignments[i]*d+dim] += points[i*d+dim];
            }
        }
        
        // Divide each center accumulator by count of points in center.
        for (centerNr = 0; centerNr < k; ++centerNr) {
            for (dim = 0; dim < d; ++dim) {
                // If at least 1 point in cluster.
                if (map_cntInEachCluster[centerNr]) {
                    clusterCenters[centerNr*d+dim] = clusterCenters[centerNr*d+dim] / map_cntInEachCluster[centerNr];
                } else {
                    //printf("Warning: Empty cluster when finding first point-to-center!\n");
                    clusterCenters[centerNr*d+dim] = prevClusterCenters[centerNr*d+dim];
                }
            }
        }
        
        // Precalculate R1 for each. Other R1 calculations are only when need to recalculate.
        
        // Step 3
        // All centers belong to the live set.
        for (i = 0; i < k; ++i) {
            isLiveSet[i] = 1;
        }
        
        // For n self->iterations do.
        while (iteration_c < self->iterations) {
            int changed = 0;
            
            // Step 4. OPTRA stage (Optimal transfer)
            
            // Clear next live set
            for (i = 0; i < k; ++i) nextLiveSet[i] = 0;
            
            
            // For each point
            for (i = 0; i < n; ++i) {
                uint currentAssignment = assignments[i];
                // If point belongs to a cluster which is live then do 4a
                if (isLiveSet[currentAssignment]) {
                    // Compute minimum of R2 over all clusters except current.
                    double minR2 = DBL_MAX;
                    int L2 = -1;
                    for (centerNr = 0; centerNr < k; ++centerNr) {
                        if (currentAssignment == centerNr) { 
                            continue;
                        }
                        // Find distance from point to center.
                        double distancePointToCenter = self->metric(d, points + i*d, clusterCenters + centerNr*d);
                        distancePointToCenter *= distancePointToCenter;
                        // Calculate how much it would affect WCSS if we'd add this point to centerNr
                        double R2 = ((double) map_cntInEachCluster[centerNr] * distancePointToCenter) / (map_cntInEachCluster[centerNr] + 1);        
                        // If smaller than currently found.
                        if (R2 < minR2) {
                            minR2 = R2;
                            L2 = centerNr;
                        }
                        
                        
                    }
                    // Having found L2 with smallest R2. If this value is greater than or equal to NC(L1) * distance / (NC(L1) -1) then no reallocate
                    // Find R1
                    // Find distance of L1 to its cluster
                   
                    if (isLiveSet[currentAssignment]) {
                        // 4a. Current center is in the live set. Iterate over all centers.
                        
                        double distanceL1Center = self->metric(d, points + i*d, clusterCenters + currentAssignment*d);
                        distanceL1Center *= distanceL1Center;
                        // How much would WCSS change if we'd remove this center from previous center.
                        if (map_cntInEachCluster[currentAssignment] - 1 > 0) {
                            R1_values[i] = ((double) map_cntInEachCluster[currentAssignment] * distanceL1Center) / (map_cntInEachCluster[currentAssignment] - 1);
                        } else {
                            R1_values[i] = 0;
                        }
                    }
                    double R1 = R1_values[i];
                    // If WCSS would not decrease if switching cluster, then don't switch
                    if (minR2 >= R1) {
                        secondClosestCenters[i] = L2;
                    } else {
                        // Reallocation necessary, I is allocated to L2 and L1 is new IC2
                        secondClosestCenters[i] = firstClosestCenters[i];
                        firstClosestCenters[i] = L2;
                        
                        // Reallocation from one center to another.
                        // Take from previous cluster and update center
                        for (dim = 0; dim < d; ++dim) {
                            if (map_cntInEachCluster[currentAssignment] - 1 > 0) {
                                    clusterCenters[currentAssignment*d+dim] *= (double)map_cntInEachCluster[currentAssignment];
                                    clusterCenters[currentAssignment*d+dim] -= points[i*d+dim];
                                    clusterCenters[currentAssignment*d+dim] /= ((double)map_cntInEachCluster[currentAssignment] - 1);
                            }

                            // Add to new cluster and update center.
                            clusterCenters[L2*d+dim] *= (double)map_cntInEachCluster[L2];
                            clusterCenters[L2*d+dim] += points[i*d+dim];
                            clusterCenters[L2*d+dim] /= ((double)map_cntInEachCluster[L2] + 1);
                        }
                        
                        
                        // Change sizes, assignments and add centers in action to live set.
                        assignments[i] = L2;
                        
                        map_cntInEachCluster[currentAssignment] -= 1;
                        map_cntInEachCluster[L2] += 1;
                        
                        nextLiveSet[currentAssignment] = 1;
                        nextLiveSet[L2] = 1;
                        
                        changed += 1;
                        
                    }
                    
                } else {
                    // 4b Current center is not in the live set. Iterate only on over centers which are in live set.
                    double minR2 = DBL_MAX;
                    uint L2 = -1;
                    for (centerNr = 0; centerNr < k; ++centerNr) {
                        // If center is not live then skip.
                        if (!isLiveSet[centerNr] || currentAssignment == centerNr) { 
                            continue;
                        }
                        double distancePointToCenter = self->metric(d, points + i*d, clusterCenters + centerNr*d);
                        distancePointToCenter *= distancePointToCenter;
                        // Calculate R2 for the center
                        double R2 = ((double) map_cntInEachCluster[centerNr] * distancePointToCenter) / (map_cntInEachCluster[centerNr] + 1);
                        
                        // If smaller than currently found.
                        if (R2 < minR2) {
                            minR2 = R2;
                            L2 = centerNr;
                        }
                        
                        
                    }
                    // Having found L2 with smallest R2. If this value is greater than or equal to NC(L1) * distance / (NC(L1) -1) then no reallocate
                    // Find R1
                    // Find distance of L1 to its cluster
                    
                    double R1 = R1_values[i];
                    // No reallocation necessary, L2 is new IC2
                    if (minR2 >= R1) {
                        secondClosestCenters[i] = L2;
                    } else {
                        // Reallocation necessary, I is allocated to L2 and L1 is new IC2
                        secondClosestCenters[i] = firstClosestCenters[i];
                        firstClosestCenters[i] = L2;
                        
                        // Reallocation
                        // Take from previous
                        for (dim = 0; dim < d; ++dim) {
                            if (map_cntInEachCluster[currentAssignment] - 1 > 0) {
                                    clusterCenters[currentAssignment*d+dim] *= (double)map_cntInEachCluster[currentAssignment];
                                    clusterCenters[currentAssignment*d+dim] -= points[i*d+dim];
                                    clusterCenters[currentAssignment*d+dim] /= ((double)map_cntInEachCluster[currentAssignment] - 1);
                            }

                            // Add to new.
                            clusterCenters[L2*d+dim] *= (double)map_cntInEachCluster[L2];
                            clusterCenters[L2*d+dim] += points[i*d+dim];
                            clusterCenters[L2*d+dim] /= ((double)map_cntInEachCluster[L2] + 1);
                        }
                        
                        
                        // Change assignments, add to cluster size.
                        assignments[i] = L2;
                        
                        map_cntInEachCluster[currentAssignment] -= 1;
                        map_cntInEachCluster[L2] += 1;
                        
                        nextLiveSet[currentAssignment] = 1;
                        nextLiveSet[L2] = 1;
                        
                        changed += 1;
                        
                    }
                }
            }
            
            // Step 5. Stop if live set is empty // Therefore no changes have occured 
            if (!changed) {
                break;
            }
            for (i = 0; i < k; ++i) {
                qtranLiveSet[i] = 0;
            }
        
            // Step 6. Quick-transfer (QTRAN) stage.
            do {
                if (changed == 2) {
                    break;
                }
                memcpy(isLiveSet, nextLiveSet, sizeof(int)*k);  
                for (i = 0; i < k; ++i) {
                    nextLiveSet[i] = 0;
                }
                changed = 0;
                for (i = 0; i < n; ++i) {
                    
                    uint L1 = firstClosestCenters[i];
                    uint L2 = secondClosestCenters[i];
                    
                    // No need to check if they haven't changed in previous n steps.
                    if (!isLiveSet[L1] && !isLiveSet[L2]) {
                        continue;
                    }
                    // Compute values R1 and R2. Need to calculate R1 only if it has changed.
                    if (isLiveSet[L1]) {
                        
                        double distanceL1Center = self->metric(d, points + i*d, clusterCenters + L1*d);
                        distanceL1Center *= distanceL1Center;

                        if (map_cntInEachCluster[L1] - 1 > 0) {
                            R1_values[i] = ((double) map_cntInEachCluster[L1] * distanceL1Center) / (map_cntInEachCluster[L1] - 1);
                        } else {
                            R1_values[i] = 0;
                        }
                    }             
                    double R1 = R1_values[i], distanceL2Center = 0;
                    
                    // Calculate distance between point and L2.
                    for (dim = 0; dim < d; ++dim) {
                        distanceL2Center += pow(points[i*d+dim] - clusterCenters[L2*d+dim],2);
                    }
                    
                    double R2 = ((double) map_cntInEachCluster[L2] * distanceL2Center) / (map_cntInEachCluster[L2] + 1);
                    
                    // If WCSS would decrease, then change clusters.
                    if (R1 < R2) {
                        continue;
                    } else {
                        // Take from previous
                        for (dim = 0; dim < d; ++dim) {
                            if (map_cntInEachCluster[L1] - 1 > 0) {
                                    clusterCenters[L1*d+dim] *= map_cntInEachCluster[L1];
                                    clusterCenters[L1*d+dim] -= points[i*d+dim];
                                    clusterCenters[L1*d+dim] /= (map_cntInEachCluster[L1] - 1);
                            }

                            // Add to new.
                            clusterCenters[L2*d+dim] *= map_cntInEachCluster[L2];
                            clusterCenters[L2*d+dim] += points[i*d+dim];
                            clusterCenters[L2*d+dim] /= (map_cntInEachCluster[L2] + 1);
                        }
                        
                        assignments[i] = L2;
                        
                        map_cntInEachCluster[L1] -= 1;
                        map_cntInEachCluster[L2] += 1;
                        
                        nextLiveSet[L1] = 1;
                        nextLiveSet[L2] = 1;
                        
                        if (!qtranLiveSet[L1]) {
                            qtranLiveSet[L1] = 1;
                            changed++;
                        }
                        if (!qtranLiveSet[L2]) {
                            qtranLiveSet[L2] = 1;
                            changed++;
                        }

                        firstClosestCenters[i] = L2;
                        secondClosestCenters[i] = L1;
                    }
                    
                }
            } while (changed > 0);
            
            memcpy(isLiveSet, qtranLiveSet, sizeof(int)*k);  
            iteration_c++;
        }
        
        printf("Hartigan-Wong stopped after %d self->iterations\n", iteration_c);
    
        ALFREE(map_cntInEachCluster);
        ALFREE(firstClosestCenters);
        ALFREE(secondClosestCenters);
        ALFREE(isLiveSet);
        ALFREE(nextLiveSet);
        ALFREE(R1_values);
        ALFREE(prevClusterCenters);
}
	#endif

int main(int argc, char *argv[]) {

  float tmp = sqrt(2);

  // FIXME: add add for different itnail clsuters ...
  /**
     1. k-means++
     2. Forgy
     3. Partition (assigns points to random cluster and then finds means of these assignments as centers)
     4. Furthest first
     5. firstn (chooses k first points from input file as initial cluster centers).
  **/


  // FIXME: add something
  /*
  OK  #include "lloyd.h"
    #include "elkan.h" <-- current
    #include "hamerly.h"
    #include "macqueen.h"
    #include "hartiganwong.h"
  */

  return 0;
}
