#ifndef configure_cCluster_h
#define configure_cCluster_h
/*
 * Copyright 2016 Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of "HCA-mem-data-anlysis"
 *
 * "HCA-mem-data-anlysis" is a free software: you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * "HCA-mem-data-anlysis" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with Hca-mem-data-anlysis. 
 */


#include "types.h"
#include "def_intri.h"

//#define __MC__inline __attribute__((always_inline))
#define __MC__inline //! where altter is omitted as the SWIG-compiler where complaining (oekseth, 06. apr. 2017)


/**
   @file configure_hcaMem
   @brief provide compiler-logics and 'live-lgoics' to configure/change the flow of exeuctions in the software-process.
   @author Ole Kristian Ekseth (oekseth, 06. july 2016).
   @remarks 
   - the 'configuraiton-swithces' whitch we provide is sued to both chagne the type of lgocis wrt. inferences (ie, the degree of correctenss) and wrt. exeuction-time-optmilaity, ie, may be used to introspect upon the feasiblity of 'such software' wrt. computer-hardware-logics.
   - the compiler-swithces which we provide only represents a sub of psosbile optmziaiton-strategies. An exmaplf of an optmziation-case which we currently do not handle conserns the case where 'the lcoal min is still the local min', ie, to include a 'second-bestScore-value-table' for each column .... ie, wrt. oru "assert_local(debug_cnt_minValueHasCahnged > 0);" in oru "s_dense_closestPair.c". We do not optmize wrt. this case as 'the case' does not seem to happend all to frequently.
 **/

#define __internal_config_useFast  1 //! which is set to "0" if we are to evalaute the 'effect' of not use our improved HCA-mem algorithm (oekseth, 06. july 2016).


//! ***************************************************************

#ifndef configInit__isToAllocate__mineScore__atLibraryLoading
#define configInit__isToAllocate__mineScore__atLibraryLoading 1 //! which if set to "1" implies that we expect the 'user of the hpLysis MINE library to have loaded the glboal optmziez data-structre when intiating 'this object': otherwise expect the MINE-libary to 'make such a loading explict at tach time' (where the latter will/may result in a performance-loss for some cases) (oekseth, 06. des. 2016).
#endif

//! ***************************************************************


#define function_prefix //! which si used in the MINE cofnigruations. 

//! ***************************************************************

//#define assert_local(value) ({;})
#define assert_cluster(value) ({assert(value);})


#define cha_cluster__config__distanceFunctionIsDefined //! which is used to 'enable' the "pcl clsuter-algorithm" in the defitnion.

//! ***************************************************************
#ifndef configure_isTo_useLog
#define configure_isTo_useLog 1
#endif


//! ***************************************************************
#ifndef configure_performanceTests__includeExtensiveAutomatedTestBuilds
#define configure_performanceTests__includeExtensiveAutomatedTestBuilds 1 //! which if used may result in a slow-down wrt. the build-process: provide tdeitlaed ifnroamtion wrt. the performance.
#endif

//! ***************************************************************

#ifndef configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany
#define configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany 0 //! a configuraiton which if set will (a) considerbly icnrease the compialtion-time while (b) on the other hand reduce the exeuction-time (fo many-to-many correlation-comptaution of non-ranked correlation-scores) (oekseth, 06. otk. 2016).
#endif
#ifndef configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany__forSubset
#define configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany__forSubset 1 //! a configuraiton which if set will (a) considerbly icnrease the compialtion-time while (b) on the other hand reduce the exeuction-time (fo many-to-many correlation-comptaution of non-ranked correlation-scores) (oekseth, 06. otk. 2016).
#endif

#if( (configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany == 1) && (configure_buildExtensivePreProcessingFunctions__correlationComputation__manyToMany__forSubset == 1) )
#error "!!\tCould be an incosnstency in your macro-defintions, i,e please investigate."
#endif

//! ***************************************************************

#ifndef configure_function_uniform_useMultInsteadOfDiv
#define configure_function_uniform_useMultInsteadOfDiv 1
#endif

#ifndef configure_function_uniform_notUseConditionals
#define configure_function_uniform_notUseConditionals __internal_config_useFast
#endif
#ifndef configure_function_uniform_dataType
#define configure_function_uniform_dataType int //! eg, "uint", "int", "longint", "ushort", 
#endif
//! Internal defintions based on [above]
//#define __configure_function_uniform_dataType_maxValue INT_MAX
#if(configure_function_uniform_dataType == uint)
#define __configure_function_uniform_dataType_maxValue UINT_MAX //! ie, 32 bit.
#elif(configure_function_uniform_dataType == int)
#define __configure_function_uniform_dataType_maxValue INT_MAX
#elif(configure_function_uniform_dataType == longint)
#define __configure_function_uniform_dataType_maxValue LONG_MAX //! ie, 64 bit.
#elif(configure_function_uniform_dataType == ushort)
#define __configure_function_uniform_dataType_maxValue __USHRT_MAX //! ie, 16-bit.
#else
#error("The data-type was not as expected, ie, please investigate")
#endif


//! ***************************************************************

#ifndef configure_generic_useMulInsteadOf_div 
#define configure_generic_useMulInsteadOf_div __internal_config_useFast
#endif


#ifndef configure_generic_isToInvertCorrelationScoresForMetric__MINE
#define configure_generic_isToInvertCorrelationScoresForMetric__MINE 1 //! which is used to 'invert' the MINE-corlreaiton-scores, ie, to 'make' the MINE-correlaiton-scores 'compatiblity' with the hpLysis metrics such as Pearson and Euclid (oekseth, 06. des. 2016)
#endif

// FIXME: make use of [”elow] ... ie, write an 'api-front-end' which tests/ensures if [”elow] is used ... and write a eprformance-comaprsion wr.t [”elow]
#ifndef configure_SSE_alwasyPad_arraySize_with_SSE_blockSize
#define configure_SSE_alwasyPad_arraySize_with_SSE_blockSize 0
#endif

#ifndef configure_SSE_useFor_mask_implict
//! Note: the "configure_SSE_useFor_mask_implict" is used to toggle on--off SSE-'code' for cases where "mask[index1][index2] == 0" is implictly stated through hte "data[index1][index2] == T_FLOAT_MAX"
// FIXME: update our different 'cases'w rt. [”elow] macro.
// FIXME: write a perofmrance-benchmark where we 'demosntrateæ the high time-cost of this 'approach.
#define configure_SSE_useFor_mask_implict 1 //! whioch if set to "1" is expected to result in a slow-down.
#endif

#ifndef configure_SSE_useFastestVersion
#define configure_SSE_useFastestVersion 1 //! which is sued to explreo dfiferent SSE-permtautiosnw hcih are expected to procue the same resutls (oekseth, 06. setp. 2016)
#endif

#ifndef configure_SSE_useFor_mask_explicit
//! Note: the "configure_SSE_useFor_mask_implict" is used to toggle on--off SSE-'code' for cases where "mask[index1][index2] == 0" is implictly stated through hte "data[index1][index2] == T_FLOAT_MAX"
// FIXME: update our different 'cases'w rt. [”elow] macro.
// FIXME: write a perofmrance-benchmark where we 'demosntrateæ the high time-cost of this 'approach.
#define configure_SSE_useFor_mask_explicit 1 //! whioch if set to "1" is expected to result in a slow-down.
#endif

#ifndef configure_performance_distanceMetricComputation_useBranchingWhenNotRequired
#define configure_performance_distanceMetricComputation_useBranchingWhenNotRequired 0
#endif

#ifndef configure_performance__alglibCmp__useMacrosinsteadOfFunctions
#define configure_performance__alglibCmp__useMacrosinsteadOfFunctions 0
#endif

#ifndef configure_performance__kt_centrality__buildForSlowCases
#define configure_performance__kt_centrality__buildForSlowCases 1 //! which may be used to test different 'slow-impelmetnaiton-effects' of this
#endif
/* #ifndef configure_distanceMetricComputation_kendallFast_useSumInsteadOf_mul */
/* #define configure_distanceMetricComputation_kendallFast_useSumInsteadOf_mul 1 */
/* #endif */


//! ***************************************************************

#ifndef optimize_parallel_useNonSharedMemory_MPI
#define optimize_parallel_useNonSharedMemory_MPI 0
#endif


#ifndef optimize_parallel_singleThreaded //! which may be used to simplify compiaoitnsl on different operaitng systems, and for cases where applsim is not needed (oekseth, 06. feb. 2018)
#define optimize_parallel_singleThreaded 1
 #endif

#ifndef optimize_parallel_2dLoop_useFast
#define optimize_parallel_2dLoop_useFast 0 //! a macro-parameter which is used if 'rows' are to be compared in parallel
#endif


#ifndef functionConfiguration__isToAssume__allDistanceMetricsUseMacroDefinedFunctions
#define functionConfiguration__isToAssume__allDistanceMetricsUseMacroDefinedFunctions 1
#endif

#ifndef configureDebug_useExntensiveTestsIn__tiling
#define configureDebug_useExntensiveTestsIn__tiling 0  //! where the latter is set to "0" as we are currenlty investigating data-sets with where sum fotne ends up corretly being "inf" (oekseth, 06. apr. 2017)
#endif

#if(configureDebug_useExntensiveTestsIn__tiling == 1)
#define configureDebug_useExntensiveTestsIn__tiling_verbous 1
#define assert_possibleOverhead_testisNot_inf(value) ({ assert(0 == isinf(value));})
#define assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(value) ({assert(value);})
#else
#define assert_possibleOverhead_testisNot_inf(value) ({;}) //! ie, then we do nothing
#define assert_possibleOverhead_timeCostHeavy_inCorrMacrosinMacros(value) ({;})
#define configureDebug_useExntensiveTestsIn__tiling_verbous 0
#endif

#define assert_possibleOverhead(value) ({ assert(value);})

#ifndef globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct
#define globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct 0 //! where we set the latter to 'do-not-use' iot. reduce the compilation-time (oekseth, 06. feb. 2017).
#define globalConfig__isToInclude_codeSupportFor__metrics__groupOf_direct__generateWarning() ({fprintf(stderr, "!!\t Have omitted support for the \"groupOf_direct\" simalrity-metric-group: if the latter is of interest then please incldue the latter option in your compilation-installation proceudre of the hpLyssi and Knitting-Tools software. If the latter soudns challenging, hten please send an email to the senior developer [oekseth@gmail.com]. Observaiton at [%s]:%s:%d\n", __FUNCTION__, __FILE__, __LINE__);})
#endif


#endif //! EOF 
