#ifndef macros_h
#define macros_h
//! return the name of the variable as seen in the calls to it when reading this comment.
#define macro_getVarName(varname) #varname  

/* //! A short-hand to avoid getting the full path of a file. */
/* //! @remarks used to reduce the clutter in some of the (informative) "printf(...)" and "fprintf(...)" calls. */
/* #define __FNAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__) */

#ifndef macro_max
#define macro_max(x, y) ((x) > (y) ? (x) : (y))
#endif
#ifndef macro_min
#define macro_min(x, y) ((x) < (y) ? (x) : (y))
#endif

//! @return a default standarized error-message (oekseth, 06. des. 2016).
#define __get_defaultErrorMessage() "If the latter 'request' sounds odd, then please contact the hpLysis Cluster Analysis Software senior developer at [oekseth@gmail.com], or visit the hpLysis web-page at 'www.knittingTools.org'.", __FUNCTION__, __FILE__, __LINE__


#endif
