#include "kt_list_1d.h"
//#include "kt_matrix_fileReadTuning.h"
//#include "kt_list_1d_string.h"

//! ----------------------- start: HEAP-logics:
//! Note: [below] is sinpred by: "http://robin-thomas.github.io/max-heap/"
#define MiF__LCHILD(x) 2 * x + 1
#define MiF__RCHILD(x) 2 * x + 2
#define MiF__PARENT(x) (uint)((x - 1) * 0.5)

//! Insert the item into a pre-allcoated data-strucutre.
// Note: psotion the "newItem" node at the right in the max-heap.
// FIXME[eval]: compare/meausre difference between [â€elow] implmentations:
#define MiF__heap__insertNode(currentPos, list, list_size, newItem) ({ uint i = currentPos; while(i && __MiF__objLessThan(list[MiF__PARENT(i)], newItem) ) { list[i] = list[MiF__PARENT(i)]; i = MiF__PARENT(i);} assert(i < list_size); list[i] = newItem;})
//#define MiF__heap__insertNode(currentPos, list, list_size, newItem) ({ uint i = currentPos; while(i && (newItem > list[MiF__PARENT(i)]) ) { list[i] = list[MiF__PARENT(i)]; i = MiF__PARENT(i);} assert(i < list_size); list[i] = newItem;})
//! Note: [â€elow] is taken from "/home/klatremus/poset_src/data_analysis/hplysis-cluster-analysis-software/src/external_cpy/valgrind-3.12.0/exp-sgcheck/tests/hackedbz2.c"
// FIXME[conceptual]: validte correctness of [below].
#define MiF__heap__insertNode__alt(currentPos, heap, list_size, newItem) ({ \
  loint zz;       \
  zz = currentPos;                             \
  while (newItem < heap[zz >> 1]) {      \
  heap[zz] = heap[zz >> 1];                       \
  zz >>= 1;                                       \
  }                                                  \
  heap[zz] = newItem;})
//! Apply a generic heapify-routine:
#define MiF__heapify(type_t, list, list_size, heap_index) ({ \
  loint cnt_iter = 0; uint largest = UINT_MAX;\
  while( /*(largest != heap_index) && */ (heap_index < (list_size+1) ) ) { \
  /*largest = (MiF__LCHILD(heap_index) < list_size && list[MiF__LCHILD(heap_index)] > list[heap_index]) ? MiF__LCHILD(heap_index) : heap_index ;*/ \
  largest = ( (MiF__LCHILD(heap_index) < list_size) && __MiF__objGreaterThan(list[MiF__LCHILD(heap_index)] , list[heap_index]) ) ? MiF__LCHILD(heap_index) : heap_index ; \
  /*if( (MiF__RCHILD(heap_index) < list_size) && (list[MiF__RCHILD(heap_index)] > list[largest]) ) { largest = MiF__RCHILD(heap_index); } */ \
  if( (MiF__RCHILD(heap_index) < list_size) && __MiF__objGreaterThan(list[MiF__RCHILD(heap_index)] , list[largest]) ) { largest = MiF__RCHILD(heap_index); } \
  if(largest != heap_index) { MF__SWAP_complexType(type_t, (list[heap_index]), (list[largest])) ; heap_index = largest; \
  } else {heap_index = (list_size+1);} /*! ie, then stop the tree-traversal-routine. */ \
  assert(cnt_iter++ < (list_size*list_size)); }})
//! Esnure that the heap-list-strucutre is satsified for all verties.
#define MiF__heapify__all(type_t, list, list_size) ({ for(int i = (list_size - 1) / 2; i >= 0; i--) {int tmp_index = i; MiF__heapify(type_t, list, list_size, tmp_index);}})
/* void deleteNode(maxHeap *hp) { */
/*   if(hp->size) { */
/*     printf("Deleting node %d\n\n", hp->elem[0].data) ; */
/*     hp->elem[0] = hp->elem[--(hp->size)] ; */
/*     hp->elem = realloc(hp->elem, hp->size * sizeof(node)) ; */
/*     heapify(hp, 0) ; */
/*   } else { */
/*     printf("\nMax Heap is empty!\n") ; */
/*     free(hp->elem) ; */
/*   } */
/* } */
/* void buildMaxHeap(maxHeap *hp, int *arr, int size) { */
/*   /\* int i ; *\/ */

/*   /\* // Insertion into the heap without violating the shape property *\/ */
/*   /\* for(i = 0; i < size; i++) { *\/ */
/*   /\*   if(hp->size) { *\/ */
/*   /\*     hp->elem = realloc(hp->elem, (hp->size + 1) * sizeof(node)) ; *\/ */
/*   /\*   } else { *\/ */
/*   /\*     hp->elem = malloc(sizeof(node)) ; *\/ */
/*   /\*   } *\/ */
/*   /\*   node nd ; *\/ */
/*   /\*   nd.data = arr[i] ; *\/ */
/*   /\*   hp->elem[(hp->size)++] = nd ; *\/ */
/*   /\* } *\/ */

/*   // Making sure that heap property is also satisfied */
/*   MiF__heapify__all(t_float, (self->list), (self->list_size)); */
/*   /\* for(i = (hp->size - 1) / 2; i >= 0; i--) { *\/ */
/*   /\*   heapify(hp, i) ; *\/ */
/*   /\* } *\/ */
/* } */
//! ----------------------- end: HEAP-logics:


//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_float_t init__s_kt_list_1d_float_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_float_t self; 
  self.list = NULL;   self.list_size = 0;   self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    const t_float empty_0 = 0;
    self.list = allocate_1d_list_float(list_size, empty_0);
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = T_FLOAT_MAX; //! ie, 'set to empty'.
    }
  }
  //! @return itnated object:
  return self;
}
s_kt_list_1d_float_t setToEmpty__s_kt_list_1d_float_t() {
  return init__s_kt_list_1d_float_t(0);
}
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
bool get_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = T_FLOAT_MAX;
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(*scalar_result != T_FLOAT_MAX) {return true;}
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
void set_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float valueTo_set) {
  assert(self);
#include "kt_list_1d__stub__alloc_float.c"
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
}
//! Increment the score-count assicatred to "index" (oekseth, 06. apr. 2017)
void increment_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, uint index, t_float valueTo_set) {
  t_float val_to_set = valueTo_set; assert(valueTo_set != T_FLOAT_MAX);
  if(index < self->list_size) {
    const t_float old_value = self->list[index];
    if(old_value != T_FLOAT_MAX) {val_to_set += old_value;}
  }
  //! Set the value:
  set_scalar__s_kt_list_1d_float_t(self, index, val_to_set);
}
//! De-allcoates the s_kt_list_1d_float_t object.
void free__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self) {
  assert(self);
  if(self->list) {
    free_1d_list_float(&(self->list)); 
    self->list = NULL; self->list_size = 0;
  }
   self->current_pos = 0;
} 
//! Add a vertex to the heap (oekseth, 06. jul. 2017).
void addTo_heap_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self, const t_float valueTo_set) {
  assert(valueTo_set != T_FLOAT_MAX);
  const uint index = self->current_pos;
  assert(index != UINT_MAX);
  if(self->current_pos >= self->list_size) {
#include "kt_list_1d__stub__alloc_float.c"
  }
  //! 
  //! Insert the node:
#define __MiF__objLessThan(obj1, obj2) obj1 < obj2
  MiF__heap__insertNode(index, (self->list), (self->list_size), valueTo_set);
#undef __MiF__objLessThan
  //! 
  //! Incrmeent current-pos:
  self->current_pos++;
}
//! Consutrct a max-heap (oekseth, 06. jul. 2017).
void buildHeap_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self) {
  /* if(false) { */
  /*   t_float *list = self->list; */
  /*   int heap_index = 0; */
  /*   int largest = 1; */
  /*   MF__SWAP_complexType(t_float, (list[heap_index]), (list[largest])) ; heap_index = largest; */
  /* } */
  { //! Set the current-pos-key:
    self->current_pos = 0;
    for(uint i = 0; i < self->list_size; i++) {
      if(self->list[i] != T_FLOAT_MAX) {
	self->current_pos = i;
      }
    }
    (self->current_pos)++;
  }
  // Making sure that heap property is also satisfied
#define __MiF__objGreaterThan(obj1, obj2) obj1 > obj2 
  MiF__heapify__all(t_float, (self->list), (self->list_size));
#undef __MiF__objGreaterThan
}
//! Remove the top-node in a max-heap (oekseth, 06. jul. 2017).
void deleteHeapNode_max__s_kt_list_1d_float_t(s_kt_list_1d_float_t *self) {
  if(self->current_pos > 0) {
    assert(self->current_pos != UINT_MAX); //! ie, as we expect latter to ahve been set.
    assert(self->current_pos > 0);
    /*     printf("Deleting node %d\n\n", hp->elem[0].data) ; */
    self->list[0] = self->list[self->current_pos-1];
    self->current_pos--; //! ie, remove the element
    /*     hp->elem[0] = hp->elem[--(hp->size)] ; */
    /*     hp->elem = realloc(hp->elem, hp->size * sizeof(node)) ; */
    /*     heapify(hp, 0) ; */
    //! Star thte heap-operaitons:
    int tmp_index = 0;
#define __MiF__objGreaterThan(obj1, obj2) obj1 > obj2 
    MiF__heapify(t_float, (self->list), (self->current_pos), tmp_index);
#undef __MiF__objGreaterThan
  }
  /*   if(hp->size) { */

  /*   } else { */
  /*     printf("\nMax Heap is empty!\n") ; */
  /*     free(hp->elem) ; */
  /*   } */
}

//! *******************************************************************
//! *******************************************************************
//! *******************************************************************

//! @return an itnailted list-object with size "list_size"
s_kt_list_1d_uint_t init__s_kt_list_1d_uint_t(uint list_size) {
  assert(list_size != UINT_MAX);
  //assert(list_size > 0);
  s_kt_list_1d_uint_t self; 
  self.list = NULL;   self.list_size = 0; self.current_pos = 0;
  if(list_size > 0) {
    self.list_size = list_size;
    const uint empty_0 = 0;
    self.list = allocate_1d_list_uint(list_size, empty_0);
    for(uint i = 0; i < list_size; i++) {
      self.list[i] = UINT_MAX; //! ie, 'set to empty'.
    }
  }
  //! @return itnated object:
  return self;
}
//! Access the self->list[index] object, a result which is used to udpate the "*scalar_result" (oekseth, 06. mar. 2017)
bool get_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint *scalar_result) {
  assert(self); assert(scalar_result);
  *scalar_result = UINT_MAX;
  if(index < self->list_size) {
    assert(self->list);
    *scalar_result = self->list[index];
    if(*scalar_result != UINT_MAX) {return true;}
  }
  return false; //! ie, as we at this exec-point assume the 'value was not set at "index"'.
}
//! Programatilly set a given value: extend list if "index >= self->list_size"
void set_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint valueTo_set) {
  assert(self);
  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     const uint empty_0 = 0;
    uint *new_list = allocate_1d_list_uint(list_size, empty_0);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = UINT_MAX; //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(old_list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      free_1d_list_uint(&(self->list)); 
    } 
    //! Update pointer:
    self->list = new_list;    
  }
  //! ----------------------------
  //! Set the value;
  assert(index < self->list_size);
  self->list[index] = valueTo_set;
}

//! Increment the score-count assicatred to "index" (oekseth, 06. apr. 2017)
void increment_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self, uint index, uint valueTo_set) {
  uint val_to_set = valueTo_set; assert(valueTo_set != UINT_MAX);
  if(index < self->list_size) {
    const uint old_value = self->list[index];
    if(old_value != UINT_MAX) {val_to_set += old_value;}
  }
  //! Set the value:
  set_scalar__s_kt_list_1d_uint_t(self, index, val_to_set);
}
//! De-allcoates the s_kt_list_1d_uint_t object.
void free__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *self) {
  assert(self);
  if(self->list) {
    free_1d_list_uint(&(self->list)); 
    self->list = NULL; self->list_size = 0;
  }
  self->current_pos = 0;
}

//! *******************************************************************
//! *******************************************************************
//! *******************************************************************


  
