  if(index >= self->list_size) {
    const uint list_size = (12 + index)*2;
    const uint old_list_size = self->list_size;
    self->list_size = list_size;     
    s_ktType_kvPair_t *new_list = MF__allocate__s_ktType_kvPair(list_size);
    for(uint i = 0; i < list_size; i++) {
      new_list[i] = MF__init__s_ktType_kvPair(); //! ie, 'set to empty'.
    } 
    //! Handle the copy-operation:
    if(self->list_size != 0) {
      for(uint i = 0; i < old_list_size; i++) {      
	new_list[i] = self->list[i];
      }
      //! ------------------------------
      //! De-allcoate 'old' list:
      if(self->list != NULL) {
	MF__free__s_ktType_kvPair((self->list)); 
      }
    }
    //! Update pointer:
    self->list = new_list;    
  }
