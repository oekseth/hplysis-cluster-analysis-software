# _Z33export__singleCall__s_kt_matrix_tP11s_kt_matrixPKcP21s_kt_longFilesHandler
rm -f hpLysis_subset.pm;
swig -perl5 -I../src/  hpLysis_subset.i;
# --
g++ -I../src/ -L../src/ -l liblib_x_hpLysis.so -c `perl -MConfig -e 'print join(" ", @Config{qw(ccflags optimize cccdlflags)}, "-I$Config{archlib}/CORE")'` -pthread -Wall -fPIC  -fopenmp -mssse3 -Wall -fopenmp -mssse3 -Wno-unused-variable -Wno-unused-value -fpermissive -fopenmp -Wno-unused-but-set-variable -Wno-address -Wno-unused-function hpLysis_subset_wrap.c;

COMPILER_PARAMS="-pthread -Wall -fPIC  -fopenmp -mssse3 -Wall -fopenmp -mssse3 -Wno-unused-variable -Wno-unused-value -fpermissive -fopenmp -Wno-unused-but-set-variable -Wno-address -Wno-unused-function";
#! Note: for a brief introduciton to compiler-params, see "http://tldp.org/LDP/abs/html/varassignment.html".
LOCAL_FILESET=$(cat compile_depend.tsv) #! where latter is generated using our "perl aux_buildListOf_compilerDependencies.pl"
g++ $COMPILER_PARAMS  `perl -MConfig -e 'print $Config{lddlflags}'`  $LOCAL_FILESET  hpLysis_subset_wrap.o -o hpLysis_subset.so;
#g++ -pthread -Wall -fPIC  -fopenmp -mssse3 -Wall -fopenmp -mssse3 -Wno-unused-variable -Wno-unused-value -fpermissive -fopenmp -Wno-unused-but-set-variable -Wno-address -Wno-unused-function  `perl -MConfig -e 'print $Config{lddlflags}'` ../src/fast_log.c ../src/parse_main.c ../src/kt_longFilesHandler.c ../src/math_generateDistribution.c ../src/matrix_deviation.c ../src/kt_set_2dsparse.c    ../src/s_dataStruct_matrix_dense.c ../src/correlation_rank.c ../src/kt_set_1dsparse.c ../src/correlation_sort.c ../src/correlation_base.c ../src/correlation_rank_rowPair.c ../src/e_template_correlation_tile.c  ../src/kt_matrix_base.c ../src/kt_list_1d.c ../src/kt_list_1d_string.c ../src/kt_matrix.c ../src/kt_metric_aux.c ../src/kt_matrix_cmpCluster.c  ../src/kt_longFilesHandler.c ../src/kt_distance__groupOf_direct__each.c ../src/kt_distance__groupOf_direct__1ToMany.c ../src/kt_distance__groupOf_direct.c ../src/matrix_cmpAlg_ROC.c ../src/matrix_deviation.c ../src/kt_matrix_filter.c  ../src/kt_distance.c ../src/kt_distance_cluster.c ../src/kt_sparse_sim.c ../src/hp_distance.c ../src/hp_distance_wrapper.c ../src/hpLysis_api.c ../src/hp_api_fileInputTight.c  hpLysis_subset_wrap.o -o hpLysis_subset.so;


#g++ -I../src/ -I../hca-memory/src/ -L../src/ -llib_ansiC_kt_hpLysis.so -c `perl -MConfig -e 'print join(" ", @Config{qw(ccflags optimize cccdlflags)}, "-I$Config{archlib}/CORE")'` -pthread -Wall -fPIC -std=c99 -fopenmp -mssse3 -Wall -std=c99 -fopenmp -mssse3 -Wno-unused-variable -Wno-unused-value -fpermissive -fopenmp -Wno-unused-but-set-variable -Wno-address -Wno-unused-function -I../src/ kt_hpLysis.c kt_hpLysis_wrap.c;
#gcc -c `perl -MConfig -e 'print join(" ", @Config{qw(ccflags optimize cccdlflags)}, "-I$Config{archlib}/CORE")'` kt_hpLysis.c kt_hpLysis_wrap.c
#g++ `perl -MConfig -e 'print $Config{lddlflags}'` hp_api_fileInputTight.o -o hpLysis_subset.so;
# gcc `perl -MConfig -e 'print $Config{lddlflags}'` kt_hpLysis.o kt_hpLysis_wrap.o -o kt_hpLysis.so ;
