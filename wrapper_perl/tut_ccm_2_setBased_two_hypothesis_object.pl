#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 


=head
    @file "tut_ccm_2_setBased_two_hypothesis_object.pl"
    @brief demosntrates how our "hpLysis.pm" class  may be used to evakuate hypotehsis (ie, use Cluster-Comparison-Matrix (CCM)) bweetween two sets of hypotehsis.
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
=cut

{ #! A permtuation of [abvoe] where we use the "hpLysis.pm" PErl-object:
     #! Specify the relationshisp to investigate:
    my $hypo_1 = [[1, 1, 2, 3, 3]]; #! where latter describe the cluster-emmberships assicated to veritces in the first hypohesis.
    my $hypo_2 = [[3, 3, 1, 3, 3]]; #! where latter describe the cluster-emmberships assicated to veritces in the second hypohesis.
    #! Use ARI to evluate simlairty in the [ªbove] hypohesis:
    my $enum_ccm_setBased = "ARI"; 
    #! Add the data:
    my $obj_hp = Algorithm::hpLysis->new();
    $obj_hp->set_matrix_1($hypo_1); $obj_hp->set_matrix_2($hypo_2);
    #! Apply logics:
    my $matrix = $obj_hp->compute__CCM_setBased($enum_ccm_setBased); #! ie, comptue the CCM.
    #! 
    #! Write out the matrix:
    for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
	my $row = $matrix->[$row_id];
	my $ncols = scalar(@{$row});
	for(my $col_id = 0; $col_id < $ncols; $col_id++) {
	    my $val = $row->[$col_id];
	    #my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
	    # my $head = $arrOf_rows->[$row_id]; my $col = $arrOf_rows->[$col_id]; #! where we in the latter 'makes use' of the proeprty asusmgin taht the simalrity-matrix consistes of "rows" x "rows", ie, which explains why we do Not invesitgte wrt. teh 'column-names'.
	    #printf("float:[$head][$col]='$val', at %s:%d\n", __FILE__, __LINE__);
	    printf("\tfloat:[$row_id][$col_id]='$val', at %s:%d\n", __FILE__, __LINE__);
	}
    }    
}
