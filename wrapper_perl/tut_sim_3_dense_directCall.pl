#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 
=head
    @file "tut_sim_3_dense_directCall.pl"
    @brief demosntrates how a 'one-liner' may be used to compute simliarty for a 'dense' matrix=[[]] of (head, tail) scores.
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
=cut

{ #! Comptue simalrity using a real-world data-input-dset:t eh data-set described the spotting-gread of guenien-pgics, and where deitals of the latter is foudn ion the hpLysis-docuemtantion.
    my $matrix = [ 
	[0.000000, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.072906],
	[0.321928, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.321928],
	[0.072906, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.000000]
	];
    my $arrOf_rows = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    my $arrOf_cols  = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    { #! MAke a 'driect call' to the simalrity-emtic comptaution:
	#! Note: to simplify our code-exampel, we in this 'example' do Not use the ª[boe] string-inptus.
	#! Note: the resutl-value in [”elow] is a perl-version of the itnernal data-object.	
	my $obj_hp = Algorithm::hpLysis->new($matrix); #! where the latter matrix is 'used direclty':
	#!
	#! Comptue teh simliarty:
	my ($matrix, $arrOf_rows_loc, $arrOf_cols_loc) = $obj_hp->compute__similarity("Euclid"); #! ie, comptue using the Euclidian simliarty-metric

	#! 
	#! Write out the matrix:
	for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
	    my $row = $matrix->[$row_id];
	    my $ncols = scalar(@{$row});
	    for(my $col_id = 0; $col_id < $ncols; $col_id++) {
		my $val = $row->[$col_id];
		#my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
		printf("float:[$row_id][$col_id]='$val', at %s:%d\n", __FILE__, __LINE__);
	    }
	    #hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	    #$index++;
	}    
    }
}
