 %module hpLysis_subset

  /**
     @brief provide a wrapper to our hpLsysis data-set
     @remarks -- related reference:
     -- Perl: "http://bioinformatics.gmu.edu/software/SWIG/Perl5.html"

     @remarks low-level access:
     // --------------------------------
     struct Vector {
     double x,y,z;
     };
     struct Vector *new_Vector();
     void           delete_Vector(Vector *v);
     double         Vector_x_get(Vector *obj)
     void           Vector_x_set(Vector *obj, double x)
     double         Vector_y_get(Vector *obj)
     void           Vector_y_set(Vector *obj, double y)
     double         Vector_z_get(Vector *obj)
     void           Vector_z_set(Vector *obj, double z)
     // --------------------------------
   **/

 %ignore s_kt_longFilesHandler_t;
 %{
   
   /* Includes the header in the wrapper code */

   //#include "../src/configure_cCluster.h"

//#include "../src/def_intri.h"
#include "types.h"
#include "types_base.h"
o#include "log_clusterC.h"
#include "e_kt_correlationFunction.h"
#include "kt_longFilesHandler.h"
#include "kt_metric_aux.h"
#include "hp_ccm.h"
#include "hp_distance.h"
#include "kt_sparse_sim.h"
#include "hp_api_fileInputTight.h"
#include "kt_terminal.h"


#include "kt_matrix.h"
#include "kt_matrix_cmpCluster.h"
#include "kt_list_1d.h"
#include "kt_list_1d_string.h"
#include "hpLysis_api.h"


/*    //#include "def_typeOf_float.h" */
/* #include "kt_api.h" */
/* // FIXME: make use of [”elow] ... ie, provide access/support for computign the 'eigen-vector-centrliaty' */
#include "e_kt_eigenVectorCentrality.h"
#include "kt_centrality.h"
#include "kt_clusterAlg_hca__node.h"
#include "parse_main.h"
#include "kt_clusterAlg_fixed_resultObject.h" //! which hold 'access' to the k-means-clsuter-object (oekseth, 06. otk. 2016)
#include "kt_clusterAlg_hca_resultObject.h" //! (oekseth, 06. nov. 2016).
#include "kt_clusterAlg_SOM_resultObject.h" //! (oekseth, 06. nov. 2016).
#include "kt_clusterAlg_hca__node.h" //! (oekseth, 06. nov. 2016).
#include "e_kt_clusterAlg_hca_type.h"
#include "kt_matrix_filter.h"
#include "kt_clusterResult_condensed.h"

#include "hp_api_fileInputTight.h"
#include "kt_swig_retValObj.h"
   %}

/**
   @remakrs docuemtnation:
   - general itnroduction to SWIG: http://www.swig.org/Doc1.3/Perl5.html
 **/


%include "typemaps.i"
/* void test_add(uint, uint, uint *OUTPUT); */
/* void test_add_int(int, int, int *OUTPUT); */
  //void test_add(int, int, int *OUTPUT);
/* int  test_sub(int *INPUT, int *INPUT); */

//! File: "hpLysis_api.h"
void get_enumOf__e_hpLysis_clusterAlg_t__retVal(const char *INPUT,  uint *OUTPUT);

//! File: "kt_matrix_cmpCluster.h", for matrix-based CCM, eg, "silhouette
void getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t__retVal__uint(const char *INPUT,  uint *OUTPUT);
//void getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t__retVal(const char *INPUT,  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t *OUTPUT);
//void getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t__retVal(const char *INPUT,  e_kt_matrix_cmpCluster_clusterDistance__cmpType_t *OUTPUT);

%inline %{


  %} 


//! File: "kt_matrix_cmpCluster.h"
//! File: "kt_matrix_cmpCluster.h", for set-based CCM (eg, Rands Index).
//void getEnumOf__e_kt_matrix_cmpCluster_metric_t__retVal(const char *INPUT,  e_kt_matrix_cmpCluster_metric_t *OUTPUT);
void getEnumOf__e_kt_matrix_cmpCluster_metric_t__retVal__uint(const char *INPUT,  uint *OUTPUT);

//! File: "kt_metric_aux.h"
void get_stringOf_enum__e_kt_correlationFunction_t_scalar(const e_kt_correlationFunction_t, s_kt_swig_retValObj_t *OUTPUT);
//! File: "kt_metric_aux.h"
void get_enumBasedOn_configuration_scalar(const char *INPUT, uint *OUTPUT);
void get_enumBasedOn_configuration_scalar__tmp(const char *INPUT, uint *OUTPUT);
//void get_enumBasedOn_configuration_scalar(const char *INPUT, e_kt_correlationFunction_t *OUTPUT);
/* %inline %{ */
/*   void __wrapper__get_enumBasedOn_configuration_scalar(const char *INPUT, int *OUTPUT) { */
/*     e_kt_correlationFunction_t tmp = 1; */
/*     get_enumBasedOn_configuration_scalar(INPUT, &tmp); */
/*     *OUTPUT = (int)tmp; */
/*   } */
/*   %} */ 

///*
//! File: "kt_metric_aux.h"
void get_typeOf_correlationPreStep_fromString_scalar(const char *INPUT, uint *OUTPUT);
//! File: "kt_metric_aux.h"
void get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_scalar(const e_kt_categoryOf_correaltionPreStep_t, s_kt_swig_retValObj_t *OUTPUT);
//!
//!
//! File: "kt_list_1d_string.h"
void setToEmpty__s_kt_list_1d_string_t(s_kt_list_1d_string_t *OUTPUT);
void get_count__s_kt_list_1d_string_t(const s_kt_list_1d_string_t *INPUT, uint *OUTPUT);
void set_string__s_kt_list_1d_string(s_kt_list_1d_string_t *INPUT, const uint, char *INPUT);
void set_string__s_kt_list_1d_stringObj(s_kt_list_1d_string_t *self, const uint index_pos, s_kt_swig_retValObj_t *INPUT);
//void set_string__s_kt_list_1d_stringObj(s_kt_list_1d_string_t *self, const uint index_pos, const s_kt_swig_retValObj_t *INPUT);
void get_string__s_kt_list_1d_string(const s_kt_list_1d_string_t *INPUT, const uint, s_kt_swig_retValObj_t *OUTPUT);
//! De-allcoate the s_kt_list_1d_string_t object.
void free__s_kt_list_1d_string(s_kt_list_1d_string_t *INOUT);
//!
//!
//! 
//! File: "kt_list_1d.h"
void init__ref_s_kt_list_1d_float_t(s_kt_list_1d_float_t *OUTPUT, uint);
//! De-allcoates the s_kt_list_1d_float_t object.
void free__s_kt_list_1d_float_t(s_kt_list_1d_float_t *INOUT);
void get_count__s_kt_list_1d_float_t(const s_kt_list_1d_float_t *INPUT, uint *OUTPUT);
bool get_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *INPUT, uint, t_float *OUTPUT);
void set_scalar__s_kt_list_1d_float_t(s_kt_list_1d_float_t *INPUT, uint, t_float);
//!
//!
//! 
//! File: "kt_list_1d.h"
void init__ref_s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *OUTPUT, uint);
//! De-allcoates the s_kt_list_1d_uint_t object.
void free__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *INOUT);
void get_count__s_kt_list_1d_uint_t(const s_kt_list_1d_uint_t *INPUT, uint *OUTPUT);
bool get_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *INPUT, uint, uint *OUTPUT);
void set_scalar__s_kt_list_1d_uint_t(s_kt_list_1d_uint_t *INPUT, uint, uint);
//!
//!
//! 
//! File: "kt_list_1d.h"
void init__ref_s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *OUTPUT, uint);
void get_count__s_kt_list_1d_pairFloat_t(const s_kt_list_1d_pairFloat_t *INPUT, uint *OUTPUT);
//! De-allcoates the s_kt_list_1d_pairFloat_t object.
void free__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *INOUT);
//! Add logics to 'handle' the 'retunging of a tripelt' task:
%apply t_float *OUTPUT { t_float *scalar_head, t_float *scalar_tail, t_float *scalar_score };
bool get_scalar__allVars__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *INPUT, uint,  t_float *scalar_head, t_float *scalar_tail, t_float *scalar_score);
//! SEt the value:
void set_scalar__allVars__s_kt_list_1d_pairFloat_t(s_kt_list_1d_pairFloat_t *INPUT, uint, const uint, const uint, const t_float);

//!

//!
//! 
//!
//! File: "kt_matrix.h"
void getProp__nrows__s_kt_matrix_t(s_kt_matrix_t *INPUT, uint *OUTPUT);
void getProp__ncols__s_kt_matrix_t(s_kt_matrix_t *INPUT, uint *OUTPUT);
// --
void init__s_kt_matrix(s_kt_matrix_t *OUTPUT, uint,  uint, bool) ;
//void init__s_kt_matrix(s_kt_matrix_t *OUTPUT, const uint, const uint, const bool) ;
void setTo_empty__s_kt_matrix_t(s_kt_matrix_t *OUTPUT);
void free__s_kt_matrix(s_kt_matrix_t *INOUT);
//void free__s_kt_matrix(s_kt_matrix_t *INPUT);
// --
void set_string__s_kt_matrix(s_kt_matrix_t *INPUT, const uint, char *INPUT, const bool);
//! A wrapper-fucntion among others used by SWIG-rwapper-languages to simplify data-access (oekset, 06. apr. 2017).
void set_string__s_kt_matrix__obj(s_kt_matrix_t *INPUT, const uint, const bool, s_kt_swig_retValObj_t *INPUT);
void set_weight__s_kt_matrix(s_kt_matrix_t *INPUT, const uint, const t_float) ;
void set_cell__s_kt_matrix(s_kt_matrix_t *INPUT, const uint, uint, const t_float);
// --
void get_cell__s_kt_matrix(const s_kt_matrix_t *INPUT, const uint, uint, t_float *OUTPUT);
void get_string__s_kt_matrix(const s_kt_matrix_t *INPUT, const uint, const bool addFor_column, char **OUTPUT);
void get_string__s_kt_matrix__swig(const s_kt_matrix_t *INPUT, const uint, const bool, s_kt_swig_retValObj_t *OUTPUT);
// --
bool export__singleCall__s_kt_matrix_t(s_kt_matrix_t *INPUT, const char *INPUT, s_kt_longFilesHandler_t *INPUT);
//! Laod data, intlaizing our "self" s_kt_matrix_t" object (oekseth, 06. apr. 2017). 
 void import__s_kt_matrix_t(s_kt_matrix_t *self, const char *INPUT);
//!
//!
//! 
//! File: "kt_clusterResult_condensed.h"
void initSetToEmpty__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *OUTPUT);
void free__s_kt_clusterResult_condensed_t(s_kt_clusterResult_condensed_t *INPUT);
// --
void s_kt_clusterResult_condensed_t__getObject__arr_names_clustering(s_kt_clusterResult_condensed_t *INPUT, s_kt_list_1d_string_t *OUTPUT);
void s_kt_clusterResult_condensed_t__getObject__arrOf_clusterRelationships(s_kt_clusterResult_condensed_t *INPUT, s_kt_list_1d_pairFloat_t *OUTPUT);
void s_kt_clusterResult_condensed_t__getObject__arr_vertexTo_centroid(s_kt_clusterResult_condensed_t *INPUT, s_kt_list_1d_uint_t *OUTPUT);
void s_kt_clusterResult_condensed_t__getObject__arr_hcaResult_distanceFromRoot(s_kt_clusterResult_condensed_t *INPUT, s_kt_list_1d_float_t *OUTPUT);
void s_kt_clusterResult_condensed_t__getObject__arr_clusterSets_child_countOfScoreSum(s_kt_clusterResult_condensed_t *INPUT, s_kt_list_1d_float_t *OUTPUT);
void s_kt_clusterResult_condensed_t__getObject__arr_clusterSets_parent_countOfScoreSum(s_kt_clusterResult_condensed_t *INPUT, s_kt_list_1d_float_t *OUTPUT);
/**
   @brief export the object to a TSV-sparse-fileformat (oekseth, 06. apr. 2017).
   @param <self>  the object which hodl the alredy-comptued cluster-results
   @param <fileName_result> the new fiel to constuct: if Not set, then we export to STDOUT
   @param <seperator> optioanl: the sepertor between tntiees, eg, "&" for Latex, "," for tsv and "," for CSV
   @param <seperator_newLine> optioanl: the sepertor between each row, eg, "\\" for Latex, and "\n" for tsv and  CSV
   @return true upon success.
   @remarks in the export-format we cpature use-cases ipmictly described (among others) in: "hpLysisVisu_api__initClusterResult(..)" ("hpLysisVisu_server.js"), "export_result_clusterResults(..)" ("hpLysis.pm"), "hpLysisVisu_api__initClusterResult(..)" tut-example ("tut_hpLysis.js")
 **/
bool export__toFormat__tsv__s_kt_clusterResult_condensed_t(const s_kt_clusterResult_condensed_t *INPUT, const char *INPUT, const char *INPUT, const char *INPUT);
/**
   @brief export the object to a java-script-fileformat (oekseth, 06. apr. 2017).
   @param <self>  the object which hodl the alredy-comptued cluster-results
   @param <fileName_result> the new fiel to constuct: if Not set, then we export to STDOUT
   @return true upon success.
   @remarks in the export-format we cpature use-cases ipmictly described (among others) in: "hpLysisVisu_api__initClusterResult(..)" ("hpLysisVisu_server.js"), "export_result_clusterResults(..)" ("hpLysis.pm"), "hpLysisVisu_api__initClusterResult(..)" tut-example ("tut_hpLysis.js")
 **/
bool export__toFormat__javaScript__s_kt_clusterResult_condensed_t(const s_kt_clusterResult_condensed_t *INPUT, const char *INPUT);


//!
//!
//! 
//! File: "e_kt_correlationFunction.h"
void init__s_kt_correlationMetric_t(s_kt_correlationMetric_t *OUTPUT, const e_kt_correlationFunction_t, const e_kt_categoryOf_correaltionPreStep_t);
bool isTo_use_MINE__e_kt_correlationFunction(const e_kt_correlationFunction_t);
//!
//!
//! 
//! File: "hp_api_fileInputTight.h"
bool apply__simMetric_dense__extraArgs__hp_api_fileInputTight(const s_kt_correlationMetric_t, s_kt_matrix_t *INPUT,  s_kt_matrix_t *INPUT, s_kt_matrix_t *OUTPUT);
//! TODO: consider using INOUT" instead of OUTPUT" for [”elow]
bool apply__clustAlg__extraArgs__hp_api_fileInputTight(const e_hpLysis_clusterAlg_t, const s_kt_correlationMetric_t, const s_kt_correlationMetric_t, const uint, const uint, t_float, uint, s_kt_matrix_t *INPUT, s_kt_matrix_t *OUTPUT, s_kt_clusterResult_condensed_t *OUTPUT, const bool, const bool);
//! --------
bool apply__simMetric_sparse__extraArgs__hp_api_fileInputTight(const s_kt_correlationMetric_t, s_kt_list_1d_pairFloat_t *INPUT,  s_kt_list_1d_pairFloat_t *INPUT, s_kt_matrix_t *OUTPUT);
//bool apply__simMetric_sparse__extraArgs__hp_api_fileInputTight__wrapper(s_kt_correlationMetric_t, s_kt_list_1d_pairFloat_t *INPUT); //,  s_kt_list_1d_pairFloat_t *INPUT, s_kt_matrix_t *OUTPUT);
//! --------
bool apply__ccm__matrixBased__matrixInput__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_clusterDistance__cmpType_t, s_kt_matrix_t *INPUT, s_kt_matrix_t *INPUT, const uint, t_float *OUTPUT);
bool apply__ccm__twoClusters__matrixInput__hp_api_fileInputTight(const e_kt_matrix_cmpCluster_metric_t, s_kt_matrix_t *INPUT, const uint, s_kt_matrix_t *INPUT, const uint,  t_float *OUTPUT);
bool readFromFile__stroreInStruct__exportResult__hp_api_fileInputTight(const char *INPUT, s_kt_matrix_t *INPUT, s_kt_matrix_t *INPUT, s_kt_matrix_t *OUTPUT, const char *INPUT, const char *INPUT, const uint, const bool);
//*/

//!
//!
//! 
//! File: ""
//!
//!
//! 
//! File: ""
//! File: ""
//! File: ""
//! File: ""
//! File: ""

%inline %{
  typedef float t_float;
  //  typedef unsigned char uint8;


  %} 
 

/* %typemap(in) unsigned int   *INPUT(uint temp) */
/* { */
/*   temp = (unsigned int) SvIV($1); */
/*   $result = &temp; */
/* } */

/* %typemap(in) t_float   *INPUT(float temp) */
/* { */
/*   temp = (float) SvNV($1); */
/*   $result = &temp; */
/* } */

//%typemap(cstype) unsigned long                    #&{$func_item}($obj_internal, $i, $value);  #! ie, fetch the value.                                                                                                                                                
/* #get_string__s_kt_list_1d_string($obj_internal, $i, $value);                                                                                                                                                        */
/* push(@arrOf_result, $value); */
/* } */
/* } */
/* } */
/* return @arrOf_result; */
/* } */
/* #! Fetch the reuslts whne 1d-list cosnists of tripelts.                                                                                                                                                                             */
/* sub __C_to_perl_generic_list { */
/*   my($obj_internal, $func_list_size, $func_item) = @_; */
/*   my @arrOf_result = (); */
/*   if(defined($obj_internal)) { */
/*     #! Note: in [”elow] we use the "&{$..} to de-reference the fucntion-pointe,r ie, to simplify our writing of cuntion-calls by iusiogn a generic appraoch.                                                                    */
/*         my $list_size = 0; &{$func_list_size}($obj_internal, $list_size); */
/*         if($list_size > 0) { */
/*             for(my $i = 0; $i < $list_size; $i++) { */
/*                 #my $value_1 = {}; my $value_2 = {}; my $value_3 = {};                                                                                                                                                              */
/*                 my @row = &{$func_item}($obj_internal, $i); #! ie, fetch the value.                                                                                                                                                 */
/*                 #, $value_1, $value_2, $value_3                                                                                                                                                                                     */
/*                 #get_string__s_kt_list_1d_string($obj_internal, $i, $value);                                                                                                                                                        */
/* -UUU:----F1  hpLysis_wrapper_1d.pm    8% L24   Hg@0  (Perl)------------------------------------------------------------------------------------------------------------------------------------------------------------------------ */




%include cpointer.i // Grab the SWIG pointer library: "http://www.swig.org/Doc1.1/HTML/Typemaps.html" and foudn on our system at "/usr/share/swig2.0/perl5/carrays.i" and oru "/usr/share/swig2.0/perl5/typemaps.i"

/* Parse the header file to generate wrappers */
//%include "configure_cCluster.h"
 //%include "def_intri.h"
%include "types.h"
%include "types_base.h"
 //%include "def_typeOf_float.h"
%include "e_kt_correlationFunction.h"
%include "kt_metric_aux.h"
%include "kt_list_1d.h"
%include "kt_list_1d_string.h"

%include "hp_ccm.h"
%include "hp_distance.h"
%include "kt_sparse_sim.h"
%include "kt_api.h"


 //FIXME: make use of [”elow] ... ie, provide access/support for computign the 'eigen-vector-centrliaty'
%include "e_kt_eigenVectorCentrality.h"
%include "kt_centrality.h"
%include "kt_clusterAlg_hca__node.h"
%include "log_clusterC.h"
%include "kt_terminal.h"
%include "kt_matrix.h"
%include "kt_matrix_cmpCluster.h"
%include "kt_list_1d.h"


%include "hpLysis_api.h"
%include "parse_main.h"
%include "kt_clusterAlg_fixed_resultObject.h"
%include "kt_clusterAlg_hca_resultObject.h" //! (oekseth, 06. nov. 2016).
%include "kt_clusterAlg_SOM_resultObject.h" //! (oekseth, 06. nov. 2016).
%include "kt_clusterAlg_hca__node.h" //! (oekseth, 06. nov. 2016).
%include "e_kt_clusterAlg_hca_type.h"
%include "kt_matrix_filter.h"
%include "kt_clusterResult_condensed.h"


%include "hp_api_fileInputTight.h"
%include "kt_swig_retValObj.h"
