#!/usr/bin/perl
use strict;
use hpLysis;
=head
    @brief demonstrates how different enums for clustering, simlairty-metircs and CCMs may be fetched. 
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
    @remarks In this 'example we focus on:
    -- useCase:  to directly access enums using in the calling-procedure to the hpLysis cluster-analsysi software, eg, when cosntrucing high-levels interface to your already written C/C++ API-code.
    -- API: retrival of enum-dienties and string-repretiaonts wrt. the latter.
=cut

{ #! Write out a subset of: cluster-algorithms:
    foreach my $string (
	"k_avg", "k_medoid") {
	my $enum_clusterAlg = hpLysis_subset::get_enumOf__e_hpLysis_clusterAlg_t__retVal($string);
	printf("enum_clusterAlg=$enum_clusterAlg givne clusterAlg=%s\n", $string);
    }
}



#if(1 == 2)
{ #! Write out a subset of: simliarty-metrics
    foreach my $string (
	"e_kt_correlationFunction_groupOf_minkowski_euclid",
	"e_kt_correlationFunction_groupOf_rank_kendall_KumarHassebrock"
	#"Euclid", "Kendall"
	) {
	#my $enum_id = hpLysis_subset::__wrapper__get_enumBasedOn_configuration_scalar($string);
	my $enum_id = hpLysis_subset::get_enumBasedOn_configuration_scalar($string);
	my $s_kt_swig_retValObj_t = hpLysis_subsetc::new_s_kt_swig_retValObj_t();
	#! 
	#! Call hpLysis to get the name (of the enum):
	my $enum_id_string = Algorithm::hpLysis_internal_returnWrappers::get__s_kt_swig_retValObj_t__string(\&hpLysis_subset::get_stringOf_enum__e_kt_correlationFunction_t_scalar, $enum_id);
	#$s_kt_swig_retValObj_t);
	#my $s_kt_swig_retValObj_t = 

#my $enum_clusterAlg = Algorithm::hpLysis_metrics::hpLysisVisu_server__static__translateIntoInternalEnum_clusterAlg($clusterAlg);
	#printf("...sdf.\n");
	printf("enum=\"$enum_id\" givne string-enum=\"$enum_id_string\", and search=%s\n", $string);
    }
}
#if(1 == 2)
{ #! Write out a subset of: simliarty-emtrics (pre-step):
    #printf("....\n");
    foreach my $string ("rank", "none", "binary") {
	my $enum_id_string = ""; #hpLysis_subset::get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_scalar($string);
	#$enum_id = 
	my $enum_id = hpLysis_subset::get_typeOf_correlationPreStep_fromString_scalar($string);
	#my $s_kt_swig_retValObj_t = hpLysis_subsetc::new_s_kt_swig_retValObj_t();
	# hpLysis_subset::get_typeOf_correlationPreStep_fromString_scalar($string, $enum_id);
	#my $enum_id = 0; #$hpLysis_subset::e_kt_categoryOf_correaltionPreStep_binary; 
	my $enum_id_string = Algorithm::hpLysis_internal_returnWrappers::get__s_kt_swig_retValObj_t__string(\&hpLysis_subset::get_stringOf_enum__e_kt_categoryOf_correaltionPreStep_t_scalar, $enum_id);
#my $enum_clusterAlg = Algorithm::hpLysis_metrics::hpLysisVisu_server__static__translateIntoInternalEnum_clusterAlg($clusterAlg);
	printf("(simPostStep)\tenum=\"$enum_id\" givne string-enum=\"$enum_id_string\", and search=%s\n", $string);
    }
}
{ #! Write out a subset of: matrix-based CCMs
    foreach my $string ("DBI", "VRC", "Silhouette", "DUNN") {
	# my $tmp = *hpLysis_subset::e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette;
	my $enum_id = hpLysis_subset::getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t__retVal__uint($string); #, $tmp);
#my $enum_clusterAlg = Algorithm::hpLysis_metrics::hpLysisVisu_server__static__translateIntoInternalEnum_clusterAlg($clusterAlg);
	printf("enum=\"$enum_id\" givne clusterAlg=%s\n", $string);
    }
}
{ #! Write out a subset of: set-based CCMs:
    foreach my $string ("Rands-index", "Mirkin", "Fowles-Mallows", "Rands-index (alternative-implementation)") {
	my $enum_id = hpLysis_subset::getEnumOf__e_kt_matrix_cmpCluster_metric_t__retVal__uint($string);
#my $enum_clusterAlg = Algorithm::hpLysis_metrics::hpLysisVisu_server__static__translateIntoInternalEnum_clusterAlg($clusterAlg);
	printf("enum=\"$enum_id\" givne clusterAlg=%s\n", $string);
    }
}
