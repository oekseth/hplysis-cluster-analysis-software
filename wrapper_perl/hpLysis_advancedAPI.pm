#---------------------------------------------------------------------------

package Algorithm::hpLysis::advancedAPI;

#---------------------------------------------------------------------------
# Copyright (c) 2016 Ole Kristian Ekseth (oekseth). All rights reserved.
# This program is free software.  You may modify and/or
# distribute it under the same terms as Perl itself.
# This copyright notice must remain attached to the file.
#
# Algorithm::hpLysis is a set of Perl wrappers around the
# hpLysis Clustering library.
#
#---------------------------------------------------------------------------
# The hpLysis clustering library for large-scale data-analysis.
# Copyright (C) 2016 Ole Kristian Ekseth (oekseth)
#
# This library was constructed in jointship with wwww.knittingTools.org in order to facilitate large-scale cluster-analysis of complex relationships
# Contact: oekseth 'AT' gmail.com
# 
#---------------------------------------------------------------------------
 
# use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS @EXPORT);
# use vars qw($DEBUG);
use strict;
# use DynaLoader;
use hpLysis_subset;
use hpLysis_internal; #! which provide wrapper-objets to gleu the hpLysis-pm simplifed API-itnerface with our "hpLysis cluster-analsysis-sfotware". (oekseth, 06. arp. 2017).
use hpLysis_metrics; #! provide logcis for 'fethcing' the correct enum when/if string-inptus are 'given'/provided (oekseth, 06. arp. 2017).
use hpLysis_wrapper_1d; #! provide logics to access data from 1d-hpLysis-lists, eg, wrt, the dat-astructres deifne din oru "kt_list_1d.h" C/C++ sturcutres (oekseth, 06. arp. 2017).
use hpLysis_internal_returnWrappers;
use hpLysis_wrapper_matrix;
require Exporter;

=head
    @brief provided an advanced 'semi-low-level' API for copmtuation of simlairty-emtircs (oekseth, 0+6. arp. 2017).
    @remarks idea is to extend our "hpLysis.pm" with 'closer' access to low-level C/C++ rotuiens, thereby 'enabling' a cocnatenation/piping of different objects, ie, by aovdiing teh 'cotnious' mappign to slow-perofmring Perl-data-containers.
=cut

sub __isAn_internalDataObj {
    my($data) = @_;
#    if(defined($data)) {
    my $isTo_free_1 = ( (ref($data) ne "hpLysis_subset::s_kt_list_1d_pairFloat_t") && (ref($data) ne "hpLysis_subset::s_kt_matrix_t") );
    return $isTo_free_1;

}

=head
    @param <metric> is the simlairty-metric to use
    @param <enum_preStep> optional: is the prestep-slimliarty-emtirc to use (ie, if any, eg, "none" or "rank" or "binary").
    @param <useInternalEnum> optional: which if set to "0" or 'undef'  impleis that a humanr-eadble name (eg, "euclid" or "cityblock" is used.
    @param <enum_metric_groupId> optional: if set in combinatioin with "useInternalEnum == 0" the latter group-paramter is sued to restrcit the searhc for a simalrity-emtric (eg, "squared" in combiatno with "euclid" impleis that we do Not use a "minkowski::Euclid" simlairty-meitrc).
    @param <data_1_perl> is the data to anlayse: either in a 'itnerla' data-format (eg, "s_kt_matrix_t" or "s_kt_list_1d_pairFloat_t") or in a Perl-based format (eg, a "$matrix = [[]];" format)
    @param <data_2_perl> optioanl: if used, then we copmtue a simalrity-emtirc using data_1 x data_2: is the data to anlayse: either in a 'itnerla' data-format (eg, "s_kt_matrix_t" or "s_kt_list_1d_pairFloat_t") or in a Perl-based format (eg, a "$matrix = [[]];" format)
    @param <data_isSparse> optional: if set to "1" we assume the data-set cosnistens of a "$arr_sparse = [(head, tial, score)];" tripelts
    @usage an exampel of usage is seen in [”elow]:
    my $metric = "Euclid";
    my $enum_preStep = ""; #! ie, as we asusem the metrics are to be used 'as-is', eg, where we are Not interested in using a Separman-appraoch wrt. 'rakning' (before computing simalrity-emtrics):
    my $useInternalEnum = 0; #! ie, as we use 'human-redalbe' enusm in [ªbove]:
    my $enum_metric_groupId = ""; #! ie, as we in this 'context' assuems that the emtic-idienties are exact enough for our use-case (where an 'oppostive' exmapole is see/examplfied in [below]).    
    #1 The call:
    my $mat_hpLysis_result = Algorithm::hpLysis::advancedAPI::compute__similarity($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId);
=cut
sub compute__similarity {
    my ($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId, $data_1_perl, $data_2_perl, $data_isSparse) = @_;
    #my $old_data1_ref = \$data_1_perl;       my $old_data2_ref = undef;      if(defined($data_2_perl)) {$old_data2_ref = \$data_2_perl;} #! which is used if the 'input-data-objects' are already converted to intenral-data-objects.
    #printf("-.--- , at %s:%d\n", __FILE__, __LINE__);
    my $sparseObj_1 = Algorithm::hpLysis_internal::Perl_to_C_generic($data_1_perl, $data_isSparse);
    #printf("-.--- , at %s:%d\n", __FILE__, __LINE__);
    my $sparseObj_2 = Algorithm::hpLysis_internal::Perl_to_C_generic($data_2_perl, $data_isSparse);
    # my $sparseObj_1 = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat(\@arrOf_triplets);
    # my $sparseObj_2 = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat(\@arrOf_triplets);
    #! -------------------------
    #! Deinfe the simliarty-metric:
    my ($enum_metric, $enum_metric_preStep) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId);    
    my ($obj_metric, $isTo_useMINE) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simObject($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId);    
    #!! *************************************************************************
    #! A 'global inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
    if($isTo_useMINE) {
	printf("allocates for MINE, at %s:%d\n", __FILE__, __LINE__);
	hpLysis_subset::hpLysis__globalInit__kt_api();    
    }
    #!! *************************************************************************
    # my $obj_metric = hpLysis_subsetc::new_s_kt_correlationMetric_t();
    # hpLysis_subset::init__s_kt_correlationMetric_t($obj_metric, $enum_metric, $enum_metric_preStep);
    #! -------------------------
    #!
    #! Compute the result: 
    # printf("-.--- , at %s:%d\n", __FILE__, __LINE__);
    my $mat_hpLysis_result = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix(); #! ie, intalise to emptoy
    #printf("-.--- , at %s:%d\n", __FILE__, __LINE__);
    #my $mat_hpLysis = 
    #hpLysis_subsetc::setTo_empty__s_kt_matrix_t($mat_hpLysis_result);
    ## my $mat_hpLysis_result = hpLysis_subset::setTo_empty__s_kt_matrix_t(); #$mat_result);
    #! 
    #! validte that we provide the correct object-datra-types:
    if(defined(ref($sparseObj_2))) {
	if(ref($sparseObj_1) ne ref($sparseObj_2)) {
	    printf(STDERR "!!\t(not-supported)\t Input-objects \"%s\" and \"%s\" have un-euqal proeprteis: we have Not yet added support, ie, if latter is of interest, hten please give us an heads-up, at %s:%d\n", ref($sparseObj_1), ref($sparseObj_2), __FILE__, __LINE__);
	    croak("!!\t Add support for this un-sopported option");	    
	}
    }

    #!
    #! Investigate if we are to free memory:
    my $isTo_free_1 = __isAn_internalDataObj($data_1_perl); #( (ref($data_1_perl) ne "hpLysis_subset::s_kt_list_1d_pairFloat_t") && (ref($data_1_perl) ne "hpLysis_subset::s_kt_matrix_t") );
    my $isTo_free_2 = (defined($data_2_perl)) ? __isAn_internalDataObj($data_2_perl) : 0; #( (ref($data_2_perl) ne "hpLysis_subset::s_kt_list_2d_pairFloat_t") && (ref($data_2_perl) ne "hpLysis_subset::s_kt_matrix_t") );
    #my $isTo_free_2 = (defined($old_data2_ref) && ($old_data2_ref != \$sparseObj_2));
    my $sparseObj_2_local = $sparseObj_2;
    { #! HAndle 'isseus' where the 'second inptu-object' is empty, ie, for whcihw e are to use the 'first' inptu-object
	if(!defined($sparseObj_2)) {
	    $sparseObj_2_local =  $sparseObj_1;
	} 	
    }
    #!
    #! Apply logics:
    if(ref($sparseObj_1) eq "hpLysis_subset::s_kt_list_1d_pairFloat_t") {
	#! Teh call:	
	hpLysis_subset::apply__simMetric_sparse__extraArgs__hp_api_fileInputTight($obj_metric, $sparseObj_1, $sparseObj_2_local, $mat_hpLysis_result);
	#$mat_hpLysis_result = hpLysis_subset::apply__simMetric_sparse__extraArgs__hp_api_fileInputTight($obj_metric, $sparseObj_1, $sparseObj_2);
	#hpLysis_subset::apply__simMetric_sparse__extraArgs__hp_api_fileInputTight__wrapper($obj_metric, $sparseObj_1);
	#!
	#! De-allcoate the lcoal inptu-data-sets:
	if($isTo_free_1) {hpLysis_subset::free__s_kt_list_1d_pairFloat_t($sparseObj_1);}
	if(defined($sparseObj_2) && $isTo_free_2) {
	    printf("(info)\t de-allcoates the data-objec,t ie, assuems the user did Not provide their own internal data-objec,t at %s:%d\n", __FILE__, __LINE__);
	    hpLysis_subset::free__s_kt_list_1d_pairFloat_t($sparseObj_2);
	}
    } elsif(ref($sparseObj_1) eq "hpLysis_subset::s_kt_matrix_t") { #! then we use an API-call simliart to abvoe, ie, where difference conserns our data-input (and funciotn-name):
	printf("\t Copmtues for dense sim-matrix, at %s:%d\n", __FILE__, __LINE__);
	#! Teh call:
	hpLysis_subset::apply__simMetric_dense__extraArgs__hp_api_fileInputTight($obj_metric, $sparseObj_1, $sparseObj_2_local, $mat_hpLysis_result);
	#!
	#! De-allcoate the lcoal inptu-data-sets:
   	if($isTo_free_1) {hpLysis_subset::free__s_kt_matrix($sparseObj_1);}
	if(defined($sparseObj_2) && $isTo_free_2) {
	    printf("(info)\t de-allcoates the data-objec,t ie, assuems the user did Not provide their own internal data-objec,t at %s:%d\n", __FILE__, __LINE__);
	    hpLysis_subset::free__s_kt_matrix($sparseObj_2);
	}
    } else {
	printf(STDERR "!!\t(not-supporte)d\t For object=\"%s\" we have Not yet added support, ie, if latter is of interest, hten please give us an heads-up, at %s:%d\n", ref($sparseObj_1), __FILE__, __LINE__);
	croak("!!\t Add support for this un-sopported option");
    } 
    #!! *************************************************************************
    if($isTo_useMINE) { #! then de-allcoat ethe lcoal MINE-objec,t ie as we then asusme tha tlterrter is No longer needed.
	hpLysis_subset::hpLysis__globalFree__kt_api();    
    }
    {
	my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_hpLysis_result);
	printf("dims=[$nrows, $ncols], at %s:%d\n", __FILE__, __LINE__);
    }
    #!! *************************************************************************    
    #!
    #! @return
    return $mat_hpLysis_result;
}
#! @®eturn a cofnigruaiton-hash wrt. comptaution of smalrity-emtrics.
sub hpLysis_getConfigHash_simliarty {
    my ($metric, $preStep) = @_;
    return {
	metric_id => $metric,
	metric_preStep => $preStep,
    };
}


=head
   @brief computes and configures the "hpLysisVisu_server.js" wrt.  (oesketh, 06. arp. 2017)
   @param <data_1_perl> is the data to anlayse: either in a 'itnerla' data-format (eg, "s_kt_matrix_t") or in a Perl-based format (eg, a "$matrix = [[]];" format)
   @param <data_2_perl> optioanl: if used, then we copmtue a simalrity-emtirc using data_1 x data_2: is the data to anlayse: either in a 'itnerla' data-format (eg, "s_kt_matrix_t") or in a Perl-based format (eg, a "$matrix = [[]];" format)
   @param <enum_alg> is the clsutering-algorithm to be applied/used
   @param <cnt_clusters_x> which for k-means-permtuations is the 'k-cluster-count', while for SOM is the x-clsuter-direction.
   @param <cnt_clusters_y>  which is used for SOM-clustering,
   @param <iterative_maxIterCount> SOM and k-means: is used to 'break' an iteraive-aloithm-exueicont (eg, wrt. SOM), 
   @param <iterative_SOM_tauInit>  SOM and k-means: which is used as a 'senitivty-trehshold' during applciaiton of SOM-clustering.
   @param <enum_ccm_matrixBased> is a matrix-based CCM, eg, "Silhouette, "DavisBouldin" and VRC; may be used in combaiton with HCA-based clsutering-algorithms
   @param <objSim_preCluster> optional: the simalrity-metric to use: if set, then we comptue the simarlity using an all-gaisnta-ll aprpaoich, ie, before comptuign wr.t the clustes.
   @param <objSim_insideCluster> the simalrity-metric to use ''during/isnide the lcustierng': deifnes how 'dsitance' si to be itnerpreted during the clusteirng-operaitons.
   @remarks an exammpel fo the  "enum_ccm_matrixBased" is "Silhouette", and "Dunn"; for clsutering may the latter be used in combaiton with HCA-based clustering-algorithms: if used in comniaont with "cluster" then we comptue/infer 'dunamic-clsuter-results' (ie, using a best-fit' aprpaoch wr.t the latter CCM-metirc-enum).
=cut
sub compute__cluster {
    my($data_1_perl, $data_2_perl, $enum_alg, $cnt_clusters_x, $cnt_clusters_y, $iterative_maxIterCount, $iterative_SOM_tauInit, $enum_ccm_matrixBased, $objSim_preCluster, $objSim_insideCluster) = @_;
    my $data_isSparse = 0;
    my $sparseObj_1 = Algorithm::hpLysis_internal::Perl_to_C_generic($data_1_perl, $data_isSparse);
    #printf("b\n");
    #printf("-.--- , at %s:%d\n", __FILE__, __LINE__);
    my $sparseObj_2 = Algorithm::hpLysis_internal::Perl_to_C_generic($data_2_perl, $data_isSparse);
    #! validte that we provide the correct object-datra-types:
    if(defined(ref($sparseObj_2))) {
	if(ref($sparseObj_1) ne ref($sparseObj_2)) {
	    printf(STDERR "!!\t(not-supported)\t Input-objects \"%s\" and \"%s\" have un-euqal proeprteis: we have Not yet added support, ie, if latter is of interest, hten please give us an heads-up, at %s:%d\n", ref($sparseObj_1), ref($sparseObj_2), __FILE__, __LINE__);
	    croak("!!\t Add support for this un-sopported option");	    
	}
    }
    # printf("c\n");
    #!
    #! Investigate if we are to free memory:
    my $isTo_free_1 = __isAn_internalDataObj($data_1_perl); #( (ref($data_1_perl) ne "hpLysis_subset::s_kt_list_1d_pairFloat_t") && (ref($data_1_perl) ne "hpLysis_subset::s_kt_matrix_t") );
    my $isTo_free_2 = (defined($data_2_perl)) ? __isAn_internalDataObj($data_2_perl) : 0; #( (ref($data_2_perl) ne "hpLysis_subset::s_kt_list_2d_pairFloat_t") && (ref($data_2_perl) ne "hpLysis_subset::s_kt_matrix_t") );

    #printf("d\n");
    #! 
    #! Configure the simlairty-metics:
    #! Note: Handle cases where users provide a human-redable enum-name, eg, 'using' "Euclid"
    my $isTo_useMINE = 0;
    my $obj_metric_pre    = undef; # hpLysis_subset::init__s_kt_correlationMetric_t($objSim_preCluster->{metric_id}, $objSim_preCluster->{metric_preStep});
    my $obj_metric_inside = undef; #hpLysis_subset::init__s_kt_correlationMetric_t($objSim_insideCluster->{metric_id}, $objSim_insideCluster->{metric_preStep});
    if(defined($objSim_preCluster))     {
	my $isTo_useMINE_local = 0;
	#printf("e-pre\n");
	($obj_metric_pre, $isTo_useMINE_local) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simObject($objSim_preCluster->{metric_id}, $objSim_preCluster->{metric_preStep});
	#printf("e-aft\n");
	if($isTo_useMINE_local) {$isTo_useMINE = 1;}
	#($objSim_preCluster->{metric_id}, $objSim_preCluster->{metric_preStep}) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($objSim_preCluster->{metric_id}, $objSim_preCluster->{metric_preStep});
    } else {
	my $isTo_useMINE_local = 0;
	#printf("f-pre\n");
	($obj_metric_pre, $isTo_useMINE_local) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simObject(); #! ie, set to empty.
	#printf("f-aft\n");
    }
    if(defined($objSim_insideCluster))   {
	my $isTo_useMINE_local = 0;
	#my ($enum_metric, $enum_metric_preStep) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($metric, $preStep, $useInternalEnum, $enum_metric_groupId);    
	#printf("g\n");
	($obj_metric_inside, $isTo_useMINE_local) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simObject($objSim_insideCluster->{metric_id}, $objSim_insideCluster->{metric_preStep});
	if($isTo_useMINE_local) {$isTo_useMINE = 1;}
	# ($obj_metric, $isTo_useMINE) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simObject($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId);    
	# ($objSim_insideCluster->{metric_id}, $objSim_insideCluster->{metric_preStep}) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($objSim_insideCluster->{metric_id}, $objSim_insideCluster->{metric_preStep});
    }  else {
	my $isTo_useMINE_local = 0;
	($obj_metric_inside, $isTo_useMINE_local) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simObject(); #! ie, set to empty.
    }
    if(defined($enum_alg)) {
	$enum_alg = Algorithm::hpLysis_metrics::hpLysis_metrics__static__translateIntoInternalEnum_clusterAlg($enum_alg);
    } else {
	$enum_alg = Algorithm::hpLysis_metrics::hpLysis_metrics__static__enumEmptyValueFor__clusterAlg();
    }
    if(defined($enum_ccm_matrixBased)) {
	$enum_ccm_matrixBased = Algorithm::hpLysis_metrics::hpLysis_metrics__static__translateIntoInternalEnum_ccm__matrixBased($enum_ccm_matrixBased);
    } else {
	$enum_ccm_matrixBased = Algorithm::hpLysis_metrics::hpLysis_metrics__static__enumEmptyValueFor__ccm_matrixBased();
    }
    #!
    #! Intalise MINE-tables, ie, fi we need the latter:
    #!! *************************************************************************
    #! A 'global inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
    if($isTo_useMINE) {
	printf("allocates for MINE, at %s:%d\n", __FILE__, __LINE__);
	hpLysis_subset::hpLysis__globalInit__kt_api();    
    }    
    #!! *************************************************************************
    my $mat_computedSim = undef;
    #! Hande the 'case' where a user provides two input-matirces:
    if(defined($data_2_perl)) {
	if(ref($sparseObj_1) eq "hpLysis_subset::s_kt_matrix_t") { #! then we use an API-call simliart to abvoe, ie, where difference conserns our data-input (and funciotn-name):
	    my $ifNotFound__printError = 0;
	    my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($sparseObj_2, $ifNotFound__printError);
	    #printf("nrows=$nrows, at %s:%d\n", __FILE__, __LINE__);
	    if(!$nrows || !$ncols) {$data_2_perl = undef;} #! ie, as the latter object is then Not set.
	} else {
	    croak("!!\t add support for thsi.");
	}
    }
    if(defined($data_2_perl)) {
	#! 
	#! Apply logics:
	my $useInternalEnum = undef; 	my $enum_metric_groupId = undef; 
	$mat_computedSim = Algorithm::hpLysis::advancedAPI::compute__similarity($objSim_preCluster->{metric_id}, $objSim_preCluster->{metric_preStep}, $useInternalEnum, $enum_metric_groupId, $sparseObj_1, $sparseObj_2); #\@arrOf_triplets, \@arrOf_triplets);
	#printf("computes-sim-sperately, at %s:%d\n", __FILE__, __LINE__);
    }
    # printf("m\n");
    my $isTo_useSimMetricInPreStep = 0; #! ie, as we then assuemt aht 'this call' has alreyd been 'amd'e:
    if(!defined($mat_computedSim)) {
	$isTo_useSimMetricInPreStep = (defined($objSim_preCluster));
	printf("isTo_useSimMetricInPreStep=$isTo_useSimMetricInPreStep, given objSim_preCluster=$objSim_preCluster, at %s:%d\n", __FILE__, __LINE__);
	if(ref($sparseObj_1) ne "hpLysis_subset::s_kt_matrix_t") { #! then we use an API-call simliart to abvoe, ie, where difference conserns our data-input (and funciotn-name):
	    croak("!!\t Add support for the \"$sparseObj_1\" data-format");
	}
    } else {
	if($isTo_free_1) {Algorithm::hpLysis_internal::generic__freeMem($sparseObj_1);}
	if($isTo_free_2) {Algorithm::hpLysis_internal::generic__freeMem($sparseObj_2);}
	#! 
	#! Update:
	$sparseObj_1 = $mat_computedSim; #! ie, uwe latter matrix as inptu to cluster.
	$isTo_free_1 = 1; #! ie, as [ªbove] is allocated
    }

    my $mat_hpLysis_result = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix(); #! ie, intalise to empto
    my $result_clusterObject = hpLysis_subsetc::new_s_kt_clusterResult_condensed_t(); hpLysis_subset::initSetToEmpty__s_kt_clusterResult_condensed_t($result_clusterObject); #! ie, intlize latter.
    my $isTo_inMatrixResult_storeSimMetricResult = 1; #! ie, our defualt assumption,.
    #printf("f\n");
    if(ref($sparseObj_1) eq "hpLysis_subset::s_kt_matrix_t") { #! then we use an API-call simliart to abvoe, ie, where difference conserns our data-input (and funciotn-name):
	#! 
	#! Apply logics:
	#hpLysis_subset::apply__clustAlg__extraArgs__hp_api_fileInputTight($enum_alg, $obj_metric_pre, undef, $cnt_clusters_x, $cnt_clusters_y, $iterative_SOM_tauInit, $iterative_maxIterCount, $sparseObj_1, $mat_hpLysis_result, $result_clusterObject, $isTo_inMatrixResult_storeSimMetricResult, $isTo_useSimMetricInPreStep);
	if(!defined($iterative_maxIterCount) || ($iterative_maxIterCount == 0)) {$iterative_maxIterCount = 300;}
	if(!defined($iterative_SOM_tauInit) || ($iterative_SOM_tauInit == 0)) {$iterative_SOM_tauInit = 0.01;}
	printf("iterative_maxIterCount='$iterative_maxIterCount'\n");
	printf("isTo_useSimMetricInPreStep=$isTo_useSimMetricInPreStep, at %s:%d\n", __FILE__, __LINE__);
	hpLysis_subset::apply__clustAlg__extraArgs__hp_api_fileInputTight($enum_alg, $obj_metric_pre, $obj_metric_inside, $cnt_clusters_x, $cnt_clusters_y, $iterative_SOM_tauInit, $iterative_maxIterCount, $sparseObj_1, $mat_hpLysis_result, $result_clusterObject, $isTo_inMatrixResult_storeSimMetricResult, $isTo_useSimMetricInPreStep);
	printf("f\n");
	#!
	#! De-allcoate the lcoal inptu-data-sets:
	if($isTo_free_1) {Algorithm::hpLysis_internal::generic__freeMem($sparseObj_1);}
    } else {
	croak("!!\t Add support for the \"$sparseObj_1\" data-format");
    }
    # printf("f\n");
    #!! *************************************************************************
    if($isTo_useMINE) { #! then de-allcoat ethe lcoal MINE-objec,t ie as we then asusme tha tlterrter is No longer needed.
	hpLysis_subset::hpLysis__globalFree__kt_api();    
    }
    {
	my $ifNotFound__printError = 0;
	my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_hpLysis_result, $ifNotFound__printError);
	printf("dims=[$nrows, $ncols], at %s:%d\n", __FILE__, __LINE__);
    }
    #!! *************************************************************************    
    return ($mat_hpLysis_result, $result_clusterObject);
}


=head
   @brief configure object to compute a set-based CCM (eg, "RAnds Adjusted Index")(oesketh, 06. arp. 2017)
    @param <self> is the object which hold the input-matrices: if only the first input-matrix is set, then we 'apply' a self-comparison (an 'appraoch' which if osten sued to evlauate/descirbe the sialmrity between differnet cluster-resutls, eg, cluster-reuslts collected suing diffenre tsim-meitrcs, cluster-alrogirhtms and CCMs). 
   @param <enum_ccm_setBased> is the setBaed cCM to evluaate the inptu-amtrix wrt. to
   @remarks an example-applicaiton is to assess the simlairty between diffenret/muliple k-means cluster-results, eg, through the use of "Rands Adjusted Index".
    @return an Perl- matrix of scores. 
    @remarks matrix-diemsniosn: we expect that data_1[*].length == data_2[*].length: the latter due to the purpose of this funciton, ie, that there is a 'hypoteiss' asiscated to each of the rows in the both matrices, ie, as we compare the identies seperately for each row_1[..]x[row_2[..].
=cut
sub compute__CCM_setBased {
    my($enum_ccm_setBased, $data_1_perl, $data_2_perl) = @_;
    my $data_isSparse = 0;
    my $mat_input_1 = Algorithm::hpLysis_internal::Perl_to_C_generic($data_1_perl, $data_isSparse);
    #printf("-.--- , at %s:%d\n", __FILE__, __LINE__);
    my $mat_input_2 = Algorithm::hpLysis_internal::Perl_to_C_generic($data_2_perl, $data_isSparse);
    if(ref($mat_input_1) ne "hpLysis_subset::s_kt_matrix_t") {
	croak("!!\t We currently only supprots a 'dense' amtrix-input, which does Not seem to be the case/issue wrt. your data-input=\"$mat_input_1\", ie, please udpate your call-API");
    }
    #!
    #! Inveiste if we are to free memory:
    my $isTo_free_1 = __isAn_internalDataObj($data_1_perl); #( (ref($data_1_perl) ne "hpLysis_subset::s_kt_list_1d_pairFloat_t") && (ref($data_1_perl) ne "hpLysis_subset::s_kt_matrix_t") );
    my $isTo_free_2 = (defined($data_2_perl)) ? __isAn_internalDataObj($data_2_perl) : 0; #
    #!
    #! Fetch cofnigruations and data to use:
    $enum_ccm_setBased = Algorithm::hpLysis_metrics::hpLysis_metrics__static__translateIntoInternalEnum_ccm__setBased($enum_ccm_setBased);
    #$enum_ccm_setBased = Algorithms::hpLysis_metrics::hpLysis_metrics__static__translateIntoInternalEnum_ccm__setBased($enum_ccm_setBased);
    if(!defined($enum_ccm_setBased)) {
	croak("!!\t inveistget ehy the enum was Not deinfedd, iegv, be reading the 'arleayd generated' erorr-messages");
    }
    #my $mat_input_1 = $self->{internal}->{data_1};      my $mat_input_2 = $self->{internal}->{data_2}; 
    #! 
    #! Get matrix-demions.
    my ($nrows_1, $ncols_1) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_input_1, 1);
    #my ($nrows_1, $ncols_1) = static_hpLysis__s_kt_matrix__getDims($mat_input_1, 1);
    if(!defined($nrows_1)) {return undef;} #! ie, thne an eror wrt. the amtrix-idemson-reading
    my ($nrows_2, $ncols_2) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_input_2, 1);
    #my ($nrows_2, $ncols_2) = static_hpLysis__s_kt_matrix__getDims($mat_input_2, 0);
    if(!defined($nrows_2)) {
	$nrows_2 = $nrows_1; $ncols_2 = $ncols_1; $mat_input_2 = $mat_input_1; #! ie, as we then assume a 'self-comparison'.
    }
    { #! Validate that there is a 'hypoteiss'asiscated to each of the rows in the first matrix
	if($ncols_1 != $ncols_2) {
	    printf(STDERR "!!\t(Input-Not-asExpected)\t In the CCM-function-input dims(matrix_1)=[$nrows_1, $ncols_1], while dims(matrix_2)=[$nrows_2, $ncols_2]. In contrast we expect that data_1[*].length == data_2[*].length: the latter due to the purpose of this funciton, ie, that there is a 'hypoteiss' asiscated to each of the rows in the both matrices, ie, as we compare the identies seperately for each row_1[..]x[row_2[..]. In brief please udpate your call. Obserivonat at %s:%d\n", __FILE__, __LINE__);
	    return undef;
	}
    }
    #!
    #! Iterate thourhg the 'second matrix', comptuating for each of the hypothesis.
    my @matrixOf_scores = ();
    for(my $row_id = 0; $row_id < $nrows_1; $row_id++) {	
	push(@matrixOf_scores, []);
	for(my $col_id = 0; $col_id < $nrows_2; $col_id++) {	
	    #!
	    #! Apply logics: 
	    my $result_scalar =   hpLysis_subset::apply__ccm__twoClusters__matrixInput__hp_api_fileInputTight($enum_ccm_setBased,  $mat_input_1, $row_id, $mat_input_2, $col_id); #, $result_scalar);
	    printf("[$row_id][$col_id] = '$result_scalar', at %s:%d\n", __FILE__, __LINE__);
	    push(@{$matrixOf_scores[$row_id]}, $result_scalar);
	}
    }
    #!
    #! De-allcoate 'if we owns the object':
    if($isTo_free_1) {hpLysis_subset::free__s_kt_matrix($mat_input_1);}
    if(defined($mat_input_2) && $isTo_free_2) {
	hpLysis_subset::free__s_kt_matrix($mat_input_2);
    }
    #!
    #! @retunr the result:
    return \@matrixOf_scores;
}

=head
   @brief configure object to compute a set-based CCM (eg, "Silhouette") (oesketh, 06. arp. 2017)
    @param <self> is the object which hold the input-matrices: 
    @param <enum_ccm_matrixBased> is the matrix-based CCM to evluaate the inptu-amtrix wrt. to
    @remarks an example-applicaiton is to assess the simlairty between diffenret/muliple k-means cluster-results, eg, through the use of "Rands Adjusted Index".
    @return an array where each score is assicatred to each row in the 'second'input-matrix.
 @remarks we expect that data_1.length == data_2[*].length: the latter due to the purpose of this funciton, ie, that there is a 'hypoteiss'asiscated to each of the rows in the first matrix
=cut
sub compute__CCM_matrixBased {
    my($enum_ccm_matrixBased, $data_1_perl, $data_2_perl) = @_;
    my $data_isSparse = 0;
    my $mat_input_1 = Algorithm::hpLysis_internal::Perl_to_C_generic($data_1_perl, $data_isSparse);
    #printf("-.--- , at %s:%d\n", __FILE__, __LINE__);
    my $mat_input_2 = Algorithm::hpLysis_internal::Perl_to_C_generic($data_2_perl, $data_isSparse);
    if(ref($mat_input_1) ne "hpLysis_subset::s_kt_matrix_t") {
	croak("!!\t We currently only supprots a 'dense' amtrix-input, which does Not seem to be the case/issue wrt. your data-input=\"$mat_input_1\", ie, please udpate your call-API");
    }
    #!
    #! Inveiste if we are to free memory:
    my $isTo_free_1 = __isAn_internalDataObj($data_1_perl); #( (ref($data_1_perl) ne "hpLysis_subset::s_kt_list_1d_pairFloat_t") && (ref($data_1_perl) ne "hpLysis_subset::s_kt_matrix_t") );
    my $isTo_free_2 = (defined($data_2_perl)) ? __isAn_internalDataObj($data_2_perl) : 0; 
    #!
    #! Fetch cofnigruations and data to use:
    $enum_ccm_matrixBased = Algorithm::hpLysis_metrics::hpLysis_metrics__static__translateIntoInternalEnum_ccm__matrixBased($enum_ccm_matrixBased);
    # my $mat_input_1 = $self->{internal}->{data_1}; 
    # my $mat_input_2 = $self->{internal}->{data_2}; 
    #! Get matrix-demions.
    my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_input_2, 1);
    #my ($nrows, $ncols) = static_hpLysis__s_kt_matrix__getDims($mat_input_2, 1);
    if(!defined($nrows)) {return undef;} #! ie, thne an eror wrt. the amtrix-idemson-reading
    { #! Validate that there is a 'hypoteiss'asiscated to each of the rows in the first matrix
	my ($nrows_1, $ncols_1) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_input_1, 1);
	#my ($nrows_1, $ncols_1) = static_hpLysis__s_kt_matrix__getDims($mat_input_1, 1);
	if(!defined($nrows)) {return undef;} #! ie, thne an eror wrt. the amtrix-idemson-reading
	if($nrows_1 != $ncols) {
	    printf(STDERR "!!\t(Input-Not-asExpected)\t In the CCM-function-input dims(matrix_1)=[$nrows_1, $ncols_1], while dims(matrix_2)=[$nrows, $ncols]. In contrast we expect that data_1.length == data_2[*].length: the latter due to the purpose of this funciton, ie, that there is a 'hypoteiss'asiscated to each of the rows in the first matrix. In brief please udpate your call. Obserivonat at %s:%d\n", __FILE__, __LINE__);
	    return undef;
	}
    }
    #!
    #! Iterate thourhg the 'second matrix', comptuating for each of the hypothesis.
    my @arrOf_scores = ();
    for(my $row_id = 0; $row_id < $nrows; $row_id++) {	
	#!
	#! Apply logics: 
	my $result_scalar = hpLysis_subset::apply__ccm__matrixBased__matrixInput__hp_api_fileInputTight($enum_ccm_matrixBased,  $mat_input_1, $mat_input_2, $row_id); #, $result_scalar);
	push(@arrOf_scores, $result_scalar);
    }
    #!
    #! De-allcoate 'if we owns the object':
    if($isTo_free_1) {hpLysis_subset::free__s_kt_matrix($mat_input_1);}
    if(defined($mat_input_2) && $isTo_free_2) {
	hpLysis_subset::free__s_kt_matrix($mat_input_2);
    }
    #!
    #! @retunr the result:
    return [\@arrOf_scores]; #! where latter truent-value is ued to 'reflect' our "set-based" cCM-bomcptuation, ie, simplifying hte use/inteprtiaotn of 'what return-valeus are givne'.
}

1;
