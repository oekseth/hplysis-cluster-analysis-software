#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 
#use hpLysis_subset;

=head
    @file "tut_clust_3_HCA.pl"
    @brief cluster useing HCA::max: focus on the sue of simlairty-metircs.
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
    @remarks demonstres logics wrt.:
    (1) similiarty-emtircs: use both a 'pre-stpe' simlairty-emtirc and an 'isndie-simalirty-emtric'
    (2) demosntrates how the HCA-max algorithm may be used/called.
    (3) use various/different export-rouitnes for data-export
=cut
{    
    my $obj_hp = Algorithm::hpLysis->new(); 
    my $matrix = [ 
	[0.000000, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.072906],
	[0.321928, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.321928],
	[0.072906, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.000000]
	];
    my $arrOf_rows = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    my $arrOf_cols  = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    #! Intialise:
    $obj_hp->set_matrix_1($matrix, $arrOf_rows, $arrOf_cols);
    #!
    #! Comptue the clusters:
    #! Ntoe: the motivaiotn of 'explcilty speicifying' the [”elow] klogn list of inptu-apramseters, is to amke us ware/ettentite that HCA-algorithsm may be sued/combined with a number of differnet algorithms, ie, o irmpvoer esutl-acucrayc by suign core-traits of differen/viarous data-proerpties.
    my $cnt_clusters_x = undef; my $cnt_clusters_y = undef; my $iterative_maxIterCount = undef; my $iterative_SOM_tauInit = undef; my $enum_ccm_matrixBased = undef; 
    my $objSim_preCluster = Algorithm::hpLysis::advancedAPI::hpLysis_getConfigHash_simliarty("kendall"); 
    my $objSim_insideCluster = Algorithm::hpLysis::advancedAPI::hpLysis_getConfigHash_simliarty("euclid");
    my ($matrix, $arrOf_rows, $arrOf_cols, $obj_cluster) = $obj_hp->compute__cluster("algorithm::HCA::max", $cnt_clusters_x, $cnt_clusters_y, $iterative_maxIterCount, $iterative_SOM_tauInit, $enum_ccm_matrixBased, $objSim_preCluster, $objSim_insideCluster); #! ie, comptue using the Euclidian simliarty-metric
    #!
    #! Investigte the results:
use Data::Dumper;
    my @resultAfterCall = ($matrix, $arrOf_rows, $arrOf_cols, $obj_cluster);
    print "resultAfterCall=" . Dumper \@resultAfterCall;
}
