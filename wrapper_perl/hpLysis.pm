#---------------------------------------------------------------------------

package Algorithm::hpLysis;

#---------------------------------------------------------------------------
# Copyright (c) 2016 Ole Kristian Ekseth (oekseth). All rights reserved.
# This program is free software.  You may modify and/or
# distribute it under the same terms as Perl itself.
# This copyright notice must remain attached to the file.
#
# Algorithm::hpLysis is a set of Perl wrappers around the
# hpLysis Clustering library.
#
#---------------------------------------------------------------------------
# The hpLysis clustering library for large-scale data-analysis.
# Copyright (C) 2016 Ole Kristian Ekseth (oekseth)
#
# This library was constructed in jointship with wwww.knittingTools.org in order to facilitate large-scale cluster-analysis of complex relationships
# Contact: oekseth 'AT' gmail.com
# 
#---------------------------------------------------------------------------
 
# use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS @EXPORT);
# use vars qw($DEBUG);
use strict;
# use DynaLoader;
use hpLysis_subset;
use hpLysis_internal; #! which provide wrapper-objets to gleu the hpLysis-pm simplifed API-itnerface with our "hpLysis cluster-analsysis-sfotware". (oekseth, 06. arp. 2017).
use hpLysis_metrics; #! provide logcis for 'fethcing' the correct enum when/if string-inptus are 'given'/provided (oekseth, 06. arp. 2017).
use hpLysis_wrapper_1d; #! provide logics to access data from 1d-hpLysis-lists, eg, wrt, the dat-astructres deifne din oru "kt_list_1d.h" C/C++ sturcutres (oekseth, 06. arp. 2017).
use hpLysis_internal_returnWrappers;
use hpLysis_wrapper_matrix;
use hpLysis_advancedAPI;
require Exporter;

# $VERSION     = '1.0';
# $DEBUG       = 1;
# @ISA         = qw(DynaLoader Exporter);

# @EXPORT_OK = qw(
#     insertMatrix__intoInternal
#     config
#     compute
# );

# use warnings::register;

# bootstrap Algorithm::hpLysis $VERSION;

#-------------------------------------------------------------
# Debugging functions
#
sub version {
    return _version();
}


#-------------------------------------------------------------
# Wrapper for printing warnings
#
sub module_warn {
    return unless warnings::enabled();
    warnings::warn("Algorithm::hpLysis", join '', @_);
}


# sub new {
#     my ($class) = @_;
#     bless($self, $class);
#     return $self;
# }


=head
    @brief intalises the hpLysis object.
    @param <class>  the objec-tyutpe, whcih is implcitly specified when "Algorithm::hpLysis->new(..)" is written/called.
    @param <matrix_input_1>  optional: the first data-set to comptue for.
    @param <matrix_input_2>  optional: the first data-set to comptue for.
    @param <data_isSparse>  optional: used if the input-matrices are deinfed: shoudl be set to "1" if the data-matrix is sparce (ie, are set of reows, hwere each row described a (head=int, tail=int, score=float) pair; if Not specifed then latter is set to "0".
=cut
sub new {
    my($class, $matrix_input_1, $matrix_input_2, $data_isSparse) =  @_;
    #my($class, $stringOf_enum, $matrix_input_1, $matrix_input_2) =  @_;
    #my($class, $stringOf_enum, $matrix_input_1, $matrix_input_2, $optional__config) =  @_;
    # if(!defined($optional__config)) {
    # 	$optional__config = hpLysis_config(); #! ie, then use defualt cofnigruations.
    # }
    #!! *************************************************************************
    #! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
    # hpLysis_subset::hpLysis__globalInit__kt_api();    
    #!! *************************************************************************
    #! 
    #! 
    if(!defined($data_isSparse)) {$data_isSparse = 0;} #! ie, our deufalt aasumption.
    my $data_1 = Algorithm::hpLysis_internal::Perl_to_C_generic($matrix_input_1, $data_isSparse);
    my $data_2 = Algorithm::hpLysis_internal::Perl_to_C_generic($matrix_input_2, $data_isSparse);
    # my $data_1 = undef; if($data_isSparse == 1) {$data_1 = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat($matrix_input_1);} else {$data_1 = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix_input_1);}
    # #my $data_1 = ($data_isSparse == 1) ? Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat($matrix_input_1) : Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix_input_1);
    # my $data_2 = undef; if($data_isSparse == 1) {$data_2 = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat($matrix_input_2);} else {$data_2 = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix_input_2);}
	#my $data_2 = ($data_isSparse == 1) ? Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat($matrix_input_2) : Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix_input_2);
    my $self = {
	#!
	#! Internal data-sets referring to hpLysiss C/C++ data-structures:
	internal => {
	    data_1 => $data_1,
	    data_2 => $data_2,
	    #data_2 = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($matrix_input_2),
	    # data_1 => static_hpLysis__insertMatrix__intoInternal($matrix_input_1), #! where we 'transfer' to a type expected to be of type "s_kt_matrix_t",
	    # data_2 => static_hpLysis__insertMatrix__intoInternal($matrix_input_2), #! where we 'transfer' to a type expected to be of type "s_kt_matrix_t",
	    result_matrix => undef,
	    result_clusterObject => undef,

	    #! PRovide support for fucniton-sepficiaotn of simlairty-metircs:
	    objSim_preCluster => undef, 	    objSim_insideCluster => undef,
	    # data_2 => undef, #! expected to be of type "s_kt_matrix_t",
	},
	#!
	#!
	#logics => $optional__config,
    };
    bless($self, $class);
    return $self;    
}

#! Closes the file-pointers opend for this object.
sub close {
    my ($self) = @_;
    if(!defined($self) ) {
	croak("The input-arguments were not correctly defined");
    }
    {
	#! De-allcoates the "s_kt_matrix_t" object, ie, if latter is set:
	Algorithm::hpLysis_internal::generic__freeMem($self->{internal}->{data_1});  	    $self->{internal}->{data_1} = undef;
	Algorithm::hpLysis_internal::generic__freeMem($self->{internal}->{data_2});  	    $self->{internal}->{data_2} = undef;
	Algorithm::hpLysis_internal::generic__freeMem($self->{internal}->{result_matrix});  $self->{internal}->{result_matrix} = undef;	
    }
    #!! *************************************************************************
    #! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
    # hpLysis__globalFree__kt_api();
    #!! *************************************************************************
}

# TODO: consider adding support for [”elow] ... ie, to incldue [”elow] call to our "kt_matrix_filter.h"..... ie, sacccofld a code-chyunk in oru "hpLysis.pm" to use latter (where we delay 'actual impelmetnainto' to a cocnrete sue-case-requrmenet wrt. oru ajvascirpt-appraoch).
sub __filterMatrix {
    my($mat_hpLysis, $s_kt_matrix_filter_t, $arrStr_orderOf_filter_eitherOr, $arrStr_orderOf_filter_cellMask, $arrStr_orderOf_adjustment, $typeOf_adjust_subtractOrAdd_rows, $typeOf_adjust_subtractOrAdd_cols) = @_;
    if(!defined($mat_hpLysis) || !defined($s_kt_matrix_filter_t)) {
	croak("!!\t API Not as expected");
    }
    #!
    #! SEt the matrix:
    my $isTo_init = 0; #! ie, as we asusme the caller has explcitly itnalized 'this'.
    hpLysis_subset::allocate__fromObj__s_kt_matrix_filter_t($s_kt_matrix_filter_t, $mat_hpLysis, $isTo_init, $typeOf_adjust_subtractOrAdd_rows, $typeOf_adjust_subtractOrAdd_cols);
    #!
    #! Apply the filters speicided by the user:
    if(defined($arrStr_orderOf_filter_eitherOr)) {
	my $arr = $arrStr_orderOf_filter_eitherOr;
	foreach my $str (@{$arr}) {
	    if(defined($str) && ($str !~/^\s*$/) ) {
		hpLysis_subset::string__updateOrderOf_filter__eitherOr__kt_matrix_filter_t($s_kt_matrix_filter_t, $str);
	    }
	}
    }
    if(defined($arrStr_orderOf_filter_cellMask)) {
	my $arr = $arrStr_orderOf_filter_cellMask;
	foreach my $str (@{$arr}) {
	    if(defined($str) && ($str !~/^\s*$/) ) {
	      hpLysis_subset::string__updateOrderOf_filter__cellMask__kt_matrix_filter_t($s_kt_matrix_filter_t, $str);
	    }
	}
    }
    if(defined($arrStr_orderOf_adjustment)) {
	my $arr = $arrStr_orderOf_adjustment;
	foreach my $str (@{$arr}) {
	    if(defined($str) && ($str !~/^\s*$/) ) {
	      hpLysis_subset::string__updateOrderOf_adjust__kt_matrix_filter_t($s_kt_matrix_filter_t, $str);
	    }
	}
    }


    # FIXME: write a tut to vierfy the lgocis of [ªbove].
}

#! Spefiy the simalirty-metirc to be sued before clsutering.
sub set_simMetric_preCluster {
    my ($self, $metric_id, $metric_preStep) = @_;
    $self->{internal}->{objSim_preCluster} = Algorithm::hpLysis::advancedAPI::hpLysis_getConfigHash_simliarty($metric_id, $metric_preStep);
}
#! Spefiy the simalirty-metirc to be used as a distance-measure 'inside' the clsuter-algorithm.
sub set_simMetric_insideCluster {
    my ($self, $metric_id, $metric_preStep) = @_;
    $self->{internal}->{objSim_insideCluster} = Algorithm::hpLysis::advancedAPI::hpLysis_getConfigHash_simliarty($metric_id, $metric_preStep);
}

#! Set a matrix for the first data-object:
sub set_matrix_1 {
    my ($self, $matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights) = @_;
    #! 
    #! Convert to the "s_kt_matrix_t" data-format: 
    my $mat_hpLysis = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights);
    $self->{internal}->{data_1} =  $mat_hpLysis; #Algorithm::hpLysis_internal::static_hpLysis__insertMatrix__intoInternal($matrix, $arrOf_rows, $arrOf_cols);
}
#! Set a matrix for the second  data-object:
sub set_matrix_2 {
    my ($self, $matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights) = @_;
    #! 
    #! Convert to the "s_kt_matrix_t" data-format: 
    my $mat_hpLysis = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights);
    $self->{internal}->{data_2} = $mat_hpLysis; #Algorithm::hpLysis_internal::static_hpLysis__insertMatrix__intoInternal($matrix, $arrOf_rows, $arrOf_cols);
}

#! @return a Perl-matrix for matrix(1) (oekseth, 06. arp. 2017).
sub get_matrix_1 {
    my($self, $format) = @_;
    #! Apply logics: 
    my ($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($self->{internal}->{data_1}, $format);
    #my ($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_internal::static_hpLysis__getMatrix__fromInternal($self->{internal}->{data_1}, $format);
    return ($matrix, $arrOf_rows, $arrOf_cols);

}
#! @return a Perl-matrix for matrix(2) (oekseth, 06. arp. 2017).
sub get_matrix_2 {
    my($self, $format) = @_;
    #! Apply logics: 
    #my ($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_internal::static_hpLysis__getMatrix__fromInternal($self->{internal}->{data_2}, $format);
    my ($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($self->{internal}->{data_2}, $format);
    return ($matrix, $arrOf_rows, $arrOf_cols);
}

#! Set a sparse for the first data-object:
sub set_sparse_1 {
    my ($self, $arrOf_triplets) = @_;
    #! 
    #! Convert to the "s_kt_sparse_t" data-format: 
    my $sparseObj = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat($arrOf_triplets);
    $self->{internal}->{data_1} =  $sparseObj; 
}
#! Set a sparse for the second  data-object:
sub set_sparse_2 {
    my ($self, $arrOf_triplets) = @_;
    #! 
    #! Convert to the "s_kt_sparse_t" data-format: 
    my $sparseObj = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat($arrOf_triplets);
    $self->{internal}->{data_2} =  $sparseObj; 
}

#! @return a Perl-sparse for sparse(1) (oekseth, 06. arp. 2017).
sub get_sparse_1 {
    my($self) = @_;
    my @arrOf_backTranslated = Algorithm::hpLysis_wrapper_1d::C_to_perl_pairFloat($self->{internal}->{data_1});
    return \@arrOf_backTranslated;

}
#! @return a Perl-sparse for sparse(2) (oekseth, 06. arp. 2017).
sub get_sparse_2 {
    my($self) = @_;
    my @arrOf_backTranslated = Algorithm::hpLysis_wrapper_1d::C_to_perl_pairFloat($self->{internal}->{data_2});
    return \@arrOf_backTranslated;
}


#! Set the inptu-file for matrix(1) (oekseth, 06. arp. 2017).
sub set_file_matrix_1 {
    my($self, $name_inputFile) = @_;
    #! Validate input:
    if(!defined($self) || !defined($name_inputFile) || ($name_inputFile == "")) {
	croak("!!\t Function-API Not proeprtly used: please udpate your calls.");
    }
    #! 
    #! Apply logics: read from file, and Convert to the "s_kt_matrix_t" data-format: 
    $self->{internal}->{data_1} = Algorithm::hpLysis_internal::__internal__getFromFile__perlFormat__matrix__intoInternalObject($name_inputFile);
}
#! Set the inptu-file for matrix(2) (oekseth, 06. arp. 2017).
sub set_file_matrix_2 {
    my($self, $name_inputFile) = @_;
    #! Validate input:
    if(!defined($self) || !defined($name_inputFile) || ($name_inputFile == "")) {
	croak("!!\t Function-API Not proeprtly used: please udpate your calls.");
    }
    #! 
    #! Apply logics: read from file, and Convert to the "s_kt_matrix_t" data-format: 
    $self->{internal}->{data_2} = Algorithm::hpLysis_internal::__internal__getFromFile__perlFormat__matrix__intoInternalObject($name_inputFile);
}

sub __static_export_matrix {
   my($mat_internal, $name_resultFile) = @_;
   Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_export($mat_internal, $name_resultFile);
    # if(defined($mat_internal)) {
    # 	#!! Export simlairty-metric:
    # 	hpLysis_subset::export__singleCall__s_kt_sparse_t($mat_internal, $name_resultFile, undef);
    # } else {
    # 	printf(STDERR "!!\t internal-sparse Not defined: if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a sparse-in-question. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
    # }
}
sub __static_export_matrix__summary {
   my($mat_internal, $name_resultFile, $for_both_nonTRansposedAndTransposed) = @_;
   Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_export__summary($mat_internal, $name_resultFile, $for_both_nonTRansposedAndTransposed);
    # if(defined($mat_internal)) {
    # 	#!! Export simlairty-metric:
    # 	hpLysis_subset::export__singleCall__s_kt_sparse_t($mat_internal, $name_resultFile, undef);
    # } else {
    # 	printf(STDERR "!!\t internal-sparse Not defined: if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a sparse-in-question. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
    # }
}

#! Export the input-sparse(1) to the file in quesiton.
sub export_matrix_input_1 {
    my($self, $name_resultFile) = @_;
    __static_export_sparse($self->{internal}->{data_1}, $name_resultFile);
}
#! Export the input-sparse(2) to the file in quesiton.
sub export_matrix_input_2 {
    my($self, $name_resultFile) = @_;
    __static_matrix_sparse($self->{internal}->{data_2}, $name_resultFile);
}
#! Export the result-matrix(1) to the file in quesiton.
sub export_matrix_result {
    my($self, $name_resultFile) = @_;
    my $mat_internal = $self->{internal}->{result_matrix};
    if(defined($mat_internal)) {
	__static_export_matrix($mat_internal, $name_resultFile);
    } else {
	printf(STDERR "!!\t result-matrix Not defined: if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a reulst-matrix. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
    }
}
#! Export the result-matrix(1) to the file in quesiton.
sub export_matrix_result__summary {
    my($self, $name_resultFile) = @_;
    my $mat_internal = $self->{internal}->{result_matrix};
    if(defined($mat_internal)) {
	__static_export_matrix__summary($mat_internal, $name_resultFile);
    } else {
	printf(STDERR "!!\t result-matrix Not defined: if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a reulst-matrix. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
    }
}
#! Export an object cosnistning of clsuter-resuls
sub export_result_clusterResults {
    my($self) = @_;
    my $obj_internal = $self->{internal}->{result_clusterObject};
    if(defined($obj_internal)) {
	return Algorithm::hpLysis_internal::C_to_perl_clusterResult($obj_internal);
    } else {
	printf(STDERR "!!\t result-cluster-object Not defined: if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a reulst-matrix. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
    }
}

#! Export an object cosnistning of clsuter-resuls
sub export_result_clusterResults__toFile {
    my($self, $name_resultFile, $isTo_exportInto_javaScript) = @_;
    my $obj_internal = $self->{internal}->{result_clusterObject};
    if(defined($obj_internal)) {
	if(!defined($isTo_exportInto_javaScript) || ($isTo_exportInto_javaScript == 0) ) {
	    hpLysis_subset::export__toFormat__tsv__s_kt_clusterResult_condensed_t($obj_internal, $name_resultFile, "\t", "\n"); #! where latter descirbed (a) the column-sperator, and (B) the row-seperator.
	} else {
	    hpLysis_subset::export__toFormat__javaScript__s_kt_clusterResult_condensed_t($obj_internal, $name_resultFile);
	}
   } else {
	printf(STDERR "!!\t result-cluster-object Not defined: if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a reulst-matrix. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
    }
}

=head
    @brief the main funciton of thos hpLysis API (oekseth, 06. mar. 2017).
    @param <self> is the object which hold the input-data, and where the reuslt-data is 'inserted'.
    @param <matric_id> is the simlairty-metric id to use.
    @param <matric_preStep> is the simlairty-id to use: 'legal' string-values are [undef, none, rank, binary]
    # @param <optional__config> optional: if used follow tempatel (or call) our "hpLysis_config()" function.
    @return the new-constructed data-set.
=cut
sub compute__similarity {
    my($self, $metric_id, $metric_preStep, $useInternalEnum, $enum_metric_groupId) =  @_;
    my $mat_input_1 = $self->{internal}->{data_1}; my $mat_input_2 = $self->{internal}->{data_2};
    my $mat_hpLysis_result = Algorithm::hpLysis::advancedAPI::compute__similarity($metric_id, $metric_preStep, $useInternalEnum, $enum_metric_groupId, $mat_input_1, $mat_input_2); #\@arrOf_triplets, \@arrOf_triplets);
    if(!defined($metric_id)) {
	if(defined($self->{internal}->{objSim_preCluster})) {
	    printf("sets-sim-metric==\"%s\", at %s:%d\n", $metric_id, __FILE__, __LINE__);
	    $metric_id = $self->{internal}->{objSim_preCluster}->{metric_id};
	    $metric_preStep = $self->{internal}->{objSim_preCluster}->{metric_preStep};
	}
    }    
    # croak("FIXME: update [”elow] calls ... sparse-and-dense ... MINE-init ... ");

    # #! 
    # #! Handle cases where users provide a human-redable enum-name, eg, 'using' "Euclid"
    # ($metric_id, $metric_preStep) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($metric_id, $metric_preStep);
    # #! 
    # #! Itniate the reuslt-set:
    # #my($self, $metric_id, $metric_preStep, $optional__config) =  @_;
    # #my $mat_computedSim = hpLysis_subset::setTo_empty__s_kt_matrix_t(); #$mat_computedSim);
    # #! 
    # #! Apply logics:

    # my $sim_config_pre = hpLysis_subset::init__s_kt_correlationMetric_t($metric_id, $metric_preStep);
    # my $mat_computedSim = hpLysis_subset::apply__simMetric_dense__extraArgs__hp_api_fileInputTight($metric_id, $mat_input_1, $mat_input_2); #, $mat_computedSim);
    $self->{internal}->{result_matrix} = $mat_hpLysis_result;
    my ($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($mat_hpLysis_result);
    #my ($matrix_input_2, $arrOf_rows_2, $arrOf_cols_2) = Algorithm::hpLysis_internal::__internal__getFromFile__perlFormat__matrix($matrix_input_2);
    #my $matrix = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($obj_matrix);
    return ($matrix, $arrOf_rows, $arrOf_cols); #! ie, to simplify data-access.
}

=head
   @brief computes and configures the "hpLysisVisu_server.js" wrt.  (oesketh, 06. arp. 2017)
   @param <enum_alg> is the clsutering-algorithm to be applied/used
   @param <cnt_clusters_x> which for k-means-permtuations is the 'k-cluster-count', while for SOM is the x-clsuter-direction.
   @param <cnt_clusters_y>  which is used for SOM-clustering,
   @param <iterative_maxIterCount> SOM and k-means: is used to 'break' an iteraive-aloithm-exueicont (eg, wrt. SOM), 
   @param <iterative_SOM_tauInit>  SOM and k-means: which is used as a 'senitivty-trehshold' during applciaiton of SOM-clustering.
   @param <enum_ccm_matrixBased> is a matrix-based CCM, eg, "Silhouette, "DavisBouldin" and VRC; may be used in combaiton with HCA-based clsutering-algorithms
   @param <objSim_preCluster> optional: the simalrity-metric to use: if set, then we comptue the simarlity using an all-gaisnta-ll aprpaoich, ie, before comptuign wr.t the clustes.
   @param <objSim_insideCluster> the simalrity-metric to use ''during/isnide the lcustierng': deifnes how 'dsitance' si to be itnerpreted during the clusteirng-operaitons.
   @remarks an exammpel fo the  "enum_ccm_matrixBased" is "Silhouette", and "Dunn"; for clsutering may the latter be used in combaiton with HCA-based clustering-algorithms: if used in comniaont with "cluster" then we comptue/infer 'dunamic-clsuter-results' (ie, using a best-fit' aprpaoch wr.t the latter CCM-metirc-enum).
=cut
sub compute__cluster {
    my($self, $enum_alg, $cnt_clusters_x, $cnt_clusters_y, $iterative_maxIterCount, $iterative_SOM_tauInit, $enum_ccm_matrixBased, $objSim_preCluster, $objSim_insideCluster) = @_;
    #!
    #! Hanlde cases wherf e a user has sperately spefiied the simliarty-metirc:
    if(!defined($objSim_preCluster)) {
	if(defined($self->{internal}->{objSim_preCluster})) {
	    $objSim_preCluster= $self->{internal}->{objSim_preCluster};
	    printf("sets-sim-metric==\"%s\", at %s:%d\n", $objSim_preCluster->{metric_id}, __FILE__, __LINE__);
	}
    }
    if(!defined($objSim_insideCluster)) {
	if(defined($self->{internal}->{objSim_insideCluster})) {
	    $objSim_insideCluster = $self->{internal}->{objSim_insideCluster};
	}
    }
    #!
    #! Input-data:
    my $mat_input_1 = $self->{internal}->{data_1}; 
    my $mat_input_2 = $self->{internal}->{data_2};
    { #! De-allcoate old data, ie, invetigate wr.t the latter:
	Algorithm::hpLysis_internal::generic__freeMem($self->{internal}->{result_matrix});  	$self->{internal}->{result_matrix} = undef;
	#Algorithm::hpLysis_internal::generic__freeMem();
	Algorithm::hpLysis_internal::generic__freeMem($self->{internal}->{result_clusterObject}); 
	$self->{internal}->{result_clusterObject} = undef;
    }
    #!
    #! Apply logics:
    my($result_matrix, $result_clusterObject) = Algorithm::hpLysis::advancedAPI::compute__cluster($mat_input_1, $mat_input_2, $enum_alg, $cnt_clusters_x, $cnt_clusters_y, $iterative_maxIterCount, $iterative_SOM_tauInit, $enum_ccm_matrixBased, $objSim_preCluster, $objSim_insideCluster);
    #! 
    #! Store result:
    $self->{internal}->{result_matrix}        = $result_matrix;
    $self->{internal}->{result_clusterObject} = $result_clusterObject;

    #!
    #! @return
    my $obj_cluster = $self->export_result_clusterResults();
    my ($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($result_matrix);
    #my ($matrix_input_2, $arrOf_rows_2, $arrOf_cols_2) = Algorithm::hpLysis_internal::__internal__getFromFile__perlFormat__matrix($matrix_input_2);
    #my $matrix = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($obj_matrix);
    return ($matrix, $arrOf_rows, $arrOf_cols, $obj_cluster); #! ie, to simplify data-access.
}


=head
   @brief configure object to compute a set-based CCM (eg, "Silhouette") (oesketh, 06. arp. 2017)
    @param <self> is the object which hold the input-matrices: 
    @param <enum_ccm_matrixBased> is the matrix-based CCM to evluaate the inptu-amtrix wrt. to
    @remarks an example-applicaiton is to assess the simlairty between diffenret/muliple k-means cluster-results, eg, through the use of "Rands Adjusted Index".
    @return an array where each score is assicatred to each row in the 'second'input-matrix.
 @remarks we expect that data_1.length == data_2[*].length: the latter due to the purpose of this funciton, ie, that there is a 'hypoteiss'asiscated to each of the rows in the first matrix
=cut
sub compute__CCM_matrixBased {
    my($self, $enum_ccm_matrixBased) = @_;
    my $mat_input_1 = $self->{internal}->{data_1}; my $mat_input_2 = $self->{internal}->{data_2};
    my $arrOf_scores = Algorithm::hpLysis::advancedAPI::compute__CCM_matrixBased($enum_ccm_matrixBased, $mat_input_1, $mat_input_2); 
    return $arrOf_scores;
}

=head
   @brief configure object to compute a set-based CCM (eg, "RAnds Adjusted Index")(oesketh, 06. arp. 2017)
    @param <self> is the object which hold the input-matrices: if only the first input-matrix is set, then we 'apply' a self-comparison (an 'appraoch' which if osten sued to evlauate/descirbe the sialmrity between differnet cluster-resutls, eg, cluster-reuslts collected suing diffenre tsim-meitrcs, cluster-alrogirhtms and CCMs). 
   @param <enum_ccm_setBased> is the setBaed cCM to evluaate the inptu-amtrix wrt. to
   @remarks an example-applicaiton is to assess the simlairty between diffenret/muliple k-means cluster-results, eg, through the use of "Rands Adjusted Index".
    @return an Perl- matrix of scores. 
    @remarks matrix-diemsniosn: we expect that data_1[*].length == data_2[*].length: the latter due to the purpose of this funciton, ie, that there is a 'hypoteiss' asiscated to each of the rows in the both matrices, ie, as we compare the identies seperately for each row_1[..]x[row_2[..].
=cut
sub compute__CCM_setBased {
    my($self, $enum_ccm_setBased) = @_;
    my $mat_input_1 = $self->{internal}->{data_1}; my $mat_input_2 = $self->{internal}->{data_2};
    my $matrix = Algorithm::hpLysis::advancedAPI::compute__CCM_setBased($enum_ccm_setBased, $mat_input_1, $mat_input_2); 
    return $matrix;
}



=head
    @brief describe different "stringOf_enum" inptu-values which may be used in our "compute(..)" function (oekseth, 06. mar. 2017).
    @param <option_case> the type of 'state' to limit the 'option-out-print'. If Not set, then we print our 'for all of the cases'.
=cut
sub help__printOptions {
    my($option_case) = @_;
    
    if(!defined($option_case) || ($option_case eq "")) {
	; #FIXME ... call function such as "printOptions__hp_api_fileInputTight()"
    } elsif($option_case eq "ccm_matrix") {
	; #FIXME
    } elsif($option_case eq "ccm_goldXgold") {
	; #FIXME
    } elsif($option_case eq "sim") {
	; #FIXME
    } elsif($option_case eq "clustAlg") {
	; #FIXME
    } else {
	printf(STDERR "!!\t Option-case=\"%s\" is unkown: please read the dcouemtatniion, and if latter does Not help, then contect senior developer [oekseth\@gmail.com]. OBserviaotn at %s:%d\n", $option_case, __PACKAGE__, __LINE__);
    }
}

#! @return a cofnigruation for our ipnut-object (oekseth, 06. mar. 2017)
sub hpLysis_config {
    my $self = {
	cnt_clusters_x => undef,	
	# cnt_clusters_y => 0,	
	# stringOf_enum => undef,
	stringOf_exportFile => undef,
	stringOf_exportFile__format => undef,
	formatOf_input__rowsAreTriplets => 0, #! which is set to "1" implies that the data-set is sparse (and Not a desne matrix of values).
	formatOf_input__argsAre_fileNames__notMatrices => 0, #! which if set to "1" implies that "matrix_input_1" and "matrix_input_2" are file-names (of TSV-files in the hpLysis matrix-data-format), ie, for "= 1" we assuem inputs are Not matrix-variables.
    };
    return $self;
}


=head
    @brief a simplifed hpLysis access-proceudre to the large hpLysis-cluster-library, ie, a simple gui with limtied function-set, ie, easy to use though limited expressive power (Eg, wr.t the metrics to use) (oekseth, 06. mar. 2017).
    @param <stringOf_enum> is the lgocial oepraiton to apply
    @param <matrix_input_1> the first input-matrix.
    @param <matrix_input_2> optional: the second input-matrix
    @param <optional__config> optional: if used follow tempatel (or call) our "hpLysis_config()" function.
    @return the new-constructed data-set.
=cut
sub hpLysis_standAlone {
    my($stringOf_enum, $matrix_input_1, $matrix_input_2, $optional__config) =  @_;
    if(!defined($optional__config)) {
	$optional__config = hpLysis_config(); #! ie, then use defualt cofnigruations.
    }
    
    # TODO: consider adding support for other enum-features ... and cosnider to 'call directly' our [ªov€] "Algorithm::hpLysis->new(..)" object.

    my $mat_internal = undef;
    my ($arrOf_rows_1, $arrOf_cols_1) = (undef, undef);     my ($arrOf_rows_2, $arrOf_cols_2) = (undef, undef);
    if($optional__config->{formatOf_input__argsAre_fileNames__notMatrices} == 1) { #! then we assuem inptu-arguemnts refers to files:
	($matrix_input_1, $arrOf_rows_1, $arrOf_cols_1) = Algorithm::hpLysis_internal::__internal__getFromFile__perlFormat__matrix($matrix_input_1);
	if(defined($matrix_input_2)) {
	    ($matrix_input_2, $arrOf_rows_2, $arrOf_cols_2) = Algorithm::hpLysis_internal::__internal__getFromFile__perlFormat__matrix($matrix_input_2);
	}
    } 
    {
	#! Convert 'high-levle' amtrix into n internal matrix: 
	my $mat_1 = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix_input_1, $arrOf_rows_1, $arrOf_cols_1); #, $arrOf_colWeights);
	#my $mat_1 = Algorithm::hpLysis_internal::static_hpLysis__insertMatrix__intoInternal($matrix_input_1, $arrOf_rows_1, $arrOf_cols_1);
	#my $is_ok = data_is_valid_matrix($matrix_input_1);
	if(defined($mat_1)) {
	    #!! *************************************************************************
	    #! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
	    hpLysis_subset::hpLysis__globalInit__kt_api();    
	    #!! *************************************************************************

	    my $mat_ret = undef;
	    my $mat_2 = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix_input_2, $arrOf_rows_2, $arrOf_cols_2); #, $arrOf_colWeights);
	    # my $mat_2 = Algorithm::hpLysis_internal::static_hpLysis__insertMatrix__intoInternal($matrix_input_2, $arrOf_rows_2, $arrOf_cols_2);
	    #!
	    #! Apply logics:
	    my $cnt_clusters_x = $optional__config->{cnt_clusters_x};
	    my $stringOf_exportFile = $optional__config->{stringOf_exportFile};
	    my $stringOf_exportFile__format = $optional__config->{stringOf_exportFile__format};
	    my $config_inputFile_holdTriplets = $optional__config->{formatOf_input__rowsAreTriplets};
	    my $mat_result = hpLysis_subset::setTo_empty__s_kt_matrix_t(); #$mat_result);
	    my $mat_result = hpLysis_subset::readFromFile__stroreInStruct__exportResult__hp_api_fileInputTight($stringOf_enum, $mat_1, $mat_2, 
													       #$mat_result, 
													       $stringOf_exportFile, $stringOf_exportFile__format, 
													       $cnt_clusters_x, $config_inputFile_holdTriplets);

	    #!
	    #! Convert the 'itnerla' "s_kt_matrix_t" format to the Perl-based data-format:
	    my ($matrix, $arrOf_rows, $arrOf_cols) = (undef, undef, undef);
	    if(defined($mat_result)) {
		($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($mat_result);
		#($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_internal::static_hpLysis__getMatrix__fromInternal($mat_result); #, $format);
		hpLysis_subset::free__s_kt_matrix($mat_result);
	    }
	    #!
	    #! De-allcoate the internal results:
	    hpLysis_subset::free__s_kt_matrix($mat_1);
	    if(defined($mat_2)) {
		hpLysis_subset::free__s_kt_matrix($mat_2);
	    }
	    #!! *************************************************************************
	    #! A 'glboal inti-funciton' wrt. our hpLysis-ciode (oekseth, 06. des. 2016).
	    hpLysis__globalFree__kt_api();
	    #!! *************************************************************************
	    
	    #!
	    #! @return the result:
	    #! Note: a 'matrix' is used to simplify the 'specifciaiton' fo different result-varialbes, ie, a 'genreliased approch'.
	    return ($matrix, $arrOf_rows, $arrOf_cols);
	} else {
	    printf(STDERR "!!\t(error)\t Input-matrices are empty, ie, unable to copmtue for your requested call: please update your configurationis, at %s:%d\n", __FILE__, __LINE__);
	    return undef;
	}
    }
    return undef;
}
