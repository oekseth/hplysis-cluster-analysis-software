package Algorithm::hpLysis_metrics;
use strict;
# use DynaLoader;
use hpLysis_subset;
require Exporter;


=head
    @brief provide logcis for 'fethcing' the correct enum when/if string-inptus are 'given'/provided (oekseth, 06. arp. 2017).
    @remarks for a use-case wrt. this module/class see our "tut_params_3_enums.pl", where the latter tutorial (tut) examplfies funciton-rutines in our "Algorithm::hpLysis_metrics" (defined in our "hpLysis_metrics.pm") for fetching of configuraiton-enums for simlairty-metircs, clusteirng, and Cluster-Comparison Metrics (CCMs).  
=cut


#! A wrapper-function to simplify adding of enum-hasses into an erray (oekseth, 06. apr. 2017)
sub __get_h1_h2_hashPair_lc {
    my($h1, $h2) = @_;
    my $map_object = {
	h1 => lc($h1),
	h2 => lc($h2),
    };
    #printf("syn=\"%s\" VS mapping==\"%s\", at %s:%d\n", $map_object->{h1}, $map_object->{h2}, __FILE__, __LINE__);
    return $map_object;
}
#! @return a 'rnaomzleid stirng', using an array of "__get_h1_h2_hashPair_lc" nraomliation-objects.
sub getUpdatedNAme__fromList____get_h1_h2_hashPair_lc {
    my ($input_name, $mapping_list) = @_;
    $input_name = lc($input_name); #! ie, to avodi case-sentivity'.
    foreach my $obj (@{$mapping_list}) {
	if(defined($obj)) {
	    # printf("searhc=\"%s\" VS mapping==\"%s\", at %s:%d\n", $input_name, $obj->{h1}, __FILE__, __LINE__);
	    if($input_name eq $obj->{h1}) {
		$input_name = $obj->{h2}; #! ie, as we havve then fixed the misspelling.
	    }	
	}
    }
    return $input_name;
}

#! @return the set of metirc-pre-step-names which may be used in configuring our "hpLysisVisu_server.prototype.set__simMetric(..)" "hpLysisVisu_server.js" object (oekseth, 06. apr. 2017).
sub hpLysis_metrics__static__getSetOf__simMetrics__preSteps {
    return ("rank", "binary", "none"); #! where "none" is the 'defualt' matirx-name to be applied.
}


#! @return the set of metirc-names which may be used, both from Perl and from other wraping-languages, eg, wrt. in configuring our "hpLysisVisu_server.prototype.set__simMetric(..)" "hpLysisVisu_server.js" object (oekseth, 06. apr. 2017).
sub hpLysis_metrics__static__getSetOf__simMetrics {
    my ($useInternalEnum)  = @_;
    if( !defined($useInternalEnum)  || ($useInternalEnum == 0) ) {
	$useInternalEnum = 0;
    } 
    my $cnt_sim = $hpLysis_subset::e_kt_correlationFunction_undef;
    # my %arrOf_groups;  = Object.keys(mapOf_group_to_enum); #! ie, then get the first key.
    # if(arrOf_groups.length == 0) {console.log(dummy_toBreak);} #! ie, then an eror int he 'datra-loading'.
    my $enum_metric_foundLocally = undef;
    my @arrOf_metrics_result = ();
    for(my $metric_id = 0; ($metric_id < $cnt_sim) && !defined($enum_metric_foundLocally); $metric_id++) {
	#! Call hpLysis to get the name (of the enum):
	my $name = Algorithm::hpLysis_internal_returnWrappers::get__s_kt_swig_retValObj_t__string(\&hpLysis_subset::get_stringOf_enum__e_kt_correlationFunction_t_scalar, $metric_id);
	# my $name = hpLysis_subset::get_stringOf_enum__e_kt_correlationFunction_t_scalar($metric_id);  #! where latter fucniton is deinfed in our "kt_metric_aux.h".
	if($name eq "") {
	    printf(STDERR "!!\t ivnegigate this case, given enum=\"$metric_id\", at %s:%d\n", __FILE__, __LINE__);
	}
	if($useInternalEnum) {push(@arrOf_metrics_result, $name);}
	else {
	    my ($group_id, $name_human) = ($name =~ /groupOf_(.+?)_(.+)$/i);
	    push(@arrOf_metrics_result, $name_human);
	}
    }
    #! @return the reuslt
    return @arrOf_metrics_result;
}


#! @return the set of metirc-pre-step-names which may be used in configuring our "hpLysisVisu_server.prototype.set__clustering(..)" "hpLysisVisu_server.js" object (oekseth, 06. apr. 2017).
#! @remakrs for detials wrt. [”elow] see our "get_stringOf__e_hpLysis_clusterAlg_t" (deifned in our "hpLysis_api.h").
sub hpLysis_metrics__static__getSetOf__clusterAlgs {
    return (
	# ----------
	"algorithm::HCA::single",
	"algorithm::HCA::max",
	"algorithm::HCA::average",
	"algorithm::HCA::centroid",
	# ----------
	"algorithm::k-means::avg",
	"algorithm::k-means::rank",
	"algorithm::k-means::medoid",
	# ----------
	"algorithm::disjoint",
	# ----------
	"algorithm::k-means::altAlg::miniBatch",
	# ----------
	"algorithm::disjoint::k-means::avg",
	"algorithm::disjoint::k-means::rank",
	"algorithm::disjoint::k-means::medoid",
	# ----------
	"algorithm::k-means::SOM",
	# ----------
	"algorithm::Kruskal::HCA",
	"algorithm::Kruskal::fixed",
	"algorithm::Kruskal::fixed::CCM",
	#"",
    );
}


#! @return the set of metirc-pre-step-names which may be used in configuring our "hpLysisVisu_server.prototype.set__clustering(..)" "hpLysisVisu_server.js" object (oekseth, 06. apr. 2017).
#! @remakrs for detials wrt. [”elow] see our "getString__shortIds__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t" (deifned in our "kt_matrix_cmpCluster.h").
sub hpLysis_metrics__static__getSetOf__ccm__matrixBased {
    return  (
	"Silhouette",
	"VRC",
	"VRC::rel", #! ie, where VRC is adjsuted wrt.t he 'relative importnace'.
	"DBI",
	"DBI(Euc)", #! ie, wher elatter use Euclid
	"Dunn",
	"Dunn(rel)", #! ie, where Dunn is adjsuted wrt.t he 'relative importnace'.
	#"",
    );
}
sub __hpLysis_metrics__static__getSetOf__ccm__matrixBased_normalizationObjects__getArrOfRef {
    return  [									     
	__get_h1_h2_hashPair_lc("Silhouette's Index", "Silhouette"),
	__get_h1_h2_hashPair_lc("Silhouette Index", "Silhouette"),
	__get_h1_h2_hashPair_lc("SilhouetteIndex", "Silhouette"),
	__get_h1_h2_hashPair_lc("CalinskiHarabasz", "VRC"),
	__get_h1_h2_hashPair_lc("Calinski Harabasz", "VRC"),
	__get_h1_h2_hashPair_lc("Calinski-Harabasz", "VRC"),
	__get_h1_h2_hashPair_lc("Davis Bouldin", "DBI"),
	__get_h1_h2_hashPair_lc("DavisBouldin", "DBI"),
	__get_h1_h2_hashPair_lc("Davis Bouldin's Index", "DBI"),
	__get_h1_h2_hashPair_lc("davis bouldins index", "DBI"),
	__get_h1_h2_hashPair_lc("Davis Bouldin Index", "DBI"),
	__get_h1_h2_hashPair_lc("Dunns Index", "Dunn"),
	__get_h1_h2_hashPair_lc("Dunn's Index", "Dunn"),
	__get_h1_h2_hashPair_lc("DunnsIndex", "Dunn"),
	__get_h1_h2_hashPair_lc("Dunns-Index", "Dunn"),
	];
}
#! @return the set of metirc-pre-step-names which may be used in configuring our "hpLysisVisu_server.prototype.set__ccm__setBased(..)" "hpLysisVisu_server.js" object (oekseth, 06. apr. 2017).
#! @remakrs for detials wrt. [”elow] see our "getStringOf__e_kt_matrix_cmpCluster_metric_t(..)" (deifned in our "kt_matrix_cmpCluster.h").
sub hpLysis_metrics__static__getSetOf__ccm__setBased {
    return (
	"Rands-index",
	"Rands-index (alternative-implementation)",
	"Adjusted Rand Index (ARI)",
	"Chi-squared",
	"Fowles-Mallows",
	"Mirkin",
	"Wallace",
	"Jaccard",
	"Jaccard (F1-score)",
	"Minkowski",
	"PR",
	"PR (alternative implementation)",
	"Partition-Difference",
	"F-Measure",
	"maximum-match-measure",
	"van Dogen",
	"van Dogen (oekseth::relative)",
	"Strehl & Gosh",
	"Fred & Jain",
	"Variation-of-information",
	"Variation-of-information (oekseth::relative)",
	"Silhouette Index",
	"Davis Bouldin",
	"Dunn",
	#"",
    );
}

#! @reutnr an 'empty and defualt vlaue' for the cluster-algorithms supproted by hpLysis
sub hpLysis_metrics__static__enumEmptyValueFor__clusterAlg {
    return $hpLysis_subset::e_hpLysis_clusterAlg_undef;
}

#! @reutnr an 'empty and defualt vlaue' for the matrix-based CCMs. 
sub hpLysis_metrics__static__enumEmptyValueFor__ccm_matrixBased {
    return $hpLysis_subset::e_kt_matrix_cmpCluster_clusterDistance__cmpType_undef;
}
#! @reutnr an 'empty and defualt vlaue' for the set-based CCMs. 
sub hpLysis_metrics__static__enumEmptyValueFor__ccm_setBased {
    return $hpLysis_subset::e_kt_matrix_cmpCluster_metric_undef;
}

#! Identified a 'correct' matrix-based enum wrt. 'base' cluster-algortihms supported by hpLysis (oekseth, 06. apr. 2017)
sub hpLysis_metrics__static__translateIntoInternalEnum_clusterAlg {
    my($enum_id) = @_;
    my $__const__enumUndef = hpLysis_metrics__static__enumEmptyValueFor__clusterAlg(); #! ie, to simplify code-validation we 'store' teh latter in an internal variable. 
    if(!defined($enum_id)) {
	printf(STDERR "!!\t funciotn-inptu Not as expected, ie, investigate. Observiaont at %s:%d\n", __FILE__, __LINE__);
	return $__const__enumUndef;
    }
    my $enum_id_updated = $__const__enumUndef;
    if(defined($enum_id) && length($enum_id) && ($enum_id !~ /^\s*$/)) {
	if($enum_id !~ /^\d+$/) {
	    #! Add a nroamlizaiotn-step, ie, to reoslve name-ambiguty:
	    $enum_id = getUpdatedNAme__fromList____get_h1_h2_hashPair_lc($enum_id, [
									     __get_h1_h2_hashPair_lc("hca-single", "algorithm::HCA::single"),
									     __get_h1_h2_hashPair_lc("hca-max", "algorithm::HCA::max"),
									     __get_h1_h2_hashPair_lc("hca-max", "algorithm::HCA::max"),
									     __get_h1_h2_hashPair_lc("hca-centroid", "algorithm::HCA::centroid"),
									     __get_h1_h2_hashPair_lc("hca-average", "algorithm::HCA::average"),
									     __get_h1_h2_hashPair_lc("k-means", "algorithm::k-means::avg"), #! ie, as the latter is the 'most-sued' k-emans-permtautison in the reseahrc-aritlces we have elvuated.
									     __get_h1_h2_hashPair_lc("kmeans", "algorithm::k-means::avg"), #! ie, as the latter is the 'most-sued' k-emans-permtautison in the reseahrc-aritlces we have elvuated.
									     __get_h1_h2_hashPair_lc("disjoint", "algorithm::disjoint"),
									     __get_h1_h2_hashPair_lc("miniBatch", "algorithm::k-means::altAlg::miniBatch"),
									     __get_h1_h2_hashPair_lc("Self-Organising Maps", "SOM"),
									     __get_h1_h2_hashPair_lc("Self Organising Maps", "SOM"),
									     __get_h1_h2_hashPair_lc("Self-Organising Map", "SOM"),
									     __get_h1_h2_hashPair_lc("Self Organising Map", "SOM"),
									     # __get_h1_h2_hashPair_lc("", ""),
									     # __get_h1_h2_hashPair_lc("", ""),
									 ]);
	    #! Apply the lgoics:
	    $enum_id_updated = hpLysis_subset::get_enumOf__e_hpLysis_clusterAlg_t__retVal($enum_id);
	    if($enum_id_updated == $__const__enumUndef) {
		printf(STDERR "!!\t(API-error)\t The provides enum_id=\"%s\" seems odd, ie, as the latter was Not found in our colleciotn of enum-ids, ie, please investigate. Observiaon at %s:%d\n", $enum_id, __FILE__, __LINE__);
		return $__const__enumUndef;
	    }
	} else {
	    if($enum_id > $__const__enumUndef) {
		printf(STDERR "!!\t(API-error)\t The provides enum_id=\"%d\" seems odd, ie, as the latter is outside the enum-number=%d, ie, please investigate. Observiaon at %s:%d\n", $enum_id, $__const__enumUndef, __FILE__, __LINE__);
		my @arrOf_metrics = Algorithms::hpLysis_metrics::hpLysis_metrics__static__getSetOf__clusterAlgs();
		printf(STDERR "!!\t(API::notSupportedInputEnum)\t Was Not able to find any cluster-algorithm-enums matching your API-call. To simplify debugging: you reuqested enum_metric-preStep=" . $enum_id .  ", where supported enum-names (for metric-preSTeps) are" .  join(",", @arrOf_metrics));
		return $__const__enumUndef;
	    }
	    #! Update: 
	    $enum_id_updated = $enum_id; #! ie, as we assuem the latter is a number represiting/cpatuign the ENUM-id in quesiton. 
	}
    }
    #!
    #! Ad this exeuct-point we asusem the oepraiton was a success.
    return $enum_id_updated;
} 

#! Idneites a 'correct' matrix-based enum wrt. matrix-based CCMs (oekseth, 06. apr. 2017)
sub hpLysis_metrics__static__translateIntoInternalEnum_ccm__matrixBased {
    my($enum_id) = @_;
    my $__const__enumUndef = hpLysis_metrics__static__enumEmptyValueFor__ccm_matrixBased();; #! ie, to simplify code-validation we 'store' teh latter in an internal variable. 
    if(!defined($enum_id)) {
	printf(STDERR "!!\t funciotn-inptu Not as expected, ie, investigate. Observiaont at %s:%d\n", __FILE__, __LINE__);
	return $__const__enumUndef;
    }
    my $enum_id_updated = $__const__enumUndef;
    if(defined($enum_id) && length($enum_id) && ($enum_id !~ /^\s*$/)) {
	if($enum_id !~ /^\d+$/) {
	    #! Add a nroamlizaiotn-step, ie, to reoslve name-ambiguty:
	    $enum_id = getUpdatedNAme__fromList____get_h1_h2_hashPair_lc($enum_id, __hpLysis_metrics__static__getSetOf__ccm__matrixBased_normalizationObjects__getArrOfRef());
	    #! Apply the lgoics:
	    $enum_id_updated = hpLysis_subset::getEnum__e_kt_matrix_cmpCluster_clusterDistance__cmpType_t__retVal__uint($enum_id);
	    if($enum_id_updated == $__const__enumUndef) {
		printf(STDERR "!!\t(API-error)\t The provides enum_id=\"%s\" seems odd, ie, as the latter was Not found in our colleciotn of enum-ids, ie, please investigate. Observiaon at %s:%d\n", $enum_id, __FILE__, __LINE__);
		return $__const__enumUndef;
	    }
	} else {
	    if($enum_id > $__const__enumUndef) {
		printf(STDERR "!!\t(API-error)\t The provides enum_id=\"%d\" seems odd, ie, as the latter is outside the enum-number=%d, ie, please investigate. Observiaon at %s:%d\n", $enum_id, $__const__enumUndef, __FILE__, __LINE__);
		my @arrOf_metrics = Algorithms::hpLysis_metrics::hpLysis_metrics__static__getSetOf__ccm__matrixBased();
		printf(STDERR "!!\t(API::notSupportedInputEnum)\t Was Not able to find any ccn-matrix-based-enums matching your API-call. To simplify debugging: you reuqested enum_metric-preStep=" . $enum_id .  ", where supported enum-names (for metric-preSTeps) are" .  join(",", @arrOf_metrics));
		return $__const__enumUndef;
	    }
	    #! Update: 
	    $enum_id_updated = $enum_id; #! ie, as we assuem the latter is a number represiting/cpatuign the ENUM-id in quesiton. 
	}
    }
    #!
    #! Ad this exeuct-point we asusem the oepraiton was a success.
    return $enum_id_updated;
}
#! Idneites a 'correct' matrix-based enum wrt. set-based CCMs (oekseth, 06. apr. 2017)
sub hpLysis_metrics__static__translateIntoInternalEnum_ccm__setBased {
    my($enum_id) = @_;
    my $__const__enumUndef = hpLysis_metrics__static__enumEmptyValueFor__ccm_setBased();; #! ie, to simplify code-validation we 'store' teh latter in an internal variable. 
    if(!defined($enum_id)) {
	printf(STDERR "!!\t funciotn-inptu Not as expected, ie, investigate. Observiaont at %s:%d\n", __FILE__, __LINE__);
	return $__const__enumUndef;
    }
    my $enum_id_updated = $__const__enumUndef;
    if(defined($enum_id) && length($enum_id) && ($enum_id !~ /^\s*$/)) {
	if($enum_id !~ /^\d+$/) {
	    #! Add a nroamlizaiotn-step, ie, to reoslve name-ambiguty:
	    $enum_id = getUpdatedNAme__fromList____get_h1_h2_hashPair_lc($enum_id, [
									     __get_h1_h2_hashPair_lc("rands index", "rands-index"),
									     __get_h1_h2_hashPair_lc("randsIndex", "rands-index"),
									     __get_h1_h2_hashPair_lc("rand", "rands-index"),
									     __get_h1_h2_hashPair_lc("Rands Adjusted Index", "Adjusted Rand Index (ARI)"),
									     __get_h1_h2_hashPair_lc("ARI", "Adjusted Rand Index (ARI)"),
									     __get_h1_h2_hashPair_lc("rands-adjusted", "Adjusted Rand Index (ARI)"),
									     __get_h1_h2_hashPair_lc("Fowles Mallows", "Fowles-Mallows"),
									     __get_h1_h2_hashPair_lc("FowlesMallows", "Fowles-Mallows"),
									     __get_h1_h2_hashPair_lc("Partition Difference", "Partition-Difference"),
									     __get_h1_h2_hashPair_lc("FMeasure", "F-Measure"),
									     __get_h1_h2_hashPair_lc("maximum match measure", "maximum-match-measure"),
									     __get_h1_h2_hashPair_lc("vanDogen", "van Dogen"),
									     __get_h1_h2_hashPair_lc("van-dogen", "van Dogen"),
									     __get_h1_h2_hashPair_lc("Strehl Gosh", "Strehl & Gosh"),
									     __get_h1_h2_hashPair_lc("Strehl-Gosh", "Strehl & Gosh"),
									     __get_h1_h2_hashPair_lc("StrehlGosh", "Strehl & Gosh"),
									     __get_h1_h2_hashPair_lc("Fred-Jain", "Fred & Jain"),
									     __get_h1_h2_hashPair_lc("Fred Jain", "Fred & Jain"),
									     __get_h1_h2_hashPair_lc("FredJain", "Fred & Jain"),
									     __get_h1_h2_hashPair_lc("variation of information", "Variation-of-information"),
									     __get_h1_h2_hashPair_lc("variationinformation", "Variation-of-information"),
									     __get_h1_h2_hashPair_lc("variation-information", "Variation-of-information"),
									     #__get_h1_h2_hashPair_lc("", ""),
									     #__get_h1_h2_hashPair_lc("", ""),
									     #! 
									     #! Note: [”elow] is 'grabbed' from our amtrix-based nroamlziaiotn-set:
									     @{__hpLysis_metrics__static__getSetOf__ccm__matrixBased_normalizationObjects__getArrOfRef()}
									 ]);
	    #! Apply the lgoics:{
	    $enum_id_updated = hpLysis_subset::getEnumOf__e_kt_matrix_cmpCluster_metric_t__retVal__uint($enum_id);
	    if($enum_id_updated == $__const__enumUndef) {
		printf(STDERR "!!\t(API-error)\t The provides enum_id=\"%s\" seems odd, ie, as the latter was Not found in our colleciotn of enum-ids, ie, please investigate. Observiaon at %s:%d\n", $enum_id, __FILE__, __LINE__);
		return $__const__enumUndef;
	    }
	} else {
	    if($enum_id > $__const__enumUndef) {
		printf(STDERR "!!\t(API-error)\t The provides enum_id=\"%d\" seems odd, ie, as the latter is outside the enum-number=%d, ie, please investigate. Observiaon at %s:%d\n", $enum_id, $__const__enumUndef, __FILE__, __LINE__);
		return $__const__enumUndef;
	    }
	    #! Update: 
	    $enum_id_updated = $enum_id; #! ie, as we assuem the latter is a number represiting/cpatuign the ENUM-id in quesiton. 
	}
    }
    #!
    #! Ad this exeuct-point we asusem the oepraiton was a success.
    return $enum_id_updated;
}


=head
   @brief configures the "hpLysisVisu_server.js" wrt. the simlairty-metic (and 'poitn of applciaiton) (oesketh, 06. arp. 2017)
   @param <enum_metric> is the simliarty-metric which we are to comptue for;
   @param <enum_preStep> is the simliarty-metric-pres-tep which is to be applied (Before applciaotn/use of the "simMetricPre_metricId" simliarty-emtric pre-step); 
   @param <isFor_preStep> whivh if set to false impleis that the simarty-emtric configruations is to be used 'inside the clsuteirng-operaiotn-phase'.
   @param <useInternalEnum> which if set to true implies that we use the ipnut-aprameters 'as-is': otherwise we use/apply a regualr expresison to idneitfy the correct 'internal enum' to apply.
   @param <useInternalEnum_notGroupId> which may be set inc onctuions with "useInternalEnum == false" to restrict/lmite the searhc for a 'matching' simlarity-metirc-enum.
   @return true if "useInternalEnum == true" or "enum_metric" is found 'inside' the set of 'allwoable' emtric-names.
   @remarks for a list of metic-names which may be applied
   @remarks the set of uspported matrics are: 
=cut
sub hpLysis_metrics__get__simMetric {
    my ($enum_metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId) = @_;
    if( !defined($useInternalEnum)) {
	if($enum_metric =~/^\d+$/) {$useInternalEnum = 1;} #! ie, as latter then is assicated with a enum-prefix used in Knitting-Tools .
	else {$useInternalEnum= 0;}
    }
    #printf("enum_metric_groupId=\"$enum_metric_groupId\", at %s:%d\n", __FILE__, __LINE__);
    if($useInternalEnum == 0) { #! then handle diffenret misepplgins whichwe have currnelty added support of (oekseth, 06. apr. 2017).
	$enum_metric = getUpdatedNAme__fromList____get_h1_h2_hashPair_lc($enum_metric, [
									     __get_h1_h2_hashPair_lc("city-block", "cityblock"),
									     __get_h1_h2_hashPair_lc("city block", "cityblock"),
									     __get_h1_h2_hashPair_lc("manhatten", "cityblock"),
									     __get_h1_h2_hashPair_lc("manhatten distance", "cityblock"),
									     __get_h1_h2_hashPair_lc("euclidian", "euclid"),
									     __get_h1_h2_hashPair_lc("Kendalls Tau", "kendall_dice"), #! ie, our defualt assumption: for details of optiosn see our "e_kt_correlationFunction.h"
									     __get_h1_h2_hashPair_lc("Kendall's Tau", "kendall_dice"), #! ie, our defualt assumption: for details of optiosn see our "e_kt_correlationFunction.h"
									 ]);
	#printf("enum_metric=$enum_metric, at %s:%d\n", __FILE__, __LINE__);
    }
    #if( !defined($isFor_preStep) ) {$isFor_preStep = 1;}
    { #! Validate the "enum_preStep" option:	
	if( !defined($enum_preStep)  || ($enum_preStep eq "") ) {$enum_preStep = $hpLysis_subset::e_kt_categoryOf_correaltionPreStep_none;} #! ie, use hte deulfat 'spec' of the sim-metic.
	if($enum_preStep !~/^\d+$/) {
	    my $old_preStep = $enum_preStep;
	    $enum_preStep = hpLysis_subset::get_typeOf_correlationPreStep_fromString_scalar($enum_preStep);	    
	    #printf("enum_preStep=\"$enum_preStep\", given input=\"$old_preStep\", at %s:%d\n", __FILE__, __LINE__);
	    #croak("..");
	}
    }
    if( !defined($useInternalEnum) || ($useInternalEnum == 0) ) {
	#! Note: [below] data-structred are expected to be dinfed and found in our "autoGenerated_web__enumCategory.js", a data-structred epsxected to be provided by the "hpLysis" software.
	my $cnt_sim = $hpLysis_subset::e_kt_correlationFunction_undef;
	my $enum_metric_foundLocally = undef;
	my @arrOf_metrics_result = ();
	my @arrOf_metrics = ();
	for(my $metric_id = 0; ($metric_id < $cnt_sim) && !defined($enum_metric_foundLocally); $metric_id++) {
	    #! Get the 'strineifed' enum-name:
	    my $name = Algorithm::hpLysis_internal_returnWrappers::get__s_kt_swig_retValObj_t__string(\&hpLysis_subset::get_stringOf_enum__e_kt_correlationFunction_t_scalar, $metric_id);
	    # my $name =  hpLysis_subset::get_stringOf_enum__e_kt_correlationFunction_t_scalar($metric_id);  #! where latter fucniton is deinfed in our "kt_metric_aux.h".
	    #my $name =  hpLysis_subset::get_stringOf_enum__e_kt_correlationFunction_t_scalar($metric_id);  #! where latter fucniton is deinfed in our "kt_metric_aux.h".
	    if($name eq "") {
		printf(STDERR "!!\t ivnegigate this case, given enum=\"$metric_id\", at %s:%d\n", __FILE__, __LINE__);
	    }
	    my ($group_id, $name_human) = ($name =~ /groupOf_(.+?)_(.+)$/i);
	    my $isOf_interest = 0;
	    if( !defined($enum_metric_groupId) ||  ($enum_metric_groupId eq "") ) {
		$isOf_interest = 1;
	    } else {
		$isOf_interest = (lc($group_id) eq lc($enum_metric_groupId)); #! ie, to aovid case-sensitivty from 'causing a no-match'.
	    }
	    if($isOf_interest) {		
		$isOf_interest = (lc($name_human) eq lc($enum_metric)); #! ie, to aovid case-sensitivty from 'causing a no-match'.
		if($isOf_interest == 0) { #! then we try replacing the '_' with 'spaces'
		    my ($string) = ($name_human =~ s/_/ /g);
		    $isOf_interest = (lc($string) eq lc($enum_metric)); #! ie, to aovid case-sensitivty from 'causing a no-match'.
		    if($isOf_interest == 0) { #! then we try replacing the '_' with '' (ie, 'consenses' the string):
			my ($string) = ($name_human =~ s/_//g);
			$isOf_interest = (lc($string) eq lc($enum_metric)); #! ie, to aovid case-sensitivty from 'causing a no-match'.
		    }
		}
	    }
	    if($isOf_interest) {		
		$enum_metric_foundLocally = $metric_id; #! ie, the 'local' metric-id.
	    }
	    push(@arrOf_metrics, $name); #! where latter is used in our debugigng-appraoch, ie, to given uses 'helpgin clues' wr.t latter.
	}   
	if(!defined($enum_metric_foundLocally)) {
	    printf(STDERR "!!\t(API::notSupportedInputEnum)\t metric=\"$enum_metric\" (in opt-group=\"$enum_metric_groupId\"): Was Not able to find any sim-metrics matching your API-call. To simplify debugging: you reuqested enum_metric=\"$enum_metric\", useInternalEnum=" . $useInternalEnum . " enum_metric_groupId=" . $enum_metric_groupId . ", at %s:%d\n", __FILE__, __LINE__); # and where supported enum-names are" . join("\t", @arrOf_metrics));
	    #croak("!!\t ivenistgte"); ## FIXME: remvoe
	    return 0;
	}	
	$enum_metric = $enum_metric_foundLocally; #! ie, the metric which we have 'found' using ª[bov]€ tieriaotn-appraoch
    } else { #! then we vlidate that the metirc is set:
	if($enum_metric !~ /^\d+$/) {
	    my $useInternalEnum_local = 1; #! ie, as the enum_di sohudl at this exect-poitn be an 'internal metric-id'.
	    my @arrOf_metrics = hpLysis_metrics__static__getSetOf__simMetrics($useInternalEnum_local);
	    my $isFound = 0;
	    my $localFound_enum = undef;
	    for(my $i = 0; $i < scalar(@arrOf_metrics); $i++) {
		if($arrOf_metrics[$i] eq $enum_metric) {
		    $isFound = 1;
		    $localFound_enum = $i; #! ie, as the idnex is asusemd to reflect the C/C++ enum
		    # printf("(found-metric)\t [$i]=\"%s\", at %s:%d\n", $enum_metric, __FILE__, __LINE__);
		}
	    }	
	    if($isFound == 0) {
		printf(STDERR "!!\t(API::notSupportedInputEnum)\t Was Not able to find any sim-metrics matching your API-call. To simplify debugging: you reuqested enum_metric=" . $enum_metric . " useInternalEnum="  . $useInternalEnum .  " enum_metric_groupId=", $enum_metric_groupId, " and where supported enum-names are ", join(",", @arrOf_metrics));
		return 0;
	    }
	    #! Update the reuslt with what we have foudn in [ªbove]:
	    $enum_metric = $localFound_enum;
	} #! else we asusem the enum-id is an 'itneranl' enum
    }
    
    #!
    #! @reutrn the metrics:
    return ($enum_metric, $enum_preStep);
    #return ($enum_metric, $enum_metric_groupId);
}

=head
   @brief configures the "hpLysisVisu_server.js" wrt. the simlairty-metic (and 'poitn of applciaiton) (oesketh, 06. arp. 2017)
   @param <enum_metric> is the simliarty-metric which we are to comptue for;
   @param <enum_preStep> is the simliarty-metric-pres-tep which is to be applied (Before applciaotn/use of the "simMetricPre_metricId" simliarty-emtric pre-step); 
   @param <isFor_preStep> whivh if set to false impleis that the simarty-emtric configruations is to be used 'inside the clsuteirng-operaiotn-phase'.
   @param <useInternalEnum> which if set to true implies that we use the ipnut-aprameters 'as-is': otherwise we use/apply a regualr expresison to idneitfy the correct 'internal enum' to apply.
   @param <useInternalEnum_notGroupId> which may be set inc onctuions with "useInternalEnum == false" to restrict/lmite the searhc for a 'matching' simlarity-metirc-enum.
   @return reutnr the "s_kt_correlationMetric" object
   @remarks for a list of metic-names which may be applied
   @remarks the set of uspported matrics are: 
=cut
sub hpLysis_metrics__get__simObject {
    my ($metric, $preStep, $useInternalEnum, $enum_metric_groupId) = @_;
    my ($enum_metric, $enum_metric_preStep) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($metric, $preStep, $useInternalEnum, $enum_metric_groupId);    
    my $obj_metric = hpLysis_subsetc::new_s_kt_correlationMetric_t();
    my $isTo_useMINE = 0;
    if(defined($metric)) {
	hpLysis_subset::init__s_kt_correlationMetric_t($obj_metric, $enum_metric, $enum_metric_preStep);
	$isTo_useMINE = hpLysis_subset::isTo_use_MINE__e_kt_correlationFunction($enum_metric);
    } else {
	hpLysis_subset::setTo_empty__s_kt_correlationMetric_t($obj_metric); #! ie, then set the emtric to empty
    }
    return ($obj_metric, $isTo_useMINE);
}
1;
