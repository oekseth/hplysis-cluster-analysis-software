#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 

=head
    @file "tut_clust_1_kMeans.pl"
    @brief cluster useing k-means, demsotnring a minaml exmaple
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
    @remarks we comptue k-emans without bothering upon simalrity-emtic, ie, where latter 'approach' (sdadly) is the common/establiehd appraoch for compiarng algorithsm (And data-setS) usign k-means-algorithm
=cut
{ #! Clustering::k-means: 
    my $alg = "k-means"; my $ncluster = 4;
    my $matrix = [ 
	[0.000000, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.072906],
	[0.321928, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.321928],
	[0.072906, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.000000]
	];
    my $arrOf_rows = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    my $arrOf_cols  = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    
    my ($mat_hpLysis_result, $result_clusterObject) = Algorithm::hpLysis::advancedAPI::compute__cluster($matrix, undef, $alg, $ncluster);
    #! Get a perl-verison of [ªbove]
    my $local_clusterObj = Algorithm::hpLysis_internal::C_to_perl_clusterResult($result_clusterObject);
    use Data::Dumper;
    print "\n\nDumper: " . Dumper $local_clusterObj;
}
