#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 

=head
    @file "tut_ccm_3_matrixBased_similarity_VS_hypothesis_directCall.pl"
    @brief demosntrates how a 'one-liner' may be used to evakuate hypotehsis (ie, use Cluster-Comparison-Matrix (CCM)) bweetween two sets of hypotehsis.
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
=cut

{ #! CCM, matrix-based: Demosntrate the 'fetching' of diffneret cluster-algorithm-enums, using differnet 'ways to speify the clsuter-algorithms':
    #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
    #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
    #! Specify the relationshisp to investigate:
    my $matrix_eval = [ #! where latter describe the distance between each vertex:
			#! Group: 1:
			[1, 1, 2, 3, 3],
			[1, 1, 2, 3, 3],
			#! Group: 2:
			[9, 9, 2, 2, 2],
			[9, 9, 2, 2, 2],
			[9, 9, 2, 2, 2]
	];
    my $hypo_1 = [[3, 3, 2, 2, 2]]; #! where latter describe the cluster-emmberships assicated to veritces in the second hypohesis.
    #! Use Silhouette to evluate simlairty in the [ªbove] hypohesis:
    my $enum_ccm_matrixBased = "Silhouette"; 
    #! The call:
    my $matrix = Algorithm::hpLysis::advancedAPI::compute__CCM_matrixBased($enum_ccm_matrixBased, $matrix_eval, $hypo_1); #! ie, comptue the CCM.
    #! 
    #! Write out the matrix:
    for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
	my $row = $matrix->[$row_id];
	my $ncols = scalar(@{$row});
	for(my $col_id = 0; $col_id < $ncols; $col_id++) {
	    my $val = $row->[$col_id];
	    #my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
	    #my $head = $arrOf_rows->[$row_id]; my $col = $arrOf_rows->[$col_id]; #! where we in the latter 'makes use' of the proeprty asusmgin taht the simalrity-matrix consistes of "rows" x "rows", ie, which explains why we do Not invesitgte wrt. teh 'column-names'.
	    #printf("float:[$head][$col]='$val', at %s:%d\n", __FILE__, __LINE__);
	    printf("\tfloat:[$row_id][$col_id]='$val', at %s:%d\n", __FILE__, __LINE__);
	}
    }    	
}
