package Algorithm::hpLysis_internal_returnWrappers;
use strict;
# use DynaLoader;
use hpLysis_subset;
require Exporter;

=head
    @brief identifes a set of return-wrappers wrt. funciton-calls (oekseth, 06. arp. 2017).
=cut

sub get__s_kt_swig_retValObj_t__string {
    my @args = @_;
    my $function = shift(@args); #! ie, the first argument
    #printf("....\n");
    my $s_kt_swig_retValObj_t = hpLysis_subsetc::new_s_kt_swig_retValObj_t(); #! where latter usage implies our assumption that the 'class-to-fetch-value-from' is static/non-permtuable, ie, where the "s_kt_swig_retValObj_t" is used a s a'trafer-object'.
    #hpLysis_subset::get_stringOf_enum__e_kt_correlationFunction_t_scalar
    #printf(".... args=%s\n", join(",", @args));
    &{$function}(@args, $s_kt_swig_retValObj_t);
    my $name = hpLysis_subsetc::s_kt_swig_retValObj_t_name_get($s_kt_swig_retValObj_t);
    #printf("--------....\n");
    return $name;
}

1;
