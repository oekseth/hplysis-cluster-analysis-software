package Algorithm::hpLysis_internal;
use strict;
# use DynaLoader;
use hpLysis_subset;
use hpLysis_wrapper_matrix;
require Exporter;

=head
    @brief provide wrapper-objets to gleu the hpLysis-pm simplifed API-itnerface with our "hpLysis cluster-analsysis-sfotware". (oekseth, 06. arp. 2017).
    @remarks the lgocis found int his package is indend to simpliyf the use of the differnet C/C++ data-structrecd foudn in oru "hpLysis cluster-analsysis-sfotware". By mdofiing the lgocis 'in this' to  fiferent file, we hope the users will find it easier to grfaphs the API-logics of our "hpLysis.pm"".
=cut




=head
    @brief converts a 'high-level' matrix into the hpLysis "s_kt_matrix_t" C/C++ matrix-object (oekseth, 06. mar. 2017).
    @param <matrix> a Perl-formattted matrix: fi values are set to "" or undef the values are 'masked' (ie, hidden from copmtuations). 
    @param <> 
    @param <> 
   @return an internal matrix based on a a 'language-specific' matrix.
   @remarks 
   -- howto: for an example wrt. different use-case-apttersn pelase see our associated tutoirals: if latter does Not help, then we suggest contacting senior developer [oekseth@gmail.com]
   -- application: interfaces the hpLysis high-performance cluster-analsysis-library
   -- sampleInput: matrix = [[12,7], [4 ,5], [3 ,8]]
=cut
# sub static_hpLysis__insertMatrix__intoInternal {
#     my ($matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights) = @_;
#     my $mat_hpLysis = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights);
#     #if(0 == data_is_valid_matrix($matrix)) {return undef;}
#     # #!
#     # #! Find the min-max-diemsions:
#     # my $max_row_id = scalar(@{$matrix}) + 1;     my $max_col_id = 0;
#     # my $nrows = scalar(@{$matrix});
#     # for(my $row_id = 0; $row_id < $nrows; $row_id++) {
#     # 	my $cnt_cols = scalar(@{$matrix->[$row_id]});
#     # 	if($cnt_cols > $max_col_id) {$max_col_id = $cnt_cols;}	    
#     # }
#     # $max_col_id++; #! ie, to icnrement 'pas't the last idnex.
#     #!
#     #! @return
#     return $mat_hpLysis;
# }


# sub static_hpLysis__getMatrix__fromInternal {

# }


#! @return a matrix loaded from a file (oekseht, 06. arp. 2017). 
#! @remarks to simplify (nad ease) maitnace we do Not expect thei fucntion to be called form outside this file.
sub __internal__getFromFile__perlFormat__matrix {
    my($file_name_in) = @_;
    open(FILE_IN, "<$file_name_in") or die("Unable to open the input-file $file_name_in\n");
    if(!(-f $file_name_in)) {
	printf(STDERR "!!\t Unable to find your input-file=\"%s\", ie, please investigate. OBservaiotn at %s:%d\n", $file_name_in, __FILE__, __LINE__);
	return undef;
    }
    # if(!defined($file_name_result_rel) || !length($file_name_result_rel)) {
    # 	$file_name_result_rel = "tmp.tsv";
    # 	printf("generates result in file=\"%s\", at %s:%d\n", $file_name_result_rel, __FILE__, __LINE__);
    # }
    # open(FILE_OUT_rel, ">$file_name_result_rel") or die("Unable to open the result<-file \"$file_name_result_rel\"\n");
    my $cnt_rows = 0;
    my @columnsOf_interst;
    my @resultMatrix = ();
    my @arrOf_cols = ();
    my @arrOf_rows = ();
    my $row_pos = 0;
    while (my $line = <FILE_IN>) {
	chomp($line); #! ie, remvoe the trail-newline.
	# if($cnt_rows > 100) {last;} # TODO: remove.
	my @cols = split("\t", $line);
	if($cnt_rows != 0) {
	    #printf(STDERR "(rows) line=\"$line\"\n");
	    if(scalar(@cols) && length($cols[0])) {
		#printf(STDERR "(rows) line=\"$line\"\n");
		#if($globalConfig__allRowsOf_interest || __isOf_interest__rowHeader($cols[0])) 
		{	
		    my @cols_updated = @cols;
		    my $globalConfig__allRowsOf_interest = 1;
		    if($globalConfig__allRowsOf_interest == 1) { #! then we evlauate wrt. the columsn:
			@cols_updated = ($cols[0]);
			for(my $i = 1; $i < scalar(@cols); $i++) {
			    #if($columnsOf_interst[$i]) 
			    {
				push(@cols_updated, $cols[$i]);
			    }
			}
		    }
		    #printf("--- %s, at %s:%d\n", join("\t", @cols_updated), __FILE__, __LINE__);
		    #printf(FILE_OUT_rel "%s\n", join("\t", @cols_updated));
		    push(@arrOf_rows, $cols_updated[0]); #! ie, hte row-header.
		    push(@resultMatrix, []);
		    #! ----------------
		    for(my $i = 1; $i < scalar(@cols_updated); $i++) {
			#! Print out the triplet:
			my $score = $cols_updated[$i];
			if( ($score ne "-") && length($score) && ($score !~ /0\.000/g) && ($score != 0) ) {
			    my $tail = $arrOf_cols[$i];
			    if(!length($tail)) {
				printf(STDERR "tail Not set, given arrOf_cols=\"@arrOf_cols\"\n");
				croak("!!\t investigate");
			    }

			    my $add = 1;
			    # if(defined($config)) {
			    # 	if(defined($config->{minVal})) {
			    # 	    if($score < $config->{minVal}) {$add = 0;}  #! ie, a simple filter
			    # 	}
			    # }
			    if($add == 0) {
				$score = "";
				#printf(FILE_OUT_rel "%s\t%s\t%f\n", $cols_updated[0], $tail, $score);
			    }
			    push(@{$resultMatrix[$row_pos]}, $score); 
			    #printf("%s\t%s\t%f\n", $cols_updated[0], $arrOf_cols[$i], $score);
			}
			#printf("%s\t%s\t%f\n", $cols_updated[0], $arrOf_cols[$i], $score);
		    }
		    #! ----------------
		    $row_pos++;
		}
	    }
	} else {
	    my $tmp = "";
	    #($tmp, $line) = ($line =~ /^(.+ )(.+)$/);
	    # printf(STDERR "line=\"$line\"\n");
	    #printf("tmp=\"$tmp\"\n");
	    #($line, $tmp) = ($line =~ /^(.*# )?(.+)$/);
	    #($line, $tmp) = ($line =~ /^(\!\#)?(.+)$/);
	    my @cols = split("\t", $line);
	    if(scalar(@cols) && length($cols[0])) {	    
		if($cols[0] =~/^\!#(.+)$/) {
		    $cols[0] = $1;
		}
		# push(@columnsOf_interst, 1);
		# for(my $i = 1; $i < scalar(@cols); $i++) {
		#     # if($globalConfig__allRowsOf_interest == 0) { #! then we evlauate wrt. the columsn:
		#     # 	my $status = __isOf_interest__rowHeader($cols[$i]);
		#     # 	push(@columnsOf_interst, $status);
		#     # } else {
		#     push(@columnsOf_interst, 1); #! ie, as we then 'for simplicty' asusmes that all column-headers 'are of interest'.
		# #}
		# }
		@arrOf_cols = @cols;
		#printf(STDERR "arrOf_cols=\"@arrOf_cols\"\n");
		# #printf("--- %s, at %s:%d\n", join("\t", @cols), __FILE__, __LINE__);
		# if($globaConfig__headerTag__remove) {
		#     printf(FILE_OUT_rel "%s\n", join("\t", @cols));
		# } else {
		#     printf(FILE_OUT_rel "#! %s\n", join("\t", @cols));
		# }
		# if($globaConfig__isTo_transpose) {
		#     push(@matrix, \@cols);
		# }
	    } else {
		if(length($line)) {
		    printf(STDERR "!!\t inveigtgate case for line=\"$line\"\n");
		}
	    }
	}
	if(scalar(@arrOf_cols) > 1) {
	    $cnt_rows++;
	}
    }
    close(FILE_IN);
    #close(FILE_OUT_rel);    
    return (\@resultMatrix, \@arrOf_rows, \@arrOf_cols);
}

#! @return a matrix loaded from a file (oekseht, 06. arp. 2017). 
#! @remarks to simplify (nad ease) maitnace we do Not expect thei fucntion to be called form outside this file.
#! 
#! Apply logics: read from file, and Convert to the "s_kt_matrix_t" data-format: 
sub __internal__getFromFile__perlFormat__matrix__intoInternalObject {
    my($file_name_in) = @_;
    if(!(-f $file_name_in)) {
	printf(STDERR "!!\t Unable to find your input-file=\"%s\", ie, please investigate. OBservaiotn at %s:%d\n", $file_name_in, __FILE__, __LINE__);
	return undef;
    }
    my($file_name_in) = @_;
    #! 
    #! Apply logics: 
    my ($matrix, $arrOf_rows, $arrOf_cols) = Algorithm::hpLysis_internal::__internal__getFromFile__perlFormat__matrix($file_name_in);
    #! 
    #! Convert to the "s_kt_matrix_t" data-format: 
    my $arrOf_colWeights = undef;
    my $mat_hpLysis = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights);
    #my $mat_hpLysis = Algorithm::hpLysis_internal::static_hpLysis__insertMatrix__intoInternal($matrix, $arrOf_rows, $arrOf_cols);
    return $mat_hpLysis;
}
    
#! @return an 'internal version' fot eh input-data: either return 'as-is' (if the object is arleayd an itenral object) or a 'converted-to-C' data-object (if the objec tis a PErl-based memory-object).
sub Perl_to_C_generic {
    my($matrix_input_1, $data_isSparse) = @_;
    if(!defined($data_isSparse)) {$data_isSparse = 0;} #! ie, our deufalt aasumption.
    my $ref = ref($matrix_input_1);
    if($ref eq "hpLysis_subset::s_kt_list_1d_pairFloat_t") {    
	printf("obj-is-internal, at %s:%d\n", __FILE__, __LINE__);
	return $matrix_input_1; #! ie, as we then assume that the object is 'arelady' an internal data-object.
    } elsif($ref eq "hpLysis_subset::s_kt_matrix_t") { #! then we use an API-call simliart to abvoe, ie, where difference conserns our data-input (and funciotn-name):
	printf("obj-is-internal, at %s:%d\n", __FILE__, __LINE__);
	return $matrix_input_1; #! ie, as we then assume that the object is 'arelady' an internal data-object.
    }
    #! Convert:
    my $data_1 = undef; if($data_isSparse == 1) {$data_1 = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat($matrix_input_1);} else {$data_1 = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix_input_1);}
    #! @return:
    return $data_1;
}

#! De-allcoate an itnernal C-object.
sub generic__freeMem {
    my ($data) = @_;
    if(defined($data)) {
	my $ref = ref($data);
	#!
	#!
	if($ref eq "hpLysis_subset::s_kt_list_1d_pairFloat_t") {    
	    hpLysis_subset::free__s_kt_list_1d_pairFloat_t($data);	    
	} elsif($ref eq "hpLysis_subset::s_kt_matrix_t") { 
	    hpLysis_subset::free__s_kt_matrix($data);	    
	} elsif($ref eq "hpLysis_subset::s_kt_clusterResult_condensed_t") { 
	    hpLysis_subset::free__s_kt_clusterResult_condensed_t($data);
	} else {
	    croak("!!\t add suport for de-allocting \"$ref\"");
	}
    }
}

#! TRanslate a "s_kt_clusterResult_condensed_t" into a Perl-object.
sub C_to_perl_clusterResult {
    my($obj_internal) = @_;
    #! Note: [below] is expected to conver/reflect our "hpLysisVisu_api__initClusterResult(..)" ("hpLysisVisu_server.js").
    my $result_obj = {
	arr_names_clustering => [], 
	arrOf_cluster_triplets => [], #! ie, the cluster-resutls: 'exports't eh clsuter-results using a feature-matrix of [head, tail, score] triplets.
	arr_vertexTo_centroid => [],
	arr_hcaResult_distanceFromRoot => [],
	arr_clusterSets_child_countOfScoreSum => [],
	arr_clusterSets_parent_countOfScoreSum => [],
    };
    #!
    #! Apply lgoics to fetch the result:
    {
	#hpLysis_subset::s_kt_list_1d_string_t
	my $list = Algorithm::hpLysis_wrapper_1d::perl_to_C_string();
	hpLysis_subset::s_kt_clusterResult_condensed_t__getObject__arr_names_clustering($obj_internal, $list);
	my @arr = Algorithm::hpLysis_wrapper_1d::C_to_perl_string($list);
	$result_obj->{arr_names_clustering}   = \@arr;
    }
    {
	my $list = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat();
	hpLysis_subset::s_kt_clusterResult_condensed_t__getObject__arrOf_clusterRelationships($obj_internal, $list);
	my @arr = Algorithm::hpLysis_wrapper_1d::C_to_perl_pairFloat($list);
	$result_obj->{arrOf_cluster_triplets} = \@arr;
    }
    {
	my $list = Algorithm::hpLysis_wrapper_1d::perl_to_C_uint();
	hpLysis_subset::s_kt_clusterResult_condensed_t__getObject__arr_vertexTo_centroid($obj_internal, $list);
	my @arr =  Algorithm::hpLysis_wrapper_1d::C_to_perl_uint($list);
	$result_obj->{arr_vertexTo_centroid}  = \@arr;
    }
    {
	my $list =  Algorithm::hpLysis_wrapper_1d::perl_to_C_float();
	#my $list = 
	hpLysis_subset::s_kt_clusterResult_condensed_t__getObject__arr_hcaResult_distanceFromRoot($obj_internal, $list);
	my @arr = Algorithm::hpLysis_wrapper_1d::C_to_perl_float($list);
	$result_obj->{arr_hcaResult_distanceFromRoot}         = \@arr;
    }
    {
	my $list =  Algorithm::hpLysis_wrapper_1d::perl_to_C_float();
	hpLysis_subset::s_kt_clusterResult_condensed_t__getObject__arr_clusterSets_child_countOfScoreSum($obj_internal, $list);
	my @arr = Algorithm::hpLysis_wrapper_1d::C_to_perl_float($list);
	$result_obj->{arr_clusterSets_child_countOfScoreSum}  = \@arr;
	#! 
	#! Note: in [below] we asusme taht 'only vlaues above 0' are of itnerest, ie, hence our 'none-use' of the default curreT_pos-apprapoch.
	my $list_ref = $result_obj->{arr_clusterSets_child_countOfScoreSum};
	my $list_size = scalar(@{$list_ref});
	my $cnt_ele = 0; 
	for(my $i = 0; $i < $list_size; $i++) {
	    my $val = $list_ref->[$i];
	    if(defined($val) && ($val != 0) ) {$cnt_ele = $i + 1; } #! ie, the biggest index:
	}  
	#! Then pop the elments which are outside the range:
	my $cnt_pop = $list_size - $cnt_ele; if($cnt_pop > 0) { for(my $i = 0; $i < $cnt_pop; $i++) { pop(@{$list_ref});} } #! ie, remove all óverlfowing'elemtns.
    }
    {
	my $list =  Algorithm::hpLysis_wrapper_1d::perl_to_C_float();
	#my $list = 
	hpLysis_subset::s_kt_clusterResult_condensed_t__getObject__arr_clusterSets_parent_countOfScoreSum($obj_internal, $list);
	my @arr = Algorithm::hpLysis_wrapper_1d::C_to_perl_float($list);
	$result_obj->{arr_clusterSets_parent_countOfScoreSum} = \@arr;
	#! 
	#! Note: in [below] we asusme taht 'only vlaues above 0' are of itnerest, ie, hence our 'none-use' of the default curreT_pos-apprapoch.
	my $list_ref = $result_obj->{arr_clusterSets_parent_countOfScoreSum};
	my $list_size = scalar(@{$list_ref});
	my $cnt_ele = 0; 
	for(my $i = 0; $i < $list_size; $i++) {
	    my $val = $list_ref->[$i];
	    if(defined($val) && ($val != 0) ) {$cnt_ele = $i + 1; } #! ie, the biggest index:
	}  
	#! Then pop the elments which are outside the range:
	my $cnt_pop = $list_size - $cnt_ele; if($cnt_pop > 0) { for(my $i = 0; $i < $cnt_pop; $i++) { pop(@{$list_ref});} } #! ie, remove all óverlfowing'elemtns.
    }
    #$result_obj->{} = Algorithm::hpLysis_wrapper_1d::C_to_perl_float();
    #!
    #! @return the result: 
    return $result_obj;
}

1;
