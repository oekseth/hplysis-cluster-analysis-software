#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 
#use hpLysis_subset;

=head
   @brief present an "Hello World" app where we demonstrate how to use the 'generic' components of the hpLysis cluster-analysis API.
   @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
   @remarks disclaimer: this use-case is constructed to demonstrate the 'complete application' of generic logic found in our "hpLysis cluster-analysis software". Therefore, this "hello world" use-case-example may exhibit a complexity which may 'see overwhelming'. If latter is the case, we suggest first starting (your learning of hpLysis) with the nine examplies (listed when writing) "ls tut_hp_api_fileInputTight_*.c". 
   @remarks general(API):
   @remarks general(undlerying routines which are used iun the hpLysis C/C++ program): the logics we use (in this example) are listed in:
   -- <data-types> "types.h" and "def_memAlloc.h": the data-types, such as "uint" and "t_float"; the "t_float" data-type is either "float" or "double", and is used to allow swithcing between different 'needs' for resutl-accuracy (where "t_float = float" is the defualt asisgnment, an assignmetn based upon the needs in the life-science community). 
   -- "hp_api_fileInputTight.h": for cluster-analysis;
   -- "kt_matrix.h": the data-container "s_kt_matrix_t", used for both input, temporary storage, and export; 
   -- "kt_list_1d.h": for 1d-lists, such as "s_kt_list_1d_uint_t" to hold a list of positive integers (ie, "unsigned int").
   @remarks general(use-case): hpLysis is designed to efficiently identify patterns in data. Therefore, we believe an introduction-example should cover these aspects: to relate similarity to predicted clusters through application of well-defined metrics. In this "Hello world" the latter is exemplified: we describe how to: step(a) identify the similarity of the rows in a wild-card input-file (eg, the "IRIS" data-set); step(b) separately hypothesized partitions through application of two different cluster-algorithms; step(3) evaluate accuracy and similarity of cluster-results VS computed similarity-matrix. For the latter "step(3)" we use hpLysis support for "cluster Comparison Metrics (CCMs)": to first separately assess the consistency of each cluster-prediction, and thereafter to compare 'directly' the clsuter-prediction-results using multiple "gold x gold" CCMs. 
=cut



# ok:FIXME[SWIG::code::ANSI_c]: ... update our new tut ... usign our [ªbov€] sample-data-input ... figure out why clsuter(1) is Not fodun int eh lsit of clsutere-result-memberships ... works!
# ok:FIXME[SWIG::code::ANSI_c]: ... update our new tut ... SOM ... why clsuter-attribute-set is empty ... whiel Not alwyas ... may be due to teh 'random sledionct' ... ie, that the cluster-set is Not alwayus optmail
# ok:FIXME[SWIG::code::ANSI_c]: ... update our new tut ... HCA ... why 'naems' are Not deinfed wrt. our HCA-cluster-reuslt .. updated our "hpLysis_api.c" ... resolved!
# ok:FIXME[SWIG::code::ANSI_c]: ... update our new tut ... HCA ... update our C-code ... 'store' distances for 'iamginary vertices' ... udpating our "constructAdjcencencyMatrix_fromNodeTable__kt_clusterAlg_hca(..)" ("kt_clusterAlg_hca_resultObject.c") ... seems correct
# ok:FIXME[SWIG::code::ANSI_c]: ... update our new tut ... HCA ... figure out why "arr_clusterSets_parent_countOfScoreSum" is Not set: ... updated iour insertion-routines.
# ok:FIXME[SWIG::code]: ... get k-means-clustering + result-fetching 'to work'. .. tut_clust_1_kMeans.pl
# ok:FIXME[SWIG::code]: ... get SOM-clustering + result-fetching 'to work'. ... tut_clust_2_SOM_notConverged.pl
# ok:FIXME[SWIG::code]: ... get hca-clustering + result-fetching 'to work'. ... tut_clust_3_HCA.pl



croak("..");
=head
    @file ""
    @brief cluster useing k-means, demsotnring a minaml exmaple
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
    @remarks 
=cut
{ #! A permtuation of [abvoe] where we use the "hpLysis.pm" PErl-object:
    ;
}
#!
#!
#!
{ #! Clustering::SOM: 
    #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
    ; 
}
{ #! A permtuation of [abvoe] where we use the "hpLysis.pm" PErl-object:
    ;
}
#!
#!
#!
{ #! Clustering::HCA: 
    #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
    ; 
}
{ #! A permtuation of [abvoe] where we use the "hpLysis.pm" PErl-object:
    ;
}

croak("..");

#!
#!
#!
# { #! 
#     #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
#     my @arrOf_metrics = Algorithm::hpLysis_metrics::($useInternalEnum);
#     printf("Metrics in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
# }
# { #! 
#     #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
#     my @arrOf_metrics = Algorithm::hpLysis_metrics::($useInternalEnum);
#     printf("Metrics in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
# }

# FIXME[code]: ... manuyally inspect our "hpLysis_metrics.pm ... udpating latter wrt. our new-added/configured/chaned API-functions (wrt. reutrn-values, ie, reoslving 'earlier' rpåoblems).
# FIXME[code]: ... write nroamlziaiton-Steps for eahc of the enums wrt. sim-metircs, cluster-algs, CCM-matrix-baesd, CCM-set-based (ie, an esntive list of name-permtautiosn) ... and then get latter working ... 
# FIXME[code]: ... 

=head
    @file ""
    @brief 
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
    @remarks In this 'example we focus on:
    -- 
    -- 
    -- 
    @remarks 
=cut



=head


// -----------------
assert(false); // FIXME[Perl::interface::"hpLysis.pm"]: ...  update our "serverSide_hpLysis.cgi" wrt. ª[bvoe] ... 
#! -------------------------------------------
assert(false); // FIXME[Perl::interface::tut:"tut_cluster_"]: ....  data-filtering
#! -------------------------------------------
# FIXME[SWIG::compilation]: ...  get our udpated code-chunk to compile ... 
# FIXME[SWIG::code::tut]: ... use-case ... build a profiel of the text for a research-artilce ... using a sparse data-set ... seperately for each paragraph count frequency of word ... and then seprately for each aritlce (ie, comparing muliple artilces) and then ....??.... ... whre latter use-case is sinpreired by the observaiton that different åaragraphs have differnet 'sigicant-occuring' words, both wrt. 'high-frequncy-words' and wrt. unqiue/rare words .... in the latter we use/laod a stop-word-list (eg, from the ....??.. data-source), and then ....??.... Novelty: this/latter use-case is included in oru ... artilce to demosntrate/examplfiy features of .....??... # <-- TODO: try scaffolding an artilce hwere the latter sue-case is used/evlauted ... 
# FIXME[SWIG::code::tut]: ... "tut_helloWorld.pl" ... complete latter ... writing/fiding an itneresting use-case, where clsuter-sepraiotn (a) is well-deifned and (b) describes/is an intersting and nvoel use-case.
# FIXME[SWIG::code::ANSI_c]: ... update our new tut ... HCA ... 
# FIXME[SWIG::code::ANSI_c]: ... update our new tut ... HCA ... 
# FIXME[SWIG::code::ANSI_c]: ... update our new tut ... 
# FIXME[SWIG::code::ANSI_c]: ... update our new tut ... 
# FIXME[SWIG::code]: ... 
# FIXME[SWIG::code]: ... 'factor out' tut-exampels into seperate fiels
# FIXME[SWIG::code]: ... 
# FIXME[SWIG::code]: 
# FIXME[SWIG::Perl]: ... add support for ... accessing the "result_clusterObject" .... wrt. the calls/results 'needed' by our "serverSide_hpLysis.cgi"
# FIXME[SWIG::compilation]: ... 
# FIXME[SWIG::code]: ... 
# FIXME[SWIG::code]: 
# FIXME[SWIG::code::ANSI_c::tut::""]: ... 
# FIXME[SWIG::code::ANSI_c]: ...
#! -------------------------------------------
# FIXME[SWIG::code::tut::new::""]: 
# FIXME[SWIG::code::tut::new::""]: 
# FIXME[SWIG::code::tut::new::""]: 
# FIXME[SWIG::code::tut::new::""]: 
# FIXME[SWIG::code::tut::new::""]: 
# FIXME[SWIG::code::tut::new::""]: 
# FIXME[SWIG::code::tut::new::""]:
// -----------------
assert(false); // FIXME[Perl::interface::tut:"tut_cluster_"]: .... 
assert(false); // FIXME[Perl::interface::tut:"tut_cluster_"]: .... 
assert(false); // FIXME[Perl::interface::tut:"tut_cluster_"]: .... 
assert(false); // FIXME[Perl::interface::tut:"tut_cluster_"]: .... 
assert(false); // FIXME[Perl::interface::tut:"tut_cluster_"]: .... 
// -----------------
assert(false); // FIXME[Perl::interface::tut:""]: new-tut where we ... sim-matris .... datsa-struct
assert(false); // FIXME[Perl::interface::"hpLysis.pm"]: ...  update our "serverSide_hpLysis.cgi" wrt. ª[bvoe] ... 
assert(false); // FIXME[Perl::interface::tut:""]: new-tut where we ... sim-matrix from sample-file
assert(false); // FIXME[Perl::interface::"hpLysis.pm"]: ...  update our "serverSide_hpLysis.cgi" wrt. ª[bvoe] ... 
assert(false); // FIXME[Perl::interface::tut:""]: new-tut where we ... sim-matris .... datsa-struct ... value-filtering
assert(false); // FIXME[Perl::interface::tut:""]: new-tut where we ... clustering ... hca 
assert(false); // FIXME[Perl::interface::"hpLysis.pm"]: ...  update our "serverSide_hpLysis.cgi" wrt. ª[bvoe] ... 
assert(false); // FIXME[Perl::interface::tut:""]: new-tut where we ... clustering ... SOM ... write our both 'simliarty-matrix', 'topology-atttributes' and 'topology-matrix'
assert(false); // FIXME[Perl::interface::tut:""]: new-tut where we ... clustering ... k-means
assert(false); // FIXME[Perl::interface::"hpLysis.pm"]: ...  update our "serverSide_hpLysis.cgi" wrt. ª[bvoe] ... 
assert(false); // FIXME[Perl::interface::tut:""]: new-tut where we ... 
assert(false); // FIXME[Perl::interface::"hpLysis.pm"]: ...  update our "serverSide_hpLysis.cgi" wrt. ª[bvoe] ... 
// -----------------


=cut


# sub build_array {
#     my $arr = shift;
#     my $nitems = scalar(@{$arr});
#     my $a = ptrcreate("float",0, $nitems);
#     my $i = 0;
#     foreach my $item (@{$arr}) {
# 	ptrset($a,$item, $i);
# 	$i = $i + 1;
#     }
#     return $a
# }
# my @arrOf_values = [1, 3];
#my $internal_arr_ref = build_array(\@arrOf_values);
#my $tmp_array = ptrcreate("float");          # Create a float
#my $avg = kt_hpLysis::mean(scalar(@arrOf_values), $internal_arr_ref);
#my $avg = kt_hpLysis::mean(scalar(@arrOf_values), \@arrOf_values);
 # print $example::My_variable,"\n";
 # print example::fact(5),"\n";
 # print example::get_time(),"\n";
