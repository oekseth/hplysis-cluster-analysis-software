#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 

=head
    @file "tut_advanced_1_2dMatrix_insertAndGet.pl"
    @brief demosntrates lgoics for accessing the internal matrix-object, among othes handy when cosntrucitng 'pipes' of data (eg, when bulidng your won analaaysis-system) (oekseth, 06. arp. 2017).
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
=cut

{  #! First export the reuslt, then improt the 'same matrix', before comparign the two matix to our 'base':  
    my $name_resultFile = "tmp_matrix.tsv";
#! Comptue simalrity using a real-world data-input-dset:t eh data-set described the spotting-gread of guenien-pgics, and where deitals of the latter is foudn ion the hpLysis-docuemtantion.
    my $matrix = [ 
	[0.000000, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.072906],
	[0.321928, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.321928],
	[0.072906, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.000000]
	];
    my $arrOf_rows = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    my $arrOf_cols  = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    my $arrOf_colWeights = undef; #! ie, for simmplify assumes that we do Not have any data desicring particular wieghts asisted to columsn (or alterntivly that we asusme taht our simalrity-emtics are desciptive enough, ie, for which we may mulipey the column-weights 'directly' into the reusl-matrix.
    #!
    #! Load data inot our C-objecto
    my $mat_hpLysis_result = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix($matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights);
    #my $data_isSparse = 0;
    #!
    #! Export the matrix to aresult-file:
    Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_export($mat_hpLysis_result, $name_resultFile);
    #!
    #! Then import the matrix:
    my $mat_imported = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix_import($name_resultFile); #! ie, laod into memory the [ªbove] exproted reuslt-file.
    my ($nrows_imp, $ncols_imp) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_imported);
    my $isTo_deAllocate = 0;
    my ($matrix) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($mat_hpLysis_result, "", $isTo_deAllocate);
    my ($matrix_afterImported) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($mat_imported); #, "", $isTo_deAllocate);
    printf("Matrix[result-after-sim-metric::importadAfterExport] has dimenions=[%u, %u], at %s:%d\n", $nrows_imp, $ncols_imp, __FILE__, __LINE__);
    #! 
    #! Write out the matrix:
    for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
	my $row = $matrix->[$row_id];
	#printf("row=$row\n");
	my $ncols = scalar(@{$row});
	for(my $col_id = 0; $col_id < $ncols; $col_id++) {
	    my $val = $row->[$col_id];
	    #my $val_result = 0; #$matrix_afterImported->[$row_id]->[$col_id];
	    my $val_result = $matrix_afterImported->[$row_id][$col_id];
	    printf("float:[$row_id][$col_id]='$val' VS imported='$val_result', at %s:%d\n", __FILE__, __LINE__);
	}
	#hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	#$index++;
    }
}
