#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 


=head
    @file "tut_sim_1_sparseData_object.pl"
    @brie demosntrates how our "hpLysis.pm" class may be used as interface to compute simliarty between two sparse sets of (head, tail, socre) objects.
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
=cut

#!
#!
{ #! Comptue simalirty-emtrics using the hpLyssi-object-based interface:
    #! Construct a sample-space-input-datqa-set, ie, of (ead, tial,  score) triplets:
    my @arrOf_triplets = (
	[0, 1, 0.1],
	[0, 3, 0.1],
	[0, 3, 0.2],
	[0, 4, 0.1],
	[1, 1, 0.3],
	[1, 3, 0.1],
	[2, 3, 0.2],
	[3, 4, 0.3],
	);
    #!
    #! Intialise the object:
    my $data_isSparse = 1;
    my $obj_hp = Algorithm::hpLysis->new(\@arrOf_triplets, \@arrOf_triplets, $data_isSparse);
    #!
    #! Comptue teh simliarty:
    #! Note: the resutl-value in [”elow] is a perl-version of the itnernal data-object.

    my ($matrix, $arrOf_rows, $arrOf_cols) = $obj_hp->compute__similarity("Euclid"); #! ie, comptue using the Euclidian simliarty-metric
    #!
    #my $matrix = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($obj_matrix);
    #! 
    #! Write out the matrix:
    for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
	my $row = $matrix->[$row_id];
	my $ncols = scalar(@{$row});
	for(my $col_id = 0; $col_id < $ncols; $col_id++) {
	    my $val = $row->[$col_id];
	    #my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
	    printf("float:[$row_id][$col_id]='$val', at %s:%d\n", __FILE__, __LINE__);
	}
	#hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	#$index++;
    }    
}
