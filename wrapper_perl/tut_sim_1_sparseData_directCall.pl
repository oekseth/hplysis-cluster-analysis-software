#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 

=head
    @file "tut_sim_1_sparseData_directCall.pl"
    @brief demosntrates how a 'one-liner' may be used to compute simliarty between two sparse sets of (head, tail, socre) objects.
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
=cut


{ #! Comptue the simalirty between two data-sets:
    #! Construct the two sparse data-sets to compare:
    my @arrOf_triplets = (
	[0, 1, 0.1],
	[0, 3, 0.1],
	[0, 3, 0.2],
	[0, 4, 0.1],
	[1, 1, 0.3],
	[1, 3, 0.1],
	[2, 3, 0.2],
	[3, 4, 0.3],
	);
    my $metric = "Euclid";
    my $enum_preStep = ""; #! ie, as we asusem the metrics are to be used 'as-is', eg, where we are Not interested in using a Separman-appraoch wrt. 'rakning' (before computing simalrity-emtrics):
    my $useInternalEnum = 0; #! ie, as we use 'human-redalbe' enusm in [ªbove]:
    my $enum_metric_groupId = ""; #! ie, as we in this 'context' assuems that the emtic-idienties are exact enough for our use-case (where an 'oppostive' exmapole is see/examplfied in [below]).    
    my $data_isSparse = 1; #! ie, to avodi our data-inptus from beeing perceived as a matrix, ie, where we instead are itnerested in each row to be conveived as a (head, tail, score) pair.
    #! Apply logics, where result is stored in an 'internal' "s_kt_matrix_t" object (an object which users of the C/C++ veriosn of hpLysis is expected to be familiar with). 
    my $mat_hpLysis_result = Algorithm::hpLysis::advancedAPI::compute__similarity($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId, \@arrOf_triplets, \@arrOf_triplets);

    #!
    #! Fetch the result, iue, inserting into a Perl-matrix: 
    { #! Print out the matrix-dimenions:
	my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_hpLysis_result);
	printf("Matrix[result-after-sim-metric] has dimenions=[%u, %u], at %s:%d\n", $nrows, $ncols, __FILE__, __LINE__);
    }
    #! 
    { #! First export the reuslt, then improt the 'same matrix', before comparign the two matix to our 'base':
	my $name_resultFile = "tmp_matrix.tsv";
	Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_export($mat_hpLysis_result, $name_resultFile);
	#!
	#! Then import the matrix:
	my $mat_imported = Algorithm::hpLysis_wrapper_matrix::perl_to_C_matrix_import($name_resultFile); #! ie, laod into memory the [ªbove] exproted reuslt-file.
	my ($nrows_imp, $ncols_imp) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_imported);
	my $isTo_deAllocate = 0;
	my ($matrix) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($mat_hpLysis_result, "", $isTo_deAllocate);
	my ($matrix_afterImported) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($mat_imported); #, "", $isTo_deAllocate);
	printf("Matrix[result-after-sim-metric::importadAfterExport] has dimenions=[%u, %u], at %s:%d\n", $nrows_imp, $ncols_imp, __FILE__, __LINE__);
	#! 
	#! Write out the matrix:
	for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
	    my $row = $matrix->[$row_id];
	    #printf("row=$row\n");
	    my $ncols = scalar(@{$row});
	    for(my $col_id = 0; $col_id < $ncols; $col_id++) {
		my $val = $row->[$col_id];
		#my $val_result = 0; #$matrix_afterImported->[$row_id]->[$col_id];
		my $val_result = $matrix_afterImported->[$row_id][$col_id];
		printf("float:[$row_id][$col_id]='$val' VS imported='$val_result', at %s:%d\n", __FILE__, __LINE__);
	    }
	    #hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	    #$index++;
	}
    }
    #! Fetch the result, and de-allcoate the internal amtrix:
    my ($matrix) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($mat_hpLysis_result);
    #! 
    #! Write out the matrix:
    for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
	my $row = $matrix->[$row_id];
	my $ncols = scalar(@{$row});
	for(my $col_id = 0; $col_id < $ncols; $col_id++) {
	    my $val = $row->[$col_id];
	    #my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
	    printf("float:[$row_id][$col_id]='$val', at %s:%d\n", __FILE__, __LINE__);
	}
	#hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	#$index++;
    }
    #croak("..");    
}
