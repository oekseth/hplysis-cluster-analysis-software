#!/usr/bin/perl
use strict;
# use DynaLoader;
use hpLysis_subset;
use hpLysis_internal_returnWrappers;
#use hpLysis_internal; #! which provide wrapper-objets to gleu the hpLysis-pm simplifed API-itnerface with our "hpLysis cluster-analsysis-sfotware". (oekseth, 06. arp. 2017).
use hpLysis_metrics; #! provide logcis for 'fethcing' the correct enum when/if string-inptus are 'given'/provided (oekseth, 06. arp. 2017).

=head
    @file "tut_params_3_enums.pl"
    @brief examplfies funciton-rutines in our "Algorithm::hpLysis_metrics" (defined in our "hpLysis_metrics.pm") for fetching of configuraiton-enums for simlairty-metircs, clusteirng, and Cluster-Comparison Metrics (CCMs).  
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
    @remarks this tuorial provides an exhaustive itnroduciton to diffenret enum-cofngiruations which may be used when analyssing data-sets, eg, wrt. clsutierng and simlairties.
    @remarks In this 'example we focus on:
    -- Write out the approx. 70+ simalrity-emtircs which are the 'base' of the supported simliarty-emtrics (emtric which may be modifed using both pre-processing, weight-adjustment, maksing, weight-psot-adjustment, etc, where all of the latter are found in differnet resehar-aritlces); demonstrate the use of our "hpLysis_metrics__static__getSetOf__simMetrics(..)" function;
    -- APi, eg, list the set of metirc-names which may be used, both from Perl and from other wraping-languages, eg, wrt. in configuring our "hpLysisVisu_server.prototype.set__simMetric(..)" "hpLysisVisu_server.js" object (oekseth, 06. apr. 2017);
    -- synonyms: descirbe difnferet pemrtuatatiosn of enum-names, eg, wrt. "euclid", "kmeans", "manhatten", and "DBI".

=cut

{ #! WRite out the set of pre-step-simalirty-metrics spported by hpLysis: 
    #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
    my @arrOf_metrics = Algorithm::hpLysis_metrics::hpLysis_metrics__static__getSetOf__simMetrics__preSteps();
    printf("Pre-simliarty-metrics in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
}
{ #! Write out the approx. 70+ simalrity-emtircs which are the 'base' of the supported simliarty-emtrics (emtric which may be modifed using both pre-processing, weight-adjustment, maksing, weight-psot-adjustment, etc, where all of the latter are found in differnet resehar-aritlces).
    #! Note[API]: in this test-block we demonstrate the use of our "hpLysis_metrics__static__getSetOf__simMetrics(..)" function:
#Algorithm::hpLysis_metrics::
    { my $useInternalEnum = 0; #! ie, write out human-reable names of the simlairty-meitrcs.
     my @arrOf_metrics = Algorithm::hpLysis_metrics::hpLysis_metrics__static__getSetOf__simMetrics($useInternalEnum);
      if(1 == 2) {
	  printf("Base-Simliarty-emtics in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
      } else { #! then we porint only the firest 'item':
	  my $name = $arrOf_metrics[0];
	  printf("Base-Simliarty-emtics in hpLysis:\n%s\n", $name);
      }
    }
    { my $useInternalEnum = 1; #! ie, write out 'high-specific' internal sim-metric-enums:
      my @arrOf_metrics = Algorithm::hpLysis_metrics::hpLysis_metrics__static__getSetOf__simMetrics($useInternalEnum);
      if(1 == 2) {
	  printf("Base-Simliarty-emtics in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
      } else { #! then we porint only the firest 'item':
	  printf("Base-Simliarty-emtics in hpLysis:\n%s\n", $arrOf_metrics[0]);
      }
    }
}
{ #! Write out the cluster-algorithms:
    #! Note[API]: in this test-block we demonstrate the use of our "hpLysis_metrics__static__getSetOf__clusterAlgs(..)" function:
    my @arrOf_metrics = Algorithm::hpLysis_metrics::hpLysis_metrics__static__getSetOf__clusterAlgs();
    printf("Base-Cluster-algorithms in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
}
{ #! Write out the 'baisc' matrix-based cluster-comaprison-algorithms (CCMs):
    #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
    my @arrOf_metrics = Algorithm::hpLysis_metrics::hpLysis_metrics__static__getSetOf__ccm__matrixBased();
    printf("Base-matrix-based-CCMs in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
}
{ #! Write out the 'baisc' set-based cluster-comaprison-algorithms (CCMs):
    #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
    my @arrOf_metrics = Algorithm::hpLysis_metrics::hpLysis_metrics__static__getSetOf__ccm__setBased();
    printf("Base-set-based-CCMs in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
}
# { #! 
#     #! Note[API]: in this test-block we demonstrate the use of our "(..)" function:
#     my @arrOf_metrics = Algorithm::hpLysis_metrics::($useInternalEnum);
#     printf("Metrics in hpLysis:\n%s\n", join(", ", @arrOf_metrics));
# }
{ #! List the set of metirc-names which may be used, both from Perl and from other wraping-languages, eg, wrt. in configuring our "hpLysisVisu_server.prototype.set__simMetric(..)" "hpLysisVisu_server.js" object (oekseth, 06. apr. 2017):
    #! Note[API]: in this test-block we demonstrate the use of our "hpLysis_metrics__get__simMetric(..)" function:
    #!
    #! First evlaute/iterate using 'human-redable' simplcitc names:
    #! Note: in belwo we test/elvuate both wrt. case-isnesntivty (eg, wrt. "Euclid" VS "euclid", where latter are to return the same metric), wrt. mispsellged simlairty-emtrics (which are to return 'no values'
    foreach my $metric (
	#! Case-insentive example:
	"Euclid", "euclid", 
	#! Misspellings: [”elow] all refers to the same emtric:
	"citblock", "city-block", "manhatten", 
	#! Ambigous call: Perason (hwere the latter has mulipel common itnerpretiaotns):
	"pearson", 
	#! Not-found, ie, as latter is Not a vector-based simliarty-metric: 
	"rank-magnitude",
	) {
	my $useInternalEnum = 0; #! ie, as we use 'human-redalbe' enusm in [ªbove]:
	my $enum_preStep = ""; #! ie, as we asusem the metrics are to be used 'as-is', eg, where we are Not interested in using a Separman-appraoch wrt. 'rakning' (before computing simalrity-emtrics):
	my $enum_metric_groupId = ""; #! ie, as we in this 'context' assuems that the emtic-idienties are exact enough for our use-case (where an 'oppostive' exmapole is see/examplfied in [below]).
	my ($enum_metric, $enum_metric_groupId) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId);
	printf("sim-metric[$metric]=\"%s\", sim-metirc-pre-step[$enum_preStep]=\"%s\", at %s:%d\n", $enum_metric, $enum_metric_groupId, __FILE__, __LINE__);
    }
    #! ----------------------------------------------------------------------
    #!
    { #! Examplfiy (a) how the "enum_metric_groupId" attribute may be used to icnrease speify of/when fetching/speiifyign simalrity-emtirc-enums, and (b) the use of the "sim-metirc-pre-step-option":
	my $metric = "Euclid";
	foreach my $enum_metric_groupId (
	    "minkowski", 
	    #"squared"
	    ) {
	    my $useInternalEnum = 0; #! ie, as we use 'human-redalbe' enusm in [ªbove]:
	    my $enum_preStep = ""; #! ie, as we asusem the metrics are to be used 'as-is', eg, where we are Not interested in using a Separman-appraoch wrt. 'rakning' (before computing simalrity-emtrics):
	    my ($enum_metric, $enum_preStep) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId);	    
	    printf("sim-metric[$metric]=\"%s\", sim-metirc-pre-step[$enum_preStep]=\"%s\", at %s:%d\n", $enum_metric, $enum_preStep, __FILE__, __LINE__);
	    #!
	    { #! Examplify the use og the "enun_prestep: option:
		foreach my $enum_preStep ("", undef, "none", "rank", "binary") {
		    my $useInternalEnum = 0; #! ie, as we use 'human-redalbe' enusm in [ªbove]:
		    my ($enum_metric, $enum_preStep) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId);	    
		    if(!defined($enum_preStep)) {#$enum_preStep = "-";} #! ie, to avoiud our Perl-compiler from compling about an undefined-value in [”elow] 'out-print-funciotn':
			printf("sim-metric[$metric]=\"%s\", sim-metirc-pre-step[-]=\"%s\", at %s:%d\n", $enum_metric, $enum_preStep, __FILE__, __LINE__);	
		    } else {
			printf("sim-metric[$metric]=\"%s\", sim-metirc-pre-step[$enum_preStep]=\"%s\", at %s:%d\n", $enum_metric, $enum_preStep, __FILE__, __LINE__);	
		    }
		}
	    }
	}
    }
    #!
    #! Iterate using C/C++ enums, where latter approach (a) provides a higher 'tithness' wrt. the udnerlygin C/C++ implemetnation (eg,m simplifying changing Perlc-ode to C/C++ to avodi performace-lag wrt. high-elvle rpogrammign-lagnagues such as Perl), and (b) ensures that any future/new-added simlairty-emtrics (whcih may be motifciaotns of eg, Euclid and Pearson) does not replce 'your own'/actually-tested simliarty-metric(s).
    foreach my $metric (
	"e_kt_correlationFunction_groupOf_minkowski_euclid",
	"e_kt_correlationFunction_groupOf_minkowski_cityblock",
	#! Note:; in [”elow] observ ethe name-simalrity with above, ie, the improtance of using the abovedescribed "enum_metric_groupId" wrt. 'human-reable' name-specificiaotn of enums.
	"e_kt_correlationFunction_groupOf_squared_Euclid",
	"e_kt_correlationFunction_groupOf_combinations_averageOfChebyshevAndCityblock",
	#! Ambigous call: in [”elow] we examplify how one estalibehd simlairty-emtirc has a nubmer of interpretioant/connotaitosn (in reseharc-litterate and estalibehd applciaitons):
	"e_kt_correlationFunction_groupOf_squared_Pearson",
	"e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_generic",
	"e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_absolute",
	"e_kt_correlationFunction_groupOf_squared_Pearson_productMoment_uncentered",
	#! Ambigous call: Perason (hwere the latter has mulipel common itnerpretiaotns):
	) {
	my $useInternalEnum = 1; #! ie, as we use 'internal-redalbe' enusm in [ªbove]:
	my $enum_preStep = ""; #! ie, as we asusem the metrics are to be used 'as-is', eg, where we are Not interested in using a Separman-appraoch wrt. 'rakning' (before computing simalrity-emtrics):
	my $enum_metric_groupId = ""; #! ie, as we in this 'context' assuems that the emtic-idienties are exact enough for our use-case (where an 'oppostive' exmapole is see/examplfied in [below]).
	#printf("enum_metric_groupId=\"$enum_metric_groupId\", at %s:%d\n", __FILE__, __LINE__);
	my ($enum_metric, $enum_preStep) = Algorithm::hpLysis_metrics::hpLysis_metrics__get__simMetric($metric, $enum_preStep, $useInternalEnum, $enum_metric_groupId);
	printf("sim-metric[$metric]=\"%s\", sim-metirc-pre-step[$enum_preStep]=\"%s\", at %s:%d\n", $enum_metric, $enum_preStep, __FILE__, __LINE__);
    }    
}
{ #! Cluster-algoritms: Demosntrate the 'fetching' of diffneret cluster-algorithm-enums, using differnet 'ways to speify the clsuter-algorithms':
    #! Note[API]: in this test-block we demonstrate the use of our "hpLysis_metrics__static__translateIntoInternalEnum_clusterAlg(..)" function:
    foreach my $name (
	#! Note: the algoritms in below which are foudn 'at the same line' are synsonym-mappigns, ie, for whcih we use oru name-nroamlziaiotn-aprpaoch wrt. clsuter-algorithm-names.
	"k-means", $hpLysis_subset::e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch,
	"hca-single", "algorithm::HCA::single", $hpLysis_subset::e_hpLysis_clusterAlg_HCA_single ,
	"hca-centroid", "algorithm::HCA::centroid", $hpLysis_subset::e_hpLysis_clusterAlg_HCA_centroid,
	"miniBatch", $hpLysis_subset::e_hpLysis_clusterAlg_kCluster_altAlg__miniBatch ,
	"SOM", "som", $hpLysis_subset::e_logCluster_typeOf_writeOut_SOM ,
	# "", $hpLysis_subset:: ,
	# "",
	) {
	my $enum_id = Algorithm::hpLysis_metrics::hpLysis_metrics__static__translateIntoInternalEnum_clusterAlg($name);
	printf("Cluster-algorithm($name)=\"$enum_id\", at %s:%d\n", __FILE__, __LINE__);
    }
}
{ #! CCM, matrix-based: Demosntrate the 'fetching' of diffneret cluster-algorithm-enums, using differnet 'ways to speify the clsuter-algorithms':
    #! Note[API]: in this test-block we demonstrate the use of our "hpLysis_metrics__static__translateIntoInternalEnum_ccm__matrixBased(..)" function:
    foreach my $name (
	#! Note: the algoritms in below which are foudn 'at the same line' are synsonym-mappigns, ie, for whcih we use oru name-nroamlziaiotn-aprpaoch wrt. clsuter-algorithm-names.
	#! Note: in [”elow] we examplfiy the use of latnerive string-anems to speifcy/name 'the same' CCM:
	"Silhouette",  "Silhouette's Index","Silhouette Index", $hpLysis_subset::e_kt_matrix_cmpCluster_clusterDistance__cmpType_Silhouette ,
	"CalinskiHarabasz", "Calinski Harabasz", "Calinski-Harabasz", "VRC",	$hpLysis_subset::e_kt_matrix_cmpCluster_clusterDistance__cmpType_CalinskiHarabasz_VRC ,
	"DBI", "Davis Bouldin", "Davis Bouldins Index", $hpLysis_subset::e_kt_matrix_cmpCluster_clusterDistance__cmpType_DavisBouldin ,
	"Dunn", "Dunns Index", 	"Dunn's Index", $hpLysis_subset::e_kt_matrix_cmpCluster_clusterDistance__cmpType_Dunns ,
	) {
	my $enum_id = Algorithm::hpLysis_metrics::hpLysis_metrics__static__translateIntoInternalEnum_ccm__matrixBased($name);
	printf("Cluster-algorithm($name)=\"$enum_id\", at %s:%d\n", __FILE__, __LINE__);
    }
}
{ #! CCM, set-based: Demosntrate the 'fetching' of diffneret cluster-algorithm-enums, using differnet 'ways to speify the clsuter-algorithms':
    #! Note[API]: in this test-block we demonstrate the use of our "hpLysis_metrics__static__translateIntoInternalEnum_ccm__setBased(..)" function:
    foreach my $name (
	#! Note: the algoritms in below which are foudn 'at the same line' are synsonym-mappigns, ie, for whcih we use oru name-nroamlziaiotn-aprpaoch wrt. clsuter-algorithm-names.
	"rands Index", "Rand", "RandsIndex", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_randsIndex ,
	"Rands Adjusted Index", "ARI", "rands-adjusted", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_randsAdjustedIndex ,
	"Chi", "Chi-squared",  $hpLysis_subset::e_kt_matrix_cmpCluster_metric_chiSquared ,
	"Fowles-Mallows", "Fowles Mallows", "FowlesMallows", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_FowlesMallows ,
	"Mirkin", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_Mirkin ,
	"Wallace", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_Wallace ,
	"PR", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_PR ,
	"partition-difference", "partition difference", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_partitionDifference ,
	"fMeasure", "f-meausure", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_fMeasure ,
	"maximum-match-measure", "maximum match measure", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_maximumMatchMeasure ,
	"van-dogen", "vanDogen", "van Dogen", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_vanDogen ,
	"Strehl & Gosh", "Strehl-Gosh", "strehl Gosh",$hpLysis_subset::e_kt_matrix_cmpCluster_metric_mutualInforation_Strehl_Ghosh,
	"Fred & Jain", "Fred-Jain", "Fred Jain", "FredJain", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_mutualInforation_Fred_Jain,
	"variation of information", $hpLysis_subset::e_kt_matrix_cmpCluster_metric_variationOfInformation,
	# "", $hpLysis_subset::
	# "", $hpLysis_subset::
	# "", $hpLysis_subset::
	# "", $hpLysis_subset::
	# "", $hpLysis_subset::
	# "", $hpLysis_subset::
	# "", $hpLysis_subset::
	) {
	my $enum_id = Algorithm::hpLysis_metrics::hpLysis_metrics__static__translateIntoInternalEnum_ccm__setBased($name);
	printf("Cluster-algorithm($name)=\"$enum_id\", at %s:%d\n", __FILE__, __LINE__);
    }
}
