package Algorithm::hpLysis_wrapper_matrix;
use strict;
#use warnings "all";
#use warnings::register;
#use CGI;
use Carp;
#use CGI::Carp qw(fatalsToBrowser);    # Remove for production use
# use DynaLoader;
use hpLysis_subset;
use hpLysis_wrapper_matrix;
use hpLysis_internal_returnWrappers;
#
require Exporter;

=head
    @brief provide logics to access data from 2d-matrix, eg, wrt, the dat-astructres defined in our "kt_matrix.h" C/C++ sturcutres (oekseth, 06. arp. 2017).
=cut

#! @brief Get matrix-demions.
#! @return the diemsnions for the "mat_hpLysis" "s_kt_matrix_t" matrix.
sub C_to_perl_matrix_getDims {
#sub static_hpLysis__s_kt_matrix__getDims {
    my ($mat_hpLysis, $ifNotFound__printError) = @_;
    #! Get key-facts:
    my $nrows = hpLysis_subset::getProp__nrows__s_kt_matrix_t($mat_hpLysis);
    if(!defined($nrows) || ($nrows == 0) ) {
	if(!defined($ifNotFound__printError) || $ifNotFound__printError) {
	    printf(STDERR "!!\t Matrix is empty: s_kt_matrix_t=\"$mat_hpLysis\" Object Not defined (or altenraitvly no rows are set), ie, pelase ivnestgiate: could be due to the pobserved being de-allcoated through the call yo our \"Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims(..\", ie, investigate if (a) such a call has been made and (b) if the optional 'not-de-allcoated' funciotn-inptu-param has been used. Oserviaotn at %s:%d\n", __FILE__, __LINE__);
	    croak("then an heads-up");
	}
	return (0, 0);
    }
    my $ncols = hpLysis_subset::getProp__ncols__s_kt_matrix_t($mat_hpLysis);
    if(!defined($nrows) || ($nrows == 0) ) {
	if(!defined($ifNotFound__printError) || $ifNotFound__printError) {
	    printf(STDERR "!!\t s_kt_matrix_t Object Not defined (or altenraitvly no columns are set), ie, pelase ivnestgiate: observiaotn at %s:%d\n", __FILE__, __LINE__);
	}
	return (0, 0);
    }
    return ($nrows, $ncols);
}


#! Valdiate cosnstiency wrt. data-matrices
sub data_is_valid_matrix {
    my $matrix = $_[0];
    if(!defined($matrix)) {return 0;}

    unless (ref($matrix) eq 'ARRAY') {
        module_warn("Wanted array reference, but got a reference to ",
                    ref($matrix), ". Cannot parse matrix");
        return;
    }
    if(!defined($matrix) || !scalar(@{$matrix})) {
	module_warn("!!\t no values found in input expected to be a matrix");
    }
    my $nrows = scalar @{ $matrix };
    unless ($nrows > 0) {
        module_warn("Matrix has zero rows.  Cannot parse matrix");
        return;
    }
    my $firstrow =  $matrix->[0];
    unless (defined $firstrow) {
        module_warn("First row in matrix is undef scalar (?). Cannot parse matrix",);
        return;
    }
    unless (ref($firstrow) eq 'ARRAY') {
        module_warn("Wanted array reference, but got a reference to ",
                     ref($firstrow), ". Cannot parse matrix");
        return;
    }
    my $ncols = scalar @{ $matrix->[0] };
    unless ($ncols > 0) {
        module_warn("Row has zero columns. Cannot parse matrix");
        return;
    }
    # unless (defined($matrix->[0]->[0])) {
    #     module_warn("Cell [0,0] is undefined. Cannot parse matrix");
    #     return;
    # }
    return 1;
}




=head
    @brief converts a 'high-level' matrix into the hpLysis "s_kt_matrix_t" C/C++ matrix-object (oekseth, 06. mar. 2017).
    @param <matrix> a Perl-formattted matrix: fi values are set to "" or undef the values are 'masked' (ie, hidden from copmtuations). 
    @param <arrOf_rows>  optional: the name of the rows/heads
    @param <arrOf_cols>  optional: the name of the cols/tails
    @param <arrOf_colWeights> optional: the weight assicated to the columsn, where latter is (among others, ie, if defined/set) used when comptuing simlairty-matrix-scores.
   @return an internal matrix based on a a 'language-specific' matrix.
   @remarks  Convert a Perl-matrix to a C "s_kt_matrix_t" object
   -- howto: for an example wrt. different use-case-apttersn pelase see our associated tutoirals: if latter does Not help, then we suggest contacting senior developer [oekseth@gmail.com]
   -- application: interfaces the hpLysis high-performance cluster-analsysis-library
   -- sampleInput: matrix = [[12,7], [4 ,5], [3 ,8]]
=cut
sub perl_to_C_matrix {
    my ($matrix, $arrOf_rows, $arrOf_cols, $arrOf_colWeights) = @_;
    my $mat_hpLysis = hpLysis_subsetc::new_s_kt_matrix_t();     hpLysis_subsetc::setTo_empty__s_kt_matrix_t($mat_hpLysis);
    my $isTo_allocateWeightColumns = (defined($arrOf_colWeights) && (scalar(@{$arrOf_colWeights})));
    if(defined($matrix)) {
	my $ref = ref($matrix);
	if($ref ne "ARRAY") {
	    croak("!!\t investigate this issue, where ref(matrix)=\"$ref\"");
	}
    }
    if(defined($matrix) && scalar(@{$matrix})) {
	#! 
	#! Intlaize, first finfidng the matrix-deimenions:
	my $nrows = scalar(@{$matrix}); my $ncols_global = 0;
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    my $row = $matrix->[$row_id];
	    my $ncols = scalar(@{$row});
	    if($ncols > $ncols_global) {$ncols_global = $ncols;}
	}
	if($ncols_global == 0) {
	    return $mat_hpLysis;
	}
	#} else {
	#printf(STDERR "!!\t(input-error)\t Matrix with dims=[$nrows, $ncols_global] seems rather odd, ie, please vlaidate your inptu-call, at %s:%d\n", __FILE__, __LINE__);
#	}
	#! Allocate the object:
	hpLysis_subset::init__s_kt_matrix($mat_hpLysis, $nrows, $ncols_global, $isTo_allocateWeightColumns);
    } elsif(defined($arrOf_rows) && scalar(@{$arrOf_rows}) && defined($arrOf_cols) && scalar(@{$arrOf_cols}) ) {
	my $nrows = scalar(@{$arrOf_rows});
	my $ncols_global = scalar(@{$arrOf_cols});
	hpLysis_subset::init__s_kt_matrix($mat_hpLysis, $nrows, $ncols_global, $isTo_allocateWeightColumns);
    } else { #! then we assuemt the matrix si empty, eg, for the case where the atmrix is to b e set as part of an objects reutnr-value.
	return $mat_hpLysis;
    }
    #!
    #! Intiate "s_kt_matrix_t" object: 
    #my $mat_hpLysis = {};
    my $isTo_allocateWeightColumns = (defined($arrOf_colWeights) && (scalar(@{$arrOf_colWeights})));
    #$init__s_kt_matrix($mat_hpLysis, $nrows, $max_col_id, $isTo_allocateWeightColumns); 
    #my $func = $hpLysis_subsetc::init__s_kt_matrix;
    #$hpLysis_subsetc::init__s_kt_matrix
    #$func($mat_hpLysis, $nrows, $max_col_id, $isTo_allocateWeightColumns); 
    # my $mat_hpLysis = hpLysis_subset::init__s_kt_matrix($nrows, $max_col_id, $isTo_allocateWeightColumns); 
    # printf("mat_hpLysis=%s, at %s:%d\n", $mat_hpLysis, __FILE__, __LINE__);
    #! ------------------------------------------------------------------
    #! 
    #printf("arr-rows, at %s:%d\n", __FILE__, __LINE__);
    if(defined($arrOf_rows) && scalar(@{$arrOf_rows})) { #! then we udpate for the row-name-table: 
	my $cnt_cols = scalar(@{$arrOf_rows});
	#printf("arr-rows cnt=$cnt_cols, , at %s:%d\n", __FILE__, __LINE__);
	for(my $col_id = 0; $col_id < $cnt_cols; $col_id++) {
	    #! Then set the row-names:
	    my $name = $arrOf_rows->[$col_id];
	    #printf("set-row-name=\"%s\", at %s:%d\n", $name, __FILE__, __LINE__);
	    if( defined($name) && ($name !~ /^\s*$/) && ($name ne "-") ) {
		my $addFor_column = 0;
		# printf("set-row-name=\"%s\", at %s:%d\n", $name, __FILE__, __LINE__);
		my $s_kt_swig_retValObj_t = hpLysis_subsetc::new_s_kt_swig_retValObj_t(); #! where latter usage implies our assumption that the 'class-to-fetch-value-from' is static/non-permtuable, ie, where the "s_kt_swig_retValObj_t" is used a s a'trafer-object'.
		hpLysis_subsetc::s_kt_swig_retValObj_t_name_set($s_kt_swig_retValObj_t, $name);
		hpLysis_subset::set_string__s_kt_matrix__obj($mat_hpLysis, $col_id,  $addFor_column, $s_kt_swig_retValObj_t);		
		#hpLysis_subset::set_string__s_kt_matrix__obj($mat_hpLysis, $col_id, "dummy", $addFor_column);
#		hpLysis_subset::set_string__s_kt_matrix($mat_hpLysis, $col_id, "dummy", $addFor_column);
		#hpLysis_subset::set_string__s_kt_matrix($mat_hpLysis, $col_id, $name, $addFor_column);
		#printf("set-row-name=\"%s\", at %s:%d\n", $name, __FILE__, __LINE__);
	    }
	}
    }
    #! ------------------------------------------------------------------
    #! 
    if(defined($arrOf_cols) && scalar(@{$arrOf_cols})) { #! then we udpate for the col-name-table: 
	my $cnt_cols = scalar(@{$arrOf_cols});
	for(my $col_id = 0; $col_id < $cnt_cols; $col_id++) {
	    #! Then set the col-names:
	    my $name = $arrOf_cols->[$col_id];
	    if( defined($name) && ($name !~ /^\s*$/) && ($name ne "-") ) {
		my $addFor_column = 1;
		my $s_kt_swig_retValObj_t = hpLysis_subsetc::new_s_kt_swig_retValObj_t(); #! where latter usage implies our assumption that the 'class-to-fetch-value-from' is static/non-permtuable, ie, where the "s_kt_swig_retValObj_t" is used a s a'trafer-object'.
		hpLysis_subsetc::s_kt_swig_retValObj_t_name_set($s_kt_swig_retValObj_t, $name);
		hpLysis_subset::set_string__s_kt_matrix__obj($mat_hpLysis, $col_id,  $addFor_column, $s_kt_swig_retValObj_t);		
		#hpLysis_subset::set_string__s_kt_matrix($mat_hpLysis, $col_id, $name, $addFor_column);
	    }
	}
    }
    #! ------------------------------------------------------------------
    #! 
    if($isTo_allocateWeightColumns) { #! then we udpate for the score-table:
	my $cnt_cols = scalar(@{$arrOf_colWeights});
	for(my $col_id = 0; $col_id < $cnt_cols; $col_id++) {
	    #! Then set the weight:
	    my $score = $arrOf_colWeights->[$col_id];
	    if( defined($score) && ($score !~ /^\s*$/) && ($score ne "-") ) {
		hpLysis_subset::set_weight__s_kt_matrix($mat_hpLysis, $col_id, $score);
	    }
	}
    }
    if(defined($matrix) && scalar(@{$matrix})) { #! then we udpate for the row-name-table: 
	#! ------------------------------------------------------------------
	#!
	#! Insert the values into "s_kt_matrix_t":
	for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
	    for(my $col_id = 0; $col_id < scalar(@{$matrix->[$row_id]}); $col_id++) {
		#! Insert the vlaue into the "s_kt_matrix_t" object
		my $score = $matrix->[$row_id][$col_id];	    
		if( defined($score) && ($score !~ /^\s*$/) && ($score ne "-") ) {
		    hpLysis_subset::set_cell__s_kt_matrix($mat_hpLysis, $row_id, $col_id, $score);
		} 
	    }
	}
    }
    #! ----------------------------------------------------------------------
    #! 
    #! @return the result: 
    return  $mat_hpLysis;
}

#! Convert a  C "s_kt_matrix_t" object to a Perl-matrix
#! Ntoe: by defualt we de-allcoate the internal amtrix after 'this' fucniton-call: to avodi/inhibit/prevent the altter behavious, please set our "$isTo_deAllocate == 0".
sub C_to_perl_matrix {
    my ($mat_hpLysis, $format, $isTo_deAllocate) = @_;
    my @resultMatrix = ();
    my @arrOf_cols = ();
    my @arrOf_rows = ();
    #! Get matrix-demions.
    my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_hpLysis, 1);
    #my ($nrows, $ncols) = static_hpLysis__s_kt_matrix__getDims($mat_hpLysis, 1);
    if(!defined($nrows)) {return undef;} #! ie, thne an eror wrt. the amtrix-idemson-reading
    if(ref($mat_hpLysis) ne "hpLysis_subset::s_kt_matrix_t") { #! then we use an API-call simliart to abvoe, ie, where difference conserns our data-input (and funciotn-name):
	printf(STDERR "!!\t(un-expected-input: while inptu-object is of type=\"%s\", we expected the matrix to be in type \"hpLysis_subset::s_kt_matrix_t\", ie, please ivneistgate the latter, at %s:%d\n", ref($mat_hpLysis), "hpLysis_subset::s_kt_matrix_t", __FILE__, __LINE__);
	return undef;
    }
    
    #!
    #! Fetch the result:
    if(!defined($format) || ($format eq "") || ($format eq "matrix")) {	
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    push(@resultMatrix, []);
	    for(my $col_id = 0; $col_id < $ncols; $col_id++) {
		#! Insert the vlaue into the "s_kt_matrix_t" object
		my $score = hpLysis_subset::get_cell__s_kt_matrix($mat_hpLysis, $row_id, $col_id);
		push(@{$resultMatrix[$row_id]}, $score);
		#printf("add-score\t $score\n");
	    }
	}
	#! 
	#! Update for: row-names:
	# printf("fetch rows w/nrows=$nrows, at %s:%d\n", __FILE__, __LINE__);
	for(my $row_id = 0; $row_id < $nrows; $row_id++) {
	    my $addFor_column = 0;
	    my $name = Algorithm::hpLysis_internal_returnWrappers::get__s_kt_swig_retValObj_t__string(\&hpLysis_subset::get_string__s_kt_matrix__swig, $mat_hpLysis, $row_id, $addFor_column);
	    #my $name = hpLysis_subset::get_string__s_kt_matrix($mat_hpLysis, $row_id, $addFor_column);
	  #  printf("row-name=\"%s\", at %s:%d\n", $name, __FILE__, __LINE__);
	    push(@arrOf_rows, $name);
	}	
	#! 
	#! Update for: column-names: 
	for(my $col_id = 0; $col_id < $ncols; $col_id++) {
	    my $name = {};
	    my $addFor_column = 0;
	    my $name = Algorithm::hpLysis_internal_returnWrappers::get__s_kt_swig_retValObj_t__string(\&hpLysis_subset::get_string__s_kt_matrix__swig, $mat_hpLysis, $col_id, $addFor_column);
	    #my  $name = hpLysis_subset::get_string__s_kt_matrix($mat_hpLysis, $col_id, $addFor_column);
	    push(@arrOf_cols, $name);
	}	
    } else {
	croak("!!\t Api-option Not supported please request support for your requested matrix-format=\"$format\"");
    }    
    if(!defined($isTo_deAllocate) || ($isTo_deAllocate == 1) ) {
	hpLysis_subset::free__s_kt_matrix($mat_hpLysis);
    }
    #! ----------------------------------------------------------------------
    #! 
    #! @return the result: 
    return (\@resultMatrix, \@arrOf_rows, \@arrOf_cols);
}

#! Exprot the internal matrix to a file
sub C_to_perl_matrix_export {
   my($mat_internal, $name_resultFile) = @_;
    if(defined($mat_internal)) {
	my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_internal, 1);
	if( ($nrows > 0) && ($ncols > 0) ) {
	    #!! Export simlairty-metric:
	    hpLysis_subset::export__singleCall__s_kt_matrix_t($mat_internal, $name_resultFile, undef);
	} else {
	    printf(STDERR "!!\t (matrix-is-empty): if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a matrix-in-question. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
	}
    } else {
	printf(STDERR "!!\t internal-matrix Not defined: if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a matrix-in-question. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
    }
}
#! Exprot the internal matrix to a file
sub C_to_perl_matrix_export__summary {
   my($mat_internal, $name_resultFile, $for_both_nonTRansposedAndTransposed) = @_;
    if(defined($mat_internal)) {
	my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_internal, 1);
	if( ($nrows > 0) && ($ncols > 0) ) {
	    #!! Export simlairty-metric:
	    if(!defined($for_both_nonTRansposedAndTransposed) || ($for_both_nonTRansposedAndTransposed == 0) ) {
		hpLysis_subset::extractAndExport__deviations__singleCall__tsv__s_kt_matrix_t($mat_internal, $name_resultFile);
	    } else {
		hpLysis_subset::extractAndExport__deviations__singleCall__transposedAndNonTransposed_tsv__s_kt_matrix_t($mat_internal, $name_resultFile);
	    }
	} else {
	    printf(STDERR "!!\t (matrix-is-empty): if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a matrix-in-question. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
	}
    } else {
	printf(STDERR "!!\t internal-matrix Not defined: if latter call is nteiontal, then please vliadate that you have porperly called a funciton cosntrucitong a matrix-in-question. Observaiton at [%s]:%s\n", __FILE__, __LINE__);
    }
}
#! Import the $name_inputFile and reutnr an itnernal data-object.
sub perl_to_C_matrix_import {
    my($name_inputFile) = @_;
    if(!defined($name_inputFile) || !length($name_inputFile)) {
	croak("!!\t Input-args Not as expected");
    }
    if(-f $name_inputFile) {
	my $mat_hpLysis = hpLysis_subsetc::new_s_kt_matrix_t();     hpLysis_subset::setTo_empty__s_kt_matrix_t($mat_hpLysis);
	hpLysis_subset::import__s_kt_matrix_t($mat_hpLysis, $name_inputFile);
	return $mat_hpLysis;
    } else {
	printf(STDERR "!!\t Was not able to find the input-file=\"%s\" on your fiel-system: might be problems with reading-access/file-permissions, or altneritvly that the provided path is wrong, ie, please investigate, at %s:%d\n", $name_inputFile, __FILE__, __LINE__);
	return undef;
    }
}

#! Insert a cell into a pre-allcoated matrix, an inseriton-process where we exsehct the 'speciifed' index-pair to be wirhting hte szie-sepcs used in the itnalisiaont/laoding-pahse.
sub perl_to_C_matrix_setScore {
    my($mat_hpLysis, $head, $tail, $score) = @_;
    if(!defined($mat_hpLysis) || !defined($head) || !defined($tail) || !defined($score) ) {
	croak("\t Input-API Not correctly used");
    }
        #! Get matrix-demions.
    my ($nrows, $ncols) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix_getDims($mat_hpLysis, 1);
    if( ($head >= $nrows) || ($tail >= $ncols) ) {
	printf(STDERR "!!\t (vlaues-outside-dims)\t W expected the cell at [$head, $tail] to be inside the matrix-ims=[$nrows, $ncols], whcih is Not the case. In brief please update your itnlaiziaotn-funciont. Observaiton at %s:%d\n", __FILE__, __LINE__);
    } else { #! then set the cell:
	hpLysis_subset::set_cell__s_kt_matrix($mat_hpLysis, $head, $tail, $score);
    }
}

1;
