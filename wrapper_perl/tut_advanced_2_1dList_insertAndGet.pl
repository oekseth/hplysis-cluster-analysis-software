#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 

=head
    @file "tut_advanced_2_1dList_insertAndGet.pl"
    @brief demosntrates routine for data-insertion and data-fetching/getting for popular/important 1d-lists (oekseth, 06. arp. 2017)
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
=cut


my $list_size = 10;
# my $s_kt_list_1d_uint_t = hpLysis_subsetc::new_s_kt_list_1d_uint_t(); #$list_size);
# printf("obj=$s_kt_list_1d_uint_t, at %s:%d\n", __FILE__, __LINE__);
# #$s_kt_list_1d_uint_t = 
# hpLysis_subset::init__ref_s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $list_size);
my @arrOf_scores =  (0, 1, 2, 3); #! which is used to simpliyf validaiton:
my @arrOf_scores_strings =  ("0", "1", "2", "3"); #! which is used to simpliyf validaiton:

{ #! EXamplify insertion and fetchign/get-operaitons for: "uint":
    #! Insert into the c-struct:
    my $obj_internal = Algorithm::hpLysis_wrapper_1d::perl_to_C_uint(\@arrOf_scores);
    #! Then bakc-transalte into the Perl-struct:
    my @arrOf_backTranslated = Algorithm::hpLysis_wrapper_1d::C_to_perl_uint($obj_internal);
    # my $index = 0;
    # #! A simple insert-operation:    
    # foreach my $val (0, 1, 2, 3) {
    # 	hpLysis_subset::set_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
    # 	$index++;
    # }
    #! Then validate: A simple fetch-operaiton:
    my $index = 0;
    foreach my $val (@arrOf_scores) {
	my $val_result = $arrOf_backTranslated[$index];
	#my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
	printf("uint:[$index] result=$val_result while expected=$val, at %s:%d\n", __FILE__, __LINE__);
	#hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	$index++;
    }
}
{ #! EXamplify insertion and fetchign/get-operaitons for: "string":
    #! Insert into the c-struct:
    my $obj_internal = Algorithm::hpLysis_wrapper_1d::perl_to_C_string(\@arrOf_scores_strings);
    #! Then bakc-transalte into the Perl-struct:
    my @arrOf_backTranslated = Algorithm::hpLysis_wrapper_1d::C_to_perl_string($obj_internal);
    #! Then validate: A simple fetch-operaiton:
    my $index = 0;
    foreach my $val (@arrOf_scores_strings) {
	my $val_result = $arrOf_backTranslated[$index];
	#my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
	printf("string:[$index] result=$val_result while expected=$val, at %s:%d\n", __FILE__, __LINE__);
	#hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	$index++;
    }
}
{ #! EXamplify insertion and fetchign/get-operaitons for: "float":
    #! Insert into the c-struct:
    my $obj_internal = Algorithm::hpLysis_wrapper_1d::perl_to_C_float(\@arrOf_scores);
    #! Then bakc-transalte into the Perl-struct:
    my @arrOf_backTranslated = Algorithm::hpLysis_wrapper_1d::C_to_perl_float($obj_internal);
    #! Then validate: A simple fetch-operaiton:
    my $index = 0;
    foreach my $val (@arrOf_scores) {
	my $val_result = $arrOf_backTranslated[$index];
	#my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
	printf("float:[$index] result=$val_result while expected=$val, at %s:%d\n", __FILE__, __LINE__);
	#hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	$index++;
    }
}
{ #! EXamplify insertion and fetchign/get-operaitons for: "float":
    #! Insert into the c-struct:
    my @arrOf_triplets = (
	[0, 1, 0.1],
	[0, 3, 0.1],
	[0, 3, 0.2],
	[0, 4, 0.1],
	[1, 1, 0.3],
	[1, 3, 0.1],
	[2, 3, 0.2],
	[3, 4, 0.3],
	);
   my $obj_internal = Algorithm::hpLysis_wrapper_1d::perl_to_C_pairFloat(\@arrOf_triplets);
    #! Then bakc-transalte into the Perl-struct:
    my @arrOf_backTranslated = Algorithm::hpLysis_wrapper_1d::C_to_perl_pairFloat($obj_internal);
# use Data::Dumper;     print Dumper @arrOf_backTranslated;
    #! Then validate: A simple fetch-operaiton:
    my $index = 0;
    foreach my $row_raw (@arrOf_triplets) {
	my $row = $arrOf_backTranslated[$index];
	for(my $i = 0; $i < scalar(@{$row}); $i++) {
	    my $val = $row_raw->[$i];
	    my $val_result = $row->[$i];
	    #my $val_result = hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index);
	    printf("float:[$index][$i] result='$val_result' while expected='$val', at %s:%d\n", __FILE__, __LINE__);
	}
	#hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
	$index++;
    }
}
