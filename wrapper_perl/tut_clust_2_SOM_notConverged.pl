#!/usr/bin/perl
use strict;
use hpLysis;
use hpLysis_internal_returnWrappers;
use hpLysis_advancedAPI; 

=head
    @file "tut_clust_2_SOM_notConverged.pl"
    @brief cluster useing SOM, demsotnring a minaml exmaple
    @author Ole Kristian Ekseth (oekseth, 06. apr. 2017).
    @remarks non-covergense: when 'computing this', observe hwo the reuslt changes between different runs: for some of the 'runs' all ndoes ends up in the same clsuter, ie, hencde idnicating an unstaiblity wrt. our approach.
    @remarks in this exmapel we among others demosntrate lgocis to expro the reuslt-data-set.
    (1) object: calls usign oru "hpLysis.pm" object.
    (2) epxort comptued simlairty-matrix: both wrt. 'default values' and 'deviaiotn-socres' (both for the non-transpsoed comptued simlairyt-matrix and a strnapsoed veriosn of the latter)
    (3) epxort comptued simlairty-matrix: epxort  a summary fo the clsuter-results, generitng export-results to both a TSV-formatted data-set and a java-scirpt JS) formatted data-set (where a use-case wrt. altter is to visauzlei results using our "hpLysisVisu.js" web-based visualziaiotn-appraoch). 
=cut
{ #! A permtuation of [abvoe] where we use the "hpLysis.pm" PErl-object:
    #! SEt data and configuraitons:
    my $obj_hp = Algorithm::hpLysis->new(); 
    my $matrix = [ 
	[0.000000, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.072906],
	[0.321928, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.970951, 0.321928],
	[0.321928, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.970951, 0.000000, 0.321928],
	[0.072906, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.321928, 0.000000]
	];
    my $arrOf_rows = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    my $arrOf_cols  = ["spotting-grade:0", "spotting-grade:X-2", "spotting-grade:3-5", "spotting-grade:6-8", "spotting-grade:9-11", "spotting-grade:12-14", "spotting-grade:15-17", "spotting-grade:18-20", "uniform-8"];
    #! Intialise:
    $obj_hp->set_matrix_1($matrix, $arrOf_rows, $arrOf_cols);
    $obj_hp->set_simMetric_preCluster("city-block");
    #!
    #! Comptue the clusters:
    my $cnt_clusters_x = 3; my $cnt_clusters_y = 1;
    my ($matrix, $arrOf_rows, $arrOf_cols, $obj_cluster) = $obj_hp->compute__cluster("SOM", $cnt_clusters_x, $cnt_clusters_y); #! ie, comptue using the Euclidian simliarty-metric
    #!
    #! Investigte the results:
use Data::Dumper;
    # my $isTo_deAllocate = 0; #! ie, as we No longer Need the $mat_hpLysis_result object.
    # #my $isTo_deAllocate = 1; #! ie, as we No longer Need the $mat_hpLysis_result object.
    # my ($matrix) = Algorithm::hpLysis_wrapper_matrix::C_to_perl_matrix($mat_hpLysis_result, "", $isTo_deAllocate);
    # #! 
    # #! Write out the matrix:
    # for(my $row_id = 0; $row_id < scalar(@{$matrix}); $row_id++) {
    # 	my $row = $matrix->[$row_id];
    # 	#printf("row=$row\n");
    # 	my $ncols = scalar(@{$row});
    # 	for(my $col_id = 0; $col_id < $ncols; $col_id++) {
    # 	    my $val = $row->[$col_id];
    # 	    #my $val_result = 0; #$matrix_afterImported->[$row_id]->[$col_id];
    # 	    #my $val_result = $matrix_afterImported->[$row_id][$col_id];
    # 	    printf("float:[$row_id][$col_id]='$val', at %s:%d\n", __FILE__, __LINE__);
    # 	    #printf("float:[$row_id][$col_id]='$val' VS imported='$val_result', at %s:%d\n", __FILE__, __LINE__);
    # 	}
    # 	#hpLysis_subset::get_scalar__s_kt_list_1d_uint_t($s_kt_list_1d_uint_t, $index, $val);
    # 	#$index++;
    # }
    my @resultAfterCall = ($matrix, $arrOf_rows, $arrOf_cols, $obj_cluster);
    print "resultAfterCall=" . Dumper \@resultAfterCall;
    ;
    { #! Export the cluster-results:
	{ #! Export cluster-results to muliple Perl-objects:
	    $obj_hp->export_matrix_result("sample_file.tsv");
	    #! Export a summary of [ªbove]:
	    $obj_hp->export_matrix_result__summary("sample_file__summary.tsv");
	}
	{ #! Export cluster-results to a java-script-object-file:
	    #! Export o tsv-format:
	    my $isTo_exportInto_javaScript = 0;
	    $obj_hp->export_result_clusterResults__toFile("sample_file_clusterResult.tsv", $isTo_exportInto_javaScript);
	    #! Export tp av ajva-script-format::
	    $isTo_exportInto_javaScript = 1;
	    $obj_hp->export_result_clusterResults__toFile("sample_file_clusterResult.tsv", $isTo_exportInto_javaScript);
	} 
    }
}
