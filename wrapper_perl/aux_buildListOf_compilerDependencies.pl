use strict;

my @arrOf_strings = split(/\n/, qq(
  kt_resultS_ccmTime.c
  kt_terminal.c
  kt_terminal_clusterResult.c
  kt_terminal_parser.c
  #! ... 
  hp_image_segment.c
  #! ... 
  hp_evalHypothesis_algOnData.c #! which among others use "hp_exportMatrix.h"
  hp_exportMatrix.c #! depedns (among others) on "hp_api_fileInputTight.h":  used to extenive exprot of data-sets, eg, wr.t sim-emtric-comptarsions (oekseth, 06. jun. 2017)
  hp_distanceCluster.c
  #!
  #! Wrapper-functions to falicte the evaluation of different caseS:
  hp_histogram.c #! eg, for iamge anlaysis of histograms.
  hp_api_fileInputTight.c #! which is 'enbapsultaes' [hpLysis_api. hp_ccm, hp_distance, kt_sparse_sim] (oekseht, 06. mar. 2017)
  hp_api_entropy.c
  hp_api_semanticSim.c
  hp_api_sim_dense.c
    hp_api_norm.c


  hp_clusterFileCollection.c
  hp_clusterFileCollection_simMetricSet.c
  hp_categorizeMatrices_syn.c #! which evlautes closenss to data-matrices using sytneitc funcitosn and dat-adistrubitosn as 'point-of-reference'.

  #!
  #! Main API access-point: provide a set of gernalizwed funcitons, eg, where "hpLysis_api.c" 'wraps' funcitoanltiy in "kt_api.c"
  kt_assessPredictions.c  
  hpLysis_api.c
  hp_ccm.c #! which is a simplifed interface to our "kt_matrix_cmpCluster.h" (oekseth, 06. mar. 2017)
  kt_clusterResult_condensed.c #! where latter 'at the time of wirting' does Not have any dpeenecneis, ie, 'order in the compiling-order' is without improtance (oekseth, 06. apr. 2017).
  #!
  #! Generators
  db_inputData_matrix.c #! (oesketh, 06. mar. 2017)

  #!
  #! Functions for computaitons of Entropy (oesketh, 06. jun. 2017).
  hp_entropy.c
  kt_entropy.c
  hp_distance_wrapper.c #! which is used to 'allow' the use (in our "hp_distance.h") of the  "s_kt_matrix_t" object when computing simlairty-scores
  hp_distance.c #! which is a wrapper-fucntion to "kt_dsitance.c" (eosekth, 06. feb. 2017).

  #!
  #! API-wrappers which provide a rich degree of speciufity:  
  kt_api.c 
  kt_api__centerPoints.c
  kt_matrix_cmpCluster.c
  hp_clusterShapes.c #! which 'provides' shapes of data-distribtuions, eg, used to validate results produced by "kt_matrix_cmpCluster.c", eg, wrt. Dunn and Rand.
  kt_matrix_setOf.c

  kt_matrix_extendedLogics.c #! which depends on "kt_matrix.c" and "kt_randomGenerator_vector.c" (oekseth, 06. feb. 2017)
  
  #!
  #! Fiels for clustering-analysis:
  kt_clusterAlg_SVD.c # graphAlgorithms_svd.cxx
  kt_clusterAlg_SOM.c kt_clusterAlg_SOM_resultObject.c #graphAlgorithms_som.cxx 
  kt_clusterAlg_fixed.c   
  kt_clusterAlg_fixed__alg__miniBatch.c  
  kt_clusterAlg_fixed_resultObject.c # graphAlgorithms_kCluster.cxx 
  #! Note: the "kt_randomGenerator_vector.c" is incuuded Before the "*hca*" and "kruskal" as we are interetsted in rpoviding support for 'supporting the use of a dynamic appraoch' to idneity candiate-clsuter-vertices in k-means-clstuering (oekseth, 06. feb. 2017).
  kt_clusterAlg_kruskal.c
  kt_clusterAlg_config__ccm.c
  kt_randomGenerator_vector.c
  kt_clusterAlg_hca.c kt_clusterAlg_hca_resultObject.c e_kt_clusterAlg_hca_type.c
  kt_centrality.c 
  kt_clusterAlg_mcl.c
  kt_clusterAlg_dbScan.c
  alg_dbScan_brute.c
  alg_dbScan_brute_sciKitLearn.c
  alg_dbScan_brute_linkedListSet.c
  kd_tree.c
  kt_api_config.c
  # graphAlgorithms_centrality.cxx 
  # # FIXME: write perofmrance-tests for "graphAlgorithms_distribution.cxx" ... and identify/suggest applicaitosn (oekseth, 06. otk. 2016).
  # graphAlgorithms_distribution.cxx
  # graphAlgorithms_distance.cxx 


  
  #!
  #! Fucntiosn for computation of distance-scores:
  kt_matrix_clusterDistance.c
  kt_semanticSim.c #! (oesketh, 06. jun. 2017).
  kt_sim_string.c
  kt_sparse_sim.c #! (oekseht, 06. mar. 2017)
  kt_earth_moving_distance_emd.c #! (oekseth, 06. feb. 2018)
  kt_distance_cluster.c kt_distance.c 
  kt_metric_aux.c #! used among others by our "kt_distance.c" and our "kt_sparse_sim.c" (oekseht, 06. mar. 2017)
  kt_matrix_filter.c matrix_deviation.c matrix_cmpAlg_ROC.c
  kt_distance_slow.c  distance_2rows_slow.c 
  kt_distance__groupOf_direct.c
  kt_distance__groupOf_direct__1ToMany.c
  kt_distance__groupOf_direct__each.c

  #!
  #! Fucntions for sytentic data-construciton
  ktSim_setOf_synData_spec.c

  #!
  #! Basis data-strucutres:
  kt_matrix.c
  kt_longFilesHandler.c #! used as an auziliary for "kt_matrix.c"
  

  #! Internal functiosn for comptaution fo distance-scores:
  correlation_api.c 
  
  #! Fucntions for: all-agaisnt-all correlation-comptuation:
  correlation_inCategory_rank_spearman.c correlation_inCategory_rank_kendall.c 
  correlation_inCategory_matrix_base.c


  #correlationFunc_optimal_compute_allAgainstAll_SIMD.cxx



  #! Fucntions for correlation-comptuation when using a feature-pair (ie, comparison of two rows):
  correlation_api_rowCompare.c 
  correlationType_kendall_partialPreCompute_kendall.c
  correlationType_spearman.c  correlationType_kendall.c 
  correlationType_delta.c 
  correlationType_proximity.c correlationType_proximity_altAlgortihmComputation.c


  
  #! Auziilary funcitons for ranks, sort and distance-comptuation:
  correlation_inCategory_delta.c 
  # s_kt_correlationConfig_allAgainstAll.cxx 
  # s_kt_correlationConfig.cxx 

  e_template_correlation_tile.c 

  correlation_rank_rowPair.c

  correlation_rank.c

  #!
  #! MINE linkage-files:
  #mine.c 
  rapidMic_core.c fast_log.c
  #mine/mine.c mine/rapidMic_core.c mine/fast_log.c

  #!
  #! C-parsers:
  parse_main.c s_dataStruct_matrix_dense.c
  #c_parse/parse_main.c c_parse/s_dataStruct_matrix_dense.c

  #!
  #! Fucntiosn used itnernaly to irmpvoing the performance of our appraoch, eg, wrt. optmaized parallel scheudliong and wrt. idnetificaiton of disjoitn-sets of vertices (oekseth, 06. okt. 2016):
  kt_para_3dSchedule.c 
  kt_forest_findDisjoint.c
  db_ds_directMapping.c

  correlation_s_allAgainstAll_SIMD_inlinePostProcess.c

  config_nonRank_manyToMany.c config_nonRank_oneToMany.c   config_nonRank_each.c
  s_kt_computeTile_subResults.c
  s_allAgainstAll_config.c

  kt_set_2dsparse.c
  kt_set_1dsparse.c


  correlation_sort.c correlation_base.c

  kt_math_matrix.c #! which is perofmrance-tested in our "measure_externalCxx__nipals.cxx" (oekseth, 06. otk. 2016).
  mask_api.c mask_base.c
  aux_findPartitionFor_tiles.c
  
  math_generateDistribution.c math_generateDistribution_incompleteBeta.c math_baseCmp_operations_alglib.c
  maskAllocate.c matrix_transpose.c

  # graphAlgorithms_aux.cxx
  db_ds_bTree_rel.c   db_ds_bTree_keyValue.c
  db_searchResult.c db_searchNode_rel.c #! (oekseth, 06. mar. 2017).
  # --- 
  kt_aux_matrix_blur.c
  kt_aux_matrix_norm.c
  kt_aux_matrix_binning.c
  # --- 
  kt_hash_string.c
  kt_list_2d.c
  kt_list_1d.c #! where the latter makes use of our "kt_matrix_base.c" for file-reading.
  kt_matrix_base.c #! which is a geneirc verison of our "kt_matrix.c" (oekseth, 06. feb. 2017).
  kt_matrix_base_uint.c
  # FIXME: write perofmrance-tests for our "graph_disjointForests.cxx" ... and identify/suggest applicaitosn (oekseth, 06. otk. 2016).
  kt_list_1d_string.c #! does not have any dpednecies of improtace; is designed to simplife initeraiton with SWIG (eg, for Perl nd JavaScript).


  #!
  #! Note: [”elow] is 'amuannly' inluced usign the FILES_HCA_MEMORY attribute/param, ie, 'seperatley' copy-pasted from our "files_for_libs.cmake"
  s_dense_closestPair.c
  rowChunk.c
  dense_closestPair_base.c
  log_hca.c


  log_clusterC.c
));

#!
#! Set the b'ase-path:
my $base_path = "../src/";
my $file_name_out = "compile_depend.tsv";


#! 
#! Iterate through the [ªbvoe]e xtract from our "files_for_libs.cmake":
my $string_result = "";
foreach my $string (@arrOf_strings) {
    if(length($string) && ($string !~/^\s*\#/)) {
	$string =~ s/(\#.+$)//; #! ie, remove comment-tags
	#$string =~ s/\s*//g; #! ie, remove blank spaces.
	if($string !~/^\s*$/) { #! ie,t eh latter 'hold' values
	    my @arrOf_files = split(/\s+/, $string);
	    foreach my $file (@arrOf_files) {
		if($file !~/^\s*$/) { #! ie,t eh latter 'hold' values
		    #printf("string: $file\n");
		    if($file !~/(\.c)|(\.cxx)$/) {
			printf(STDEDRR "!!\t Investigate: string: $file\n");
		    } else {
			$string_result = $base_path . $file . " " . $string_result; #! where latter 'reverse-isenrtion-order' is to ot oru aassumptiont ath teh "files_for_libs.cmake" is orangised using a reverse-iinseriotn-roder wrt. the file-dependcneis.
		    }
		}
		#printf("string: $string\n");
	    }
	}
    }
}

#!
#! Writ eout the result:
#! Note we do Not used STDOUT to simplify evalautin og status-mesages (ie, if any).
open(FILE_OUT, ">$file_name_out") or die("!!\t Unable to open result-file \"$file_name_out\"\n");
printf(FILE_OUT "%s\n", $string_result);
close(FILE_OUT);
printf("generated result, storing latter in our \"%s\", at %s:%d\n", $file_name_out, __FILE__, __LINE__);
