package Algorithm::hpLysis_wrapper_1d;
use strict;
# use DynaLoader;
use hpLysis_subset;
require Exporter;

=head
    @brief provide logics to access data from 1d-hpLysis-lists, eg, wrt, the dat-astructres deifne din oru "kt_list_1d.h" C/C++ sturcutres (oekseth, 06. arp. 2017).
=cut

#! Fetch ther esult when a 1d-lsit  cosnsits of 'atomic' elements (eg, in cotnrast to tripelts).
sub __C_to_perl_generic_1x {
    my($obj_internal, $func_list_size, $func_item, $isA_string) = @_;
    my @arrOf_result = ();
    #printf("obj_internal=$obj_internal, at %s:%d\n", __FILE__, __LINE__);
    if(defined($obj_internal)) {
	my $list_size = 0; 
	#! Note: in [”elow] we use the "&{$..} to de-reference the fucntion-pointe,r ie, to simplify our writing of cuntion-calls by iusiogn a generic appraoch.
	#hpLysis_subset::get_count__s_kt_list_1d_string_t($obj_internal, $list_size);
	$list_size = &{$func_list_size}($obj_internal);
	#$func_list_size($obj_internal, $list_size);
	if($list_size > 0) {
	    for(my $i = 0; $i < $list_size; $i++) {
		my $value = 0;
		if(!defined($isA_string) || ($isA_string == 0) ) {
		    $value = &{$func_item}($obj_internal, $i);  #! ie, fetch the value.
		} else {
		    #my $s_kt_swig_retValObj_t = hpLysis_subsetc::new_s_kt_swig_retValObj_t(); #! where latter usage implies our assumption that the 'class-to-fetch-value-from' is static/non-permtuable, ie, where the "s_kt_swig_retValObj_t" is used a s a'trafer-object'.
		    # my $function = $func_item;
		    # my @args = ($obj_internal, $i);
		    # &{$function}(@args, $s_kt_swig_retValObj_t);
		    # my $name = hpLysis_subsetc::s_kt_swig_retValObj_t_name_get($s_kt_swig_retValObj_t);
		    # printf("name=\"$name\"\n");
		    $value = Algorithm::hpLysis_internal_returnWrappers::get__s_kt_swig_retValObj_t__string($func_item, $obj_internal, $i);
		}
		#&{$func_item}($obj_internal, $i, $value);  #! ie, fetch the value.
		#get_string__s_kt_list_1d_string($obj_internal, $i, $value);
		push(@arrOf_result, $value);
	    }
	}
    }
    return @arrOf_result;
}
#! Fetch the reuslts whne 1d-list cosnists of tripelts. 
sub __C_to_perl_generic_list {
    my($obj_internal, $func_list_size, $func_item, $funcHas__booleanRetVal) = @_;
    my @arrOf_result = ();
    if(defined($obj_internal)) {
	#! Note: in [”elow] we use the "&{$..} to de-reference the fucntion-pointe,r ie, to simplify our writing of cuntion-calls by iusiogn a generic appraoch.
	my $list_size = &{$func_list_size}($obj_internal); #, $list_size);
	if($list_size > 0) {
	    for(my $i = 0; $i < $list_size; $i++) {
		#my $value_1 = {}; my $value_2 = {}; my $value_3 = {};
		my @row = &{$func_item}($obj_internal, $i); #! ie, fetch the value.
		#, $value_1, $value_2, $value_3
		#get_string__s_kt_list_1d_string($obj_internal, $i, $value);
		if(defined($funcHas__booleanRetVal) && ($funcHas__booleanRetVal == 1) ) {
		    my $is_ok = shift(@row); #! ie, as we assuemt eh first value is return-value, ie, as we expec the funciton to 'cotnain' a 'retval'
		}
		# printf("[$i]=@row\t at %s:%d\n", __FILE__, __LINE__);
		push(@arrOf_result, \@row);
		#[$value_1, $value_2, $value_3]);
	    }
	}
    }
    #use Data::Dumper; print "arrOf_result=", Dumper @arrOf_result;
    return @arrOf_result;
}
#! Format-translates the dat-astuructre of: "s_kt_list_1d_string_t" ("kt_list_1d_string.h").
sub C_to_perl_string {
    my ($obj_internal, $isTo_deAllocate) = @_;
    my $isA_string = 1;
    my @ret_val = __C_to_perl_generic_1x($obj_internal, \&hpLysis_subset::get_count__s_kt_list_1d_string_t, \&hpLysis_subset::get_string__s_kt_list_1d_string, $isA_string);
    if(!defined($isTo_deAllocate) || ($isTo_deAllocate == 1) ) {
	hpLysis_subset::free__s_kt_list_1d_string($obj_internal); #! ie, then de-allocate.
    }    
    return @ret_val;
}
#! Format-translates the dat-astuructre of: "s_kt_list_1d_uint_t" ("kt_list_1d.h").
sub C_to_perl_uint {
    my ($obj_internal, $isTo_deAllocate) = @_;
    my @ret_val = __C_to_perl_generic_1x($obj_internal, \&hpLysis_subset::get_count__s_kt_list_1d_uint_t, \&hpLysis_subset::get_scalar__s_kt_list_1d_uint_t);
    if(!defined($isTo_deAllocate) || ($isTo_deAllocate == 1) ) {
	hpLysis_subset::free__s_kt_list_1d_uint_t($obj_internal); #! ie, then de-allocate.
    }    
    return @ret_val;
}
#! Format-translates the dat-astuructre of: "s_kt_list_1d_float_t" ("kt_list_1d.h").
sub C_to_perl_float {
    my ($obj_internal, $isTo_deAllocate) = @_;
    my @ret_val =  __C_to_perl_generic_1x($obj_internal, \&hpLysis_subset::get_count__s_kt_list_1d_float_t, \&hpLysis_subset::get_scalar__s_kt_list_1d_float_t);
    if(!defined($isTo_deAllocate) || ($isTo_deAllocate == 1) ) {
	hpLysis_subset::free__s_kt_list_1d_float_t($obj_internal); #! ie, then de-allocate.
    }    
    return @ret_val;
}
#! Format-translates the dat-astuructre of: "s_kt_list_1d_pairFloat_t" ("kt_list_1d.h").
sub C_to_perl_pairFloat {
    my ($obj_internal, $isTo_deAllocate) = @_;
    # printf("obj_internal=$obj_internal, at %s:%d\n", __FILE__, __LINE__);
    my $funcHas__booleanRetVal = 1; #! ie, as the funcotn in quesiton 'has' a reutnr-value, ie, as we are Not interested in 'associateding' the retl-va with each idneitifed tirplet.
    my @ret_val = __C_to_perl_generic_list($obj_internal, \&hpLysis_subset::get_count__s_kt_list_1d_pairFloat_t, \&hpLysis_subset::get_scalar__allVars__s_kt_list_1d_pairFloat_t, $funcHas__booleanRetVal);
    if(!defined($isTo_deAllocate) || ($isTo_deAllocate == 1) ) {
	hpLysis_subset::free__s_kt_list_1d_pairFloat_t($obj_internal); #! ie, then de-allocate.
    }    
    return @ret_val;
    #return __C_to_perl_generic_list($obj_internal, \&hpLysis_subset::get_count__s_kt_list_1d_pairFloat_t, \&get_scalar__s_kt_list_1d_pairFloat_t);
}

#! ----------------------------------
#! ----------------------------------
#! 
#! Insert elements into C-object From Perl:
#! 
#! 
#! Initate an object, add the elements, and return the nternal C-object
sub __perl_to_C_generic_1x {
    my( $obj_internal, $arrOf_scores, $func_item, $isA_stringObj) = @_;
    if(!defined($obj_internal)) {
	croak("!!\t Input Not as expected: obj_internal Not defined");
    }
    #printf("obj_internal=$obj_internal, at %s:%d\n", __FILE__, __LINE__);
    if(defined($arrOf_scores) && scalar(@{$arrOf_scores})) {	
	my $list_size = scalar(@{$arrOf_scores});
	for(my $i = 0; $i < $list_size; $i++) {
	    my $value = $arrOf_scores->[$i];
	    if(defined($value) && length($value)) {
		# printf("inserts[$i]=\"$value\", at %s:%d\n", __FILE__, __LINE__);
		if(!defined($isA_stringObj) || ($isA_stringObj == 0) ) {
		    &{$func_item}($obj_internal, $i, $value);  #! ie, then insert hte value
		} else {
		    my $s_kt_swig_retValObj_t = hpLysis_subsetc::new_s_kt_swig_retValObj_t(); #! where latter usage implies our assumption that the 'class-to-fetch-value-from' is static/non-permtuable, ie, where the "s_kt_swig_retValObj_t" is used a s a'trafer-object'.
		    hpLysis_subsetc::s_kt_swig_retValObj_t_name_set($s_kt_swig_retValObj_t, $value);
		    #! Then insert:
		    &{$func_item}($obj_internal, $i, $s_kt_swig_retValObj_t);  #! ie, then insert hte value
		}
	    }
	}
    }
}
#! Fetch the reuslts whne 1d-list cosnists of tripelts. 
sub __perl_to_C_generic_list {
    my( $obj_internal, $arrOf_scores, $func_item) = @_;
    if(!defined($obj_internal)) {
	croak("!!\t Input Not as expected: obj_internal Not defined");
    }
    if(defined($arrOf_scores) && scalar(@{$arrOf_scores})) {	
	my $list_size = scalar(@{$arrOf_scores});
	for(my $i = 0; $i < $list_size; $i++) {
	    my $row = $arrOf_scores->[$i];
	    if(defined($row) && scalar(@{$row})) {
		&{$func_item}($obj_internal, $i, @{$row});  #! ie, then insert mulitpel values.
	    }
	}
    }
}


#! Format-translates the dat-astuructre of: "s_kt_list_1d_string_t" ("kt_list_1d_string.h").
sub perl_to_C_string {
    my ($arrOf_scores) = @_;
    my $list_size = (defined($arrOf_scores)) ? scalar(@{$arrOf_scores}) : 0;
    my $obj_internal = hpLysis_subsetc::new_s_kt_list_1d_string_t(); #$list_size);
    # printf("obj=$s_kt_list_1d_uint_t, at %s:%d\n", __FILE__, __LINE__);
#$s_kt_list_1d_uint_t = 
    hpLysis_subset::setToEmpty__s_kt_list_1d_string_t($obj_internal); #, $list_size);
    if($list_size > 0) {
	my $isA_stringObj = 1;
	__perl_to_C_generic_1x($obj_internal, $arrOf_scores, \&hpLysis_subset::set_string__s_kt_list_1d_stringObj, $isA_stringObj);
#	__perl_to_C_generic_1x($obj_internal, $arrOf_scores, \&hpLysis_subset::set_string__s_kt_list_1d_string, $isA_stringObj);
    }
    return $obj_internal;
}
#! Format-translates the dat-astuructre of: "s_kt_list_1d_uint_t" ("kt_list_1d.h").
sub perl_to_C_uint {
    my ($arrOf_scores) = @_;
    my $list_size = (defined($arrOf_scores)) ? scalar(@{$arrOf_scores}) : 0;
    my $obj_internal = hpLysis_subsetc::new_s_kt_list_1d_uint_t(); #$list_size);
    # printf("obj=$s_kt_list_1d_uint_t, at %s:%d\n", __FILE__, __LINE__);
#$s_kt_list_1d_uint_t = 
    hpLysis_subset::init__ref_s_kt_list_1d_uint_t($obj_internal, $list_size);
    if($list_size > 0) {
	__perl_to_C_generic_1x($obj_internal, $arrOf_scores, \&hpLysis_subset::set_scalar__s_kt_list_1d_uint_t);
    }
    return $obj_internal;
}
#! Format-translates the dat-astuructre of: "s_kt_list_1d_float_t" ("kt_list_1d.h").
sub perl_to_C_float {
    my ($arrOf_scores) = @_;
    my $list_size = (defined($arrOf_scores)) ? scalar(@{$arrOf_scores}) : 0;
    my $obj_internal = hpLysis_subsetc::new_s_kt_list_1d_float_t(); #$list_size);
    # printf("obj=$s_kt_list_1d_uint_t, at %s:%d\n", __FILE__, __LINE__);
#$s_kt_list_1d_uint_t = 
    my $list_size = (defined($arrOf_scores)) ? scalar(@{$arrOf_scores}) : 0;
    hpLysis_subset::init__ref_s_kt_list_1d_float_t($obj_internal, $list_size);
    if($list_size > 0) {
	__perl_to_C_generic_1x($obj_internal, $arrOf_scores, \&hpLysis_subset::set_scalar__s_kt_list_1d_float_t);
    }
    return $obj_internal;
}
#! Format-translates the dat-astuructre of: "s_kt_list_1d_pairFloat_t" ("kt_list_1d.h").
#! param<arrOf_scores> is a row-set, where each row is expected to desicr be a (head, tail, score) relationship (ie, hence the "pairFloat" function-suffix-name).
sub perl_to_C_pairFloat {
    my ($arrOf_scores) = @_;
    my $list_size = (defined($arrOf_scores)) ? scalar(@{$arrOf_scores}) : 0;
    my $obj_internal = hpLysis_subsetc::new_s_kt_list_1d_pairFloat_t(); #$list_size);
    hpLysis_subset::init__ref_s_kt_list_1d_pairFloat_t($obj_internal, $list_size);
    if($list_size > 0) {
	my $row = $arrOf_scores->[0];
	my $row_size = scalar(@{$row});
	if($row_size != 3) { #! then the input is Not as expected
	    croak("!!\t The first row has cnt_rows=$row_size. The latter seems errnorus: expected each row to have 3 elemetsn (namely head, tial, score), which is Not the case");
	}
	__perl_to_C_generic_list($obj_internal, $arrOf_scores, \&hpLysis_subset::set_scalar__allVars__s_kt_list_1d_pairFloat_t);
    }
    return $obj_internal;
    #return __perl_to_C_generic_list($obj_internal, \&hpLysis_subset::get_count__s_kt_list_1d_pairFloat_t, \&get_scalar__s_kt_list_1d_pairFloat_t);
}


1;
