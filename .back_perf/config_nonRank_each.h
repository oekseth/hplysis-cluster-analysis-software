#ifndef e_config_nonRank_each_h
#define e_config_nonRank_each_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file e_config_nonRank_each
   @brief provide a simplfies interface for efficent comptuation of a given non-rank correlation-metric
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "s_allAgainstAll_config.h"

/**
   @struct config_nonRank_each
   @brief the data-strucutre to hold the neccessary informationi to efficently compute a non-ranked correlation-emtric, ei, when comparing tow feature/data rows.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/
typedef struct config_nonRank_each {
  uint ncols;
  const t_float *__restrict__ rmul1;
  const t_float *__restrict__ rmul2;
  const t_float *__restrict__ weight;
  const char *__restrict__ mask1;
  const char *__restrict__ mask2;
  s_allAgainstAll_config_t config;
} config_nonRank_each_t;

//! Initaites the config_nonRank_each_t to empty.
void setTo_empty___config_nonRank_each(config_nonRank_each_t *self);
//! @brief intiate an object of type config_nonRank_each_t
//! @return an intiated object (ie, which is set to default empty values) wrt. the config_nonRank_each_t structure-type.
config_nonRank_each_t get_init__config_nonRank_each();
//! Initiate the config_nonRank_each_t object with the complete lsit of specificaitons.
void init__config_nonRank_each(config_nonRank_each_t *self, const uint ncols, const t_float *__restrict__ rmul1, const t_float *__restrict__ rmul2, const t_float *__restrict__ weight, char *__restrict__ mask1, char *__restrict__ mask2, const s_allAgainstAll_config_t config);



#endif //! EOF
