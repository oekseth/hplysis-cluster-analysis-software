#ifndef e_config_nonRank_manyToMany_h
#define e_config_nonRank_manyToMany_h
/*
 * Copyright (2012--2018) Ole Kristian Ekseth (oekseth@gmail.com)
 *
 * This file is part of the hpLysis machine learning software.
 *
 * the hpLysis machine learning software is free software only for academic use.   you can redistribute it and/or modify
 * it under the terms of the hpLysis documentation 
 * 
 * 
 *
 * the hpLysis machine learning software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
v * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * hpLysis documentation for more details.
 *
 * You should have received a copy of the hpLysis documentation
 * along with the hpLysis machine learning software. 
 */

/**
   @file e_config_nonRank_manyToMany
   @brief provide a simplfies interface for efficent comptuation of a given non-rank correlation-metric
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
**/

#include "s_allAgainstAll_config.h"
#include "s_kt_computeTile_subResults.h" //! which if set may be used to hold the sub-results of the comptaution, eg, wrt MPI-communicaiton where there is a need for partila communicaitons.
/**
   @struct config_nonRank_manyToMany
   @brief the data-strucutre to hold the neccessary informationi to efficently compute a non-ranked correlation-emtric, ei, when comparing tow feature/data rows.
   @author Ole Kristian Ekseth (oekseth, 06. sept. 2016).
 **/
typedef struct config_nonRank_manyToMany {
  //uint index1; 
  uint nrows;
  uint ncols;
  t_float **data1; 
  t_float **data2;
  char** mask1; 
  char** mask2;
  const t_float *weight;
  e_kt_correlationFunction_t typeOf_metric;
  e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation;
  s_allAgainstAll_config_t *config_allAgainstAll;
  t_float **resultMatrix;
  s_kt_computeTile_subResults_t *obj_resultsFromTiling; 
  //t_float *arrOf_result;
} config_nonRank_manyToMany_t;

//! Initaites the config_nonRank_manyToMany_t to empty.
void setTo_empty___config_nonRank_manyToMany(config_nonRank_manyToMany_t *self);
//! @brief intiate an object of type config_nonRank_manyToMany_t
//! @return an intiated object (ie, which is set to default empty values) wrt. the config_nonRank_manyToMany_t structure-type.
config_nonRank_manyToMany_t get_init__config_nonRank_manyToMany();
//! Initiate the config_nonRank_manyToMany_t object with the complete lsit of specificaitons.
void init__config_nonRank_manyToMany(config_nonRank_manyToMany_t *self, const uint nrows, const uint ncols, t_float **data1, t_float **data2,  char** mask1, char** mask2, const t_float weight[], t_float **resultMatrix, const e_kt_correlationFunction_t typeOf_metric, const e_typeOf_metric_correlation_pearson_t typeOf_metric_correlation, s_allAgainstAll_config_t *config_allAgainstAll, s_kt_computeTile_subResults_t *obj_resultsFromTiling);




#endif //! EOF
